// import React from 'react';
// import AppNavigation from './android/app/src/Navigation/Navigation';
// import 'react-native-gesture-handler'
// import NetInfo from '@react-native-community/netinfo';
// import { StatusBar } from 'react-native';
// import StatusBarAlert from 'react-native-statusbar-alert';
// import { View } from 'react-native';
// import styles from 'react-native-dots-pagination/styles';




// class App extends React.Component {
//   constructor(props) {
//     super(props)
//     this.state = {
//       showModal: false,
//       statusColor: '',
//       networkStatus: ''
//     }
//   }

//   componentDidMount() {
//     NetInfo.addEventListener(state1 => {
//       if (state1.isConnected) {
//         StatusBar.setBackgroundColor("blue")

//         this.setState({
//           statusColor: "blue",
//           networkStatus: 'Internet Connection Available...'
//         })
//         this.setState({
//           showModal: false,
//         })
//         // this.timeout = setTimeout(() => {
//         //   this.setState({
//         //     showModal: false,
//         //   })
//         // }, 1000)

//       }
//       else {
//         StatusBar.setBackgroundColor("red")
//         this.setState({
//           showModal: true,
//           statusColor: "red",
//           networkStatus: 'Internet Connection Not Available...'
//         })

//       }
//     })
//   }

//   hideModal = () => {
//     this.setState({
//       showModal: false
//     })
//   }

//   // componentWillMount() {
//   //   StatusBar.setBackgroundColor(Colors.appblue)
//   // }


//   async componentWillUnmount() {
//     clearTimeout(this.timeout);
//   }
//   // render() {
//   //   return (
//   //     <Provider store={configureStore}>
//   //       <PersistGate persistor={persistor}>
//   //         {
//   //           this.state.showModal ?
//   //             <StatusBarAlert
//   //               visible={true}
//   //               message={this.state.networkStatus}
//   //               backgroundColor={this.state.statusColor}
//   //               color="white"
//   //               pulse='text'
//   //               // statusbarHeight={20}
//   //               // networkActivityIndicatorVisible={true}
//   //               // style={{
//   //                 // padding: 5
//   //               // }}
//   //             /> : null
//   //         }
//   //         {/* <StatusBarAlert
// 	// 							visible={this.state.showModal}
// 	// 							message={this.state.networkStatus}
// 	// 							backgroundColor={this.state.statusColor}
// 	// 							color="white"
//   //               pulse='text'
//   //               // statusbarHeight={20}
//   //               // networkActivityIndicatorVisible = {true}
//   //               // style={{
//   //               //   // padding: 5
//   //               // }}
// 	// 						/> */}
//   //         <AppNavigation />
//   //       </PersistGate>
//   //     </Provider>
//   //   )
//   // }

//   render() {
//    return  <AppNavigation />;
//   }
// }

// export default App;


/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Fragment} from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { View, Alert } from 'react-native';
import NetInfo from '@react-native-community/netinfo';
import { StatusBar } from 'react-native';
import StatusBarAlert from 'react-native-statusbar-alert';
import { configStore, persistor } from './store/configureStore';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import { BackHandler } from 'react-native';
import AppNavigation from './src/Navigation/Navigation';
import * as internateAction  from './src/Actions/InternetAction';
// import database from '@react-native-firebase/database';
import messaging from '@react-native-firebase/messaging';
import firebase from '@react-native-firebase/app';
// import firestore from '@react-native-firebase/firestore';
import Toast from 'react-native-simple-toast';
import * as loginSuccess  from './src/Actions/LoginAction';
// const reference = database().ref('/user/123');

class App extends React.Component{
  backHandler;
   constructor(props) {
    super(props)
    this.state = {
      showModal: false,
      statusColor: '#DCF0FA',
      networkStatus: ''
    }
    
  }

  async componentDidMount() {
    await this.initializeApp();
    await this.requestUserPermission();
    this.getFirebaseToken();
    // this.addRealTimeData();
    this.alertOnMessageReceive();
    // this.backgroundHandlerFireBase();
    BackHandler.addEventListener('hardwareBackPress', this.backAndroid);
    NetInfo.addEventListener(state1 => {
      if (state1.isConnected) {
        StatusBar.setBackgroundColor("#588BAE")
        // this.props.internateAction.internetAction(false);
        this.setState({
          statusColor: "#DCF0FA",
          networkStatus: 'Internet Connection Available...'
        })
        this.setState({
          showModal: false,
        })
        // this.timeout = setTimeout(() => {
        //   this.setState({
        //     showModal: false,
        //   })
        // }, 1000)

      }
      else {
        StatusBar.setBackgroundColor("red")
        // this.props.internateAction.internetAction(true);
        this.setState({
          showModal: true,
          statusColor: "red",
          networkStatus: 'Internet Connection Not Available...'
        })

      }
    })
    // this.readRealTimeData();
  }

  initializeApp = async() => {
    firebase.initializeApp({
      clientId: '880449304349-dkabisn6tlcq8r07n45u0n35qgrasdh1.apps.googleusercontent.com',
      appId: '1:880449304349:ios:0e9d1b9d549bd606f313f4',
      apiKey: 'AIzaSyD1GC6a7y8RorUAmZq7ZyUBhxAK20JOFlM',
      databaseURL: 'https://flash-f5c03.firebaseio.com',
      storageBucket: 'flash-f5c03.appspot.com',
      messagingSenderId: '880449304349',
      projectId: 'flash-f5c03',

      // enable persistence by adding the below flag
      persistence: true,
    });
  }
  

  requestUserPermission = async() => {
    console.warn('inside requestUserPermission');
    const authStatus = await messaging().requestPermission();
    const enabled =
      authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
      authStatus === messaging.AuthorizationStatus.PROVISIONAL;

    if (enabled) {
      console.warn('Authorization status:', authStatus);
    }
    console.warn('authStatus--->', authStatus);
    console.warn('enabled--->Hi working', enabled);
    
  }

  // backAndroid = () => {
  //   console.warn("in 8787675.js", this.props.selectedIcon.selectedIcon);
  //   if(this.props.selectedIcon.selectedIcon == '') {
  //     console.warn('3333');
  //     return false;
  //   }
  //   if(this.props.selectedIcon.selectedIcon == "Search" ||
  //     this.props.selectedIcon.selectedIcon == "Basket" || this.props.selectedIcon.selectedIcon == "Profile" ||
  //     this.props.selectedIcon.driverSelectedIcon == "Order" || this.props.selectedIcon.driverSelectedIcon == "Activity" ||
  //     this.props.selectedIcon.driverSelectedIcon == "Ongoing" || this.props.selectedIcon.driverSelectedIcon == "Delivered" ||
  //     this.props.selectedIcon.shopSelectedIcon == "Bid" || this.props.selectedIcon.shopSelectedIcon == "Profile"){
  //     console.warn('111');
  //     return true;
  //   } else if (this.props.selectedIcon.selectedIcon == "address") {
  //     Toast.show('Please select the location', Toast.LONG);
  //     console.warn('222');
  //     return true;
  //   }
  //   return false;
    
  // } 

  getFirebaseToken = () => {
    // Get the device token
    messaging()
      .getToken()
      .then(token => {
        console.warn('refreget token token-->',token);
        return saveTokenToDatabase(token);
      });

    // Listen to whether the token changes
    return messaging().onTokenRefresh(token => {
      console.warn('refresh token-->',token);
      saveTokenToDatabase(token);
    });
  }

  // async saveTokenToDatabase(token) {
  //   // Assume user is already signed in
  //   const userId = auth().currentUser.uid;
  
  //   // Add the token to the users datastore
  //   await firestore()
  //     .collection('users')
  //     .doc(userId)
  //     .update({
  //       tokens: firestore.FieldValue.arrayUnion(token),
  //     });
  // }

  // backgroundHandlerFireBase = () => {
  //   console.warn('inside am ruunning back');
  //   messaging().setBackgroundMessageHandler(async remoteMessage => {
  //     console.warn('Message handles in the background!', remoteMessage);
  //   });
  // }
  
  alertOnMessageReceive = () => {
    console.warn('inside alertOnMessageReceive');
    messaging().onMessage(async remoteMessage => {
      console.warn('A new FCM message arrived!', JSON.stringify(remoteMessage));
      Alert.alert('Greetings from Flash, ', JSON.stringify(remoteMessage.notification.body));
    });

  }

  // addRealTimeData = () => {
  //   database()
  //   .ref('/user/123')
  //   .set({
  //     lat: '2323423',
  //     long: '34234'
  //   })
  //   .then(() => console.warn('Data set'));
  // }

  // readRealTimeData = () => {
  //   console.warn('in reak world');
  //   database()
  //   .ref('/appVersion')
  //   .on('value', snapshot => {
  //     console.warn('User Data: ', snapshot.val());
  //   });
  // }


  hideModal = () => {
    this.setState({
      showModal: false
    })
  }



  render(){
      // return (
      //   <Provider store={configStore}>
      //     <PersistGate persistor={persistor}>
      //       {
      //         this.state.showModal ?
      //           <StatusBarAlert
      //             visible={true}
      //             message={this.state.networkStatus}
      //             backgroundColor={this.state.statusColor}
      //             color="white"
      //             pulse='text'
      //             // statusbarHeight={20}
      //             // networkActivityIndicatorVisible={true}
      //             // style={{
      //               // padding: 5
      //             // }}
      //           /> : null
      //       }
      //       <AppNavigation />
      //     </PersistGate>
      //   </Provider>
      // )
      return <AppNavigation />;
        
  }
}

const mapStateToProps = (state) => {
	return {
        userData: state.loginData,
        internateState: state.internateState,
        selectedIcon: state.selectedIcon,
	};
}

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(loginSuccess,dispatch),
        internateAction: bindActionCreators(internateAction,dispatch),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(App);

