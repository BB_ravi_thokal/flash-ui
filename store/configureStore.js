// import {createStore, applyMiddleware, compose} from 'redux';
// import thunk from 'redux-thunk';
// import { persistStore, persistReducer} from 'redux-persist';
// import AsyncStorage from '@react-native-community/async-storage'
// import {rootReducer} from './reducers';

// let middleWare = [thunk];

// const persistConfig = {
//     key: 'root',
//     storage: AsyncStorage
// }

// const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

// const persistStoreReducer = persistReducer(persistConfig, rootReducer)

// // export const configStore = createStore(persistStoreReducer
// //     ,applyMiddleware(...middleWare));

// // export let persistor = persistStore(configureStore); 

// export const configureStore = () => {
//     let store = createStore(
//         persistStoreReducer,
//         composeEnhancers(applyMiddleware(thunk, ...middleWare))
//     );
//     let persistor = persistStore(store)
//     return { store, persistor }
// }
// //     )
// //     return createStore(persistStoreReducer, applyMiddleware(...middleWare));
// // }

// // export default configureStore;

import { createStore, combineReducers } from 'redux';
import cartReducer from '../src/Reducers/cartReducer';
import loginReducer from '../src/Reducers/loginReducer';
import basketReducer from '../src/Reducers/basketReducer';
import cartModalReducer from '../src/Reducers/cartModalReducer';
import activityReducer from '../src/Reducers/activityReducers';
import internetReducer from '../src/Reducers/internetReducer';
import getPackageReducer from '../src/Reducers/getPackageReducers';
import navigationReducer from '../src/Reducers/navigationBarReducers';
import couponReducer from '../src/Reducers/couponReducer';
import currentLocationReducer from '../src/Reducers/currentLocationReducer';
import walletReducer from '../src/Reducers/walletReducers';
import driverStatusReducer from '../src/Reducers/driverStatusReducers';
import locationReducer from '../src/Reducers/locationReducer';
import { persistStore, persistReducer } from 'redux-persist';
// import storage from 'redux-persist/lib/storage'
import { 
    AsyncStorage,
    Platform
} from 'react-native';
const rootReducer = combineReducers( 
    { cartItem: cartReducer,  loginData: loginReducer, basketItem: basketReducer,
      cartState: cartModalReducer, activityData: activityReducer, internetState: internetReducer,
      getPackageData: getPackageReducer, selectedIcon: navigationReducer, availableCoupons: couponReducer,
      currentLocation: currentLocationReducer, walletBalance: walletReducer, driverStatus: driverStatusReducer, 
      driverLocation: locationReducer }
);

const persistConfig = {
    key: 'root',
    storage: AsyncStorage,
  }

const persistedReducer = persistReducer(persistConfig, rootReducer)

// configureStore = (reducer) => {
//     let store = createStore(persistedReducer)
//     let persistor = persistStore(store)
//     return { store, persistor }
    
// }

// const persistStoreReducer = persistReducer(persistConfig, rootReducer)

export const configStore = createStore(persistedReducer);

export let persistor = persistStore(configStore);


// export default () => {
//     let store = createStore(persistedReducer)
//     let persistor = persistStore(store)
//     return { store, persistor }
// }

// const appReducer = combineReducers({
//     cartItem: cartReducer,  loginData: loginReducer, basketItem: basketReducer,
//     cartState: cartModalReducer, activityData: activityReducer, internetState: internetReducer,
//     getPackageData: getPackageReducer, selectedIcon: navigationReducer, availableCoupons: couponReducer,
//     currentLocation: currentLocationReducer, walletBalance: walletReducer
// });

// const rootReducer = (state, action) => {
//     if (action.type === USER_LOGGED_OUT) {
//         state = undefined;
//     }

//     return appReducer(state, action);
// }

// export default configureStore;


