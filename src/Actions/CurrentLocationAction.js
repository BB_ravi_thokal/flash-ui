import { CHANGE_LOCATION } from '../Constant/index';

export function changeLocation(region, place) {
    return {
        type: CHANGE_LOCATION,
        currentRegion: region,
        currentPlace: place,
        payload: region
    }
}

