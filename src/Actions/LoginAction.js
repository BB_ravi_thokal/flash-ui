import { USER_LOGIN, OFFER_DETAILS, DRIVER_LOGIN, AVAILABLE_PINCODES, TOP_SLIDER } from '../Constant/index';

//Function to store userDetails after login  
export function loginSuccess(data) {
    
    return {
        type: USER_LOGIN,
        userData: data, 
        payload: data
    }

}

//Function to store offerDetails after login  
export function addOfferDetails(data) {
    return {
        type: OFFER_DETAILS,
        offerData: data, 
        payload: data
    }

}

//Function to store offerDetails after login  
export function addTopSliders(data) {
    return {
        type: TOP_SLIDER,
        topSlider: data, 
        payload: data
    }

}

//Function to store driver after login  
export function driverLoginSuccess(data) {
    return {
        type: DRIVER_LOGIN,
        driverData: data, 
        payload: data
    }

}

//Function to store pincodes 
export function savePincodes(data) {
    return {
        type: AVAILABLE_PINCODES,
        availablePincode: data, 
        payload: data
    }

}
