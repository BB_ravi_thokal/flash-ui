import { CHANGE_CART_STATE, CHANGE_BID_STATE, CHANGE_ORDER_STATE,
        CHANGE_GET_ACTIVITY_STATE, CHANGE_SEND_ACTIVITY_STATE } from '../Constant/index';

export function changeCartState(cartState,cartSum,cartCount) {
    return {
        type: CHANGE_CART_STATE,
        cartState: cartState,
        cartSum: cartSum,
        cartCount: cartCount,
    }
}

export function changeBidState(bidState) {
    return {
        type: CHANGE_BID_STATE,
        bidState: bidState,
    }
}

export function changeOrderState(orderState) {
    return {
        type: CHANGE_ORDER_STATE,
        orderState: orderState,
    }
}

export function changeGetActivityState(getState) {
    return {
        type: CHANGE_GET_ACTIVITY_STATE,
        getActivityState: getState,
    }
}

export function changeSendActivityState(sendState) {
    return {
        type: CHANGE_SEND_ACTIVITY_STATE,
        sendActivityState: sendState,
    }
}
