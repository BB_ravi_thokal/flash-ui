import { CART_CHANGE, CART_REPLACE, PLACED_CART, PLACED_BID } from '../Constant/index';

export function addToCart(cart) {
    return {
        type: CART_CHANGE,
        cartItem: cart,
        payload: cart
    }
}

export function replaceCart(cart) {
    return {
        type: CART_REPLACE,
        cartItem: cart,
        payload: cart
    }
}

export function placedCart(cart) {
    return {
        type: PLACED_CART,
        placedCart: cart,
        payload: cart
    }
}

export function placedbBid(cart) {
    return {
        type: PLACED_CART,
        placedBid: cart,
        payload: cart
    }
}
