import { INTERNET_CHANGE } from '../Constant/index';

//Function to store activity pickup address
export function internetAction(value) {
    return {
        type: INTERNET_CHANGE,
        isInternet: value, 
        payload: value
    }

}
