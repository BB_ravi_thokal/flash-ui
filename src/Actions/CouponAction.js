import { COUPON_CODE } from '../Constant/index';

export function addCoupons(coupon, appliedCoupon) {
    return {
        type: COUPON_CODE,
        couponList: coupon,
        appliedCoupon: appliedCoupon,
        payload: coupon
    }
}

