import { PICKUP_ADDRESS, DELIVERY_ADDRESS, PLACE_ACTIVITY, ONGOING_ACTIVITY } from '../Constant/index';

//Function to store activity pickup address
export function pickupAddress(data) {
    return {
        type: PICKUP_ADDRESS,
        pickupAddress: data, 
        payload: data
    }

}

//Function to store activity delivery address
export function deliveryAddress(data) {
    return {
        type: DELIVERY_ADDRESS,
        deliveryAddress: data, 
        payload: data
    }

}

//Function to store placed activity
export function placedActivity(data) {
    return {
        type: PLACE_ACTIVITY,
        placedActivity: data, 
        payload: data
    }

}

//Function to store placed activity
export function ongoingActivity(data) {
    return {
        type: ONGOING_ACTIVITY,
        ongoingActivity: data, 
        payload: data
    }

}