import { SELECTED_ICON, SELECTED_DRIVER_ICON, SELECTED_SHOP_ICON, SELECTED_ADMIN_ICON } from '../Constant/index';

//Function to store userDetails after login  
export function navigateAction(data) {
    return {
        type: SELECTED_ICON,
        selectedIcon: data, 
        payload: data
    }

}

//Function to store userDetails after login  
export function driverNavigateAction(data) {
    return {
        type: SELECTED_DRIVER_ICON,
        driverSelectedIcon: data, 
        payload: data
    }

}

//Function to store userDetails after login  
export function shopNavigateAction(data) {
    return {
        type: SELECTED_SHOP_ICON,
        shopSelectedIcon: data, 
        payload: data
    }

}

//Function to store userDetails after login  
export function adminNavigateAction(data) {
    return {
        type: SELECTED_ADMIN_ICON,
        adminSelectedIcon: data, 
        payload: data
    }

}
