import { ADD_BASKET, REPLACE_BASKET } from '../Constant/index';

export function addToBasket(basket) {
    return {
        type: ADD_BASKET,
        basketItem: basket,
        payload: basket
    }
}

export function replaceBasket(basket) {
    return {
        type: REPLACE_BASKET,
        basketItem: basket,
        payload: basket
    }
}