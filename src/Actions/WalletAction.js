import { CHANGE_WALLET_AMOUNT } from '../Constant/index';

export function updateWallet(amount, canUse) {
    return {
        type: CHANGE_WALLET_AMOUNT,
        walletBalance: amount,
        canUse: canUse,
        payload: amount
    }
}


