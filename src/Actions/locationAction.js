import { ADD_LOCATION, REPLACE_LOCATION } from '../Constant';

export function addLocation(location) {
    return {
        type: ADD_LOCATION,
        driverLocation: location,
        payload: location
    }
}

export function replaceLocation(location) {
    return {
        type: REPLACE_LOCATION,
        driverLocation: location,
        payload: location
    }
}
