import { GET_PKG_PICK_ADDRESS, GET_PKG_DELIVER_ADDRESS, PLACED_GET_PKG, ONGOING_GET_PKG } from '../Constant/index';

//Function to store activity pickup address
export function pickupAddress(data) {
    return {
        type: GET_PKG_PICK_ADDRESS,
        pickupAddress: data, 
        payload: data
    }

}

//Function to store activity delivery address
export function deliveryAddress(data) {
    return {
        type: GET_PKG_DELIVER_ADDRESS,
        deliveryAddress: data, 
        payload: data
    }

}

//Function to store placed activity
export function placedActivity(data) {
    return {
        type: PLACED_GET_PKG,
        placedActivity: data, 
        payload: data
    }
}

//Function to store placed activity
export function ongoingActivity(data) {
    return {
        type: ONGOING_GET_PKG,
        ongoingActivity: data, 
        payload: data
    }
}