import { DRIVER_STATUS_CHANGE } from '../Constant/index';

//Function to store userDetails after login  
export function driverStatusChange(status) {
    return {
        type: DRIVER_STATUS_CHANGE,
        isActive: status, 
        payload: status
    }

}
