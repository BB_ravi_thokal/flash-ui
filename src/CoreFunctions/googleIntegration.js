import Geolocation from '@react-native-community/geolocation';

class GoogleIntegration {
    constructor(){}

    getCurrentPosition = async() => {
        await Geolocation.getCurrentPosition(
            async(position) => {
                console.warn('location-->', position);    
                let region = {};
                region = {
                    latitude: position.coords.latitude,
                    longitude: position.coords.longitude,
                    latitudeDelta: 0.001,
                    longitudeDelta: 0.001
                };
                return region; 
            },
            (error) => {
                return error;
            },
            { enableHighAccuracy: false, timeout: 200000, maximumAge: 5000 },
        );
    }

    fetchAddress = async(lat, long) => {
        return fetch("https://maps.googleapis.com/maps/api/geocode/json?address=" + lat + "," + long + "&key=" + "AIzaSyCUREttpI8l0AgHrxXAJ8_NhP7XntJTNUU")
        .then((response) => response.json())
        .then((responseJson) => {
            const temp = {
                rawAddress: responseJson.results[0].address_components,
                formattedAddress: responseJson.results[0].formatted_address
            }; 
            // console.warn('temp--', temp);
            return temp; 
        });
    }

    verifyPostalCode = async(address, postalCodes) => {
        let postalCode = '';
        address.forEach(element => {
            if(element.types == 'postal_code') {
                // console.warn('inside postal cond', element.short_name);
                postalCode = element.short_name
            }
        })
        if (postalCode) {
            if (postalCodes.indexOf(postalCode) < 0) {
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }

    fetchLatLong = async(text) => {
        return fetch("https://maps.googleapis.com/maps/api/geocode/json?address=" + text + "&key=" + "AIzaSyCUREttpI8l0AgHrxXAJ8_NhP7XntJTNUU")
        .then((response) => response.json())
        .then((responseJson) => {
            
            const lat = responseJson.results[0].geometry.location.lat;
            const lng = responseJson.results[0].geometry.location.lng;
            // console.warn('lat--- lag', lat, lng);
            const region = {
                latitude: lat,
                longitude: lng,
                latitudeDelta: 0.001,
                longitudeDelta: 0.001
            };
            return region;
        });
    }
}

const googleIntegration = new GoogleIntegration();
export default googleIntegration;

