import * as Config from '../Constant/Config';
import axios from 'axios';

class CallAPI {
    constructor(){}
  
    axiosCall = async(url, reqBody = {}) => {
        return axios.post( 
            Config.URL + url,
            reqBody,
            { 
              headers: {
                  'Content-Type': 'application/json',
              }
            }
        )
        .then(res => {
            console.warn('axios response -->', res);
            if (res.data.status == 0) {
                return res;
            } else {
                return false;
            }
        });
    }
    
}

const callAPI = new CallAPI();
export default callAPI;

