import  { CallAPI } from './callAPI';
import * as Config from '../Constant/Config';

class PaytmIntegration {
    constructor(){}
  
    initiatePaymentAndroid = async(reqBody) => {
        this.setState({showLoader: true});
        const details = await CallAPI.axiosCall(Config.checkout, reqBody);
        const paymentDetails = {
            // mode: "Production",
            // mode: "Staging",
            mode: details.data.data.mode, 
            MID: details.data.data.MID,
            ORDER_ID: details.data.data.ORDER_ID,
            CUST_ID: details.data.data.CUST_ID,
            INDUSTRY_TYPE_ID: details.data.data.INDUSTRY_TYPE_ID,
            CHANNEL_ID: details.data.data.CHANNEL_ID,
            TXN_AMOUNT: details.data.data.TXN_AMOUNT,
            WEBSITE: details.data.data.WEBSITE,
            MOBILE_NO: details.data.data.MOBILE_NO,
            EMAIL : details.data.data.EMAIL,
            CALLBACK_URL: details.data.data.CALLBACK_URL,
            CHECKSUMHASH: details.data.data.CHECKSUMHASH,
        };
        return paymentDetails;
    }
    
}

const paytmIntegration = new PaytmIntegration();
export default paytmIntegration;

