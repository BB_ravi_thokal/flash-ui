/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import {
  SafeAreaView,
} from 'react-native';
import { WebView } from 'react-native-webview';

class PaytmViewComponent extends Component{
  
    constructor(props){
        super(props);    
        
        // console.warn("plave cart ---->", this.props.cartItem.placedCart);
        
    }

    handleResponse = (data) => {
        console.warn('response--->', data);
        let msg;
        if (data.title) {
            switch (data.title) {
                case 'success': 
                    msg = 'Your order placed successfully';
                    this.props.paytmResponse(data.title, msg);
                    break;
                case 'fail':
                    msg = 'Fail to verify the payment. Any transaction happened please contact our support team';
                    this.props.paytmResponse(false, msg);
                    break;
                case 'cancelled':
                    msg = 'You cancelled the transaction';
                    this.props.paytmResponse(false, msg);
                    break;
                case 'error':
                    msg = 'Something went wrong. Any transaction happened please contact our support team';
                    this.props.paytmResponse(false, msg);
                    break;    
                default:
                    console.warn('response -->', data.title);
                    // msg = 'Sorry, Not able to place your order please try again';
                    // this.props.paytmResponse(false, msg);
                    break;
            }
        }

    }

    render() {
        return (
            <SafeAreaView style={{flex: 1}}>
                <WebView
                    useWebKit={true}
                    source={{ uri: this.props.url }}
                    onNavigationStateChange={data => this.handleResponse(data)}
                />
            </SafeAreaView>
        )
    }
}

export default PaytmViewComponent;


