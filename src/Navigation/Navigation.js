import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { AppRegistry, BackHandler } from 'react-native';

import LandingPage from '../Component/IntroComponent/landingComponent';
import Login from '../Component/LoginComponent/Login';
import SignUpPage from '../Component/LoginComponent/SignUpPage.js';
import ForgotPasswordPage from '../Component/LoginComponent/ForgotPasswordPage.js';
import HomePage from '../Component/HomeComponent/HomePage.js';
import ShopDetailPage from '../Component/HomeComponent/ShopDetailPage.js';
import OrderDetailsPage from '../Component/OrderComponent/OrderDetailsPage.js';
import ShopList from '../Component/HomeComponent/ShopList.js';
import ProfilePage from '../Component/ProfileComponent/ProfilePage.js';
import AddressPage from '../Component/ProfileComponent/AddressPage.js';
import PersonalInfoPage from '../Component/ProfileComponent/PersonalInfoPage.js';
import ProductSearch from '../Component/HomeComponent/ProductSearch.js';
import ProductDetailsPage from '../Component/HomeComponent/ProductDetailsPage.js';
import AppTitle from '../Component/IntroComponent/AppTitleComponent';
import SendPackageAd from '../Component/IntroComponent/SendPackage';
import DeliveryAd from '../Component/IntroComponent/DeliveryAd';
import MonthlyCartAd from '../Component/IntroComponent/MonthlyCartAd';
import GenerateOTP from '../Component/LoginComponent/GenerateOTP';
import BasketListPage from '../Component/OrderComponent/basketListPage';
import BasketDetailsPage from '../Component/OrderComponent/BasketDetailsPage';
import FlashHubPage from '../Component/HomeComponent/FlashHubPage';
import SendPackage from '../Component/HomeComponent/SendPackagePage';
import OrderTracking from '../Component/OrderComponent/OrderTrackingPage';
import SendPackageList from '../Component/HomeComponent/SendPackageListPage';
import PackageDetailsPage from '../Component/HomeComponent/packageDetailsPage';
import TrackOrderPage from '../Component/OrderComponent/TrackOrderPage';
import PlacedOrderDetails from '../Component/OrderComponent/PlacedOrderDetails';
import DriverOrderListing from '../Component/DriverComponent/OrderListingPage';
import DriverOngoingOrder from '../Component/DriverComponent/OngoingOrderPage';
import DriverProfilePage from '../Component/DriverComponent/DriverProfilePage';
import AboutUsPage from '../Component/ProfileComponent/AboutUsPage';
import TermsAndConditionsPage from '../Component/ProfileComponent/TermsAndConditions';
import DriverActivityListing from '../Component/DriverComponent/ActivityListingPage';
import ShopHomePage from '../Component/ShopComponent/ShopHomePage';
import BidListPage from '../Component/ShopComponent/BidListPage';
import BidDetailsPage from '../Component/ShopComponent/BidDetailsPage';
import UserBidDetailsPage from '../Component/BidComponent/BidDetailsPage';
import ShopProfilePage from '../Component/ShopComponent/ShopProfilePage';
import CurrentLocationPage from '../Component/ProfileComponent/LocationPage';
import SelectLocationPage from '../Component/ProfileComponent/CurrentLocation';
import DriverAddressPage from '../Component/DriverComponent/DriverAddressPage';
import AccountDetailsPage from '../Component/ProfileComponent/AccountDetails';
import WalletPage from '../Component/ProfileComponent/WalletPage';
import AdminHomePage from '../Component/AdminComponent/AdminHome';
import AdminProfilePage from '../Component/AdminComponent/AdminProfilePage';
import RegularOfferPage from '../Component/OfferComponent/RegularOffer';
import ComboOfferPage from '../Component/OfferComponent/ComboOffer';
import ProductListPage from '../Component/HomeComponent/Prodcut';
import PaytmViewPage from '../Component/OrderComponent/PaytmViewPage';
import TrackingListPage from '../Component/OrderComponent/Track';
import ActiveOrderShop from '../Component/ShopComponent/ActiveOrderPage';
import ActiveOrderDetailsShop from '../Component/ShopComponent/ActiveOrderDetailsPage';
import WholeSalerDetails from '../Component/ShopComponent/WholeSalerDetailsPage';
import ShopOrderDetailsPage from '../Component/ShopComponent/ShopOrderDetailsPage';
import ShopAddressPage from '../Component/ShopComponent/ShopAddressPage';
import ShopPaytmViewPage from '../Component/ShopComponent/ShopPaytmViewPage';
// import AddressMapPage from '../Component/ProfileComponent/AddressPage';

// const TestNavigator = createStackNavigator ({
//   appTitle: {
//     screen: AppTitle,
//     navigationOptions: {
//       headerShown: false,
//       gesturesEnabled: true
//     },
//   },
//   deliveryAd: {
//     screen: DeliveryAd,
//     navigationOptions: {
//       headerShown: false,
//       gesturesEnabled: true
//     }
//   },
//   sendPackageAd: {
//     screen: SendPackageAd,
//     navigationOptions: {
//       headerShown: false,
//       gesturesEnabled: true
//     }
//   },
// })
const MainNavigator = createStackNavigator ({
  landingPage: {
    screen: LandingPage,
    navigationOptions: {
      headerShown: false
    },
  },
  // splash: {
  //   screen: TestNavigator
  // },
  appTitle: {
    screen: AppTitle,
    navigationOptions: {
      headerShown: false,
      gesturesEnabled: true
    },
  },
  deliveryAd: {
    screen: DeliveryAd,
    navigationOptions: {
      headerShown: false,
      gesturesEnabled: true
    }
  },
  sendPackageAd: {
    screen: SendPackageAd,
    navigationOptions: {
      headerShown: false,
      gesturesEnabled: true
    }
  },
  monthlyCartAd: {
    screen: MonthlyCartAd,
    navigationOptions: {
      headerShown: false,
      gesturesEnabled: true
    }
  },
    login: {
      screen: Login,
      navigationOptions: {
        headerShown: false,
      }
    },
    
    generateOTP: {
      screen: GenerateOTP,
      navigationOptions: {
        headerShown: false,
        
      }
    },
    signUp: {
      screen: SignUpPage,
      navigationOptions: {
        headerShown: false,
      }
    },
    forgotPassword: {
      screen: ForgotPasswordPage,
      navigationOptions: {
        headerShown: false,
        
      }
    },
    homePage: {
      screen: HomePage,
      navigationOptions: {
        headerShown: false,
        title: 'Home'
      }
    },
    flashHubPage: {
      screen: FlashHubPage,
      navigationOptions: {
        headerShown: true,
        title: 'Store',
        // safeAreaInsets: { top: 5 },
        headerTitle: 'Shop details',
        headerTintColor: 'white',
        headerTitleStyle: {
          fontWeight: 'bold',
          color: "white"
        },
        headerStyle: {
          backgroundColor: '#00CCFF',
          // height: 55
        },
      }
    },
    productListPage: {
      screen: ProductListPage,
      navigationOptions: {
        headerShown: true,
        // safeAreaInsets: { top: 5 },
        headerTitle: 'Shop menu',
        headerTintColor: 'white',
        headerTitleStyle: {
          fontWeight: 'bold',
          color: "white",
        },
        headerStyle: {
          backgroundColor: '#00CCFF',
          // height: 55
        },
      }
    },
    shopDetailPage: {
      screen: ShopDetailPage,
      navigationOptions: {
        headerShown: true,
        // safeAreaInsets: { top: 5 },
        headerTitle: 'Shop Detail',
        headerTintColor: 'white',
        headerTitleStyle: {
          fontWeight: 'bold',
          color: "white"
        },
        headerStyle: {
          backgroundColor: '#00CCFF',
          // height: 55
        },
      }
    },
    // orderListPage: {
    //   screen: OrderListPage
    // },
    orderDetailsPage: {
      screen: OrderDetailsPage,
      navigationOptions: {
        headerShown: true,
        title: 'My Order',
        // safeAreaInsets: { top: 5 },
        headerTitle: 'Order Details',
        headerTintColor: 'white',
        headerTitleStyle: {
          fontWeight: 'bold',
          color: "white"
        },
        headerStyle: {
          backgroundColor: '#00CCFF',
          // height: 55
        },
      }
    },
    shopList: {
      screen: ShopList,
      navigationOptions: {
        headerShown: true,
        title: 'Shop list',
        // safeAreaInsets: { top: 5 },
        headerTitle: 'Shop List',
        headerTintColor: 'white',
        headerTitleStyle: {
          fontWeight: 'bold',
          color: "white"
        },
        headerStyle: {
          backgroundColor: '#00CCFF',
          // height: 55
        },
      }
    },
    profilePage: {
      screen: ProfilePage,
      // safeAreaInsets: { top: 5 },
      navigationOptions: {
        headerShown: false,
        title: 'Profile'
      }
    },
    addressPage: {
      screen: AddressPage,
      navigationOptions: {
        // safeAreaInsets: { top: 5 },
        headerShown: true,
        headerTitle: 'Address',
        headerTintColor: 'white',
        headerTitleStyle: {
          fontWeight: 'bold',
          color: "white",
        },
        headerStyle: {
          backgroundColor: '#00CCFF',
          // height: 55
        },
      }
    },
    shopAddressPage: {
      screen: ShopAddressPage,
      navigationOptions: {
        // safeAreaInsets: { top: 5 },
        headerShown: true,
        headerTitle: 'Address',
        headerTintColor: 'white',
        headerTitleStyle: {
          fontWeight: 'bold',
          color: "white",
        },
        headerStyle: {
          backgroundColor: '#00CCFF',
          // height: 55
        },
      }
    },
    productSearch: {
      screen: ProductSearch,
      // safeAreaInsets: { top: 5 },
      navigationOptions: {
        headerShown: false,
        title: 'Search'
      }
    },
    productDetailsPage: {
      screen: ProductDetailsPage,
      navigationOptions: {
        headerShown: true,
        // safeAreaInsets: { top: 5 },
        headerTitle: 'Product Details',
        headerTintColor: 'white',
        headerTitleStyle: {
          fontWeight: 'bold',
          color: "white"
        },
        headerStyle: {
          backgroundColor: '#00CCFF',
          // height: 55
        },
      }
    },
    personalInfoPage: {
      screen: PersonalInfoPage,
      navigationOptions: {
        headerShown: true,
        // safeAreaInsets: { top: 5 },
        headerTitle: 'Edit Profile',
        headerTintColor: 'white',
        headerTitleStyle: {
          fontWeight: 'bold',
          color: "white",
        },
        headerStyle: {
          backgroundColor: '#00CCFF',
          // height: 55
        },
      }
    },
    basketListPage: {
      screen: BasketListPage,
      // safeAreaInsets: { top: 5 },
      navigationOptions: {
        headerShown: false,
        title: 'Baskets'
      }
    },
    basketDetailsPage: {
      screen: BasketDetailsPage,
      navigationOptions: {
        headerShown: true,
        // safeAreaInsets: { top: 5 },
        headerTitle: 'Basket content',
        headerTintColor: 'white',
        headerTitleStyle: {
          fontWeight: 'bold',
          color: "white",
        },
        headerStyle: {
          backgroundColor: '#00CCFF',
          // height: 55
        },
      }
    },
    sendPackage: {
      screen: SendPackage,
      navigationOptions: {
        headerShown: true,
        // safeAreaInsets: { top: 5 },
        title: 'Package',
        headerTitle: 'Deliver Package',
        headerTintColor: 'white',
        headerTitleStyle: {
          fontWeight: 'bold',
          color: "white",
        },
        headerStyle: {
          backgroundColor: '#00CCFF',
          // height: 55
        },
      }
    },
    orderTracking: {
      screen: OrderTracking,
      navigationOptions: {
        headerShown: true,
        // safeAreaInsets: { top: 5 },
        title: 'Tracking',
        headerTitle: 'Track order',
        headerTintColor: 'white',
        headerTitleStyle: {
          fontWeight: 'bold',
          color: "white",
        },
        headerStyle: {
          backgroundColor: '#00CCFF',
          // height: 55
        },
      },
    },
    sendPackageList: {
      screen: SendPackageList,
      navigationOptions: {
        headerShown: true,
        // safeAreaInsets: { top: 5 },
        headerTitle: 'Activity List',
        headerTintColor: 'white',
        headerTitleStyle: {
          fontWeight: 'bold',
          color: "white",
        },
        headerStyle: {
          backgroundColor: '#00CCFF',
          // height: 55
        },
      }
    },
    packageDetailsPage: {
      screen: PackageDetailsPage,
      navigationOptions: {
        headerShown: true,
        // safeAreaInsets: { top: 5 },
        headerTitle: 'Activity List',
        headerTintColor: 'white',
        headerTitleStyle: {
          fontWeight: 'bold',
          color: "white",
        },
        headerStyle: {
          backgroundColor: '#00CCFF',
          // height: 55
        },
      }
    },
    trackOrderPage: {
      screen: TrackOrderPage,
      navigationOptions: {
        headerShown: true,
        // safeAreaInsets: { top: 5 },
        headerTitle: 'Track your order',
        headerTintColor: 'white',
        headerTitleStyle: {
          fontWeight: 'bold',
          color: "white",
        },
        headerStyle: {
          backgroundColor: '#00CCFF',
          // height: 55
        },
      }
    },
    placedOrderDetails: {
      screen: PlacedOrderDetails,
      navigationOptions: {
        headerShown: true,
        // safeAreaInsets: { top: 5 },
        headerTitle: 'Order summary',
        headerTintColor: 'white',
        headerTitleStyle: {
          fontWeight: 'bold',
          color: "white",
        },
        headerStyle: {
          backgroundColor: '#00CCFF',
          // height: 55
        },
      }
    },
    driverOrderListing: {
      screen: DriverOrderListing,
      navigationOptions: {
        headerShown: true,
        // safeAreaInsets: { top: 5 },
        headerTitle: 'Pending Orders',
        headerTintColor: 'white',
        headerTitleStyle: {
          fontWeight: 'bold',
          color: "white",
        },
        headerStyle: {
          backgroundColor: '#00CCFF',
          // height: 55
        },
      }
    },
    driverOngoingOrder: {
      screen: DriverOngoingOrder,
      navigationOptions: {
        headerShown: true,
        // safeAreaInsets: { top: 5 },
        headerLeft: null,
        headerTitle: 'Ongoing deliveries',
        headerTintColor: 'white',
        headerTitleStyle: {
          fontWeight: 'bold',
          color: "white",
        },
        headerStyle: {
          backgroundColor: '#00CCFF',
          // height: 55
        },
      }
    },
    driverProfilePage: {
      screen: DriverProfilePage,
      navigationOptions: {
        headerShown: true,
        // safeAreaInsets: { top: 5 },
        headerLeft: null,
        headerTitle: 'My Profile',
        headerTintColor: 'white',
        headerTitleStyle: {
          fontWeight: 'bold',
          color: "white",
        },
        headerStyle: {
          backgroundColor: '#00CCFF',
          // height: 55
        },
      }
    },
    driverAddressPage: {
      screen: DriverAddressPage,
      navigationOptions: {
        headerShown: true,
        // safeAreaInsets: { top: 5 },
        headerLeft: null,
        headerTitle: 'Select location',
        headerTintColor: 'white',
        headerTitleStyle: {
          fontWeight: 'bold',
          color: "white",
        },
        headerStyle: {
          backgroundColor: '#00CCFF',
          // height: 55
        },
      }
    },
    aboutUsPage: {
      screen: AboutUsPage,
      navigationOptions: {
        headerShown: true,
        // safeAreaInsets: { top: 5 },
        headerTitle: 'About Us',
        headerTintColor: 'white',
        headerTitleStyle: {
          fontWeight: 'bold',
          color: "white",
        },
        headerStyle: {
          backgroundColor: '#00CCFF',
          // height: 55
        },
      }
    },
    termsAndConditionsPage: {
      screen: TermsAndConditionsPage,
      navigationOptions: {
        headerShown: true,
        // safeAreaInsets: { top: 5 },
        headerTitle: 'Terms & conditions',
        headerTintColor: 'white',
        headerTitleStyle: {
          fontWeight: 'bold',
          color: "white",
        },
        headerStyle: {
          backgroundColor: '#00CCFF',
          // height: 55
        },
      }
    },
    driverActivityListing: {
      screen: DriverActivityListing,
      navigationOptions: {
        headerShown: true,
        headerLeft: null,
        // safeAreaInsets: { top: 5 },
        headerTitle: 'Pending activities',
        headerTintColor: 'white',
        headerTitleStyle: {
          fontWeight: 'bold',
          color: "white",
        },
        headerStyle: {
          backgroundColor: '#00CCFF',
          // height: 55
        },
      }
    },
    shopHomePage: {
      screen: ShopHomePage,
      navigationOptions: {
        headerShown: false,
        headerTitle: 'Home',
      },
      headerStyle: {
        backgroundColor: '#00CCFF',
        // height: 55
      },
    },
    bidListPage: {
      screen: BidListPage,
      navigationOptions: {
        headerShown: false,
      },
      headerStyle: {
        backgroundColor: '#00CCFF',
        // height: 55
      },
    },
    bidDetailsPage: {
      screen: BidDetailsPage,
      navigationOptions: {
        // safeAreaInsets: { top: 5 },
        headerShown: true,
        headerTitle: 'Bid Details',
        headerTintColor: 'white',
        headerTitleStyle: {
          fontWeight: 'bold',
          color: "white",
        },
        headerStyle: {
          backgroundColor: '#00CCFF',
          // height: 55
        },
      }
    },
    userBidDetailsPage: {
      screen: UserBidDetailsPage,
      navigationOptions: {
        headerShown: true,
        // safeAreaInsets: { top: 5 },
        headerTitle: 'Bid Details',
        headerTintColor: 'white',
        headerTitleStyle: {
          fontWeight: 'bold',
          color: "white",
        },
        headerStyle: {
          backgroundColor: '#00CCFF',
          // height: 55
        },
      }
    },
    currentLocationPage: {
      screen: CurrentLocationPage,
      navigationOptions: {
        headerShown: true,
        // safeAreaInsets: { top: 5 },
        headerTitle: 'Select current location',
        headerTintColor: 'white',
        headerTitleStyle: {
          fontWeight: 'bold',
          color: "white",
        },
        headerStyle: {
          backgroundColor: '#00CCFF',
          // height: 55
        },
      }
    },
    selectLocationPage: {
      screen: SelectLocationPage,
      navigationOptions: {
        headerShown: true,
        headerLeft: null,
        gesturesEnabled: false,
        // safeAreaInsets: { top: 5 },
        headerTitle: 'Confirm your location',
        headerTintColor: 'white',
        headerTitleStyle: {
          fontWeight: 'bold',
          color: "white",
        },
        headerStyle: {
          backgroundColor: '#00CCFF',
          // height: 55
        },
      }
    },
    accountDetailsPage: {
      screen: AccountDetailsPage,
      navigationOptions: {
        headerShown: true,
        // safeAreaInsets: { top: 5 },
        headerTitle: 'Account Details',
        headerTintColor: 'white',
        headerTitleStyle: {
          fontWeight: 'bold',
          color: "white",
        },
        headerStyle: {
          backgroundColor: '#00CCFF',
          // height: 55
        },
      }
    },
    walletPage: {
      screen: WalletPage,
      navigationOptions: {
        headerShown: true,
        // safeAreaInsets: { top: 5 },
        headerTitle: 'My flash wallet',
        headerTintColor: 'white',
        headerTitleStyle: {
          fontWeight: 'bold',
          color: "white",
        },
        headerStyle: {
          backgroundColor: '#00CCFF',
          // height: 55
        },
      }
    },
    adminHomePage: {
      screen: AdminHomePage,
      navigationOptions: {
        headerShown: true,
        // safeAreaInsets: { top: 5 },
        headerTitle: 'Refund request list',
        headerTintColor: 'white',
        headerTitleStyle: {
          fontWeight: 'bold',
          color: "white",
        },
        headerStyle: {
          backgroundColor: '#00CCFF',
          // height: 55
        },
      }
    },
    adminProfilePage: {
      screen: AdminProfilePage,
      navigationOptions: {
        headerShown: true,
        // safeAreaInsets: { top: 5 },
        headerTitle: 'My profile',
        headerTintColor: 'white',
        headerTitleStyle: {
          fontWeight: 'bold',
          color: "white",
        },
        headerStyle: {
          backgroundColor: '#00CCFF',
          // height: 55
        },
      }
    },
    regularOfferPage: {
      screen: RegularOfferPage,
      navigationOptions: {
        headerShown: true,
        // safeAreaInsets: { top: 5 },
        headerTitle: 'My offers',
        headerTintColor: 'white',
        headerTitleStyle: {
          fontWeight: 'bold',
          color: "white",
        },
        headerStyle: {
          backgroundColor: '#00CCFF',
          // height: 55
        },
      }
    },
    comboOfferPage: {
      screen: ComboOfferPage,
      navigationOptions: {
        headerShown: true,
        // safeAreaInsets: { top: 5 },
        headerTitle: 'Combo packs',
        headerTintColor: 'white',
        headerTitleStyle: {
          fontWeight: 'bold',
          color: "white",
        },
        headerStyle: {
          backgroundColor: '#00CCFF',
          // height: 55
        },
      }
    },
    activeOrderShop: {
      screen: ActiveOrderShop,
      navigationOptions: {
        headerShown: false,
        title: 'Active orders'
      }
    },
    activeOrderDetailsShop: {
      screen: ActiveOrderDetailsShop,
      navigationOptions: {
        headerShown: true,
        // safeAreaInsets: { top: 5 },
        headerTitle: 'Order details',
        headerTintColor: 'white',
        headerTitleStyle: {
          fontWeight: 'bold',
          color: "white",
        },
        headerStyle: {
          backgroundColor: '#00CCFF',
          // height: 55
        },
      }
    },
    shopOrderDetailsPage: {
      screen: ShopOrderDetailsPage,
      navigationOptions: {
        headerShown: true,
        // safeAreaInsets: { top: 5 },
        headerTitle: 'Order details',
        headerTintColor: 'white',
        headerTitleStyle: {
          fontWeight: 'bold',
          color: "white",
        },
        headerStyle: {
          backgroundColor: '#00CCFF',
          // height: 55
        },
      }
    },
    shopProfilePage: {
      screen: ShopProfilePage,
      // safeAreaInsets: { top: 5 },
      navigationOptions: {
        headerShown: false,
        headerTitle: 'Profile',
      }
    },
    wholeSalerDetails: {
      screen: WholeSalerDetails,
      // safeAreaInsets: { top: 5 },
      navigationOptions: {
        headerShown: false,
        headerTitle: 'Flash Mart',
      }
    },
    paytmViewPage: {
      screen: PaytmViewPage,
      // safeAreaInsets: { top: 5 },
      navigationOptions: {
        headerShown: false,
        gesturesEnabled: false
      }
    },
    shopPaytmViewPage: {
      screen: ShopPaytmViewPage,
      // safeAreaInsets: { top: 5 },
      navigationOptions: {
        headerShown: false,
        gesturesEnabled: false
      }
    },
    trackingListPage: {
      screen: TrackingListPage,
      // safeAreaInsets: { top: 5 },
      navigationOptions: {
        headerShown: true,
        // safeAreaInsets: { top: 5 },
        headerTitle: 'My orders',
        headerTintColor: 'white',
        headerTitleStyle: {
          fontWeight: 'bold',
          color: "white",
        },
        headerStyle: {
          backgroundColor: '#00CCFF',
          // height: 55
        },
      }
    },
  },
  
  {
    initialRouteName: "landingPage"
  });


const AppNavigation = createAppContainer(MainNavigator);

export default AppNavigation;

AppRegistry.registerComponent('AppTitle','SendPackageAd','DeliveryAd','MonthlyCartAd','Login','ForgotPasswordPage','SignUpPage',
                              'GenerateOTP','HomePage','OrderDetailsPage','ShopList','ProfilePage','AddressPage','FlashHubPage',
                              'ProductSearch','ProductDetailsPage','PersonalInfoPage','BasketListPage','BasketDetailsPage','SendPackage',
                              'DriverOrderListing', 'DriverOngoingOrder', 'DriverProfilePage', 'AboutUsPage', 'DriverActivityListing',
                              'ShopHomePage', 'BidDetailsPage', 'UserBidDetailsPage','ShopProfilePage','OrderTracking',
                              "CurrentLocationPage", "SelectLocationPage", "LandingPage", "DriverAddressPage", "AccountDetailsPage",
                              "WalletPage", "AdminHomePage", "AdminProfilePage", "RegularOfferPage", "ProductListPage", "ComboOfferPage",
                              "PaytmViewPage", "TrackingListPage", "activeOrderShop", "ActiveOrderDetailsShop", "WholeSalerDetails", 
                              "ShopOrderDetailsPage", "ShopAddressPage", "ShopPaytmViewPage", () => App);


