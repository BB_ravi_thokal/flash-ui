/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import * as addToCart  from '../../Actions/CartItem';
import { bindActionCreators } from 'redux';
import axios from 'axios';
import * as Config from '../../Constant/Config.js';
import * as Constant from '../../Constant/Constant.js';
import NavigationBar from '../ReusableComponent/NavigationBar.js';
import * as loginSuccess  from '../../Actions/LoginAction';
import * as walletAction  from '../../Actions/WalletAction';
import  s  from '../../sharedStyle.js'
import PopupModal from '../ReusableComponent/PopupComponent.js';
import AddressModal from '../ReusableComponent/addressPopUp';
import * as cartModalAction  from '../../Actions/CartModalAction';
import CouponModal from '../ReusableComponent/couponModal';
// import Paytm from 'react-native-paytm';
import * as navigateAction  from '../../Actions/NavigationBarAction';
import { PaytmIntegration } from '../../CoreFunctions/paytmIntegration';
import callAPI from '../../CoreFunctions/callAPI';
import ImageViewer from 'react-native-image-zoom-viewer';
// import 'bootstrap/dist/css/bootstrap.min.css';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  Dimensions,  
  Image,
  TouchableOpacity,
  FlatList ,
  Platform,
  ToastAndroid,
  modal,
  Modal
} from 'react-native';
import Toast from 'react-native-simple-toast';
import InternateModal from '../ReusableComponent/InternetModal';
import AnimatedLoader from "react-native-animated-loader";
import SceneLoader from 'react-native-scene-loader';
import * as couponAction  from '../../Actions/CouponAction';
import PaytmViewComponent from '../../CoreComponents/PaytmView';

class OrderDetailsPage extends React.Component{
  constructor(props) {
    super(props);
    this.state={
      // cartProduct: this.props.cartItem.cartItem,
      cartTotal: 0.00,
      cartTotalExlGst: 0.00,
      totalPayable: 0.00,
      modalVisible: false,
      removeProduct: false,
      selectedProduct: {},
      data : "", 
      address:[],
      addAddress: false,   
      addressModal: false,
      defaultAddress: [],
      appliedCoupon: "--//--",
      totalDiscount: 0.00,
      deliveryCharges: 0.00,
      couponList: [],
      isCouponAvailable: false,
      checkMashObj: {},
      showLoader: false,
      walletAmountUsed: 0.00,
      paidAmount: 0.00,
      paytmUrl: Config.paytmViewURL,
      isPastOrder: false,
      couponModal: false,
      gst: 0.00,
      disableCheckout: false,
      modalVisibleOrder: false,
      confirmationMessage: '',
      paymentType: ''
    }
    console.warn("in con ",this.props.userData.userData.deliveryCharge);
    
    
    if(this.props.userData.userData.addressDetails.length > 0) {
      this.state.defaultAddress.push(this.props.userData.userData.addressDetails[0]);
      this.setState({
        defaultAddress: this.state.defaultAddress
      })
    }

  }

  async componentDidMount(){
    
    if (this.props.navigation.state.params) {
      if (this.props.navigation.state.params.orderType == 'Past') {
        this.setState({isPastOrder: true})
      }
    }
    const ind = this.props.cartItem.cartItem.findIndex(ind => ind.isProductAvailable === true);
    console.warn('=====================> ind', ind);
    if (ind < 0) {
      await this.setState({
        disableCheckout: true
      });
    } 
    
    // Paytm.addListener(Paytm.Events.PAYTM_RESPONSE, this.handlePaytmResponse);

    this.replaceCart();
    // Api to fetch available coupons
    const isLogin = await this.checkLogin();
    if (isLogin) {
      const reqBody = {
        "userId": this.props.userData.userData.userId,
      };
      const getCoupon = await callAPI.axiosCall(Config.applyCoupon, reqBody);
      console.warn('res get coupon', getCoupon);
      if (getCoupon) {
        this.props.couponAction.addCoupons(getCoupon.data.result, '');
      } else {
        this.setState ({isCouponAvailable: false});
      } 
    } else {
      this.setState ({isCouponAvailable: false});
    }    
  }

  // handlePaytmResponse = (resp) => {
  //   console.warn('response --', resp);
  //   let paytmRes;
  //   if (Platform.OS == 'android') {
  //     paytmRes = resp;
  //   } else {
  //     paytmRes = JSON.parse(resp.response);
  //   }
    
  //   if (paytmRes.STATUS == 'TXN_FAILURE') {
  //     if (paytmRes.RESPCODE == "141") {
  //       Toast.show('You cancelled the transaction',Toast.LONG);
  //     } else {
  //       this.placedOrdered('Failed', paytmRes);  
  //     }
  //   }  else if (paytmRes.STATUS == 'TXN_SUCCESS'){
  //     this.placedOrdered('Completed', paytmRes);
  //   } else {
  //     if (paytmRes.RESPCODE == "141") {
  //       Toast.show('You cancelled the transaction',Toast.LONG);
  //     } else {
  //       this.placedOrdered('Failed', paytmRes);  
  //     }
  //   }

  // };

    //-----------------------------
    CartListing = ({data}) => {
      return (  

        <View style={[styles.prodcutListingView, {backgroundColor: data.backGroundColor}]}>
          
            <View style={styles.addCardImage}>
              <Image style={styles.productImage} source={{uri: data.productLogo}}/>
            </View>
            <View style={styles.stores}>
              
              {!data.comboId
              ? 
              <View style={{flex:10}}>
                <Text style={[s.subHeaderText, {marginBottom: 2}]} numberOfLines={4}>{data.productName} </Text>
              </View>
              :
              <View style={{flex:10}}>
                <Text style={s.subHeaderText} numberOfLines={4}>{data.comboName} </Text>
              </View>
              }
              
              <View style={styles.Mystores}>
              {!data.comboId
              ? 
              <View style={{flexDirection: "column", flex: 7}}>
                <Text style={[s.normalText, {marginBottom: 2}]} numberOfLines={4}>Qty:{data.unitCost}/{data.unit}</Text>
                <Text style={[s.normalText, {marginBottom: 2}]} numberOfLines={1}>Total: ₹{data.totalCost}</Text>
                <Text style={[s.normalText, {marginBottom: 2}]} numberOfLines={3}>Shop Name: {data.shopName}</Text>
                {data.isBogo 
                ?
                  <Text style={[s.subHeaderText]} numberOfLines={2}>Buy 1 Get 1 Free</Text>
                :
                  null
                }
                {!data.isProductAvailable 
                ?
                  <Text style={[s.subHeaderText]} numberOfLines={2}>Out of stock</Text>
                :
                  null
                }
              </View>
              :
              <View style={{flexDirection: "column", flex: 7}}>
                {/* <Text style={[s.normalText, {marginBottom: 2}]} numberOfLines={4}>{data.comboDescription}</Text> */}
                <Text style={[s.normalText, {marginBottom: 2}]} numberOfLines={1}>Total: ₹{data.totalCost}</Text>
                <Text style={[s.normalText, {marginBottom: 2}]} numberOfLines={3}>Shop Name: {data.shopName}</Text>
                {!data.isProductAvailable 
                ?
                <Text style={[s.subHeaderText]} numberOfLines={2}>Out of stock</Text>
                :
                null
                }
              </View>
              }
              
              {!data.isProductAvailable 
              ?
                <TouchableOpacity onPress={() => this.removeUnAvailable(data)}>
                  {/* <Image source={require('./android/assets/edit.png')}/>    */}
                  <Image style={{height: 20, width: 20}} source={require('../../../assets/delete.png')}/>
                </TouchableOpacity>
              : 
                <View style={styles.flexRow}>
                  <View style={styles.flexRowAdd}>
                    <TouchableOpacity onPress={() => this.decrementCount(data)}>
                      <View style={styles.orderCardButton}>
                        <Text style={{fontSize:18,color:"#0080FE"}}>-</Text>
                      </View>
                    </TouchableOpacity>
                      <Text style={styles.orderCount}>{data.counter}</Text>
                    <TouchableOpacity onPress={() => this.incrementCount(data)}>
                      <View style={styles.orderCardButton}>
                        <Text style={{fontSize:18,color:"#0080FE"}}>+</Text>
                      </View>
                    </TouchableOpacity>
                  </View>

                </View>
              } 
              </View>
              {data.comboId
              ?
              <View>
                <Text style={[s.normalText]} numberOfLines={8}>Combo details: {data.comboDescription}</Text>
              </View> 
              : null
              }
            </View>
            
          </View>
      )
    }    
    //----

    removeUnAvailable = (data) => {
      console.warn('remove product-->', data);
      let i;
      if (data.comboId) {
        i = this.props.cartItem.cartItem.findIndex(ind => ind.comboId === data.comboId);
      } else {
        console.warn('in product');
        i = this.props.cartItem.cartItem.findIndex(ind => ind.productId === data.productId);
      }
      console.warn('index-->', i);
      this.props.cartItem.cartItem.splice(i, 1);
      this.replaceCart();
      // this.props.actions.replaceCart(this.props.cartItem.cartItem);
      // if(this.props.cartItem.cartItem.length == 0) {
      //   this.props.navigationAction.navigateAction('Home');
      //   this.props.navigation.navigate('homePage');
      // }
    }

    removeProduct = () => {

      this.setState({
        removeProduct: true,
        modalVisible: false
      });
      const i = this.props.cartItem.cartItem.findIndex(ind => ind.productId === this.state.selectedProduct.productId);
      
      this.props.cartState.cartCount -= 1;
      this.props.cartState.cartSum = parseFloat(this.props.cartState.cartSum) - parseFloat(this.props.cartItem.cartItem[i].unitCost); 
      
      this.props.cartItem.cartItem[i].counter = this.props.cartItem.cartItem[i].counter - 1;
      this.props.cartItem.cartItem[i].totalCost = this.props.cartItem.cartItem[i].unitCost * this.props.cartItem.cartItem[i].counter;

      this.props.cartItem.cartItem.splice(i,1);
      if(this.props.cartState.cartCount === 0){
        this.props.cartModalAction.changeCartState(false,this.props.cartState.cartSum,this.props.cartState.cartCount);
      }
      else{
        this.props.cartModalAction.changeCartState(true,this.props.cartState.cartSum,this.props.cartState.cartCount);
      }
      
      this.replaceCart();
    
    }

    hideModal = () => {
      this.setState({ 
        removeProduct: false,
        modalVisible: false,
        addressModal: false,
        modalVisibleOrder: false
      });
      if(this.props.userData.userData.addressDetails.length > 0){
        this.props.userData.userData.addressDetails.forEach(element => {
          if(element.isSelected == true){
            this.state.defaultAddress.push(element); 
            this.setState({
              defaultAddress: this.state.defaultAddress
            })
          }
  
        });  
      }
    }

    hideCouponModal = () => {
      this.setState({
        couponModal: false
      })
    }

    //function to increment the count of particular item
    decrementCount = (productData) => {
      this.setState({
        selectedProduct: productData
      });
      let id;
      if (productData.productId) {
        id = this.props.cartItem.cartItem.findIndex(ind => ind.productId === productData.productId);
      } else {
        id = this.props.cartItem.cartItem.findIndex(ind => ind.comboId === productData.comboId);
      }
      
      if(productData.counter - 1 <= 0){
        this.setState({
          modalVisible: true
        });
      }
      else {
        this.props.cartItem.cartItem[id].counter = this.props.cartItem.cartItem[id].counter - 1;
        this.props.cartItem.cartItem[id].totalCost = this.props.cartItem.cartItem[id].unitCost * this.props.cartItem.cartItem[id].counter;
        this.props.cartState.cartCount -= 1;
        this.props.cartState.cartSum = parseFloat(this.props.cartState.cartSum) - parseFloat(this.props.cartItem.cartItem[id].unitCost); 
        this.props.cartModalAction.changeCartState(true,this.props.cartState.cartSum,this.props.cartState.cartCount);
        this.replaceCart();
      }
      
    }

    replaceCart = () => {
      this.props.actions.replaceCart(this.props.cartItem.cartItem);
      let cartSum = 0;
      let cartSumExlGst = 0.00;
      let temp = [];
      let counter = 0;
      console.warn('this.props.cartItem.cartItem', this.props.cartItem.cartItem);
      this.props.cartItem.cartItem.forEach(element => {
        if (element.isProductAvailable) {
          console.warn('counter inside map---->', element.counter);
          counter = counter + element.counter;
          if (!temp.some(arr => arr.shopId == element.shopId)) {
            temp.push(element);
          }
          cartSum = parseFloat(cartSum) + parseFloat(element.totalCost);
          cartSumExlGst = parseFloat(cartSumExlGst) + (parseFloat(element.costExlGst) * element.counter);
        }
      });
      console.warn('this.props.userData.userData.deliveryCharge', this.props.userData.userData.deliveryCharge);
      console.warn('this.props.userData.userData.multiplier', this.props.userData.userData.multiplier);
      
      let deliveryCharge = 0.0;
      let payable;
      if (temp.length > 1) {
        deliveryCharge = parseFloat(this.props.userData.userData.deliveryCharge) + (((temp.length - 1) * 25) * parseFloat(this.props.userData.userData.multiplier));
      } else {
        deliveryCharge = parseFloat(this.props.userData.userData.deliveryCharge)
      }
      if (deliveryCharge > parseFloat(this.props.userData.userData.maxDeliveryCharge)) {
        deliveryCharge = parseFloat(this.props.userData.userData.maxDeliveryCharge);
      }
      if (this.props.walletBalance.canUse) {
        payable = (parseFloat(cartSum) + deliveryCharge) - parseFloat(this.props.walletBalance.walletBalance);
      } else {
        payable = parseFloat(cartSum) + deliveryCharge;
      }

      if (payable <= 0) {
        this.setState({
          totalPayable: 0.00,
          paidAmount: (parseFloat(cartSum) + deliveryCharge),
          walletAmountUsed: parseFloat(cartSum) + deliveryCharge
        })
  
      } else {
        this.setState({
          totalPayable: payable,
          paidAmount: (parseFloat(cartSum) + deliveryCharge),
          walletAmountUsed: parseFloat(this.props.walletBalance.walletBalance)
        })
      }
      
      this.setState({
        cartTotal: parseFloat(cartSum),
        deliveryCharges: deliveryCharge,
        gst: parseFloat(cartSum) - parseFloat(cartSumExlGst),
        cartTotalExlGst: parseFloat(cartSumExlGst)
      });
      console.warn('counterrrrrrrrrr', parseFloat(cartSum) - parseFloat(cartSumExlGst), parseFloat(cartSumExlGst) );
      this.props.cartModalAction.changeCartState(true, cartSum, counter);

      if(this.props.cartItem.cartItem.length == 0) {
        this.props.cartModalAction.changeCartState(false, 0.0, 0);
        this.props.navigationAction.navigateAction('Home');
        this.props.navigation.navigate('homePage');
      } else if (this.state.appliedCoupon != '--//--') {
        this.setState({
          appliedCoupon: '--//--',
          walletAmountUsed: 0.00,
          totalDiscount: 0.00,
        });
        // this.applyCoupon(this.state.appliedCoupon);
      }
    }

    //function to increment the count of particular item
    incrementCount = (productData) => {
      this.setState({
        selectedProduct: productData
      });
      let index;
      if (productData.productId) {
        index = this.props.cartItem.cartItem.findIndex(ind => ind.productId === productData.productId);
      } else {
        index = this.props.cartItem.cartItem.findIndex(ind => ind.comboId === productData.comboId);
      }
      this.props.cartState.cartCount += 1;
      this.props.cartState.cartSum = parseFloat(this.props.cartState.cartSum) + parseFloat(this.props.cartItem.cartItem[index].unitCost); 
      this.props.cartModalAction.changeCartState(true,this.props.cartState.cartSum,this.props.cartState.cartCount);
      this.props.cartItem.cartItem[index].counter = this.props.cartItem.cartItem[index].counter + 1;
      this.props.cartItem.cartItem[index].totalCost = this.props.cartItem.cartItem[index].unitCost * this.props.cartItem.cartItem[index].counter;
      this.replaceCart();
    }

    //function to increment the count of particular item
    changeAddress = () => {
      this.setState({
        addressModal: true
      });
    }

    editAddress = (addressId) => {
      this.setState({
          addressModal: false
      });
      this.props.navigation.navigate('addressPage',{"addressId": addressId, 'callingPage': 'orderDetailsPage'});
    }


    DeliveryAddress = () => {
      const {navigate} = this.props.navigation;
      if (this.props.userData.userData.addressDetails.length > 0) {
        return(
        <View style={{flex:4,backgroundColor:"white",paddingTop: 10, borderTopWidth:2,borderTopColor:"#7285A5"}}>
                
          <View style={{flex:8,paddingHorizontal:15, borderBottomWidth: 1, borderColor: "grey"}}>
            <View style={{flexDirection:"row"}}>
              <View style={{flex: 6}}>
                <Text style={s.headerText}>Deliver to</Text>
              </View>
              <TouchableOpacity style={{flex: 6,alignItems:"flex-end"}} onPress={() => this.changeAddress()}>
                <Text style={{color: "orange"}}>Change Address</Text>
              </TouchableOpacity>
            </View>
            <View style={{ paddingVertical: 5}}>    
              <View >
                  <Text style={{fontSize: 14,fontWeight:"bold",color:"#131E3wA"}}>Address </Text>           
              </View>
              <View>
                <Text style={s.normalText}>{this.props.userData.userData.addressDetails[0].flatNumber}</Text>
                <Text style={s.normalText}>{this.props.userData.userData.addressDetails[0].addressLine2}</Text>
              </View>   
            </View>
          </View>
          <View style={{paddingVertical: 10, paddingHorizontal:15}}>
            <Text style={{fontSize: 16,fontWeight: "bold",color:"#131E3A"}}>Total Payable: ₹{(this.state.totalPayable).toFixed(2)}</Text>
          </View>
          <View style={{flex:4,flexDirection:"row"}}>
          {this.state.disableCheckout
          ?
            <View style={{flex: 1, flexDirection:"row-reverse"}}>
              <TouchableOpacity style={{flex: 1,backgroundColor:"silver",alignItems:"center",justifyContent:"center"}}
                  onPress={() => Toast.show('The product from your cart are out of stock',Toast.LONG)}>
                <Text style={[s.headerText,{color:"white"}]}>Checkout</Text>
              </TouchableOpacity>
              <TouchableOpacity style={{flex: 1,backgroundColor:"black",alignItems:"center",justifyContent:"center"}}
                  onPress={() => Toast.show('The product from your cart are out of stock',Toast.LONG)}>
                <Text style={[s.headerText,{color:"white"}]}>COD</Text>
              </TouchableOpacity>
            </View>
            
          :
          <View style={{flex: 1, flexDirection:"row-reverse"}}>
            <TouchableOpacity style={{flex: 1,backgroundColor:"#00CCFF",alignItems:"center",justifyContent:"center"}}
                onPress={() => this.checkout('Completed')}>
              <Text style={[s.headerText,{color:"white"}]}>Checkout</Text>
            </TouchableOpacity>
            <TouchableOpacity style={{flex: 1,backgroundColor:"black",alignItems:"center",justifyContent:"center"}}
                onPress={() => this.checkout('COD')}>
              <Text style={[s.headerText,{color:"white"}]}>COD</Text>
            </TouchableOpacity>  
          </View>
            
          }
          
        </View>
      </View>
        )
      } else {
        return (
          <View style={{flex:2,backgroundColor:"white",paddingTop: 10,borderTopWidth:2,borderTopColor:"#7285A5"}}>
             
            <View style={{flex:0.5,paddingHorizontal:15,alignItems:"center"}}>
              <TouchableOpacity onPress={() => navigate('addressPage',{"addressId": "", 'callingPage': 'orderDetailsPage'})}>
                <Text style={[s.headerText,{textAlign:"center", color:"#00CCFF"}]}>Add address</Text>
              </TouchableOpacity>
              
            </View>
            <View style={{paddingVertical: 10, paddingHorizontal: 15}}>
              <Text style={{fontSize: 16,fontWeight: "bold",color:"#131E3A"}}>Total Payable: ₹{(this.state.totalPayable).toFixed(2)}</Text>
            </View>
            <View style={{flex:1,flexDirection:"row"}}>
            {/* <TouchableOpacity style={{flex:6,backgroundColor:"#F5F5F5",justifyContent:"center",alignItems:"center"}}
              onPress={() => this.placeBid()}>
              <Text style={{fontSize: 16,fontWeight: "bold",color:"#131E3A"}}>Place bid</Text>
            </TouchableOpacity> */}
            {this.state.disableCheckout
            ?
            <View style={{flex: 1, flexDirection:"row-reverse"}}>
              <TouchableOpacity style={{flex: 1,backgroundColor:"silver",alignItems:"center",justifyContent:"center"}}
                  onPress={() => Toast.show('The product from your cart are out of stock',Toast.LONG)}>
                <Text style={[s.headerText,{color:"white"}]}>Checkout</Text>
              </TouchableOpacity>
              <TouchableOpacity style={{flex: 1,backgroundColor:"black",alignItems:"center",justifyContent:"center"}}
                  onPress={() => Toast.show('The product from your cart are out of stock',Toast.LONG)}>
                <Text style={[s.headerText,{color:"white"}]}>COD</Text>
              </TouchableOpacity>
            </View>
            :
            <View style={{flex: 1, flexDirection:"row-reverse"}}>
              <TouchableOpacity style={{flex: 1,backgroundColor:"#00CCFF",alignItems:"center",justifyContent:"center"}}
                  onPress={() => this.checkout('Completed')}>
                <Text style={[s.headerText,{color:"white"}]}>Checkout</Text>
              </TouchableOpacity>
              <TouchableOpacity style={{flex: 1,backgroundColor:"black",alignItems:"center",justifyContent:"center"}}
                  onPress={() => this.checkout('COD')}>
                <Text style={[s.headerText,{color:"white"}]}>COD</Text>
              </TouchableOpacity>  
            </View>
            }
          </View>
          </View>
        )
      }
      
    }

    setDefaultAddress = () => {
      if(this.props.userData.userData.addressDetails.length > 0){
        // this.props.userData.userData.addressDetails.forEach(element => {
        //   if(element.isSelected == true){
        //     this.state.defaultAddress = [];
        //     this.state.defaultAddress.push(element); 
        //     this.setState({
        //       defaultAddress: this.state.defaultAddress,
        //       addressModal: false
        //     })
        //   }
  
        // });  
        const i = this.props.userData.userData.addressDetails.findIndex(ind => ind.isSelected === true);
        if (i == 1) {
          const temp = this.props.userData.userData.addressDetails[0];
          this.props.userData.userData.addressDetails[0] = this.props.userData.userData.addressDetails[1];
          this.props.userData.userData.addressDetails[1] = temp;
        }
        this.setState({
          addressModal: false
        })

      }
    }

    showCouponModal = async() => {
      const isLogin = await this.checkLogin();
      if (isLogin) {
        console.warn('this.sate.coupon list', this.state.couponList, this.props.availableCoupons);
        this.setState({
          couponModal: true
        });
      } else {
        Toast.show('Please login into application to get offer coupons', Toast.LONG); 
      }
    }

    applyCoupon = (couponCode) => {
      console.warn('couponCode-->', couponCode);
      if(couponCode == '' || couponCode == undefined) {
        Toast.show('Please select valid coupon from list',Toast.LONG);
      } else {
        const i = this.props.availableCoupons.couponList.findIndex(ind => ind.coupon === couponCode);
        if (i < 0) {
          Toast.show('Coupon code is invalid',Toast.LONG);
        } else {
            if (parseFloat(this.state.cartTotal) > parseFloat(this.props.availableCoupons.couponList[i].minLimit)) {
              console.warn('couponCode-->', this.props.availableCoupons.couponList);
              console.warn('index---', i);
              const percentage = parseFloat(this.props.availableCoupons.couponList[i].percentage);
              console.warn('percentage', percentage);
              console.warn('totalCost, totalPay, deliveryCharges', this.state.cartTotal, this.state.totalPayable, this.state.deliveryCharges);
              let discount = (parseFloat(this.state.cartTotal) * (percentage/100));
              if(discount > parseFloat(this.props.availableCoupons.couponList[i].maxDiscount)) {
                discount = parseFloat(this.props.availableCoupons.couponList[i].maxDiscount)
              }
    
              let payable;
              if (this.props.walletBalance.canUse) {
                payable = (parseFloat(this.state.cartTotal) - discount) + parseFloat(this.state.deliveryCharges) - parseFloat(this.props.walletBalance.walletBalance);
              } else {
                payable = (parseFloat(this.state.cartTotal) - discount) + parseFloat(this.state.deliveryCharges);
              }
    
              if(payable <= 0) {
                this.setState({
                  totalPayable: 0.00,
                  paidAmount: ((parseFloat(this.state.cartTotal) - discount) + parseFloat(this.state.deliveryCharges)),
                  walletAmountUsed: (parseFloat(this.state.cartTotal) - discount) + parseFloat(this.state.deliveryCharges)
                })
              } else {
                this.setState({
                  totalPayable: payable,
                  paidAmount: ((parseFloat(this.state.cartTotal) - discount) + parseFloat(this.state.deliveryCharges)),
                  walletAmountUsed: parseFloat(this.props.walletBalance.walletBalance)
                })
              }
              
              this.setState({
                appliedCoupon: couponCode,
                couponModal: false,
                deliveryCharges: this.state.deliveryCharges,
                totalDiscount: discount,
              });
              Toast.show('Coupon applied successfully. You saved total Rs ' + discount + ' on your cart amount',Toast.LONG);
        
            
            } else {
              Toast.show('Coupon is valid only if cart value is more than Rs ' + this.props.availableCoupons.couponList[i].minLimit,Toast.LONG); 
            }
          }
         
      }
     
    }

    placeBid = () => {
      if (this.props.userData.userData.addressDetails.length > 0) {
        const tempObj = {
          totalCost: this.state.cartTotal,
          itemList: this.props.cartItem.cartItem,
          deliveryAddress: this.state.defaultAddress,
        }
        
        this.setState ({
          showLoader: true 
        })
        axios.post( 
        Config.URL + Config.placedBid,
        {
          "userId": this.props.userData.userData.userId,
          "bidInfo": tempObj
        },
        { 
          headers: {
              'Content-Type': 'application/json',
          }
        }
        )
        .then(res => {
          if(res.data.status == 0) {
            this.setState ({
              showLoader: false
            })
            this.props.cartItem.cartItem = [];
            this.props.actions.placedbBid(tempObj);
            this.props.actions.replaceCart(this.props.cartItem.cartItem);
            this.props.cartState.totalPlacedBids = 0;
            this.props.cartState.cartSum = 0; 
            this.props.cartModalAction.changeCartState(false,this.props.cartState.cartSum,this.props.cartState.cartCount);  
            this.props.totalPlacedBids = 1; 
            this.props.cartModalAction.changeBidState(true,this.props.totalPlacedBids);      
            Toast.show('Bid Placed successfully',Toast.LONG);
          } else {
            this.setState ({
              showLoader: false
            })
            Toast.show('Error while submiting the bid. Please try again',Toast.LONG);
          }
          
        });  
      } else {
        Toast.show('Please add delivery address',Toast.LONG);
      }
      
    }

    checkLogin = async () => {
      console.warn('this.props.userData.userData -->', this.props.userData.userData);
      if (Object.keys(this.props.userData.userData).length > 0) { 
        if (this.props.userData.userData.userDetails.length > 0) {
          return true;
        }
        return false;
      }
      return false;
    }

    checkout = async(paymentType) => {
      const isLogin = await this.checkLogin();
      if (isLogin) {
        await this.setState({ paymentType: paymentType });
        if (this.props.userData.userData.addressDetails.length > 0) {
          if (this.props.userData.userData.userDetails[0].contactNumber) {
            let coupon;
            if (this.state.appliedCoupon == '--//--') {
              await this.setState({appliedCoupon: ''})
              coupon = '';
            }
            this.VerifyOrderFromBackend();
          } else {
            Toast.show('Please add mobile number',Toast.LONG);  
            this.props.navigation.navigate('personalInfoPage', {callingPage: "orderDetails"});
          }
        } else {
          Toast.show('Please add delivery address',Toast.LONG);
        }  
      } else {
        this.props.navigation.navigate('login', {didSkipLogin: true, callingPage: "orderDetailsPage"});
      }
    }

    VerifyOrderFromBackend = async() => {
      console.warn('called');
      try {
        const orderObj = await this.getOrderObject(false);
        // const order = {
        //   "userId": this.props.userData.userData.userId,
        //   "order": orderObj,
        // }
        const reqBody = {
          order: orderObj
        }
        await this.setState({ showLoader: true });
        axios.post( 
          Config.URL + Config.verifyOrder,
          {
            order: orderObj 
          },
          { 
            headers: {
                'Content-Type': 'application/json',
            }
          }
          )
          .then(res => {
            console.warn('response of verify order', res.data);
            this.setState({ showLoader: false });
            if (res.data.status == 0) {
              // Toast.show(res.data.message, Toast.LONG);
              this.confirmForOrdercheckout();
            } else if (res.data.status == 1) {
              console.warn('here 111')
              this.setState ({ 
                confirmationMessage: res.data.message,
                modalVisibleOrder: true
              });
            } else {
              Toast.show(res.data.message, Toast.LONG);
            }
            
          });
      } catch (err) {
        this.setState({ showLoader: false });

      }
      
      
    }
    
    confirmForOrdercheckout = () => {
      console.warn('confirmForOrdercheckout --->');
      this.setState({ modalVisibleOrder: false })
      if (this.state.totalPayable <= 0) {
        this.placedOrdered();
      } else if (this.state.paymentType == 'COD') {
        console.warn('inside COD check');
        this.placedOrdered();
      }else {
        // this.initiatePaymentIOS();
        this.saveOrderToDB();
      }
    }

    saveOrderToDB = async () => {
      console.warn('inside saveOrderToDB -----');
      this.setState({showLoader: true});
      const orderObj = await this.getOrderObject(false);
      const order = {
        "userId": this.props.userData.userData.userId,
        "order": orderObj,
      }
      const reqBody = {
        order: order,
      }
      const placeOrder = await callAPI.axiosCall(Config.saveOrderActivityDetails, reqBody);
      
      this.setState({showLoader: false});
      if (placeOrder) {
        console.warn('placeOrder res--->', placeOrder);
        this.initiatePaymentIOS(placeOrder.data.orderId);
      } else {
        setTimeout(() => {
          Toast.show('Failed to open payment gateway. Please try again later', Toast.LONG);
        }, 100);
      }
    }

    initiatePaymentIOS = async(orderId) => {
      this.setState({showLoader: false});
      console.warn('initiate payment IOS');
      // const orderObj = await this.getOrderObject(false);
      // const order = {
      //   "userId": this.props.userData.userData.userId,
      //   "order": orderObj,
      // }
      // await this.setState({
      //   paytmUrl: this.state.paytmUrl + 'orderId=' + orderId + '&userId=' + this.props.userData.userData.userId
      //   + '&totalAmount=' + parseFloat(this.state.totalPayable) + '&activityType=' + '&amountUsedFromWallet=' + this.state.walletAmountUsed
      //   + '&bidId='
      // });
      const paytmUrl = this.state.paytmUrl + 'orderId=' + orderId + '&userId=' + this.props.userData.userData.userId
        + '&totalAmount=' + parseFloat(this.state.totalPayable) + '&activityType=' + '&amountUsedFromWallet=' + this.state.walletAmountUsed
        + '&bidId=';
      console.warn('called---->', this.state.paytmUrl);
      this.props.navigationAction.navigateAction('PaytmGateway');
      this.props.navigation.navigate('paytmViewPage', {"paytmUrl": paytmUrl, "callingPage": 'order'});
    }

    // initiatePaymentAndroid = async() => {
    //   this.setState({showLoader: true});
    //   var reqBody = {
    //     "userId": this.props.userData.userData.userId,
    //     "totalAmount": parseFloat(this.state.totalPayable) 
    //   }
    //   const paymentDetails = await PaytmIntegration.initiatePaymentAndroid(reqBody);
    //   if (paymentDetails) {
    //     try {
    //       // Paytm.startPayment(details);  // un-comment for android
    //     } catch(err) {
    //       Toast.show('Sorry, Not able to process for payment. PLease try again',Toast.LONG);
    //     }
    //   } else {
    //     Toast.show('Sorry, Not able to process for payment. PLease try again',Toast.LONG);
    //   }
    // }

    // placedOrdered = async(paymentStatus, paytmObj) => {
    //   this.setState ({showLoader: true});

    //   console.warn(' this.state.walletAmountUsed',  this.state.walletAmountUsed);
    //   if (this.state.checkMashObj.ORDER_ID) {
    //     console.warn('this.props.userData.userData.addressDetails', this.props.userData.userData.addressDetails);
    //     let temp = [];
    //     temp.push(this.props.userData.userData.addressDetails[0]);
    //     const orderObj = await this.getOrderObject(true);

    //     const apiObj = {
    //       "orderId": this.state.checkMashObj.ORDER_ID,
    //       "userId": this.props.userData.userData.userId,
    //       "order": orderObj,
    //       "paymentStatus" : paymentStatus,
    //       "amountUsedFromWallet": this.state.walletAmountUsed,
    //       "MID": this.state.checkMashObj.MID,
    //       "CHECKSUMHASH": this.state.checkMashObj.CHECKSUMHASH
    //     }
    //     this.placeOrderAPI(apiObj);
    //   } else if (this.state.totalPayable <= 0) {
    //     const orderObj = await this.getOrderObject(false);
    //     const apiObj = {
    //       "userId": this.props.userData.userData.userId,
    //       "order": orderObj,
    //       "paymentStatus" : paymentStatus,
    //       "amountUsedFromWallet": this.state.walletAmountUsed,
    //     }
    //     this.placeOrderAPI(apiObj);
    //   }
      
    // }

    placedOrdered = async(paytmObj) => {
      this.setState ({showLoader: true});
      console.warn(' this.state.walletAmountUsed',  this.state.walletAmountUsed);
      if (this.state.totalPayable <= 0) {
        const orderObj = await this.getOrderObject(false);
        const apiObj = {
          "userId": this.props.userData.userData.userId,
          "order": orderObj,
          "paymentStatus" : this.state.paymentType,
          "amountUsedFromWallet": this.state.walletAmountUsed,
          "isNewVersion": true
        }
        this.placeOrderAPI(apiObj);
      } else if (this.state.paymentType == 'COD'){
        const orderObj = await this.getOrderObject(false);
        const apiObj = {
          "userId": this.props.userData.userData.userId,
          "order": orderObj,
          "paymentStatus" : this.state.paymentType,
          "amountUsedFromWallet": this.state.walletAmountUsed,
          "isNewVersion": true
        }
        this.placeOrderAPI(apiObj);
      }
      
    }

    getOrderObject = async(hasOrderId) => {
      let orderData;
      let temp = [];
      temp.push(this.props.userData.userData.addressDetails[0]);
      if (hasOrderId) {
        orderData = {
          orderId: this.state.checkMashObj.ORDER_ID,
          coupon: this.state.appliedCoupon,
          totalDiscount: this.state.totalDiscount,
          deliveryCharge: this.state.deliveryCharges,
          totalCost: this.state.cartTotal,
          totalCostExclTax: parseFloat(this.state.cartTotalExlGst).toFixed(2),
          appliedGst: parseFloat(this.state.gst).toFixed(2), 
          toPay: this.state.paidAmount,
          itemList: this.props.cartItem.cartItem,
          deliveryAddress: temp,
          paytmResponse: paytmObj,
          userDetails: {
            fullName: this.props.userData.userData.userDetails[0].fullName,
            contactNumber: this.props.userData.userData.userDetails[0].contactNumber 
          }
        } 
      } else {
        orderData = {
          coupon: this.state.appliedCoupon,
          totalDiscount: this.state.totalDiscount,
          deliveryCharge: this.state.deliveryCharges,
          totalCost: this.state.cartTotal,
          totalCostExclTax: parseFloat((this.state.cartTotalExlGst).toFixed(2)),
          appliedGst: parseFloat((this.state.gst).toFixed(2)), 
          toPay: this.state.paidAmount,
          itemList: this.props.cartItem.cartItem,
          deliveryAddress: temp,
          userDetails: {
            fullName: this.props.userData.userData.userDetails[0].fullName,
            contactNumber: this.props.userData.userData.userDetails[0].contactNumber 
          }
        }
      }
      return orderData;
    }

    placeOrderAPI = async(apiObj) => {
      console.warn('placeOrderAPI called');
      try {
        const placeOrder = await callAPI.axiosCall(Config.placedOrder, apiObj);
        console.warn('place order response', placeOrder);
        if (placeOrder) {
          if(placeOrder.data.status == 0) {
            Toast.show(placeOrder.data.message, Toast.LONG);
            this.postPayment(placeOrder);
          } else {
            Toast.show(placeOrder.data.message,Toast.LONG);
          }
        } else {
          await this.setState({showLoader: false});
          Toast.show('Selected shop for order is far away from your delivery location. Please shop from local shops near you', Toast.LONG);
        }
      } catch (err) {
        console.warn('error in cod place', err);
        this.setState({showLoader: false});
      }
      
    }

    WalletAmount = () => {
      if(this.props.walletBalance.canUse) {
        return(
          <View style={{flex: 1, flexDirection: "row",paddingVertical: 3}}>
            <View style={{flex: 8}}>
              <Text style={[s.normalText]} >Flash Wallet </Text>
            </View>
            <View style={{flex:4,alignItems:"flex-end"}}>
              <Text style={[s.normalText]} >₹{this.props.walletBalance.walletBalance} </Text>
            </View>
          </View>
        )
      } else {
        return null;
      }
    }

    // paytmResponse = (status, message) => {
    //   console.warn('response from paytm gateway IOS', status);
    //   Toast.show(message, Toast.LONG);
    //   this.setState({showCheckOut: false});
    //   if (status) {
    //     let index = status.indexOf("-");
    //     if (index >= 0) {
    //       console('wallet amount--->', index, status.splice(index, string.length - 1));
    //       this.props.walletAction.updateWallet(status.splice(index, string.length - 1), true);
    //     } 
    //     this.setState({showCheckOut: false});
    //     this.props.navigationAction.navigateAction('Home');
    //     this.props.navigation.navigate('homePage');
    //     // this.postPayment();
    //   } else {
    //     Toast.show(message, Toast.LONG);
    //   }
    // }

    postPayment = (res) => {
      this.setState({showLoader: false});
      this.props.cartItem.cartItem = [];
      // this.props.actions.placedCart(tempObj);
      this.props.actions.replaceCart(this.props.cartItem.cartItem);
      this.props.cartState.cartCount = 0;
      this.props.cartState.cartSum = 0; 
      // this.props.walletAction.updateWallet(0, true);
      this.props.cartModalAction.changeOrderState(true); 
      this.props.cartModalAction.changeCartState(false,this.props.cartState.cartSum,this.props.cartState.cartCount);  
      if (res) {
        this.props.walletAction.updateWallet(res.data.wallet, true);
      } 
      this.props.navigationAction.navigateAction('Home');
      this.props.navigation.navigate('homePage');
    }


    render() {
        const {navigate} = this.props.navigation;
      //   if (this.state.trackOrder) {
      //     return (
      //         <View style={[s.body]}>
      //           {/* <InternateModal /> */}
      //           <AnimatedLoader
      //             visible={this.state.showLoader}
      //             overlayColor="rgba(255,255,255,0.75)"
      //             source={require("../../../assets/loader.json")}
      //             animationStyle= {[styles.lottie,{height: 150,width: 150}]}
      //             speed={1}
      //           />
      //             <View style={{flex: 1,alignItems:"center",marginHorizontal: 20,marginTop: 50}}>
      //                 <TouchableOpacity style={styles.addressButtonView} onPress={() => navigate('trackOrderPage',{action: 'order'})}>
      //                 <   Text style={styles.addressButtonText}>Track Order</Text>   
      //                 </TouchableOpacity>
      //             </View>
      //         </View>
      //     )
      // } else {
        if(this.props.cartItem.cartItem.length == 0) {
          return (
            <SafeAreaView style={{flex: 1}}>
              <View style={[s.bodyGray]}>
              <AnimatedLoader
                visible={this.state.showLoader}
                overlayColor="rgba(255,255,255,0.75)"
                source={require("../../../assets/loader.json")}
                animationStyle= {[styles.lottie,{height: 150,width: 150}]}
                speed={1}
              />
                {/* <InternateModal /> */}
                <View style={[{flex:1, paddingHorizontal: 10, alignItems:"center"}]}> 
                  {/* <View style={{flex:1,paddingVertical: 20}}>
                    <Text style={s.headerText}> Shooping Bag is Empty</Text>
                  </View>
                  <View style={{flex:1,paddingVertical: 20}}>
                    <Text style={s.normalText}> There is nothing in your bag go & grab the offers</Text>
                  </View> */}
                    <Image style={{width:ScreenWidth-30,height:ScreenHeight - 80}} source={require('../../../assets/emptyCartPage.jpeg')}/>
                  
                  
                </View>
                {/* <NavigationBar navigate={navigate}></NavigationBar> */}
              </View>
              
            </SafeAreaView>
            
          )
        } else {
          return (
            <SafeAreaView style={{flex: 1}}>
              <View style={[s.bodyGray]}>
              <AnimatedLoader
                visible={this.state.showLoader}
                overlayColor="rgba(255,255,255,0.75)"
                source={require("../../../assets/loader.json")}
                animationStyle= {[styles.lottie,{height: 150,width: 150}]}
                speed={1}
              />
                {/* <InternateModal /> */}
              <PopupModal visible={this.state.modalVisible} onDismiss={this.hideModal} removeProduct={this.removeProduct} 
                message={"Are you sure want to remove the product from cart?"} btnTag={"Remove"}/>
              <AddressModal visible={this.state.addressModal} onDismiss={this.hideModal} changeAddress={this.setDefaultAddress} 
                navigation = {navigate} editAddress={this.editAddress} shopList={this.state.availableShopList}/>
              <CouponModal visible={this.state.couponModal} avalableCoupons= {this.state.couponList} onDismiss={this.hideCouponModal} navigation = {navigate} applyCoupon={this.applyCoupon} />
              <PopupModal visible={this.state.modalVisibleOrder} onDismiss={this.hideModal} removeProduct={this.confirmForOrdercheckout} 
                  message={this.state.confirmationMessage} 
                  btnTag={"Proceed"}/>
    
                {/* <View style={{flex:1,backgroundColor:"#F5FCFF"}}>
                  <View style={{marginHorizontal:15}}>
                    <Text style={[s.headerText,styles.header]}>My Orders </Text>
                  </View>
    
                </View> */}
                <View style={{flex:8}}>
                  <ScrollView showsVerticalScrollIndicator={false}>
                    <View style={{backgroundColor:"white",elevation: 6}}>
                      <FlatList
                        data={this.props.cartItem.cartItem}
                        renderItem={( { item } ) => (
                          <this.CartListing
                            data={item}
                          />
                          
                        )}
                        extraData={this.props} 
                      />
                    </View>
                    <View style={{backgroundColor:"white",marginTop:15,paddingVertical: 15,paddingHorizontal:15, elevation: 6}}>
                      <TouchableOpacity style={{flex: 1, flexDirection: "row"}} onPress={() => this.showCouponModal()}> 
                        <View style={{flex: 2,alignItems:"flex-start",justifyContent:"center",alignContent:"center"}}>
                          <Image style={{height: 30,width: 30}} source={require('../../../assets/offericon.png')}/>
                        </View>
                        <View style={{flex: 8,alignItems:"flex-start",justifyContent:"center",alignContent:"center"}}>
                          <Text style={s.headerText}>Apply Coupon </Text>
                        </View>
                        <View style={{flex: 2,alignItems:"center",justifyContent:"center",alignContent:"center"}}>
                          <Text style={s.headerText}> > </Text>
                        </View>
                      </TouchableOpacity>
                    </View>
                    <View style={{backgroundColor:"white",marginTop:15,paddingVertical: 15,paddingHorizontal:15, elevation: 6}}>
                      <View>
                        <Text style={s.headerText}>Bill Details </Text>
                      </View>
                      <View style={{paddingTop:10}}>
                        <View style={{flex: 1, flexDirection: "row",paddingVertical: 3}}>
                          <View style={{flex: 8}}>
                            <Text style={[s.normalText]} >Item Total</Text>
                          </View>
                          <View style={{flex:4,alignItems:"flex-end"}}>
                            <Text style={[s.normalText]} >₹{(this.state.cartTotal).toFixed(2)} </Text>
                          </View>
                        </View>
                        <View style={{flex: 1, flexDirection: "row",paddingVertical: 3}}>
                          <View style={{flex: 8}}>
                            <Text style={[s.normalText]} >Delivery Fee</Text>
                          </View>
                          <View style={{flex:4,alignItems:"flex-end"}}>
                            <Text style={[s.normalText]} >₹{(this.state.deliveryCharges).toFixed(2)}</Text>
                          </View>
                        </View>
                        <View style={{paddingTop:10,borderBottomWidth: 1, borderBottomColor:"#7285A5"}}/>
                          <View style={{flex: 1, flexDirection: "row",paddingVertical: 3}}>
                            <View style={{flex: 8}}>
                              <Text style={[s.normalText]} >Total cost</Text>
                            </View>
                            <View style={{flex:4,alignItems:"flex-end"}}>
                              <Text style={[s.normalText]} >₹{(this.state.cartTotalExlGst).toFixed(2)} </Text>
                            </View>
                          </View>
                          <View style={{flex: 1, flexDirection: "row",paddingVertical: 3}}>
                            <View style={{flex: 8}}>
                              <Text style={[s.normalText]} >Total GST applicable </Text>
                            </View>
                            <View style={{flex:4,alignItems:"flex-end"}}>
                              <Text style={[s.normalText]} >₹{(this.state.gst).toFixed(2)} </Text>
                            </View>
                          </View>
                          <View style={{flex: 1, flexDirection: "row",paddingVertical: 3}}>
                            <View style={{flex: 8}}>
                              <Text style={[s.normalText]} >Coupon </Text>
                            </View>
                            <View style={{flex:4,alignItems:"flex-end"}}>
                              <Text style={[s.normalText]} >{this.state.appliedCoupon} </Text>
                            </View>
                          </View>
                          <View style={{flex: 1, flexDirection: "row",paddingVertical: 3}}>
                            <View style={{flex: 8}}>
                              <Text style={[s.normalText]} >Total discount </Text>
                            </View>
                            <View style={{flex:4,alignItems:"flex-end"}}>
                              <Text style={[s.normalText]} >₹{(this.state.totalDiscount).toFixed(2)} </Text>
                            </View>
                          </View>
                          
                          <this.WalletAmount />
                      </View>
                      <View style={{paddingTop:10,flexDirection: "row",flex:1}}>
                        <View style={{flex: 8}}>
                          <Text style={[s.normalText]} >To Pay</Text>
                        </View>
                        <View style={{flex:4,alignItems:"flex-end"}}>
                          <Text style={[s.normalText]} >₹{(this.state.totalPayable).toFixed(2)} </Text>
                        </View>
                      </View>
                    </View>
                  </ScrollView>
    
                </View>
                <this.DeliveryAddress />
                
                {/* <View style={{flex:4,backgroundColor:"white",paddingTop: 10,borderTopWidth:2,borderTopColor:"#7285A5"}}>
                  
                  <View style={{flex:4,flexDirection:"row"}}>
                    <View style={{flex:6,backgroundColor:"#F5F5F5",paddingLeft:15,justifyContent:"center"}}>
                      <Text style={{fontSize: 16,fontWeight: "bold",color:"#131E3A"}}>Total Payable</Text>
                      <Text style={s.normalText}>₹{this.state.cartTotal}</Text>
                    </View>
                    <TouchableOpacity style={{flex:6,backgroundColor:"#00CCFF",alignItems:"center",justifyContent:"center"}}
                      onPress={() => this.checkout()}>
                      <Text style={[s.headerText,{color:"white"}]}>Checkout</Text>
                    </TouchableOpacity>
                  </View>
                </View> */}
                {/* <NavigationBar navigate={navigate}></NavigationBar>   */}
                
              </View>
              
              
            </SafeAreaView>
            
            
          );
        
    }
  }
     
};

let ScreenHeight = Dimensions.get("window").height;
let ScreenWidth = Dimensions.get("window").width;

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: "#F5FCFF",
    height: ScreenHeight
  },
  body: {
    backgroundColor: '#F5FCFF',
  },
  prodcutListingView: {
    flexDirection: 'row',
    marginVertical: 5,
    backgroundColor: "white",
    padding: 10

  },
  addCardImage: {
    width: 80,
    height: 80,
    flex: 4
  },
  productImage: {
    width: 80,
    height: 80
  },
  stores: {
    paddingHorizontal: 10,
    flex: 8,
    flexDirection: "column"
  },
  addressButtonView: {
		backgroundColor:"#00CCFF",  
		width: '80%',
		//borderRadius: 50,
		elevation: 6,
		shadowColor: 'rgba(0, 0, 0, 0.1)',
		shadowOpacity: 0.8,
		height: 40,
		justifyContent: 'center',
		alignItems: 'center',
	},

	addressButtonText: {
		color:"white",  
		fontSize: 16 ,
		fontWeight:"bold"
	},
  Mystores: {
    flexDirection: "row",
    alignItems: "flex-start",
    justifyContent: "space-between",
    textAlignVertical: "bottom",
    flex: 2
    // paddingBottom:15,
  },
  flexRow: {
    flex: 5,
    textAlignVertical: "bottom",
    flexDirection: 'row',
  },
  flexRowAdd: {
    paddingTop: 10,
    textAlignVertical: "bottom",
    flexDirection: 'row',
  },
  basketBtn:{
    paddingVertical: 5,
    paddingHorizontal: 10,
    marginBottom: 10,
    borderRadius: 20,
    height: 30,
    width: 30
  },
  basketImg: {
    height: 30,
    width: 30,
  },
  storeBtn: {
    paddingVertical: 5,
    paddingHorizontal: 5,
    marginBottom: 5,
    color: 'white',
    borderRadius: 0,
    width: 50
  },
  addtoCartBtnColor: {
    fontSize:12,
    borderWidth: 1,
    borderColor: '#0080FE',
    marginLeft: 20,
  },
  header: {
    fontSize: 24,
    paddingVertical: 5
  },
  orderDtlContainer:{
    borderRadius:0,
  },
  orderHeading:{
    paddingLeft: 10,
    paddingVertical:5,
    marginTop:15
  },
  orders:{
    paddingLeft:10,
  },
  orderTitles:{
    paddingLeft: 10,
    paddingVertical:15,
  },
  orderCount:{
    fontSize:18,
    fontWeight:'bold',
    paddingHorizontal:10,
    color: '#131E3A'
},
orderProductName:{
  fontWeight: 'bold',
  fontSize:16
},
  orderPriceText:{
    color:'#3cb371',
    fontWeight:'bold'
  },
  orderProductHeading:{
    fontWeight: 'bold',
    fontSize:30,
  },
  
  orderCardImage:{
    backgroundColor:'hotpink',
    width:50,
    height:50,
    borderRadius:50
},
orderCardButton:{
  borderColor:'#0080FE',
  borderWidth: 1,
  width:25,
  height:25,
  borderRadius:50,
  alignItems:"center",
  justifyContent:'center',
},
  orderList:{
    flex: 1,
    flexDirection: "column",
    padding:20,
    paddingLeft: 10,
  },
  orderBorder:{    
    borderTopColor: 'grey',
    borderTopWidth: 1,
  },
flexColumnAlignEnd:{
  display:'flex',
  flexDirection:'column',
  alignItems:'flex-end'
},
priceAlign:{
  paddingRight:5,
  paddingTop: 5,
},
orderDtlCenter:{
  display:"flex",
  flexDirection:"row",
  justifyContent:"center"
},
  MyOrders:{
    flexDirection:"row",
    alignItems:"center",
    justifyContent:"space-between",
    // paddingBottom:15,
  },
  orderCardSpace:{
    marginBottom: 15,
  },
  orderCardPadding:{
    paddingBottom: 15,
  },
  orderMainView: {
    marginHorizontal: 20,
    // flex: 3,
    backgroundColor: '#EBECF0',
  },
  headerstyle: {
    paddingBottom: 20,
    paddingTop: 20
  },

  orderBtnViewColor: {
    backgroundColor: '#ba55d3',
    color:'white',
  },
  continueBtnViewColor: {
    backgroundColor: '#2f4f4f',
    color:'white',
  },
  checkoutBtnViewColor: {
    backgroundColor: '#20b2aa',
    color:'white',
    marginLeft:20,
  },
  orderBtn: {
    paddingVertical:10,
    paddingHorizontal:20,
    marginBottom:10,
    color:'white',
    borderRadius: 20,
  },

});



//--------------------------------------------------- store related fuction  -- Start

const mapStateToProps = (state) => {
	return {
    cartItem: state.cartItem,
    userData: state.loginData,
    cartState: state.cartState,
    availableCoupons: state.availableCoupons,
    walletBalance: state.walletBalance
	}
}

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(addToCart, dispatch),
        loginActions: bindActionCreators(loginSuccess,dispatch),
        cartModalAction: bindActionCreators(cartModalAction,dispatch),
        couponAction: bindActionCreators(couponAction,dispatch),
        navigationAction: bindActionCreators(navigateAction,dispatch),
        walletAction: bindActionCreators(walletAction,dispatch),
    };
}

// ---------------------------------------------------- store related function  -- END


export default connect(mapStateToProps, mapDispatchToProps)(OrderDetailsPage);