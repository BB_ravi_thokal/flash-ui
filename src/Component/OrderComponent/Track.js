/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
// import 'bootstrap/dist/css/bootstrap.min.css';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  Dimensions,
  Image,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import * as Config from '../../Constant/Config.js';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import AnimatedLoader from "react-native-animated-loader";
import  s  from '../../sharedStyle.js';
import * as navigateAction  from '../../Actions/NavigationBarAction';
import axios from 'axios'; 

SendActivityListViewTrack = ({item, self}) => {
    const {navigate} = self.props.navigation;
    return (
      <TouchableOpacity style={[styles.activeListingView]} onPress={() => navigate('trackOrderPage',{action: 'sendActivity', orderId: item.orderId})}>
          <View>
              <Text style={[[s.subHeaderText, {marginBottom: 4}], {marginBottom: 4}]} numberOfLines={1}>Activity Tag: {item.order.activityDetails[0].activityTag}</Text>
          </View>
          <View>
              <Text style={s.normalText} numberOfLines={1}>Activity status: {item.orderStatus}</Text>
          </View>
      </TouchableOpacity>
      
    )
}

GetActivityListViewTrack = ({item, self}) => {
    const {navigate} = self.props.navigation;
    return (
      <TouchableOpacity style={[styles.activeListingView]} onPress={() => navigate('trackOrderPage',{action: 'getActivity', orderId: item.orderId})}>
          <View>
              <Text style={[s.subHeaderText, {marginBottom: 4}]} numberOfLines={1}>Activity Tag: {item.order.activityDetails[0].activityTag}</Text>
          </View>
          <View>
              <Text style={s.normalText} numberOfLines={1}>Activity status: {item.orderStatus}</Text>
          </View>
      </TouchableOpacity>
    )
}

OrderListViewTrack = ({item, self}) => {
    const {navigate} = self.props.navigation;
    return (
      <TouchableOpacity style={[styles.activeListingView]} onPress={() => navigate('trackOrderPage',{action: 'order', orderId: item.orderId})}>
          <View style={{flexDirection:"row"}}>
              <Text style={[s.subHeaderText, {marginBottom: 4}]} numberOfLines={1}>Order Id:</Text>
              <Text style={[s.normalText, {marginLeft: 5, paddingTop: 1}]} numberOfLines={2}>{item.orderId}</Text>
          </View>
          <View style={{paddingTop: 10}}>
              <Text style={[s.normalText, {marginLeft: 5}]}>Total amount: ₹ {(item.order.toPay).toFixed(2)}</Text>   
            </View>
      </TouchableOpacity>
      
    )
}

class TrackingListPage extends React.Component {
        constructor(props) {
            super(props);
            this.state = {
                sendActivity: false,
                getActivity: false,
                isOrder: false, 
                listView: false,
                orderData: [],
                getActivityData: [],
                SendActivityData: [],
                showLoader: false,
            };

        }

        componentDidMount() {
            
            console.warn('am here in list page');
            if (this.props.navigation.state.params) {
            
                if(this.props.navigation.state.params.action == 'sendActivity') {
                    this.setState({
                        sendActivity: true
                    })
                    this.callAPI(true, 'sendActivity');
                } else if (this.props.navigation.state.params.action == 'getActivity') {
                    this.setState({
                        getActivity: true
                    })
                    this.callAPI(true, 'getActivity');
                } else if (this.props.navigation.state.params.action == 'order') {
                    this.setState({
                        isOrder: true
                    })
                    this.callAPI(false, 'order');
                }
            }
        }

        callAPI = (isActivity, activityType) => {
            console.warn('call API called', activityType);
            axios.post(
              Config.URL + Config.getOrderDetailsUser,
              {
                "userId": this.props.userData.userData.userId,
                "activity": isActivity
              },
              {
                headers: {
                    'Content-Type': 'application/json',
                }
              }
            )
            .then(res => {
                // console.warn('---- res from send -->', res.data.result.currentSendActivityOrderDetails);
                // console.warn('---- res from  get -->', res.data.result.currentGetActivityOrderDetails);
                console.warn('---- res from -->', res.data);
                if (res.data.status == 0) {
                    this.setState({
                        showLoader: false
                    });
                    if (activityType == 'sendActivity') {
                        this.setState({
                            sendActivityData: res.data.result.currentSendActivityOrderDetails,
                            listView: true,
                            detailsView: false,
                            sendActivity: true,
                            showLoader: false
                        });
                    } else if (activityType == 'getActivity') {
                        this.setState({
                            getActivityData: res.data.result.currentGetActivityOrderDetails,
                            listView: true,
                            detailsView: false,
                            getActivity: true,
                            showLoader: false
                        })
                    } else {
                        this.setState({
                            orderData: res.data.result.orderDetails,
                            listView: true,
                            detailsView: false,
                            isOrder: true,
                            showLoader: false
                        });
                    }    
                } else {
                    Toast.show('Failed to fetch the order status. Please try again',Toast.LONG);
                    console.warn('error',res)
                    this.setState({
                        showLoader: false
                    });
                }
        
            })
        }

        OrderDetails = () => {
            if(this.state.sendActivity) {
                if(this.state.listView) {
                    return (
                    <ScrollView style={{flex: 1}} showsVerticalScrollIndicator={false}> 
                        <View style={styles.mapContainer}>
                            <Image style={styles.mapImage} source={require('../../../assets/map.jpg')}/>
                        </View>
                        <View>
                            <FlatList
                                data={this.state.sendActivityData}
                                renderItem={( { item } ) => (
                                <SendActivityListViewTrack
                                    item={item}
                                    self={this}
                                />
                                )}
                                extraData={this.state.sendActivityData} 
                            />
                        </View>
                    </ScrollView>
                    )
                } else {
                    return null;
                }
            } else if (this.state.getActivity) {
                if (this.state.listView) {
                    return (
                    <ScrollView showsVerticalScrollIndicator={false}>
                        <View style={styles.mapContainer}>
                            <Image style={styles.mapImage} source={require('../../../assets/map.jpg')}/>
                        </View>
                        <FlatList
                            data={this.state.getActivityData}
                            renderItem={( { item } ) => (
                            <GetActivityListViewTrack
                                item={item}
                                self={this}
                            />
                            )}
                            extraData={this.state.getActivityData} 
                        />
                    </ScrollView>
                    )  
                } else {
                    return null;
                }
            } else if (this.state.isOrder) {
                if(this.state.listView) {
                    return (
                    <ScrollView showsVerticalScrollIndicator={false}>
                        <View style={styles.mapContainer}>
                            <Image style={styles.mapImage} source={require('../../../assets/map.jpg')}/>
                        </View>
                        <FlatList
                            data={this.state.orderData}
                            renderItem={( { item } ) => (
                            <OrderListViewTrack
                                item={item}
                                self={this}
                            />
                            )}
                            extraData={this.state.orderData} 
                        />
                    </ScrollView>
                    )  
                } else {
                    return null;
                }
              
            } else {
              return null;
            }
        }

        render() {
            return (
              <SafeAreaView style={{ flex: 1 }}>
                <AnimatedLoader
                    visible={this.state.showLoader}
                    overlayColor="rgba(255,255,255,0.75)"
                    source={require("../../../assets/loader.json")}
                    animationStyle= {[styles.lottie,{height: 150,width: 150}]}
                    speed={1}
                />
                <View style={s.bodyGray}>
                    <this.OrderDetails />
                </View>
              </SafeAreaView>
            );
        }
}

let ScreenHeight = Dimensions.get("window").height;
let ScreenWidth = Dimensions.get("window").width;

const styles = StyleSheet.create({
    mapContainer: {
        height: 300,
        width: ScreenWidth
    },
    mapImage: {
        flex: 1,
        width: undefined,
        height: undefined,
        resizeMode: "cover",
    },
    activeListingView: {
        marginVertical: 10,
        backgroundColor: "white",
        padding: 15,
        elevation: 6,
        flex: 1,
        marginHorizontal: 15,
        marginVertical: 5,
    },
});

const mapStateToProps = (state) => {
	return {
        userData: state.loginData,
	};
}

const mapDispatchToProps = (dispatch) => {
    return {
      navigationAction: bindActionCreators(navigateAction,dispatch),
    };
}

// ---------------------------------------------------- store related function  -- END

export default connect(mapStateToProps, mapDispatchToProps)(TrackingListPage);

