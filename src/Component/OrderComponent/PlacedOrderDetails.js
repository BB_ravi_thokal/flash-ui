/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import * as addToCart  from '../../Actions/CartItem';
import { bindActionCreators } from 'redux';
import axios from 'axios';
import * as Config from '../../Constant/Config.js';
import * as Constant from '../../Constant/Constant.js';
import NavigationBar from '../ReusableComponent/NavigationBar.js';
import * as loginSuccess  from '../../Actions/LoginAction';
import  s  from '../../sharedStyle.js'
import PopupModal from '../ReusableComponent/PopupComponent.js';
import AddressModal from '../ReusableComponent/addressPopUp';
import * as cartModalAction  from '../../Actions/CartModalAction';
import CouponModal from '../ReusableComponent/couponModal';
import { Platform } from 'react-native';
// import Paytm from 'react-native-paytm';
import InternateModal from '../ReusableComponent/InternetModal';
import * as navigateAction  from '../../Actions/NavigationBarAction';
import Toast from 'react-native-simple-toast';
// import 'bootstrap/dist/css/bootstrap.min.css';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  Dimensions,  
  Image,
  TouchableOpacity,
  Button,
  TextInput,
  StatusBar,
  TouchableWithoutFeedback,
  FlatList ,
  ToastAndroid,
  modal,
  Modal
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';



class PlacedOrderDetails extends React.Component{
  constructor(props) {
    super(props);
    this.state={
        orderDetails: {},
        isRefund: false,
        showMessage: false
    }
  }

  componentDidMount(){
    
    console.warn('------>',this.props.navigation.state.params.orderDetails);
    
    if (this.props.navigation.state.params.orderDetails.order.itemList.length > 0) {
      this.props.navigation.state.params.orderDetails.order.itemList = this.props.navigation.state.params.orderDetails.order.itemList.map( item => {
          if (item.isProductAvailable == false) {
            this.setState({ showMessage: true });
            item.backGroundColor = "red"   
          } else {
            item.backGroundColor = "white" 
          }
          return item;
      })
    } 
    if (this.props.navigation.state.params.orderDetails.unavailableProdAmt > 0) {
      this.setState({})
    }
    this.setState({
      orderDetails: this.props.navigation.state.params.orderDetails
    })

  }    //-----------------------------
   
  CartListing = ({data}) => {
      return (  
  
        <View style={[styles.prodcutListingView,{backgroundColor: data.backGroundColor}]}>
          
          <View style={styles.addCardImage}>
          <Image style={styles.productImage} source={{uri: data.productLogo}}/>
          </View>
            <View style={styles.stores}>
              {!data.comboId
              ? 
              <View style={{flex:10}}>
                <Text style={s.subHeaderText} numberOfLines={4}>{data.productName} </Text>
              </View>
              :
              <View style={{flex:10}}>
                <Text style={s.subHeaderText} numberOfLines={4}>{data.comboName} </Text>
              </View>
              }
              
              <View style={styles.Mystores}>
              {!data.comboId
              ? 
              <View style={{flexDirection: "column", flex: 7}}>
                <Text style={[s.normalText]} numberOfLines={4}>{data.unitCost}/{data.unit}</Text>
                <Text style={[s.normalText]} numberOfLines={1}>Total: ₹{data.totalCost}</Text>
                <Text style={[s.normalText]} numberOfLines={3}>Shop Name: {data.shopName}</Text>
              </View>
              :
              <View style={{flexDirection: "column", flex: 7}}>
                <Text style={[s.normalText]} numberOfLines={4}>{data.comboDescription}</Text>
                <Text style={[s.normalText]} numberOfLines={1}>Total: ₹{data.totalCost}</Text>
                <Text style={[s.normalText]} numberOfLines={3}>Shop Name: {data.shopName}</Text>
              </View>
              }
                {/* <View style={styles.flexRow}>
                  <View style={styles.flexRowAdd}>
                    <TouchableOpacity onPress={() => this.decrementCount(data)}>
                      <View style={styles.orderCardButton}>
                        <Text style={{fontSize:18,color:"#0080FE"}}>-</Text>
                      </View>
                    </TouchableOpacity>
                      <Text style={styles.orderCount}>{data.count}</Text>
                    <TouchableOpacity onPress={() => this.incrementCount(data)}>
                      <View style={styles.orderCardButton}>
                        <Text style={{fontSize:18,color:"#0080FE"}}>+</Text>
                      </View>
                    </TouchableOpacity>
                  </View>

                </View> */}
              </View>
            </View>
          </View>
      )
    }    


    render() {
        const {navigate} = this.props.navigation;
          return (
            <SafeAreaView style={{flex: 1}}>
              <View style={[s.body]}>
                {/* <InternateModal /> */}
                <View style={{flex:8}}>
                  <ScrollView showsVerticalScrollIndicator={false}>
                    <View style={{backgroundColor:"white",elevation: 6}}>
                      <FlatList
                        data={this.props.navigation.state.params.orderDetails.order.itemList}
                        renderItem={( { item } ) => (
                          <this.CartListing
                            data={item}
                          />
                          
                        )}
                        extraData={this.state} 
                      />
                    </View>
                    <View style={{backgroundColor:"white",marginTop:15,paddingVertical: 15,paddingHorizontal:15, elevation: 6}}>
                      <View>
                        <Text style={s.headerText}>Bill Details </Text>
                      </View>
                      <View style={{paddingTop:10}}>
                        <View style={{flex: 1, flexDirection: "row",paddingVertical: 3}}>
                          <View style={{flex: 8}}>
                            <Text style={[s.normalText]} >Item Total</Text>
                          </View>
                          <View style={{flex:4,alignItems:"flex-end"}}>
                            <Text style={[s.normalText]} >₹{(this.props.navigation.state.params.orderDetails.order.totalCost).toFixed(2)} </Text>
                          </View>
                        </View>
                        <View style={{flex: 1, flexDirection: "row",paddingVertical: 3}}>
                          <View style={{flex: 8}}>
                            <Text style={[s.normalText]} >Delivery Fee</Text>
                          </View>
                          <View style={{flex:4,alignItems:"flex-end"}}>
                            <Text style={[s.normalText]} >₹{this.props.navigation.state.params.orderDetails.order.deliveryCharge} </Text>
                          </View>
                        </View>
                        <View style={{paddingTop:10,borderBottomWidth: 1, borderBottomColor:"#7285A5"}}/>
                          <View style={{flex: 1, flexDirection: "row",paddingVertical: 3}}>
                            <View style={{flex: 8}}>
                              <Text style={[s.normalText]} >Coupon </Text>
                            </View>
                            <View style={{flex:4,alignItems:"flex-end"}}>
                              <Text style={[s.normalText]} >{this.props.navigation.state.params.orderDetails.order.coupon} </Text>
                            </View>
                          </View>
                          <View style={{flex: 1, flexDirection: "row",paddingVertical: 3}}>
                            <View style={{flex: 8}}>
                              <Text style={[s.normalText]} >Total discount </Text>
                            </View>
                            <View style={{flex:4,alignItems:"flex-end"}}>
                              <Text style={[s.normalText]} >₹{(this.props.navigation.state.params.orderDetails.order.totalDiscount).toFixed(2)} </Text>
                            </View>
                          </View>
                      </View>
                      {this.props.navigation.state.params.orderDetails.paymentStatus == 'COD' && this.props.navigation.state.params.orderDetails.order.finalCODAmountToPay
                        ?
                        <View style={{paddingTop:10,flexDirection: "row",flex:1}}>
                          <View style={{flex: 8}}>
                            <Text style={[s.normalText]} >Amount to pay</Text>
                          </View>
                          <View style={{flex:4,alignItems:"flex-end"}}>
                            <Text style={[s.normalText]} >₹{(this.props.navigation.state.params.orderDetails.order.finalCODAmountToPay).toFixed(2)} </Text>
                          </View>
                        </View>
                        :
                        this.props.navigation.state.params.orderDetails.paymentStatus == 'COD'
                        ?
                        <View style={{paddingTop:10,flexDirection: "row",flex:1}}>
                          <View style={{flex: 8}}>
                            <Text style={[s.normalText]} >Amount to pay</Text>
                          </View>
                          <View style={{flex:4,alignItems:"flex-end"}}>
                            <Text style={[s.normalText]} >₹{(this.props.navigation.state.params.orderDetails.order.toPay).toFixed(2)} </Text>
                          </View>
                        </View>
                        :
                        <View style={{paddingTop:10,flexDirection: "row",flex:1}}>
                          <View style={{flex: 8}}>
                            <Text style={[s.normalText]} >Paid amount</Text>
                          </View>
                          <View style={{flex:4,alignItems:"flex-end"}}>
                            <Text style={[s.normalText]} >₹{(this.props.navigation.state.params.orderDetails.order.toPay).toFixed(2)} </Text>
                          </View>
                        </View>

                      }
                      <this.RefundAmount />
                      
                    </View>
                    <this.ProductUnavilable />
                  </ScrollView>
                </View>
                {/* <NavigationBar navigate={navigate}></NavigationBar>   */}
              </View>

            </SafeAreaView>

          );
        }

        RefundAmount = () => {
          if (this.props.navigation.state.params.orderDetails.refundAmount > 0) {
            return (
              <View style={{paddingTop:10,flexDirection: "row",flex:1}}>
                  <View style={{flex: 8}}>
                    <Text style={[s.normalText]} >Refund Amount</Text>
                  </View>
                  <View style={{flex:4,alignItems:"flex-end"}}>
                    <Text style={[s.normalText]} >₹{(this.props.navigation.state.params.orderDetails.refundAmount).toFixed(2)} </Text>
                  </View>
              </View>
            )
          } else {
            return null;
          }
        }

        ProductUnavilable = () => {
          if (this.state.showMessage && this.props.navigation.state.params.orderDetails.paymentStatus != 'COD') {
            return (
              <View style={{paddingVertical:20, paddingHorizontal:15,flexDirection: "row",flex:1}}>
                <View style={{flex: 8}}>
                  <Text style={[s.normalText]} numberOfLines={6}>Product/Products which is/are marked red is/are currently not available in the stock and total amount of unavailable product/products will be refunded to your wallet after rest of product will get delivered.</Text>
                </View>
              </View>
            )
          } else if (this.state.showMessage) {
            return (
              <View style={{paddingVertical:20, paddingHorizontal:15,flexDirection: "row",flex:1}}>
                <View style={{flex: 8}}>
                  <Text style={[s.normalText]} numberOfLines={6}>Product/Products which is/are marked red is/are currently not available in the stock. You need to pay only for the product you will receive</Text>
                </View>
              </View>
            )
          } else {
            return null;
          }
        }
};

let ScreenHeight = Dimensions.get("window").height;
let ScreenWidth = Dimensions.get("window").width;

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: "#F5FCFF",
    height: ScreenHeight
  },
  body: {
    backgroundColor: '#F5FCFF',
  },
  prodcutListingView: {
    flexDirection: 'row',
    marginVertical: 5,
    backgroundColor: "white",
    padding: 10

  },
  addCardImage: {
    width: 80,
    height: 80,
    flex: 4
  },
  productImage: {
    width: 80,
    height: 80
  },
  stores: {
    paddingHorizontal: 10,
    flex: 8,
    flexDirection: "column"
  },
  addressButtonView: {
		backgroundColor:"#00CCFF",  
		width: '80%',
		//borderRadius: 50,
		elevation: 6,
		shadowColor: 'rgba(0, 0, 0, 0.1)',
		shadowOpacity: 0.8,
		height: 40,
		justifyContent: 'center',
		alignItems: 'center',
	},

	addressButtonText: {
		color:"white",  
		fontSize: 16 ,
		fontWeight:"bold"
	},
  Mystores: {
    flexDirection: "row",
    alignItems: "flex-start",
    justifyContent: "space-between",
    textAlignVertical: "bottom",
    flex: 2
    // paddingBottom:15,
  },
  flexRow: {
    //flex: 1,
    textAlignVertical: "bottom",
    flexDirection: 'row',
  },
  flexRowAdd: {
    paddingTop: 10,
    textAlignVertical: "bottom",
    flexDirection: 'row',
  },
  basketBtn:{
    paddingVertical: 5,
    paddingHorizontal: 10,
    marginBottom: 10,
    borderRadius: 20,
    height: 30,
    width: 30
  },
  basketImg: {
    height: 30,
    width: 30,
  },
  storeBtn: {
    paddingVertical: 5,
    paddingHorizontal: 5,
    marginBottom: 5,
    color: 'white',
    borderRadius: 0,
    width: 50
  },
  addtoCartBtnColor: {
    fontSize:12,
    borderWidth: 1,
    borderColor: '#0080FE',
    marginLeft: 20,
  },
  header: {
    fontSize: 24,
    paddingVertical: 5
  },
  orderDtlContainer:{
    borderRadius:0,
  },
  orderHeading:{
    paddingLeft: 10,
    paddingVertical:5,
    marginTop:15
  },
  orders:{
    paddingLeft:10,
  },
  orderTitles:{
    paddingLeft: 10,
    paddingVertical:15,
  },
  orderCount:{
    fontSize:18,
    fontWeight:'bold',
    paddingHorizontal:10,
    color: '#131E3A'
},
orderProductName:{
  fontWeight: 'bold',
  fontSize:16
},
  orderPriceText:{
    color:'#3cb371',
    fontWeight:'bold'
  },
  orderProductHeading:{
    fontWeight: 'bold',
    fontSize:30,
  },
  
  orderCardImage:{
    backgroundColor:'hotpink',
    width:50,
    height:50,
    borderRadius:50
},
orderCardButton:{
  borderColor:'#0080FE',
  borderWidth: 1,
  width:25,
  height:25,
  borderRadius:50,
  alignItems:"center",
  justifyContent:'center',
},
  orderList:{
    flex: 1,
    flexDirection: "column",
    padding:20,
    paddingLeft: 10,
  },
  orderBorder:{    
    borderTopColor: 'grey',
    borderTopWidth: 1,
  },
flexRow:{
  display:'flex',
  flexDirection:'row',
},
flexColumnAlignEnd:{
  display:'flex',
  flexDirection:'column',
  alignItems:'flex-end'
},
priceAlign:{
  paddingRight:5,
  paddingTop: 5,
},
orderDtlCenter:{
  display:"flex",
  flexDirection:"row",
  justifyContent:"center"
},
  MyOrders:{
    flexDirection:"row",
    alignItems:"center",
    justifyContent:"space-between",
    // paddingBottom:15,
  },
  orderCardSpace:{
    marginBottom: 15,
  },
  orderCardPadding:{
    paddingBottom: 15,
  },
  orderMainView: {
    marginHorizontal: 20,
    // flex: 3,
    backgroundColor: '#EBECF0',
  },
  headerstyle: {
    paddingBottom: 20,
    paddingTop: 20
  },

  orderBtnViewColor: {
    backgroundColor: '#ba55d3',
    color:'white',
  },
  continueBtnViewColor: {
    backgroundColor: '#2f4f4f',
    color:'white',
  },
  checkoutBtnViewColor: {
    backgroundColor: '#20b2aa',
    color:'white',
    marginLeft:20,
  },
  orderBtn: {
    paddingVertical:10,
    paddingHorizontal:20,
    marginBottom:10,
    color:'white',
    borderRadius: 20,
  },

});



//--------------------------------------------------- store related fuction  -- Start

const mapStateToProps = (state) => {
	return {
    cartItem: state.cartItem,
    userData: state.loginData,
    cartState: state.cartState,
	}
}

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(addToCart, dispatch),
        loginActions: bindActionCreators(loginSuccess,dispatch),
        cartModalAction: bindActionCreators(cartModalAction,dispatch),
        navigationAction: bindActionCreators(navigateAction,dispatch),
    };
}

// ---------------------------------------------------- store related function  -- END

export default connect(mapStateToProps, mapDispatchToProps)(PlacedOrderDetails);
