/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import * as addToCart  from '../../Actions/CartItem';
import { bindActionCreators } from 'redux';
import axios from 'axios';
import * as Config from '../../Constant/Config.js';
import * as Constant from '../../Constant/Constant.js';
import  NavigationBar  from '../ReusableComponent/NavigationBar.js';
import  s  from '../../sharedStyle.js';
import CartModal from '../ReusableComponent/cartModal';
import * as basketAction  from '../../Actions/BasketAction';
import * as cartModalAction  from '../../Actions/CartModalAction';
import BasketModal from '../ReusableComponent/basketModal';
import InternateModal from '../ReusableComponent/InternetModal';
import { Card, CardTitle, CardContent, CardAction, CardButton, CardImage } from 'react-native-material-cards';
import * as navigateAction  from '../../Actions/NavigationBarAction';
import { Rating } from 'react-native-ratings';
import Toast from 'react-native-simple-toast';
// import 'bootstrap/dist/css/bootstrap.min.css';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    Dimensions,
    TouchableOpacity,
    Button,
    TextInput,
    StatusBar,
    TouchableWithoutFeedback,
    ToastAndroid,
    Image,
    FlatList,
    BackHandler
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';



class BasketListPage extends React.Component{
    constructor(props) {
        super(props);
        this.state={
            paddingBottomHeight: 0
        }
    }

    componentDidMount() {
        this.props.navigationAction.navigateAction('Basket');
        if (this.props.cartState.cartState) {
            this.setState({paddingBottomHeight: 80});
        }
        this.props.navigation.addListener('willFocus', () =>{
            console.warn('am in focus Home');
            this.props.navigationAction.navigateAction('Basket');
        });
    }


    //function to add the new item in the cart   --- Start
  addCart = (basketId) => {
    console.warn('this.props.basketItem.basketItem', this.props.basketItem.basketItem);
    const index = this.props.basketItem.basketItem.findIndex(ind => ind.basketId === basketId);
    this.props.basketItem.basketItem[index].itemDetails.forEach(element => {
        if(!this.props.cartItem.cartItem.some(arr => arr.productId == element.productId)) {
            const cart = {...element}
            this.props.cartState.cartCount += 1;
            this.props.cartState.cartSum = parseFloat(this.props.cartState.cartSum) + parseFloat(element.unitCost); 
            this.props.cartModalAction.changeCartState(true,this.props.cartState.cartSum,this.props.cartState.cartCount);
            this.props.actions.addToCart(cart);
        }   

    }); 
    Toast.show(this.props.basketItem.basketItem[index].basketName + " is added in the cart",Toast.LONG);
    this.setState({ paddingBottomHeight: 80 });
  }


  //function to add the new item in the cart   --- End

    BasketListing = (item) => {
        const {navigate} = this.props.navigation;
        return (
            <View style={{ marginHorizontal: 15, width: ScreenWidth - 30 }}>
                <Card style={{ elevation: 6 }}>
                    <CardImage 
                        source={require('../../../assets/shoppingBasket.png')}
                        title= {item.item.basketName}
                        textStyle = {styles.cardImgTxt}
                        style = {styles.basketImage}

                    />
                    <CardAction 
                        
                        separator={true}
                        inColumn={false}
                        >
                        <TouchableOpacity style={{justifyContent:"center",alignItems:"center",backgroundColor:"#00CCFF",width:'50%'}}
                            onPress={() => navigate('basketDetailsPage',{basketId: item.item.basketId})}>
                            <Text style={[s.headerText,{padding: 15}]}>Explore</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{justifyContent:"center",alignItems:"center",backgroundColor:"#00CCFF",width:'50%'}}
                            onPress={() => {this.addCart(item.item.basketId)}}>
                            <Text style={[s.headerText,{padding: 15}]}>Add to cart</Text>
                        </TouchableOpacity>       
                    </CardAction>
                </Card>
            </View>
       
        )
    }



    render() {
        const {navigate} = this.props.navigation;
        if(this.props.basketItem.basketItem.length === 0) {
            return (
                <Fragment>
                <SafeAreaView style={{flex: 0, backgroundColor: '#f1f1f1'}} />
                    <SafeAreaView style={{flex: 1, backgroundColor: 'white'}}>
                        <View style={[s.body]}>
                            {/* <InternateModal /> */}
                            <View style={[{flex:1}]}> 
                                <Image style={{width:ScreenWidth,height:ScreenHeight - 80}} source={require('../../../assets/EmptyCart.jpg')}/>
                            </View>
                            <NavigationBar navigate={navigate}></NavigationBar>
                        </View>
                    </SafeAreaView>
                </Fragment>
            )
        }
        return (
            <Fragment>
                <SafeAreaView style={{flex: 0, backgroundColor: '#f1f1f1'}} />
                <SafeAreaView style={{flex: 1, backgroundColor: 'white'}}>
                    <View style={s.bodyGray} >
                        <View style={{flex: 1, paddingBottom: this.state.paddingBottomHeight}}>
                            <ScrollView showsVerticalScrollIndicator={false} >
                                <FlatList data={this.props.basketItem.basketItem}
                                    renderItem={( { item } ) => (
                                        <this.BasketListing
                                            item = {item}
                                            navigate = {this.props.navigation}
                                        />
                                    )}
                                    keyExtractor= {item => item}
                                    horizontal={false}
                                    extraData={this.Props}
                                    
                                />
                            </ScrollView>
                        </View>
                        <CartModal navigate={navigate} navigationPage={'orderDetailsPage'}></CartModal>
                        <NavigationBar navigate={navigate}></NavigationBar>
                    </View>
                </SafeAreaView>
            </Fragment>
        );
  }
}
let ScreenHeight = Dimensions.get("window").height;
let ScreenWidth = Dimensions.get("window").width;
const styles = StyleSheet.create({
    scrollView: {
        //paddingVertical: 20,
        // flex: 1,
        marginTop: 20
    },
    cardImgTxt: {
        color: '#03AC13',
        fontSize:24,
        fontWeight: "bold",
        fontFamily: "verdana",
        elevation: 6
    },
    basketImage: {
        width: ScreenWidth - 45,
        
    }
});

//--------------------------------------------------- store related fuction  -- Start

const mapStateToProps = (state) => {
	return {
    cartItem: state.cartItem,
    basketItem: state.basketItem,
    cartState: state.cartState,
	}
}

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(addToCart, dispatch),
        basketAction: bindActionCreators(basketAction,dispatch),
        cartModalAction: bindActionCreators(cartModalAction,dispatch),
        navigationAction: bindActionCreators(navigateAction,dispatch),
    };
}

// ---------------------------------------------------- store related function  -- END

export default connect(mapStateToProps, mapDispatchToProps)(BasketListPage);

