/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import * as addToCart  from '../../Actions/CartItem';
import { bindActionCreators } from 'redux';
import axios from 'axios';
import * as Config from '../../Constant/Config.js';
import * as Constant from '../../Constant/Constant.js';
import  NavigationBar  from '../ReusableComponent/NavigationBar.js';
import  s  from '../../sharedStyle.js';
import CartModal from '../ReusableComponent/cartModal';
import * as basketAction  from '../../Actions/BasketAction';
import * as cartModalAction  from '../../Actions/CartModalAction';
import BasketModal from '../ReusableComponent/basketModal';
import InternateModal from '../ReusableComponent/InternetModal';
import Toast from 'react-native-simple-toast';

import { Rating } from 'react-native-ratings';
// import 'bootstrap/dist/css/bootstrap.min.css';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    Dimensions,
    TouchableOpacity,
    Button,
    TextInput,
    StatusBar,
    TouchableWithoutFeedback,
    ToastAndroid,
    Image,
    FlatList,
    BackHandler
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import * as navigateAction  from '../../Actions/NavigationBarAction';
import PopupModal from '../ReusableComponent/PopupComponent.js';

class BasketDetailsPage extends React.Component{
    constructor(props) {
        super(props);
        this.state={
            basketItems: [],
            itemIndex: null,
            basketIndex: null,
            cart: {},
            popModalVisible: false,
            cartRemoveModal: false,
            selectedProduct: {},
            paddingBottomHeight: 0
        }
    }

    componentDidMount(){
        this.props.navigationAction.navigateAction('Basket');
        if (this.props.cartState.cartState) {
            console.warn('here')
            this.setState({paddingBottomHeight: 110});
        }
        const basketId = this.props.navigation.state.params.basketId;
        console.warn('basket id current--->', basketId);
        
        const index = this.props.basketItem.basketItem.findIndex(ind => ind.basketId === basketId);
        this.setState({
            itemIndex: index
        })
        console.warn("basketId",this.props.basketItem.basketItem[index]);
        this.setState({
            basketItems: this.props.basketItem.basketItem[index].itemDetails
        })

        console.warn('----display baste iten--->', this.state.basketItems);
    }

    
  //function to add the new item in the cart   --- Start
  addCart = async(productData) => {
    console.warn("---------productId ",productData);
    // console.warn("---------productId ",productData.productId);
    const cart = {...productData};

    await this.setState({
        cart: {...productData}
    })

    if(this.props.cartItem.cartItem.some(arr => arr.productId == productData.productId)){
      Toast.show("Already added in the cart",Toast.LONG);
    } else if (!this.props.cartItem.cartItem.some(arr => arr.shopId == cart.shopId)) {
        if (this.props.cartItem.cartItem.length > 0) {
          this.setState({
            popModalVisible: true
          })
        } else {
          this.confirmAddCart(cart);
        }
      }
      else{
        this.confirmAddCart(cart);
      }



    // else{
    //   this.props.cartState.cartCount += 1;
    //   this.props.cartState.cartSum += (parseFloat(this.props.cartState.cartSum) + parseFloat(productData.unitCost)); 
    //   this.props.cartModalAction.changeCartState(true,this.props.cartState.cartSum,this.props.cartState.cartCount);
    //   this.props.actions.addToCart(cart);
    //   Toast.show(productData.productName + " is added in the cart",Toast.LONG);

    // }
  }

  confirmAddCart = (cart) => {
    this.props.cartState.cartCount += 1;
    this.props.cartState.cartSum = (parseFloat(this.props.cartState.cartSum) + parseFloat(cart.unitCost)); 
    this.props.cartModalAction.changeCartState(true,this.props.cartState.cartSum,this.props.cartState.cartCount);
    this.props.actions.addToCart(cart);
    this.setState({ basketItems: this.state.basketItems, paddingBottomHeight: 110 });
    Toast.show(cart.productName + " is added in the cart",Toast.LONG);

  }

  userAckCart = () => {
    this.setState({
        popModalVisible: false
    })
    this.confirmAddCart(this.state.cart);

  }

  hideModal = () => {
    this.setState({
        popModalVisible: false,
        cartRemoveModal: false
    })
  }


  //function to add the new item in the cart   --- End

//function to add the new item in the cart   --- Start
removeItem = (productData) => {
    console.warn("---------productId ",productData.productId);
    const index = this.props.basketItem.basketItem[this.state.itemIndex].itemDetails.findIndex(ind => ind.productId === productData.productId);

    this.props.basketItem.basketItem[this.state.itemIndex].itemDetails.splice(index,1);
    this.setState({
        basketItems: this.props.basketItem.basketItem[this.state.itemIndex].itemDetails
    })
    if (this.props.basketItem.basketItem[this.state.itemIndex].itemDetails.length <= 0) {
        this.props.basketItem.basketItem.splice(this.state.itemIndex, 1);
    }
    this.props.basketAction.replaceBasket(this.props.basketItem.basketItem);
    console.warn('baskt itm-->', this.state.basketItems);
    Toast.show(productData.productName + " is remove from the basket",Toast.LONG);

}

    incrementCount = async(productData) => {
        this.setState({
        selectedProduct: productData,
        });
        let index = this.props.cartItem.cartItem.findIndex(ind => ind.productId === productData.productId);
        this.props.cartState.cartCount += 1;
        this.props.cartState.cartSum = parseFloat(this.props.cartState.cartSum) + parseFloat(this.props.cartItem.cartItem[index].unitCost); 
        this.props.cartModalAction.changeCartState(true,this.props.cartState.cartSum,this.props.cartState.cartCount);
        this.props.cartItem.cartItem[index].counter = this.props.cartItem.cartItem[index].counter + 1;
        this.props.cartItem.cartItem[index].totalCost = this.props.cartItem.cartItem[index].unitCost * this.props.cartItem.cartItem[index].counter;
        this.props.actions.replaceCart(this.props.cartItem.cartItem);
        await this.setState({ basketItems: this.state.basketItems});
    }

    decrementCount = async(productData) => {
        this.setState({
        selectedProduct: productData
        });
        let id = this.props.cartItem.cartItem.findIndex(ind => ind.productId === productData.productId);
        if (id >= 0) {
            if(this.props.cartItem.cartItem[id].counter - 1 <= 0){
            this.setState({
                cartRemoveModal: true
            });
            } else {
            this.props.cartItem.cartItem[id].counter = this.props.cartItem.cartItem[id].counter - 1;
            this.props.cartItem.cartItem[id].totalCost = this.props.cartItem.cartItem[id].unitCost * this.props.cartItem.cartItem[id].counter;
            this.props.cartState.cartCount -= 1;
            this.props.cartState.cartSum = parseFloat(this.props.cartState.cartSum) - parseFloat(this.props.cartItem.cartItem[id].unitCost); 
            this.props.cartModalAction.changeCartState(true,this.props.cartState.cartSum,this.props.cartState.cartCount);
            this.props.actions.replaceCart(this.props.cartItem.cartItem);
            await this.setState({ basketItems: this.state.basketItems});
            }
        }  
    }

    
    removeProduct = async() => {
        this.setState({
            cartRemoveModal: false,
            removeProduct: true,
        });
        const i = this.props.cartItem.cartItem.findIndex(ind => ind.productId === this.state.selectedProduct.productId);
        
        this.props.cartState.cartCount -= 1;
        this.props.cartState.cartSum = parseFloat(this.props.cartState.cartSum) - parseFloat(this.props.cartItem.cartItem[i].unitCost); 
        
        this.props.cartItem.cartItem.splice(i,1);
        if(this.props.cartState.cartCount === 0){
        this.props.cartModalAction.changeCartState(false,this.props.cartState.cartSum,this.props.cartState.cartCount);
        } else {
        this.props.cartModalAction.changeCartState(true,this.props.cartState.cartSum,this.props.cartState.cartCount);
        }
        this.props.actions.replaceCart(this.props.cartItem.cartItem);
        await this.setState({ basketItems: this.state.basketItems});
    }


  //function to add the new item in the cart   --- End

  


    ItemListing = (item) => {
        return (
    
            <View style={[styles.prodcutListingView]}>
                <View style={styles.addCardImage}>
                    <Image style={styles.productImage} source={{uri: item.item.productLogo}}/>
                </View>
                <View style={styles.stores}>
                    <View>
                        <Text style={[s.subHeaderText, {marginBottom: 2}]} numberOfLines={4}>{item.item.productName} </Text>
                    </View>
                    <View style={{paddingTop: 10}}>
                        <Text style={[s.normalText, {marginBottom: 2}]} numberOfLines={4}>Qty:{item.item.unitCost}/{item.item.unit}</Text>
                        <Text style={[s.normalText, {marginBottom: 2}]} numberOfLines={3}>Shop Name: {item.item.shopName}</Text>
                    </View>
                    
                    <View style={styles.Mystores}>
                        <View style={{paddingTop: 10, flex: 5}}>
                            <Text style={[s.normalText, {marginBottom: 2}]} numberOfLines={1}>Total: ₹ {item.item.totalCost}</Text>
                        </View>
                        <View style={[styles.flexRow, {marginTop: 15}]}>
                            <TouchableOpacity style={styles.basketBtn} onPress={() => this.removeItem(item.item)}>
                                <Image style={{height: 30, width: 30}} source={require('../../../assets/basketDelete.png')}/>
                            </TouchableOpacity>
                            {/* <TouchableOpacity onPress={() => this.addCart(item.item)}>
                                <View style={[styles.storeBtn, styles.addtoCartBtnColor]}>
                                    <Text style={{ color: '#0080FE',paddingHorizontal: 4}}>ADD</Text>
                                </View>
                            </TouchableOpacity> */}
                            {this.props.cartItem.cartItem.findIndex(ind => ind.productId === item.item.productId) >= 0
                            ?
                                <View style={styles.flexRowAdd}>
                                    <TouchableOpacity onPress={() => this.decrementCount(item.item)}>
                                        <View style={styles.orderCardButton}>
                                            <Text style={{fontSize:18,color:"#0080FE"}}>-</Text>
                                        </View>
                                    </TouchableOpacity>
                                    {/* <Text style={styles.orderCount}>{item.item.counter}</Text> */}
                                    <Text style={styles.orderCount}>{this.props.cartItem.cartItem[this.props.cartItem.cartItem.findIndex(ind => ind.productId === item.item.productId)].counter}</Text>
                                    <TouchableOpacity onPress={() => this.incrementCount(item.item)}>
                                    <View style={styles.orderCardButton}>
                                        <Text style={{fontSize:18,color:"#0080FE"}}>+</Text>
                                    </View>
                                    </TouchableOpacity>
                                </View>
                                :
                                <TouchableOpacity style={[styles.storeBtn, styles.addtoCartBtnColor]} onPress={() => this.addCart(item.item)}>
                                    <Text style={{ color: '#0080FE',paddingHorizontal: 4}}>ADD</Text>
                                </TouchableOpacity>

                            }
            
                        </View>
                    </View>
                </View>
            </View>
        )
    }

    



    render() {
        const {navigate} = this.props.navigation;
        return (
            <SafeAreaView style={{flex: 1}}>
                <View style={[s.bodyGray, {flex: 1}]}>
                    {/* <InternateModal /> */}
                    <ScrollView style={[styles.scrollView]} showsVerticalScrollIndicator={false}>
                        <View style= {{paddingBottom: this.state.paddingBottomHeight}}>
                            <FlatList data={this.state.basketItems}
                                renderItem={( { item } ) => (
                                    <this.ItemListing
                                        item = {item}
                                    />
                                )}
                                keyExtractor= {item => item}
                                horizontal={false}
                                extraData={this.state}
                            />
                        </View>
                    </ScrollView>
                    {/* <CartModal navigate={navigate}></CartModal> */}
                    {/* <NavigationBar navigate={navigate}></NavigationBar> */}
                    <CartModal navigate={navigate} navigationPage={'orderDetailsPage'}></CartModal>
                    <PopupModal visible={this.state.cartRemoveModal} onDismiss={this.hideModal} removeProduct={this.removeProduct} 
                            message={"Are you sure want to remove the product from cart?"} btnTag={"Remove"}/>
                    <PopupModal visible={this.state.popModalVisible} onDismiss={this.hideModal} removeProduct={this.userAckCart} 
                        message={"You are adding product of diffrent shop in cart. Your delivery charges may vary. Do you want to continue?"} 
                        btnTag={"Add to cart"}/>
                </View>
                
        
            </SafeAreaView>
     
    );
    }
}
let ScreenHeight = Dimensions.get("window").height;
let ScreenWidth = Dimensions.get("window").width;

const styles = StyleSheet.create({
    scrollView: {
        paddingTop: 20,
    },
    prodcutListingView: {
        flexDirection: 'row',
        marginVertical: 5,
        backgroundColor: "white",
        padding: 10,
        elevation: 4,
        marginHorizontal: 15
    },
    addCardImage: {
        width: 80,
        height: 80,
        flex: 3
    },
    productImage: {
        width: 80,
        height: 80
    },
    stores: {
        paddingHorizontal: 15,
        flex: 9,
        flexDirection: "column",
    },
    Mystores: {
        flexDirection: "row",
        alignItems: "flex-start",
        justifyContent: "space-between",
        textAlignVertical: "bottom",
        flex: 1
        // paddingBottom:15,
    },
    flexRow: {
        flex: 7,
        textAlignVertical: "bottom",
        flexDirection: 'row',
    },
    orderCardButton:{
        borderColor:'#0080FE',
        borderWidth: 1,
        width:25,
        height:25,
        borderRadius:50,
        alignItems:"center",
        justifyContent:'center',
      },
      orderCount:{
        fontSize:18,
        fontWeight:'bold',
        paddingHorizontal:10,
        color: '#131E3A'
      },
      flexRowAdd: {
        marginLeft: 20,
        paddingTop: 8,
        textAlignVertical: "bottom",
        flexDirection: 'row',
      },
    basketBtn:{
        paddingVertical: 5,
        paddingHorizontal: 10,
        marginBottom: 10,
        height: 30,
        width: 30
    },
    basketImg: {
        height: 30,
        width: 30,
    },
    storeBtn: {
        paddingVertical: 5,
        paddingHorizontal: 5,
        marginBottom: 5,
        color: 'white',
        borderRadius: 0,
        width: 50
    },
    addtoCartBtnColor: {
        fontSize:12,
        borderWidth: 1,
        borderColor: '#0080FE',
        marginLeft: 20,
    },
});

//--------------------------------------------------- store related fuction  -- Start

const mapStateToProps = (state) => {
	return {
        cartItem: state.cartItem,
        basketItem: state.basketItem,
        cartState: state.cartState,
	}
}

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(addToCart, dispatch),
        basketAction: bindActionCreators(basketAction,dispatch),
        cartModalAction: bindActionCreators(cartModalAction,dispatch),
        navigationAction: bindActionCreators(navigateAction,dispatch),
    };
}

// ---------------------------------------------------- store related function  -- END

export default connect(mapStateToProps, mapDispatchToProps)(BasketDetailsPage);

