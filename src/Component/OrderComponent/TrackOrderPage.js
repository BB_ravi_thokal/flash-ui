// +++++++++++++++++++++++++++++++++

/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Fragment } from 'react';

import * as Config from '../../Constant/Config.js';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as addToCart  from '../../Actions/CartItem';
import  s  from '../../sharedStyle.js';
import InternateModal from '../ReusableComponent/InternetModal';
import AnimatedLoader from "react-native-animated-loader";
import MapView, { Marker, AnimatedRegion, Polyline } from 'react-native-maps';
import Geolocation from '@react-native-community/geolocation';
// import SceneLoader from 'react-native-scene-loader';
import PopupModal from '../ReusableComponent/PopupComponent.js';
import * as cartModalAction  from '../../Actions/CartModalAction';
import axios from 'axios'; 
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  Image,
  Dimensions,
  Button,
  TouchableOpacity,
  FlatList,
  ToastAndroid,
  Linking
} from 'react-native';
import * as navigateAction  from '../../Actions/NavigationBarAction';
import * as walletAction  from '../../Actions/WalletAction';
import * as driverLocationAction  from '../../Actions/locationAction';
import Toast from 'react-native-simple-toast';
// npm install react-native-step-indicator --save
import StepIndicator from 'react-native-step-indicator';

const {width, height} = Dimensions.get("window")
const ASPECT_RATIO = width / height;
const LATITUDE = 37.78825;
const LONGITUDE = -122.4324;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
let labels = ["Approval Pending","Order Approved","Shipped","Out for delivery", "Delivered"];
const customStyles = {
  stepIndicatorSize: 30,
  currentStepIndicatorSize:30,
  separatorStrokeWidth: 2,
  currentStepStrokeWidth: 3,
  stepStrokeCurrentColor: '#fe7013',
  stepStrokeWidth: 2,
  stepStrokeFinishedColor: '#fe7013',
  stepStrokeUnFinishedColor: '#aaaaaa',
  separatorFinishedColor: '#fe7013',
  separatorUnFinishedColor: '#aaaaaa',
  stepIndicatorFinishedColor: '#fe7013',
  stepIndicatorUnFinishedColor: '#ffffff',
  stepIndicatorCurrentColor: '#ffffff',
  stepIndicatorLabelFontSize: 13,
  currentStepIndicatorLabelFontSize: 13,
  stepIndicatorLabelCurrentColor: '#fe7013',
  stepIndicatorLabelFinishedColor: '#ffffff',
  stepIndicatorLabelUnFinishedColor: '#aaaaaa',
  labelColor: '#999999',
  labelSize: 13,
  currentStepLabelColor: '#fe7013'
}

SendActivityListViewTrack = ({item, self}) => {
  const {navigate} = self.props.navigation;
  return (
    <TouchableOpacity style={[styles.activeListingView]} onPress={() => self.getDataOfSpecificId('sendActivity', item.orderId, true)}>
        <View>
            <Text style={s.subHeaderText} numberOfLines={1}>Activity Tag: {item.order.activityDetails[0].activityTag}</Text>
        </View>
        <View>
            <Text style={s.normalText} numberOfLines={1}>Activity status: {item.orderStatus}</Text>
        </View>
    </TouchableOpacity>
    
  )
}

SendActivityListTrack = ({item, self}) => {
  const {navigate} = self.props.navigation;
  return (
      <View>
          <View style={{backgroundColor: "white", elevation: 6, paddingHorizontal: 15}}>
              <Text style={styles.headingText}>Activity Status</Text>
              <Text style={[s.headerText,{textAlign: "center", marginTop: 10}]}>Activity Tag: {item.order.activityDetails[0].activityTag}</Text>
              <View style={styles.horizontalLine}/>
              <StepIndicator
                customStyles={customStyles}
                currentPosition={self.state.currentPosition}
                labels={labels}
              />
          </View>
          <View style={{backgroundColor:"white",marginTop:15,paddingVertical: 15,paddingHorizontal:15, elevation: 6}}>
              <View>
                  <Text style={s.headerText}>Activity Details </Text>
              </View>
              <View style={{marginTop: 10, flexDirection: "row"}}>
                  <Text style={s.subHeaderText}>Activity Id:</Text>
                  <Text style={[s.normalText, {marginLeft: 5, paddingTop: 2}]}>{item.orderId}</Text>
              </View>
              <View style={{marginTop: 10}}>
                  <Text style={s.subHeaderText}>Package Description:</Text>
                  <Text style={s.normalText} numberOfLines={200}>{item.order.activityDetails[0].packageDescription}</Text>
              </View>
              <View style={{marginTop: 10, flexDirection: "row"}}>
                  <Text style={s.subHeaderText}>Contact person name:</Text>
                  <Text style={[s.normalText, {marginLeft: 5, paddingTop: 2}]}>{item.order.activityDetails[0].contactPersonName}</Text>
              </View>
              <View style={{marginTop: 10, flexDirection: "row"}}>
                  <Text style={s.subHeaderText}>Contact person number:</Text>
                  <Text style={[s.normalText, {marginLeft: 5, paddingTop: 2}]}>{item.order.activityDetails[0].contactPersonMobNo}</Text>
              </View>
          </View> 
          <View style={{backgroundColor:"white",marginTop:15,paddingVertical: 15,paddingHorizontal:15, elevation: 6}}>
              <View style={{flex: 1, flexDirection:"row"}}>
                <View style={{flex: 6}}>
                  <Text style={[s.headerText]}>Bill Details </Text>
                </View>
                {self.state.isPastOrder && item.invoiceURL ?
                  <TouchableOpacity style={{flex: 6, alignItems: "flex-end"}} onPress={() => self.downloadInvoice(item.invoiceURL)}>
                    <Image style={{height: 30, width: 30}} source={require('../../../assets/download.png')}/>  
                  </TouchableOpacity> 
                :
                null
                }
              </View>
              <View style={{paddingTop:10}}>
                  
                  <View style={{flex: 1, flexDirection: "row",paddingVertical: 3}}>
                    <View style={{flex: 8}}>
                        <Text style={[s.normalText]} >Delivery Fee</Text>
                    </View>
                    <View style={{flex:4,alignItems:"flex-end"}}>
                        <Text style={[s.normalText]} >₹{(item.order.deliveryCharge).toFixed(2)} </Text>
                    </View>
                  </View>
              </View>
              <View style={{paddingTop:10,flexDirection: "row",flex:1}}>
                  <View style={{flex: 8}}>
                    <Text style={[s.normalText]} >Amount paid</Text>
                  </View>
                  <View style={{flex:4,alignItems:"flex-end"}}>
                    <Text style={[s.normalText]} >₹{(item.order.toPay).toFixed(2)} </Text>
                  </View>
              </View>
              {/* <self.downloadInvoice /> */}
          </View> 
          <View style={{marginTop: 15,backgroundColor: "white", paddingHorizontal: 15, paddingVertical:15, elevation: 6}}>
              <View>
                  <Text style={s.headerText}>Pickup Address</Text>
              </View>
              <View style={{marginTop: 10}}>
                  <Text style={s.normalText}>Flat No: {item.order.activityDetails[0].pickupAddress.flatNumber}, 
                      {item.order.activityDetails[0].pickupAddress.addressLine1} </Text>
                  <Text style={s.normalText}>{item.order.activityDetails[0].pickupAddress.addressLine2}</Text>    
              </View>
          </View>  
          <View style={{marginTop: 15,backgroundColor: "white", paddingHorizontal: 15, paddingVertical:15, elevation: 6}}>
              <View>
                  <Text style={s.headerText}>Delivery Address</Text>
              </View>
              <View style={{marginTop: 10}}>
                  <Text style={s.normalText}>Flat No: {item.order.activityDetails[0].deliveryAddress.flatNumber}, 
                      {item.order.activityDetails[0].deliveryAddress.addressLine1} </Text>
                  <Text style={s.normalText}>{item.order.activityDetails[0].deliveryAddress.addressLine2}</Text>    
              </View>
          </View> 
          <View style={{backgroundColor:"white",marginTop:15,paddingVertical: 15,paddingHorizontal:15, elevation: 6}}>
              <View>
                  <Text style={s.headerText}>Delivery man details </Text>
              </View>
              <self.DeliveryManDetails />
              
          </View> 
                  
      </View>
  )
}

GetActivityListViewTrack = ({item, self}) => {
  const {navigate} = self.props.navigation;
  return (
    <TouchableOpacity style={[styles.activeListingView]} onPress={() => self.getDataOfSpecificId('getActivity', item.orderId, true)}>
        <View>
            <Text style={s.subHeaderText} numberOfLines={1}>Activity Tag: {item.order.activityDetails[0].activityTag}</Text>
        </View>
        <View>
            <Text style={s.normalText} numberOfLines={1}>Activity status: {item.orderStatus}</Text>
        </View>
    </TouchableOpacity>
  )
}

GetActivityListTrack = ({item, self}) => {
  const {navigate} = self.props.navigation;
  return (
      <View>
          <View style={{backgroundColor: "white", elevation: 6, paddingHorizontal: 15}}>
              <Text style={styles.headingText}>Activity Status</Text>
              <Text style={[styles.subHeadingText, {textAlign:"center"}]}>{item.orderId}</Text>
              <View style={styles.horizontalLine}/>
              <StepIndicator
                customStyles={customStyles}
                currentPosition={self.state.currentPosition}
                labels={labels}
              />
          </View>
          <View style={{backgroundColor: "white", elevation: 6, paddingHorizontal: 15}}>
          <Text style={[s.headerText,{textAlign: "center", marginTop: 10}]}>Activity list</Text>
            <FlatList
              data={item.order.activityDetails}
              renderItem={( { item } ) => (
              <ActivityListTrack
                  item={item}
                  self={self}
              />
              )}
              extraData={item.activityDetails} 
            />
            
          </View>
          <View style={{backgroundColor:"white",marginTop:15,paddingVertical: 15,paddingHorizontal:15, elevation: 6}}>
              <View style={{flex: 1, flexDirection:"row"}}>
                  <View style={{flex: 6}}>
                    <Text style={[s.headerText]}>Bill Details </Text>
                  </View>
                  {self.state.isPastOrder && item.invoiceURL?
                  <TouchableOpacity style={{flex: 6, alignItems: "flex-end"}} onPress={() => self.downloadInvoice(item.invoiceURL)}>
                    <Image style={{height: 30, width: 30}} source={require('../../../assets/download.png')}/>  
                  </TouchableOpacity> 
                  :
                  null
                  }
                </View>
              <View style={{paddingTop:10}}>
                  
                  <View style={{flex: 1, flexDirection: "row",paddingVertical: 3}}>
                    <View style={{flex: 8}}>
                        <Text style={[s.normalText]} >Delivery Fee</Text>
                    </View>
                    <View style={{flex:4,alignItems:"flex-end"}}>
                        <Text style={[s.normalText]} >₹ {(item.order.deliveryCharge).toFixed(2)} </Text>
                    </View>
                  </View>
              </View>
              <View style={{paddingTop:10,flexDirection: "row",flex:1}}>
                  <View style={{flex: 8}}>
                    <Text style={[s.normalText]} >Amount paid</Text>
                  </View>
                  <View style={{flex:4,alignItems:"flex-end"}}>
                    <Text style={[s.normalText]} >₹{(item.order.toPay).toFixed(2)} </Text>
                  </View>
              </View>
          </View> 
          <View style={{marginTop: 15,backgroundColor: "white", paddingHorizontal: 15, paddingVertical:15, elevation: 6}}>
              <View>
                  <Text style={s.headerText}>Pickup Address</Text>
              </View>
              <View style={{marginTop: 10}}>
                  <Text style={s.normalText}>Flat No: {item.order.pickupAddress.flatNumber}, 
                      {item.order.pickupAddress.addressLine1} </Text>
                  <Text style={s.normalText}>{item.order.pickupAddress.addressLine2}</Text>    
              </View>
          </View>  
          <View style={{backgroundColor:"white",marginTop:15,paddingVertical: 15,paddingHorizontal:15, elevation: 6}}>
              <View>
                  <Text style={s.headerText}>Delivery man details </Text>
              </View>
              <self.DeliveryManDetails />
              
          </View> 
          {/* <View style={{marginTop: 15,backgroundColor: "white", paddingHorizontal: 15, paddingVertical:15, elevation: 6}}>
              <View>
                  <Text style={s.headerText}>Delivery Address</Text>
              </View>
              <View style={{marginTop: 10}}>
                  <Text sytele={s.normalText}>Flat No: {item.deliveryAddress.flatNumber}, 
                      {item.deliveryAddress.addressLine1} </Text>
                  <Text sytele={s.normalText}>{item.deliveryAddress.addressLine2}, 
                      district: {item.deliveryAddress.district}</Text>
                  <Text sytele={s.normalText}>state: {item.deliveryAddress.state}, 
                      {item.deliveryAddress[0].pinCode}</Text>    
              </View>
          </View>   */}
          {/* <View style={{alignItems:"center", marginVertical:'2%'}}> 
              <TouchableOpacity style={styles.addressButtonView} 
                  onPress={() => navigate('placedOrderDetails',{"orderId": item.placedOrderId })}>
                  <Text style={styles.addressButtonText}>More Details</Text>   
              </TouchableOpacity>
          </View> */}
                  
      </View>
  )
}

ActivityListTrack = ({item, self}) => {
  return (
    <View>
      <View style={{marginTop: 10}}>
        <Text style={s.subHeaderText}>Activity Tag:</Text>
        <Text style={[s.normalText]}>{item.activityTag}</Text>
      </View>
      
      <View style={{marginTop: 10}}>
        <Text style={s.subHeaderText}>Package Description:</Text>
        <Text style={s.normalText} numberOfLines={200}>{item.activityTag}</Text>
      </View>
      <View style={{marginTop: 10}}>
        <Text style={s.subHeaderText}>Delivery Address</Text>
        <Text style={s.normalText}>Flat No: {item.deliveryAddress.flatNumber}, 
            {item.deliveryAddress.addressLine1} </Text>
        <Text style={s.normalText}>{item.deliveryAddress.addressLine2}</Text>    
      </View>
      <View style={{marginTop: 10}}>
        <Text style={s.subHeaderText}>Contact person name:</Text>
        <Text style={[s.normalText, {marginLeft: 5, paddingTop: 2}]}>{item.contactPersonName}</Text>
      </View>
      <View style={{marginTop: 10}}>
        <Text style={s.subHeaderText}>Contact person number:</Text>
        <Text style={[s.normalText, {marginLeft: 5, paddingTop: 2}]}>{item.contactPersonMobNo}</Text>
      </View>
      <View style={styles.horizontalLine} />
    </View>
  )
}

OrderListViewTrack = ({item, self}) => {
  const {navigate} = self.props.navigation;
  return (
    <TouchableOpacity style={[styles.activeListingView]} onPress={() => self.getDataOfSpecificId('order', item.orderId, false)}>
        <View style={{flexDirection:"row"}}>
            <Text style={s.headerText} numberOfLines={1}>Order Id:</Text>
            <Text style={[s.normalText, {marginLeft: 5, paddingTop: 1}]} numberOfLines={1}>{item.orderId}</Text>
        </View>
        <View style={{paddingTop: 10}}>
            <Text style={[s.normalText, {paddingLeft: 5}]}>Total amount: ₹ {(item.order.toPay).toFixed(2)}</Text>   
          </View>
    </TouchableOpacity>
    
  )
}

OrderListTrack = ({item, self}) => {
  const {navigate} = self.props.navigation;
  return (
      <View >
        <View>
          <View style={{backgroundColor: "white", elevation: 6, paddingHorizontal: 15}}>
                <Text style={styles.headingText}>Order Status</Text>
                <Text style={styles.headingText}>Order Id: {item.orderId}</Text>
                {!self.state.isCancelledOrder 
                ?
                <>
                  <View style={styles.horizontalLine}/>
                  <StepIndicator
                    customStyles={customStyles}
                    currentPosition={self.state.currentPosition}
                    labels={labels}
                  />
                </>
                :
                 null
                }

                {self.state.shopLabels.length > 1 
                ?
                  <>
                    <View style={styles.horizontalSpaceLine} />
                    <StepIndicator
                      customStyles={customStyles}
                      currentPosition={self.state.shopCurrentPosition}
                      labels={self.state.shopLabels}
                      stepCount={self.state.shopLabels.length}
                    />
                  </>
                :
                  null
                }
                
            </View>
            <View style={{backgroundColor:"white",marginTop:15,paddingVertical: 15,paddingHorizontal:15, elevation: 6}}>
                <View style={{flex: 1, flexDirection:"row"}}>
                  <View style={{flex: 6}}>
                    <Text style={[s.headerText]}>Bill Details </Text>
                  </View>
                  {self.state.isPastOrder && item.invoiceURL?
                  <TouchableOpacity style={{flex: 6, alignItems: "flex-end"}} onPress={() => self.downloadInvoice(item.invoiceURL)}>
                    <Image style={{height: 30, width: 30}} source={require('../../../assets/download.png')}/>  
                  </TouchableOpacity> 
                  :
                  null
                  }
                </View>
                <View style={{paddingTop:10}}>
                    <View style={{flex: 1, flexDirection: "row",paddingVertical: 3}}>
                    <View style={{flex: 8}}>
                        <Text style={[s.normalText]} >Item Total</Text>
                    </View>
                    <View style={{flex:4,alignItems:"flex-end"}}>
                        <Text style={[s.normalText]} >₹{(item.order.totalCost).toFixed(2)} </Text>
                    </View>
                    </View>
                    <View style={{flex: 1, flexDirection: "row",paddingVertical: 3}}>
                    <View style={{flex: 8}}>
                        <Text style={[s.normalText]} >Delivery Fee</Text>
                    </View>
                    <View style={{flex:4,alignItems:"flex-end"}}>
                        <Text style={[s.normalText]} >₹{(item.order.deliveryCharge).toFixed(2)} </Text>
                    </View>
                    </View>
                    <View style={{paddingTop:10,borderBottomWidth: 1, borderBottomColor:"#7285A5"}}/>
                    <View style={{flex: 1, flexDirection: "row",paddingVertical: 3}}>
                        <View style={{flex: 8}}>
                        <Text style={[s.normalText]} >Coupon </Text>
                        </View>
                        <View style={{flex:4,alignItems:"flex-end"}}>
                        <Text style={[s.normalText]} >{item.order.coupon} </Text>
                        </View>
                    </View>
                    <View style={{flex: 1, flexDirection: "row",paddingVertical: 3}}>
                        <View style={{flex: 8}}>
                        <Text style={[s.normalText]} >Total discount </Text>
                        </View>
                        <View style={{flex:4,alignItems:"flex-end"}}>
                        <Text style={[s.normalText]} >₹{(item.order.totalDiscount).toFixed(2)} </Text>
                        </View>
                    </View>
                </View>
                {item.paymentStatus == 'COD' && item.order.finalCODAmountToPay
                ?
                <View style={{paddingTop:10,flexDirection: "row",flex:1}}>
                    <View style={{flex: 8}}>
                    <Text style={[s.normalText]} >Amount to pay</Text>
                    </View>
                    <View style={{flex:4,alignItems:"flex-end"}}>
                    <Text style={[s.normalText]} >₹{(item.order.finalCODAmountToPay).toFixed(2)} </Text>
                    </View>
                </View>
                :
                item.paymentStatus == 'COD'
                ?
                <View style={{paddingTop:10,flexDirection: "row",flex:1}}>
                    <View style={{flex: 8}}>
                    <Text style={[s.normalText]} >Amount to pay</Text>
                    </View>
                    <View style={{flex:4,alignItems:"flex-end"}}>
                    <Text style={[s.normalText]} >₹{(item.order.toPay).toFixed(2)} </Text>
                    </View>
                </View>
                :
                <View style={{paddingTop:10,flexDirection: "row",flex:1}}>
                    <View style={{flex: 8}}>
                    <Text style={[s.normalText]} >Paid amount</Text>
                    </View>
                    <View style={{flex:4,alignItems:"flex-end"}}>
                    <Text style={[s.normalText]} >₹{(item.order.toPay).toFixed(2)} </Text>
                    </View>
                </View>

                }
                
                <self.RefundAmount />
            </View>  
            <View style={{marginTop: 15,backgroundColor: "white", paddingHorizontal: 15, paddingVertical:15, elevation: 6}}>
                <View>
                    <Text style={s.headerText}>Delivery Address</Text>
                </View>
                <View style={{marginTop: 10}}>
                    <Text style={s.normalText}>Flat No: {item.order.deliveryAddress[0].flatNumber}, 
                        {item.order.deliveryAddress[0].addressLine1} </Text>
                    <Text style={s.normalText}>{item.order.deliveryAddress[0].addressLine2}</Text>    
                </View>
            </View> 
            <View style={{backgroundColor:"white",marginTop:15,paddingVertical: 15,paddingHorizontal:15, elevation: 6}}>
              
              <self.DeliveryManDetails />
              <View style={{marginTop: 10}}>
                <self.LargeQtyMessage />
              </View>
              
          </View>
        </View>
           
          {/* <View style={{alignItems:"center", marginVertical:'2%'}}> 
              <TouchableOpacity style={styles.addressButtonView} 
                  onPress={() => navigate('placedOrderDetails',{"orderDetails": this.state.orderData })}>
                  <Text style={styles.addressButtonText}>Order Details</Text>   
              </TouchableOpacity>
          </View>
          <View style={{alignItems:"center", marginVertical:'2%'}}> 
              <TouchableOpacity style={styles.addressButtonView} 
                  onPress={() => navigate('placedOrderDetails',{"orderDetails": this.state.orderData })}>
                  <Text style={styles.addressButtonText}>Order Details</Text>   
              </TouchableOpacity>
          </View> */}
          <self.BottomButtons />
                  
      </View>
  )
}

MapViewTag = () => {
  return (
    <View style={{flex: 1}}>
      <View style={styles.container}>
          <MapView
              style={styles.map}
              showUserLocation
              followUserLocation
              loadingEnabled
              region={this.getMapRegion()}
          >
              <Polyline coordinates={this.state.routeCoordinates} strokeWidth={3} />
              {/* <Marker.Animated
                  ref={marker => {
                  this.marker = marker;
                  }}
                  coordinate={this.state.coordinate}
                  image
              /> */}
              {/* <Marker 
                  coordinate={this.state.coordinate}
                  image={marker.logo}
              /> */}
              {this.state.markers.map(marker => (
                <Marker 
                  coordinate={marker.coordinate}
                  title={marker.title}
                  image={marker.logo}
                />
              ))

              }
          </MapView>
      </View>
      {/* <View style={styles.buttonContainer}>
          <View style={[styles.bubble, styles.button]}>
              <Text style={styles.bottomBarContent}>
              {parseFloat(this.state.distanceTravelled).toFixed(2)} km
              </Text>
          </View>
      </View> */}
    </View>
  )
  
}

class TrackOrderPage extends React.Component{
  interval2 = "abc";
  interval;
  constructor(props){
    super(props);    
    this.state = {
        currentPosition: 0,   
        shopCurrentPosition: 0, 
        sendActivity: false,
        getActivity: false,
        isOrder: false, 
        listView: false,
        detailsView: false, 
        isOrderConfirm: false,
        listView: false,
        detailsView: true,
        orderData: [],
        getActivityData: [],
        SendActivityData: [],
        showLoader: false,
        isPastOrder: false,
        isCancelledOrder: false,
        deliveryBoyDetails: {},
        isLargeQuantity: false,
        isDriverCancel: false,
        modalVisible: false,
        cancelOrderId: '',
        isProductsUnavailable: false,
        refundAmount: 0.00,
        latitude: LATITUDE,
        longitude: LONGITUDE,
        routeCoordinates: [],
        distanceTravelled: 0,
        prevLatLng: {},
        markers: [],
        coordinate: {
          latitude: LATITUDE,
          longitude: LONGITUDE, 
        },
        shopLabels: [],
        isDeliveryBoyAssigned: false,
    }
    
    // console.warn("plave cart ---->", this.props.cartItem.placedCart);
  }
  

  componentDidMount() {
    
      if (this.props.navigation.state.params) {
      
        if(this.props.navigation.state.params.action == 'sendActivity') {
          if (this.props.navigation.state.params.orderId != '') {
            this.getDataOfSpecificId('sendActivity', this.props.navigation.state.params.orderId, true);
          } else {
            this.setState({
              sendActivity: true
            })
            this.callAPI(true, 'sendActivity');
          }
          
        } else if (this.props.navigation.state.params.action == 'getActivity') {
          if (this.props.navigation.state.params.orderId != '') {
            this.getDataOfSpecificId('getActivity', this.props.navigation.state.params.orderId, true);
          } else {
            this.setState({
              getActivity: true
            })
            this.callAPI(true, 'getActivity');
          }
        } else if (this.props.navigation.state.params.action == 'order') {
          if (this.props.navigation.state.params.orderId != '') {
            this.getDataOfSpecificId('order', this.props.navigation.state.params.orderId, false);
          } else {
            console.warn('Rvaiiii');
            this.setState({
              isOrder: true
            })
            this.callAPI(false, 'order');
          } 
        }
      }

      // this.watchLocation();
      
    }

    // watchLocation = () => {
    //   this.watchID = Geolocation.watchPosition(
    //     position => {
    //       const { coordinate, routeCoordinates, distanceTravelled } =   this.state;
    //       const { latitude, longitude } = position.coords;
          
    //       const newCoordinate = {
    //           latitude,
    //           longitude
    //       };
    //       if (Platform.OS === "android") {
    //           console.warn('change-->', newCoordinate);
    //           if (this.marker) {
    //           this.marker._component.animateMarkerToCoordinate(
    //               newCoordinate,
    //               500
    //           );
    //           }
    //       } else {
    //           coordinate.timing(newCoordinate).start();
    //       }
    //       this.setState({
    //           latitude,
    //           longitude,
    //           coordinate: newCoordinate,
    //           routeCoordinates: this.state.routeCoordinates.concat([newCoordinate]),
    //           distanceTravelled: this.state.distanceTravelled + this.calcDistance(newCoordinate),
    //           prevLatLng: newCoordinate
    //       });

    //       console.warn('routeCoordinates-->', this.state.routeCoordinates);
    //     },
    //     error => console.log(error),
    //     { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
    //   );
    // };
  
    

  callAPI = (isActivity, activityType) => {
    console.warn('call API called', activityType);
    axios.post(
      Config.URL + Config.getOrderDetailsUser,
      {
        "userId": this.props.userData.userData.userId,
        "activity": isActivity
      },
      {
        headers: {
            'Content-Type': 'application/json',
        }
      }
    )
    .then(res => {
        // console.warn('---- res from send -->', res.data.result.currentSendActivityOrderDetails);
        // console.warn('---- res from  get -->', res.data.result.currentGetActivityOrderDetails);
        console.warn('---- res from -->', res.data);
        if (res.data.status == 0) {
          
          if (activityType == 'sendActivity') {
            if (res.data.result.currentSendActivityOrderDetails.length == 1) {
              this.setState({
                showLoader: false
              })
              this.getDataOfSpecificId(activityType, res.data.result.currentSendActivityOrderDetails[0].orderId, true);
            } else {
              this.setState({
                sendActivityData: res.data.result.currentSendActivityOrderDetails,
                listView: true,
                detailsView: false,
                sendActivity: true,
                showLoader: false
              })
            }
            
          } else if (activityType == 'getActivity') {
            if (res.data.result.currentGetActivityOrderDetails.length == 1) {
              this.setState({
                showLoader: false
              })
              this.getDataOfSpecificId(activityType, res.data.result.currentGetActivityOrderDetails[0].orderId, true);
            } else {
              this.setState({
                getActivityData: res.data.result.currentGetActivityOrderDetails,
                listView: true,
                detailsView: false,
                getActivity: true,
                showLoader: false
              })
            }
            
          } else {
            if (res.data.result.orderDetails.length == 1) {
              this.setState({
                showLoader: false
              })
              this.getDataOfSpecificId(activityType, res.data.result.orderDetails[0].orderId, false);
            } else {
              this.setState({
                orderData: res.data.result.orderDetails,
                listView: true,
                detailsView: false,
                isOrder: true,
                showLoader: false
              })
            }
          }
          
        } else {
          Toast.show('Failed to fetch the order status. Please try again',Toast.LONG);
          console.warn('error',res)
          this.setState({
            showLoader: false
          })
        }

    })
  }

  getDataOfSpecificId = (activityType, id, isActivity) => {
    this.getDataOfSpecificIdDetails(activityType, id, isActivity);
    this.interval = setInterval(async () => {
      this.getDataOfSpecificIdDetails(activityType, id, isActivity)
    }, 20000);
  }

  getDataOfSpecificIdDetails = (activityType, id, isActivity) => {
    // console.warn('id in specific fun', id);
    axios.post(
      Config.URL + Config.getOrderDetailsUser,
      {
        "userId": this.props.userData.userData.userId,
        "activity": isActivity,
        "orderId": id
      },
      {
        headers: {
            'Content-Type': 'application/json',
        }
      }
    )
    .then(async(res) => {
        // console.warn('---- res from of specific id -->', res.data.result);
        // console.warn('order status---->  ', res.data.result);
        console.warn('delivery boy details-========-->', res.data.result.orderDetails[0].orderStatus);
        
        if (res.data.status == 0) {
          switch(res.data.result.orderDetails[0].orderStatus) {
            case 'Pending': 
                          this.setState({
                            currentPosition: 0
                          })
                          break;
            case 'Confirmed': 
                            this.setState({
                              currentPosition: 1,
                              isOrderConfirm: false
                            })
                            break;
            case 'Shipped'  : 
                            this.setState({
                              currentPosition: 2,
                              isOrderConfirm: true
                            })
                            break;
            case 'Out for Delivery': 
                            this.setState({
                              currentPosition: 3,
                              isOrderConfirm: true
                            })
                            break;
            case 'Delivered':
                            this.markDelivered();
                            break;
            case 'Cancelled':
                            this.setState({
                              isCancelledOrder: true,
                              cancelMsg: 'You cancelled this order'
                            });
                            const i = this.props.driverLocation.driverLocation.findIndex(ind => ind.orderId === res.data.result.orderDetails[0].orderId);
                            if (i >= 0) {
                              this.props.driverLocation.driverLocation.splice(i, 1);
                              this.props.driverLocationAction.replaceLocation(this.props.driverLocation.driverLocation);
                            }
                            break;
            case 'Rejected':
                            this.setState({
                              isCancelledOrder: true,
                              cancelMsg: 'Order is rejected by shop'
                            });
                            const j = this.props.driverLocation.driverLocation.findIndex(ind => ind.orderId === res.data.result.orderDetails[0].orderId);
                            if (j >= 0) {
                              this.props.driverLocation.driverLocation.splice(i, 1);
                              this.props.driverLocationAction.replaceLocation(this.props.driverLocation.driverLocation);
                            }
                            break;
          }

          if (res.data.result.orderDetails[0].shopOrderStatus) {
            const shopData = res.data.result.orderDetails[0].shopOrderStatus;
            // console.warn('shopOrderStatus---->', shopData);
            let temp = [];
            let count = 0;
            if (shopData.length > 0) {
              for (let i = 0; i < shopData.length; i++) {
                console.warn('inside loop--->', i, shopData[i]);
                if (shopData[i].status == 'Confirmed' || shopData[i].status == 'Rejected' || shopData[i].status == 'Partially Confirmed') {
                  count++;
                  if (shopData[i].status == 'Confirmed' || shopData[i].status == 'Partially Confirmed') {
                    temp.push(shopData[i].shopName + ' (Approved)');
                  } else {
                    temp.push(shopData[i].shopName + ' (Rejected)');
                    // console.warn('shopData.length --->', labels);
                    // if (shopData.length == 1) {
                    //   labels = [shopData[i].shopName + ' (Rejected)'];
                    // }
                  }
                }
              }
              for (let i = 0; i < shopData.length; i++) {
                if (shopData[i].status == 'Pending') {
                  count++;
                  temp.push(shopData[i].shopName + ' (Pending)'); 
                }
                // console.warn('am here');
                if (shopData[i].shopName != 'Flash Mart') {
                  const tempM = {
                    title: shopData[i].shopName,
                    coordinate: {
                      latitude: shopData[i].shopLocation.latitude,
                      longitude: shopData[i].shopLocation.longitude,
                    },
                    logo: require('../../../assets/DeliveryShop.png')
                  }
                  // console.warn('test this one', tempM);
                  this.state.markers.push(tempM);
                  await this.setState({
                    markers: this.state.markers
                  });
                }
                
              }
            }
            this.setState({shopLabels: temp, shopCurrentPosition: count});
            // console.warn('shopLabels array===>', temp);
            // console.warn('shopCurrentPosition array===>', count);
          }
         console.warn('isProDuctUnva------------------------------->',res.data.result.orderDetails[0]);
          if(res.data.result.orderDetails[0].refundAmount > 0) {
            console.warn('inside unavailable condition====================>', res.data.result.orderDetails[0].refundAmount);
            this.setState({refundAmount: res.data.result.orderDetails[0].refundAmount});
          }
          console.warn('test========', res.data.result.orderDetails[0].isAssignForDelivery);
          if (res.data.result.orderDetails[0].isAssignForDelivery && res.data.result.orderDetails[0].orderStatus != 'Delivered') {
            this.setState({ 
              deliveryBoyDetails: res.data.result.orderDetails[0].deliveryBoyDetails,
              isDeliveryBoyAssigned: true
             });
            console.warn('insid is confirm-->', res.data.result.orderDetails[0].deliveryBoyDetails.location);
            const tempLatLong = {
              latitude: res.data.result.orderDetails[0].deliveryBoyDetails.location.latitude,
              longitude: res.data.result.orderDetails[0].deliveryBoyDetails.location.longitude 
            };
            const tempArray = {
              orderId: res.data.result.orderDetails[0].orderId,
              coordinates: [tempLatLong]
            }
            const markerTemp = {
              title: 'Delivery Boy',
              coordinate: tempLatLong,
              logo: require('../../../assets/logobiker.png')
            }
            const index = this.state.markers.findIndex(ind => ind.logo === require('../../../assets/logobiker.png'));
            if (index < 0) {
              this.state.markers.push(markerTemp);
              await this.setState({ markers: this.state.markers });
            } else {
              this.state.markers[index] = markerTemp;
              await this.setState({ markers: this.state.markers });
            }
            
            console.warn('driverLocation', this.props.driverLocation);
            console.warn('driverLocation', this.props.driverLocation);
            

            const i = this.props.driverLocation.driverLocation.findIndex(ind => ind.orderId === res.data.result.orderDetails[0].orderId);
            if (i >= 0 ) {
              let cArray = [...this.props.driverLocation.driverLocation[i].coordinates];
              console.warn('cArray', cArray);
              
              console.warn('tempLatLong', tempLatLong);
              cArray.push(tempLatLong);
              this.props.driverLocation.driverLocation[i].coordinates = cArray;
              
              await this.props.driverLocationAction.replaceLocation(this.props.driverLocation.driverLocation);
              console.warn('after concat in fist if', this.props.driverLocation.driverLocation[i]);
              await this.setState ({
                routeCoordinates: this.props.driverLocation.driverLocation[i].coordinates
              });
            } else {
              console.warn('in else condition');
              await this.props.driverLocationAction.addLocation(tempArray);
              await this.setState ({
                routeCoordinates: [tempLatLong]
              });
            }
            await this.setState({
              latitude: res.data.result.orderDetails[0].deliveryBoyDetails.location.latitude,
              longitude: res.data.result.orderDetails[0].deliveryBoyDetails.location.longitude,
              isOrderConfirm: true,
              coordinate: tempLatLong,
              // routeCoordinates: this.props.driverLocation.driverLocation[i].coordinates,
            });

            console.warn('test 22 routeCoordinates', this.state.routeCoordinates);
            console.warn('driver location', this.props.driverLocation.driverLocation);
          }
          if (activityType == 'sendActivity') {
            await this.setState({
              sendActivityData: res.data.result.orderDetails,
              deliveryBoyDetails: res.data.result.orderDetails[0].deliveryBoyDetails,
              listView: false,
              detailsView: true,
              sendActivity: true,
              showLoader: false,
            });
            const tempMarkerPick = {
              title: 'Pickup location',
              coordinate: {
                latitude: res.data.result.orderDetails[0].order.activityDetails[0].pickupAddress.position.latitude,
                longitude: res.data.result.orderDetails[0].order.activityDetails[0].pickupAddress.position.longitude,
              },
              logo: require('../../../assets/LogoMarker.png')
            }
            this.state.markers.push(tempMarkerPick);
            await this.setState({ 
              markers: this.state.markers,
              latitude: res.data.result.orderDetails[0].order.activityDetails[0].pickupAddress.position.latitude,
              longitude: res.data.result.orderDetails[0].order.activityDetails[0].pickupAddress.position.longitude,
            });
            const tempMarkerD = {
              title: 'Delivery Location',
              coordinate: {
                latitude: res.data.result.orderDetails[0].order.activityDetails[0].deliveryAddress.position.latitude,
                longitude: res.data.result.orderDetails[0].order.activityDetails[0].deliveryAddress.position.longitude,
              },
              logo: require('../../../assets/LogoMarker.png')
            }
            this.state.markers.push(tempMarkerD);
            await this.setState({ 
              markers: this.state.markers,
              latitude: res.data.result.orderDetails[0].order.activityDetails[0].deliveryAddress.position.latitude,
              longitude: res.data.result.orderDetails[0].order.activityDetails[0].deliveryAddress.position.longitude,
            });

            console.warn('SendActivity===>', res.data.result.orderDetails[0].order.activityDetails);
            console.warn('Markers ===>', this.state.markers);
          } else if (activityType == 'getActivity') {
           await this.setState({
              getActivityData: res.data.result.orderDetails,
              deliveryBoyDetails: res.data.result.orderDetails[0].deliveryBoyDetails,
              listView: false,
              detailsView: true,
              getActivity: true,
              showLoader: false
            })
            console.warn('Get activity===>', res.data.result.orderDetails[0].order.activityDetails);
            console.warn('Get activity pAdd===>', res.data.result.orderDetails[0].order.pickupAddress);
            for (let i = 0; i < res.data.result.orderDetails[0].order.activityDetails.length; i++) {
              const temp = {
                title: res.data.result.orderDetails[0].order.activityDetails[i].activityTag + ' delivery location',
                coordinate: {
                  latitude: res.data.result.orderDetails[0].order.activityDetails[i].deliveryAddress.position.latitude,
                  longitude: res.data.result.orderDetails[0].order.activityDetails[i].deliveryAddress.position.longitude,
                },
                logo: require('../../../assets/LogoMarker.png')
              }
              this.state.markers.push(temp);
              await this.setState({ markers: this.state.markers });
            }

            const tempMarker = {
              title: 'Pickup location',
              coordinate: {
                latitude: res.data.result.orderDetails[0].order.pickupAddress.position.latitude,
                longitude: res.data.result.orderDetails[0].order.pickupAddress.position.longitude,
              },
              logo: require('../../../assets/DeliveryShop.png')
            }
            this.state.markers.push(tempMarker);
            await this.setState({ 
              markers: this.state.markers,
              latitude: res.data.result.orderDetails[0].order.pickupAddress.position.latitude,
              longitude: res.data.result.orderDetails[0].order.pickupAddress.position.longitude,
            });
          } else {
            console.warn('order details=====', res.data.result.orderDetails[0].order.deliveryAddress[0].position);
            await this.setState({
              orderData: res.data.result.orderDetails,
              deliveryBoyDetails: res.data.result.orderDetails[0].deliveryBoyDetails,
              listView: false,
              detailsView: true,
              isOrder: true,
              showLoader: false,
              isLargeQuantity: res.data.result.orderDetails[0].isLargeQty,
              isDriverCancel: false,
              latitude: res.data.result.orderDetails[0].order.deliveryAddress[0].position.latitude,
              longitude: res.data.result.orderDetails[0].order.deliveryAddress[0].position.longitude
            });

            const tempMarker = {
              title: 'Delivery location',
              coordinate: {
                latitude: res.data.result.orderDetails[0].order.deliveryAddress[0].position.latitude,
                longitude: res.data.result.orderDetails[0].order.deliveryAddress[0].position.longitude
              },
              logo: require('../../../assets/logoHome.png')
            }
            this.state.markers.push(tempMarker);
            console.warn('final markers array===>', this.state.markers);
            await this.setState({ markers:  this.state.markers})
          }
          
        } else {
          Toast.show('Failed to fetch the order status. Please try again',Toast.LONG);
          console.warn('error',res)
          this.setState({
            showLoader: false
          })

        }
        
    })
  }

  markDelivered = () => {
    this.setState({
      currentPosition: 4,
      isOrderConfirm: true,
      isPastOrder: true,
      routeCoordinates: []
    });
    this.props.driverLocationAction.replaceLocation([]);
    this.props.cartModalAction.changeOrderState(false);  
    this.props.cartModalAction.changeGetActivityState(false);
    this.props.cartModalAction.changeSendActivityState(false);
  }
  

  componentWillUnmount() {
    console.warn('in will unmount', this.interval);
    clearInterval(this.interval);
  }

BottomButtons = () => {
  const {navigate} = this.props.navigation;
  if (this.state.isPastOrder || this.state.isCancelledOrder) {
    return (
      <View style={{flexDirection:"row", backgroundColor: "white", elevation: 6}}>
        {/* <TouchableOpacity style={{flex:6,backgroundColor:"#F5F5F5",alignItems:"center",justifyContent:"center", paddingVertical: 10}}
          onPress={() => this.cancelOrder()}>
          <Text style={{fontSize: 16,fontWeight: "bold",color:"#131E3A"}}>Cancel order</Text>
        </TouchableOpacity> */}
        <TouchableOpacity style={{flex:1,backgroundColor:"#00CCFF",alignItems:"center",justifyContent:"center", paddingVertical: 10}}
            onPress={() => navigate('placedOrderDetails',{"orderDetails": this.state.orderData[0], refundAmount: this.state.refundAmount })}>
          <Text style={[s.headerText,{color:"white"}]}>More Details</Text>
        </TouchableOpacity>
      </View>
    )
  } else {
    return (
      <View style={{flexDirection:"row", backgroundColor: "white", elevation: 6}}>
        <TouchableOpacity style={{flex:6,backgroundColor:"#F5F5F5",alignItems:"center",justifyContent:"center", paddingVertical: 10}}
          onPress={() => this.cancelOrder()}>
          <Text style={{fontSize: 16,fontWeight: "bold",color:"#131E3A"}}>Cancel order</Text>
        </TouchableOpacity>
        <TouchableOpacity style={{flex:6,backgroundColor:"#00CCFF",alignItems:"center",justifyContent:"center", paddingVertical: 10}}
            onPress={() => navigate('placedOrderDetails',{"orderDetails": this.state.orderData[0], refundAmount: this.state.refundAmount })}>
          <Text style={[s.headerText,{color:"white"}]}>More Details</Text>
        </TouchableOpacity>
      </View>
    )
  }
  
}

cancelOrder = () => {
  console.warn('in cancel order-->', this.state.orderData[0].orderId);
  if (this.state.isDriverCancel) {
    console.warn('cancel order api call', this.state.orderData[0].orderId);
    this.setState({
      cancelOrderId: this.state.orderData[0].orderId
    })
    this.confirmCancelOrder();
  } else {
    this.setState({
      cancelOrderId: this.state.orderData[0].orderId
    })
    console.warn('modalVisible true');
    this.setState({
      modalVisible: true
    })
  }
}

hideModal = () => {
  this.setState({
    modalVisible: false
  });
}


confirmCancelOrder = () => {
  this.setState({
    modalVisible: false,
    showLoader: true
  });
  console.warn('orderID-->', this.state.cancelOrderId);
  console.warn('userId-->', this.props.userData.userData.userId);
  
  axios.post(
    Config.URL + Config.cancelOrder,
    {
      "orderId": this.state.cancelOrderId,
      "userId": this.props.userData.userData.userId,
    },
    {
      headers: {
          'Content-Type': 'application/json',
      }
    }
  )
  .then(res => {
    console.warn('response--->', res);
    this.setState({ showLoader: false });
    if (res.data.status == 0) {
      this.props.cartModalAction.changeOrderState(false); 
      this.props.walletAction.updateWallet(res.data.wallet,true);
      Toast.show('Your order is cancelled successfully. Amount of Rs ' + res.data.wallet + ' is added in your wallet',Toast.LONG);
      this.props.navigation.navigate('homePage');
    } else {
        this.setState({ showLoader: false });
        console.warn('error')
    }
  })
  
}


RefundAmount = () => {
  if (this.state.refundAmount > 0) {
    return (
      <View style={{paddingTop:10,flexDirection: "row",flex:1}}>
          <View style={{flex: 8}}>
            <Text style={[s.normalText]} >Refund Amount</Text>
          </View>
          <View style={{flex:4,alignItems:"flex-end"}}>
            <Text style={[s.normalText]} >₹{this.state.refundAmount} </Text>
          </View>
      </View>
    )
  } else {
    return null;
  }
}


DeliveryManDetails = () => {
  if (this.state.isDeliveryBoyAssigned) {
    return (
        <View>
          <View>
              <Text style={s.headerText}>Delivery man details </Text>
          </View>
          
          <View style={{marginTop: 10}}>
              <Text style={s.subHeaderText}>Name:</Text>
              <Text style={s.normalText}>{this.state.deliveryBoyDetails.fullName}</Text>
            </View>
            {/* <View style={{marginTop: 10}}>
              <Text style={s.subHeaderText}>Mobile number:</Text>
              <Text style={s.normalText}>{this.state.deliveryBoyDetails.contactNumber}</Text>
          </View> */}
          <View style={{marginTop: 10}}>
          <Text style={s.subHeaderText}>Mobile No:</Text>
          <TouchableOpacity style={{flex: 1, flexDirection: "row"}} onPress={() => this.navigateToCall(this.state.deliveryBoyDetails.contactNumber)} >
            <Image style={{width: 20, height: 20}} source={require('../../../assets/call.png')}/>
            <Text style={[s.normalText, {paddingLeft: 15, color:"#00CCFF"}]}>{this.state.deliveryBoyDetails.contactNumber}</Text>  
          </TouchableOpacity>
        </View> 
          <View style={{marginTop: 10}}>
            <Text style={s.normalText}>Regarding any order specific changes, please feel free to reach out to any of our delivery executive or support team. Order amount may vary as per the changes.</Text>
          </View>
        </View>
      )
  } else if (this.state.isPastOrder || this.state.isCancelledOrder) {
    return null;
  }
  else {
    return (
      <View>
        <View style={{marginTop: 10}}>
            <Text style={s.normalText} numberOfLines={6}>Our delivery executive will be assigned shortly, once your order is confirmed. 
              Feel free to reach out to our delivery executive or our support team:</Text>
        </View>
        <View style={{marginTop: 10}}>
            <Text style={s.subHeaderText}>Flash Support:</Text>
          </View>
          <View style={{marginTop: 10}}>
            <Text style={s.subHeaderText}>Email Id:</Text>
            <Text style={s.normalText}>info@flashit.co.in</Text>
          </View>
          {/* <View style={{marginTop: 10}}>
            <Text style={s.subHeaderText}>Mobile number:</Text>
            <Text style={s.normalText}>+91 73043 41660</Text>
          </View> */}
          <View style={{marginTop: 10}}>
            <Text style={s.subHeaderText}>Mobile No:</Text>
            <TouchableOpacity style={{flex: 1, flexDirection: "row"}} onPress={() => this.navigateToCall('73043 41660')} >
              <Image style={{width: 20, height: 20}} source={require('../../../assets/call.png')}/>
              <Text style={[s.normalText, {paddingLeft: 15, color:"#00CCFF"}]}>+91 73043 41660</Text>  
            </TouchableOpacity>
        </View> 
          

      </View>
    )
  }
  
}

LargeQtyMessage = () => {
  if (this.state.isLargeQuantity) {
    return (
        <View>
          <View style={{marginTop: 10}}>
            <Text style={s.normalText} numberOfLines={6}>Due to the large quantity, your order will be served in next 2-3 hrs</Text>
          </View>
        </View>
      )
  } else {
    return null;
  }
  
}

navigateToCall = (phoneNumber) => {
  Linking.canOpenURL(`tel:${phoneNumber}`)
  .then(supported => {
    if (!supported) {
      Toast.show('Sorry..We not able to redirect you on call log.',Toast.LONG);
    } else {
      return Linking.openURL(`tel:${phoneNumber}`)
    }
  })
}

downloadInvoice = (url) => {
  Linking.openURL(url).catch((err) =>
    Toast.show('Not able to download invoice. Please contact support team',Toast.LONG));
}

getMapRegion = () => ({
  latitude: this.state.latitude,
  longitude: this.state.longitude,
  latitudeDelta: LATITUDE_DELTA,
  longitudeDelta: LONGITUDE_DELTA
});


OrderDetails = () => {
  if(this.state.sendActivity) {
    if(this.state.listView) {
      return (
        <View>
          <View style={styles.mapContainer}>
            <Image style={styles.mapImage} source={require('../../../assets/map.jpg')}/>
          </View>
          <View style={{marginTop: 15, paddingHorizontal: 15}}>
            <Text style={s.headerText}>Your placed activities:</Text>
          </View>
          <FlatList
            data={this.state.sendActivityData}
            renderItem={( { item } ) => (
            <SendActivityListViewTrack
                item={item}
                self={this}
            />
            )}
            extraData={this.state.sendActivityData} 
          />
        </View>
      )
    } else if (this.state.detailsView) {
      return (
        <View style={{flex: 1}}>
          <View style={{flex: 6, height: height/2}}>
            <View style={styles.container}>
              <MapView
                style={styles.map}
                showUserLocation
                followUserLocation
                loadingEnabled
                region={this.getMapRegion()}
              >
                  <Polyline coordinates={this.state.routeCoordinates} strokeWidth={2} />
                  {/* <Marker.Animated
                      ref={marker => {
                      this.marker = marker;
                      }}
                      coordinate={this.state.coordinate}
                      image
                  /> */}
                  {this.state.markers.map(marker => (
                    <Marker 
                      coordinate={marker.coordinate}
                      title={marker.title}
                      image={marker.logo}
                    />
                  ))

                  }
                  {/* <Marker 
                      coordinate={this.state.coordinate}
                      image={marker.logo}
                  /> */}
              </MapView>
            </View>
            
          </View>
          <View style={{flex: 6}}>
            <FlatList
              data={this.state.sendActivityData}
              renderItem={( { item } ) => (
              <SendActivityListTrack
                  item={item}
                  self={this}
              />
              )}
              extraData={this.state.sendActivityData} 
            />
          </View>
          
        </View>
      )
    } else {
      return null;
    }
  } else if (this.state.getActivity) {
    if (this.state.listView) {
      return (
        <View>
          <View style={styles.mapContainer}>
            <Image style={styles.mapImage} source={require('../../../assets/map.jpg')}/>
          </View>
          <View style={{marginTop: 15, paddingHorizontal: 15}}>
            <Text style={s.headerText}>Your placed activities:</Text>
          </View>
          <FlatList
            data={this.state.getActivityData}
            renderItem={( { item } ) => (
            <GetActivityListViewTrack
                item={item}
                self={this}
            />
            )}
            extraData={this.state.getActivityData} 
          />
        </View>
      )  
    } else if (this.state.detailsView) {
      return (
        <View style={{flex: 1}}>
          <View style={{flex: 6, height: height/2}}>
            <View style={styles.container}>
              <MapView
                style={styles.map}
                showUserLocation
                followUserLocation
                loadingEnabled
                region={this.getMapRegion()}
              >
                  <Polyline coordinates={this.state.routeCoordinates} strokeWidth={2} />
                  {/* <Marker.Animated
                      ref={marker => {
                      this.marker = marker;
                      }}
                      coordinate={this.state.coordinate}
                      image
                  /> */}
                  {this.state.markers.map(marker => (
                    <Marker 
                      coordinate={marker.coordinate}
                      title={marker.title}
                      image={marker.logo}
                    />
                  ))

                  }
                  {/* <Marker 
                      coordinate={this.state.coordinate}
                      image={marker.logo}
                  /> */}
              </MapView>
            </View>
            
          </View>
          <View style={{flex: 6}}>
            <FlatList
              data={this.state.getActivityData}
              renderItem={( { item } ) => (
              <GetActivityListTrack
                  item={item}
                  self={this}
              />
              )}
              extraData={this.state.getActivityData} 
            />
          </View>
          
        </View>
      )
    } else {
      return null;
    }
  } else if (this.state.isOrder) {
    if(this.state.listView) {
      return (
        <View>
          <View style={styles.mapContainer}>
            <Image style={styles.mapImage} source={require('../../../assets/map.jpg')}/>
          </View>
          <View style={{marginTop: 15, paddingHorizontal: 15}}>
            <Text style={s.headerText}>Your placed orders:</Text>
          </View>
          <FlatList
            data={this.state.orderData}
            renderItem={( { item } ) => (
            <OrderListViewTrack
                item={item}
                self={this}
            />
            )}
            extraData={this.state.orderData} 
          />
        </View>
      )  
    } else if (this.state.detailsView) {
      return (
        <View style={{flex: 1}}>
          <View style={{flex: 6, height: height/2}}>
          <View style={{flex: 1}}>
            <View style={styles.container}>
                <MapView
                    style={styles.map}
                    showUserLocation
                    followUserLocation
                    loadingEnabled
                    region={this.getMapRegion()}
                >
                    <Polyline coordinates={this.state.routeCoordinates} strokeWidth={2} />
                    {/* <Marker.Animated
                        ref={marker => {
                        this.marker = marker;
                        }}
                        coordinate={this.state.coordinate}
                        image
                    /> */}
                    {this.state.markers.map(marker => (
                      <Marker 
                        coordinate={marker.coordinate}
                        title={marker.title}
                        image={marker.logo}
                      />
                    ))

                    }
                    {/* <Marker 
                        coordinate={this.state.coordinate}
                        image={marker.logo}
                    /> */}
                </MapView>
            </View>
            {/* <View style={styles.buttonContainer}>
                <View style={[stylses.bubble, styles.button]}>
                    <Text style={styles.bottomBarContent}>
                    {parseFloat(this.state.distanceTravelled).toFixed(2)} km
                    </Text>
                </View>
            </View> */}
          </View>
          </View>
          <View style={{flex: 6}}>
            <FlatList
              data={this.state.orderData}
              renderItem={( { item } ) => (
              <OrderListTrack
                item={item}
                self={this}
              />
              )}
              extraData={this.state.orderData} 
            />
          </View>
          
        </View>
      )
    } else {
      return null;
    }
    
  } else {
    return null;
  }
}



  render(){
    const {navigate} = this.props.navigation;
    // if (this.state.isDriverCancel) {
    //   return (
    //     <View style={styles.cointainer}>
    //       <View style={{paddingHorizontal: 15, backgroundColor: "white", elevation: 6}}>
    //         <Text style={s.normalText} numberOfLines={6}>Currently we are serving only in Navi Mumbai region. The delivery address you have given is 
    //         is not in Navi Mumbai. So, requesting you to cancel your order by clicking below button. We will refund your amount without 
    //         any deduction in 1-2 business days</Text>
    //       </View>
    //     </View>
    //   )
    // }
    if (this.state.isPastOrder) {
      return (
        <SafeAreaView style={{flex: 1}}>
          <View style={{flex: 1}}>
            
            <ScrollView style={styles.cointainer} showsVerticalScrollIndicator={false}>
              {/* <InternateModal /> */}
              <AnimatedLoader
                visible={this.state.showLoader}
                overlayColor="rgba(255,255,255,0.75)"
                source={require("../../../assets/loader.json")}
                animationStyle= {[styles.lottie,{height: 150,width: 150}]}
                speed={1}
              />
              {/* <SceneLoader
                visible={this.state.showLoader}
                animation={{
                    fade: {timing: {duration: 1000 }}
                }}
              /> */}
              <View style={{paddingHorizontal: 15, paddingVertical: 20, alignItems: "center"}}>
                <Text style={s.headerText}> Your order is Delivered successfully!!</Text>
              </View>
              <View style={styles.bodyContainerMain}>
                  <View>
                      <this.OrderDetails />
                  </View>
              </View>
            
            </ScrollView>   
          </View>
  
        </SafeAreaView>

        
      );
    } else if (this.state.isCancelledOrder){
      return (
        <SafeAreaView style={{flex: 1}}>
          <View style={{flex: 1}}>
            
            <ScrollView style={styles.cointainer} showsVerticalScrollIndicator={false}>
              {/* <InternateModal /> */}
              {/* <AnimatedLoader
                visible={this.state.showLoader}
                overlayColor="rgba(255,255,255,0.75)"
                source={require("../../../assets/loader.json")}
                animationStyle= {[styles.lottie,{height: 150,width: 150}]}
                speed={1}
              /> */}
              <View style={{paddingHorizontal: 15, paddingVertical: 20, alignItems: "center"}}>
                <Text style={s.headerText}>{this.state.cancelMsg}</Text>
              </View>
              <View style={styles.bodyContainerMain}>
                  <View>
                      <this.OrderDetails />
                  </View>
              </View>
            
            </ScrollView>   
          </View>
      
        </SafeAreaView>
      
    ); 
    } else {
      return (
        <SafeAreaView style={{flex: 1}}>
          <View style={{flex: 1}}>
            <PopupModal visible={this.state.modalVisible} onDismiss={this.hideModal} removeProduct={this.confirmCancelOrder} 
                message={"Delivery charges will be charged as cancellation fee on cancelling an order. Do you want to continue with the cancellation process? Need help? Feel free to reach out to our support team. "} btnTag={"Confirm"}/>
            <ScrollView showsVerticalScrollIndicator={false}>
              {/* <InternateModal /> */}
              {/* <AnimatedLoader
                  visible={this.state.showLoader}
                  overlayColor="rgba(255,255,255,0.75)"
                  source={require("../../../assets/loader.json")}
                  animationStyle= {[styles.lottie,{height: 150,width: 150}]}
                  speed={1}
                /> */}
                
              {/* <View style={styles.mapContainer}>
                <Image style={styles.mapImage} source={require('../../../assets/map.jpg')}/>
              </View> */}
              <this.OrderDetails />
            </ScrollView>   
          </View>
            
        </SafeAreaView>
      
    );
    }
    
  }

}


let ScreenWidth = Dimensions.get("window").width;
let ScreenHeight = Dimensions.get("window").height;

const styles = StyleSheet.create({
    container: {
      ...StyleSheet.absoluteFillObject,
      justifyContent: "flex-end",
      alignItems: "center"
    },
    map: {
      ...StyleSheet.absoluteFillObject
    },
    // cointainer:{
    //   flex : 1
    // },
    mapContainer:{
      flex : 1
    },
    bodyContainer:{
      flex : 1,
    },
    mapImage:{
      width : width,
      height: 340
    },
    totalAmountLine:{
      borderBottomColor: '#E0E0E0',
      borderBottomWidth: 1 
    },
    activeListingView: {
      marginVertical: 10,
      backgroundColor: "white",
      padding: 10,
      elevation: 6,
      flex: 1,
      marginHorizontal: 15,
      marginVertical: 5,
    },
    horizontalLine:{
      borderBottomColor: '#131E3A',
      borderBottomWidth: 3,
      marginBottom:20,
      marginTop: 5

    },
    horizontalSpaceLine:{
      borderBottomColor: '#E0E0E0',
      borderBottomWidth: 1,
      marginTop:15,
      marginBottom:15,
    },
    headingText:{
      textAlign: "center",
      color: "black",
      fontWeight : "bold",
      fontFamily: "verdana",
      fontSize: 20,
      marginTop:10,   
      color: "#131E3A"     
    },
    subHeadingText:{
      textAlign: "left",
      color: "black",
      fontFamily: "verdana",
      fontSize: 13,
      fontWeight : "bold",
      fontSize: 15,
    },
    text:{
      textAlign: "left",
      color: "black",
      fontFamily: "verdana",
      fontSize: 13,
    },
    textCenter:{
      textAlign: "center",
      color: "black",
      fontFamily: "verdana",
      fontSize: 13,
    },
    button:{
      marginTop:20,
      marginBottom:20
    },
    bodyLeftRightMargin:{
      marginLeft:15,
      marginRight:15
    },
    addressButtonView: {
		backgroundColor:"#00CCFF",  
		width: '80%',
		//borderRadius: 50,
		elevation: 6,
		shadowColor: 'rgba(0, 0, 0, 0.1)',
		shadowOpacity: 0.8,
		height: 40,
		justifyContent: 'center',
		alignItems: 'center',
	},
	addressButtonText: {
		color:"white",  
		fontSize: 16 ,
		fontWeight:"bold"
	},
  });
//--------------------------------------------------- store related fuction  -- Start

const mapStateToProps = (state) => {
	return {
    cartItem: state.cartItem,
    activityData: state.activityData,
    getPackageData: state.getPackageData,
    userData: state.loginData,
    driverLocation: state.driverLocation
	};
}

const mapDispatchToProps = (dispatch) => {
    return {
      addCartActions: bindActionCreators(addToCart, dispatch),
      navigationAction: bindActionCreators(navigateAction,dispatch),
      walletAction: bindActionCreators(walletAction,dispatch),
      cartModalAction: bindActionCreators(cartModalAction,dispatch),
      driverLocationAction: bindActionCreators(driverLocationAction,dispatch),
    };
}

// ---------------------------------------------------- store related function  -- END

export default connect(mapStateToProps, mapDispatchToProps)(TrackOrderPage);