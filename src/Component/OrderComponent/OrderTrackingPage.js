/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import MapView, { Marker, AnimatedRegion } from 'react-native-maps';
import Geolocation from '@react-native-community/geolocation';
import PubNub from 'pubnub';
import PubNubReact from 'pubnub-react';
import { PubNubProvider, usePubNub } from 'pubnub-react';
import InternateModal from '../ReusableComponent/InternetModal';
import Toast from 'react-native-simple-toast';
// import 'bootstrap/dist/css/bootstrap.min.css';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  Dimensions,
  Button,
  TextInput,
  StatusBar,
  TouchableWithoutFeedback,
} from 'react-native';

const { width, height } = Dimensions.get("window");
const ASPECT_RATIO = width / height;
const LATITUDE = 19.156004673977666;
const LONGITUDE = 73.06293586269021;
const LATITUDE_DELTA = 0.001;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
const pubnub = new PubNub({
    publishKey: "pub-c-9b21df32-1b75-40ba-a995-d2f6e02ffacd",
    subscribeKey: "sub-c-9473a668-6529-11ea-9174-5e95b827fd71",
});
// const pubnubReact = new PubNubReact();
// const pubnub;
class OrderTracking extends React.Component {
        constructor(props) {
            super(props);

          //   console.warn('PubNubReact',PubNubReact);


        // this.pubnub = {
        //     publishKey: 'pub-c-9b21df32-1b75-40ba-a995-d2f6e02ffacd',
        //     subscribeKey: 'sub-c-9473a668-6529-11ea-9174-5e95b827fd71',
        // }

            this.state = {
                latitude: LATITUDE,
                longitude: LONGITUDE,
                coordinate: new AnimatedRegion({
                    latitude: LATITUDE,
                    longitude: LONGITUDE,
                    latitudeDelta: 0,
                    longitudeDelta: 0
                })
            };

            // pubnub.init(this);

        }

        componentDidMount() {
            //if delivery app
            this.watchLocation();

            //If user app
           this.subscribeToPubNub();
           this.interval = setInterval(async () => {
            this.setState({
                latitude: this.state.latitude + 0.0000001,
                longitude: this.state.longitude + 0.0000001,
            });
            this.subscribeToPubNub();
          }, 10000);

        }

        componentDidUpdate(prevProps, prevState) {
            console.warn('in component did update', prevState.latitude);
            if (this.props.latitude !== prevState.latitude) {
                pubnub.publish({
                    message: {
                    latitude: this.state.latitude,
                    longitude: this.state.longitude
                    },
                    channel: "location"
                });
            }
        }

        subscribeToPubNub = () => {
            pubnub.addListener({
               
                message: function(message) {
                    // handle message
                    console.warn('MESSAGE', message);
                },
            })
            // pubnub.addListener({
                
            //     message: function(message) {
            //         // handle message
            //     },
            // })
            pubnub.subscribe({
              channels: ['location'],
              withPresence: true,
            });
            pubnub.getMessage('location', msg => {
                console.warn('lat lang', msg);
              const { coordinate } = this.state;
              const { latitude, longitude } = msg.message;
              const newCoordinate = { latitude, longitude };

              if (Platform.OS === 'android') {
                if (this.marker) {
                  this.marker._component.animateMarkerToCoordinate(newCoordinate, 500);
                }
              } else {
                coordinate.timing(newCoordinate).start();
              }

              this.setState({
                latitude,
                longitude,
              });
            });
            // pubnub.publish(
            //     {
            //         message: { 
            //             such: 'object' 
            //         },
            //         channel: 'location',
            //         sendByPost: false, // true to send via post
            //         storeInHistory: false, //override default storage options
            //         meta: { 
            //             "cool": "meta"
            //         }   // publish extra meta with the request
            //     }, 
            //     function (status, response) {
            //         if (status.error) {
            //             // handle error
            //             console.warn('status in publish',status)
            //         } else {
            //             console.log("message Published w/ timetoken", response.timetoken)
            //         }
            //     }
            // );
        };


        componentWillUnmount() {
            // navigator.geolocation.clearWatch(this.watchID);
            Geolocation.clearWatch(this.watchID);
        }

        watchLocation = () => {
            const { coordinate } = this.state;

            // this.watchID = navigator.geolocation.watchPosition(
              this.watchID = Geolocation.watchPosition(
              position => {
                console.warn('dkckjnsdjk', position.coords);  
                const { latitude, longitude } = position.coords;

                const newCoordinate = {
                  latitude,
                  longitude
                };

                if (Platform.OS === "android") {
                  if (this.marker) {
                    this.marker._component.animateMarkerToCoordinate(
                      newCoordinate,
                      500 // 500 is the duration to animate the marker
                    );
                  }
                } else {
                  coordinate.timing(newCoordinate).start();
                }

                this.setState({
                  latitude,
                  longitude
                });
              },
              error => console.log(error),
              {
                enableHighAccuracy: true,
                timeout: 20000,
                maximumAge: 1000,
                distanceFilter: 10
              }
            );
        };

        getMapRegion = () => ({
            latitude: this.state.latitude,
            longitude: this.state.longitude,
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA
        });

        render() {
            return (
              <SafeAreaView style={{ flex: 1 }}>
                {/* <InternateModal /> */} 
                <View style={styles.container}>
                  <MapView
                    style={styles.map}
                    showUserLocation
                    followUserLocation
                    loadingEnabled
                    region={this.getMapRegion()}
                  >
                    <Marker.Animated
                      ref={marker => {
                        this.marker = marker;
                      }}
                      coordinate={this.state.coordinate}
                    />
                  </MapView>
                </View>
              </SafeAreaView>
            );
        }
}

const styles = StyleSheet.create({
    container: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: "flex-end",
    alignItems: "center"
    },
    map: {
    ...StyleSheet.absoluteFillObject
    }
});



export default OrderTracking;
