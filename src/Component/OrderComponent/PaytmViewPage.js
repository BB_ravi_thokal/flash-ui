/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Fragment } from 'react';
import * as walletAction  from '../../Actions/WalletAction';
import * as navigateAction  from '../../Actions/NavigationBarAction';
import * as GetPackageAction  from '../../Actions/GetPackageAction';
import * as cartModalAction  from '../../Actions/CartModalAction';
import * as addToCart  from '../../Actions/CartItem';
import * as activityAction  from '../../Actions/ActivityAction';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  SafeAreaView,
  StyleSheet,
} from 'react-native';
import { WebView } from 'react-native-webview';
import Toast from 'react-native-simple-toast';

class PaytmViewPage extends React.Component{
  
    constructor(props){
        super(props);
        console.warn('this.props.navigation.state.params.paytmUrl', this.props.navigation.state.params.paytmUrl);
    }

    handleNavigation = (data) => {
        console.warn('response--->', data);
        let msg;
        if (data.title) {
            if (data.title.startsWith('success')) {
                console.warn('here once half half success');
                this.paytmResponse(data.title, msg);   
            } else {
                switch (data.title) {
                    case '1': 
                        msg = 'The selected shop is not available or far from your delivery location.';
                        this.paytmResponse(false, msg);
                        break;
                    case '2': 
                        msg = 'The selected shop is not available or far from your delivery location.';
                        this.paytmResponse(false, msg);
                        break;
                    case '3': 
                        msg = 'Not able to proceed for payment. Please contact support.';
                        this.paytmResponse(false, msg);
                        break;
                    case '4': 
                        msg = 'Not able to proceed for payment. Please contact support.';
                        this.paytmResponse(false, msg);
                        break;
                    case '5': 
                        msg = 'Not able to proceed for payment. Please contact support.';
                        this.paytmResponse(false, msg);
                        break;
                    case 'success': 
                        msg = 'Your order placed successfully';
                        this.paytmResponse(data.title, msg);
                        break;
                    case 'fail':
                        msg = 'Fail to verify the payment. Any transaction happened please contact our support team';
                        this.paytmResponse(false, msg);
                        break;
                    case 'cancelled':
                        msg = 'You cancelled the transaction';
                        this.paytmResponse(false, msg);
                        break;
                    case 'error':
                        msg = 'Something went wrong. Any transaction happened please contact our support team';
                        this.paytmResponse(false, msg);
                        break;
                    case 'notFound':
                        msg = 'Order not found. Any transaction happened please contact our support team';
                        this.paytmResponse(false, msg);
                        break;        
                    default:
                        console.warn('response in default-->', data.title);
                        // msg = 'Sorry, Not able to place your order please try again';
                        // this.props.paytmResponse(false, msg);
                        break;
                }
            }
            
        }

    }

    paytmResponse = (status, message) => {
        console.warn('response from paytm gateway IOS', status);
        Toast.show(message, Toast.LONG);
        this.setState({showCheckOut: false});
        if (status) {
          let index = status.indexOf("-");
          if (index >= 0) {
              console.warn('inside 1', status);
            const amt = status.slice(index, status.length - 1);  
            console.warn('inside 2', amt);
            // console.warn('wallet amount--->', index, status.splice(index, status.length - 1));
            this.props.walletAction.updateWallet(amt, true);
          } 
          this.postPayment('success');
        } else {
            Toast.show(message, Toast.LONG);
            this.postPayment('fail');
        }
    }

    postPayment = (status) => {
        if (status == 'success') {
            this.manageStore(this.props.navigation.state.params.callingPage);
        } else {
            this.handleBackNavigation(this.props.navigation.state.params.callingPage);
        }
    }

    manageStore = (callingPage) => {
        switch (callingPage) {
            case 'order': 
                this.props.cartItem.cartItem = [];
                // this.props.actions.placedCart(tempObj);
                this.props.actions.replaceCart(this.props.cartItem.cartItem);
                this.props.cartState.cartCount = 0;
                this.props.cartState.cartSum = 0; 
                // this.props.walletAction.updateWallet(0, true);
                
                this.props.cartModalAction.changeCartState(false,this.props.cartState.cartSum,this.props.cartState.cartCount); 
                this.props.cartModalAction.changeOrderState(true); 
                this.props.navigationAction.navigateAction('Home');
                this.props.navigation.navigate('homePage');
                break;
            case 'getActivity': 
                // this.props.activityAction.placedActivity(tempObj);
                this.props.activityData.ongoingActivity = [];
                this.props.activityData.pickupAddress = {};
                this.props.activityData.deliveryAddress = {};
                this.props.activityAction.pickupAddress(this.props.activityData.pickupAddress);
                this.props.activityAction.deliveryAddress(this.props.activityData.deliveryAddress);
                this.props.activityAction.ongoingActivity(this.props.activityData.ongoingActivity);
                this.props.cartModalAction.changeGetActivityState(true);
                this.props.navigationAction.navigateAction('Home');
                this.props.navigation.navigate('homePage');
                break;
            case 'sendActivity': 
                // this.props.GetPackageAction.placedActivity(tempObj);
                this.props.getPackageData.pickupAddress = {};
                this.props.getPackageData.deliveryAddress = {};
                this.props.GetPackageAction.pickupAddress(this.props.getPackageData.pickupAddress);
                this.props.GetPackageAction.deliveryAddress(this.props.getPackageData.deliveryAddress);
                this.props.cartModalAction.changeSendActivityState(true);
                this.props.navigationAction.navigateAction('Home');
                this.props.navigation.navigate('homePage');
                break;
            default:
                this.handleBackNavigation(callingPage);
        }
    }

    handleBackNavigation = (callingPage) => {
        switch (callingPage) {
            case 'order': 
                this.props.navigationAction.navigateAction('Home');
                this.props.navigation.navigate('orderDetailsPage');
                break;
            case 'getActivity': 
                this.props.navigationAction.navigateAction('Home');
                this.props.navigation.navigate('packageDetailsPage');
                break;
            case 'sendActivity': 
                this.props.navigationAction.navigateAction('Home');
                this.props.navigation.navigate('sendPackageList');
                break;
            default:
                this.props.navigationAction.navigateAction('Home');
                this.props.navigation.navigate('homePage');
        }
    }



    render() {
        return (
            <SafeAreaView style={{flex: 1}}>
                <WebView
                    useWebKit={true}
                    source={{ uri: this.props.navigation.state.params.paytmUrl }}
                    onNavigationStateChange={data => this.handleNavigation(data)}
                />
            </SafeAreaView>
        )
    }
}


const styles = StyleSheet.create({
 
});

const mapStateToProps = (state) => {
	return {
        walletBalance: state.walletBalance,
        cartItem: state.cartItem,
        cartState: state.cartState,
        getPackageData: state.getPackageData,
        activityData: state.activityData,
	}
}

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(addToCart, dispatch),
        GetPackageAction: bindActionCreators(GetPackageAction,dispatch),
        cartModalAction: bindActionCreators(cartModalAction,dispatch),
        navigationAction: bindActionCreators(navigateAction,dispatch),
        walletAction: bindActionCreators(walletAction,dispatch),
        activityAction: bindActionCreators(activityAction,dispatch),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(PaytmViewPage);