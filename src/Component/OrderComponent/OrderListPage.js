/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Fragment } from 'react';
// import 'bootstrap/dist/css/bootstrap.min.css';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  Dimensions,
} from 'react-native';
import Toast from 'react-native-simple-toast';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import InternateModal from '../ReusableComponent/InternetModal';


class OrderListPage extends React.Component{
    render() {
        const {navigate} = this.props.navigation;
        return (
            <SafeAreaView style={{flex: 1}}>
                <ScrollView contentInsetAdjustmentBehavior="automatic" style={styles.scrollView}>
                {/* <InternateModal /> */}
                    <View style={styles.mainView}>
                        <View style={styles.orderMainView}>
                        
                            <View style={styles.orderHeading}>
                                <Text style={{fontWeight: 'bold',fontSize:30}}>MY ORDERS</Text>
                            </View>
                                <View style={styles.orderTitles}>
                                    <Text style={{fontWeight: 'bold',fontSize:16}}>Today</Text>
                                </View>
            
                            <View style={styles.orderContainer}>
                                <View style={[styles.orderList,styles.orderBorder]}>
                                    <View style={[styles.MyOrders,styles.orderCardPadding]}>
                                        <View style={styles.orders}>
                                            <Text style={{fontWeight: 'bold',fontSize:16}}>Spicy fries</Text>
                                            <Text style={{ color: '#A9A9A9' }}>3 Item X ₹7.99</Text>
                                        </View>
                                        <Text style={styles.orderPriceText}>₹23.76</Text>
                                    </View>
                                    <View style={[styles.MyOrders]}>
                                        <View style={styles.orders}>
                                            <Text style={{fontWeight: 'bold',fontSize:16}}>Spicy burger</Text>
                                            <Text style={{ color: '#A9A9A9' }}>3 Item X ₹7.99</Text>
                                        </View>
                                        <Text style={styles.orderPriceText}>₹23.76</Text>
                                    </View>
                                </View>
            
                                <View style={styles.orderList}>
                                    <View style={[styles.MyOrders,styles.orderCardPadding]}>
                                        <View>
                                            <Text style={{fontWeight: 'bold',fontSize:16}}>Delivery charges</Text>
                                        </View>
                                        <Text style={styles.orderPriceText}>₹0.00</Text>
                                    </View>
                                    <View style={styles.MyOrders}>
                                        <View>
                                            <Text style={{fontWeight: 'bold',fontSize:16}}>Total</Text>
                                        </View>
                                        <Text style={styles.orderPriceText}>₹45.55</Text>
                                    </View>
                                </View>
                                <View style={styles.orderCenter}>
                                    <View style = {[styles.orderBtn,styles.orderBtnViewColor]}>
                                        <Text style={{ color: 'white' }}>View Orders</Text>
                                    </View>
                                </View>
                                
                            </View>
                            <View style={styles.orderTitles}>
                                <Text style={{fontWeight: 'bold',fontSize:16}}>Older</Text> 
                            </View>
                            <View style={[styles.orderContainer,styles.orderCardSpace]}>
                                <View style={[styles.orderList]}>
                                    <View style={[styles.MyOrders]}>
                                        <View style={styles.orders}>
                                            <Text style={{fontWeight: 'bold',fontSize:16}}>Spicy fries</Text>
                                            <Text style={{ color: '#A9A9A9' }}>3 Item X ₹7.99</Text>
                                        </View>
                                        <Text style={styles.orderPriceText}>₹23.76</Text>
                                    </View>
                                </View>
                            </View>
                            <View style={[styles.orderContainer,styles.orderCardSpace]}>
                                <View style={[styles.orderList]}>
                                    <View style={[styles.MyOrders]}>
                                        <View style={styles.orders}>
                                            <Text style={{fontWeight: 'bold',fontSize:16}}>Spicy burger</Text>
                                            <Text style={{ color: '#A9A9A9' }}>3 Item X ₹7.99</Text>
                                        </View>
                                        <Text style={styles.orderPriceText}>₹23.76</Text>
                                    </View>
                                </View>
            
                            </View>
                        </View>
                    </View>
                </ScrollView>
        
            </SafeAreaView>
        
    );
    }
}

let ScreenHeight = Dimensions.get("window").height;

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,
    height: ScreenHeight
  },
  body: {
    backgroundColor: 'grey',
  },
  mainView: {
    backgroundColor: '#EBECF0',
    height: ScreenHeight
  },
  orderContainer:{
    backgroundColor: '#FFF',
    borderRadius:20,
  },
  orderHeading:{
    paddingLeft: 10,
    paddingVertical:5,
    marginTop:15
  },
  orderTitles:{
    paddingLeft: 10,
    paddingVertical:15,
  },
  orderPriceText:{
    color:'#3cb371',
    fontWeight:'bold'
  },
  orderList:{
    display:"flex",
    flexDirection: "column",
    padding:20,
  },
  orderBorder:{    
    borderBottomColor: 'grey',
    borderBottomWidth: 1,
},
orderCenter:{
  display:"flex",
  flexDirection:"row",
  justifyContent:"center"
},
  MyOrders:{
    flexDirection:"row",
    alignItems:"center",
    justifyContent:"space-between",
    // paddingBottom:15,
  },
  orderCardSpace:{
    marginBottom: 15,
  },
  orderCardPadding:{
    paddingBottom: 15,
  },
  orderMainView: {
    marginHorizontal: 20,
    // flex: 3,
    backgroundColor: '#EBECF0',
  },
  headerstyle: {
    paddingBottom: 20,
    paddingTop: 20
    // flex: 1,
    // marginLeft: 20,
    // marginRight: 20
  },

  orderBtnViewColor: {
    backgroundColor: '#ba55d3',
    color:'white',
  },
  orderBtn: {
    paddingVertical:10,
    paddingHorizontal:20,
    marginBottom:10,
    color:'white',
    borderRadius: 20,
  },
  logoContainer: {
    height: 250,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    // height:50
    // flex: 11,
    // flexDirection: 'column'
  },
});


export default OrderListPage;
