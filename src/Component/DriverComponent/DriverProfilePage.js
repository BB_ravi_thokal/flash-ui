/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Fragment } from 'react';

import * as Config from '../../Constant/Config.js';
import * as Constant from '../../Constant/Constant.js';
import { connect } from 'react-redux';
import * as loginSuccess  from '../../Actions/LoginAction';
import * as driverStatusAction  from '../../Actions/DriverStatusAction';
import { bindActionCreators } from 'redux';
import  s  from '../../sharedStyle.js';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  Dimensions,
  TouchableOpacity,
  Button,
  TextInput,
  StatusBar,
  TouchableWithoutFeedback,
  Image,
  FlatList,
  Modal,
  ToastAndroid,
  AsyncStorage
} from 'react-native';
import Toast from 'react-native-simple-toast';

import axios from 'axios'; 
import {
  Colors,
} from 'react-native/Libraries/NewAppScreen';
import InternateModal from '../ReusableComponent/InternetModal';
import DriverNavigationBar from '../ReusableComponent/DriverNavigationBar';
import * as navigateAction  from '../../Actions/NavigationBarAction';
import FlipToggle from 'react-native-flip-toggle-button';
import { NavigationActions, StackActions } from 'react-navigation';

PastOrdersListDriver = ({item, self}) => {
  return (
    <View>
      <TouchableOpacity style={{marginVertical:10}}>
        <View>
          <Text style={{fontSize: 16,fontWeight:"bold"}}>Order Id: {item.orderId}</Text>
        </View>
        <View>
          <Text style={s.normalText}>Item Qty: 10</Text>
        </View>
        <View>
          <Text style={s.normalText}>Total Amount Paid: ₹ {item.order.toPay}</Text>
        </View>
      </TouchableOpacity>
      <View style={{borderBottomColor:"#131E3A",borderBottomWidth:1,marginTop:10}}></View>
      
    </View> 
  )
}

PastGetActivitiesListDriver = ({item, self}) => {
  return (
    <View>
      <TouchableOpacity style={{marginVertical:10}}>
        <View>
          <Text style={{fontSize: 16,fontWeight:"bold"}}>Order Id: {item.orderId}</Text>
        </View>
        <View>
          <Text style={s.normalText}>Item Qty: 10</Text>
        </View>
        <View>
          <Text style={s.normalText}>Total Amount Paid: $1000</Text>
        </View>
        <View style={{flex:1,flexDirection:"row"}}>
          <View style={{flex:2}}>
            <Text style={s.normalText}>Ordered Date: {item.pickupDate}</Text>
          </View>
          <View style={{flex:1,alignItems:"flex-end"}}>
            <Text style={[s.normalText,{color:"#00CCFF"}]}>See Details ></Text>
          </View>
        </View>
      </TouchableOpacity>
      <View style={{borderBottomColor:"#131E3A",borderBottomWidth:1,marginTop:10}}></View>
      
    </View> 
  )
}

PastSendActivitiesListDriver = ({item, self}) => {
  return (
    <View>
      <TouchableOpacity style={{marginVertical:10}}>
        <View>
          <Text style={{fontSize: 16,fontWeight:"bold"}}>Order Id: {item.orderId}</Text>
        </View>
        <View>
          <Text style={s.normalText}>Item Qty: 10</Text>
        </View>
        <View>
          <Text style={s.normalText}>Total Amount Paid: $1000</Text>
        </View>
        <View style={{flex:1,flexDirection:"row"}}>
          <View style={{flex:2}}>
            <Text style={s.normalText}>Ordered Date: {item.pickupDate}</Text>
          </View>
          <View style={{flex:1,alignItems:"flex-end"}}>
            <Text style={[s.normalText,{color:"#00CCFF"}]}>See Details ></Text>
          </View>
        </View>
      </TouchableOpacity>
      <View style={{borderBottomColor:"#131E3A",borderBottomWidth:1,marginTop:10}}></View>
      
    </View> 
  )
}

class DriverProfilePage extends React.Component{

  constructor(props){
    super(props);    
    this.state = {
      data : "", 
      address:[],
      addAddress: false,
      isModalVisible: false,
      removeAddressId: [],
      removeAddressIndex: 0,       
      getActivityData: [],
      sendActivityData: [],
      orderData: [],
      isActive: false  
    }
    
  }

  
  componentDidMount(){     
    this.props.navigationAction.driverNavigateAction('Delivered');  
    console.warn('this.props.driverStatus.driverStatus', this.props.driverStatus.isActive);
    if(this.props.driverStatus.isActive) {
      this.setState({
        isActive: true
      })
    }
    this.props.navigation.addListener('willFocus', () =>{
      console.warn('am in focus Search');
      this.props.navigationAction.driverNavigateAction('Delivered');  
    });
    
    axios.post(
      Config.URL + Config.pastDeliveredOrderDriver,
      {
        deliveryBoyId: this.props.userData.driverData.userDetails[0].userId,
        // deliveryBoyId : "12345678890",
        activity: false
      },
      {
        headers: {
            'Content-Type': 'application/json',
        }
      }
    )
    .then(res => {
        console.warn('response in driver profile-->', res.data.result);
        if (res.data.status == 0) {
          this.setState({
            orderData: res.data.result,
          })
        }
      }).catch(err => {
        //console.warn(res.data.status);
    })

    axios.post(
      Config.URL + Config.pastDeliveredOrderDriver,
      {
        deliveryBoyId: this.props.userData.driverData.userDetails[0].userId,
        // deliveryBoyId : "12345678890",
        activity: true
      },
      {
        headers: {
            'Content-Type': 'application/json',
        }
      }
    )
    .then(res => {
        console.warn('response in driver profile-->', res.data);
        if (res.data.status == 0) {
          this.setState({
            getActivityData: res.data.result.sendActivityOrderDetails,
            sendActivityData: res.data.result.getActivityOrderDetails,
          })
        }
      }).catch(err => {
        //console.warn(res.data.status);
    })
  }

  
  logoutAction = () => {
    Toast.show("Logout successfully!!",Toast.LONG);
    this.props.userData.driverData.token = '';
    this.props.loginActions.driverLoginSuccess(this.props.userData.driverData);
    const resetAction = StackActions.reset({
        index: 0,
        actions: [NavigationActions.navigate({ routeName: 'login'})]
  
      })
      
      this.props.navigation.dispatch(resetAction);
  }

  PastOrdersDriver = () => {
    if (this.state.orderData.length > 0) {
      return (
        <View style={{backgroundColor:"white",marginTop:5}}>
          <View style={{flex:1,paddingVertical:10,paddingHorizontal:15}}>
            <View style={{marginBottom:10}}>
              <Text style={s.headerText}>Past Orders</Text>
            </View>
              <View>
                <FlatList data={this.state.orderData}   
                  renderItem={( { item } ) => (
                    <PastOrdersListDriver
                      item = {item}
                      self = {this}
                    // navigation={this.props.navigation}
                    />
                  )}
                  keyExtractor= {item => item.orderID}
                  extraData = {this.state.orderData} 
                />
              </View>
          </View>
        </View> 
      )
    } else {
      return null;
    }
  }

  PastGetActivitiesDriver = () => {
    if (this.state.getActivityData.length > 0) {
      return (
        <View style={{backgroundColor:"white",marginTop:5}}>
          <View style={{flex:1,paddingVertical:10,paddingHorizontal:15}}>
            <View style={{marginBottom:10}}>
              <Text style={s.headerText}>Past get activities</Text>
            </View>
              <View>
                <FlatList data={this.state.getActivityData}   
                  renderItem={( { item } ) => (
                    <PastGetActivitiesListDriver
                      item = {item}
                      self = {this}
                    // navigation={this.props.navigation}
                    />
                  )}
                  keyExtractor= {item => item.orderID}
                  extraData = {this.state.getActivityData} 
                />
              </View>
          </View>
        </View> 
      )
    } else {
      return null;
    }
  }

  
  PastSendActivitiesDriver = () => {
    if (this.state.sendActivityData.length > 0) {
      return (
        <View style={{backgroundColor:"white",marginTop:5}}>
          <View style={{flex:1,paddingVertical:10,paddingHorizontal:15}}>
            <View style={{marginBottom:10}}>
              <Text style={s.headerText}>Past send activities</Text>
            </View>
              <View>
                <FlatList data={this.state.sendActivityData}   
                  renderItem={( { item } ) => (
                    <PastSendActivitiesListDriver
                      item = {item}
                      self = {this}
                    // navigation={this.props.navigation}
                    />
                  )}
                  keyExtractor= {item => item.orderID}
                  extraData = {this.state.sendActivityData} 
                />
              </View>
          </View>
        </View> 
      )
    } else {
      return null;
    }
  }

  changeStatus = (newState) => {
    console.warn('newState--->', newState);
    this.setState({
      isActive: newState
    });
    if (newState) {
      // this.props.driverStatusAction.driverStatusChange(false);
      this.props.navigation.navigate('driverAddressPage');
    }
    
  }

  render(){
    const {navigate} = this.props.navigation;
    return (
      <Fragment>
        <SafeAreaView style={{flex: 1, backgroundColor: '#f1f1f1'}}>
          <View style={[styles.mainView]}>
          {/* <InternateModal /> */}
          <ScrollView>
            <View >
              <View style={{backgroundColor:"white",flex: 1, flexDirection:"row", marginBottom: 5, paddingHorizontal: 15, paddingVertical: 10}}>
                <View style={{flex: 1, paddingVertical: 10}}>
                  <Text style={s.headerText}>My Status</Text>
                </View>
                <View style={{flex: 1, alignItems:"flex-end"}}>
                  <FlipToggle
                    value={this.state.isActive}
                    buttonWidth={150}
                    buttonHeight={50}
                    buttonRadius={50}
                    sliderWidth={20}
                    sliderHeight={30}
                    sliderRadius={100}
                    sliderOnColor={'white'}
                    sliderOffColor={'white'}
                    buttonOnColor={'#00CCFF'}
                    buttonOffColor={'#131E3A'}
                    onLabel={'Active'}
                    offLabel={'DeActive'}
                    labelStyle={{ color: 'white', fontWeight:"bold" }}
                    onToggle={(newState) => this.changeStatus(newState)}
                    onToggleLongPress={() => console.warn('toggle long pressed!')}
                  />
                </View>
              
              </View>
              <View>
                <View style={{backgroundColor:"white"}}>    
                  <View style={{padding:10 , marginLeft:5 , marginRight:5 , borderBottomWidth:3,borderBottomColor:"#003151"}}>
                    <View style={{flex:1,flexDirection:"row"}}>
                      <View style={{flex:1}}>
                        <Text style={s.headerText}>{this.props.userData.driverData.userDetails[0].fullName}</Text>  
                      </View>
                      <View style={{flex:1,alignItems:"flex-end"}}>
                        <Image style={{height: 20, width: 20}} source={require('../../../assets/edit.png')}/>
                      </View>
                    </View>
                    <Text style={s.normalText}>{this.props.userData.driverData.userDetails[0].email}</Text>
                    <Text style={s.normalText}>{this.props.userData.driverData.userDetails[0].contactNumber}</Text>
                  </View>     
                </View>
              </View>
              <this.PastOrdersDriver />
              <this.PastGetActivitiesDriver />
              <this.PastSendActivitiesDriver />
              <TouchableOpacity style={{backgroundColor:"white",marginTop:5,marginBottom:5}} onPress={() => this.logoutAction()}>
                  <View style={{flex:1,flexDirection:"row",paddingVertical:10,alignItems:"center",paddingHorizontal:15}}>
                    <Text style={s.headerText}>Log Out</Text>
                    <View style={{flex:1,alignItems:"flex-end",marginRight:5}}>
                      <Image style={{height:15,width:15}} source={require('../../../assets/next.png')}/>
                    </View>
                  </View>
              </TouchableOpacity>
            </View>
            </ScrollView>
            <DriverNavigationBar navigate={navigate}></DriverNavigationBar>  
          </View>
      
      </SafeAreaView>
      </Fragment>

      
    );
  }

}


let ScreenWidth = Dimensions.get("window").width;
let ScreenHeight = Dimensions.get("window").height;

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,
    height: ScreenHeight,
    paddingBottom:20,
  },
  addtoCartBtnColor: {
    borderWidth: 2,
    borderColor: '#00CCFF',
  //  backgroundColor: '#00009a',
    //color: 'white',
    marginTop: 10
  },
  storeBtn: {
    paddingVertical: 5,
    marginBottom: 5,
    color: 'white',
    borderRadius: 0,
    width: 150,
  },
  containerIcons:{
    width:30,
    height:30,
    backgroundColor:'blue',
  },
  body: {
    backgroundColor: 'grey',
  },
  mainView: {
    // backgroundColor: '#fff',
    flex: 1,
    //height: ScreenHeight - 135
  },
  categoryFooter:{
    width:ScreenWidth,
    paddingVertical:10,
    height:60,
    backgroundColor:'lightgray',
    display:'flex',
    flexDirection:'row',
    justifyContent:"space-evenly",
    alignItems:"center"
  },

  profileViewData:{
    display:"flex",
    alignItems:"center",
    justifyContent:"space-between",
    flexDirection:"row",
    paddingTop:5
  },
  flexRow:{
    display:'flex',
    flexDirection:'row',
  },
updateprofileBtn:{
  padding:5,
  marginLeft:5,
  backgroundColor:'white',
  borderWidth:1,
  borderColor:"lightgray"
},
  profileLayout: {
    height: 150,
    width: ScreenWidth,
    borderBottomLeftRadius: ScreenWidth/2,
    borderBottomRightRadius: ScreenWidth/2,
    borderColor: 'black',
    backgroundColor: 'skyblue',
    position:'relative',
    justifyContent: 'center',
    zIndex:1,
  },
  profilemage:{
    position:'absolute',
    alignSelf: 'center',
    bottom:'-30%',
    height:100,
    width:100,
    borderRadius:50,
    backgroundColor:'darkblue',
  
  },
  profileHeading: {
    color: 'black',
    fontSize: 20,
    fontWeight:"bold"
  },
  profileText:{
    color: 'black',
    fontSize: 20,
    fontWeight:'600',
  },
  profileData:{ 
    borderBottomWidth:1,
    borderBottomColor:'grey',
    paddingVertical:5,
    marginBottom:10
 },
 profileAboutView: {
  marginHorizontal: 20,
  paddingVertical:5,
  // flex: 3,
  backgroundColor: '#fff',
},
  profileMainView: {
    marginHorizontal: 20,
    paddingTop:20,
    // flex: 3,
    backgroundColor: '#fff',
  },
  profilePartition:{
    width:ScreenWidth,
    height:20,
    backgroundColor: 'lightgrey',
    marginTop:10
  },

  // mk start --
  profileHeading: {
    color: 'black',
    fontSize: 20,
    fontWeight:"bold"
  },

});


//--------------------------------------------------- store related fuction  -- Start

const mapStateToProps = (state) => {
	return {
    userData: state.loginData,
    driverStatus: state.driverStatus,
	};
}

const mapDispatchToProps = (dispatch) => {
    return {
      loginActions: bindActionCreators(loginSuccess,dispatch),
      navigationAction: bindActionCreators(navigateAction,dispatch),
      driverStatusAction: bindActionCreators(driverStatusAction,dispatch),
    };
}

// ---------------------------------------------------- store related function  -- END

export default connect(mapStateToProps, mapDispatchToProps)(DriverProfilePage);


