/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Fragment } from 'react';
import { SafeAreaView, StyleSheet, Text, View, FlatList, Button, Dimensions, AppRegistry } from 'react-native';
import axios from 'axios';
import * as Config from '../../Constant/Config.js';
import InternateModal from '../ReusableComponent/InternetModal';
import { ScrollView } from 'react-native-gesture-handler';
import AnimatedLoader from "react-native-animated-loader";
import  s  from '../../sharedStyle.js';
import DriverNavigationBar from '../ReusableComponent/DriverNavigationBar';
import { connect } from 'react-redux';
import * as loginSuccess  from '../../Actions/LoginAction';
import { bindActionCreators } from 'redux';
import * as navigateAction  from '../../Actions/NavigationBarAction';
import Toast from 'react-native-simple-toast';
import googleIntegration from '../../CoreFunctions/googleIntegration';
// import BackgroundTask from 'react-native-background-task';
// import BackgroundFetch from "react-native-background-fetch";
import Geolocation from '@react-native-community/geolocation';
import callAPI from '../../CoreFunctions/callAPI';
// import SceneLoader from 'react-native-scene-loader';

// BackgroundTask.define(async () => {
//   console.warn('am running in background');
//   Geolocation.getCurrentPosition(
//     (position) => {
//       const region = {
//         latitude: position.coords.latitude,
//         longitude: position.coords.longitude,
//         latitudeDelta: 0.001,
//         longitudeDelta: 0.001
//       };
//       const reqBody = {
//         "deliveryBoyId": this.props.userData.driverData.userDetails[0].userId,
//         "status": 'Active',
//         "location": region
//       }
//       await callAPI.axiosCall(Config.updateCArtBasket, reqBody);
//       BackgroundTask.finish();
//     },
//     (error) => {
//       if (error.PERMISSION_DENIED === 1) {
//         BackgroundTask.finish();
//       }
//     },
//     { enableHighAccuracy: false, timeout: 200000, maximumAge: 5000 },
//   );
// })


class DriverOrderListing extends React.Component{

    constructor(props) {
        super(props);
        this.state={
          orderData: [],
          showLoader: true,
        }
    }

    assignDeliveryBoy = (id) => {
      this.setState({showLoader: true});
      axios.post(
        Config.URL + Config.assignedDeliveryBoy,
        {
          orderId : id,
          deliveryBoyId: this.props.userData.driverData.userDetails[0].userId,
          // deliveryBoyId : "12345678890"
        },
        {
          headers: {
              'Content-Type': 'application/json',
          }
        }
      )
      .then(res => {
          if (res.data.status == 0) {
            console.warn('res-- assign', res.data);
            this.setState({
              orderData: res.data.result,
              showLoader: false
            })
            Toast.show(res.data.message,Toast.LONG);
            this.props.navigation.navigate('driverOngoingOrder',{isActivity: false, activityType: ''});
          } else {
            Toast.show(res.data.message,Toast.LONG);
          }
        }).catch(err => {
          console.warn('res-- assign', res.data);
          this.setState({showLoader: false});
          Toast.show(res.data.message,Toast.LONG);
      })
      
    }

    componentDidMount(){
      console.warn('Hi am in order list');
      this.props.navigationAction.driverNavigateAction('Order');
      // BackgroundFetch.configure({
      //   minimumFetchInterval: 15,     // <-- minutes (15 is minimum allowed)
      //   // Android options
      //   forceAlarmManager: false,     // <-- Set true to bypass JobScheduler.
      //   stopOnTerminate: false,
      //   startOnBoot: true,
      //   requiredNetworkType: BackgroundFetch.NETWORK_TYPE_NONE, // Default
      //   requiresCharging: false,      // Default
      //   requiresDeviceIdle: false,    // Default
      //   requiresBatteryNotLow: false, // Default
      //   requiresStorageNotLow: false  // Default
      // }, async (taskId) => {
      //   console.log("[js] Received background-fetch event: ", taskId);
      //   await this.updateLocation();
      //   // Required: Signal completion of your task to native code
      //   // If you fail to do this, the OS can terminate your app
      //   // or assign battery-blame for consuming too much background-time
      //   BackgroundFetch.finish(taskId);
      // }, (error) => {
      //   console.log("[js] RNBackgroundFetch failed to start");
      // });
   
      // // Optional: Query the authorization status.
      // BackgroundFetch.status((status) => {
      //   switch(status) {
      //     case BackgroundFetch.STATUS_RESTRICTED:
      //       console.log("BackgroundFetch restricted");
      //       break;
      //     case BackgroundFetch.STATUS_DENIED:
      //       console.log("BackgroundFetch denied");
      //       break;
      //     case BackgroundFetch.STATUS_AVAILABLE:
      //       console.log("BackgroundFetch is enabled");
      //       break;
      //   }
      // });
      // BackgroundTask.schedule({
      //   period: 900, // Aim to run every 30 mins - more conservative on battery
      // })
      // this.checkStatus();
      this.intervalBack = setInterval(async () => {
        this.updateLocation();
      }, 20000);
      this.setState({showLoader: true});

      this.getPlaceOrderTimer();

      this.props.navigation.addListener('willFocus', () =>{
        console.warn('am in focus Home');
        this.props.navigationAction.driverNavigateAction('Order');
      });
    }

    updateLocation = async() => {
      console.warn('am running in background');
      Geolocation.getCurrentPosition(
        async(position) => {
          const region = {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
            latitudeDelta: 0.001,
            longitudeDelta: 0.001
          };
          const reqBody = {
            "deliveryBoyId": this.props.userData.driverData.userDetails[0].userId,
            "status": 'Active',
            "location": region
          }
          await callAPI.axiosCall(Config.updateDeliveryBoyStatus, reqBody);
        },
        (error) => {
          if (error.PERMISSION_DENIED === 1) {
            // BackgroundTask.finish();
          }
        },
        { enableHighAccuracy: false, timeout: 200000, maximumAge: 5000 },
      );
    }

    // async checkStatus() {
    //   const status = await BackgroundTask.statusAsync()
      
    //   if (status.available) {
    //     // Everything's fine
    //     return
    //   }
      
    //   const reason = status.unavailableReason;
    //   if (reason === BackgroundTask.UNAVAILABLE_DENIED) {
    //     Alert.alert('Denied', 'Please enable background "Background App Refresh" for this app')
    //   } else if (reason === BackgroundTask.UNAVAILABLE_RESTRICTED) {
    //     Alert.alert('Restricted', 'Background tasks are restricted on your device')
    //   }
    // }

    updateCurrentLocation = async() => {
      const currentPosition =  await googleIntegration.getCurrentPosition(); 
      console.warn('Driver current position--->', currentPosition);

    }

    getPlaceOrderTimer = () => {
      this.getPlaceOrder();
      this.interval = setInterval(async () => {
        this.getPlaceOrder();
      }, 20000);
    }

    getPlaceOrder = () => {
      axios.post(
        Config.URL + Config.getPlacedOrderDriver,
        {
          activity:false
        },
        {
          headers: {
              'Content-Type': 'application/json',
          }
        }
      )
      .then(res => {
        console.warn('Order requests for driver--->', res);
          if (res.data.status == 0) {
            this.setState({
              orderData: res.data.result,
              showLoader: false
            });
          }
        }).catch(err => {
          console.warn('Order requests for driver error--->', res);
          this.setState({
            showLoader: false
        })
          //console.warn(res.data.status);
      })
    }


    orderList = (item) => {
        return (
            <View style={styles.card}>   
                <View>
                  <Text style={s.subHeaderText}>Order Id:</Text>
                  <Text style={s.normalText}>{item.item.orderId}</Text>
                </View>  
                <View>
                  <Text style={s.subHeaderText}>Large quantity:</Text>
                  <Text style={[s.normalText,{color:"red"}]}>{item.item.isLargeQuantity}</Text>
                </View>
                
                <View style={styles.horizontalSpaceLine} />
                <View style={{justifyContent: "space-between"}}>
                  <Text style={s.subHeaderText}>Delivery Address</Text>
                  <Text style={s.normalText}>Flat No: {item.item.order.deliveryAddress[0].flatNumber}, 
                      {item.item.order.deliveryAddress[0].addressLine1} </Text>
                  <Text style={s.normalText}>{item.item.order.deliveryAddress[0].addressLine2}</Text>
                  {/* <Text style={s.normalText}>state: {item.item.order.deliveryAddress[0].state}, 
                      {item.item.order.deliveryAddress[0].pinCode}, 
                      district: {item.item.order.deliveryAddress[0].district}</Text>     */}
                </View>
                
                <View style={styles.horizontalSpaceLine} />
                <View>
                  <Button title = "Accept Request" onPress={() => this.assignDeliveryBoy(item.item.orderId)} />
                </View>
            </View>
        )
        
    }

    render() {
        const {navigate} = this.props.navigation;
        return (
          <Fragment>
            <SafeAreaView style={{flex: 1, backgroundColor: '#332b5c'}}>
              <View style={styles.container}>
                  <AnimatedLoader
                      visible={this.state.showLoader}
                      overlayColor="rgba(255,255,255,0.75)"
                      source={require("../../../assets/loader.json")}
                      animationStyle= {[styles.lottie,{height: 150,width: 150}]}
                      speed={1}
                  />
                  {/* <SceneLoader
                    visible={this.state.showLoader}
                    animation={{
                        fade: {timing: {duration: 1000 }}
                    }} 
                  />*/}
                  <ScrollView showsVerticalScrollIndicator={false} style={{marginBottom: 20}}>
                      <FlatList data={this.state.orderData}
                        renderItem={( { item } ) => (
                          <this.orderList
                            item={item}
                          />
                        )}
                        keyExtractor= {item => item.orderId}
                        extraData = {this.state.orderData}
                      />
                  </ScrollView> 
                  <DriverNavigationBar navigate={navigate}></DriverNavigationBar>
            </View>
        
          </SafeAreaView>
          </Fragment>
          
        
      );
    }

}


let ScreenWidth = Dimensions.get("window").width;
let ScreenHeight = Dimensions.get("window").height;

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#f1f1f1'
    },
    card: {
      flex: 1,
      marginHorizontal: 15,
      marginVertical:10,
      borderRadius: 3,
      borderColor: 'transparent',
      padding: 10,
      shadowColor: "#000",
      elevation: 3,
      backgroundColor: "white"
    },
    horizontalSpaceLine:{
      borderBottomColor : "#808080",
      borderBottomWidth : 3,
      marginBottom: 5,
      marginTop: 5
    },
    categoryFooter:{
        width:ScreenWidth,
        //paddingBottom:20,
        position: "relative",
        bottom: 0,
        height: 50,
        //flexGrow:0.05,
        backgroundColor:'#fff',
        //display:'flex',
        flexDirection:'row',
        justifyContent:"space-evenly",
        alignItems:"center",
        zIndex: 9999999,
        elevation: 4
      },
  })



//--------------------------------------------------- store related fuction  -- Start

const mapStateToProps = (state) => {
	return {
    userData: state.loginData,
	};
}

const mapDispatchToProps = (dispatch) => {
    return {
      loginActions: bindActionCreators(loginSuccess,dispatch),
      navigationAction: bindActionCreators(navigateAction,dispatch),
    };
}

// ---------------------------------------------------- store related function  -- END

export default connect(mapStateToProps, mapDispatchToProps)(DriverOrderListing);  






