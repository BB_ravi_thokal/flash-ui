/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Fragment } from 'react';
import { SafeAreaView, StyleSheet, Text, View, FlatList, Button, Dimensions, TouchableOpacity, Image, ToastAndroid } from 'react-native';
import axios from 'axios';
import * as Config from '../../Constant/Config.js';
import InternateModal from '../ReusableComponent/InternetModal';
import { ScrollView } from 'react-native-gesture-handler';
import AnimatedLoader from "react-native-animated-loader";
import  s  from '../../sharedStyle.js';
import DriverNavigationBar from '../ReusableComponent/DriverNavigationBar';
import { connect } from 'react-redux';
import * as loginSuccess  from '../../Actions/LoginAction';
import { bindActionCreators } from 'redux';  
import * as navigateAction  from '../../Actions/NavigationBarAction';
import Toast from 'react-native-simple-toast';
// import SceneLoader from 'react-native-scene-loader';

GetActivityListActDriver = ({item, self}) => {
  return (
    <View style={styles.card}>   
        <View>
          <Text style={[s.subHeaderText, {marginBottom: 5}]}>Activity Id:</Text>
          <Text style={s.normalText}>{item.orderId}</Text>
        </View> 
        <View style={styles.horizontalSpaceLine} />  
        <View>
          <Text style={[s.subHeaderText, {marginBottom: 5}]}>Pickup Date:</Text>
          <Text style={s.normalText}>{item.order.pickupDate}</Text>
        </View> 
        {/* <View>
          <Text style={[s.subHeaderText, {marginBottom: 5}]}>Delivery Date:</Text>
          <Text style={s.normalText} >{item.order.deliveryDate}</Text>
        </View>  */}

        <View style={styles.horizontalSpaceLine} />
        <View style={{marginTop: 10}}>
          <Text style={[s.subHeaderText, {marginBottom: 5}]}>First delivery Address:</Text>
          <Text style={s.normalText}>Flat No: {item.order.activityDetails[0].deliveryAddress.flatNumber}, 
              {item.order.activityDetails[0].deliveryAddress.addressLine1} </Text>
          <Text style={s.normalText}>{item.order.activityDetails[0].deliveryAddress.addressLine2}</Text>
          {/* <Text style={s.normalText}>state: {item.order.activityDetails[0].deliveryAddress.state}, 
              {item.order.activityDetails[0].deliveryAddress.pinCode},district: {item.order.activityDetails[0].deliveryAddress.district}</Text>     */}
        </View>

        <View style={styles.horizontalSpaceLine} />
        <View style={{justifyContent: "space-between"}}>
          <Text style={[s.subHeaderText, {marginBottom: 5}]}>Pickup address:</Text>
          <Text style={s.normalText}>Flat No: {item.order.pickupAddress.flatNumber}, 
              {item.order.pickupAddress.addressLine1} </Text>
          <Text style={s.normalText}>{item.order.pickupAddress.addressLine2}</Text>
          {/* <Text style={s.normalText}>state: {item.order.deliveryAddress.state}, 
              {item.order.deliveryAddress.pinCode}, 
              district: {item.order.deliveryAddress.district}</Text>     */}
        </View>
        
        <View style={styles.horizontalSpaceLine} />
        <View>
          <Button title = "Accept Request" onPress={() => self.assignDeliveryBoy(item.orderId, 'Get')} />
        </View>
    </View>
  )
}

SendActivityListActDriver = ({item, self}) => {
  return (
    <View style={styles.card}>   
        <View>
          <Text style={[s.subHeaderText, {marginBottom: 5}]}>Activity Id:</Text>
          <Text style={s.normalText}>{item.orderId}</Text>
        </View>  
        <View style={styles.horizontalSpaceLine} />
        <View>
          <Text style={[s.subHeaderText, {marginBottom: 5}]}>Package Description:</Text>
          <Text style={s.normalText} numberOfLines={200}>{item.order.activityDetails[0].packageDescription}</Text>
        </View>  
        <View>
          <Text style={[s.subHeaderText, {marginBottom: 5}]}>Pickup Date:</Text>
          <Text style={s.normalText}>{item.order.pickupDate}</Text>
        </View> 
        {/* <View>
          <Text style={[s.subHeaderText, {marginBottom: 5}]}>Delivery Date:</Text>
          <Text style={s.normalText} >{item.order.deliveryDate}</Text>
        </View>  */}
        
        <View style={styles.horizontalSpaceLine} />
        <View style={{justifyContent: "space-between"}}>
          <Text style={[s.subHeaderText, {marginBottom: 5}]}>Pickup Address</Text>
          <Text style={s.normalText}>Flat No: {item.order.activityDetails[0].pickupAddress.flatNumber}, 
              {item.order.activityDetails[0].pickupAddress.addressLine1} </Text>
          <Text style={s.normalText}>{item.order.activityDetails[0].pickupAddress.addressLine2}</Text>
          {/* <Text style={s.normalText}>state: {item.order.activityDetails[0].pickupAddress.state}, 
              {item.order.activityDetails[0].pickupAddress.pinCode}, 
              district: {item.order.activityDetails[0].pickupAddress.district}</Text>     */}
        </View>
        <View style={{marginTop: 10}}>
          <Text style={[s.subHeaderText, {marginBottom: 5}]}>Delivery Address</Text>
          <Text style={s.normalText}>Flat No: {item.order.activityDetails[0].deliveryAddress.flatNumber}, 
              {item.order.activityDetails[0].deliveryAddress.addressLine1} </Text>
          <Text style={s.normalText}>{item.order.activityDetails[0].deliveryAddress.addressLine2}</Text>
          {/* <Text style={s.normalText}>state: {item.order.activityDetails[0].deliveryAddress.state}, 
              {item.order.activityDetails[0].deliveryAddress.pinCode}, 
              district: {item.order.activityDetails[0].deliveryAddress.district}</Text>     */}
        </View>
        
        <View style={styles.horizontalSpaceLine} />
        <View>
          <Button title = "Accept Request" onPress={() => self.assignDeliveryBoy(item.orderId, 'Send')} />
        </View>
    </View>
  )
}

class DriverActivityListing extends React.Component{

    constructor(props) {
        super(props);
        this.state={
          getActivityData: [],
          sendActivityData: [],
          showLoader: false,
          isGetActivity: false,
          getColor: "black",
          sendColor: "blue"
        }
    }

    assignDeliveryBoy = async(id, activityType) => {
      await this.setState({showLoader: true});
      axios.post(
        Config.URL + Config.assignedDeliveryBoy,
        {
          orderId : id,
          activity: true,
          deliveryBoyId: this.props.userData.driverData.userDetails[0].userId,
          // deliveryBoyId : "12345678890"
        },
        {
          headers: {
              'Content-Type': 'application/json',
          }
        }
      )
      .then(res => {
          this.setState({showLoader: false});
          if (res.data.status == 0) {
            Toast.show("Activity assigned to you",Toast.LONG);
            this.props.navigation.navigate('driverOngoingOrder',{isActivity: true, activityType: activityType});
          }
        }).catch(err => {
          Toast.show("Failed to assign activity. Try again",Toast.LONG);
      })
      
    }

    componentDidMount(){
      this.props.navigationAction.driverNavigateAction('Activity');
        axios.post(
            Config.URL + Config.getPlacedOrderDriver,
            {
              activity: true
            },
            {
              headers: {
                  'Content-Type': 'application/json',
              }
            }
        )
        .then(res => {
          // console.warn('response---  send activity list->', res.data.result.sendActivityOrderDetails);
          console.warn('response---  get activity list->', res.data.result.getActivityOrderDetails.order);
            if (res.data.status == 0) {
              this.setState({
                sendActivityData: res.data.result.sendActivityOrderDetails,
                getActivityData: res.data.result.getActivityOrderDetails,
                showLoader: false
              })
              // console.warn('in get placed order', this.state.orderData);
            }
          }).catch(err => {
            console.warn('in error-->', err);
            // this.setState({
            //   showLoader: false
            // })
        });

        this.props.navigation.addListener('willFocus', () =>{
          console.warn('am in focus Home');
          this.props.navigationAction.driverNavigateAction('Activity');
        });
    }

    GetActivities = () => {
      if (this.state.isGetActivity) {
        return (
          <View>
            <FlatList data={this.state.getActivityData}
              renderItem={( { item } ) => (
                <GetActivityListActDriver
                  item={item}
                  self={this}
                />
              )}
              keyExtractor= {item => item.orderId}
              extraData = {this.state.getActivityData}
            />
          </View>
        )
        
      } else {
        return null;
      }
    }

    SendActivities = () => {
      if (!this.state.isGetActivity) {
        return (
          <View>
            <FlatList data={this.state.sendActivityData}
              renderItem={( { item } ) => (
                <SendActivityListActDriver
                  item={item}
                  self={this}
                />
              )}
              keyExtractor= {item => item.orderId}
              extraData = {this.state.sendActivityData}
            />
          </View>
        )
        
      } else {
        return null;
      }
    }


  searchType = (flag) => {
    if(flag == 'send') {
      this.setState ({
        isGetActivity: false,
        sendColor: "blue",
        getColor: "black"
      })
    } else {
      this.setState ({
        isGetActivity: true,
        sendColor: "black",
        getColor: "blue"
      })
    }
    
  }

    render() {
        const {navigate} = this.props.navigation;
        return (
          <Fragment>
            <SafeAreaView style={{flex: 1, backgroundColor: '#332b5c'}}>
              <View style={styles.container}>
                    <AnimatedLoader
                        visible={this.state.showLoader}
                        overlayColor="rgba(255,255,255,0.75)"
                        source={require("../../../assets/loader.json")}
                        animationStyle= {[styles.lottie,{height: 150,width: 150}]}
                        speed={1}
                    />
                    {/* <SceneLoader
                      visible={this.state.showLoader}
                      animation={{
                          fade: {timing: {duration: 1000 }}
                      }}
                    /> */}
                    <View style={styles.searchHeaderView}>
                      <TouchableOpacity style={[styles.searchHeader]}
                        onPress={() => this.searchType('get')}>
                        <Text style={[styles.storeHeading,{color: this.state.getColor}]}>Get Activities</Text>
                      </TouchableOpacity>
                      <TouchableOpacity style={[styles.searchHeader]}
                        onPress={() => this.searchType('send')}>
                        <Text style={[styles.storeHeading,{color: this.state.sendColor}]}>Send Activities</Text>
                      </TouchableOpacity>
                  </View>
                  <ScrollView> 
                    <this.GetActivities />
                    <this.SendActivities />
                  </ScrollView>
                  <DriverNavigationBar navigate={navigate}></DriverNavigationBar>
                </View>
              
            </SafeAreaView>
          </Fragment>
      );
    }

}


let ScreenWidth = Dimensions.get("window").width;
let ScreenHeight = Dimensions.get("window").height;

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#f1f1f1'
    },
    card: {
      flex: 1,
      marginHorizontal: 15,
      marginVertical:10,
      borderRadius: 3,
      borderColor: 'transparent',
      padding: 10,
      shadowColor: "#000",
      elevation: 3,
      backgroundColor: "white"
    },
    horizontalSpaceLine:{
      borderBottomColor : "#808080",
      borderBottomWidth : 3,
      marginBottom: 5,
      marginTop: 5
    },
    categoryFooter:{
        width:ScreenWidth,
        //paddingBottom:20,
        position: "relative",
        bottom: 0,
        height: 50,
        //flexGrow:0.05,
        backgroundColor:'#fff',
        //display:'flex',
        flexDirection:'row',
        justifyContent:"space-evenly",
        alignItems:"center",
        zIndex: 9999999,
        elevation: 4
      },
    searchHeaderView:{
      flexDirection:"row",
      marginHorizontal:20,
      marginTop: 20,
      paddingBottom: 20
    },  
    searchHeader:{
      flex:1, 
      borderBottomWidth:2,
      alignItems:"center"
    },
    storeHeading:{
      color: '#131E3A',
      fontSize:18,
      fontWeight: "bold"
    },
  })


//--------------------------------------------------- store related fuction  -- Start

const mapStateToProps = (state) => {
	return {
    userData: state.loginData,
	};
}

const mapDispatchToProps = (dispatch) => {
    return {
      loginActions: bindActionCreators(loginSuccess,dispatch),
      navigationAction: bindActionCreators(navigateAction,dispatch),
    };
}

// ---------------------------------------------------- store related function  -- END

export default connect(mapStateToProps, mapDispatchToProps)(DriverActivityListing);  






