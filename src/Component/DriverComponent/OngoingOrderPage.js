import React, { Fragment } from 'react';
import { SafeAreaView, StyleSheet, Text, View, FlatList, Button,TouchableOpacity, ToastAndroid, Image, Linking } from 'react-native';
import axios from 'axios';
import * as Config from '../../Constant/Config.js';
import InternateModal from '../ReusableComponent/InternetModal';
import  s  from '../../sharedStyle.js';
import { ScrollView } from 'react-native-gesture-handler';
import DriverNavigationBar from '../ReusableComponent/DriverNavigationBar';
import { connect } from 'react-redux';
import * as loginSuccess  from '../../Actions/LoginAction';
import { bindActionCreators, $CombinedState } from 'redux';
import * as navigateAction  from '../../Actions/NavigationBarAction';
import Toast from 'react-native-simple-toast';
import AnimatedLoader from "react-native-animated-loader";
import StepIndicator from 'react-native-step-indicator';
import ShopDetailsModal from '../ReusableComponent/shopDetailsPopUp';

const customStyles = {
  stepIndicatorSize: 30,
  currentStepIndicatorSize:30,
  separatorStrokeWidth: 2,
  currentStepStrokeWidth: 3,
  stepStrokeCurrentColor: '#fe7013',
  stepStrokeWidth: 2,
  stepStrokeFinishedColor: '#fe7013',
  stepStrokeUnFinishedColor: '#aaaaaa',
  separatorFinishedColor: '#fe7013',
  separatorUnFinishedColor: '#aaaaaa',
  stepIndicatorFinishedColor: '#fe7013',
  stepIndicatorUnFinishedColor: '#ffffff',
  stepIndicatorCurrentColor: '#ffffff',
  stepIndicatorLabelFontSize: 13,
  currentStepIndicatorLabelFontSize: 13,
  stepIndicatorLabelCurrentColor: '#fe7013',
  stepIndicatorLabelFinishedColor: '#ffffff',
  stepIndicatorLabelUnFinishedColor: '#aaaaaa',
  labelColor: '#999999',
  labelSize: 13,
  currentStepLabelColor: '#fe7013'
}

GetActivityListOnGoing = ({item, self}) => {
  return (
    <View style={styles.card}>   
        <View>
          <Text style={[s.subHeaderText, {marginBottom: 5}]}>Order Id:</Text>
          <Text style={s.normalText}>{item.orderId}</Text>
        </View> 
        <View style={styles.horizontalSpaceLine} />  
        <View>
          <Text style={[s.subHeaderText, {marginBottom: 5}]}>User Name:</Text>
          <Text style={s.normalText}>{item.order.userDetails.fullName}</Text>
        </View> 
        <View>
          <Text style={[s.subHeaderText, {marginBottom: 5}]}>Mobile No:</Text>
          <TouchableOpacity style={{flex: 1, flexDirection: "row"}} onPress={() => self.navigateToCall(item.order.userDetails.contactNumber)} >
            <Image style={{width: 20, height: 20}} source={require('../../../assets/call.png')}/>
            <Text style={[s.normalText, {paddingLeft: 15, color:"#00CCFF"}]}>{item.order.userDetails.contactNumber}</Text>  
          </TouchableOpacity>
        </View> 
        <View>
          <Text style={[s.subHeaderText, {marginBottom: 5}]}>Pickup Date:</Text>
          <Text style={s.normalText}>{item.order.pickupDate}</Text>
        </View> 
        {/* <View>
          <Text style={[s.subHeaderText, {marginBottom: 5}]}>Delivery Date:</Text>
          <Text style={s.normalText} >{item.order.deliveryDate}</Text>
        </View>  */}

        <View style={styles.horizontalSpaceLine} />
        <View style={{marginTop: 10}}>
          <Text style={[s.subHeaderText, {marginBottom: 5}]}>First Delivery Address:</Text>
          <Text style={s.normalText}>Flat No: {item.order.activityDetails[0].deliveryAddress.flatNumber}, 
              {item.order.activityDetails[0].deliveryAddress.addressLine1} </Text>
          <Text style={s.normalText}>{item.order.activityDetails[0].deliveryAddress.addressLine2}</Text>
          {/* <Text style={s.normalText}>state: {item.order.activityDetails[0].deliveryAddress.state}, 
              {item.order.activityDetails[0].deliveryAddress.pinCode}, 
              district: {item.order.activityDetails[0].deliveryAddress.district}</Text>     */}
        </View>

        <View style={styles.horizontalSpaceLine} />
        <View style={{justifyContent: "space-between"}}>
          <Text style={[s.subHeaderText, {marginBottom: 5}]}>Pickup Address:</Text>
          <Text style={s.normalText}>Flat No: {item.order.pickupAddress.flatNumber}, 
              {item.order.pickupAddress.addressLine1} </Text>
          <Text style={s.normalText}>{item.order.pickupAddress.addressLine2}</Text>
          {/* <Text style={s.normalText}>state: {item.order.pickupAddress.state}, 
              {item.order.pickupAddress.pinCode}, 
              district: {item.order.pickupAddress.district}</Text>     */}
        </View>
        <View style={styles.horizontalSpaceLine} />
        <View>
          <Button title = "See each delivery location" onPress={() => self.moreDetails(item.orderId, 'show')} />
        </View>
        
        <View style={styles.horizontalSpaceLine} />
        <View>
        { self.RenderElement(item.orderStatus, item.orderId, true) }
        </View>
    </View>
  )
}

SendActivityListOnGoing = ({item, self}) => {
  return (
    <View style={styles.card}>   
        <View>
          <Text style={[s.subHeaderText, {marginBottom: 5}]}>Activity Id:</Text>
          <Text style={s.normalText}>{item.orderId}</Text>
        </View>  
        <View style={styles.horizontalSpaceLine} />
        <View>
          <Text style={[s.subHeaderText, {marginBottom: 5}]}>Package Description:</Text>
          <Text style={s.normalText} numberOfLines={200}>{item.order.activityDetails[0].packageDescription}</Text>
        </View>  
        <View>
          <Text style={[s.subHeaderText, {marginBottom: 5}]}>User Name:</Text>
          <Text style={s.normalText}>{item.order.userDetails.fullName}</Text>
        </View> 
        <View>
          <Text style={[s.subHeaderText, {marginBottom: 5}]}>Mobile No:</Text>
          <TouchableOpacity style={{flex: 1, flexDirection: "row"}} onPress={() => self.navigateToCall(item.order.userDetails.contactNumber)} >
            <Image style={{width: 25, height: 25}} source={require('../../../assets/call.png')}/>
            <Text style={s.normalText}>{item.order.userDetails.contactNumber}</Text>  
          </TouchableOpacity>
        </View> 
        <View>
          <Text style={[s.subHeaderText, {marginBottom: 5}]}>Contact person name:</Text>
          <Text style={s.normalText} >{item.order.activityDetails[0].contactPersonName}</Text>
        </View> 
        {/* <View>
          <Text style={[s.subHeaderText, {marginBottom: 5}]}>Contact person no:</Text>
          <Text style={s.normalText} >{item.order.activityDetails[0].contactPersonMobNo}</Text>
        </View>  */}
        <View>
          <Text style={[s.subHeaderText, {marginBottom: 5}]}>Contact person mobile no:</Text>
          <TouchableOpacity style={{flex: 1, flexDirection: "row"}} onPress={() => self.navigateToCall(item.order.activityDetails[0].contactPersonMobNo)} >
            <Image style={{width: 20, height: 20}} source={require('../../../assets/call.png')}/>
            <Text style={[s.normalText, {paddingLeft: 15, color:"#00CCFF"}]}>{item.order.activityDetails[0].contactPersonMobNo}</Text>  
          </TouchableOpacity>
        </View>        
         
        <View>
          <Text style={[s.subHeaderText, {marginBottom: 5}]}>Pickup Date:</Text>
          <Text style={s.normalText}>{Date(item.order.pickupDate)}</Text>
        </View> 
        {/* <View>
          <Text style={[s.subHeaderText, {marginBottom: 5}]}>Delivery Date:</Text>
          <Text style={s.normalText} >{Date(item.order.deliveryDate)}</Text>
        </View>  */}
        
        <View style={styles.horizontalSpaceLine} />
        <View style={{justifyContent: "space-between"}}>
          <Text style={[s.subHeaderText, {marginBottom: 5}]}>Pickup Address</Text>
          <Text style={s.normalText}>Flat No: {item.order.activityDetails[0].pickupAddress.flatNumber}, 
              {item.order.activityDetails[0].pickupAddress.addressLine1} </Text>
          <Text style={s.normalText}>{item.order.activityDetails[0].pickupAddress.addressLine2}</Text>
          {/* <Text style={s.normalText}>state: {item.order.activityDetails[0].pickupAddress.state}, 
              {item.order.activityDetails[0].pickupAddress.pinCode}, 
              district: {item.order.activityDetails[0].pickupAddress.district}</Text>     */}
        </View>
        <View style={{marginTop: 10}}>
          <Text style={[s.subHeaderText, {marginBottom: 5}]}>Delivery Address</Text>
          <Text style={s.normalText}>Flat No: {item.order.activityDetails[0].deliveryAddress.flatNumber}, 
              {item.order.activityDetails[0].deliveryAddress.addressLine1} </Text>
          <Text style={s.normalText}>{item.order.activityDetails[0].deliveryAddress.addressLine2}</Text>
          {/* <Text style={s.normalText}>state: {item.order.activityDetails[0].deliveryAddress.state}, 
              {item.order.activityDetails[0].deliveryAddress.pinCode}, 
              district: {item.order.activityDetails[0].deliveryAddress.district}</Text>     */}
        </View>
        
        <View style={styles.horizontalSpaceLine} />
        <View>
        { self.RenderElement(item.orderStatus, item.orderId, true) }
        </View>
    </View>
  )
}

GetActivityDetailsListOnGoing = ({item, self}) => {
  return (
    <View style={styles.card}>   
        <View>
          <Text style={[s.subHeaderText, {marginBottom: 5}]}>Activity Tag:</Text>
          <Text style={s.normalText}>{item.activityTag}</Text>
        </View> 
        <View style={styles.horizontalSpaceLine} />
        <View>
          <Text style={[s.subHeaderText, {marginBottom: 5}]}>Package Description:</Text>
          <Text style={s.normalText} numberOfLines={200}>{item.packageDescription}</Text>
        </View> 
        <View>
          <Text style={[s.subHeaderText, {marginBottom: 5}]}>Contact person name:</Text>
          <Text style={s.normalText}>{item.contactPersonName}</Text>
        </View> 
        {/* <View>
          <Text style={[s.subHeaderText, {marginBottom: 5}]}>Contact person mobile no:</Text>
          <Text style={s.normalText}>{item.contactPersonMobNo}</Text>
        </View>  */}
        <View>
          <Text style={[s.subHeaderText, {marginBottom: 5}]}>Contact person mobile no:</Text>
          <TouchableOpacity style={{flex: 1, flexDirection: "row"}} onPress={() => self.navigateToCall(item.contactPersonMobNo)} >
            <Image style={{width: 20, height: 20}} source={require('../../../assets/call.png')}/>
            <Text style={[s.normalText, {paddingLeft: 15, color:"#00CCFF"}]}>{item.contactPersonMobNo}</Text>  
          </TouchableOpacity>
        </View> 
        <View style={styles.horizontalSpaceLine} />
        <View style={{marginTop: 10}}>
          <Text style={[s.subHeaderText, {marginBottom: 5}]}>Delivery Address:</Text>
          <Text style={s.normalText}>Flat No: {item.deliveryAddress.flatNumber}, 
              {item.deliveryAddress.addressLine1} </Text>
          <Text style={s.normalText}>{item.deliveryAddress.addressLine2}</Text>
          {/* <Text style={s.normalText}>state: {item.deliveryAddress.state}, 
              {item.deliveryAddress.pinCode}</Text>     */}
        </View>
        
    </View>
  )
}

OrderListOnGoing = ({item, self}) => {
  return (
    <View style={styles.card}>   
      <View>
        <Text style={[s.subHeaderText, {marginBottom: 5}]}>Order Id:</Text>
        <Text style={s.normalText}>{item.orderId}</Text>
      </View>  
      
      <View style={styles.horizontalSpaceLine} />
      <View style={{marginBottom: 5}}>
          <Text style={[s.subHeaderText, {marginBottom: 5}]}>User Name:</Text>
          <Text style={s.normalText}>{item.order.userDetails.fullName}</Text>
        </View> 
        <View style={{flex: 1, flexDirection:"row", marginBottom: 5}}>
          <Text style={[s.subHeaderText, {marginBottom: 5}]}>Mobile No:</Text>
          <TouchableOpacity style={{flex: 1, flexDirection: "row", marginLeft: 10}} onPress={() => self.navigateToCall(item.order.userDetails.contactNumber)} >
            <Image style={{width: 20, height: 20}} source={require('../../../assets/call.png')}/>
            <Text style={[s.normalText, {paddingLeft: 15, color:"#00CCFF"}]}>{item.order.userDetails.contactNumber}</Text>  
          </TouchableOpacity>
        </View> 
        <View style={{flex: 1, flexDirection:"row", marginBottom: 5}}>
          <Text style={[s.subHeaderText, {marginBottom: 5}]}>Payment Type:</Text>
          <Text style={[s.normalText, { marginLeft: 10 }]}>{item.paymentStatus}</Text>
        </View> 
        {item.paymentStatus == 'COD' && item.order.finalCODAmountToPay 
        ?
        <View style={{flex: 1, flexDirection:"row", marginBottom: 5}}>
          <Text style={[s.subHeaderText, {marginBottom: 5}]}>Payment to collect:</Text>
          <Text style={[s.normalText, { marginLeft: 10 }]}>{item.order.finalCODAmountToPay}</Text>
        </View>
        :
        item.paymentStatus == 'COD'
        ?
        <View style={{flex: 1, flexDirection:"row", marginBottom: 5}}>
          <Text style={[s.subHeaderText, {marginBottom: 5}]}>Payment to collect:</Text>
          <Text style={[s.normalText, { marginLeft: 10 }]}>{item.order.toPay}</Text>
        </View>
        :
        <View style={{flex: 1, flexDirection:"row", marginBottom: 5}}>
          <Text style={[s.subHeaderText, {marginBottom: 5}]}>Paid amount:</Text>
          <Text style={[s.normalText,{ marginLeft: 10 }]}>{item.order.toPay}</Text>
        </View>
        }
      <View style={{justifyContent: "space-between"}}>
        <Text style={[s.subHeaderText, {marginBottom: 5}]}>Delivery Address</Text>
        <Text style={s.normalText}>Flat No: {item.order.deliveryAddress[0].flatNumber}, 
            {item.order.deliveryAddress[0].addressLine1} </Text>
        <Text style={s.normalText}>{item.order.deliveryAddress[0].addressLine2}</Text>
        {/* <Text style={s.normalText}>state: {item.order.deliveryAddress[0].state}, 
            {item.order.deliveryAddress[0].pinCode}, 
            district: {item.order.deliveryAddress[0].district}</Text>     */}
      </View>
      
      <View style={styles.horizontalSpaceLine} />
      <View>
      <View style={{marginVertical: 5}}>
        <Button title = "See Details" onPress={() => self.orderDetails(item.orderId)} />
      </View> 
      <View style={{marginVertical: 5}}>
        { self.RenderElement(item.orderStatus, item.orderId, false) }
      </View> 
      
      </View>
    </View>
  )
}



class DriverOngoingOrder extends React.Component {
  constructor(props) {
    super(props);
    this.state={
      shopCurrentPosition: 0,
      shopLabels: [],
      orderData: [],
      detailsOrder: {},
      isDetailsView: false,
      isActivity: false,
      activityType: '',
      sendActivityData: [],
      getActivityData: [],
      getActivityDetails: false,
      detailsGetActivity: {},
      getColor: "black",
      sendColor: "black",
      orderColor: "black",
      unAvailableProducts: [],
      showUnMark: true,
      showLoader: false,
      shopData: {},
      showShopDetailsModal: false
    }
  }

  componentDidMount() {
    this.props.navigationAction.driverNavigateAction('Ongoing');
    this.setState({showLoader: true});
    console.warn('userIDRRRRR--->',this.props.userData.driverData.userDetails[0].userId);
    axios.post(
        Config.URL + Config.ongoingOrderListDriver,
        {
          deliveryBoyId: this.props.userData.driverData.userDetails[0].userId,
          // deliveryBoyId : "12345678890",
        },
        {
          headers: {
              'Content-Type': 'application/json',
          }
        }
    )
    .then(res => { 
      console.warn('oder details----------->', res.data.result[0].paymentStatus);
        if (res.data.status == 0) {
          this.setState({
            orderData: res.data.result,
          })
          
        }
      }).catch(err => {
        console.warn('get ongoing order error', err);
        
    })

    axios.post(
      Config.URL + Config.ongoingOrderListDriver,
      {
        deliveryBoyId: this.props.userData.driverData.userDetails[0].userId,
        // deliveryBoyId : "12345678890",
        activity: true
      },
      {
        headers: {
            'Content-Type': 'application/json',
        }
      }
  )
  .then(res => {
    this.setState({showLoader: false});
      if (res.data.status == 0) {
        
        this.setState({
          sendActivityData: res.data.result.sendActivityOrderDetails,
          getActivityData: res.data.result.getActivityOrderDetails,
        })
        
      }
    }).catch(err => {
      console.warn('error in get activity', err);
      this.setState({showLoader: false});
  })
    console.warn('params ac type-->', this.props.navigation.state.params);
    if (this.props.navigation.state.params) {
      if(this.props.navigation.state.params.activityType) {
        switch(this.props.navigation.state.params.activityType) {
          case 'get': this.setState({
                        activityType: this.props.navigation.state.params.activityType,
                        getColor: "blue"
                      })
                      break;
          case 'send': this.setState({
                        activityType: this.props.navigation.state.params.activityType,
                        sendColor: "blue"
                      })
                      break;
          case ''     : this.setState({
                        activityType: this.props.navigation.state.params.activityType,
                        orderColor: "blue"
                      })
                      break;
          default     : this.setState({
                        activityType: this.props.navigation.state.params.activityType,
                        orderColor: "blue"
                      })
                      break;
        }
        this.setState ({
          activityType: this.props.navigation.state.params.activityType
        })
      }
      if(this.props.navigation.state.params.isActivity) {
        this.setState({
          isActivity: true
        })
      } else {
        this.setState({
          isActivity: false
        })
      }
    } else {
      this.setState ({
        activityType: 'Send',
        orderColor: "blue"
      })
    }
    
  }

  navigateToCall = (phoneNumber) => {
    Linking.canOpenURL(`tel:${phoneNumber}`)
    .then(supported => {
      if (!supported) {
        console.warn('error', supported);
      } else {
        return Linking.openURL(`tel:${phoneNumber}`)
      }
    })
  }

  checkCanUpdateStatus = async(id) => {
    const i = this.state.orderData.findIndex(ind => ind.orderId === id);
    const shopDetails = this.state.orderData[i].shopOrderStatus;
    let count = 0;
    for (let i = 0; i < shopDetails.length; i++) {
      if (shopDetails[i].status == 'Pending') {
        count++;
      }
    }
     if (count > 0) {
       return false;
     } else {
       return true;
     }
    
  }

  async updateOrderStatus(id,status, isActivity) {
    if (!isActivity) {
      const check = await this.checkCanUpdateStatus(id);
      if (check) {
        this.callUpdateApi(id,status, isActivity);
      } else {
        this.showToaster("Order is not yet approved from all the shops. So, please call the shops before shipping the order");
        // Toast.show("Order is not yet approved from all the shops. So, please call the shops before shipping the order", Toast.LONG);
      }
    } else {
      this.callUpdateApi(id,status, isActivity);
    }
  }

  callUpdateApi(id,status, isActivity) {
    this.setState({showLoader: true});  
    axios.post(
      Config.URL + Config.updateOrderStatus,  
      {
        orderId : id,
        deliveryBoyId: this.props.userData.driverData.userDetails[0].userId,
        // deliveryBoyId : "12345678890",
        orderStatus : status,
        activity: isActivity
      },
      {
        headers: {
            'Content-Type': 'application/json',
        }
      }
    )
    .then(res => {
        console.warn('user status updated---->', res);
        this.setState({showLoader: false});
        if (res.data.status == 0) {
          if (isActivity) {
            this.setState({
              sendActivityData: res.data.result.sendActivityOrderDetails,
              getActivityData: res.data.result.getActivityOrderDetails,
            })
          } else {
            this.setState({
              orderData: res.data.result,
            })
          }
          
          if (status == 'Delivered') {
            this.showToaster("Thanks for serving the order");
            // Toast.show("Thanks for serving the order",Toast.LONG);
          } else {
            this.showToaster("You have updated the order status");
            // Toast.show("You have updated the order status", Toast.LONG);
          }
          
        } else {
          this.showToaster('Failed to update status. Please try again');
          // Toast.show('Failed to update status. Please try again',Toast.LONG);
        }
      }).catch(err => {
        this.showToaster('Failed to update status. Please try again');
        // Toast.show('Failed to update status. Please try again', Toast.LONG);
    })
  }

  orderDelivered = (id) => {
    axios.post(
      Config.URL + Config.orderDeliveredDriver,
      {
          orderId : id,
          // deliveryBoyId : "12345678890"
          deliveryBoyId: this.props.userData.driverData.userDetails[0].userId,
      },
      {
          headers: {
              'Content-Type': 'application/json',
          }
      }
    )
    .then(res => {
        console.warn("res---",res.data);
        if (res.data.status == 0) {
          this.setState({
            orderData: res.data.result,
          })
          this.showToaster("Thanks for serving the order");
          // Toast.show("Thanks for serving the order",Toast.LONG);
        } else {
          this.showToaster("Thanks for serving the order");
          Toast.show("Failed to update status. Please try again",Toast.LONG);
        }
      }).catch(err => {
        this.showToaster("Failed to update status. Please try again");
        // Toast.show("Failed to update status. Please try again",Toast.LONG);
    })
    
  }

  ItemListOnGoing = (item) => {
    return (
      <View style={{marginVertical: 5, elevation: 6, padding: 15, backgroundColor: item.item.backGroundColor}}>
        {!item.item.comboId
        ?
        <>
        <View style={{flex: 1, flexDirection: "row", alignContent:"flex-end"}}>
          <View style={{flex: 4}}>
            <Image style={{width: 100, height: 100}} source={{uri: item.item.productLogo}}/>
          </View>
          <View style={{flex: 8, paddingLeft: 10}}>
            <Text style={s.normalText} numberOfLines={4}>Product Name: {item.item.productName} </Text>
            <Text style={s.normalText} numberOfLines={4}>Unit: {item.item.unit} </Text>
            <Text style={s.normalText}>Quantity: {item.item.counter}</Text>
            {item.item.isBogo 
            ?
                <Text style={[s.subHeaderText]} numberOfLines={2}>Buy 1 Get 1 Free</Text>
            :
                null
            }
          </View>
        </View>
        {/* <View style={{flex: 1, flexDirection: "row"}}>
          <Text style={s.normalText}>Unit:  </Text>
          <Text style={s.normalText} numberOfLines={4}>{item.item.unit}</Text>
        </View> */}
        </>
        :
        <>
        <View style={{flex: 1, flexDirection: "row", alignContent:"flex-end"}}>
          <View style={{flex: 4}}>
            <Image style={{width: 100, height: 100}} source={{uri: item.item.productLogo}}/>
          </View>
          <View>
            <Text style={s.normalText} numberOfLines={4}>Combo Name: {item.item.comboName}</Text>
            <Text style={s.normalText}>Quantity: {item.item.counter}</Text>
          </View>
        </View>
        <View style={{flex: 1, flexDirection: "row", marginTop: 5}}>
          <Text style={s.normalText} numberOfLines={200}>Description: {item.item.comboDescription}</Text>
        </View>
        {/* <View style={{flex: 1, flexDirection: "row", alignContent:"flex-end"}}>
          <Text style={s.subHeaderText}>Combo Name:  </Text>
          <Text style={s.subHeaderText} numberOfLines={2}>{item.item.comboName}</Text>
        </View>
        <View style={{flex: 1, flexDirection: "row"}}>
          <Text style={s.subHeaderText}>Description:  </Text>
          <Text style={s.normalText} numberOfLines={8} >{item.item.productDescription}</Text>
        </View> */}
        </>    
        }
        <View style={{flex: 1, flexDirection: "row", marginTop: 5}}>
          <Text style={s.normalText}>Total cost:  </Text>
          <Text style={s.normalText}>₹{(item.item.totalCost).toFixed(2)}</Text>
        </View>
        <TouchableOpacity style={{flex: 1, flexDirection: "row", marginTop: 5}} onPress={() => this.showShopData(item.item)}>
          <Text style={[s.subHeaderText, {color:"#00CCFF"}]} numberOfLines={3}>Shop name: {item.item.shopName}</Text>
        </TouchableOpacity>
      </View>
    )
  }

  showShopData = async(data) => {
    console.warn('in show Shop Data', data);
    await this.setState({
      shopData: {
        shopName: data.shopName,
        shopContactNumber: data.shopContactNumber,
        shopAddress: data.shopAddress
      }
    });
    this.setState({showShopDetailsModal: true});
  }

  hideModal = async() => {
    await this.setState({
      shopData: {},
      showShopDetailsModal: false
    });
  }

  showToaster = (message) => {
    setTimeout(() => {
        Toast.show(message, Toast.LONG);
    }, 100);
  }


  selectProduct = (productId) => {
    if(!this.state.showUnMark) {
      this.showToaster('You cannot change the status now');
      // Toast.show('You cannot change the status now', Toast.LONG);
    } else {
      const i = this.state.detailsOrder.order.itemList.findIndex(ind => ind.productId === productId);
      if (i >= 0) {
        if (this.state.detailsOrder.order.itemList[i].backGroundColor == 'white') {
          console.warn('push--->',productId);
          this.state.detailsOrder.order.itemList[i].backGroundColor = '#FF0000';
          this.state.unAvailableProducts.push(productId);
          console.warn('push color change',this.state.detailsOrder);
          this.setState({
            unAvailableProducts: this.state.unAvailableProducts,
            detailsOrder: this.state.detailsOrder
          })
  
        } else {
          console.warn('in if');
          this.state.detailsOrder.order.itemList[i].backGroundColor = 'white'; 
          const delIndex = this.state.unAvailableProducts.indexOf(productId);
          if (delIndex >= 0) {
            this.state.unAvailableProducts.splice(delIndex, 1);
          } 
          this.setState({
            unAvailableProducts: this.state.unAvailableProducts,
            detailsOrder: this.state.detailsOrder
          })
        }
  
      }
    }
    
  }

  orderDetails = async(id) => {
    const i = this.state.orderData.findIndex(ind => ind.orderId === id);
    console.warn('this.state.orderData[i].order.itemList',this.state.orderData[i]);
    if (this.state.orderData[i].order.itemList.length > 0) {
      this.state.orderData[i].order.itemList = this.state.orderData[i].order.itemList.map( item => {
        if(item.isProductAvailable == false) {
          console.warn('in 1st cond');
          item.backGroundColor = "#FF0000" 
        } else {
          console.warn('in 2nd cond');
          item.backGroundColor = "white" 
        }
          return item;
      })
    }
    if (this.state.orderData[i].shopOrderStatus) {
      console.warn('shopOrde status found', this.state.orderData[i].shopOrderStatus);
      const shopDetails = this.state.orderData[i].shopOrderStatus;
      const temp = [];
      let count = 0;
      if (shopDetails.length > 1) {
        for (let i = 0; i < shopDetails.length; i++) {
          if (shopDetails[i].status == 'Confirmed' || shopDetails[i].status == 'Rejected') {
            count++;
            if (shopDetails[i].status == 'Confirmed') {
              temp.push(shopDetails[i].shopName + ' (Approved)'); 
            } else {
              temp.push(shopDetails[i].shopName + ' (Rejected)'); 
            }
          }
        }
        for (let i = 0; i < shopDetails.length; i++) {
          if (shopDetails[i].status == 'Pending') {
            count++;
            temp.push(shopDetails[i].shopName + ' (Pending)'); 
          }
        }
      }
      await this.setState({shopLabels: temp, currentPosition: count});
      console.warn('shopLabels===>', temp);
    }
    console.warn('in see order details---', this.state.orderData[i].unavailableProdAmt);
    if(parseFloat(this.state.orderData[i].unavailableProdAmt) > 0) {
      this.setState({showUnMark: false});
    }
    this.setState({
      detailsOrder: this.state.orderData[i],
      isDetailsView: true,
    })
  }

  backtoAssignList = () => {
    console.warn('in back');
    this.setState({
      detailsOrder: {},
      isDetailsView: false,
      showUnMark: true
    })
  }

  GetActivities = () => {
    if (this.state.activityType == 'Get') {
      return (
        <View>
          <FlatList data={this.state.getActivityData}
            renderItem={( { item } ) => (
              <GetActivityListOnGoing
                item={item}
                self={this}
              />
            )}
            keyExtractor= {item => item.orderId}
            extraData = {this.state.getActivityData}
          />
        </View>
      )
      
    } else {
      return null;
    }
  }

  SendActivities = () => {
    if (this.state.activityType == 'Send') {
      return (
        <View>
          <FlatList data={this.state.sendActivityData}
            renderItem={( { item } ) => (
              <SendActivityListOnGoing
                item={item}
                self={this}
              />
            )}
            keyExtractor= {item => item.orderId}
            extraData = {this.state.sendActivityData}
          />
        </View>
      )
      
    } else {
      return null;
    }
  }

  GetActivityDetails = () => {
    if(this.state.getActivityDetails) {
      return (
        <View>
          <FlatList data={this.state.detailsGetActivity}
            renderItem={( { item } ) => (
              <GetActivityDetailsListOnGoing
                item={item}
                self={this}
              />
            )}
            keyExtractor= {item => item.tagId}
            extraData = {this.state.detailsGetActivity}
          />
            <View style={{marginHorizontal: 20, marginBottom: 20}}>
              <Button title = "Back to list" onPress={() => this.moreDetails()} />
            </View>
        </View>
      )
    } else {
      return null;
    }
  }

  moreDetails = (orderId, type) => {
    console.warn('in more details back click');
    if (type == 'show') {
      const i = this.state.getActivityData.findIndex(ind => ind.orderId === orderId);
      
      this.setState ({
        detailsGetActivity: this.state.getActivityData[i].order.activityDetails,
        getActivityDetails: true,
        activityType: '',
        showUnMark: true
      });
    } else {
      this.setState ({
        detailsGetActivity: [],
        getActivityDetails: false,
        activityType: 'Get',
        showUnMark: true
      })
    }
    
  }


searchType = (flag) => {
  if(flag == 'send') {
    this.setState ({
      isActivity: true,
      isGetActivity: false,
      sendColor: "blue",
      getColor: "black",
      activityType: 'Send'
    })
  } else if(flag == 'get') {
    this.setState ({
      isActivity: true,
      isGetActivity: true,
      sendColor: "black",
      getColor: "blue",
      activityType: 'Get'
    })
  } else if (flag == 'order') {
    this.setState ({
      isActivity: false,
      isGetActivity: true,
      orderColor: "blue",
      activityColor: "black"
    })
  } else if (flag == 'activity') {
    this.setState ({
      isActivity: true,
      isGetActivity: false,
      activityColor: "blue",
      orderColor: "black",
      sendColor: "blue",
      getColor: "black",
      activityType: 'Send'
    })
  }
  
}

  RenderElement = (orderStatus, orderId, isActivity) => {
    if(orderStatus == 'Confirmed') {
      return <Button title = "Ship Order" onPress={() => this.updateOrderStatus(orderId,"Shipped", isActivity)} />;
    } else if(orderStatus == 'Shipped') {
      return <Button title = "Out for Delivery" onPress={() => this.updateOrderStatus(orderId, "Out for Delivery", isActivity)} />;
    } else if(orderStatus == 'Cancelled') {
      return <Text style={s.headerText}>Order is Cancelled by User</Text>
    } else {
      return <Button title = "Delivered" onPress={() => this.updateOrderStatus(orderId, "Delivered", isActivity)} />;
    }
  }

  // OrderDisplay = () => {
  //   if(!this.state.isDetailsView) {
  //     return (
  //           <View style={styles.container}>
  //             {/* <InternateModal /> */}  
  //             <FlatList data={this.state.orderData}
  //               renderItem={( { item } ) => (
  //                 <OrderListOnGoing
  //                   item={item}
  //                   self={this}
  //                 />
  //               )}
  //               keyExtractor= {item => item.orderId}
  //               extraData = {this.state.orderData}
  //             />
              
  //           </View>
  //         );
  //   } else {
  //       return (
  //         <View style={styles.containerDetails}>
  //           {/* <InternateModal /> */}  
            
  //           <View style={{flex: 9.5, paddingHorizontal: 15}}>
  //             <TouchableOpacity style={{marginBottom: 10}} onPress={() => this.backtoAssignList()}>
  //               <Text style={[s.subHeaderText,{color:'#00CCFF'}]}> {'<'} Back to list</Text>
  //             </TouchableOpacity>
  //             <View>
  //               <Text style={[s.headerText,{textAlign:"center"}]}> Order Id:</Text>
  //               <Text style={[s.subHeaderText,{textAlign:"center"}]}>{this.state.detailsOrder.orderId}</Text>
  //             </View>
  //             <View style={styles.horizontalSpaceLine} />
  //             <View style={{marginTop: 10}}>
  //               <View>
  //                 <Text style={s.headerText}>Product list:</Text>
  //               </View>
  //               <ScrollView showsVerticalScrollIndicator={false} style={{marginBottom: 10}}>
  //                 <FlatList data={this.state.detailsOrder.order.itemList}
  //                   renderItem={( { item } ) => (
  //                     <ItemListOnGoing
  //                       item={item}
  //                       self={this}
  //                     />
  //                   )}
  //                   keyExtractor= {item => item.productId}
  //                   extraData={this.state.detailsOrder}
  //                 />
  //               </ScrollView>
                
  //             </View>
              
              
  //           </View>
  //           <View style={{flex: 2.5, marginBottom: 5}}>
  //             <View style={{marginTop: 10, flex: 4, flexDirection: "row", backgroundColor: "white", padding: 15}}>
  //               <Text style={s.subHeaderText}>Total amount paid:  </Text>
  //               <Text style={s.subHeaderText}>₹{this.state.detailsOrder.order.toPay}</Text>
  //             </View>
  //             {this.state.showUnMark ?
  //               <View style={{flex: 8}}>
  //               <TouchableOpacity style={{flex:1,backgroundColor:"#F5F5F5",alignItems:"center",justifyContent:"center"}}
  //                 onPress={() => this.markUnavailabe(this.state.detailsOrder.orderId)}>
  //                 <Text style={{fontSize: 14,fontWeight: "bold",color:"#131E3A"}}>Mark Unvavailable</Text>
  //               </TouchableOpacity>
  //               {/* <TouchableOpacity style={{flex:6,backgroundColor:"#00CCFF",alignItems:"center",justifyContent:"center"}}
  //                   onPress={() => this.cancelOrder(this.state.detailsOrder.orderId)}>
  //                 <Text style={[s.subHeaderText,{color:"white"}]}>Cancel Order</Text>
  //               </TouchableOpacity> */}
  //             </View> 
  //             : null

  //             }
              
  //           </View>
              
              
  //         </View>
  //       );
  //   }
  // }


  markUnavailabe = (orderId) => {
    this.setState({showLoader: true});
    console.warn('unavailabe product list', this.state.orderData);
    axios.post(
      Config.URL + Config.markProductUnavailable,
      {
        'deliveryBoyId': this.props.userData.driverData.userDetails[0].userId,
        'orderId': orderId,
        'productId': this.state.unAvailableProducts
      },
      {
        headers: {
            'Content-Type': 'application/json',
        }
      }
    )
    .then(res => {
      this.setState({showLoader: false});
      console.warn('oder details-->', res);
        if (res.data.status == 0) {
          this.showToaster("Selected product mark unavailable successfully");
          // Toast.show("Selected product mark unavailable successfully", Toast.LONG);
          this.setState({showUnMark: false, orderData: res.data.result})
        }
      }).catch(err => {
        
    })
  }
   cancelOrder = (orderId) => {
    console.warn('unavailabe product list', orderId);
   }

  render() {
    const {navigate} = this.props.navigation;
    if(!this.state.isActivity) {
      return (
        <Fragment>
          <SafeAreaView style={{flex: 1, backgroundColor: '#332b5c'}}>
            <View style={styles.container}>
              {/* <InternateModal /> */}  
              <AnimatedLoader
                visible={this.state.showLoader}
                overlayColor="rgba(255,255,255,0.75)"
                source={require("../../../assets/loader.json")}
                animationStyle= {[styles.lottie,{height: 150,width: 150}]}
                speed={1}
              />
              <View style={styles.searchHeaderView}>
                <TouchableOpacity style={[styles.searchHeader]}
                  onPress={() => this.searchType('order')}>
                  <Text style={[styles.storeHeading,{color: this.state.orderColor}]}>Orders</Text>
                </TouchableOpacity>
                <TouchableOpacity style={[styles.searchHeader]}
                  onPress={() => this.searchType('activity')}>
                  <Text style={[styles.storeHeading,{color: this.state.activityColor}]}>Activities</Text>
                </TouchableOpacity>
              </View>
              {/* <this.OrderDisplay /> */}
              {!this.state.isDetailsView 
              ?
                <View style={styles.container}>
                  {/* <InternateModal /> */}  
                  <FlatList data={this.state.orderData}
                    renderItem={( { item } ) => (
                      <OrderListOnGoing
                        item={item}
                        self={this}
                      />
                    )}
                    keyExtractor= {item => item.orderId}
                    extraData = {this.state.orderData}
                  />
                  
                </View>
                : 
                <ScrollView style={styles.containerDetails}>
                    {/* <InternateModal /> */}  
                    
                    <View style={{flex: 1, paddingHorizontal: 15}}>
                      <TouchableOpacity style={{marginBottom: 10}} onPress={() => this.backtoAssignList()}>
                        <Text style={[s.subHeaderText,{color:'#00CCFF'}]}> {'<'} Back to list</Text>
                      </TouchableOpacity>
                      
                      <View style={{borderBottomColor: "silver", borderBottomWidth: 1, marginBottom: 15}}>
                        <Text style={[s.headerText,{textAlign:"center"}]}> Order Id:</Text>
                        <Text style={[s.subHeaderText,{textAlign:"center", marginBottom: 5}]}>{this.state.detailsOrder.orderId}</Text>
                      </View>
                      {this.state.shopLabels.length > 1 
                      ?
                        <View style={{marginBottom: 10}}>
                          <StepIndicator
                            customStyles={customStyles}
                            currentPosition={this.state.shopCurrentPosition}
                            labels={this.state.shopLabels}
                            stepCount={this.state.shopLabels.length}
                          />
                        </View>
                      :
                        null
                      }
                      <View style={styles.horizontalSpaceLine} />
                      <View style={{marginTop: 10}}>
                        <View>
                          <Text style={s.headerText}>Product list:</Text>
                        </View>
                        <View showsVerticalScrollIndicator={false} style={{marginBottom: 20}}>
                          <FlatList data={this.state.detailsOrder.order.itemList}
                            renderItem={( { item } ) => (
                              <this.ItemListOnGoing
                                item={item}
                                self={this}
                              />
                            )}
                            keyExtractor= {item => item.productId}
                            extraData={this.state}
                          />
                        </View>
                        
                      </View>
                      
                      
                    </View>
                      
                      
                  </ScrollView>

              }
              <DriverNavigationBar navigate={navigate}></DriverNavigationBar> 
              <ShopDetailsModal navigation = {navigate} visible={this.state.showShopDetailsModal} onDismiss={this.hideModal} shopData={this.state.shopData}></ShopDetailsModal>
            </View>
          </SafeAreaView>
        </Fragment>
        
        
      )
    } else {
      return (
        <Fragment>
          <SafeAreaView style={{flex: 1, backgroundColor: '#332b5c'}}>
            <View style={styles.container}>
              {/* <InternateModal /> */} 
              <AnimatedLoader
                  visible={this.state.showLoader}
                  overlayColor="rgba(255,255,255,0.75)"
                  source={require("../../../assets/loader.json")}
                  animationStyle= {[styles.lottie,{height: 150,width: 150}]}
                  speed={1}
                />
              <View style={[styles.searchHeaderView]}>
                <TouchableOpacity style={[styles.searchHeader]}
                  onPress={() => this.searchType('order')}>
                  <Text style={[styles.storeHeading,{color: this.state.orderColor}]}>Orders</Text>
                </TouchableOpacity>
              </View> 
              <View style={styles.searchHeaderView}>
                <TouchableOpacity style={[styles.searchHeader]}
                  onPress={() => this.searchType('get')}>
                  <Text style={[styles.storeHeading,{color: this.state.getColor}]}>Get Activities</Text>
                </TouchableOpacity>
                <TouchableOpacity style={[styles.searchHeader]}
                  onPress={() => this.searchType('send')}>
                  <Text style={[styles.storeHeading,{color: this.state.sendColor}]}>Send Activities</Text>
                </TouchableOpacity>
              </View>
              <ScrollView> 
                <this.GetActivities />
                <this.SendActivities />
                <this.GetActivityDetails />
              </ScrollView>
              <DriverNavigationBar navigate={navigate}></DriverNavigationBar> 
            </View>
        
          </SafeAreaView>
      
        </Fragment>
        
    )
    }
  }
}



const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f1f1f1'
  },
  containerDetails: {
    flex: 1,
  },
  card: {
    flex: 1,
    marginHorizontal: 15,
    marginVertical:10,
    borderRadius: 3,
    borderColor: 'transparent',
    padding: 10,
    shadowColor: "#000",
    elevation: 3,
    backgroundColor: "white"
  },
  horizontalSpaceLine:{
    borderBottomColor : "#808080",
    borderBottomWidth : 3,
    marginBottom: 5,
    marginTop: 5
  },
  horizontalSpaceInLine: {
    borderBottomColor : "#808080",
    borderBottomWidth : 1,
    marginBottom: 5,
    marginTop: 5
  },
  searchHeaderView:{
    flexDirection:"row",
    marginHorizontal:20,
    marginTop: 20,
    paddingBottom: 20
  },  
  searchHeader:{
    flex:1, 
    borderBottomWidth:2,
    alignItems:"center"
  },
  storeHeading:{
    color: '#131E3A',
    fontSize:18,
    fontWeight: "bold"
  },
})


//--------------------------------------------------- store related fuction  -- Start

const mapStateToProps = (state) => {
	return {
    userData: state.loginData,
	};
}

const mapDispatchToProps = (dispatch) => {
    return {
      loginActions: bindActionCreators(loginSuccess,dispatch),
      navigationAction: bindActionCreators(navigateAction,dispatch),
    };
}

// ---------------------------------------------------- store related function  -- END

export default connect(mapStateToProps, mapDispatchToProps)(DriverOngoingOrder);  