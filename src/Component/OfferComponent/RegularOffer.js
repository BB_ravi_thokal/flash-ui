/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import axios from 'axios';
import * as Config from '../../Constant/Config.js';
import * as Constant from '../../Constant/Constant.js';
import NavigationBar from '../ReusableComponent/NavigationBar.js'
import  s  from '../../sharedStyle.js'
import InternateModal from '../ReusableComponent/InternetModal';
import ShopNavigationBar from '../ReusableComponent/ShopNavigationBar'; 
import * as navigateAction  from '../../Actions/NavigationBarAction';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  Dimensions,
  TouchableOpacity,
  TextInput,
  Image,
  ToastAndroid,
  FlatList,
} from 'react-native';
import Toast from 'react-native-simple-toast';

class RegularOfferPage extends React.Component{

  constructor(props) {
    super(props);
    this.state={
        offers: []
    }
  }


  OfferListing = ({item}) => {
      return (
        <View style={[styles.prodcutListingView]}>
            <View style={{flexDirection:"row"}}>
                <View style={styles.addCardImage}>
                    <View style={{width: 80, height: 80, flex: 1}}>
                      <Image style={{width: undefined, height: undefined, flex: 1, resizeMode:"contain"}} source={{uri: item.offerLogo}}/>
                    </View>
                </View>
                <View style={{flex: 8, alignItems:"center"}}>
                    <Text style={s.subHeaderText} numberOfLines={1}>PromoCode: </Text>
                    <Text style={[s.subHeaderText, {color:"#00CCFF", paddingTop: 10}]} numberOfLines={8}>{item.coupon} </Text>
                </View>
            </View>
            <View style={styles.stores}>
                <View style={{paddingVertical: 5}}>
                    <Text style={s.subHeaderText} numberOfLines={1}>Description: </Text>
                    <Text style={s.normalText} numberOfLines={8}>{item.offerDescription} </Text>
                </View>
                <View style={{paddingVertical: 5}}>
                    <Text style={s.subHeaderText} numberOfLines={1}>Terms and conditions: </Text>
                    <Text style={s.normalText} numberOfLines={8}>{item.offerTermsCondition} </Text>
                </View>
            </View>
        </View>
      )
  }

  componentDidMount(){
    
    axios.post(
      Config.URL + Config.applyCoupon,
      {
          "userId": this.props.userData.userData.userId,
      },
      {
        headers: {
            'Content-Type': 'application/json',
        }
      }
    )
    .then(res => {
      console.warn('response of coupon-->', res.data.result);
        if (res.data.status == 0) {
          this.setState({
            offers: res.data.result
          })

        }
        else if(res.data.status == 1){
          Toast.show('No Coupons available for you',Toast.LONG);
          
        }
      }).catch(err => {
        console.warn(err);
    });
    
    // axios.post(
    //     Config.URL + Config.getAllBidForShop,
    //     {
    //     },
    //     {
    //       headers: {
    //           'Content-Type': 'application/json',
    //       }
    //     }
    // )
    // .then(res => {
    //     console.warn("res---",res.data.result);
    //     if (res.data.status == 0) {
    //       this.setState({
    //         activeBids: res.data.result
    //       })
          
    //     }  

    //   }).catch(err => {
    //       console.warn(res.data.status);
    //   })

  }

  render() {
    const {navigate} = this.props.navigation;
      return (
        <SafeAreaView style={{flex: 1}}>
          <View style={{flex: 1}}>
            <View style={[s.bodyGray, {paddingVertical: 15}]}>
              {/* <InternateModal /> */}
              <ScrollView showsVerticalScrollIndicator = {false}>
                  <FlatList data={this.state.offers}
                    renderItem={( { item } ) => (
                        <this.OfferListing
                            item={item}
                        />
                    )}
                    keyExtractor= {item => item.offerId}
                    extraData = {this.state}
                  />
                </ScrollView>
            </View>
          </View>
          
        </SafeAreaView>
        
      )
   
  }

}


let ScreenWidth = Dimensions.get("window").width;
let ScreenHeight = Dimensions.get("window").height;

const styles = StyleSheet.create({
  scrollView: {
    flex: 1
  },
  imgBlank: {
    height: ScreenHeight,
    width: ScreenWidth
  },
  containerIcons:{
    width:30,
    height:30,
  },

  searchShops:{ 
    height: 40, 
    borderColor: '#7285A5', 
    borderWidth: 1,
    paddingHorizontal:10, 
    borderRadius:4 
  },
  sectionContainer: {
    marginTop: 20,
    paddingHorizontal: 24,
  },
  iconView: {
    width: 30,
    height: 30,
    marginRight: 20,
    borderRadius: 100,
  },
  storeCardLayout:{
    marginHorizontal: 15,
    paddingVertical:14,
    borderBottomWidth: 2,
    borderBottomColor: "#131E3A",
    alignItems: "center"
  },
  categoryFooter:{
    width:ScreenWidth,
    paddingBottom:20,
    height:80,
    flexGrow:0.05,
    backgroundColor:'white',
    display:'flex',
    flexDirection:'row',
    justifyContent:"space-evenly",
    alignItems:"center"
  },
  storeDtlContainer: {
    marginHorizontal: 15,
    paddingVertical: 10
  },

  storeContainer:{
    display:"flex",
    flexDirection: "row"
  },
  stores: {
    paddingVertical: 15,
    flex: 1,
    flexDirection: "column"
  },

  storeCount: {
    fontSize: 22,
    fontWeight: 'bold',
    paddingHorizontal: 10,
  },
  storeProductHeading: {
    fontWeight: 'bold',
    fontSize: 30,
  },
  shopdtlCards: {
    width: 80,
    height: 80,
    backgroundColor: 'orange',
    borderRadius: 100,
    borderWidth: 0.5,
    borderColor: 'black',
    marginVertical: 5,
    marginHorizontal: 10,
  },
  storeCardDisplay: {
    // padding: 10,
    paddingVertical: 10,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  storeCards: {
    height: 100,
    borderRadius: 4,
    borderWidth: 0.5,
    borderColor: 'black',
    backgroundColor: 'white',
    flex: 3,
  },
  advertiseData: {
    height: 200,
    width: ScreenWidth,
    borderBottomLeftRadius: 90,
    borderBottomRightRadius: 90,
    elevation: 4
  },
  storeHeading: {
    fontSize:24,
    
  },
  storeDesc: {
    alignItems: "center"
  },
  trendingShops: {
    borderBottomWidth: 2,
    borderColor: "#131E3A",
    marginHorizontal: 15,
    paddingTop: 10,
  },
  addCardImage: {
    flex: 4
  },
  productImage: {
    width: 80,
    height: 80,
  },
  storeCardButton: {
    backgroundColor: 'white',
    color: 'black',
    width: 30,
    height: 30,
    borderRadius: 50,
    alignItems: "center",
    justifyContent: 'center',
  },
  storeborder: {
    borderBottomColor: 'grey',
    borderBottomWidth: 1,
  },
  flexRow: {
    flex: 1,
    textAlignVertical: "bottom",
    flexDirection: 'row',
  },
  prodcutListingView: {
    flexDirection: 'row',
    marginVertical: 5,
    backgroundColor: "white",
    padding: 10,
    marginHorizontal: 10,
    elevation: 4

},
  flexColumnAlignEnd: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-end'
  },
  storeDtlCenter: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "center"
  },
  Mystores: {
    alignItems: "flex-start",
    justifyContent: "space-between",
    textAlignVertical: "bottom",
    flex: 1
    // paddingBottom:15,
  },
  storeCardSpace: {
    marginBottom: 15,
  },
  headerstyle: {
    paddingBottom: 20,
    paddingTop: 20
  },

  storeBtnViewColor: {
    backgroundColor: '#ba55d3',
    color: 'white',
  },
  continueBtnViewColor: {
    backgroundColor: '#2f4f4f',
    color: 'white',
  },
  addtoCartBtnColor: {
    fontSize:12,
    borderWidth: 1,
    borderColor: '#0080FE',
    marginLeft: 20,
  },
  storeBtn: {
    paddingVertical: 5,
    paddingHorizontal: 5,
    marginBottom: 5,
    color: 'white',
    borderRadius: 0,
    width: 50
  },
  basketBtn:{
    paddingVertical: 5,
    paddingHorizontal: 10,
    marginBottom: 10,
    borderRadius: 20,
    height: 30,
    width: 30
  },
  basketImg: {
    height: 30,
    width: 30,
  }

});

//--------------------------------------------------- store related fuction  -- Start

const mapStateToProps = (state) => {
	return {
    userData: state.loginData,
	}
}

const mapDispatchToProps = (dispatch) => {
    return {
      navigationAction: bindActionCreators(navigateAction,dispatch),
    };
}

// ---------------------------------------------------- store related function  -- END


export default connect(mapStateToProps, mapDispatchToProps)(RegularOfferPage);


