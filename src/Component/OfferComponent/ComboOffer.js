/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import axios from 'axios';
import * as Config from '../../Constant/Config.js';
import NavigationBar from '../ReusableComponent/NavigationBar.js'
import  s  from '../../sharedStyle.js' 
import * as loginSuccess  from '../../Actions/LoginAction';
import * as navigateAction  from '../../Actions/NavigationBarAction';
import {AccordionList} from "accordion-collapse-react-native";
import AnimatedLoader from "react-native-animated-loader";
import PopupModal from '../ReusableComponent/PopupComponent.js';
import CartModal from '../ReusableComponent/cartModal';
import * as cartModalAction  from '../../Actions/CartModalAction';
import * as addToCart  from '../../Actions/CartItem';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  Dimensions,
  TouchableOpacity,
  TextInput,
  Image,
  ToastAndroid,
  FlatList,
  Clipboard
} from 'react-native';
import Toast from 'react-native-simple-toast';
import Accordion from 'react-native-collapsible/Accordion';


class ComboOfferPage extends React.Component{

  constructor(props) {
    super(props);
    this.state= { 
        comboOfferData: [],
        showLoader: false,
        cart: {},
        modalVisible: false,
        shopId: '',
        paddingBottomHeight: 0,
        extraItemModal: false,
        test: true,
        offers: [],
        searchType: 'ComboOffers',
        comboBBColor: "#0080FE",
        regOfferBBColor: "#7285A5",
        collapsed: false,
        cartModalMessage: '',
        cartRemoveModal: false,
        activeSections: [],
    }
  }

  componentDidMount(){
    
    this.setState({showLoader: true});
    
    if (this.props.cartState.cartState) {
      this.setState({paddingBottomHeight: 80});
    };
    this.getComboOffers();

    
  }

  getRegularOffers = () => {
    axios.post(
        Config.URL + Config.applyCoupon,
        {
            "userId": this.props.userData.userData.userId,
        },
        {
          headers: {
              'Content-Type': 'application/json',
          }
        }
      )
      .then(res => {
        console.warn('response of coupon-->', res.data.result);
          if (res.data.status == 0) {
            this.setState({
              offers: res.data.result,
              showLoader: false
            });
            this.setState({
                regOfferBBColor: "#0080FE",
                comboBBColor: "#7285A5",
                comboOfferData: [],
                searchType: "RegularOffers",
            });
  
          }
          else if(res.data.status == 1){
            this.setState({showLoader: false});
            Toast.show('No Coupons available for you',Toast.LONG); 
          }
        }).catch(err => {
            this.setState({showLoader: false});
          console.warn(err);
    });
  }

  getComboOffers = () => {
    axios.post(
        Config.URL + Config.getComboOffers,
        {
          headers: {
              'Content-Type': 'application/json',
          }
        }
      )
      .then(res => {
        console.warn('response of combo-->', res.data.result);
          if (res.data.status == 0) {
              this.setState({comboOfferData: res.data.result, showLoader: false});
              this.setState({
                searchType: "ComboOffers",
                regOfferBBColor: "#7285A5",
                comboBBColor: "#0080FE",
                data: []
            });
          }
          else {
          this.setState({showLoader: false});
            Toast.show(res.data.message,Toast.LONG);
            
          }
        }).catch(err => {
          this.setState({showLoader: false});
          console.warn(err);
    })
  }


    // ----

    // _renderSectionTitle = sections => {
    //     return (
    //         <View bordered style={{flex: 1, backgroundColor:"white", paddingHorizontal: 15, marginBottom: 5}}>
    //             <View style={{paddingVertical: 15, flex: 1, flexDirection:"row"}}>
    //                 <View style={{flex: 6, alignItems: "flex-start"}}>
    //                     <Text style={s.subHeaderText}>{sections.comboCategory}</Text>
    //                 </View>
    //                 <View style={{flex: 6, alignItems: "flex-end"}}>
    //                     <Image style={{width: 20, height: 20}} source={require('../../../assets/downArrow.png')}/>
    //                 </View>
    //             </View>
    //         </View>
    //     );
    //   };
     
      _renderHeader = sections => {
        return (
            <View bordered style={{flex: 1, backgroundColor:"white", paddingHorizontal: 15, marginBottom: 5}}>
                <View style={{paddingVertical: 15, flex: 1, flexDirection:"row"}}>
                    <View style={{flex: 6, alignItems: "flex-start"}}>
                        <Text style={s.subHeaderText}>{sections.comboCategory}</Text>
                    </View>
                    <View style={{flex: 6, alignItems: "flex-end"}}>
                        <Image style={{width: 20, height: 20}} source={require('../../../assets/downArrow.png')}/>
                    </View>
                </View>
            </View>
        );
      };
     
      _renderContent = sections => {
        return (
            <View style={{marginBottom:15, flex: 1}}>
                <FlatList data={sections.comboDetails}
                    renderItem={( { item } ) => (
                        <this.ComboListing
                            item={item}
                            self={this}                      
                        />
                    )}
                    keyExtractor= {item => item.comboId}
                    extraData={this.state.test}
                />
                {/* <Text>sbdkcjkjsbdfkjbksjdbvkjbskdbvk jbk jdfbkjxcv kjsdk</Text> */}
            </View>
        );
      };
     
      _updateSections = activeSections => {
        this.setState({ activeSections });
      };
    //---

    AccordianHead(item){
        return(
            <View bordered style={{flex: 1, backgroundColor:"white", paddingHorizontal: 15, marginBottom: 5}}>
                <View style={{paddingVertical: 15, flex: 1, flexDirection:"row"}}>
                    <View style={{flex: 6, alignItems: "flex-start"}}>
                        <Text style={s.subHeaderText}>{item.comboCategory}</Text>
                    </View>
                    <View style={{flex: 6, alignItems: "flex-end"}}>
                        <Image style={{width: 20, height: 20}} source={require('../../../assets/downArrow.png')}/>
                    </View>
                </View>
            </View>
        );
    }

    AccordianBody = (item) => {
        return (
            <View style={{marginBottom:15, flex: 1}}>
                <FlatList data={item.comboDetails}
                    renderItem={( { item } ) => (
                        <this.ComboListing
                            item={item}
                            self={this}                      
                        />
                    )}
                    keyExtractor= {item => item.comboId}
                    extraData={this.state.test}
                />
                {/* <Text>sbdkcjkjsbdfkjbksjdbvkjbskdbvk jbk jdfbkjxcv kjsdk</Text> */}
            </View>
        );
    }

    ComboListing({item, self}) {
        return (
            <View style={{backgroundColor: "white", paddingHorizontal: 15, paddingVertical: 20}}>
                <View style={{flex: 1, flexDirection:"row"}}>
                    <View style={{flex: 8}}>
                        <Text style={[s.subHeaderText]}>{item.comboName}</Text>
                        {item.comboPrice == item.comboOfferPrice
                        ?   
                            <Text style={[s.subHeaderText, {marginTop: 15}]}>Price: {item.comboOfferPrice}</Text>
                        : 
                            <>
                            <Text style={[s.subHeaderText, {textDecorationLine: 'line-through', textDecorationStyle: 'solid', marginTop: 15}]}>Regular price: {item.comboPrice}</Text>
                            <Text style={[s.subHeaderText, {marginTop: 5}]}>Today's offer: {item.comboOfferPrice}</Text>
                            </>
                        }
                        <Text style={[s.normalText]}></Text>
                    </View>
                    <View style={{flex: 4, width: 100, height: 100}}>
                        <Image style={{flex: 1, width: undefined, height: undefined, resizeMode:"contain"}}
                        source={{uri: item.comboLogo}}></Image>
                    </View>
                    
                </View>
                <View style={{flex: 1, marginTop: 5}}>
                    <Text style={[s.normalText, {fontSize: 12}]} numberOfLines={10}>{item.comboDescription}</Text>
                </View>
                {/* <TouchableOpacity onPress={() => self.addCombo(item)} style={{flex: 1, marginTop: 5, alignItems: "flex-end"}}>
                    <View style={[styles.storeBtn, styles.addtoCartBtnColor]}>
                        <Text style={{ color: '#0080FE',paddingHorizontal: 4}}>ADD</Text>
                    </View>
                </TouchableOpacity> */}
                {self.props.cartItem.cartItem.findIndex(ind => ind.comboId === item.comboId) >= 0
                ?
                    <View style={styles.flexRowAdd}>
                        <TouchableOpacity onPress={() => self.decrementCount(item)}>
                            <View style={styles.orderCardButton}>
                            <Text style={{fontSize:18,color:"#0080FE"}}>-</Text>
                            </View>
                        </TouchableOpacity>
                            {/* <Text style={styles.orderCount}>{item.counter}</Text> */}
                            <Text style={styles.orderCount}>{self.props.cartItem.cartItem[self.props.cartItem.cartItem.findIndex(ind => ind.comboId === item.comboId)].counter}</Text>
                        <TouchableOpacity onPress={() => self.incrementCount(item)}>
                            <View style={styles.orderCardButton}>
                            <Text style={{fontSize:18,color:"#0080FE"}}>+</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                :
                    <View style={{flex: 1, alignItems: "flex-end"}}>
                        <TouchableOpacity style={[styles.storeBtn, styles.addtoCartBtnColor]} onPress={() => self.addCombo(item)}>
                            <Text style={{ color: '#0080FE',paddingHorizontal: 4}}>ADD</Text>
                        </TouchableOpacity>
                    </View>
                    
    
                }
            </View>
        )
    }

   

  render() {
    const {navigate} = this.props.navigation;
      return (
        <SafeAreaView style={{flex: 1}}>
            <View style={[s.bodyGray,{flex: 1}]}>
                <ScrollView showsVerticalScrollIndicator={false}>
                    <AnimatedLoader
                        visible={this.state.showLoader}
                        overlayColor="rgba(255,255,255,0.75)"
                        source={require("../../../assets/loader.json")}
                        animationStyle= {[styles.lottie,{height: 150,width: 150}]}
                        speed={1}
                    />
                    <View style={{paddingBottom: this.state.paddingBottomHeight, marginTop: 25}}>
                        <View style={styles.searchHeaderView}>
                            <TouchableOpacity style={[styles.searchHeader,{ borderBottomColor: this.state.comboBBColor}]}
                                onPress={() => this.searchType('ComboOffers')}>
                                <Text style={styles.storeHeading}>Combo offer</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={[styles.searchHeader,{borderBottomColor: this.state.regOfferBBColor}]}
                                onPress={() => this.searchType('RegularOffers')}>
                                <Text style={styles.storeHeading}>Regular offer</Text>
                            </TouchableOpacity>
                        </View>

                         <Accordion
                            sections={this.state.comboOfferData}
                            activeSections={this.state.activeSections}
                            renderSectionTitle={this._renderSectionTitle}
                            renderHeader={this._renderHeader}
                            renderContent={this._renderContent}
                            onChange={this._updateSections}
                        />

                        {/* {this.state.searchType == 'ComboOffers'
                        ?
                        <View>
                            <AccordionList
                                list={this.state.comboOfferData}
                                header={this.AccordianHead}
                                body={this.AccordianBody}
                                keyExtractor={item => `${item.comboId}`}
                            />
                        </View>
                        : 
                        null
                        } */}
                        {/* {this.state.searchType == 'ComboOffers' && !this.state.test
                        ?
                        <View>
                            <AccordionList
                                list={this.state.comboOfferData}
                                header={this.AccordianHead}
                                body={this.AccordianBody}
                                keyExtractor={item => `${item.comboId}`}
                            />
                        </View>
                        : 
                        null
                        } */}
                        {this.state.searchType != 'ComboOffers'
                        ?
                        <View style={{flex: 1}}>
                            <FlatList data={this.state.offers}
                                renderItem={( { item } ) => (
                                    <this.OfferListing
                                        item={item}
                                    />
                                )}
                                keyExtractor= {item => item.offerId}
                                extraData = {this.state}
                            />
                        </View>
                        :
                        null
                        }
                       
                    </View>
                      
                </ScrollView>
                <CartModal navigate={navigate} navigationPage={'orderDetailsPage'}></CartModal>
                <PopupModal visible={this.state.modalVisible} onDismiss={this.hideModal} removeProduct={this.userAckCart} 
                    message={this.state.cartModalMessage} 
                    btnTag={"Add to cart"}/>  
                <PopupModal visible={this.state.extraItemModal} onDismiss={this.hideModal} removeProduct={this.navigateToFlashMart} 
                    message={"if you need any extra item along with this combo, get it from flash mart."} 
                    btnTag={"Flash mart"}/>  
                <PopupModal visible={this.state.cartRemoveModal} onDismiss={this.hideModal} removeProduct={this.removeProduct} 
                    message={"Are you sure want to remove the product from cart?"} btnTag={"Remove"}/>
            </View>
            
        </SafeAreaView>
        
      )
   
  }

  toggleAccordian = (item) => {
    console.warn('here item', item);
  } 

  OfferListing = ({item}) => {
    return (
      <View style={[styles.prodcutListingView]}>
          <View style={{flex: 1, flexDirection:"row"}}>
              <View style={styles.addCardImage}>
                  <View style={{width: 80, height: 80, flex: 1}}>
                    <Image style={{width: undefined, height: undefined, flex: 1, resizeMode:"contain"}} source={{uri: item.offerLogo}}/>
                  </View>
              </View>
              <View style={{flex: 8, alignItems:"center"}}>
                  <Text style={s.subHeaderText} numberOfLines={1}>PromoCode: </Text>
                  <View style={{flexDirection:"row"}}>
                    <Text style={[s.subHeaderText, {color:"#00CCFF", paddingTop: 10}]} numberOfLines={8}>{item.coupon} </Text>
                    <TouchableOpacity onPress={() => this.copyToClipboard(item.coupon)}>
                        <Image style={{width: 30, height: 30, padding: 5, marginLeft: 15, marginTop: 5}} source={require('../../../assets/copy.png')}/>
                    </TouchableOpacity>
                  </View>
                  
              </View>
          </View>
          <View style={styles.stores}>
              <View style={{paddingVertical: 5}}>
                  <Text style={s.subHeaderText} numberOfLines={1}>Description: </Text>
                  <Text style={s.normalText} numberOfLines={8}>{item.offerDescription} </Text>
              </View>
              <View style={{paddingVertical: 5}}>
                  <Text style={s.subHeaderText} numberOfLines={1}>Terms and conditions: </Text>
                  <Text style={s.normalText} numberOfLines={8}>{item.offerTermsCondition} </Text>
              </View>
          </View>
      </View>
    )
}

    copyToClipboard = (code) => {
        Clipboard.setString(code);
        Toast.show("Coupon copied", Toast.LONG);
    }

    incrementCount = async(productData) => {
        this.setState({
            selectedProduct: productData,
        });
        let index = this.props.cartItem.cartItem.findIndex(ind => ind.comboId === productData.comboId);
        this.props.cartState.cartCount += 1;
        this.props.cartState.cartSum = parseFloat(this.props.cartState.cartSum) + parseFloat(this.props.cartItem.cartItem[index].unitCost); 
        this.props.cartModalAction.changeCartState(true,this.props.cartState.cartSum,this.props.cartState.cartCount);
        this.props.cartItem.cartItem[index].counter = this.props.cartItem.cartItem[index].counter + 1;
        this.props.cartItem.cartItem[index].totalCost = this.props.cartItem.cartItem[index].unitCost * this.props.cartItem.cartItem[index].counter;
        this.props.actions.replaceCart(this.props.cartItem.cartItem);
        await this.setState({ shopMenu: this.state.shopMenu});
        const temp = this.state.comboOfferData;
        await this.setState({ comboOfferData: [] });
        await this.setState({ comboOfferData: temp });
    }
    
    decrementCount = async(productData) => {
        console.warn('productData');
        this.setState({
            selectedProduct: productData
        });
        let id = this.props.cartItem.cartItem.findIndex(ind => ind.comboId === productData.comboId);
        if (id >= 0) {
            if(this.props.cartItem.cartItem[id].counter - 1 <= 0){
                this.setState({
                    cartRemoveModal: true
                });
            } else {
                this.props.cartItem.cartItem[id].counter = this.props.cartItem.cartItem[id].counter - 1;
                this.props.cartItem.cartItem[id].totalCost = this.props.cartItem.cartItem[id].unitCost * this.props.cartItem.cartItem[id].counter;
                this.props.cartState.cartCount -= 1;
                this.props.cartState.cartSum = parseFloat(this.props.cartState.cartSum) - parseFloat(this.props.cartItem.cartItem[id].unitCost); 
                this.props.cartModalAction.changeCartState(true,this.props.cartState.cartSum,this.props.cartState.cartCount);
                this.props.actions.replaceCart(this.props.cartItem.cartItem);
                await this.setState({ shopMenu: this.state.shopMenu});
                const temp = this.state.comboOfferData;
                await this.setState({ comboOfferData: [] });
                await this.setState({ comboOfferData: temp });
            }
        }
    }
    
    removeProduct = async() => {
        this.setState({
            cartRemoveModal: false,
            removeProduct: true,
            modalVisible: false
        });
        const i = this.props.cartItem.cartItem.findIndex(ind => ind.comboId === this.state.selectedProduct.comboId);
        
        this.props.cartState.cartCount -= 1;
        this.props.cartState.cartSum = parseFloat(this.props.cartState.cartSum) - parseFloat(this.props.cartItem.cartItem[i].unitCost); 
        
        this.props.cartItem.cartItem.splice(i,1);
        if (this.props.cartState.cartCount === 0){
            this.props.cartModalAction.changeCartState(false,this.props.cartState.cartSum,this.props.cartState.cartCount);
        } else {
            this.props.cartModalAction.changeCartState(true,this.props.cartState.cartSum,this.props.cartState.cartCount);
        }
        this.props.actions.replaceCart(this.props.cartItem.cartItem);
        await this.setState({ shopMenu: this.state.shopMenu});
        const temp = this.state.comboOfferData;
        await this.setState({ comboOfferData: [] });
        await this.setState({ comboOfferData: temp });
    }
  

    //function to add the new item in the cart   --- Start
    addCombo =async(comboDetails) => {
        console.warn('in add combo--->', comboDetails.shopDetails.deliveryTime);
        const cart = {
            comboId: comboDetails.comboId,
            comboName: comboDetails.comboName,
            productLogo: comboDetails.comboLogo,
            comboDescription: comboDetails.comboDescription,
            comboPrice: parseFloat(comboDetails.comboPrice),
            unitCost: parseFloat(comboDetails.comboOfferPrice),
            comboMenu: comboDetails.comboMenu,
            shopId: comboDetails.shopId,
            shopName: comboDetails.shopName,
            category: comboDetails.comboCategory,
            counter: 1,
            totalCost: parseFloat(comboDetails.comboOfferPrice), 
            isProductAvailable: true,
            cgst: parseFloat(comboDetails.cgst),
            sgst: parseFloat(comboDetails.sgst),
            costExlGst: parseFloat(comboDetails.comboPriceExclTax),
            shopRate: parseFloat(comboDetails.shopRate),
            shopAddress: comboDetails.shopDetails.shopAdress,
            shopContactNumber: comboDetails.shopDetails.shopContactNumber,
            shopEmail: comboDetails.shopDetails.shopEmail,
            shopLocation: comboDetails.shopDetails.shopLocation,
            shopOwnerName: comboDetails.shopDetails.shopOwnerName,
            deliveryTime: comboDetails.shopDetails.deliveryTime   
        }
    
        await this.setState({
          cart: cart,
          shopId: comboDetails.shopId 
        })
        
        // console.warn('testtttt-->', this.props.cartItem.cartItem.some(arr => arr.shopId == this.state.shopId));
        console.warn('1st cart-->', this.props.cartItem.cartItem);
        if(this.props.cartItem.cartItem.some(arr => arr.comboId == comboDetails.comboId)){
          Toast.show("Already added in the cart",Toast.LONG);
        } else if (!this.props.cartItem.cartItem.some(arr => arr.shopId == this.state.shopId)) {
            if (this.props.cartItem.cartItem.length > 0) {
                if (!this.props.cartItem.cartItem.some(arr => arr.deliveryTime == comboDetails.shopDetails.deliveryTime)) {
                    this.setState({
                        modalVisible: true,
                        cartModalMessage: 'Delivery time and delivery charges of your order added in cart depends upon the delivery time of the individual shop and number of shop you shopped, if ordered time crosses the delivery time of any shop(s) then the whole order will be delivered tomorrow or next day.'
                    })
                } else {
                    this.setState({
                        modalVisible: true,
                        cartModalMessage: 'Delivery charges may vary as per the changes in different shop locations. Would you like to continue with your purchase?'
                    });
                }
            } else {
                this.confirmAddCart(cart);
            }
        }
        else{
          this.confirmAddCart(cart);
        }
      }
    
    confirmAddCart = async(cart) => {
        this.props.cartState.cartCount += 1;
        this.props.cartState.cartSum = (parseFloat(this.props.cartState.cartSum) + parseFloat(cart.totalCost)); 
        this.props.cartModalAction.changeCartState(true,this.props.cartState.cartSum,this.props.cartState.cartCount);
        await this.props.actions.addToCart(cart);
        console.warn('2nd cart-->', this.props.cartItem.cartItem);
        this.setState({paddingBottomHeight: 100});
        Toast.show(cart.comboName + " is added in the cart",Toast.LONG);
        const temp = this.state.comboOfferData;
        await this.setState({ comboOfferData: [] });
        await this.setState({ comboOfferData: temp });
        this.setState({test: !this.state.test, extraItemModal: true});
        console.warn('testttt', this.state.test);
    }
    
    userAckCart = () => {
        this.setState({
          modalVisible: false
        })
        this.confirmAddCart(this.state.cart);
    
    }

    hideModal = () => { 
        console.warn('in hide modal');
        this.setState({
          modalVisible: false,
          extraItemModal: false,
          cartRemoveModal: false,
        });
    }

    navigateToFlashMart = () => {
        this.setState({
            test: !this.state.test,
            searchType: 'ComboOffers',
            extraItemModal: false
        })
        this.props.navigation.navigate('flashHubPage');
    }

    checkLogin = async () => {
        if (Object.keys(this.props.userData.userData).length > 0) { 
          if (this.props.userData.userData.userDetails.length > 0) {
            return true;
          }
          return false;
        }
        return false;
    }

    searchType = async(type) => {
        console.warn("in searchType method",type);
        if(type === "RegularOffers"){
            const isLogin = await this.checkLogin();
            if (isLogin) {
                this.setState({showLoader: true});
                this.getRegularOffers();
            } else {
                Toast.show('Please login in application to get offers', Toast.LONG); 
            }
        }
        else{
            this.setState({showLoader: true});
            this.getComboOffers();
        }
        
    }
}


let ScreenWidth = Dimensions.get("window").width;
let ScreenHeight = Dimensions.get("window").height;

const styles = StyleSheet.create({
    advertiseData: {
        height: 220,
        width: ScreenWidth,
    },
    transcript: {
        textAlign: 'center',
        color: '#B0171F',
        marginBottom: 1,
        top: '400%',
      },
    imageView: {
        flex: 1,
        width: undefined,
        height: undefined,
        resizeMode: 'contain',
    },
    prodcutListingView: {
        marginVertical: 5,
        backgroundColor: "white",
        padding: 10,
        marginHorizontal: 10,
        elevation: 6,
        flex: 1
    
    },
    addCardImage: {
        flex: 4
    },
    productImage: {
        width: 80,
        height: 80,
    },
    stores: {
        paddingVertical: 15,
        flex: 1,
    },
    Mystores: {
        alignItems: "flex-end",
        textAlignVertical: "bottom",
        flex: 2,
        marginTop: 5
        // paddingBottom:15,ß
    },
    storeBtn: {
        paddingVertical: 5,
        paddingHorizontal: 5,
        marginHorizontal: 15,
        marginVertical: 15,
        color: 'white',
        borderRadius: 0,
        width: 100,
        alignItems:"center",
        justifyContent: 'flex-end',
    },
    addtoCartBtnColor: {
        fontSize:12,
        borderWidth: 1,
        borderColor: '#0080FE',
        marginLeft: 20,
    },
    searchHeaderView:{
        // flex:1,
        flexDirection:"row",
        // borderBottomWidth: 2,
        // borderBottomColor: "grey",
        marginHorizontal:20,
        paddingBottom: 20
    },
    flexRowAdd: {
        marginHorizontal: 15,
        marginVertical: 15, 
        // paddingTop: 10,
        textAlignVertical: "bottom",
        flexDirection: 'row',
        justifyContent: 'flex-end'
    },
    storeHeading:{
        color: '#131E3A',
        fontSize:18,
        paddingHorizontal: 15,
        fontWeight: "bold"
    },
    searchHeader:{
        flex:1, 
        borderBottomWidth:2,
        alignItems:"center"
    },
    orderCardButton:{
        borderColor:'#0080FE',
        borderWidth: 1,
        width:25,
        height:25,
        borderRadius:50,
        alignItems:"center",
        justifyContent:'center',
    },
    orderCount:{
        fontSize:18,
        fontWeight:'bold',
        paddingHorizontal:10,
        color: '#131E3A'
    },
});

//--------------------------------------------------- store related fuction  -- Start

const mapStateToProps = (state) => {
	return {
    cartItem: state.cartItem,
    cartState: state.cartState,
    userData: state.loginData,
	}
}

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(addToCart, dispatch),
        cartModalAction: bindActionCreators(cartModalAction,dispatch),
        loginActions: bindActionCreators(loginSuccess,dispatch),
        navigationAction: bindActionCreators(navigateAction,dispatch),
    };
}
// ---------------------------------------------------- store related function  -- END


export default connect(mapStateToProps, mapDispatchToProps)(ComboOfferPage);


