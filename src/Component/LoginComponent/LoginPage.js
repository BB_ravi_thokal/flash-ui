/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Fragment } from 'react';
import axios from 'axios';
import * as Config from '../../Constant/Config.js';
import * as Constant from '../../Constant/Constant.js';
import  s  from '../../sharedStyle.js';
// import 'bootstrap/dist/css/bootstrap.min.css';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  Button,
  TextInput,
  StatusBar,
  TouchableWithoutFeedback,
  TouchableOpacity,
  Image,
  ToastAndroid
} from 'react-native';
// import { Container, Row, Col } from 'reactstrap';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import Toast from 'react-native-simple-toast';
// import {Validation} from '../ReusableComponent/Validate.js';
// import validate from '../ReusableComponent/ValidateWrapper.js';
// import TextField from '../ReusableComponent/TextField.js';


class LoginPage extends React.Component{
    // constructor(props) {
    //     super(props)
    
    //     this.state = {
    //       email: '',
    //       emailError: '',
    //       password: '',
    //       passwordError: ''
    //     }
    // }
    // register() {
    //     const emailError = validate('email', 'abc')
    //     const passwordError = validate('password', this.state.password)
    
    //     this.setState({
    //       emailError: emailError,
    //       passwordError: passwordError
    //     })
    
    //     if (!emailError && !passwordError) {
    //       alert('Details are valid!')
    //     }
    // }

    constructor(props) {
        super(props);
        this.state={
            emailId:"",
            password:"",
            showGenerateButton: true,
            isPresent: '',
            emailErrorColor: 'black',
            emailErrorBorderWidth: 0,
            passwordErrorColor: 'white',
            passwordErrorWidth: 0
        }
    }
    render() {
        const {navigate} = this.props.navigation;
        return (
            <ScrollView style={{height:'100%'}}>
                <View style={s.body}>
                    <View style={styles.logoContainer}>
                        <Image style={styles.logoStyle} source={require('../../../assets/Logo.png')}/>
                    </View>
                    <View style={styles.loginHeaderView}>
                        <View style={styles.loginContainer}>
                            <View style={styles.headerstyle}>
                                <Text style={styles.loginText}>LOG IN</Text>
                            </View>
                            <View>
                                <View style={styles.socialIcons}>
                                    <TouchableOpacity>
                                        <Image style={styles.iconView} source={require('../../../assets/fbIcon.png')}/>
                                    </TouchableOpacity>
                                    <TouchableOpacity>
                                        <Image style={styles.iconView} source={require('../../../assets/googleIcon.png')}/>
                                    </TouchableOpacity>
                                    
                                </View>
                                <View>
                                    <Text style={{ color: '#A9A9A9' }}>or login with your mobile number</Text>
                                </View>
                            </View>
                        </View>
                        <View style={styles.inputBoxView}>
        
                            {/* <TextField  onChangeText={value => this.setState({email: value.trim()})}
                                onBlur={() => {
                                    this.setState({
                                    emailError: validate('email', this.state.email)
                                    })
                                }}
                                error={this.state.emailError} 
                                inlineImageLeft='email' style={styles.emailInput} placeholder="Enter your email" placeholderTextColor="#9a73ef">
                            </TextField>
                            <TextField  onChangeText={value => this.setState({password: value.trim()})}
                                onBlur={() => {
                                    this.setState({
                                    passwordError: validate('password', this.state.password)
                                    })
                                }}
                                error={this.state.passwordError}
                                secureTextEntry={true}
                                inlineImageLeft='password' style={styles.passwordInput} placeholder="*******" placeholderTextColor="#9a73ef" >
                            </TextField> */}

                            <TextInput inlineImageLeft='email' style={[styles.emailInput, {borderColor: this.state.emailErrorColor,borderWidth: this.state.emailErrorBorderWidth}]} placeholder="Enter your email" placeholderTextColor="#9a73ef"
                                value={this.state.emailId}
                                keyboardType = {'numeric'} maxLength={10}
                                onChangeText={(emailId) => {
                                this.setState({
                                    emailId: emailId
                                })}} >
                            </TextInput>
                            <TextInput inlineImageLeft='password' style={[styles.passwordInput, {borderColor: this.state.passwordErrorColor, borderWidth: this.state.passwordErrorWidth}]} placeholder="*******" placeholderTextColor="#9a73ef"
                                secureTextEntry={true} value={this.state.password}
                                onChangeText={(password) => {
                                this.setState({
                                    password: password
                                })}} >
                            </TextInput>

                        </View>
                        <View style={{ display: 'flex', flexDirection: "row",marginRight: 40}} >
                            <TouchableOpacity style={{flex: 1, justifyContent: "flex-start"}} onPress={() => navigate('signUp')}>
                                {/* <Text style={{ color: '#A9A9A9'}}>Create account</Text> */}
                                <Text style={{ color: '#A9A9A9'}}>Create account</Text>
                                <Text style={{ color: '#A9A9A9'}}>Referral code login</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={{flex: 1,alignItems: "flex-end"}} onPress={() => navigate('forgotPassword')}>
                                <Text style={{ color: '#A9A9A9'}}>Forgot password?</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.loginFooterView}>
                            <TouchableOpacity onPress={() => this.authenticateUser(this.state.emailId,this.state.password)}>
                                <View style = {styles.loginButton1}>
                                    <Text  style={{ color: 'white' }} title="LOGIN">LOGIN</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={styles.boxBackground}></View>
                </View>
            </ScrollView>
          );
    }

    showToaster = (message) => {
        setTimeout(() => {
            Toast.show(message, Toast.LONG);
        }, 100);
    }

    authenticateUser = (email,pass) => {
        axios.post(
            Config.URL + Config.loginUser,
            {
                "contactNumber": email,
                "password": pass,
            },
            {
                headers: {
                    'Content-Type': 'application/json',
                }
            }
        )
        .then(res => {
            //console.warn(res.data.status,res.data.roleName);
            if (res.data.status == 0 && res.data.roleName != "") {
                // this.setState({
                //     showGenerateButton: false,
                //     showLogin: true,
                //     loading: false
                // })
                //Toast.show(senOTPMessage, Toast.SHORT)
                this.showToaster(res.data.message);
                // Toast.show(res.data.message,Toast.LONG);
                this.props.navigation.navigate('homePage');
            }
            else if(res.data.status == 2 && res.data.roleName == ""){  // wrong password
                console.warn(res.data.message)
                this.setState({
                    emailErrorColor: "white",
                    emailErrorBorderWidth: 0,
                    passwordErrorColor: "red",
                    passwordErrorWidth: 1,
                });
                this.showToaster(res.data.message);
                // Toast.show(res.data.message,Toast.LONG);
                // Toast.show(
                //     this.data.message,
                //     Toast.LONG,
                //     ToastAndroid.TOP,
                //     25,
                //     50,
                // );
            }
            else if(res.data.status == 3 ){ // userID wrong
                console.warn(res.data.status)
                this.setState({
                    emailErrorColor: "red",
                    emailErrorBorderWidth: 1,
                    passwordErrorColor: "red",
                    passwordErrorWidth: 1,
                })
                Toast.show(res.data.message,Toast.LONG);
                // this.setState({
                //     loading: false
                // })
                //Toast.show(this.data.message,Toast.SHORT);
                // Toast.showWithGravityAndOffset(
                //     this.data.message,
                //     Toast.LONG,
                //     ToastAndroid.BOTTOM,
                //     25,
                //     50,
                // );
            }
        }).catch(err => {
            //console.log('err', err)
            // this.setState({
            //     loading: false
            // })
        })
    }
      
}

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,
  },
  body: {
    backgroundColor: 'grey',
  },
  loginButton1: {
    paddingVertical:10,
    paddingHorizontal:20,
    marginRight:-10,
    marginBottom:-7,
    backgroundColor: 'navy',
    color:'white',
  },
  logoContainer: {
    height: 250,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    // height:50
    // flex: 11,
    // flexDirection: 'column'
  },
  loginFooterView:{
    marginRight: 20,
    paddingBottom: 40,
    paddingTop: 40,
    height:150,
    display:'flex',
    alignItems:'flex-end',
  },
  boxBackground: {
    position: 'absolute',
    top: 0,
    right: 0,
    bottom:0,
    width: 10,
    backgroundColor: 'navy'
  },
  logoView: {
    height: 50,
    width: 50,
    backgroundColor: 'grey',
    // height: 50
    // flex: 2,
    // backgroundColor: '#EBECF0',
    // justifyContent: 'center'
  },
  iconView: {
    width: 30,
    height: 30,
    marginRight: 20,
    borderRadius: 100,
  },
  logoStyle: {
    height: 200,
    width: 150
    // marginLeft: 20
  },
  loginContainer:{
    marginBottom: 10,
  },
  socialIcons: {
    height: 30,
    display: 'flex',
    flexDirection: 'row',
    marginBottom: 10,
  },
  loginHeaderView: {
    paddingLeft: 20,
    // flex: 3,
    backgroundColor: '#EBECF0'
  },
  headerstyle: {
    paddingBottom: 20
    // flex: 1,
    // marginLeft: 20,
    // marginRight: 20
  },

  loginText: {
    fontWeight: 'bold',
    fontSize: 30
  },
  inputBoxView: {
    // flex: 4,
    // marginLeft: 20,
    // marginRight: 20,
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
    marginBottom:10,
    marginRight:30,
  },
  passwordInput: {
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    padding: 10,
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10
  },
  emailInput: {
    height: 40,
    borderBottomWidth: 1,
    borderBottomColor: 'grey',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    padding: 10,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    
    
  },
});


export default LoginPage;
