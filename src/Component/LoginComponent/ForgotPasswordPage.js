/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Fragment } from 'react';
import  s  from '../../sharedStyle.js';
import * as Config from '../../Constant/Config.js';
import InternateModal from '../ReusableComponent/InternetModal';
import axios from 'axios'; 
import { encrypt } from 'react-native-simple-encryption';
import Toast from 'react-native-simple-toast';
// import 'bootstrap/dist/css/bootstrap.min.css';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  Button,
  TextInput,
  StatusBar,
  Image,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Dimensions,
  ToastAndroid,
  KeyboardAvoidingView
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

class ForgotPasswordPage extends React.Component{
  constructor(props) {
    super(props);
    this.state={            
      contactNumber: "",
      password:"",
      confirmPassword:"",
      passColor: "white",
      passBorder: 0,
      confPassColor: "white",
      confPassBorder: 0,
      isFormValid: false,
      key: ""
    }
  }

  componentDidMount() {
    this.setState ({
      contactNumber: this.props.navigation.state.params.contactNumber
    })
  }



    render() {
        const {navigate} = this.props.navigation;
        return (
          <SafeAreaView style={{flex: 1}}>
              <View style={{flex:1,flexDirection:"row"}}>
              
                {/* <InternateModal /> */}
                <KeyboardAvoidingView style={styles.forgotPasswordContainer} behavior="padding">
                  <View style={styles.logoContainer}>
                      <Image style={styles.logoStyle} source={require('../../../assets/Logo.png')}/>
                      <Text style={styles.logoText}>FLASH</Text>
                  </View>
                  <View style={{flex:1,marginLeft: 20}} behavior="padding"> 
                      <View style={styles.socialLogoContainer}>
                        <View style={styles.headerstyle}>
                            <Text style={styles.loginText}>RESET PASSWORD</Text>
                        </View>
                      </View>
                      <View style={{flex:2}}>
                        <TextInput inlineImageLeft='password' style={[styles.passwordInput, {borderColor: this.state.passColor , borderWidth: this.state.passBorder}]}
                            placeholder="New password" placeholderTextColor="#000"
                              secureTextEntry={true} 
                              onChangeText={(password) => {
                              this.setState({
                                  password: password,
                                  passBorder: 0,
                                  passColor: "white",
                                  confPassBorder: 0,
                                  confPassColor: "white"
                              })}} >
                          </TextInput>

                          <TextInput inlineImageLeft='password' style={[styles.confirmPasswordInput, {borderColor: this.state.confPassColor ,borderWidth: this.state.confPassBorder}]}
                            placeholder="Confirm password" placeholderTextColor="#000"
                              secureTextEntry={true} 
                              onChangeText={(confirmPassword) => {
                              this.setState({
                                  confirmPassword: confirmPassword,
                                  confPassBorder: 0,
                                  confPassColor: "white",
                                  passBorder: 0,
                                  passColor: "white",
                              })}} >
                          </TextInput>
                      </View>
                  </View>
                  <View style={{flex:1, justifyContent: "center",alignItems:"flex-end",}}>
                      <TouchableOpacity onPress={() => this.resetPassword()}
                        style={{alignItems:"center",backgroundColor:"#332b5c",padding:15,width:ScreenWidth/4,borderTopLeftRadius:30,borderBottomLeftRadius:30}}>
                              <Text  style={{ color: 'white' }}>RESET</Text>
                      </TouchableOpacity>
                  </View>
              </KeyboardAvoidingView>
              <View style={{flex:0.5,backgroundColor:"#332b5c"}}></View> 
            </View>
            
    
          </SafeAreaView>

        
      );    
    }

    showToaster = (message) => {
      setTimeout(() => {
          Toast.show(message, Toast.LONG);
      }, 100);
    }

    resetPassword = async() => {
      await this.setState({ isFormValid: true });
      if (this.state.password == "") {
        await this.setState({
          isFormValid: false,
          passBorder: 1,
          passColor: "red"
        })
      } else if (this.state.confirmPassword == "") {
        await this.setState({
          isFormValid: false,
          confPassBorder: 1,
          confPassColor: "red"
        })
      } else if (this.state.password != this.state.confirmPassword) {
        await this.setState({
          isFormValid: false,
          confPassBorder: 1,
          confPassColor: "red",
          passBorder: 1,
          passColor: "red"
        })
      }
      let password = encrypt(Config.encryptionKey, this.state.password);
      if (this.state.isFormValid) {
        axios.post(
          Config.URL + Config.resetPassword,
          {
            "contactNumber": this.state.contactNumber,
            "password": password
          },
          {
            headers: {
                'Content-Type': 'application/json',
            }
          }
      )
      .then(res => {
        if (res.data.status == 0 ) {
            this.props.navigation.navigate('login');
        } else {
          this.setState({
            showLoader: false,
          });
          this.showToaster("Something went wrong! Please try again later");
        }
      }).catch(err => {
        this.setState({
          showLoader: false
        });
        this.showToaster("Something went wrong!  Please try again later");

      })
    }  
  }
}

let ScreenHeight = Dimensions.get("window").height;
let ScreenWidth = Dimensions.get("window").width;

const styles = StyleSheet.create({
  forgotPasswordContainer:{
    flex:11.5,
    backgroundColor:"#f1f1f1"
  },
  logoContainer:{
    flex:2,
    justifyContent: 'center',
    alignItems: 'center',
    
  },
  logoStyle: {
    height: 200,
    width: 150
  },
  logoText: {
    fontWeight: 'bold',
    fontSize: 30,
    fontStyle:"italic"

  },
  socialIcons: {
    height: 30,
    display: 'flex',
    flexDirection: 'row',
    marginBottom: 10,
  },
  iconView: {
    width: 30,
    height: 30,
    marginRight:20
    
  },
  socialLogoContainer:{
    flex:1,
  },
  headerstyle: {
    paddingBottom: 10
  },
  loginText: {
    fontWeight: 'bold',
    fontSize: 30
  },

  passwordInput: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    height:50,
    padding: 10,
    marginRight:20,
    // borderBottomWidth: 1,
    // borderBottomColor: 'grey',
    //borderRadius: 10,
     borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    borderBottomWidth: 1,
    borderBottomColor: 'grey',
  },
  emailInput: {
    height: 50,
    borderBottomWidth: 1,
    borderBottomColor: 'grey',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    padding: 10,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    marginRight:20
  },
  confirmPasswordInput: {
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    padding: 10,
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
    marginRight:20,
    
  },
});

export default ForgotPasswordPage;
