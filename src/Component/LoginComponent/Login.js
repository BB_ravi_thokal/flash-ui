/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Fragment } from 'react';
import axios from 'axios';
import { connect } from 'react-redux';
import * as loginSuccess  from '../../Actions/LoginAction';
import * as basketAction  from '../../Actions/BasketAction';
import * as cartModalAction  from '../../Actions/CartModalAction';
import * as addToCart  from '../../Actions/CartItem';
import { bindActionCreators } from 'redux';
import * as Config from '../../Constant/Config.js';
import * as Constant from '../../Constant/Constant.js';
import AnimatedLoader from "react-native-animated-loader";
import InternateModal from '../ReusableComponent/InternetModal';
import * as navigateAction  from '../../Actions/NavigationBarAction';

import * as walletAction  from '../../Actions/WalletAction';
// import firestore from '@react-native-firebase/firestore';
// import SceneLoader from 'react-native-scene-loader';
 // import 'bootstrap/dist/css/bootstrap.min.css';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  Button,
  TextInput,
  StatusBar,
  TouchableWithoutFeedback,
  TouchableOpacity,
  Image,
  ToastAndroid,
  Dimensions,
  KeyboardAvoidingView,
  AsyncStorage,
  Platform
} from 'react-native';
import Toast from 'react-native-simple-toast';
import { encrypt } from 'react-native-simple-encryption';
import s from '../../sharedStyle';
// import messaging from '@react-native-firebase/messaging';
// import { Container, Row, Col } from 'reactstrap';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import { NavigationEvents } from 'react-navigation';
import ValidationComponent from 'react-native-validation';
import { GoogleSignin, GoogleSigninButton, statusCodes } from '@react-native-community/google-signin';
import messaging from '@react-native-firebase/messaging';
import { NavigationActions, StackActions } from 'react-navigation';
import { appleAuth, AppleButton } from '@invertase/react-native-apple-authentication';
// const resetAction = StackActions.reset({
//   index: 0,
//   actions: [NavigationActions.navigate({ routeName: 'login' })
//   ]
// });

// this.props.navigation.dispatch(resetAction);

class Login extends React.Component{
   backHandler;
    constructor(props) {
        super(props);
        this.state={
            emailId:"",
            password:"",
            showGenerateButton: true,
            isPresent: '',
            emailErrorColor: 'black',
            emailErrorBorderWidth: 0,
            passwordErrorColor: 'white',
            passwordErrorWidth: 0,
            showLoader: false,
            firebaseToken: '',
            deviceType: '',
            gmailUserName: '',
            gmailEmailId: '',
            skipLogin: false,
            callingPage: ''
        }
        // this.backHandler =  BackHandler.addEventListener('hardwareBackPress', this._backAndroid);
    }

    

    componentDidMount () {
      if(this.props.navigation.state.params) { 
        this.setState({ skipLogin: true, callingPage: this.props.navigation.state.params.callingPage }); 
      }

      this.setState({deviceType: Platform.OS});
      this.getFirebaseToken();
      // this.alertOnMessageReceive();
      // this.props.actions.loginSuccess({});
      // this.props.actions.driverLoginSuccess({});
      // this.getFirebaseToken();
      // this.getFirebaseToken();
    // this.addRealTimeData();
    // this.alertOnMessageReceive();
      GoogleSignin.configure({
        // scopes: ['https://www.googleapis.com/auth/drive.readonly'], // what API you want to access on behalf of the user, default is email and profile
        webClientId: '880449304349-ul59h1sspd1e3faud25a1n14fgfe5nns.apps.googleusercontent.com', // client ID of type WEB for your server (needed to verify user ID and offline access)
        offlineAccess: false, // if you want to access Google API on behalf of the user FROM YOUR SERVER
        scopes: ['profile', 'email'],
        // hostedDomain: '', // specifies a hosted domain restriction
        // loginHint: '', // [iOS] The user's ID, or email address, to be prefilled in the authentication UI if possible. [See docs here](https://developers.google.com/identity/sign-in/ios/api/interface_g_i_d_sign_in.html#a0a68c7504c31ab0b728432565f6e33fd)
        forceConsentPrompt: true, // [Android] if you want to show the authorization prompt at each login.
        // accountName: '', // [Android] specifies an account name on the device that should be used
        // iosClientId: '<FROM DEVELOPER CONSOLE>', // [iOS] optional, if you want to specify the client ID of type iOS (otherwise, it is taken from GoogleService-Info.plist)
      });
    }

    getFirebaseToken = async() => {
      // Get the device token
      messaging()
        .getToken()
        .then(token => {
          console.warn('refreget token token lllogin-->',token);
          this.setState({firebaseToken: token})
          return saveTokenToDatabase(token);
        });
  
      // Listen to whether the token changes
      return messaging().onTokenRefresh(token => {
        this.setState({firebaseToken: token})
        saveTokenToDatabase(token);
        console.warn('refresh token login-->',token);
      });
    }

    // async saveTokenToDatabase(token) {
    //   // Assume user is already signed in
    //   const userId = auth().currentUser.uid;
    
    //   // Add the token to the users datastore
    //   await firestore()
    //     .collection('users')
    //     .doc(userId)
    //     .update({
    //       tokens: firestore.FieldValue.arrayUnion(token),
    //     });
    // }
  
    // backgroundHandlerFireBase = () => {
    //   messaging().setBackgroundMessageHandler(async remoteMessage => {
    //     console.warn('Message handles in the background!', remoteMessage);
    //   });
    // }
    
    // alertOnMessageReceive = () => {
    //   messaging().onMessage(async remoteMessage => {
    //     // console.warn('A new FCM message arrived!', JSON.stringify(remoteMessage));
    //     Alert.alert('A new FCM message arrived!', JSON.stringify(remoteMessage));
    //   });
  
    // }


    signIn = async () => {
      try {
        await GoogleSignin.hasPlayServices();
        const userInfo = await GoogleSignin.signIn();
        console.warn('token', userInfo);
        await this.setState({
          gmailUserName: userInfo.user.name,
          gmailEmailId: userInfo.user.email
        });
        this.checkGmailLogin();
        // console.warn('User', userInfo.user.name);
      } catch (error) {
        console.warn('google sign in error===>', error);
        await this.signOut();
        if (error.code === statusCodes.SIGN_IN_CANCELLED) {
          // user cancelled the login flow
          this.showToaster('You cancelled google login');
          // Toast.show('You cancelled google login',Toast.LONG);
        } else if (error.code === statusCodes.IN_PROGRESS) {
          // operation (f.e. sign in) is in progress already
          this.showToaster('Please wait, signing is in progress');
          // Toast.show('Please wait, signing is in progress',Toast.LONG);
        } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
          // play services not available or outdated
          this.showToaster('Play service app not available or outed in your device');
          // Toast.show('Play service app not available or outed in your device',Toast.LONG);
        } else {
          this.showToaster('Sorry, not able to proceed with google login. Please register your self');
          // Toast.show('Sorry, not able to proceed with google login. Please register your self',Toast.LONG);
          // some other error happened
        }
      }
    };


  
    signOut = async () => {
      try {
        await GoogleSignin.revokeAccess();
        await GoogleSignin.signOut();
        setUserInfo(null); // Remember to remove the user from your app's state as well
        await this.setState({
          gmailUserName: '',
          gmailEmailId: ''
        });
      } catch (error) {
        console.warn(error);
      }
    };  


    render() {
        const {navigate} = this.props.navigation;
        return (
          <SafeAreaView style={{flex: 1}}>
            <View style={{flex: 1}}>
              {/* <InternateModal /> */} 
              
                <View style={{flex:1,flexDirection:"row"}}>
                    
                  <View style={styles.loginContainer}>
                    <AnimatedLoader
                      visible={this.state.showLoader}
                      overlayColor="rgba(255,255,255,0.75)"
                      source={require("../../../assets/loader.json")}
                      animationStyle= {[styles.lottie,{height: 150,width: 150}]}
                      speed={1}
                      />
                      {/* <SceneLoader
                        visible={this.state.showLoader}
                        animation={{
                            fade: {timing: {duration: 1000 }}
                        }}
                      /> */}
                    <View style={{ flex:1, marginRight: 10,paddingVertical:10, alignItems: "flex-end"}} >
                        <TouchableOpacity style={{backgroundColor:"white", width: 100, margin: 15, borderRadius: 50}} onPress={() => this.skipLogin('')}>
                          <Text style={[s.subHeaderText, {paddingVertical: 10, paddingHorizontal: 20, alignItems:"center", color:"#00CCFF"}]}>Skip ></Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.logoContainer}>
                        <Image style={styles.logoStyle} source={require('../../../assets/Logo.png')}/>
                        <Text style={[styles.logoText]}>FLASH</Text>
                    </View>
                    <KeyboardAvoidingView style={{flex:2,marginLeft: 20}}  behavior="padding"> 
                        <View style={styles.socialLogoContainer}>
                          <View style={styles.headerstyle}>
                              <Text style={[styles.loginText]}>LOG IN</Text>
                          </View>
                          <View>
                              <View style={styles.socialIcons}>
                                  {/* <TouchableOpacity onPress={() => this.socialLogin()}>
                                      <Image style={styles.iconView} source={require('../../../assets/fbIcon.png')}/>
                                  </TouchableOpacity>
                                  <TouchableOpacity onPress={() => this.socialLogin()}>
                                      <Image style={styles.iconView} source={require('../../../assets/googleIcon.png')}/>
                                  </TouchableOpacity>          */}
                                  <View>
                                    <GoogleSigninButton
                                      style={{ width: 48, height: 48 }}
                                      size={GoogleSigninButton.Size.Icon}
                                      color={GoogleSigninButton.Color.Dark}
                                      onPress={this.signIn}
                                      disabled={this.state.isSigninInProgress} 
                                    /> 
                                  </View>
                                  <View style={{paddingHorizontal: 10}}>
                                    <AppleButton
                                      buttonStyle={AppleButton.Style.WHITE}
                                      buttonType={AppleButton.Type.SIGN_IN}
                                      style={{
                                        width: 48, // You must specify a width
                                        height: 48, // You must specify a height
                                      }}
                                      onPress={() => this.onAppleButtonPress()}
                                    />
                                  </View>
                              </View>
                              <View>
                              <Text style={{ color: '#A9A9A9' }}>or login with your mobile number</Text>
                              </View>
                          </View>
                        </View>
                        <View style={{flex:2, elevation: 4}}>
                            <TextInput inlineImageLeft='email' style={[styles.emailInput, {borderColor: this.state.emailErrorColor,borderWidth: this.state.emailErrorBorderWidth}]} placeholder="Enter your mobile number" placeholderTextColor="#A9A9A9"
                                value={this.state.emailId}
                                keyboardType = {'numbers-and-punctuation'} maxLength={10}
                                onChangeText={(emailId) => {
                                this.setState({
                                    emailId: emailId,
                                    emailErrorColor: "white",
                                    emailErrorBorderWidth: 0
                                })}} >
                            </TextInput>
                            <TextInput inlineImageLeft='password' style={[styles.passwordInput, {borderColor: this.state.passwordErrorColor, borderWidth: this.state.passwordErrorWidth}]} placeholder="*******" placeholderTextColor="#A9A9A9"
                                secureTextEntry={true} value={this.state.password}
                                onChangeText={(password) => {
                                this.setState({
                                    password: password,
                                    passwordErrorColor: "white",
                                    passwordErrorWidth: 0
                                })}} >
                            </TextInput>
                        </View>
                        <View style={{ flex:1, flexDirection: "row",marginRight: 40,paddingVertical:10}} >
                            <TouchableOpacity style={{flex: 1, justifyContent: "flex-start"}} onPress={() => this.removeListener('SignUp')}>
                                {/* <Text style={{ color: '#A9A9A9'}}>Create account</Text> onPress={() => navigate('signUp')} onPress={() => navigate('forgotPassword')}*/}
                                <Text style={{ color: '#000'}}>Create account</Text>
                                <Text style={{ color: '#000'}}>Referral code login</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={{flex: 1,alignItems: "flex-end"}} onPress={() => this.removeListener('forgotPass')} >
                                <Text style={{ color: '#000'}}>Forgot password?</Text>
                            </TouchableOpacity>
                        </View>
                    </KeyboardAvoidingView>
                    <View style={{flex:1, justifyContent: "center",alignItems:"flex-end",}}>
                        <TouchableOpacity onPress={() => this.authenticateUser(this.state.emailId,this.state.password)}
                          style={{alignItems:"center",backgroundColor:"#332b5c",padding:15,width:ScreenWidth/4,borderTopLeftRadius:30,borderBottomLeftRadius:30}}>
                                <Text  style={{ color: 'white',fontWeight:"bold" }} title="LOGIN">LOGIN</Text>
                        </TouchableOpacity>
                    </View>
                  </View>
                  <View style={{flex:0.5,backgroundColor:"#332b5c"}}></View>
                  {/* #332b5c" #00ccff */}
                </View>
            </View>
              
          </SafeAreaView>
        
        );
    }

    socialLogin() {
      Toast.show('Currently this service is not available. Please click on create account',Toast.LONG);
    }

    showToaster = (message) => {
      setTimeout(() => {
          Toast.show(message, Toast.LONG);
      }, 100);
    }

    removeListener = (page) => {
      // this.backHandler = BackHandler.removeEventListener('hardwareBackPress');
      if(page == 'SignUp'){
        this.props.navigation.navigate('signUp');
      }
      else{
          this.props.navigation.navigate('generateOTP');
      }
    }

    onAppleButtonPress = async () => {
      try {
        // performs login request
        const appleAuthRequestResponse = await appleAuth.performRequest({
          requestedOperation: appleAuth.Operation.LOGIN,
          requestedScopes: [appleAuth.Scope.EMAIL, appleAuth.Scope.FULL_NAME],
        });
        console.warn('appleAuthRequestResponse', appleAuthRequestResponse);
        // get current authentication state for user
        // /!\ This method must be tested on a real device. On the iOS simulator it always throws an error.
        const credentialState = await appleAuth.getCredentialStateForUser(appleAuthRequestResponse.user);

        // use credentialState response to ensure the user is authenticated
        if (credentialState === appleAuth.State.AUTHORIZED) {
          this.setState({
            showLoader: true
          });
          axios.post(
            Config.URL + Config.gmailLoginUser,
            {
              "deviceToken": this.state.firebaseToken,
              "deviceType": this.state.deviceType,
              "fullName": appleAuthRequestResponse.fullName.givenName + appleAuthRequestResponse.fullName.familyName,
              "email": appleAuthRequestResponse.email,
              "appleLoginKey": appleAuthRequestResponse.user
            },
            {
              headers: {
                  'Content-Type': 'application/json',
              }
            }
          )
          .then(res => {
            console.warn('in response of check gmail login', res);
            if (res.data.status == 0) {
              this.setState({
                showLoader: false,
                emailId: '',
                password: ''
              });
              this.enrollUser(res);
            } else {
              this.setState({showLoader: false});
              this.showToaster(res.data.message);
            }
          });
        } else {
          console.warn('Error in user authenticate');
        }
      } catch(err) {
        console.warn('catch error', err);
      }
      
    }

    checkGmailLogin = () => {
      console.warn('in checkgamail login');
      this.setState({
        showLoader: true
      });

      axios.post(
        Config.URL + Config.gmailLoginUser,
        {
          "deviceToken": this.state.firebaseToken,
          "deviceType": this.state.deviceType,
          "fullName": this.state.gmailUserName,
          "email": this.state.gmailEmailId
        },
        {
          headers: {
              'Content-Type': 'application/json',
          }
        }
      )
      .then(res => {
        console.warn('in response of check gmail login', res);
        if (res.data.status == 0) {
          this.setState({
            showLoader: false,
            emailId: '',
            password: ''
          });
          
          this.enrollUser(res);
        } else {
          this.signOut();
          this.setState({showLoader: false});
          this.showToaster(res.data.message);
        }
      });
      
    }

    enrollUser = async(response) => {
      this.setState({showLoader: false});
      console.warn('login user role--->', response.data.roleDetails[0].roleName);
      AsyncStorage.setItem("alreadyLunched",'loggedIn');
      if(response.data.roleDetails[0].roleName == 'deliveryBoy') {
        this.showToaster(response.data.message);
        // Toast.show(response.data.message,Toast.LONG);
        this.props.actions.driverLoginSuccess(response.data);
        this.props.navigationAction.driverNavigateAction('Order');
        const resetAction = StackActions.reset({
          index: 0,
          actions: [NavigationActions.navigate({ routeName: 'driverOrderListing'})]
        });
        this.props.navigation.dispatch(resetAction);
      } else if (response.data.roleDetails[0].roleName == 'shopOwner') {
        console.warn('inside shop', response.data);
        this.showToaster(response.data.message);
        // Toast.show(response.data.message,Toast.LONG);
        this.props.navigationAction.shopNavigateAction('ShopHome');
        this.props.walletAction.updateWallet(response.data.wallet, true);
        if (response.data.shopCartDetails.length > 0) {
          if (response.data.shopCartDetails[0].cart) {
            this.props.addCartActions.replaceCart(response.data.shopCartDetails[0].cart);
          } else {
            this.props.addCartActions.replaceCart([]);
          }
        }  else {
          this.props.addCartActions.replaceCart([]);
        }
        this.props.actions.loginSuccess(response.data);
        const resetAction = StackActions.reset({
          index: 0,
          actions: [NavigationActions.navigate({ routeName: 'shopHomePage'})]
    
        })
        this.props.navigation.dispatch(resetAction);
      } else if (response.data.roleDetails[0].roleName == 'admin') {
        this.showToaster(response.data.message);
        // Toast.show(response.data.message,Toast.LONG);
        this.props.navigationAction.adminNavigateAction('Home');
        this.setState({
          showLoader: false
        });
        this.props.actions.loginSuccess(response.data);
        const resetAction = StackActions.reset({
          index: 0,
          actions: [NavigationActions.navigate({ routeName: 'adminHomePage'})]
    
        });
        this.props.navigation.dispatch(resetAction);
      } else {
        console.warn('inside customer');
        response.data.addressDetails = response.data.addressDetails.map( item => {
          if(item.isSelected == true){
            item.backGroundColor = "#DCF0FA" 
          }
          else{
            item.backGroundColor = "white"
          }
          return item;
          
        });
        this.showToaster(response.data.message);
        // Toast.show(response.data.message,Toast.LONG);
        console.warn('login sucess data cart 111111', response.data);
        this.props.actions.loginSuccess(response.data);
        console.warn('wallet amount fetched', response.data.userDetails[0].wallet);
        this.props.walletAction.updateWallet(response.data.userDetails[0].wallet, true);
        if (response.data.topSliderDetails) {
          await this.props.actions.addTopSliders(response.data.topSliderDetails);
        }
        if (!this.state.skipLogin) {
          if (response.data.cartDetails.length > 0) {
            if (response.data.cartDetails[0].cart) {
              this.props.addCartActions.replaceCart(response.data.cartDetails[0].cart);
            } else {
              this.props.addCartActions.replaceCart([]);
            }
          }  else {
            this.props.addCartActions.replaceCart([]);
          }
          if (response.data.basketDetails.length > 0) {
            if (response.data.basketDetails[0].basket) {
              this.props.basketAction.replaceBasket(response.data.basketDetails[0].basket);
            } else {
              this.props.basketAction.replaceBasket([]);  
            }
          } else {
            this.props.basketAction.replaceBasket([]);
          }
        }
        const resetAction = StackActions.reset(
          {
            index: 0,
            actions: [NavigationActions.navigate({ routeName: 'homePage'})]
          }
        );
        this.props.navigation.dispatch(resetAction);
        
      }
    }

    skipLogin = async () => {
      this.props.navigationAction.navigateAction('Home');
        const resetAction = StackActions.reset({
          index: 0,
          actions: [NavigationActions.navigate({ routeName: 'homePage'})]
        });
        this.props.navigation.dispatch(resetAction);
    }

    // Created By: Ravi Thokal
    //Description: to authinticate user 
    authenticateUser = async (email,pass) => {
      this.setState({
        showLoader: true
      })
      console.warn('token --->', this.state.firebaseToken);
      console.warn('type --->', this.state.deviceType);

      // pass = Base64.encode(pass);
      let password = encrypt(Config.encryptionKey, pass);
      
    //this.backHandler = BackHandler.removeEventListener('hardwareBackPress');
        axios.post(
            Config.URL + Config.loginUser,
            { 
              "deviceToken": this.state.firebaseToken,
              "deviceType": this.state.deviceType,
              "contactNumber": email,
              "password": password,
            },
            {
                headers: {
                    'Content-Type': 'application/json',
                }
            }
        )
        .then(res => {
            console.warn('login result===>', res);
            if (res.data.status == 0 && res.data.roleName != "") {
                this.setState({
                  showLoader: false,
                  emailId: '',
                  password: ''
                });

                this.enrollUser(res);
            }
            else if(res.data.status == 2 && res.data.roleName == ""){  // wrong password
                this.setState({
                    emailErrorColor: "white",
                    emailErrorBorderWidth: 0,
                    passwordErrorColor: "red",
                    passwordErrorWidth: 1,
                });
                this.setState({
                  showLoader: false
                });
                this.showToaster(res.data.message);
                // Toast.show(res.data.message,Toast.LONG);
                
            }
            else if(res.data.status == 3 ){ // userID wrong
                this.setState({
                    emailErrorColor: "red",
                    emailErrorBorderWidth: 1,
                    passwordErrorColor: "red",
                    passwordErrorWidth: 1,
                });
                this.setState({
                  showLoader: false
                });
                this.showToaster(res.data.message);
                // Toast.show(res.data.message,Toast.LONG);
                
            }
            else if(res.data.status == 1){ // userID wrong
              this.setState({
                  emailErrorColor: "red",
                  emailErrorBorderWidth: 1,
                  passwordErrorColor: "red",
                  passwordErrorWidth: 1,
              });
              this.setState({
                showLoader: false
              });
              this.showToaster(res.data.message);
              // Toast.show(res.data.message,Toast.LONG);
              
          }
        }).catch(err => {
          console.warn('err-->', err);
          this.setState({
            showLoader: false
          });
          this.showToaster("Failed to login");
        })
    }

  
}

let ScreenHeight = Dimensions.get("window").height;
let ScreenWidth = Dimensions.get("window").width;

const styles = StyleSheet.create({
  loginContainer:{
    flex:11.5,
    //backgroundColor:"#eeeeee"
    // backgroundColor:"#dcf0fa"
    backgroundColor: '#f1f1f1'
  },
  logoContainer:{
    flex:2,
    justifyContent: 'center',
    alignItems: 'center',
  },
  logoStyle: {
    height: 200,
    width: 150
  },
  logoText: {
    fontWeight: 'bold',
    fontSize: 30,
    fontFamily: "verdana"
  },

  socialIcons: {
    height: 30,
    display: 'flex',
    flexDirection: 'row',
    marginBottom: 20,
  },
  iconView: {
    width: 30,
    height: 30,
    marginRight:20
    
  },
  socialLogoContainer:{
    flex:3,
  },
  loginText: {
    fontWeight: 'bold',
    fontSize: 30,
    fontFamily: "verdana"
  },

  passwordInput: {
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    padding: 10,
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
    marginRight:20,
    backgroundColor:"white",
    elevation: 4
  },
  emailInput: {
    height: 40,
    borderBottomWidth: 1,
    borderBottomColor: 'grey',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    padding: 10,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    marginRight:20,
    backgroundColor:"white",
    elevation:4 
  },
});
//--------------------------------------------------- store related fuction  -- Start

const mapStateToProps = (state) => {
	return {
    userData: state.loginData,
    cartItem: state.cartItem,
    basketItem: state.basketItem,
    cartState: state.cartState,
	};
}

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(loginSuccess,dispatch),
        addCartActions: bindActionCreators(addToCart, dispatch),
        basketAction: bindActionCreators(basketAction,dispatch),
        cartModalAction: bindActionCreators(cartModalAction,dispatch),
        navigationAction: bindActionCreators(navigateAction,dispatch),
        walletAction: bindActionCreators(walletAction,dispatch),
    };
}

// ---------------------------------------------------- store related function  -- END

export default connect(mapStateToProps, mapDispatchToProps)(Login);
