/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Fragment } from 'react';
import axios from 'axios';
import * as Config from '../../Constant/Config.js';
import CodeInput from 'react-native-confirmation-code-input';
import * as Constant from '../../Constant/Constant.js';
import  s  from '../../sharedStyle.js';
import Loader from '../ReusableComponent/loader.js'
import AnimatedLoader from "react-native-animated-loader"
import InternateModal from '../ReusableComponent/InternetModal';
import Toast from 'react-native-simple-toast';
// import SceneLoader from 'react-native-scene-loader';
// import 'bootstrap/dist/css/bootstrap.min.css';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  Button,
  TextInput,
  StatusBar,
  TouchableWithoutFeedback,
  Image,
  TouchableOpacity,
  KeyboardAvoidingView,
  Dimensions
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

class GenerateOTP extends React.Component{

    constructor(props) {
        super(props);
        this.state={            
          contactNumber: "",
          showOTPField:false,
          contactNumberEdit:true,
          headerText:"OTP GENERATION",
          showGetOTPButtion:true,
          showLoader:false,
          key: "",
          btnTag: "Get OTP"
        }
    }

    OTPField = () => {
        if (this.state.showOTPField == true){
          return(  
            // <View>
            //     <AnimatedLoader
            //     visible={this.state.showLoader}
            //     overlayColor="rgba(255,255,255,0.75)"
            //     source={require("../../../assets/loader.json")}
            //     animationStyle= {[styles.lottie,{height: 150,width: 150}]}
            //     speed={1}
            //   />
            //     <CodeInput
            //         secureTextEntry
            //         codeLength={6}
            //         keyboardType="numeric"
            //         autoFocus={true}
            //         inputPosition='center'
            //         size={50}
            //         className='underline'
            //         containerStyle={{flex:1,justifyContent: 'center',alignItems: 'center',padding: 10,marginRight:20}}
            //         codeInputStyle={{ backgroundColor:"white", marginRight:5}}
            //         onFulfill={(otp) => this.verifyOTP(this.state.contactNumber,otp)}
            //     />  

            // </View>
            <KeyboardAvoidingView style={{flex:1,marginLeft: 20}} behavior="padding">
              <CodeInput
                    secureTextEntry
                    codeLength={6}
                    keyboardType="numeric"
                    autoFocus={true}
                    inputPosition='center'
                    size={50}
                    className='underline'
                    containerStyle={{flex:1,justifyContent: 'center',alignItems: 'center',padding: 10,marginRight:15}}
                    codeInputStyle={{ backgroundColor:"white", marginHorizontal:2}}
                    onFulfill={(otp) => this.verifyOTP(this.state.contactNumber, otp)}
                />  
            </KeyboardAvoidingView>
                
          )
        }
        else{
          return null
        }

        
      
    }

    verifyOTP = (contactNumber,otp) => {
      this.setState({
        showLoader: true
      })
      axios.post(
        Config.URL + Config.verifyOTP,
        {
          contactNumber: contactNumber,
          otp:otp,
          key: this.state.key
        },
        {
          headers: {
              'Content-Type': 'application/json',
          }
        }
    )
    .then(res => {
      if (res.data.status == 0 ) {
          this.setState({
            showLoader: false,
            showOTPField:true
          })
          this.props.navigation.navigate('forgotPassword',{contactNumber: this.state.contactNumber});
      }
      else{
        this.setState({
          showLoader: false,
        });
        this.showToaster("OTP didn't match")
      }
    }).catch(err => {
      this.setState({ showLoader: false });
      this.showToaster("Not able to verify OTP at this moment please try later");
    })
    }
    
    generateOTP = async(contactNumber) => {
      this.setState({
        showLoader: true
      });
        if (this.state.btnTag === 'Re-Send OTP') {
          await this.setState ({
            showOTPField: false,
          });
        }
      
        axios.post(
            Config.URL + Config.generateOTP,
            {
                "contactNumber": contactNumber,
                
            },
            {
                headers: {
                    'Content-Type': 'application/json',
                }
            }
        )
        .then(res => {
          this.setState({
            showLoader: false
          });
          if (res.data.status == 0 ) {
            this.showToaster("OTP send on your mobile number/ email please check");
              this.setState({
                showOTPField:true,
                btnTag: 'Re-Send OTP',
                contactNumberEdit:false,
                headerText:"OTP VERIFICATION",
                key: res.data.key
              });
          } else {
            this.showToaster('Please enter registered mobile no');
          }
        }).catch(err => {
          console.warn(err);
          this.setState({
            showLoader: false
          });
          // Toast.show("Something went wrong", Toast.LONG);
          this.showToaster("Not able to generate OTP at this moment try later");
          

        })
      
    }

    showToaster = (message) => {
      setTimeout(() => {
          Toast.show(message, Toast.LONG);
      }, 100);
    }
    
    render() {
        const {navigate} = this.props.navigation;
        return (
          <SafeAreaView style={{flex: 1}}>
            <View style={{flex: 1}}>
              {/* <InternateModal /> */}
                <View style={{flex:1,flexDirection:"row"}}>
                  <AnimatedLoader
                    visible={this.state.showLoader}
                    overlayColor="rgba(255,255,255,0.75)"
                    source={require("../../../assets/loader.json")}
                    animationStyle= {[styles.lottie,{height: 150,width: 150}]}
                    speed={1}
                  />
                    {/* <SceneLoader
                      visible={this.state.showLoader}
                      animation={{
                          fade: {timing: {duration: 1000 }}
                      }}
                    /> */}
                        
                  
                    <View style={styles.forgotPasswordContainer}>
                        <View style={styles.logoContainer}>
                            <Image style={styles.logoStyle} source={require('../../../assets/Logo.png')}/>
                            <Text style={styles.logoText}>FLASH</Text>
                        </View>
                        <KeyboardAvoidingView style={{flex:1,marginLeft: 20}} behavior="padding"> 
                            <View style={styles.socialLogoContainer}>
                                <View style={styles.headerstyle}>
                                    <Text style={styles.loginText}>{this.state.headerText}</Text>
                                </View>
                            </View>
                            <View style={{flex:2}}>
                                <TextInput inlineImageLeft='email' style={styles.emailInput} editable={this.state.contactNumberEdit} placeholder="Enter your mobile no" placeholderTextColor="#000"
                                    keyboardType = {'numbers-and-punctuation'} maxLength={10}
                                    onChangeText={(contactNumber) => {
                                    this.setState({
                                      contactNumber: contactNumber
                                    })}} >
                                </TextInput>
                                  
                                

                                
                            </View>
                        </KeyboardAvoidingView>
                        <this.OTPField />
                        <View style={{flex:1, justifyContent: "center",alignItems:"flex-end"}}>
                            
                          <TouchableOpacity onPress={() => this.generateOTP(this.state.contactNumber)}
                            style={{alignItems:"center",backgroundColor:"#332b5c",padding:15,width:ScreenWidth/4,borderTopLeftRadius:30,borderBottomLeftRadius:30}}>
                                <Text  style={{ color: 'white' }} >{this.state.btnTag}</Text>
                          </TouchableOpacity>
                
                        </View>
                    </View>
                    <View style={{flex:0.5,backgroundColor:"#332b5c"}}></View> 
                </View>  
              
        
            </View>
          </SafeAreaView>

          
       );      
    }

}

let ScreenHeight = Dimensions.get("window").height;
let ScreenWidth = Dimensions.get("window").width;

const styles = StyleSheet.create({
  forgotPasswordContainer:{
    flex:11.5,
    backgroundColor:"#f1f1f1"
  },
  logoContainer:{
    flex:2,
    justifyContent: 'center',
    alignItems: 'center',
    
  },
  logoStyle: {
    height: 200,
    width: 150
  },
  socialIcons: {
    height: 30,
    display: 'flex',
    flexDirection: 'row',
    marginBottom: 10,
  },
  iconView: {
    width: 30,
    height: 30,
    marginRight:20
    
  },
  socialLogoContainer:{
    flex:1,
  },
  headerstyle: {
    paddingBottom: 10
  },
  loginText: {
    fontWeight: 'bold',
    fontSize: 30
  },
  logoText: {
    fontWeight: 'bold',
    fontSize: 30,
    fontStyle:"italic"

  },
  passwordInput: {
  
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    height:50,
    padding: 10,
    marginRight:20,
    borderBottomWidth: 1,
    borderBottomColor: 'grey',
  },
  emailInput: {
    height: 50,
    // borderBottomWidth: 1,
    // borderBottomColor: 'grey',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    padding: 10,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
    marginRight:30,
    elevation: 4
  },
  confirmPasswordInput: {
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    padding: 10,
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
    marginRight:20,
    elevation: 4
  },
});

export default GenerateOTP;
