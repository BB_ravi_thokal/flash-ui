/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Fragment } from 'react';
import axios from 'axios';
import * as Config from '../../Constant/Config.js';
import * as Constant from '../../Constant/Constant.js';
import  s  from '../../sharedStyle.js';
import InternateModal from '../ReusableComponent/InternetModal';
import CodeInput from 'react-native-confirmation-code-input';
import { encrypt } from 'react-native-simple-encryption';
// import SceneLoader from 'react-native-scene-loader';
import AnimatedLoader from "react-native-animated-loader";
// import 'bootstrap/dist/css/bootstrap.min.css';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  Button,
  TextInput,
  StatusBar,
  TouchableWithoutFeedback,
  Image,
  TouchableOpacity,
  ToastAndroid,
  Dimensions,
  KeyboardAvoidingView
} from 'react-native';
import Toast from 'react-native-simple-toast';
// import AnimatedLoader from "react-native-animated-loader"

class SignUpPage extends React.Component{

    constructor(props) {
        super(props);
        this.state={
            fullName:"",
            contactNumber: "",
            password:"",
            referralCode:"",
            numberErrorColor: 'white',
            numberErrorBorderWidth: 1,
            nameBorderColor: 'white',
            nameBorderWidth: 0,
            passBorderColor: 'white',
            passBorderWidth: 0,
            referBorderColor: 'white',
            referBorderWidth: 0,
            btnTag: 'Get OTP',
            showOTPField: false,
            isEditable: true,
            key: '',
            showLoader: false,
            emailId: '',
            emailErrorBorderWidth: 0,
            emailErrorColor: 'white'
        }
    }

    OTPField = () => {
      if (this.state.showOTPField == true){
        return(  
          <KeyboardAvoidingView behavior="padding">
            <CodeInput
                secureTextEntry
                codeLength={6}
                keyboardType="numeric"
                autoFocus={true}
                inputPosition='center'
                size={50}
                className='underline'
                containerStyle={{flex:1,justifyContent: 'center',alignItems: 'center',padding: 10}}
                codeInputStyle={{ backgroundColor:"white", marginHorizontal:2}}
                onFulfill={(otp) => this.verifyOTP(otp)}
            />  
          </KeyboardAvoidingView>
              
        )
      }
      else{
        return null
      }

      
    
  }

  
    render() {
        const {navigate} = this.props.navigation;
        return (
          <SafeAreaView style={{flex: 1}}>
            <AnimatedLoader
                visible={this.state.showLoader}
                overlayColor="rgba(255,255,255,0.75)"
                source={require("../../../assets/loader.json")}
                animationStyle= {[styles.lottie,{height: 150,width: 150}]}
                speed={1}
              />
            <View style={{flex: 1}}>
              
              {/* <SceneLoader
                  visible={this.state.showLoader}
                  animation={{
                      fade: {timing: {duration: 1000 }}
                  }}
                /> */}
              {/* <InternateModal /> */}
                <KeyboardAvoidingView style={{flex:1,flexDirection:"row"}} behavior="padding">
                  
                  <View style={styles.signUpContainer}>
                    <View style={styles.logoContainer}>
                        <Image style={styles.logoStyle} source={require('../../../assets/Logo.png')}/>
                    </View>
                    <View style={{flex:3,marginLeft: 20}}> 
                        <View style={styles.socialLogoContainer}>
                          <View style={styles.headerstyle}>
                              <Text style={styles.loginText}>SIGN UP</Text>
                          </View>
                          {/* <View>
                              <View style={styles.socialIcons}>
                                  <TouchableOpacity>
                                      <Image style={styles.iconView} source={require('../../../assets/fbIcon.png')}/>
                                  </TouchableOpacity>
                                  <TouchableOpacity>
                                      <Image style={styles.iconView} source={require('../../../assets/googleIcon.png')}/>
                                  </TouchableOpacity>         
                              </View>
                              <View>
                              <Text style={{ color: '#A9A9A9' }}>or sign up with your mobile number</Text>
                              </View>
                          </View> */}
                        </View>
                        <KeyboardAvoidingView style={{flex:10}} behavior={Platform.OS == "ios" ? "padding" : "height"}>
                              <TextInput inlineImageLeft='email' style={[styles.emailInput,{borderColor: this.state.nameBorderColor,borderWidth: this.state.nameBorderWidth}]} placeholder="Enter your full name" placeholderTextColor="#000"
                                onChangeText={(fullName) => {
                                  this.setState({
                                      fullName: fullName,
                                      nameBorderColor: "white",
                                      nameBorderWidth: 0
                                  })}}
                                editable= {this.state.isEditable}
                                placeholder="Enter your full name" />
                              <TextInput inlineImageLeft='password' style={[styles.passwordInput,{borderColor: this.state.numberErrorColor,borderWidth: this.state.numberErrorBorderWidth}]} placeholderTextColor="#000"
                                  value={this.state.contactNumber}
                                  onChangeText={(contactNumber) => {
                                      this.setState({
                                          contactNumber: contactNumber,
                                          numberErrorColor: "white",
                                          numberErrorBorderWidth: 0,
                                          emailErrorColor: "white",
                                          emailErrorBorderWidth: 0
                                      })}}
                                  editable= {this.state.isEditable}
                                  keyboardType = {'numbers-and-punctuation'} maxLength={10}
                                  placeholder="Enter your mobile number"/>
                              <TextInput inlineImageLeft='password' style={[styles.passwordInput,{borderColor: this.state.emailErrorColor,borderWidth: this.state.emailErrorBorderWidth}]} placeholderTextColor="#000"
                                  value={this.state.emailId}
                                  onChangeText={(emailId) => {
                                      this.setState({
                                          emailId: emailId,
                                          emailErrorColor: "white",
                                          emailErrorBorderWidth: 0,
                                          numberErrorColor: "white",
                                          numberErrorBorderWidth: 0,
                                      })}}
                                  editable= {this.state.isEditable}
                                  placeholder="Enter your email Id"/>    
                              <TextInput inlineImageLeft='password' style={[styles.confirmPasswordInput,{borderColor: this.state.passBorderColor,borderWidth: this.state.passBorderWidth}]} 
                                placeholderTextColor="#000"
                                placeholder="Enter secure password"
                                // secureTextEntry={true} 
                                value={this.state.password}
                                editable= {this.state.isEditable}
                                onChangeText={(password) => {
                                this.setState({
                                    password: password,
                                    passBorderColor: "white",
                                    passBorderWidth: 0
                                })}}
                              />
                              <Text style={[{marginTop: 15, marginBottom: 5} ,s.normalText]}>Have any friends referral code?</Text>
                              <TextInput inlineImageLeft='password' style={[styles.referralInput,{borderColor: this.state.referBorderColor,borderWidth: this.state.referBorderWidth}]} 
                                placeholderTextColor="#000"
                                placeholder="Enter referral code"
                                // secureTextEntry={true} 
                                value={this.state.referralCode}
                                editable= {this.state.isEditable}
                                onChangeText={(referralCode) => {
                                this.setState({
                                    referralCode: referralCode,
                                    referBorderColor: "white",
                                    referBorderWidth: 0
                                })}}
                              />
                              <View style={{ height: 100 }} />
                        </KeyboardAvoidingView>
                        
                    </View>
                      <this.OTPField />
                    
                    <View style={{flex:1, justifyContent: "center",alignItems:"flex-end",}}>
                        <TouchableOpacity onPress={() => this.generateOTP()}
                        style={{alignItems:"center",backgroundColor:"#1d2951",padding:10,width:ScreenWidth/4,borderTopLeftRadius:10,borderBottomLeftRadius:10}}>
                                <Text  style={{ color: 'white' }} title="SIGN UP">{this.state.btnTag}</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={{flex:0.5,backgroundColor:"#1d2951"}}></View> 
              </KeyboardAvoidingView>
            </View>  
            
          </SafeAreaView>  
        
            
       );      
    }

    generateOTP = async() => {
      var valid = true;
      const expression = /(?!.*\.{2})^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([\t]*\r\n)?[\t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([\t]*\r\n)?[\t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
      if(this.state.fullName == ""){
        valid = false;
        this.setState({
          nameBorderColor: "red",
          nameBorderWidth: 1
        });
        Toast.show("Please enter your name", Toast.LONG);  
      } else if(this.state.contactNumber == ""){
        valid = false;
        this.setState({
          numberErrorColor: "red",
          numberErrorBorderWidth: 1
        });
        Toast.show("Please enter contact number", Toast.LONG);  
      } else if(this.state.emailId == ""){
        valid = false;
        this.setState({
          emailErrorColor: "red",
          emailErrorBorderWidth: 1
        });
        Toast.show("Please enter email id", Toast.LONG);  
      } else if(!expression.test(String(this.state.emailId).toLowerCase())) {
        console.warn('email validate-->', expression.test(String(this.state.emailId).toLowerCase()));
        
          valid = false;
          this.setState({
            emailErrorColor: "red",
            emailErrorBorderWidth: 1
          })
          Toast.show("Please enter valid email id",Toast.LONG);  
      }  else if(this.state.password == ""){
        valid = false;
        this.setState({
          passBorderColor: "red",
          passBorderWidth: 1
        });
        Toast.show("Please enter valid password", Toast.LONG);  
      } else if (valid) {
        console.warn('here');
        await this.setState({showLoader: true});
        const res = await this.otpCall();
        console.warn('res otp ----->', res);
        await this.setState({showLoader: false});
        // if (!this.state.showLoader) {
          console.warn('inside if')
          if (res) {
            if (res.data.status == 0 ) {
              this.setState({
                showOTPField:true,
                btnTag: 'Re-Send OTP',
                isEditable:false,
                showGetOTPButtion:false,
                key: res.data.key,
              })
              setTimeout(() => {
                Toast.show("OTP send on your mobile number/ email please check",Toast.LONG);  
              }, 100);
            
            }
            else if (res.data.status == 4){
              await this.setState({
                numberErrorColor: "red",
                numberErrorBorderWidth: 1,
                emailErrorBorderWidth: 1,
                emailErrorColor: 'red'
              });
              setTimeout(() => {
                Toast.show(res.data.message, Toast.LONG);
              }, 100);
            } else if (res.data.status == 5) {
              console.warn('response in failed otp', res);
              setTimeout(() => {
                Toast.show(res.data.message,Toast.LONG);
              }, 100);
              
              this.setState({
                referBorderColor: 'red',
                referBorderWidth: 1
              });
            } else {
              setTimeout(() => {
                Toast.show(res.data.message, Toast.LONG);
              }, 100);
              
            }
          }
        // }
        
        // axios.post(
        //     Config.URL + Config.genrateOTPSMS,
        //     {
        //       "contactNumber": this.state.contactNumber,
        //       "email": this.state.emailId,
        //       "referAndEarnCode": this.state.referralCode
        //     },
        //     {
        //         headers: {
        //             'Content-Type': 'application/json',
        //         }
        //     }
        // )
        // .then(res => {
        //   this.setState({showLoader: false});
            
            
        // }).catch(err => {
        //   Toast.show("Failed to generate OTP please try again",Toast.LONG);
        //   this.setState({
        //     showLoader: false
        //   })

        // })
      } else {
        setTimeout(() => {
          Toast.show("Please fill the required fields", Toast.LONG);
        }, 100);
        
      }
     
    }

    otpCall = async() => {
      console.warn('referAndEarnCode', this.state.referralCode);
      return axios.post(
        Config.URL + Config.genrateOTPSMS,
        {
          contactNumber: this.state.contactNumber,
          email: this.state.emailId,
          referAndEarnCode: this.state.referralCode
        },
        {
            headers: {
                'Content-Type': 'application/json',
            }
        }
      )
      .then(res => {
        return res;
          
      }).catch(err => {
        return false;
        // Toast.show("Failed to generate OTP please try again",Toast.LONG);
        // this.setState({
        //   showLoader: false
        // })

      })
    }

    verifyOTP = (otp) => {
      this.setState({
        showLoader: true
      })
      axios.post(
        Config.URL + Config.verifyOTP,
        {
            "contactNumber": this.state.contactNumber,
            "otp":otp,
            "key": this.state.key
            
        },
        {
            headers: {
                'Content-Type': 'application/json',
            }
        }
    )
    .then(res => {
        console.warn("aaaaaaaaaaaaaaa",res)
        
        if (res.data.status == 0 ) {
            
            this.setState({
              showLoader: false,
              showOTPField:true
            })
            setTimeout(() => {
              Toast.show("Verification done successfully!! Creating account for you.",Toast.LONG);  
            }, 100);  
          
          this.addUser();
        }
        else{
          this.setState({
            showLoader: false,
          });
          setTimeout(() => {
            Toast.show("OTP didn't match",Toast.LONG);
          }, 100);  
          
          
        }

        
    }).catch(err => {
      this.setState({
        showLoader: false,
        
      })

    })
    }

    // Author: Ravi Thokal
    // Description: Function to add/insert new Login user
    // Date: 22:01:2020
    addUser = () => {
      this.setState({
        showLoader: true,
      })
      console.warn('this.state.referralCode-->', this.state.referralCode);
      let password = encrypt(Config.encryptionKey, this.state.password);
      axios.post(
        Config.URL + Config.addNewUser,
        {
          fullName: this.state.fullName,
          contactNumber: this.state.contactNumber,
          password: password,
          email: this.state.emailId,
          referAndEarnCode: this.state.referralCode
        },
        {
            headers: {
                'Content-Type': 'application/json',
            }
        }
      )
      .then(res => {
        setTimeout(() => {
          Toast.show(res.data.message,Toast.LONG);
        }, 100);  
          if (res.data.status == 0) { 
              this.setState({
                  numberErrorColor: 'white',
                  numberErrorBorderWidth: 0,
                  showLoader: false
              });
              this.props.navigation.navigate('login');
          }
          else if(res.data.status == 1){  // Fail to signup
              this.setState({
                showLoader: false,
              })
          }
          else if(res.data.status == 4){  // Contact number already in use
            this.setState({
              showLoader: false
            });
          }
          
      }).catch(err => {
      })
    
        
    }
}

let ScreenHeight = Dimensions.get("window").height;
let ScreenWidth = Dimensions.get("window").width;
const styles = StyleSheet.create({
  signUpContainer:{
    flex:11.5,
    backgroundColor:"#eeeeee"
  },
  logoContainer:{
    flex:2,
    justifyContent: 'center',
    alignItems: 'center',
    
  },
  logoStyle: {
    height: 200,
    width: 150
  },
  socialIcons: {
    height: 30,
    display: 'flex',
    flexDirection: 'row',
    marginBottom: 10,
  },
  iconView: {
    width: 30,
    height: 30,
    marginRight:20
    
  },
  socialLogoContainer:{
    flex:2,
  },
  headerstyle: {
    paddingBottom: 10
  },
  loginText: {
    fontWeight: 'bold',
    fontSize: 30
  },

  passwordInput: {
  
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    height:50,
    padding: 10,
    marginRight:20,
    borderBottomWidth: 1,
    borderBottomColor: 'grey',
  },
  emailInput: {
    height: 50,
    borderBottomWidth: 1,
    borderBottomColor: 'grey',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    padding: 10,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    marginRight:20
  },
  confirmPasswordInput: {
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    padding: 10,
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
    marginRight:20,
    
  },
  referralInput: {
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    padding: 10,
    borderRadius: 10,
    marginRight:20,
  },
});


export default SignUpPage;
