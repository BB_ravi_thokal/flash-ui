/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Fragment } from 'react';

import * as Config from '../../Constant/Config.js';
import * as Constant from '../../Constant/Constant.js';
import { NavigationBar } from '../ReusableComponent/NavigationBar.js';
import  s  from '../../sharedStyle.js';
import { connect } from 'react-redux';
import * as loginSuccess  from '../../Actions/LoginAction';
import { bindActionCreators } from 'redux';
import InternateModal from '../ReusableComponent/InternetModal';
import * as navigateAction  from '../../Actions/NavigationBarAction';
import Toast from 'react-native-simple-toast';
import AnimatedLoader from "react-native-animated-loader";
// import 'bootstrap/dist/css/bootstrap.min.css';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  Dimensions,
  TouchableOpacity,
  TextInput,
} from 'react-native';

import axios from 'axios'; 
import {
  Colors,
} from 'react-native/Libraries/NewAppScreen';


class PersonalInfoPage extends React.Component{

  constructor(props){
    super(props);    
    this.state = {
      fullName: "",
      emailId: "",
      mobileNumber: "",
      fullNameBottomColor: "#D6D6D6",
      emailIdBottomColor: "#D6D6D6",
      mobileBottomColor: "#D6D6D6",
      userId: "",
      userType: "",
      isMobEdit: false,
      showLoader: false
    }
    
  }

  componentDidMount() {
    
    if (this.props.userData.userData.userDetails[0].contactNumber == '' || this.props.userData.userData.userDetails[0].contactNumber == undefined) {
      this.setState({isMobEdit: true})
    }
      //const index = this.props.userData.userData.addressDetails.findIndex(ind => ind.addressId === this.props.navigation.state.params.addressId);
      this.setState({
        fullName: this.props.userData.userData.userDetails[0].fullName,
        emailId: this.props.userData.userData.userDetails[0].email,
        mobileNumber: this.props.userData.userData.userDetails[0].contactNumber,
        userId: this.props.userData.userData.userId,
      }) 
  }

  render(){
    return ( 
      <SafeAreaView style={{flex: 1}}>
        <View style={[s.body,{flex:1,backgroundColor:"#F5FCFF"}]}>
          {/* <InternateModal /> */}
          <AnimatedLoader
            visible={this.state.showLoader}
            overlayColor="rgba(255,255,255,0.75)"
            source={require("../../../assets/loader.json")}
            animationStyle= {[styles.lottie,{height: 150,width: 150}]}
            speed={1}
          />
          <View style={ styles.addressCardView }> 
            <Text style={ styles.addressText }> Full Name </Text>
            <TextInput style={[styles.addressTextInput,{borderBottomColor: this.state.fullNameBottomColor}]} value={this.state.fullName}
              editable={false}
              onChangeText={(fullName) => {
              this.setState({
                fullName: fullName,
                fullNameBottomColor: "#D6D6D6"
              })}}/>
          </View> 
          <View style={ styles.addressCardView }>
            <Text style={ styles.addressText }>Email Id</Text>
            <TextInput style={[styles.addressTextInput,{borderBottomColor: this.state.emailIdBottomColor}]} value={this.state.emailId}
              editable={false}
              onChangeText={(emailId) => {
              this.setState({
                emailId: emailId,
                emailIdBottomColor: "#D6D6D6"
              })}}/>
          </View>
          <View style={ styles.addressCardView }>
            <Text style={ styles.addressText }>Mobile Number</Text>
            <TextInput style={[styles.addressTextInput,{borderBottomColor: this.state.mobileBottomColor}]} value={this.state.mobileNumber}
              editable={this.state.isMobEdit}
              keyboardType = {'numeric'}
              onChangeText={(mobileNumber) => {
              this.setState({
                mobileNumber: mobileNumber,
                mobileBottomColor: "#D6D6D6"
              })}}/>
          </View> 
          <this.SaveButton />
        
        </View>
      
      </SafeAreaView>
    );
  }

  SaveButton = () => {
    if(this.state.isMobEdit) {
      return (
        <View style={{alignItems:"center", marginTop:'2%'}}> 
          <TouchableOpacity style={styles.addressButtonView} onPress={() => this.saveProfile()}>
              <Text style={styles.addressButtonText}>SAVE</Text>   
          </TouchableOpacity>
        </View>
      )
    } else {
      return null;
    }
  }

  saveProfile = () => {
    console.warn("in save address",this.props.userData.userData.userDetails);
    var tempValid = true;
    if(this.state.fullName == ""){
      this.setState({
        fullNameBottomColor: "red"
      })
      tempValid = false;
    }
    if(this.state.emailId == ""){
      this.setState({
        emailIdBottomColor: "red"
      })
      tempValid = false;
    }
    if(this.state.mobileNumber == ""){
      this.setState({
        mobileBottomColor: "red"
      })
      tempValid = false;
    }

    if(tempValid){
      console.warn("test----",this.props.userData.userData.userDetails[0]);
      
      var tempObj = {
        "userId": this.state.userId,
        "fullName": this.state.fullName,
        "contactNumber": this.state.mobileNumber,
        "email": this.state.emailId,
        "userType": 'customer'
      }
      this.saveProfileDb(tempObj);
      
    }
    else{
      Toast.show("Please fill required details",Toast.LONG);  
    }

    
  }

  saveProfileDb = (data) => {
    console.warn('in save call');
    // this.setState({showLoader: true});
    axios.post(
      Config.URL + Config.addUpdateUserDetails,
      data,
      {
        headers: {
            'Content-Type': 'application/json',
        }
      }
    )
    .then(res => {
        console.warn("response--- of update profile-->",this.props.navigation.state.params.callingPage);
        // this.setState({showLoader: false});
        if (res.data.status == 0) {  
          Toast.show(res.data.message,Toast.LONG);
          console.warn('userData-----> before update',this.props.userData.userData);
          this.props.userData.userData.userDetails[0].fullName = this.state.fullName;
          this.props.userData.userData.userDetails[0].email = this.state.emailId;
          this.props.userData.userData.userDetails[0].contactNumber = this.state.mobileNumber;
          this.props.loginActions.loginSuccess(this.props.userData.userData);
          
          if (this.props.navigation.state.params.callingPage == 'profile') {
            this.props.navigationAction.navigateAction('Profile');
            console.warn('userData-----> after update',this.props.userData.userData);
            this.props.navigation.navigate('profilePage');
          } else if (this.props.navigation.state.params.callingPage == 'orderDetails') {
            this.props.navigation.navigate('orderDetailsPage');
          } else if (this.props.navigation.state.params.callingPage == 'sendActivity') { 
            this.props.navigation.navigate('packageDetailsPage');
          } else if (this.props.navigation.state.params.callingPage == 'getActivity') { 
            this.props.navigation.navigate('sendPackageList');
          }
          
        } else {
          console.warn('res', res);  
          Toast.show(res.data.message, Toast.LONG);
          // this.setState({showLoader: false});
        }
      }).catch(err => {
        console.warn('catch --', err);
        // this.setState({showLoader: false});
        // Toast.show(res.data.message, Toast.LONG);
    }) 
  }

}

let ScreenWidth = Dimensions.get("window").width;
let ScreenHeight = Dimensions.get("window").height;

const styles = StyleSheet.create({

	addressTextInput: {
		borderBottomWidth: 1,
		height: 35
	},

	addressText: {
		fontSize: 14,
		opacity: 0.6,
		marginStart: 4,
		color: Colors.black
	},

	addressCardView: {
		borderRadius: 5,
		marginHorizontal: '2%',
		elevation: 4,
		alignContent: 'center',
		padding: '3%',      
		backgroundColor:"white",
	},

	addressButtonView: {
		backgroundColor:"#00CCFF",  
		width: '80%',
		elevation: 6,
		shadowColor: 'rgba(0, 0, 0, 0.1)',
		shadowOpacity: 0.8,
		height: 40,
		justifyContent: 'center',
		alignItems: 'center',
	},

	addressButtonText: {
		color:"white",  
		fontSize: 16 ,
		fontWeight:"bold"
	},
    

});


//--------------------------------------------------- store related fuction  -- Start

const mapStateToProps = (state) => {
	return {
    userData: state.loginData
	};
}

const mapDispatchToProps = (dispatch) => {
    return {
      loginActions: bindActionCreators(loginSuccess,dispatch),
      navigationAction: bindActionCreators(navigateAction,dispatch),
    };
}

// ---------------------------------------------------- store related function  -- END

export default connect(mapStateToProps, mapDispatchToProps)(PersonalInfoPage);

