import React, { Component } from 'react';
import { SafeAreaView, Text, View, StyleSheet,Dimensions,
  TouchableOpacity, ToastAndroid, Image } from 'react-native';
import * as Config from '../../Constant/Config.js';
import * as walletAction  from '../../Actions/WalletAction';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import  s  from '../../sharedStyle';
import * as navigateAction  from '../../Actions/NavigationBarAction';
import axios from 'axios'; 
import Toast from 'react-native-simple-toast';
import ShopAmountModal from '../ReusableComponent/shopAmountModal';

class WalletPage extends React.Component{
  constructor(props) {
    super(props);
    this.state = {
      isShop: false,
      showModal: false,
      outStandingBalance: 0.00
    };
  }

componentDidMount() {
  
  console.warn('this.props.navigation.state.params.roleName', this.props.navigation.state.params.roleName);
  if (this.props.navigation.state.params.roleName == 'shopOwner') {
    this.setState({
      isShop: true
    });
  }

  axios.post(
    Config.URL + Config.getWallet,
    {
      // "userId": this.props.userData.userData.userId,
      "userId": this.props.navigation.state.params.id,
      "roleName": this.props.navigation.state.params.roleName 
    },
    {
      headers: {
          'Content-Type': 'application/json',
      }
    }
  )
  .then(res => {
    console.warn('response--->', res.data);
    if (res.data.status == 0) {
      this.props.walletAction.updateWallet(res.data.wallet, true);
      this.setState({
        outStandingBalance: res.data.outStandingAmount
      })
    } else {
        console.warn('error-- get wallet');
    }


  })
}

  withDrawReq = () => {
    if(this.props.walletBalance.walletBalance <= 0) {
      Toast.show('Insufficient balance to withdraw',Toast.LONG);
    } else {
      this.props.navigation.navigate('accountDetailsPage', { isShop: this.state.isShop });
    }
  }

  initiatePayment = (amount) => {
    if (amount) {
      if (parseFloat(amount) > 0) {
        this.setState({ showModal: false });
        console.warn('paytm', amount, this.props.navigation.state.params.id);
        const url = Config.shopPaytmViewURL + "shopId=" + this.props.navigation.state.params.id + "&totalAmount=" + amount
        console.warn('Url--->', url);
        this.props.navigation.navigate('shopPaytmViewPage', { paytmUrl: url });
      } else {
        Toast.show('Please enter valid amount', Toast.LONG);
      }
      
    } else {
      Toast.show('Please enter valid amount', Toast.LONG);
    }
    
  }

  showAmountModal = () => {
    if (this.state.outStandingBalance <= 0) {
      Toast.show('You have no outstanding amount to pay.', Toast.LONG);
    } else {
      this.setState({ showModal: true });
    }
    
  }

  hideModal = () => {
    this.setState({ showModal: false });
  }

  ShopBalanceView = () => {
    if (this.state.isShop) {
      return (
        <View>
          <View style={{paddingHorizontal: 15, paddingVertical: 10, backgroundColor: "white", elevation: 6, flexDirection:"row"}}>
            <View style={{flex: 3}}>
                <Image style={{width: undefined, height: undefined, flex: 1, resizeMode:"contain"}} source={require('../../../assets/wallet.png')}/>  
            </View>
            <View style={{flex: 9}}>
                <Text style={s.headerText}>My Balance</Text>
                <View style={{paddingTop: 10, flexDirection:"row"}}>
                  <View >
                    <Image style={{width: 30, height: 30}} source={require('../../../assets/money.jpg')}/>  
                  </View>
                  <View>
                    <Text style={[s.headerText, {paddingLeft: 5}]}>{this.props.walletBalance.walletBalance}</Text>   
                  </View>  
                  
                </View>
            </View>
            
          </View>
          <TouchableOpacity style={{paddingHorizontal: 15, paddingVertical: 15, backgroundColor: "white", elevation: 6, marginTop: 10}}
              onPress={() => this.withDrawReq()}>
              <View style={{flexDirection:"row",alignItems:"center",paddingHorizontal:15}}>
                <View style={{flex: 6, alignItems:"flex-start"}}>
                  <Text style={s.headerText}>Withdraw</Text>
                </View>
                <View style={{flex: 6, alignItems:"flex-end",marginRight:5}}>
                  <Image style={{height:15,width:15}} source={require('../../../assets/next.png')}/>
                </View>
              </View>
          </TouchableOpacity>
          <View style={{marginTop: 15, padding: 15}}>
            <Text style={[s.normalText, {color:"silver"}]} numberOfLines={5}>Note: Wallet amount will be used whenever you buy any goods from the app.</Text> 
            <Text style={[s.normalText, {color:"silver"}]} numberOfLines={5}>Withdraw will transfer amount from wallet to your bank account.</Text>
            <Text style={[s.normalText, {color:"silver"}]} numberOfLines={5}>Thank you!</Text>
          </View>
          <View style={{paddingHorizontal: 15, paddingVertical: 10, marginTop: 15, backgroundColor: "white", elevation: 6, flexDirection:"row"}}>
            <View style={{flex: 3}}>
                <Image style={{width: undefined, height: undefined, flex: 1, resizeMode:"contain"}} source={require('../../../assets/cashless.png')}/>  
            </View>
            <View style={{flex: 9}}>
                <Text style={s.headerText}>Outstanding balance</Text>
                <View style={{paddingTop: 10, flexDirection:"row"}}>
                  <View >
                    <Image style={{width: 30, height: 30}} source={require('../../../assets/money.jpg')}/>  
                  </View>
                  <View>
                    <Text style={[s.headerText, {paddingLeft: 5}]}>{this.state.outStandingBalance}</Text>   
                  </View>  
                  
                </View>
            </View>
            
          </View>
          <TouchableOpacity style={{paddingHorizontal: 15, paddingVertical: 15, backgroundColor: "white", elevation: 6, marginTop: 10}}
              onPress={() => this.showAmountModal()}>
              <View style={{flexDirection:"row",alignItems:"center",paddingHorizontal:15}}>
                <View style={{flex: 6, alignItems:"flex-start"}}>
                  <Text style={s.headerText}>Pay</Text>
                </View>
                <View style={{flex: 6, alignItems:"flex-end",marginRight:5}}>
                  <Image style={{height:15,width:15}} source={require('../../../assets/next.png')}/>
                </View>
              </View>
          </TouchableOpacity>
          
        </View>
        
      )
    } else {
      return null;
    }
  }

  UserBalanceView = () => {
    if (!this.state.isShop) {
      return (
        <View>
          <View style={{paddingHorizontal: 15, paddingVertical: 10, backgroundColor: "white", elevation: 6, flexDirection:"row"}}>
            <View style={{flex: 3}}>
                <Image style={{width: undefined, height: undefined, flex: 1, resizeMode:"contain"}} source={require('../../../assets/wallet.png')}/>  
            </View>
            <View style={{flex: 9}}>
                <Text style={s.headerText}>My Balance</Text>
                <View style={{paddingTop: 10, flexDirection:"row"}}>
                  <View >
                    <Image style={{width: 30, height: 30}} source={require('../../../assets/money.jpg')}/>  
                  </View>
                  <View>
                    <Text style={[s.headerText, {paddingLeft: 5}]}>{this.props.walletBalance.walletBalance}</Text>   
                  </View>  
                  
                </View>
            </View>
            
          </View>
          <TouchableOpacity style={{paddingHorizontal: 15, paddingVertical: 15, backgroundColor: "white", elevation: 6, marginTop: 10}}
              onPress={() => this.withDrawReq()}>
              <View style={{flexDirection:"row",alignItems:"center",paddingHorizontal:15}}>
                <View style={{flex: 6, alignItems:"flex-start"}}>
                  <Text style={s.headerText}>Withdraw</Text>
                </View>
                <View style={{flex: 6, alignItems:"flex-end",marginRight:5}}>
                  <Image style={{height:15,width:15}} source={require('../../../assets/next.png')}/>
                </View>
              </View>
          </TouchableOpacity>
          <View style={{marginTop: 15, padding: 15}}>
            <Text style={[s.normalText, {color:"silver"}]} numberOfLines={5}>Note: Wallet amount will be used whenever you buy any goods from the app.</Text> 
            <Text style={[s.normalText, {color:"silver"}]} numberOfLines={5}>Withdraw will transfer amount from wallet to your bank account.</Text>
            <Text style={[s.normalText, {color:"silver"}]} numberOfLines={5}>Thank you!</Text>
          </View>
        </View>
        
      )
    } else {
      return null;
    }
  }

  render() {
    const {navigate} = this.props.navigation;
    return (
      <SafeAreaView style={{flex: 1}}>
        <View style={{flex: 1}}>
            {/* <InternateModal /> */}
            <this.UserBalanceView />
            <this.ShopBalanceView />
        </View>
        <ShopAmountModal visible={this.state.showModal} balance={this.state.outStandingBalance} 
          navigation = {navigate} initiatePay={this.initiatePayment} onDismiss={this.hideModal}/>
      </SafeAreaView>
    
    );
  }


}




const styles = StyleSheet.create({
  
});


//--------------------------------------------------- store related fuction  -- Start

const mapStateToProps = (state) => {
	return {
        currentLocation: state.currentLocation,
        walletBalance: state.walletBalance,
        userData: state.loginData,
	};
}

const mapDispatchToProps = (dispatch) => {
    return {
      navigationAction: bindActionCreators(navigateAction,dispatch),
      walletAction: bindActionCreators(walletAction,dispatch),
    };
}

// ---------------------------------------------------- store related function  -- END


export default connect(mapStateToProps, mapDispatchToProps)(WalletPage);