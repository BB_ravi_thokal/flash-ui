 // ++++++++++++++++++++++++

/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Fragment } from 'react';

import * as Config from '../../Constant/Config.js';
import * as Constant from '../../Constant/Constant.js';
import  NavigationBar  from '../ReusableComponent/NavigationBar.js';
import * as walletAction  from '../../Actions/WalletAction';
import { connect } from 'react-redux';
import * as loginSuccess  from '../../Actions/LoginAction';
import * as basketAction  from '../../Actions/BasketAction';
import * as cartModalAction  from '../../Actions/CartModalAction';
import * as addToCart  from '../../Actions/CartItem';
import { bindActionCreators } from 'redux';
import  s  from '../../sharedStyle.js';
import PopupModal from '../ReusableComponent/PopupComponent';
import * as navigateAction  from '../../Actions/NavigationBarAction';
import Toast from 'react-native-simple-toast';
import Share from 'react-native-share';
import AnimatedLoader from "react-native-animated-loader";
import { GoogleSignin, GoogleSigninButton, statusCodes } from '@react-native-community/google-signin';
// import 'bootstrap/dist/css/bootstrap.min.css';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  Dimensions,
  TouchableOpacity,
  ImageBackground,
  Button,
  TextInput,
  StatusBar,
  TouchableWithoutFeedback,
  Image,
  FlatList,
  Modal,
  ToastAndroid,
  BackHandler,
  AsyncStorage,
  Linking
} from 'react-native';
import { NavigationActions, StackActions } from 'react-navigation';
import axios from 'axios'; 
import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import InternateModal from '../ReusableComponent/InternetModal';
import callAPI from '../../CoreFunctions/callAPI';

AddressListProfile = ({item, self}) => {
  const {navigate} = self.props.navigation;
  if(self.props.userData.userData.addressDetails.length == 1){
    return(
      <View>
        <View style={{ padding:10 , marginLeft:5,marginRight:10}}>    
          <View style={{flex:1,flexDirection:"row"}}>
            <View style={{flex:1}}>
              <Text style={{fontSize: 14,fontWeight:"bold",color:"#131E3A"}}>Address </Text>   
            </View>
            <View style={{flex:1,flexDirection:"row",justifyContent:"flex-end"}}>
              <TouchableOpacity style={{paddingHorizontal: 5}} onPress={() => navigate('addressPage',{"addressId": item.addressId,"callingPage": "profile"})}>
                {/* <Image source={require('./android/assets/edit.png')}/>     */}
                <Image style={{height: 20, width: 20}} source={require('../../../assets/edit.png')}/>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => self.showAddressModal(item.addressId)}>
                {/* <Image source={require('./android/assets/edit.png')}/>    */}
                <Image style={{height: 20, width: 20}} source={require('../../../assets/delete.png')}/>
              </TouchableOpacity>
            </View>          
          </View>
          <View>
            <Text style={s.normalText}>{item.flatNumber}</Text>
            <Text style={s.normalText}>{item.addressLine1}</Text> 
            <Text style={s.normalText}>{item.addressLine2}</Text> 
          </View>   
        </View>
        <View style={{alignItems:"flex-end",padding:10}}>
          <TouchableOpacity onPress={() => navigate('addressPage',{"addressId": ""})}>
              <Text style={[s.normalText,{color:"#00CCFF"}]}> + Add New Address</Text>
          </TouchableOpacity>
        </View>
      </View>

    )
  }
  else {
    return(
      <View style={{ padding:10 , marginLeft:5,marginRight:10}}>    
        <View style={{flex:1,flexDirection:"row"}}>
          <View style={{flex:1}}>
            <Text style={{fontSize: 14,fontWeight:"bold",color:"#131E3A"}}>Address</Text>   
          </View>
          <View style={{flex:1,flexDirection:"row",justifyContent:"flex-end"}}>
            <TouchableOpacity style={{paddingHorizontal: 5}} onPress={() => navigate('addressPage',{"addressId": item.addressId})}>
              {/* <Image source={require('./android/assets/edit.png')}/> onPress={() => navigate('addressPage',{"addressId": item.addressId})}    */}
              {/* <Text style={{color:"#00CCFF",fontSize:14}}>Edit</Text> */}
              <Image style={{height: 20, width: 20}} source={require('../../../assets/edit.png')}/>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => self.showAddressModal(item.addressId)}>
              {/* <Image source={require('./android/assets/edit.png')}/>    */}
              {/* <Text style={{color:"#00CCFF",fontSize:14}}>Remove</Text> */}
              <Image style={{height: 20, width: 20}} source={require('../../../assets/delete.png')}/>
            </TouchableOpacity>
          </View>             
        </View>
        <View>
          <Text style={s.normalText}>{item.flatNumber}</Text>
          <Text style={s.normalText}>{item.addressLine1}</Text> 
          <Text style={s.normalText}>{item.addressLine2}</Text> 
        </View>   
      </View>
    )  
  }
}

ActiveOrdersListProfile = ({item, self}) => {
  const {navigate} = self.props.navigation;
  return (
    <View>
      <TouchableOpacity style={{marginVertical:10}}
        onPress={() => navigate('trackOrderPage',{action: 'order', orderId: item.orderId})}>
        <View>
          <Text style={{fontSize: 16,fontWeight:"bold"}}>Order Id: {item.orderId}</Text>
        </View>
        <View>
          <Text style={s.normalText}>Total Amount Paid: ₹{item.order.toPay}</Text>
        </View>
        <View style={{flex:1,flexDirection:"row"}}>
          {/* <View style={{flex:2}}>
            <Text style={s.normalText}>Ordered Date: 02 Jan 2020</Text>
          </View> */}
        </View>
      </TouchableOpacity>
      <TouchableOpacity style={{display:"flex",padding:5, alignItems:"center",backgroundColor:"#00ccff",marginTop:10}} 
          onPress={() => navigate('trackOrderPage',{action: 'order', orderId: item.orderId})}>     
          <Text style={{ color:'#fff',fontSize: 16,fontWeight:"bold",justifyContent:"center"}}>Track Order</Text>                     
      </TouchableOpacity>
      <View style={{borderBottomColor:"#131E3A",borderBottomWidth:1,marginTop:10}}></View>
      
    </View> 
  )
}

ActiveBidListingProfile = ({item, self}) => {
  const {navigate} = self.props.navigation;
  return (
    <View>
      <TouchableOpacity style={{marginVertical:10}} 
        onPress={() => navigate('userBidDetailsPage',{"bidId": item.bidId})}>
        <View>
          <Text style={{fontSize: 16,fontWeight:"bold"}}>Bid status: {item.bidStatus}</Text>
        </View>
        <View>
          <Text style={s.normalText}>Total cart amount: ₹ {item.bidInfo.totalCost}</Text>
        </View>
        <View style={{flex:1,flexDirection:"row"}}>
          {/* <View style={{flex:2}}>
            <Text style={s.normalText}>Ordered Date: 02 Jan 2020</Text>
          </View> */}
          <View style={{flex:1,alignItems:"flex-end"}}>
            <Text style={[s.normalText,{color:"#00CCFF"}]}>See Details ></Text>
          </View>
        </View>
      </TouchableOpacity>
      {/* <TouchableOpacity style={{display:"flex",padding:5, alignItems:"center",backgroundColor:"#00ccff",marginTop:10}}>     
          <Text style={{ color:'#fff',fontSize: 16,fontWeight:"bold",justifyContent:"center"}}>REORDER</Text>                     
      </TouchableOpacity> */}
      <View style={{borderBottomColor:"#131E3A",borderBottomWidth:1,marginTop:10}}></View>

    </View> 
      
  )
}

ActiveGetActivityListProfile = ({item, self}) => {
  const {navigate} = self.props.navigation;
  return (
    <View>
      <View style={{marginVertical:10}}
        onPress={() => navigate('trackOrderPage',{action: 'getActivity', orderId: item.orderId})}>
        <View>
          <Text style={{fontSize: 16,fontWeight:"bold"}}>Activity Id: {item.orderId}</Text>
        </View>
        {/* <View>
          <Text style={s.normalText}>Pickup date: ₹{item.order.pickupDate}</Text>
        </View>
        <View>
          <Text style={s.normalText}>Delivery date: ₹{item.order.deliveryDate}</Text>
        </View> */}
        <View style={{flex:1,flexDirection:"row"}}>
          {/* <View style={{flex:2}}>
            <Text style={s.normalText}>Ordered Date: 02 Jan 2020</Text>
          </View> */}
          {/* <View style={{flex:1,alignItems:"flex-end"}}>
            <Text style={[s.normalText,{color:"#00CCFF"}]}>See Details ></Text>
          </View> */}
        </View>
      </View>
      <TouchableOpacity style={{display:"flex",padding:5, alignItems:"center",backgroundColor:"#00ccff",marginTop:10}} 
          onPress={() => navigate('trackOrderPage',{action: 'getActivity', orderId: item.orderId})}>     
          <Text style={{ color:'#fff',fontSize: 16,fontWeight:"bold",justifyContent:"center"}}>Track Order</Text>                     
      </TouchableOpacity>
      <View style={{borderBottomColor:"#131E3A",borderBottomWidth:1,marginTop:10}}></View>
      
    </View> 
  )
}

ActiveSendActivityListProf = ({item, self}) => {
  const {navigate} = self.props.navigation;
  return (
    <View>
      <TouchableOpacity style={{marginVertical:10}}
        onPress={() => navigate('trackOrderPage',{action: 'sendActivity', orderId: item.orderId})}>
        <View>
          <Text style={{fontSize: 16,fontWeight:"bold"}}>Order Id: {item.orderId}</Text>
        </View>
        <View>
          <Text style={{fontSize: 16,fontWeight:"bold"}}>Activity tag: {item.order.activityDetails[0].activityTag}</Text>
        </View>
        {/* <View>
          <Text style={s.normalText}>Pickup date: {item.order.pickupDate}</Text>
        </View>
        <View>
          <Text style={s.normalText}>Delivery date: {item.order.deliveryDate}</Text>
        </View> */}
        <View style={{flex:1,flexDirection:"row"}}>
          {/* <View style={{flex:2}}>
            <Text style={s.normalText}>Ordered Date: 02 Jan 2020</Text>
          </View> */}
          {/* <View style={{flex:1,alignItems:"flex-end"}}>
            <Text style={[s.normalText,{color:"#00CCFF"}]}>See Details ></Text>
          </View> */}
        </View>
      </TouchableOpacity>
      <TouchableOpacity style={{display:"flex",padding:5, alignItems:"center",backgroundColor:"#00ccff",marginTop:10}} 
          onPress={() => navigate('trackOrderPage',{action: 'sendActivity', orderId: item.orderId})}>     
          <Text style={{ color:'#fff',fontSize: 16,fontWeight:"bold",justifyContent:"center"}}>Track Order</Text>                     
      </TouchableOpacity>
      <View style={{borderBottomColor:"#131E3A",borderBottomWidth:1,marginTop:10}}></View>
      
    </View> 
  )
}

PastOrdersListProf = ({item, self}) => {
  const {navigate} = self.props.navigation;
  return (
    <View>
      <View style={{marginVertical:10}}>
        <View>
          <Text style={{fontSize: 16,fontWeight:"bold"}}>Order Id: {item.orderId}</Text>
        </View>
        <View>
          <Text style={s.normalText}>Total Amount Paid: ₹ {item.order.toPay}</Text>
        </View>
        <View style={{flex:1,flexDirection:"row"}}>
          {item.invoiceURL 
          ?
          <View style={{flex: 6, flexDirection:"row"}}>
            <Text style={[s.normalText, {justifyContent:"center", paddingTop: 5, marginRight: 15}]}>Download invoice </Text>
            <TouchableOpacity onPress={() => self.downloadInvoice(item.invoiceURL)}>
              <Image style={{height: 30, width: 30}} source={require('../../../assets/download.png')}/>  
            </TouchableOpacity>
          </View>
          :
            null
          }
          <TouchableOpacity style={{flex: 6,alignItems:"flex-end"}} onPress={() => self.reOrder(item.orderId)}>
            <Text style={[s.normalText,{color:"#00CCFF"}]}>Reorder ></Text>
          </TouchableOpacity>
        </View>
      </View>
      {/* <TouchableOpacity style={{display:"flex",padding:5, alignItems:"center",backgroundColor:"#00ccff",marginTop:10}}>     
          <Text style={{ color:'#fff',fontSize: 16,fontWeight:"bold",justifyContent:"center"}}>REORDER</Text>                     
      </TouchableOpacity> */}
      <View style={{borderBottomColor:"#131E3A",borderBottomWidth:1,marginTop:10}}></View>
      
    </View> 
  )
}

PastGetActivityListProfile = ({item, self}) => {
  const {navigate} = self.props.navigation;
  return (
    <TouchableOpacity onPress={() => navigate('trackOrderPage',{action: 'getActivity', orderId: item.orderId})}>
      <View style={{marginVertical:10}}>
        <View>
          <Text style={{fontSize: 16,fontWeight:"bold"}}>Activity Id: {item.orderId}</Text>
        </View>
        {/* <View>
          <Text style={s.normalText}>Pickup date: ₹{item.order.pickupDate}</Text>
        </View>
        <View>
          <Text style={s.normalText}>Delivery date: ₹{item.order.deliveryDate}</Text>
        </View> */}
        <View style={{flex:1,flexDirection:"row"}}>
          {/* <View style={{flex:2}}>
            <Text style={s.normalText}>Ordered Date: 02 Jan 2020</Text>
          </View> */}
          {/* <View style={{flex:1,alignItems:"flex-end"}}>
            <Text style={[s.normalText,{color:"#00CCFF"}]}>See Details ></Text>
          </View> */}
        </View>
      </View>
      <View style={{borderBottomColor:"#131E3A",borderBottomWidth:1,marginTop:10}}></View>
      
    </TouchableOpacity> 
  )
}

PastSendActivityListProf = ({item, self}) => {
  const {navigate} = self.props.navigation;
  return (
    <TouchableOpacity onPress={() => navigate('trackOrderPage',{action: 'sendActivity', orderId: item.orderId})}>
      <View style={{marginVertical:10}}>
        <View>
          <Text style={{fontSize: 16,fontWeight:"bold"}}>Order Id: {item.orderId}</Text>
        </View>
        <View>
          <Text style={{fontSize: 16,fontWeight:"bold"}}>Activity tag: {item.order.activityDetails[0].activityTag}</Text>
        </View>
        {/* <View>
          <Text style={s.normalText}>Pickup date: ₹{item.order.activityDetails[0].pickupDate}</Text>
        </View>
        <View>
          <Text style={s.normalText}>Delivery date: ₹{item.order.activityDetails[0].deliveryDate}</Text>
        </View> */}
        <View style={{flex:1,flexDirection:"row"}}>
          {/* <View style={{flex:2}}>
            <Text style={s.normalText}>Ordered Date: 02 Jan 2020</Text>
          </View> */}
          {/* <View style={{flex:1,alignItems:"flex-end"}}>
            <Text style={[s.normalText,{color:"#00CCFF"}]}>See Details ></Text>
          </View> */}
        </View>
      </View>
      <View style={{borderBottomColor:"#131E3A",borderBottomWidth:1,marginTop:10}}></View>
      
    </TouchableOpacity> 
  )
}

class ProfilePage extends React.Component {
  constructor(props){
    super(props);    
    this.state = {
      data : "", 
      address:[],
      addAddress: false,
      isModalVisible: false,
      showMessageModal: false,
      removeAddressId: [],
      removeAddressIndex: 0,       
      pastOrders: [],
      activeOrders: [],
      activeGetActivity: [],
      activeSendActivity: [],
      pastGetActivity: [],
      pastSendActivity: [],
      activeBids: [],
      showLoader: false,
      showReOrderModal: false,
      reOrderId: '',
      isSkipLogin: true,
      hide: false
    }
    
  }

  
  
  componentDidMount(){     
    this.props.navigationAction.navigateAction('Profile');
    console.warn('userDetails--->', this.props.userData.userData);
    GoogleSignin.configure({
      // scopes: ['https://www.googleapis.com/auth/drive.readonly'], // what API you want to access on behalf of the user, default is email and profile
      webClientId: '880449304349-ul59h1sspd1e3faud25a1n14fgfe5nns.apps.googleusercontent.com', // client ID of type WEB for your server (needed to verify user ID and offline access)
      offlineAccess: false, // if you want to access Google API on behalf of the user FROM YOUR SERVER
      scopes: ['profile', 'email'],
      // hostedDomain: '', // specifies a hosted domain restriction
      // loginHint: '', // [iOS] The user's ID, or email address, to be prefilled in the authentication UI if possible. [See docs here](https://developers.google.com/identity/sign-in/ios/api/interface_g_i_d_sign_in.html#a0a68c7504c31ab0b728432565f6e33fd)
      forceConsentPrompt: true, // [Android] if you want to show the authorization prompt at each login.
      // accountName: '', // [Android] specifies an account name on the device that should be used
      // iosClientId: '<FROM DEVELOPER CONSOLE>', // [iOS] optional, if you want to specify the client ID of type iOS (otherwise, it is taken from GoogleService-Info.plist)
    }); 
    // BackHandler.addEventListener('hardwareBackPress', this.backAndroid(this));
    if (Object.keys(this.props.userData.userData).length > 0) {
      if (this.props.userData.userData.userDetails.length > 0) {
        this.setState({isSkipLogin: false});
        this.getUserData();
      } else {
        this.setState({isSkipLogin: true});  
      }
    } else {
      console.warn('am here');
      this.setState({isSkipLogin: true});
    }
  }

  getUserData = () => {
    if(this.props.userData.userData.addressDetails.length <= 0){
      this.setState({
        addAddress: true
      })
    }

    axios.post(
      Config.URL + Config.getOrderDetailsUser,
      {
        "userId": this.props.userData.userData.userId,
      },
      {
        headers: {
            'Content-Type': 'application/json',
        }
      }
    )
    .then(res => {
      console.warn(' past order data response------>', res.data.result.pastOrderDetails);
      for (const element of res.data.result.pastOrderDetails) {
        console.warn('element.invoiceUrl', element.invoiceURL);
      }
        if (res.data.status == 0) {
          this.setState({
            activeOrders: res.data.result.orderDetails,
            pastOrders: res.data.result.pastOrderDetails
          })
          if (res.data.result.orderDetails.length > 0) {
            this.props.cartModalAction.changeOrderState(true);
          } else {
            this.props.cartModalAction.changeOrderState(false);
          }
          
        } else {
            console.warn('error',)
        }
    

    });
    

    axios.post(
      Config.URL + Config.getOrderDetailsUser,
      {
        "userId": this.props.userData.userData.userId,
        "activity": true
      },
      {
        headers: {
            'Content-Type': 'application/json',
        }
      }
    )
    .then(res => {
        if (res.data.status == 0) {
          this.setState({
            activeGetActivity: res.data.result.currentGetActivityOrderDetails,
            activeSendActivity: res.data.result.currentSendActivityOrderDetails,
            pastGetActivity: res.data.result.pastGetActivityOrderDetails,
            pastSendActivity: res.data.result.pastSendActivityOrderDetails
          })
          if (this.state.activeGetActivity.length > 0) {
            this.props.cartModalAction.changeGetActivityState(true);
          } else {
            this.props.cartModalAction.changeGetActivityState(false);
          }
          if (this.state.activeSendActivity.length > 0) {
            this.props.cartModalAction.changeSendActivityState(true);
          } else {
            this.props.cartModalAction.changeSendActivityState(false);
          }
          
        } else {
            console.warn('error',)
        }
    
    })

    axios.post(
      Config.URL + Config.getBidByUserId,
      {
        "userId": this.props.userData.userData.userId,
      },
      {
        headers: {
            'Content-Type': 'application/json',
        }
      }
  )
  .then(res => {
      if (res.data.status == 0) {

        this.setState ({
          activeBids: res.data.result,
        });
        if (this.state.activeBids.length > 0) {
          this.props.cartModalAction.changeBidState(true);
        }
         
      } else {
          console.warn('error')
      }
  

  })

    this.props.navigation.addListener('willFocus', () =>{
      console.warn('am in focus Search');
      this.props.navigationAction.navigateAction('Profile');
    });
  }

  downloadInvoice = (url) => {
    console.warn('rl --', url);
    if (url) {
      Linking.openURL(url).catch((err) =>
      Toast.show('Not able to download invoice. Please contact support team',Toast.LONG));
    } else {
      Toast.show('Not able to download invoice. Please contact support team',Toast.LONG);
    }
    
  }

  reOrder = async(orderId) => {
    if (this.props.cartState.cartState) {
      await this.setState ({
        reOrderId: orderId,
        showReOrderModal: true
      });
    } else {
      await this.setState ({
        reOrderId: orderId,
      });
      this.confirmReOrder();
    }
  }

  confirmReOrder = async() => {
    this.setState({showReOrderModal: false, showLoader: true});
    let index = await this.state.pastOrders.findIndex(ind => ind.orderId == this.state.reOrderId);
    console.warn('Order object--->', this.state.pastOrders[index].order);
    axios.post(
      Config.URL + Config.reOrder,
      {
        "order": this.state.pastOrders[index].order
      },
      {
        headers: {
          'Content-Type': 'application/json',
        }
      }
    )
    .then(res => {
      this.setState({showLoader: false});
      console.warn('res ---', res);
      if (res.data.status == 0) {  
        console.log('order data in res--->', res.data.result);
        res.data.result = res.data.result.map( item => {
          if (item.isProductAvailable == false) {
            item.backGroundColor = "#999DA0"   
          } else {
            item.backGroundColor = "white" 
          }
          return item;
        });
        this.props.cartItem.cartItem = [...this.props.cartItem.cartItem, ...res.data.result];
        console.warn('after push --->', this.props.cartItem.cartItem.length);
        const uniqueCart = this.props.cartItem.cartItem.reduce((cart, current) => {
          const x = cart.find(item => item.productId === current.productId);
          if (!x) {
            return cart.concat([current]);
          } else {
            return cart;
          }
        }, []);
        this.props.addCartActions.replaceCart(uniqueCart);
        this.props.navigation.navigate('orderDetailsPage', {"orderType": 'Past'});
      } else if (res.data.status == 2) {
        Toast.show(res.data.message, Toast.LONG);  
      } else {
        Toast.show('Not able to place this order. Please try again', Toast.LONG);    
      }
    }).catch(err => {
      Toast.show('Not able to place this order. Please try again', Toast.LONG);
    })
  }

  addDetailsToCart = async(order) => {
    order.data.result = order.data.result.map( item => {
      if (item.isProductAvailable == false) {
        item.backGroundColor = "red"   
      } else {
        item.backGroundColor = "white" 
      }
      return item;
    });
    await this.props.addCartActions.addToCart(order.data.result);
    console.warn('cart change', this.props.cartItem.cartItem);
    this.props.navigation.navigate('orderDetailsPage',{"orderType": 'Past'});
  }

  

  showReferConditions = () => {
    this.setState({
      showMessageModal: true
    })
  } 

  showAddressModal = (addressId) => {
    const index = this.props.userData.userData.addressDetails.findIndex(ind => ind.addressId === addressId);
    const tempArr = [];
    tempArr.push(this.props.userData.userData.addressDetails[index]);
    this.setState({ 
      removeAddressData: tempArr,
      isModalVisible: true,
      removeAddressIndex: index
    }); 

  }

  hideModal = () => {
    this.setState({ 
      isModalVisible: false,
      showMessageModal: false,
      showReOrderModal: false
    });
  }

  removeAddress = () => {

    this.setState({ 
      isModalVisible: false,
    });
    axios.post(
      Config.URL + Config.deleteUserAddress,
      {
          "addressId": this.props.userData.userData.addressDetails[this.state.removeAddressIndex].addressId,
          "userId": this.props.userData.userData.addressDetails[this.state.removeAddressIndex].userId
      },
      {
        headers: {
          'Content-Type': 'application/json',
        }
      }
    )
    .then(res => {
        if (res.data.status == 0) {  
          if(this.props.userData.userData.addressDetails.length > 0){
            console.warn("remove add 0");
            this.props.userData.userData.addressDetails.splice(this.state.removeAddressIndex,1);
            this.props.loginActions.loginSuccess(this.props.userData.userData);
            this.setState({
              addAddress: false
            })
          }
          else{
            this.setState({
              addAddress: true
            })
          }
          Toast.show(res.data.message,Toast.LONG);

        }
        else if(res.data.status == 1){  
          Toast.show("Sucesssssssssssss",Toast.LONG);  
        }
      }).catch(err => {
          //console.warn(res.data.status);
    })
 
    
  }
  
  MyAddress = () => {
    const {navigate} = this.props.navigation;
    if(this.props.userData.userData.addressDetails.length <= 0){
      return(
        <View style={{backgroundColor:"white"}}>
          <View style={{ padding:10 , marginLeft:5}}>
            <Text style={s.headerText}>My Address</Text>
          </View>
          <View style={{display:"flex",alignItems:"center"}}>
            <TouchableOpacity style={[styles.storeBtn, styles.addtoCartBtnColor,{display:"flex",alignItems:"center"}]} 
              onPress={() => navigate('addressPage',{"addressId": ""})}>
              <Text style={{ color:'#00CCFF',fontSize: 16,fontWeight:"bold",justifyContent:"center"}}> ADD ADDRESS</Text> 
            </TouchableOpacity>
          </View>
        </View>
      )
    }
    else{
      return(
        <View style={{backgroundColor:"white"}}>
          <View style={{ padding:10 , marginLeft:5}}>
            <Text style={s.headerText}>My Address</Text>            
          </View>
          <FlatList data={this.props.userData.userData.addressDetails}   
            renderItem={( { item } ) => (
              <AddressListProfile
                item = {item}
                self = {this}
              />
            )}
            keyExtractor= {item => item.addressId}
            horizontal={false}
          />
        </View>
      )
    }
  }
	
	
  
  logoutAction = () => {
    console.warn('while log -- cart--', this.props.cartItem.cartItem);
    axios.post(
      Config.URL + Config.logout,
      {
        "userId": this.props.userData.userData.userId,
        "basket": this.props.basketItem.basketItem,
        "cart": this.props.cartItem.cartItem
      },
      {
          headers: {
              'Content-Type': 'application/json',
          }
      }
      )
      .then(res => {
          console.warn("res",res);
          if (res.data.status == 0) {
            Toast.show("Logout successfully!!",Toast.LONG);
            this.handleLogout();
            
          }
          else if(res.data.status == 1){  //
            Toast.show("Failed to logout. Please try again",Toast.LONG);
          }
      }).catch(err => {
        console.warn('error in logout', err)
        Toast.show("Failed to logout. Please try again",Toast.LONG);
      })

  }

  signOut = () => {
    // this.props.userData.userData = {};
    this.setState({ hide: true })
    this.props.loginActions.loginSuccess({});
    this.props.addCartActions.replaceCart([]);
    this.props.cartModalAction.changeCartState(false,0,0);
    this.props.cartModalAction.changeOrderState(false); 
    this.props.cartModalAction.changeBidState(false); 
    this.props.cartModalAction.changeGetActivityState(false); 
    this.props.cartModalAction.changeSendActivityState(false); 
    const resetAction = StackActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName: 'login'})]
    })
    this.props.navigation.dispatch(resetAction);
  }

  handleLogout = async () => {
    try {
      await GoogleSignin.revokeAccess();
      await GoogleSignin.signOut();
      setUserInfo(null); // Remember to remove the user from your app's state as well
      this.signOut();
    } catch (error) {
      console.warn(error);
      this.signOut();
    }
  };  

  ActiveBids = () => {
    const {navigate} = this.props.navigation;
    if (this.state.activeBids.length > 0) {
      return (
        <View style={{backgroundColor:"white",marginTop:10}}>
          <View style={{flex:1,paddingVertical:10,paddingHorizontal:15}}>
            <View style={{marginBottom:10}}>
              <Text style={s.headerText}>Active Bids</Text>
            </View>
              <View>
                <FlatList data={this.state.activeBids}   
                  renderItem={( { item } ) => (
                    <ActiveBidListingProfile
                      item = {item}
                      self = {this}
                    // navigation={this.props.navigation}
                    />
                  )}
                  keyExtractor= {item => item.bidId}
                  extraData = {this.state.activeBids} 
                />
              </View>
          </View>
        </View> 
      )
    } else {
      return null;
    }
  }

  Pastorders = () => {
    const {navigate} = this.props.navigation;
    if (this.state.pastOrders.length > 0) {
      return (
        <View style={{backgroundColor:"white",marginTop:10}}>
          <View style={{flex:1,paddingVertical:10,paddingHorizontal:15}}>
            <View style={{marginBottom:10}}>
              <Text style={s.headerText}>Past Orders</Text>
            </View>
              <View>
                <FlatList data={this.state.pastOrders}   
                  renderItem={( { item } ) => (
                    <PastOrdersListProf
                      item = {item}
                      self = {this}
                    // navigation={this.props.navigation}
                    />
                  )}
                  keyExtractor= {item => item.orderID}
                  extraData = {this.state.pastOrders} 
                />
              </View>
          </View>
        </View> 
      )
    } else {
      return null;
    }
  }

  ActiveOrders = () => {
    const {navigate} = this.props.navigation;
    if (this.state.activeOrders.length > 0) {
      return (
        <View style={{backgroundColor:"white",marginTop:10}}>
          <View style={{flex:1,paddingVertical:10,paddingHorizontal:15}}>
            <View style={{marginBottom:10}}>
              <Text style={s.headerText}>Active Orders</Text>
            </View>
              <View>
                <FlatList data={this.state.activeOrders}   
                  renderItem={( { item } ) => (
                    <ActiveOrdersListProfile
                      item = {item}
                      self = {this}
                    // navigation={this.props.navigation}
                    />
                  )}
                  keyExtractor= {item => item.orderId}
                  extraData = {this.state.activeOrders} 
                />
              </View>
          </View>
        </View> 
      )
    } else {
      return null;
    }
  }

 

  ActiveGetActivity = () => {
    const {navigate} = this.props.navigation;
    if (this.state.activeGetActivity.length > 0) {
      return (
        <View style={{backgroundColor:"white",marginTop:10}}>
          <View style={{flex:1,paddingVertical:10,paddingHorizontal:15}}>
            <View style={{marginBottom:10}}>
              <Text style={s.headerText}>Active Get Package Activities</Text>
            </View>
              <View>
                <FlatList data={this.state.activeGetActivity}   
                  renderItem={( { item } ) => (
                    <ActiveGetActivityListProfile
                      item = {item}
                      self = {this}
                    // navigation={this.props.navigation}
                    />
                  )}
                  keyExtractor= {item => item.orderId}
                  extraData = {this.state.activeGetActivity} 
                />
              </View>
          </View>
        </View> 
      )
    } else {
      return null;
    }
  }

  PastGetActivity = () => {
    const {navigate} = this.props.navigation;
    if (this.state.pastGetActivity.length > 0) {
      return (
        <View style={{backgroundColor:"white",marginTop:10}}>
          <View style={{flex:1,paddingVertical:10,paddingHorizontal:15}}>
            <View style={{marginBottom:10}}>
              <Text style={s.headerText}>Past Get Package Activities</Text>
            </View>
              <View>
                <FlatList data={this.state.pastGetActivity}   
                  renderItem={( { item } ) => (
                    <PastGetActivityListProfile
                      item = {item}
                      self = {this}
                    // navigation={this.props.navigation}
                    />
                  )}
                  keyExtractor= {item => item.orderId}
                  extraData = {this.state.pastGetActivity} 
                />
              </View>
          </View>
        </View> 
      )
    } else {
      return null;
    }
  }

  ActiveSendActivity = () => {
    const {navigate} = this.props.navigation;
    if (this.state.activeSendActivity.length > 0) {
      return (
        <View style={{backgroundColor:"white",marginTop:10}}>
          <View style={{flex:1,paddingVertical:10,paddingHorizontal:15}}>
            <View style={{marginBottom:10}}>
              <Text style={s.headerText}>Active Send Package Activities</Text>
            </View>
              <View>
                <FlatList data={this.state.activeSendActivity}   
                  renderItem={( { item } ) => (
                    <ActiveSendActivityListProf
                      item = {item}
                      self = {this}
                    // navigation={this.props.navigation}
                    />
                  )}
                  keyExtractor= {item => item.orderId}
                  extraData = {this.state.activeSendActivity} 
                />
              </View>
          </View>
        </View> 
      )
    } else {
      return null;
    }
  }

  PastSendActivity = () => {
    const {navigate} = this.props.navigation;
    if (this.state.pastSendActivity.length > 0) {
      return (
        <View style={{backgroundColor:"white",marginTop:10}}>
          <View style={{flex:1,paddingVertical:10,paddingHorizontal:15}}>
            <View style={{marginBottom:10}}>
              <Text style={s.headerText}>Past Send Package Activities</Text>
            </View>
              <View>
                <FlatList data={this.state.pastSendActivity}   
                  renderItem={( { item } ) => (
                    <PastSendActivityListProf
                      item = {item}
                      self = {this}
                    // navigation={this.props.navigation}
                    />
                  )}
                  keyExtractor= {item => item.orderId}
                  extraData = {this.state.pastSendActivity} 
                />
              </View>
          </View>
        </View> 
      )
    } else {
      return null;
    }
  }

  openTermsAndCondition = () => {
    if ('policy') {
      Linking.openURL("https://flash-local-deliver.flycricket.io/privacy.html").catch((err) =>
      Toast.show('Not able to open privacy policy. Please try after sometime',Toast.LONG));
 
    } else {
      Linking.openURL("http://flash-local-deliver.flycricket.io/terms.html").catch((err) =>
      Toast.show('Not able to open terms and conditions. Please try after sometime',Toast.LONG));
 
    }

  }

  navigateToWhatsApp = () => {
    try {
      const url = "whatsapp://send?text=&phone=" + this.props.userData.userData.whatsappNumber;
      Linking.canOpenURL(url)
      .then(supported => {
        if (!supported) {
          Toast.show('Sorry..We not able to redirect you on whats app. Please text us on +91 7304341660.',Toast.LONG);
        } else {
          return Linking.openURL(url)
        }
      })
    } catch(err) {
      console.warn('error response-->', supported);
      Toast.show('Not able to open whats app due to' + err ,Toast.LONG); 
    }
    
  }

  inviteFriends = () => {
    options = {
      message: this.props.userData.userData.referAndEarnShareMessage,
      title: this.props.userData.userData.appName,
    }
    Share.open(options)
      .then((res) => {console.warn('res')})
      .catch((err) => { err && console.warn(err);});
  }
  
  render(){
    const {navigate} = this.props.navigation;
    return (
      <Fragment>
        <SafeAreaView style={{flex: 0, backgroundColor: 'white'}} />
        <SafeAreaView style={{flex: 1, backgroundColor: 'white'}}>
        {!this.state.hide
        ?
          <View style={[s.bodyGray]}>
            {/* <InternateModal /> */}
            <AnimatedLoader
              visible={this.state.showLoader}
              overlayColor="rgba(255,255,255,0.75)"
              source={require("../../../assets/loader.json")}
              animationStyle= {[styles.lottie,{height: 150,width: 150}]}
              speed={1}
            />
            <ScrollView style={{flex: 1}}>
              <View >
                {!this.state.isSkipLogin 
                ?
                  <>
                    <View>
                      <View style={{backgroundColor:"white"}}>    
                        <View style={{padding:10 , marginLeft:5 , marginRight:5 , borderBottomWidth:3,borderBottomColor:"#003151"}}>
                          <View style={{flex:1,flexDirection:"row"}}>
                            <View style={{flex:1}}>
                              <Text style={s.headerText}>{this.props.userData.userData.userDetails[0].fullName}</Text>  
                            </View>
                            <TouchableOpacity style={{flex:1,alignItems:"flex-end"}} onPress={() => navigate('personalInfoPage', {"callingPage": "profile"})}>
                              <Image style={{height:30, width: 30}} source={require('../../../assets/edit.png')}/>
                            </TouchableOpacity>
                          </View>
                          <Text style={s.normalText}>{this.props.userData.userData.userDetails[0].email}</Text>
                          <Text style={s.normalText}>{this.props.userData.userData.userDetails[0].contactNumber}</Text>
                        </View>     
                      </View>
                      <this.MyAddress />
                    </View>
                    <this.ActiveOrders />
                    <this.ActiveBids />
                    <this.ActiveGetActivity />
                    <this.ActiveSendActivity />
                    <this.Pastorders />
                    <this.PastGetActivity />
                    <this.PastSendActivity />
                  </>
                :
                  <TouchableOpacity style={{backgroundColor:"white",marginTop:10}} onPress={() => navigate('login', {didSkipLogin: true, callingPage: 'profilePage'})}>
                      <View style={{flex:1,flexDirection:"row",paddingVertical:10,alignItems:"center",paddingHorizontal:15}}>
                        <Text style={s.headerText}>Login</Text>
                      </View>
                  </TouchableOpacity>  
                }
                
                <TouchableOpacity style={{backgroundColor:"white",marginTop:10}} onPress={() => this.navigateToWhatsApp()}>
                    <View style={{flex:1,flexDirection:"row",paddingVertical:10,alignItems:"center",paddingHorizontal:15}}>
                      <Text style={s.headerText}>Contact Us</Text>
                      <View style={{flex:1,alignItems:"flex-end",marginRight:5}}>
                        <Image style={{height:25,width:25}} source={require('../../../assets/whatsappG.png')}/>
                      </View>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity style={{backgroundColor:"white",marginTop:10}} onPress={() => navigate('aboutUsPage')}>
                    <View style={{flex:1,flexDirection:"row",paddingVertical:10,alignItems:"center",paddingHorizontal:15}}>
                      <Text style={s.headerText}>About Us</Text>     
                      <View style={{flex:1,alignItems:"flex-end",marginRight:5}}>
                        <Image style={{height:15,width:15}} source={require('../../../assets/next.png')}/>
                      </View>
                      
                    </View>
                </TouchableOpacity>
                <TouchableOpacity style={{backgroundColor:"white",marginTop:10}} onPress={() => this.openTermsAndCondition('TnC')}>
                    <View style={{flex:1,flexDirection:"row",paddingVertical:10,alignItems:"center",paddingHorizontal:15}}>
                      <Text style={s.headerText}>Terms & Conditions</Text>
                      <View style={{flex:1,alignItems:"flex-end",marginRight:5}}>
                        <Image style={{height:15,width:15}} source={require('../../../assets/next.png')}/>
                      </View>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity style={{backgroundColor:"white",marginTop:10}} onPress={() => this.openTermsAndCondition('policy')}>
                    <View style={{flex:1,flexDirection:"row",paddingVertical:10,alignItems:"center",paddingHorizontal:15}}>
                      <Text style={s.headerText}>Privacy policy</Text>
                      <View style={{flex:1,alignItems:"flex-end",marginRight:5}}>
                        <Image style={{height:15,width:15}} source={require('../../../assets/next.png')}/>
                      </View>
                    </View>
                </TouchableOpacity>
                {!this.state.isSkipLogin
                ?
                  <TouchableOpacity style={{backgroundColor:"white",marginTop:10,marginBottom:5}} onPress={() => this.logoutAction()}>
                      <View style={{flex:1,flexDirection:"row",paddingVertical:10,alignItems:"center",paddingHorizontal:15}}>
                        <Text style={s.headerText}>Log Out</Text>
                        <View style={{flex:1,alignItems:"flex-end",marginRight:5}}>
                          <Image style={{height:15,width:15}} source={require('../../../assets/next.png')}/>
                        </View>
                      </View>
                  </TouchableOpacity>
                :
                null
                }
                
              </View>
              <ImageBackground source={require('../../../assets/invite.png')} style={{marginTop:20, width: '100%', height: 200, position: "relative"}}>
                  <Text style={[s.headerText, {paddingHorizontal: 15, marginTop: 10}]}>Invite your friends</Text>
                  <Text style={[s.normalText, {paddingHorizontal: 15, marginTop: 10}]}>{this.props.userData.userData.referAndEarnMessage}</Text>
                  <TouchableOpacity style={{backgroundColor:"white", width: 100, margin: 15, borderRadius: 50}} onPress={() => this.inviteFriends()}>
                    <Text style={[s.subHeaderText, {paddingVertical: 10, paddingHorizontal: 20, alignItems:"center", color:"#00CCFF"}]}>Invite</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={{flex: 1, alignItems: "flex-end", marginRight: 15}} onPress={() => this.showReferConditions()}>
                    <View style={{flex: 1, flexDirection:"row-reverse", bottom: 0, position: "absolute", backgroundColor:"white", borderRadius: 50}}>
                      <Text style={[s.normalText, {paddingVertical: 8, paddingHorizontal: 10, alignItems:"center", color:"#00CCFF"}]}>Terms & Conditions</Text>
                    </View>
                  </TouchableOpacity>
                </ImageBackground>
              </ScrollView>
              
              <PopupModal visible={this.state.isModalVisible} onDismiss={this.hideModal} removeAddress = {this.removeAddress} 
              message={ "Are you sure want to remove this Address?" } btnTag={"Remove"} data={this.state.removeAddressData} type={"removeAddress"}
              />  
              <PopupModal visible={this.state.showMessageModal} onDismiss={this.hideModal} removeAddress = {this.hideModal} 
              message={this.props.userData.userData.referAndEarnTermAndCondition} btnTag={"Back"} type={"displayMessage"} title="Referral conditions"
              />  
              {/* <PopupModal visible={this.state.showReOrderModal} onDismiss={this.hideModal} removeAddress = {this.confirmReOrder} 
              message={ "Your cart contains some item(s). Do you want to discard the selected item(s)?" } btnTag={"Ok"} data={this.state.removeAddressData} type={"removeAddress"}
              /> */}
              <PopupModal visible={this.state.showReOrderModal} onDismiss={this.hideModal} removeProduct={this.confirmReOrder} 
                message={'Your cart contains some item(s). Do you agree if similar item(s) will get replace?'} 
                btnTag={"Ok"}/>  
              {/* <PopupModal visible={this.state.showReOrderModal} onDismiss={this.hideModal} removeAddress = {this.confirmReOrder} 
              message={'Your cart contains some item(s). Do you want to discard the selected item(s)?'} btnTag={"Ok"} type={"displayMessage"}
              />   */}
              <NavigationBar navigate={navigate}></NavigationBar>  
          </View>
        :
        null
        }
        
      </SafeAreaView>
     </Fragment>
        
    );
  }

}


let ScreenWidth = Dimensions.get("window").width;
let ScreenHeight = Dimensions.get("window").height;

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,
    height: ScreenHeight,
    paddingBottom:20,
  },
  addtoCartBtnColor: {
    borderWidth: 2,
    borderColor: '#00CCFF',
  //  backgroundColor: '#00009a',
    //color: 'white',
    marginTop: 10
  },
  storeBtn: {
    paddingVertical: 5,
    marginBottom: 5,
    color: 'white',
    borderRadius: 0,
    width: 150,
  },
  containerIcons:{
    width:30,
    height:30,
    backgroundColor:'blue',
  },
  mainView: {
    backgroundColor: '#fff',
    flex: 1,

    //height: ScreenHeight - 135
  },
  categoryFooter:{
    width:ScreenWidth,
    paddingVertical:10,
    height:60,
    backgroundColor:'lightgray',
    display:'flex',
    flexDirection:'row',
    justifyContent:"space-evenly",
    alignItems:"center"
  },

  profileViewData:{
    display:"flex",
    alignItems:"center",
    justifyContent:"space-between",
    flexDirection:"row",
    paddingTop:5
  },
  flexRow:{
    display:'flex',
    flexDirection:'row',
  },
updateprofileBtn:{
  padding:5,
  marginLeft:5,
  backgroundColor:'white',
  borderWidth:1,
  borderColor:"lightgray"
},
  profileLayout: {
    height: 150,
    width: ScreenWidth,
    borderBottomLeftRadius: ScreenWidth/2,
    borderBottomRightRadius: ScreenWidth/2,
    borderColor: 'black',
    backgroundColor: 'skyblue',
    position:'relative',
    justifyContent: 'center',
    zIndex:1,
  },
  profilemage:{
    position:'absolute',
    alignSelf: 'center',
    bottom:'-30%',
    height:100,
    width:100,
    borderRadius:50,
    backgroundColor:'darkblue',
  
  },
  profileHeading: {
    color: 'black',
    fontSize: 20,
    fontWeight:"bold"
  },
  profileText:{
    color: 'black',
    fontSize: 20,
    fontWeight:'600',
  },
  profileData:{ 
    borderBottomWidth:1,
    borderBottomColor:'grey',
    paddingVertical:5,
    marginBottom:10
 },
 profileAboutView: {
  marginHorizontal: 20,
  paddingVertical:5,
  // flex: 3,
  backgroundColor: '#fff',
},
  profileMainView: {
    marginHorizontal: 20,
    paddingTop:20,
    // flex: 3,
    backgroundColor: '#fff',
  },
  profilePartition:{
    width:ScreenWidth,
    height:20,
    backgroundColor: 'lightgrey',
    marginTop:10
  },

  // mk start --
  profileHeading: {
    color: 'black',
    fontSize: 20,
    fontWeight:"bold"
  },

  // addressTextInput: {
  //   borderBottomWidth: 1,
  //   borderBottomColor: '#D6D6D6',
  //   height: 35
  // },

  // addressText: {
  //   fontSize: 14,
  //   opacity: 0.6,
  //   marginStart: 4,
  //   color: Colors.black
  // },

  // addressCardView: {
  //   borderRadius: 5,
  //   marginHorizontal: '2%',
  //   elevation: 4,
  //   alignContent: 'center',
  //   padding: '3%',      
  //   backgroundColor:"white",
  // },

  // addressButtonView: {
  //   backgroundColor:"#87CEEB",  
  //   width: '80%',
  //   borderRadius: 50,
  //   elevation: 6,
  //   shadowColor: 'rgba(0, 0, 0, 0.1)',
  //   shadowOpacity: 0.8,
  //   height: 30,
  //   justifyContent: 'center',
  //   alignItems: 'center',
  // },

  // addressButtonText: {
  //   color:"white",  
  //   fontSize: 16 ,
  //   fontWeight:"bold"
  // },
  // Mk end --


});

//--------------------------------------------------- store related fuction  -- Start

const mapStateToProps = (state) => {
	return {
    userData: state.loginData,
    cartItem: state.cartItem,
    basketItem: state.basketItem,
    cartState: state.cartState,
	};
}

const mapDispatchToProps = (dispatch) => {
    return {
      loginActions: bindActionCreators(loginSuccess,dispatch),
      addCartActions: bindActionCreators(addToCart, dispatch),
      basketAction: bindActionCreators(basketAction,dispatch),
      cartModalAction: bindActionCreators(cartModalAction,dispatch),
      navigationAction: bindActionCreators(navigateAction,dispatch),
      walletAction: bindActionCreators(walletAction,dispatch),
    };
}

// ---------------------------------------------------- store related function  -- END

export default connect(mapStateToProps, mapDispatchToProps)(ProfilePage);

