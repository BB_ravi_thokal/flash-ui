import React, { Component } from 'react';
import { SafeAreaView, Text, View, StyleSheet,Dimensions,
  TouchableOpacity, ToastAndroid, TextInput, KeyboardAvoidingView, Linking } from 'react-native';
import MapView, { PROVIDER_GOOGLE } from "react-native-maps";
import Geolocation from '@react-native-community/geolocation';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import  s  from '../../sharedStyle.js';
import InternateModal from '../ReusableComponent/InternetModal';
import AnimatedLoader from "react-native-animated-loader";
import * as currentLocation  from '../../Actions/CurrentLocationAction';
import * as navigateAction  from '../../Actions/NavigationBarAction';
import Toast from 'react-native-simple-toast';
import * as loginSuccess  from '../../Actions/LoginAction';
import axios from 'axios';
import * as Config from '../../Constant/Config.js';
import { NavigationActions, StackActions } from 'react-navigation';
import googleIntegration  from '../../CoreFunctions/googleIntegration';
import DelayInput from "react-native-debounce-input";
import PopupModal from '../ReusableComponent/PopupComponent.js';
// import SceneLoader from 'react-native-scene-loader';

const {width, height} = Dimensions.get("window")
const ASPECT_RATIO = width / height;
const LATITUDE = 37.78825;
const LONGITUDE = -122.4324;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

class SelectLocationPage extends React.Component{
  constructor(props) {
    super(props);
    this.state = {
      showLoading: false,
      region: {
        latitude: LATITUDE,
        longitude: LONGITUDE,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA
      },
      isMapReady: false,
      marginTop: 1,
      userLocation: "",
      regionChangeProgress: false,
      modalVisible: false
      
    };
    
  }

componentDidMount() {
  this.props.navigationAction.navigateAction('Home');
  this.getCurrentPosition(false);

}

showToaster = (message) => {
  setTimeout(() => {
      Toast.show(message, Toast.LONG);
  }, 100);
}

getCurrentPosition = async(search) => {
  await this.setState({showLoading: true});
  if(search) {
    Geolocation.getCurrentPosition(
      (position) => {
        this.setState({
          region: this.state.region,
          showLoading: false,
          error: null,
        });
      },
      (error) => {
        if (error.PERMISSION_DENIED === 1) {
          this.setState({showLoading: false});
          this.showToaster("You will not get the list of your local shops. If you deny the location access")
          // Toast.show("You will not get the list of your local shops. If you deny the location access", Toast.LONG);
        }
      },
      { enableHighAccuracy: false, timeout: 60000, maximumAge: 5000 },
    );  
  } else {
    console.warn('het current location');
    this.setState({ showLoading: false });
    Geolocation.getCurrentPosition(
      (position) => {
        const region = {
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
          latitudeDelta: 0.001,
          longitudeDelta: 0.001
        };
        this.setState({
          region: region,
          showLoading: false,
          error: null,
        });
      },
      (error) => {
        if (error.PERMISSION_DENIED === 1) {
          this.setState({showLoading: false});
          this.showToaster("You will not get the list of your local shops. If you deny the location access")
          // Toast.show("You will not get the list of your local shops. If you deny the location access", Toast.LONG);
        }
      },
      { enableHighAccuracy: false, timeout: 60000, maximumAge: 5000 },
    );
  }
  
}

  onMapReady = () => {
    this.setState({ isMapReady: true, marginTop: 0 });
  }

  // Fetch location details as a JOSN from google map API AIzaSyB9Ak6KilFqRJsdGnWV43NK6TTMbEETicY
  fetchAddress = async(search) => {
    const address = await googleIntegration.fetchAddress(this.state.region.latitude, this.state.region.longitude);
    this.setState({
      userLocation: address.formattedAddress,
      fetchAddress: address.rawAddress,
      regionChangeProgress: false
    });
  }

  // Update state on region change
  onRegionChange = (region) => {
    console.warn('test');
    this.setState({
      region,
      regionChangeProgress: true
    }, () => this.fetchAddress(false));
  }

  fetchLatLong = async(text) => {
    if(text.length > 4) {
      const region = await googleIntegration.fetchLatLong(text);
      this.onRegionChange(region);
    }
  }

  hideModal = () => {
    this.setState({ modalVisible: false })
  }

  // Action to be taken after select location button click
  onLocationSelect = () => alert(this.state.userLocation);

  render() {
      return (
        <SafeAreaView style={{flex: 1}}>
        <AnimatedLoader
            visible={this.state.showLoading}
            overlayColor="rgba(255,255,255,0.75)"
            source={require("../../../assets/loader.json")}
            animationStyle= {[styles.lottie,{height: 150,width: 150}]}
            speed={1}
          />
          <View style={s.bodyGray}>
            {/* <InternateModal /> */}
            <KeyboardAvoidingView behavior="padding" style={{flex: 1}}>
              <View style={{ flex: 8 }}>
                {!!this.state.region.latitude && !!this.state.region.longitude &&
                  <MapView
                    style={styles.map}
                    region={this.state.region}
                    showsUserLocation={true}
                    onMapReady={this.onMapReady}
                    onRegionChangeComplete={this.onRegionChange}
                  >
                    <MapView.Marker
                      coordinate={{ "latitude": this.state.region.latitude, "longitude": this.state.region.longitude }}
                      title={this.state.userLocation}
                      draggable
                    />
                  </MapView>
                }

                {/* <View style={styles.mapMarkerContainer}>
                  <Text style={{ fontFamily: 'fontawesome', fontSize: 42, color: "#ad1f1f" }}>&#xf041;</Text>
                </View> */}
              </View> 

              <View style={{flex:4}}>
                <View style={{flex:8}}>
                  <View style={{paddingVertical: 5, paddingHorizontal: 15}}>
                      <Text style={[s.subHeaderText,{paddingBottom: 5}]}>Search :</Text>
                      {/* <TextInput 
                        style={[styles.inputBox,s.normalText,{elevation: 4, height: 40}]}
                        // value={this.st}
                        onChangeText={(text) => {
                        this.fetchLatLong(text)
                        }}
                        underlineColorAndroid="transparent"
                      />  */}
                      <DelayInput placeholder = {this.state.placeholderText}
                        delayTimeout={500}
                        minLength={4}
                        style={styles.inputBox} inlineImageLeft='searchicon' 
                        value = {this.state.searchText}
                        onChangeText={(searchText) => this.fetchLatLong(searchText)}
                      />
                    </View> 
                  <View>
                    <Text numberOfLines={3} style={[s.normalText,{paddingHorizontal: 15}]}> 
                        {!this.state.regionChangeProgress ? this.state.userLocation : "Identifying Location..."}</Text>
                  </View>
                </View>


                
                <TouchableOpacity style={{flex:4,backgroundColor:"#00CCFF",alignItems:"center",justifyContent:"center"}}
                    onPress={() => this.pickLocation()}>
                  <Text style={[s.headerText,{color:"white"}]}>Select this location</Text>
                </TouchableOpacity>
            </View>
            </KeyboardAvoidingView>
            
            
          </View>
          <PopupModal visible={this.state.modalVisible} onDismiss={this.hideModal} removeProduct={this.navigateToWhatsApp} 
              message={"We are currently operating only in Navi Mumbai areas. Please message us on whats app so we can activate delivery at your place"} btnTag={"Support"}/>
        </SafeAreaView>
      
    );
  }

  pickLocation = async() => {
    console.warn('select pick location', this.state.region);
    this.setState({showLoading: true});
    const currentAddress = await googleIntegration.fetchAddress(this.state.region.latitude, this.state.region.longitude); 
    console.warn('currentAddress', currentAddress);
    const isValidRegion = await googleIntegration.verifyPostalCode(currentAddress.rawAddress, this.props.userData.userData.pinCode);
    if (isValidRegion) {
      this.showToaster('Fetching local shops near you');
      // Toast.show('Fetching local shops near you', Toast.LONG);
      axios.post(
        Config.URL + Config.getTopSlider,
        {
          "position": this.state.region,
        },
        {
          headers: {
              'Content-Type': 'application/json',
          }
        }
      )
      .then(res => {
        this.setState({showLoading: false});
        if (res.data.status == 0) {
          this.props.loginActions.addTopSliders(res.data.result);
          this.props.locationAction.changeLocation(this.state.region, currentAddress.rawAddress[1].short_name);
          this.props.navigationAction.navigateAction('Home');
          const resetAction = StackActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({ routeName: 'homePage'})]
          });
          this.props.navigation.dispatch(resetAction);
        } else {
          this.showToaster('Failed to locate the location please try again');
          // Toast.show('Failed to locate the location please try again', Toast.SHORT);
        }

      })
    } else {
      this.setState({showLoading: false, modalVisible: true});
      this.showToaster('We are currently operating only in Navi Mumbai areas');
      // Toast.show('We are currently operating only in Mumbai and Navi Mumbai areas', Toast.LONG);
    }
  }

  navigateToWhatsApp = () => {
    console.warn('this.props.userData.userData.whatsappNumber', this.props.userData.userData.whatsappNumber);
    try {
      const url = "whatsapp://send?text=&phone=" + this.props.userData.userData.whatsappNumber;
      Linking.canOpenURL(url)
      .then(supported => {
        console.warn('response-->', supported);
        if (!supported) {
          this.showToaster('Sorry..We not able to redirect you to Whats App. Please text us on +91 7304341660');
          // Toast.show('Sorry..We not able to redirect you to Whats App. Please text us on +91 7304341660',Toast.LONG);
        } else {
          console.warn('response 22-->', supported);
          return Linking.openURL(url)
        }
      })
    } catch(err) {
      console.warn('error response-->', supported);
      this.showToaster('Not able to open whats app due to' + err);
      // Toast.show('Not able to open whats app due to' + err ,Toast.LONG); 
    }
    
  }

}




const styles = StyleSheet.create({
  map: {
    ...StyleSheet.absoluteFillObject
  },
  inputBox: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    borderRadius: 10,
    width: '100%',
    height: 40,
    paddingHorizontal: 15,
  },
  mapMarkerContainer: {
    left: '47%',
    position: 'absolute',
    top: '42%'
  },
  mapMarker: {
    fontSize: 40,
    color: "red"
  },
  deatilSection: {
    flex: 6,

  },
  spinnerView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  btnContainer: {
    width: Dimensions.get("window").width - 20,
    position: "absolute",
    bottom: 100,
    left: 10
  }
});


//--------------------------------------------------- store related fuction  -- Start

const mapStateToProps = (state) => {
	return {
        currentLocation: state.currentLocation,
        userData: state.loginData,
	};
}

const mapDispatchToProps = (dispatch) => {
    return {
      navigationAction: bindActionCreators(navigateAction,dispatch),
      locationAction: bindActionCreators(currentLocation,dispatch),
      loginActions: bindActionCreators(loginSuccess,dispatch),
      //cartActions: bindActionCreators(addToCart,dispatch),
    };
}

// ---------------------------------------------------- store related function  -- END


export default connect(mapStateToProps, mapDispatchToProps)(SelectLocationPage);