/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Fragment } from 'react';

import * as Config from '../../Constant/Config.js';
import * as Constant from '../../Constant/Constant.js';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import  s  from '../../sharedStyle.js';
import * as loginSuccess  from '../../Actions/LoginAction';
import * as walletAction  from '../../Actions/WalletAction';
import Toast from 'react-native-simple-toast';
// import { Dropdown } from 'react-native-material-dropdown';
// import 'bootstrap/dist/css/bootstrap.min.css';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  Dimensions,
  TouchableOpacity,
  TextInput,
  ToastAndroid,

} from 'react-native';

import { NavigationEvents } from 'react-navigation';
import AnimatedLoader from "react-native-animated-loader";
import axios from 'axios'; 
import {
  Colors,
} from 'react-native/Libraries/NewAppScreen';
import InternateModal from '../ReusableComponent/InternetModal';
import * as navigateAction  from '../../Actions/NavigationBarAction';

   

class AccountDetailsPage extends React.Component{

  constructor(props){
    super(props);    
    this.state = {
      userId: '',
      accountHolderName: '',
      accountNumber: '',
      ifscCode: '',
      holderNameBorder: 0,
      holderNameColor: 'black',
      accountNumberBorder: 0,
      accountNumberColor: 'black',
      ifscCodeBorder: 0,
      ifscCodeColor: 'black',
      showLoader: false,
    }
    
  }

  componentDidMount() {
    // this.getCityData();
    
    
    if (this.props.navigation.state.params.isShop) {
      this.setState({ userId: this.props.userData.userData.shopId });
    } else {
      this.setState({ userId: this.props.userData.userData.userId });
    }
    
  }

  

  render(){
    return ( 
      <SafeAreaView style={{flex: 1}}>
        <View style={[s.bodyGray]}>
          {/* <InternateModal /> */}
          <AnimatedLoader
            visible={this.state.showLoader}
            overlayColor="rgba(255,255,255,0.75)"
            source={require("../../../assets/loader.json")}
            animationStyle= {[styles.lottie,{height: 150,width: 150}]}
            speed={1}
          />
          <View style={ styles.addressCardView }> 
            <Text style={ styles.addressText }>Account holder name </Text>
            <TextInput style={[styles.addressTextInput,{borderWidth: this.state.holderNameBorder, borderColor: this.state.holderNameColor} ]}
            value={this.state.accountHolderName}
              onChangeText={(name) => {
              this.setState({
                accountHolderName: name,
                isFormValid: true,
                holderNameBorder: 0,
                holderNameColor: "white"
              })}} />
          </View> 
          <View style={ styles.addressCardView }>
            <Text style={ styles.addressText }>Account number </Text>
            <TextInput style={[styles.addressTextInput,{borderWidth: this.state.accountNumberBorder, borderColor: this.state.accountNumberColor} ]} 
            value={this.state.accountNumber}
              onChangeText={(accountNumber) => {
              this.setState({
                accountNumber: accountNumber,
                isFormValid: true,
                accountNumberBorder: 0,
                accountNumberColor: "white"
              })}} />
          </View>
          <View style={ styles.addressCardView }>
            <Text style={ styles.addressText }>IFSC code</Text>
            <TextInput style={[styles.addressTextInput,{borderWidth: this.state.ifscCodeBorder, borderColor: this.state.ifscCodeColor} ]} 
            value={this.state.ifscCode}
              onChangeText={(ifscCode) => {
              this.setState({
                ifscCode: ifscCode,
                isFormValid: true,
                ifscCodeBorder: 0,
                ifscCodeColor: "white"
              })}} />
          </View> 
          
          <View style={{alignItems:"center", marginTop:'2%'}}> 
            <TouchableOpacity style={styles.addressButtonView} onPress={() => this.submitForm()}>
                <Text style={styles.addressButtonText}>SAVE</Text>   
            </TouchableOpacity>
          </View>

          <View style={{marginTop: 25, paddingHorizontal: 15}}>
              <Text numberOfLines={5} style={s.normalText}>Please provide your account details. We will transfer your wallet amount in respective account in 2-3 buisiness days</Text>
          </View>
        
        </View>
      
      </SafeAreaView>
    
    );
  }

  submitForm = () => {
    console.warn('in subit form');
    this.setState({ showLoader: true });
    if(this.state.accountHolderName == "") {
      this.setState({
        isFormValid: false,
        holderNameBorder: 1,
        holderNameColor: "red",
        showLoader: false
      })
      Toast.show('Please fill account holder name',Toast.LONG);
    } else if(this.state.accountNumber == "") {
      this.setState({
        isFormValid: false,
        accountNumberBorder: 1,
        accountNumberColor: "red",
        showLoader: false
      })
      Toast.show('Please fill account number',Toast.LONG);
    } else if(this.state.ifscCode == "") {
        this.setState({
          isFormValid: false,
          ifscCodeBorder: 1,
          ifscCodeColor: "red",
          showLoader: false
        })
        Toast.show('Please fill IFSC code',Toast.LONG);
      } else if(this.state.isFormValid) {
      this.saveDetails();
    }

  }

  saveDetails = () => {
    console.warn("in save adetails", this.state.userId, this.props.walletBalance.walletBalance);
    axios.post(
      Config.URL + Config.withDrawWallet,
      {
        "userId": this.state.userId,
        "acctNo": this.state.accountNumber,
        "acctHolderName": this.state.accountHolderName,
        "ifscCode": this.state.ifscCode
      },
      {
        headers: {
            'Content-Type': 'application/json',
        }
      }
    )
    .then(res => {
      console.warn('response--->', res.data);
      this.setState ({ showLoader: false });
      if (res.data.status == 0) {
        this.props.walletAction.updateWallet(res.data.wallet, true);
        Toast.show('Your wallet withdrawal request is placed successfully.',Toast.LONG);
        this.props.navigationAction.navigateAction('Home');
        this.props.navigation.navigate('homePage')
      } else {
          console.warn('error');
      }
  
    })
  }

}

const styles = StyleSheet.create({

	addressTextInput: {
		borderBottomWidth: 1,
		borderBottomColor: '#D6D6D6',
		height: 35
	},

	addressText: {
		fontSize: 14,
		opacity: 0.6,
		marginStart: 4,
		color: Colors.black
	},

	addressCardView: {
		borderRadius: 5,
		marginHorizontal: '2%',
		elevation: 4,
		alignContent: 'center',
		padding: '3%',      
		backgroundColor:"white",
	},

	addressButtonView: {
		backgroundColor:"#00CCFF",  
		width: '80%',
		//borderRadius: 50,
		elevation: 6,
		shadowColor: 'rgba(0, 0, 0, 0.1)',
		shadowOpacity: 0.8,
		height: 40,
		justifyContent: 'center',
		alignItems: 'center',
	},

	addressButtonText: {
		color:"white",  
		fontSize: 16 ,
		fontWeight:"bold"
	},
    

});


//--------------------------------------------------- store related fuction  -- Start

const mapStateToProps = (state) => {
	return {
    walletBalance: state.walletBalance,
    userData: state.loginData,
	};
}

const mapDispatchToProps = (dispatch) => {
    return {
        navigationAction: bindActionCreators(navigateAction,dispatch),
        walletAction: bindActionCreators(walletAction,dispatch),
    };
}

// ---------------------------------------------------- store related function  -- END


export default connect(mapStateToProps, mapDispatchToProps)(AccountDetailsPage);

