import React, { Component } from 'react';
import { SafeAreaView, Text, View, ActivityIndicator, Button, StyleSheet,Dimensions, TextInput,
  TouchableOpacity, ToastAndroid, KeyboardAvoidingView } from 'react-native';
import MapView from "react-native-maps";
import Geolocation from '@react-native-community/geolocation';
import { connect } from 'react-redux';
import * as loginSuccess  from '../../Actions/LoginAction';
import * as activityAction  from '../../Actions/ActivityAction';
import * as getPackageAction  from '../../Actions/GetPackageAction';
import { bindActionCreators } from 'redux';
import  s  from '../../sharedStyle.js';
import { ScrollView } from 'react-native-gesture-handler';
import InternateModal from '../ReusableComponent/InternetModal';
import * as navigateAction  from '../../Actions/NavigationBarAction';
import axios from 'axios'; 
import * as Config from '../../Constant/Config.js';
import AnimatedLoader from "react-native-animated-loader";
// import SceneLoader from 'react-native-scene-loader';
import Toast from 'react-native-simple-toast';
import DelayInput from "react-native-debounce-input";

const {width, height} = Dimensions.get("window")
const ASPECT_RATIO = width / height;
const LATITUDE = 37.78825;
const LONGITUDE = -122.4324;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

class AddressPage extends React.Component{
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      region: {
        latitude: LATITUDE,
        longitude: LONGITUDE,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA
      },
      isMapReady: false,
      marginTop: 1,
      userLocation: "",
      regionChangeProgress: false,
      addressId: "",
      flatNumber: "",
      flatNoBorder: 0,
      flatNoColor: "white",
      addressLine1: "",
      address1Border: 0,
      address1Color: "white",
      addressLine2: "",
      address2Border: 0,
      address2Color: "white",
      addressState: "Maharashtra",
      addressStateBorder: 0,
      addressStateColor: "white",
      district: "",
      districtBorder: 0,
      districtColor: "white",
      pinCode: "",
      pinCodeBorder: 0,
      pinCodeColor: "white",
      isSelected: false,
      backGroundColor: "white",
      isFormValid: true,
      fetchAddress: [],
      cityData: [ {value:'Navi Mumbai'},{value:'Panvel'},{value:'Kharghar'},{value:'Mumbai'}]
    };
  }

  componentDidMount() {
    // this.getCityData();
    
    if(this.props.navigation.state.params.addressId) {
      console.warn("inside edit -----",this.props.navigation.state.params.addressId);
      this.setState({
        loading: false,
      });
      if(this.props.navigation.state.params.callingPage == 'getPackagePick') {
        // const index = this.props.getPackageData.pickupAddress.findIndex(ind => ind.addressId === this.props.navigation.state.params.addressId);
        this.setState({
          addressId: this.props.getPackageData.pickupAddress.addressId,
          flatNumber: this.props.getPackageData.pickupAddress.flatNumber,
          addressLine1: this.props.getPackageData.pickupAddress.addressLine1,
          addressLine2: this.props.getPackageData.pickupAddress.addressLine2,
          region: this.props.getPackageData.pickupAddress.position
          
        })
      } else if(this.props.navigation.state.params.callingPage == 'getPackageDeliver') {
        // const index = this.props.getPackageData.deliveryAddress.findIndex(ind => ind.addressId === this.props.navigation.state.params.addressId);
        this.setState({
          addressId: this.props.getPackageData.deliveryAddress.addressId,
          flatNumber: this.props.getPackageData.deliveryAddress.flatNumber,
          addressLine1: this.props.getPackageData.deliveryAddress.addressLine1,
          addressLine2: this.props.getPackageData.deliveryAddress.addressLine2,
          region: this.props.getPackageData.deliveryAddress.position
        })
      } else if (this.props.navigation.state.params.callingPage == 'sendPackageList') {
        // const index = this.props.activityData.deliveryAddress.findIndex(ind => ind.addressId === this.props.navigation.state.params.addressId);
          this.setState({
            addressId: this.props.activityData.deliveryAddress.addressId,
            flatNumber: this.props.activityData.deliveryAddress.flatNumber,
            addressLine1: this.props.activityData.deliveryAddress.addressLine1,
            addressLine2: this.props.activityData.deliveryAddress.addressLine2,
            region: this.props.activityData.deliveryAddress.position
          })
      } else if (this.props.navigation.state.params.callingPage == 'pickAddress') {
        console.warn('----------------------- data in nav', this.props.activityData.pickupAddress);
        // const index = this.props.activityData.pickupAddress.findIndex(ind => ind.addressId === this.props.navigation.state.params.addressId);
        this.setState({
          addressId: this.props.activityData.pickupAddress.addressId,
          flatNumber: this.props.activityData.pickupAddress.flatNumber,
          addressLine1: this.props.activityData.pickupAddress.addressLine1,
          addressLine2: this.props.activityData.pickupAddress.addressLine2,
          region: this.props.activityData.pickupAddress.position
        })
    } else if (this.props.navigation.state.params.callingPage == 'profile') {
      
      const index = this.props.userData.userData.addressDetails.findIndex(ind => ind.addressId === this.props.navigation.state.params.addressId);
      console.warn('position-------------------------------------->', this.props.userData.userData.addressDetails[index].position);  
      this.setState({
          addressId: this.props.navigation.state.params.addressId,
          flatNumber: this.props.userData.userData.addressDetails[index].flatNumber,
          addressLine1: this.props.userData.userData.addressDetails[index].addressLine1,
          addressLine2: this.props.userData.userData.addressDetails[index].addressLine2,
          region: this.props.userData.userData.addressDetails[index].position
        })
    } else {
        const index = this.props.userData.userData.addressDetails.findIndex(ind => ind.addressId === this.props.navigation.state.params.addressId);
        this.setState({
          addressId: this.props.navigation.state.params.addressId,
          flatNumber: this.props.userData.userData.addressDetails[index].flatNumber,
          addressLine1: this.props.userData.userData.addressDetails[index].addressLine1,
          addressLine2: this.props.userData.userData.addressDetails[index].addressLine2,
          region: this.props.userData.userData.addressDetails[index].position
        })
    }

    console.warn("length---->",this.props.userData.userData.addressDetails.length);

    if(this.props.userData.userData.addressDetails.length <= 1){
      console.warn('change isSelected----'); 
      this.setState({
        isSelected: true,
        backGroundColor: "#DCF0FA",
        addressModal: false
      })
    }
      
  } else {
    this.setState({
      loading: false,
    });
    this.getCurrentLocation();
  }

  }

  getCurrentLocation() {
      Geolocation.getCurrentPosition(
        (position) => {
          const region = {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA
          };
          this.setState({
            region: region,
            loading: false,
            error: null,
          });
        },
        (error) => {
          if (error.PERMISSION_DENIED === 1) {
            this.setState({showLoader: false});
            Toast.show("You will not get the list of your local shops. If you deny the location access", Toast.LONG);
          }
        },
        { enableHighAccuracy: false, timeout: 200000, maximumAge: 5000 },
      );
  }

  onMapReady = () => {
    this.setState({ isMapReady: true, marginTop: 0 });
  }

  // Fetch location details as a JOSN from google map API AIzaSyB9Ak6KilFqRJsdGnWV43NK6TTMbEETicY
  fetchAddress = () => {
    fetch("https://maps.googleapis.com/maps/api/geocode/json?address=" + this.state.region.latitude + "," + this.state.region.longitude + "&key=" + "AIzaSyCUREttpI8l0AgHrxXAJ8_NhP7XntJTNUU")
      .then((response) => response.json())
      .then((responseJson) => {
        
        const userLocation = responseJson.results[0].formatted_address;
        
        console.warn("responseJson------------->", userLocation)
        this.setState({
          userLocation: userLocation,
          fetchAddress: responseJson.results[0].address_components,
          regionChangeProgress: false
        });
      });
  }

  // Update state on region change
  onRegionChange = (region) => {
    console.warn('test');
    this.setState({
      region,
      regionChangeProgress: true
    }, () => this.fetchAddress());
  }

  fetchLatLong = (text) => {
    if(text.length > 4) {
      console.warn('tect--->', text);
      this.setState({ isMapReady: false, marginTop: 0 });
      fetch("https://maps.googleapis.com/maps/api/geocode/json?address=" + text + "&key=" + "AIzaSyCUREttpI8l0AgHrxXAJ8_NhP7XntJTNUU")
      .then((response) => response.json())
      .then((responseJson) => {
        
        const lat = responseJson.results[0].geometry.location.lat;
        const lng = responseJson.results[0].geometry.location.lng;
        console.warn('lat--- lag', lat, lng);
        const region = {
          latitude: lat,
          longitude: lng,
          latitudeDelta: 0.001,
          longitudeDelta: 0.001
        };
        this.setState({region: region, isMapReady: false, marginTop: 0});


        // this.setState({
        //   region: region,
        //   regionChangeProgress: true
        // }, () => this.fetchAddress(true));
        this.onRegionChange(region);

        
      });
    }
  }

  // Action to be taken after select location button click
  onLocationSelect = () => alert(this.state.userLocation);

  render() {
    if (this.state.loading) {
      return (
        <SafeAreaView style={{flex: 1}}>
          {/* <SceneLoader
            visible={this.state.showLoader}
            animation={{
                fade: {timing: {duration: 1000 }}
            }}
          /> */}
            <AnimatedLoader
            visible={this.state.loading}
            overlayColor="rgba(255,255,255,0.75)"
            source={require("../../../assets/loader.json")}
            animationStyle= {[styles.lottie,{height: 150,width: 150}]}
            speed={1}
          />
        </SafeAreaView>
        
      );
    } else {
      return (
        <SafeAreaView style={{flex: 1}}>
          <KeyboardAvoidingView behavior="padding" style={s.bodyGray}>
          {/* <View style={s.bodyGray}> */}
          
            <View style={{ flex: 4.5 }}>
              {this.state.region.latitude && this.state.region.longitude &&
                <MapView
                  // provider={PROVIDER_GOOGLE}
                  style={styles.map}
                  region={this.state.region}
                  showsUserLocation={true}
                  onMapReady={this.onMapReady}
                  onRegionChangeComplete={this.onRegionChange}
                >
                  <MapView.Marker
                    coordinate={{ "latitude": this.state.region.latitude, "longitude": this.state.region.longitude }}
                    title={"Your Location"}
                    draggable
                  />
                </MapView>
              }

              {/* <View style={styles.mapMarkerContainer}>
                <Text style={{ fontSize: 42, color: "#ad1f1f" }}>&#xf041;</Text>
              </View> */}
            </View>
            
            <View style={{flex: 6.5, paddingHorizontal:15}}>
            
              <ScrollView showsVerticalScrollIndicator={false}>
                <Text style={[s.normalText,{marginBottom: 10, color:"silver"}]}>Move map for location</Text>
                <View style={{paddingVertical: 5}}>
                  <Text style={[s.subHeaderText,{paddingBottom: 5}]}>Track location</Text>
                  <Text numberOfLines={4} style={s.normalText}>
                  {!this.state.regionChangeProgress ? this.state.userLocation : "Identifying Location..."}</Text>
                  {/* <TextInput 
                  style={[styles.inputBox,s.normalText,{elevation: 4}]}
                  underlineColorAndroid="transparent"
                  numberOfLines={2}
                  value={!this.state.regionChangeProgress ? this.state.userLocation : "Identifying Location..."}

                /> */}
                </View>
                <View style={{paddingVertical: 5}}>
                  <Text style={[s.subHeaderText,{paddingBottom: 5}]}>Search :</Text>
                  {/* <TextInput 
                    style={[styles.inputBox,s.normalText,{elevation: 4}]}
                    // value={this.st}
                    onChangeText={(text) => {
                    this.fetchLatLong(text)
                    }}
                    underlineColorAndroid="transparent"
                  />  */}
                  <DelayInput placeholder = {this.state.placeholderText}
                    delayTimeout={500}
                    minLength={4}
                    style={styles.inputBox} inlineImageLeft='searchicon' 
                    value = {this.state.searchText}
                    onChangeText={(searchText) => this.fetchLatLong(searchText)}
                  />
                </View>
                
                {/* <Text numberOfLines={2} style={{ fontSize: 14, paddingVertical: 10, borderBottomColor: "silver", borderBottomWidth: 0.5 }}>
                  {!this.state.regionChangeProgress ? this.state.userLocation : "Identifying Location..."}</Text> */}
                <View style={{paddingVertical: 5}}>
                  <Text style={[s.subHeaderText,{paddingBottom: 5}]}>Apartment / House / Flat / Block No:</Text>
                  <TextInput 
                    style={[styles.inputBox,s.normalText,{elevation: 4, borderWidth: this.state.flatNoBorder, borderColor: this.state.flatNoColor}]}
                    value={this.state.flatNumber}
                    onChangeText={(flatNumber) => {
                    this.setState({
                      flatNumber: flatNumber,
                      isFormValid: true,
                      flatNoBorder: 0,
                      flatNoColor: "white"
                    })}}
                    underlineColorAndroid="transparent"
                  /> 
                </View>  
                <View style={{paddingVertical: 5}}>
                  <Text style={[s.subHeaderText,{paddingBottom: 5}]}>Landmark:-</Text>
                  <TextInput 
                    style={[styles.inputBox,s.normalText,{elevation: 4,borderWidth: this.state.address2Border, borderColor: this.state.address2Color}]}
                    value={this.state.addressLine2}
                    onChangeText={(addressLine2) => {
                    this.setState({
                      addressLine2: addressLine2,
                      isFormValid: true,
                      address2Border: 0,
                      address2Color: "white"
                    })}}
                    underlineColorAndroid="transparent"
                  /> 
                </View> 
                  
                {/* <View style={styles.btnContainer}>
                  <Button
                    title="PICK THIS LOCATION"
                    disabled={this.state.regionChangeProgress}
                    onPress={this.onLocationSelect}
                  >
                  </Button>
                </View> */}
              </ScrollView> 
              {/* <View style={{ height: 100 }} /> */}
            </View>
            <View style={{flex:1}}>
              <TouchableOpacity style={{flex:6,backgroundColor:"#00CCFF",alignItems:"center",justifyContent:"center"}}
                  onPress={() => this.submitForm()}>
                <Text style={[s.headerText,{color:"white"}]}>Save</Text>
              </TouchableOpacity>
            </View>
          {/* </View> */}
          
          </KeyboardAvoidingView>
        </SafeAreaView>
    );
    }
  }

  submitForm = () => {
    console.warn('in submit form', this.state.fetchAddress);
    let postalCode = '';

    this.state.fetchAddress.forEach(element => {
      if(element.types == 'postal_code') {
        console.warn('inside postal cond', element.short_name);
        postalCode = element.short_name
      }
    })
    if (postalCode) {
      console.warn('postal code -->', postalCode);
      console.warn('indexOf result --->', this.props.userData.userData.pinCode.indexOf(postalCode));
      if (this.props.userData.userData.pinCode.indexOf(postalCode) >= 0) {
        this.validateForm();
      } else {
        Toast.show('We are currently operating only in Navi Mumbai areas',Toast.LONG);
      }
    } else {
      Toast.show('We are currently operating only in Navi Mumbai areas',Toast.LONG);
    }

  }

  validateForm = () => {
    if(this.state.flatNumber == "") {
      this.setState({
        isFormValid: false,
        flatNoBorder: 1,
        flatNoColor: "red"
      })
      Toast.show('Please fill flat number field',Toast.LONG);
    }  else if(this.state.addressLine2 == "") {
      this.setState({
        isFormValid: false,
        address2Border: 1,
        address2Color: "red"
      })
      Toast.show('Please fill address',Toast.LONG);
    } else if(this.state.isFormValid) {
      this.saveAddress();
    }
  }

  saveAddress = () => {
    console.warn("in save address userLocation",this.state.userLocation);
    console.warn("hiii",this.props.navigation.state.params.callingPage);
    if (this.props.navigation.state.params.callingPage) {
      var tempObj = {
        "addressId": new Date().getTime(),
        "userId": this.props.userData.userData.userDetails[0].userId,
        "userType": this.props.userData.userData.roleDetails[0].roleName,
        "addressType": "Home",
        "position": this.state.region,
        "flatNumber": this.state.flatNumber,
        "addressLine1": this.state.userLocation,
        "addressLine2": this.state.addressLine2,
        "isSelected": this.state.isSelected,
        "backGroundColor": this.state.backGroundColor
      }
      console.warn("address update");
      if(this.props.navigation.state.params.callingPage == "sendPackageList") {
        console.warn('here--->', tempObj);
        this.props.activityAction.deliveryAddress(tempObj);
        this.props.navigation.navigate('sendPackage',{"address": tempObj});
      } else if (this.props.navigation.state.params.callingPage == "deliveryAddress") {
        this.props.activityAction.deliveryAddress(tempObj);
        this.props.navigation.navigate('packageDetailsPage');
      } else if (this.props.navigation.state.params.callingPage == "pickAddress") {
        this.props.activityAction.pickupAddress(tempObj);
        this.props.navigation.navigate('sendPackageList');
      } else if (this.props.navigation.state.params.callingPage == "getPackagePick") {
        this.props.getPackageAction.pickupAddress(tempObj);
        this.props.navigation.navigate('packageDetailsPage');
      } else if (this.props.navigation.state.params.callingPage == "getPackageDeliver") {
        this.props.getPackageAction.deliveryAddress(tempObj);
        this.props.navigation.navigate('packageDetailsPage');
      }
    } else if(this.state.addressId) {
      console.warn('address id exist', this.state.addressId);
      var tempObj = {
        "userId": this.props.userData.userData.userDetails[0].userId,
        "userType": this.props.userData.userData.roleDetails[0].roleName,
        "addressType": "Home",
        "position": this.state.region,
        "flatNumber": this.state.flatNumber,
        "addressLine1": this.state.userLocation,
        "addressLine2": this.state.addressLine2,
        "addressId": this.state.addressId,
        // "isSelected": this.state.isSelected,
        "backGroundColor": this.state.backGroundColor
      }
      this.saveAddressDb(tempObj);
    }
    else{
      console.warn("in else save");
      var tempObj = {
        "userId": this.props.userData.userData.userDetails[0].userId,
        "addressType": "Home",
        "userType": this.props.userData.userData.roleDetails[0].roleName,
        "position": this.state.region,
        "flatNumber": this.state.flatNumber,
        "addressLine1": this.state.userLocation,
        "addressLine2": this.state.addressLine2,
        "isSelected": this.state.isSelected,
        "backGroundColor": this.state.backGroundColor
      }
      this.saveAddressDb(tempObj);
    }

    if (this.props.navigation.state.params.callingPage == "orderDetailsPage" || this.props.navigation.state.params.callingPage == "profile") {
      if (this.state.addressId) {
        var tempObj = {
          "userId": this.props.userData.userData.userDetails[0].userId,
          "userType": this.props.userData.userData.roleDetails[0].roleName,
          "addressType": "Home",
          "position": this.state.region,
          "flatNumber": this.state.flatNumber,
          "addressLine1": this.state.userLocation,
          "addressLine2": this.state.addressLine2,
          "addressId": this.state.addressId,
          // "isSelected": this.state.isSelected,
          "backGroundColor": this.state.backGroundColor
        }
        this.saveAddressDb(tempObj);
      } else {
        var tempObj = {
          "userId": this.props.userData.userData.userDetails[0].userId,
          "addressType": "Home",
          "userType": this.props.userData.userData.roleDetails[0].roleName,
          "position": this.state.region,
          "flatNumber": this.state.flatNumber,
          "addressLine1": this.state.userLocation,
          "addressLine2": this.state.addressLine2,
          "isSelected": this.state.isSelected,
          "backGroundColor": this.state.backGroundColor
        }
          this.saveAddressDb(tempObj);
        }
    }
    

  }

  saveAddressDb = (data) => {
    console.warn("in save address DB",data);
    axios.post(
      Config.URL + Config.addUserAddress,
      data,
      {
        headers: {
            'Content-Type': 'application/json',
        }
      }
    )
    .then(res => {
      console.warn("addressin sucess response",res);
      console.warn("addressin sucess response",this.props);
        if (res.data.status == 0) {  
          console.warn("addressin sucess",this.props);
          res.data.result = res.data.result.map( item => {
            if(item.isSelected == true){
              item.backGroundColor = "#DCF0FA" 
            }
            else{
              item.backGroundColor = "white"
            }
            return item;
          })
          this.props.userData.userData.addressDetails = res.data.result;
          this.props.actions.loginSuccess(this.props.userData.userData);
          if (this.props.navigation.state.params.callingPage == "orderDetailsPage") {
            this.props.navigation.navigate('orderDetailsPage');
          } else {
            this.props.navigationAction.navigateAction('Profile');
            Toast.show(res.data.message,Toast.LONG);
            this.props.navigation.navigate('profilePage');
          }
          
        }
        else if(res.data.status == 1){  
          Toast.show("Failed to save the address. Please try again",Toast.LONG); 
        }
      }).catch(err => {
        console.warn(res.data.status);
        Toast.show("Failed to save the address. Please try again",Toast.LONG); 
    }) 
  }
}




const styles = StyleSheet.create({
  map: {
    ...StyleSheet.absoluteFillObject
  },
  inputBox: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    borderRadius: 10,
    width: '100%',
    height: 40,
    paddingHorizontal: 15
  },
  mapMarkerContainer: {
    left: '47%',
    position: 'absolute',
    top: '42%'
  },
  mapMarker: {
    fontSize: 40,
    color: "red"
  },
  deatilSection: {
    flex: 6,

  },
  spinnerView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  btnContainer: {
    width: Dimensions.get("window").width - 20,
    position: "absolute",
    bottom: 100,
    left: 10
  }
});


//--------------------------------------------------- store related fuction  -- Start

const mapStateToProps = (state) => {
	return {
    userData: state.loginData,
    activityData: state.activityData,
    getPackageData: state.getPackageData,
	};
}

const mapDispatchToProps = (dispatch) => {
    return {
      actions: bindActionCreators(loginSuccess,dispatch),
      activityAction: bindActionCreators(activityAction,dispatch),
      getPackageAction: bindActionCreators(getPackageAction,dispatch),
      navigationAction: bindActionCreators(navigateAction,dispatch),

      //cartActions: bindActionCreators(addToCart,dispatch),
    };
}

// ---------------------------------------------------- store related function  -- END


export default connect(mapStateToProps, mapDispatchToProps)(AddressPage);