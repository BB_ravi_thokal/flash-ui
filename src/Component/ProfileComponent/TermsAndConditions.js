/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Fragment } from 'react';
import  s  from '../../sharedStyle.js';
// import 'bootstrap/dist/css/bootstrap.min.css';
import {
  SafeAreaView,
  StyleSheet,
  View,
  Dimensions,
  Image,
  ScrollView,
  Text
} from 'react-native';
import InternateModal from '../ReusableComponent/InternetModal';
import * as navigateAction  from '../../Actions/NavigationBarAction';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Toast from 'react-native-simple-toast';

class TermsAndConditionsPage extends React.Component{

  constructor(props){
    super(props);    
  
  }

  componentDidMount() {
    
  }
  render(){
    return ( 
      // <SafeAreaView style={{flex: 1}}>
      //   <View style={[s.body,{flex:1}]}>
      //       {/* <InternateModal /> */}
      //       <Image style={styles.imageView} source={require('../../../assets/aboutUs.jpeg')}/>
      //   </View>
      // </SafeAreaView>
      <SafeAreaView style={{flex: 1}}>
      <ScrollView style={[s.body,{flex:1, paddingHorizontal: 15}]} showVerticalScrollIndicator={false}>
      {/* <InternateModal /> */}
        <View>
          <Text style={[s.subHeaderText, styles.spacing]} numberOfLines={300}>Flash – The local delivery app</Text>
          <Text style={[s.normalText]} numberOfLines={10}>
            Install our app and get groceries, vegetables, fruits, dairy products, meat and fish, Plants in a  Flash!

            Grocery shopping just got way easier with Flash, the quickest and safest way for getting groceries delivered right to your home!</Text>
            <Text style={[s.subHeaderText, styles.spacing]} numberOfLines={1}>About this App</Text>
            <Text style={[s.normalText]} numberOfLines={6}>
            With Flash, you can order fresh, healthy foods, and supermarket staples from your favourite brands, right from the comforts of your home. Flash connects independent local vendors and shopkeepers with local consumers to fulfil all their needs.
            </Text>
            <Text style={[s.subHeaderText, styles.spacing]} numberOfLines={1}>Why Flash?</Text>
            <Text style={[s.normalText]} numberOfLines={30}>
            We strive to promote local businesses and help our customers to get their needs delivered in shortest span possible.
            Pick from a variety of grocery or daily needs items that deliver right to your door! Create grocery lists at home and get all items delivered with utmost care and safety.

            As Indias grocer, our urgent mission is to be here for our customers when they need us most, with open-hearted hospitality. We’re taking proactive steps to protect the health and safety of our associates, customers and communities, including adjusted store operating hours, enhanced cleaning procedures, physical distancing precautions and expanded delivery benefits
            </Text>

            <Text style={[s.subHeaderText, styles.spacing]} numberOfLines={1}>Our Services</Text>

            <Text style={[s.subHeaderText, styles.spacing]} numberOfLines={1}>-Plants</Text>
            <Text style={[s.normalText]} numberOfLines={30}>
            Do you have a knack for gardening or just love money plants to decorate your balcony? We've got you covered! With Flash, you can order a large variety of plants at your home!
            </Text>

            <Text style={[s.subHeaderText, styles.spacing]} numberOfLines={2}>Kirana, vegetables, Fruits & Dairy stores</Text>
            <Text style={[s.normalText]} numberOfLines={30}>- Do all your grocery shopping at your fingertips as Flash provides you the best options. Get the best Dairy products as well as the freshest Fruits and Vegetables delivered right to your door.
            </Text>
            <Text style={[s.subHeaderText, styles.spacing]} numberOfLines={2}>Meat and Fish</Text>
            <Text style={[s.normalText]} numberOfLines={10}>We also provide healthy meat and seafood products delivered with the best safety practices.
            </Text>
            <Text style={[s.subHeaderText, styles.spacing]} numberOfLines={2}>-Paan Shop</Text>
            <Text style={[s.normalText]} numberOfLines={10}>A never-before-seen experience exclusive to Flash, we also provide you the favourite after-dinner Paan from the local Paan tapri to save time and make it a perfect meal!
            </Text>





            <Text style={[s.subHeaderText, styles.spacing]} numberOfLines={2}>-Hardware Equipment</Text>
            <Text style={[s.normalText]} numberOfLines={10}>Need a plier, spanner or a drill to get work done at home but not able to locate it? Look no further, we'll be there in a Flash! Get all your hardware requirements satisfied at your doorstep with ease!
            </Text>

            <Text style={[s.subHeaderText, styles.spacing]} numberOfLines={2}>Medicine store</Text>
            <Text style={[s.normalText]} numberOfLines={10}>- Save time and money by ordering all your necessary Medicines ranging from Allopathic medicines, Over the Counter (OTC) products, Homeopathic pills and many more from the comfort of your phone at the best rates.
            </Text>

            <Text style={[s.normalText, {paddingVertical: 10}]} numberOfLines={10}>
            We are currently operating in Navi Mumbai! For any queries or complaints, get in touch with us at flashitt20@gmail.com! Need something? Install our app and get it in a Flash!
            </Text> 
        </View>
                  {/* <Image style={styles.imageView} source={require('../../../assets/aboutUs.jpeg')}/> */}
      </ScrollView>
    </SafeAreaView>
      
    );
  }
}

let ScreenWidth = Dimensions.get("window").width;
let ScreenHeight = Dimensions.get("window").height;

const styles = StyleSheet.create({
    imageView: {
        width: ScreenWidth,
        height: ScreenHeight - 65
    },

    spacing: {
      paddingVertical: 5
    }
	
});


const mapStateToProps = (state) => {
	return {
    userData: state.loginData,
    activityData: state.activityData,
    getPackageData: state.getPackageData,
	};
}
const mapDispatchToProps = (dispatch) => {
    return {
      navigationAction: bindActionCreators(navigateAction,dispatch),
    };
}

// ---------------------------------------------------- store related function  -- END

export default connect(mapStateToProps, mapDispatchToProps)(TermsAndConditionsPage);
