// /**
//  * Sample React Native App
//  * https://github.com/facebook/react-native
//  *
//  * @format
//  * @flow
//  */

// import React, { Fragment } from 'react';

// import * as Config from '../../Constant/Config.js';
// import * as Constant from '../../Constant/Constant.js';
// import { NavigationBar } from '../ReusableComponent/NavigationBar.js';
// import { connect } from 'react-redux';
// import * as loginSuccess  from '../../Actions/LoginAction';
// import * as activityAction  from '../../Actions/ActivityAction';
// import * as getPackageAction  from '../../Actions/GetPackageAction';
// import { bindActionCreators } from 'redux';
// import  s  from '../../sharedStyle.js';
// import Toast from 'react-native-simple-toast';
// // import { Dropdown } from 'react-native-material-dropdown';
// // import 'bootstrap/dist/css/bootstrap.min.css';
// import {
//   SafeAreaView,
//   StyleSheet,
//   ScrollView,
//   View,
//   Text,
//   Dimensions,
//   TouchableOpacity,
//   Button,
//   TextInput,
//   StatusBar,
//   ToastAndroid,
//   TouchableWithoutFeedback,
//   Image,
//   FlatList,
// } from 'react-native';

// import { NavigationEvents } from 'react-navigation';
// import axios from 'axios'; 
// import {
//   Header,
//   LearnMoreLinks,
//   Colors,
//   DebugInstructions,
//   ReloadInstructions,
// } from 'react-native/Libraries/NewAppScreen';
// import InternateModal from '../ReusableComponent/InternetModal';
// import * as navigateAction  from '../../Actions/NavigationBarAction';

   

// class AddressPageOld extends React.Component{

//   constructor(props){
//     super(props);    
//     this.state = {
//       addressId: "",
//       flatNumber: "",
//       flatNoBorder: 0,
//       flatNoColor: "white",
//       addressLine1: "",
//       address1Border: 0,
//       address1Color: "white",
//       addressLine2: "",
//       address2Border: 0,
//       address2Color: "white",
//       addressState: "Maharashtra",
//       addressStateBorder: 0,
//       addressStateColor: "white",
//       district: "",
//       districtBorder: 0,
//       districtColor: "white",
//       pinCode: "",
//       pinCodeBorder: 0,
//       pinCodeColor: "white",
//       isSelected: false,
//       backGroundColor: "white",
//       isFormValid: true,
//       cityData: [ {value:'Navi Mumbai'},{value:'Panvel'},{value:'Kharghar'},{value:'Mumbai'}]
//     }
    
//   }

//   componentDidMount() {
//     // this.getCityData();
//     if(this.props.navigation.state.params.addressId) {
//       console.warn("inside edit -----",this.props.navigation.state.params.addressId);
//       if(this.props.navigation.state.params.callingPage == 'getPackagePick') {
//         this.setState({
//           addressId: this.props.getPackageData.pickupAddress.addressId,
//           flatNumber: this.props.getPackageData.pickupAddress.flatNumber,
//           addressLine1: this.props.getPackageData.pickupAddress.addressLine1,
//           addressLine2: this.props.getPackageData.pickupAddress.addressLine2,
//           addressState: this.props.getPackageData.pickupAddress.state,
//           district: this.props.getPackageData.pickupAddress.district,
//           pinCode: this.props.getPackageData.pickupAddress.pincode
          
//         })
//       } else if(this.props.navigation.state.params.callingPage == 'getPackageDeliver') {
//         this.setState({
//           addressId: this.props.getPackageData.deliveryAddress.addressId,
//           flatNumber: this.props.getPackageData.deliveryAddress.flatNumber,
//           addressLine1: this.props.getPackageData.deliveryAddress.addressLine1,
//           addressLine2: this.props.getPackageData.deliveryAddress.addressLine2,
//           addressState: this.props.getPackageData.deliveryAddress.state,
//           district: this.props.getPackageData.deliveryAddress.district,
//           pinCode: this.props.getPackageData.deliveryAddress.pincode
          
//         })
//       } else if (this.props.navigation.state.params.callingPage == 'sendPackageList') {
//           this.setState({
//             addressId: this.props.activityData.deliveryAddress.addressId,
//             flatNumber: this.props.activityData.deliveryAddress.flatNumber,
//             addressLine1: this.props.activityData.deliveryAddress.addressLine1,
//             addressLine2: this.props.activityData.deliveryAddress.addressLine2,
//             addressState: this.props.activityData.deliveryAddress.state,
//             district: this.props.activityData.deliveryAddress.district,
//             pinCode: this.props.activityData.deliveryAddress.pincode
            
//           })
//       } else if (this.props.navigation.state.params.callingPage == 'pickAddress') {
//         this.setState({
//           addressId: this.props.activityData.pickupAddress.addressId,
//           flatNumber: this.props.activityData.pickupAddress.flatNumber,
//           addressLine1: this.props.activityData.pickupAddress.addressLine1,
//           addressLine2: this.props.activityData.pickupAddress.addressLine2,
//           addressState: this.props.activityData.pickupAddress.state,
//           district: this.props.activityData.pickupAddress.district,
//           pinCode: this.props.activityData.pickupAddress.pincode
          
//         })
//     } else if (this.props.navigation.state.params.callingPage == 'profile') {
//       const index = this.props.userData.userData.addressDetails.findIndex(ind => ind.addressId === this.props.navigation.state.params.addressId);
//         this.setState({
//           addressId: this.props.navigation.state.params.addressId,
//           flatNumber: this.props.userData.userData.addressDetails[index].flatNumber,
//           addressLine1: this.props.userData.userData.addressDetails[index].addressLine1,
//           addressLine2: this.props.userData.userData.addressDetails[index].addressLine2,
//           addressState: this.props.userData.userData.addressDetails[index].state,
//           district: this.props.userData.userData.addressDetails[index].district,
//           pinCode: this.props.userData.userData.addressDetails[index].pincode
//         })
//     } else {
//         const index = this.props.userData.userData.addressDetails.findIndex(ind => ind.addressId === this.props.navigation.state.params.addressId);
//         this.setState({
//           addressId: this.props.navigation.state.params.addressId,
//           flatNumber: this.props.userData.userData.addressDetails[index].flatNumber,
//           addressLine1: this.props.userData.userData.addressDetails[index].addressLine1,
//           addressLine2: this.props.userData.userData.addressDetails[index].addressLine2,
//           addressState: this.props.userData.userData.addressDetails[index].state,
//           district: this.props.userData.userData.addressDetails[index].district,
//           pinCode: this.props.userData.userData.addressDetails[index].pincode
//         })
//     }

//     console.warn("length---->",this.props.userData.userData.addressDetails.length);

//     if(this.props.userData.userData.addressDetails.length <= 1){
//       console.warn('change isSelected----'); 
//       this.setState({
//         isSelected: true,
//         backGroundColor: "#DCF0FA",
//         addressModal: false
//       })
//     }
      
//   }

//   }

//   getCityData = () => {
//     this.setState({
//       isFormValid: true,
//       districtBorder: 0,
//       districtColor: "white"
//     })
//     axios.post(
//       Config.URL + Config.getCity,
//       {
//         "state": this.state.addressState,
//         "searchData": this.state.district
//       },
//       {
//         headers: {
//             'Content-Type': 'application/json',
//         }
//       }
//     )
//     .then(res => {
//       console.warn("city data success",res);
//         if (res.data.status == 0) {  
//           this.setState({
//             cityData: res.data.result
//           })
//         }
//         else if(res.data.status == 1){  
//           // Toast.show("",Toast.LONG);  
//         }
//       }).catch(err => {
//         console.warn(res.data.status);
//     })
//   }



//   render(){
//     return ( 
//       <View style={[s.body],{flex:1,backgroundColor:"#F5FCFF"}}>
//         {/* <InternateModal /> */}
//         <View style={ styles.addressCardView }> 
//           <Text style={ styles.addressText }> Flat Number </Text>
//           <TextInput style={[styles.addressTextInput,{borderWidth: this.state.flatNoBorder, borderColor: this.state.flatNoColor} ]}
//            value={this.state.flatNumber}
//             keyboardType = {'numeric'}
//             onChangeText={(flatNumber) => {
//             this.setState({
//               flatNumber: flatNumber,
//               isFormValid: true,
//               flatNoBorder: 0,
//               flatNoColor: "white"
//             })}} />
//         </View> 
//         <View style={ styles.addressCardView }>
//           <Text style={ styles.addressText }>Address Line 1</Text>
//           <TextInput style={[styles.addressTextInput,{borderWidth: this.state.address1Border, borderColor: this.state.address1Color} ]} 
//           value={this.state.addressLine1}
//             onChangeText={(addressLine1) => {
//             this.setState({
//               addressLine1: addressLine1,
//               isFormValid: true,
//               address1Border: 0,
//               address1Color: "white"
//             })}} />
//         </View>
//         <View style={ styles.addressCardView }>
//           <Text style={ styles.addressText }>Address Line 2</Text>
//           <TextInput style={[styles.addressTextInput,{borderWidth: this.state.address2Border, borderColor: this.state.address2Color} ]} 
//           value={this.state.addressLine2}
//             onChangeText={(addressLine2) => {
//             this.setState({
//               addressLine2: addressLine2,
//               isFormValid: true,
//               address2Border: 0,
//               address2Color: "white"
//             })}} />
//         </View> 
//         <View style={ styles.addressCardView }>
//           <Text style={ styles.addressText }>State</Text>
//           <TextInput style={[styles.addressTextInput,{borderWidth: this.state.addressStateBorder, borderColor: this.state.addressStateColor} ]} 
//           value={this.state.addressState}
//           editable={false}
//             onChangeText={(addressState) => {
//             this.setState({
//               addressState: addressState,
//               isFormValid: true,
//               addressStateBorder: 0,
//               addressStateColor: "white"
//             })}}/>
//         </View>
//         {/* <View style={ styles.addressCardView }>
//           <Text style={ styles.addressText }>District</Text>
//             <Dropdown
//               data={this.state.cityData}
//               value={this.state.district}
//               dropdownOffset={{ 'top': 0 }}
//               onChangeText={(value)=> {this.setState({
//                 district: value
//               });}}
//             />
//         </View> */}

//         <View style={ styles.addressCardView }>   
//           <Text style={ styles.addressText }>Pin Code</Text>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           
//           <TextInput style={[styles.addressTextInput,{borderWidth: this.state.pinCodeBorder, borderColor: this.state.pinCodeColor} ]}
//            value={this.state.pinCode}
//             onChangeText={(pinCode) => {
//             this.setState({
//               pinCode: pinCode,
//               isFormValid: true,
//               pinCodeBorder: 0,
//               pinCodeColor: "white"
//             })}} />  
//         </View> 
//         <View style={{alignItems:"center", marginTop:'2%'}}> 
//           <TouchableOpacity style={styles.addressButtonView} onPress={() => this.submitForm()}>
//               <Text style={styles.addressButtonText}>SAVE</Text>   
//           </TouchableOpacity>
//         </View>
      
//       </View>
//     );
//   }

//   submitForm = () => {
//     if(this.state.flatNumber == "") {
//       this.setState({
//         isFormValid: false,
//         flatNoBorder: 1,
//         flatNoColor: "red"
//       })
//       Toast.show('Please fill flat number field',Toast.LONG);
//     }  else if(this.state.addressLine2 == "") {
//       this.setState({
//         isFormValid: false,
//         address2Border: 1,
//         address2Color: "red"
//       })
//       Toast.show('Please fill address',Toast.LONG);
//     } else if(this.state.isFormValid) {
//       this.saveAddress();
//     }

//   }

//   saveAddress = () => {
//     console.warn("in save address",this.props.userData.userData.userDetails);
//     console.warn("hiii",this.props.navigation.state.params.callingPage);
//     if (this.props.navigation.state.params.callingPage) {
//       var tempObj = {
//         "addressId": new Date().getTime(),
//         "userId": this.props.userData.userData.userDetails[0].userId,
//         "userType": this.props.userData.userData.roleDetails[0].roleName,
//         "addressType": "Home",
//         "flatNumber": this.state.flatNumber,
//         "addressLine1": this.state.addressLine1,
//         "addressLine2": this.state.addressLine2,
//         "isSelected": this.state.isSelected,
//         "backGroundColor": this.state.backGroundColorm
//       }
//       console.warn("address update");
//       if(this.props.navigation.state.params.callingPage == "pickAddress") {
//         this.props.activityAction.pickupAddress(tempObj);
//         this.props.navigation.navigate('sendPackage',{"address": tempObj});
//       } else if (this.props.navigation.state.params.callingPage == "deliveryAddress") {
//         this.props.activityAction.deliveryAddress(tempObj);
//         this.props.navigation.navigate('packageDetailsPage');
//       } else if (this.props.navigation.state.params.callingPage == "sendPackageList") {
//         this.props.activityAction.deliveryAddress(tempObj);
//         this.props.navigation.navigate('sendPackageList');
//       } else if (this.props.navigation.state.params.callingPage == "getPackagePick") {
//         this.props.getPackageAction.pickupAddress(tempObj);
//         this.props.navigation.navigate('packageDetailsPage');
//       } else if (this.props.navigation.state.params.callingPage == "getPackageDeliver") {
//         this.props.getPackageAction.deliveryAddress(tempObj);
//         this.props.navigation.navigate('packageDetailsPage');
//       }
//     } else if(this.state.addressId) {
//       console.warn('address id exist', this.state.addressId);
//       var tempObj = {
//         "userId": this.props.userData.userData.userDetails[0].userId,
//         "userType": this.props.userData.userData.roleDetails[0].roleName,
//         "addressType": "Home",
//         "flatNumber": this.state.flatNumber,
//         "addressLine1": this.state.addressLine1,
//         "addressLine2": this.state.addressLine2,
//         "addressId": this.state.addressId,
//         // "isSelected": this.state.isSelected,
//         "backGroundColor": this.state.backGroundColor
//       }
//       this.saveAddressDb(tempObj);
//     }
//     else{
//       console.warn("in else save");
//       var tempObj = {
//         "userId": this.props.userData.userData.userDetails[0].userId,
//         "addressType": "Home",
//         "userType": this.props.userData.userData.roleDetails[0].roleName,
//         "flatNumber": this.state.flatNumber,
//         "addressLine1": this.state.addressLine1,
//         "addressLine2": this.state.addressLine2,
//         "isSelected": this.state.isSelected,
//         "backGroundColor": this.state.backGroundColor
//       }
//       this.saveAddressDb(tempObj);
//     }


//   }

//   saveAddressDb = (data) => {
//     console.warn("in save address DB",data);
//     axios.post(
//       Config.URL + Config.addUserAddress,
//       data,
//       {
//         headers: {
//             'Content-Type': 'application/json',
//         }
//       }
//     )
//     .then(res => {
//       console.warn("addressin sucess response",res);
//       console.warn("addressin sucess response",this.props);
//         if (res.data.status == 0) {  
//           console.warn("addressin sucess",this.props);
//           res.data.result = res.data.result.map( item => {
//             if(item.isSelected == true){
//               item.backGroundColor = "#DCF0FA" 
//             }
//             else{
//               item.backGroundColor = "white"
//             }
//             return item;
//           })
//           console.warn("1111111",this.props.userData.userData.addressDetails);
//           console.warn("1222222222",res.data.result);
//           this.props.userData.userData.addressDetails = res.data.result;
//           this.props.actions.loginSuccess(this.props.userData.userData, this.props.userData.skipLogin);
//           console.warn("change store",this.props.userData.userData.addressDetails);
//           this.props.navigationAction.navigateAction('Profile');
//           Toast.show(res.data.message,Toast.LONG);
//           this.props.navigation.navigate('profilePage');
//         }
//         else if(res.data.status == 1){  
//           // Toast.show("",Toast.LONG);  
//         }
//       }).catch(err => {
//         console.warn(res.data.status);
//     }) 
//   }
// }

// let ScreenWidth = Dimensions.get("window").width;
// let ScreenHeight = Dimensions.get("window").height;

// const styles = StyleSheet.create({

// 	addressTextInput: {
// 		borderBottomWidth: 1,
// 		borderBottomColor: '#D6D6D6',
// 		height: 35
// 	},

// 	addressText: {
// 		fontSize: 14,
// 		opacity: 0.6,
// 		marginStart: 4,
// 		color: Colors.black
// 	},

// 	addressCardView: {
// 		borderRadius: 5,
// 		marginHorizontal: '2%',
// 		elevation: 4,
// 		alignContent: 'center',
// 		padding: '3%',      
// 		backgroundColor:"white",
// 	},

// 	addressButtonView: {
// 		backgroundColor:"#00CCFF",  
// 		width: '80%',
// 		//borderRadius: 50,
// 		elevation: 6,
// 		shadowColor: 'rgba(0, 0, 0, 0.1)',
// 		shadowOpacity: 0.8,
// 		height: 40,
// 		justifyContent: 'center',
// 		alignItems: 'center',
// 	},

// 	addressButtonText: {
// 		color:"white",  
// 		fontSize: 16 ,
// 		fontWeight:"bold"
// 	},
    

// });


// //--------------------------------------------------- store related fuction  -- Start

// const mapStateToProps = (state) => {
// 	return {
//     userData: state.loginData,
//     activityData: state.activityData,
//     getPackageData: state.getPackageData,
// 	};
// }

// const mapDispatchToProps = (dispatch) => {
//     return {
//       actions: bindActionCreators(loginSuccess,dispatch),
//       activityAction: bindActionCreators(activityAction,dispatch),
//       getPackageAction: bindActionCreators(getPackageAction,dispatch),
//       navigationAction: bindActionCreators(navigateAction,dispatch),

//       //cartActions: bindActionCreators(addToCart,dispatch),
//     };
// }

// // ---------------------------------------------------- store related function  -- END


// export default connect(mapStateToProps, mapDispatchToProps)(AddressPageOld);

