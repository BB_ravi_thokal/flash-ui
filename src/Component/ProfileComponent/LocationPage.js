import React, { Component } from 'react';
import { SafeAreaView, View, Text, Modal, TouchableOpacity, StyleSheet, Dimensions,FlatList, Linking} from 'react-native';
import  s  from '../../sharedStyle.js';
import { getCategoriesByShop } from '../../Constant/Config.js';
import axios from 'axios';
import * as Config from '../../Constant/Config.js';
import * as Constant from '../../Constant/Constant.js';
import { connect } from 'react-redux';
import * as loginSuccess  from '../../Actions/LoginAction';
import { bindActionCreators } from 'redux';
import * as currentLocation  from '../../Actions/CurrentLocationAction';
import * as navigateAction  from '../../Actions/NavigationBarAction';
import Toast from 'react-native-simple-toast';
import AnimatedLoader from "react-native-animated-loader";
import { NavigationActions, StackActions } from 'react-navigation';
const HEIGHT = Dimensions.get('screen').height;

AddressListLoc = ({item, self}) => {
    const {navigate} = self.props.navigation;
    if(self.props.userData.userData.addressDetails.length == 1){
        return(
            <View>
                <TouchableOpacity style={{ padding:10 , marginLeft:5,marginRight:10}} onPress={() => self.selectItem(item.position)}>    
                    <View style={{flex:1,flexDirection:"row"}}>
                        <View style={{flex:1}}>
                            <Text style={{fontSize: 14,fontWeight:"bold",color:"#131E3A"}}>Address</Text>   

                        </View>        
                    </View>
                    <View>
                        <Text style={s.normalText}>{item.addressLine1}</Text>
                        <Text style={s.normalText}>{item.flatNumber}</Text> 
                        <Text style={s.normalText}>{item.addressLine2}</Text> 
                    </View>  
                </TouchableOpacity>
                {/* <View style={{alignItems:"flex-end",padding:10}}>
                    <TouchableOpacity onPress={() =>  navigate('addressPage',{"addressId": ""})}>
                        <Text style={{color:"#00CCFF",fontSize:14}}> + Add New Address</Text>
                    </TouchableOpacity>
                </View> */}

            </View>
        )

    }
    else{
        return(
            
            <View>
                <TouchableOpacity style={{ padding:10 , marginLeft:5,marginRight:10}} onPress={() => self.selectItem(item.position)}>    
                    <View style={{flex:1,flexDirection:"row"}}>
                        <View style={{flex:1}}>
                            <Text style={{fontSize: 14,fontWeight:"bold",color:"#131E3A"}}>Address</Text>   
                        </View>        
                    </View>
                    <View>
                        <Text style={s.normalText}>{item.flatnumber}</Text> 
                        <Text style={s.normalText}>{item.addressLine2}</Text>
                        <Text style={s.normalText}>{item.addressLine1}</Text> 
                         
                    </View>  
                </TouchableOpacity>
                
               
            </View>
        )
        
    }
        
}

class CurrentLocationPage extends Component {
    constructor(props){
        super(props);
        this.state = {
            // itemBackground: "white",
            showLoader: false
        }
    }

    componentDidMount(){
        // this.props.userData.userData.addressDetails.forEach(element => {
        //     if(element.isSelected == true)
        //         this.setState({
        //             itemBackground: "grey"
        //         })
        // });
    }

    showToaster = (message) => {
        setTimeout(() => {
            Toast.show(message, Toast.LONG);
        }, 100);
    }


    selectItem = (region) => {
        this.setState({showLoader: true});
        
        fetch("https://maps.googleapis.com/maps/api/geocode/json?address=" + region.latitude + "," + region.longitude + "&key=" + "AIzaSyCUREttpI8l0AgHrxXAJ8_NhP7XntJTNUU")
          .then((response) => response.json())
          .then((responseJson) => {
            this.showToaster('Fetching local shops near you');  
            // Toast.show('Fetching local shops near you', Toast.LONG); 
            axios.post(
                Config.URL + Config.getTopSlider,
                {
                  "position": region,
                },
                {
                  headers: {
                      'Content-Type': 'application/json',
                  }
                }
              )
              .then(res => {
                if (res.data.status == 0) {
                    this.setState({showLoader: false});
                    const userLocation = responseJson.results[0].formatted_address;
                    this.props.loginActions.addTopSliders(res.data.result);
                    this.props.locationAction.changeLocation(region, responseJson.results[0].address_components[1].short_name);
                    this.props.navigationAction.navigateAction('Home');
                    const resetAction = StackActions.reset({
                        index: 0,
                        actions: [NavigationActions.navigate({ routeName: 'homePage'})]
                    });
                    this.props.navigation.dispatch(resetAction);
                } else {
                    this.setState({showLoader: false});
                    this.showToaster('Failed to locate the location please try again');
                    // Toast.show('Failed to locate the location please try again', Toast.SHORT);
                }
    
              })
            // const userLocation = responseJson.results[0].formatted_address;
            // console.warn("responseJson----------", responseJson.results[0])
            // this.props.locationAction.changeLocation(region, responseJson.results[0].address_components[1].short_name);
            // this.props.navigationAction.navigateAction('Home');
            // this.props.navigation.navigate('homePage');
        });
    }
    

     
//   componentWillMount(){             
//     const userId = "U15796901450423894000";
//     console.warn("cat id---",userId);

//     axios.post(
// 			Config.URL + Config.getUserDetails,
// 			{
// 				"userId": userId,
// 			},
// 			{
// 				headers: {
// 						'Content-Type': 'application/json',
// 				}
// 			}
//     )
//     .then(res => {
//         res = res.map(item => {
//           item.iseSelected = false;
//           item.isSelectedClass = styles.list;
//           return item;
//         });
//         console.warn("res---",res.data.result); 
//         console.warn("---------------- length",res.data.result[0].addressDetails.length);  
//         if (res.data.status == 0) {  
//           this.setState({
//             data: res.data.result[0], 
//             address:res.data.result[0].addressDetails
//           });
          
//           if(res.data.result[0].addressDetails.length === 0){
//             this.setState({
//               addAddress: true
//             })
//           }
//         }
//         else if(res.data.status == 1){  
//           Toast.show("Sucesssssssssssss",Toast.LONG);  
//         }
//       }).catch(err => {
//           //console.warn(res.data.status);
//       })
//   }

    MyAddress = () => {
        const {navigate} = this.props.navigation;
        if(this.props.userData.userData.addressDetails.length == 0){
          return(
            <View style={{backgroundColor:"white"}} >
              <View style={{ padding:10 , marginLeft:5}}>
                <Text style={s.subHeaderText}>Saved address</Text>
              </View>
              <View style={{display:"flex",alignItems:"center", paddingBottom: 15}}>
                  <Text style={[s.normalText, {color:"silver"}]}>Address not added yet</Text>
              </View>
            </View>
          )
        }
        else{
          return(
            <View style={{backgroundColor:"white"}}>
                <View style={{ padding:10 , marginLeft:5}}>
                <Text style={s.subHeaderText}>Saved address</Text>            
                </View>
                <FlatList data={this.props.userData.userData.addressDetails}   
                    renderItem={( { item } ) => (
                    <AddressListLoc
                        item = {item}
                        self={this}
                    // navigation={this.props.navigation}
                    />
                    )}
                    keyExtractor= {item => item.addressId}
                    horizontal={false} 
                    extraData= {this.props}
                />
            </View>
            
          )
        }
    }

    

    render() {
        const {navigate} = this.props.navigation;
        return (
            <SafeAreaView style={{flex: 1}}>
                <AnimatedLoader
                    visible={this.state.showLoader}
                    overlayColor="rgba(255,255,255,0.75)"
                    source={require("../../../assets/loader.json")}
                    animationStyle= {[styles.lottie,{height: 150,width: 150}]}
                    speed={1}
                />
               <View style={styles.rootView}>
                    <View>
                        <View style={{backgroundColor: "white", elevation: 6}}>
                            <TouchableOpacity style={{marginVertical: 20, paddingHorizontal: 15}} onPress={() => navigate('selectLocationPage')}>
                                <Text style={s.subHeaderText}>Current Location</Text>
                                <Text style={[s.normalText,{color:"silver"}]}>Using GPS</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{backgroundColor: "white", elevation: 6, marginTop: 10}}>
                            <this.MyAddress />
                        </View>
                    </View>
                </View>
        
            </SafeAreaView>
        
        );
    }
}

const styles = StyleSheet.create({
    rootView: {
        flex: 1,
    },
    modalView: {
        backgroundColor:"#F5F5F5",
        bottom: 0,
        position: "absolute"
    },
    sChildHeaderView: {
        paddingHorizontal: 15,
        paddingVertical: 10,
        flexDirection: 'row',
        flexWrap: 'wrap',
    },
    headerTextView: {
        flex: 4, 
        flexDirection: 'row' 
    },
    headerText: {
        color: "black",  
        fontSize:18,
    },
    headerIcon: {
        flex: 1, 
        alignItems: 'flex-end'
    },
 
    bodyMessageView: {
        marginTop: 2,
        paddingHorizontal: 15
    },
    bodyText: {
        fontSize: 16,
        textAlign:'justify',

    },
    buttonView: {
        backgroundColor: '#3692E5',
        width: '80%',
        borderRadius: 50,
        elevation: 6,
        shadowColor: 'rgba(0, 0, 0, 0.1)',
        shadowOpacity: 0.8,
        height: 20,
        justifyContent: 'center',
        alignItems: 'center',
        bottom: 80
      },
    applyButtonStyle: {
        backgroundColor: "blue",
        borderRadius: 25, 
        width: '75%', 
        padding:10,
        // height: 42,
        alignItems: 'center', 
        justifyContent: 'center',
        elevation: 4
    },
    bodyButtonView: {
        marginTop: 10, 
        marginBottom:10, 
        alignItems: 'center', 
        justifyContent: 'center',
        // backgroundColor:'green'
    },
    applyTextStyle: {
        color: "#00000099",
        textTransform: 'uppercase',
    },
})

//--------------------------------------------------- store related fuction  -- Start

const mapStateToProps = (state) => {
	return {
    userData: state.loginData,
    currentLocation: state.currentLocation
	};
}

const mapDispatchToProps = (dispatch) => {
    return {
      loginActions: bindActionCreators(loginSuccess,dispatch),
      locationAction: bindActionCreators(currentLocation,dispatch),
      navigationAction: bindActionCreators(navigateAction,dispatch),
    };
}

// ---------------------------------------------------- store related function  -- END

export default connect(mapStateToProps, mapDispatchToProps)(CurrentLocationPage);
