/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Fragment } from 'react';

import * as Config from '../../Constant/Config.js';
import * as Constant from '../../Constant/Constant.js';
import { connect } from 'react-redux';
import * as loginSuccess  from '../../Actions/LoginAction';
import { bindActionCreators } from 'redux';
import  s  from '../../sharedStyle.js';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  Dimensions,
  TouchableOpacity,
  Button,
  TextInput,
  StatusBar,
  TouchableWithoutFeedback,
  Image,
  FlatList,
  Modal,
  ToastAndroid,
  AsyncStorage
} from 'react-native';

import axios from 'axios'; 
import {
  Colors,
} from 'react-native/Libraries/NewAppScreen';
import InternateModal from '../ReusableComponent/InternetModal';
import AdminNavigationBar from '../ReusableComponent/adminNavigationBar';
import * as navigateAction  from '../../Actions/NavigationBarAction';
import { NavigationActions, StackActions } from 'react-navigation';
import Toast from 'react-native-simple-toast';

class AdminProfilePage extends React.Component{

  constructor(props){
    super(props);    
    this.state = {
      data : "", 
      address:[],
      addAddress: false,
      isModalVisible: false,
      removeAddressId: [],
      removeAddressIndex: 0,       
      getActivityData: [],
      sendActivityData: [],
      orderData: []   
    }
    
  }
	
  logoutAction = () => {
    Toast.show("Logout successfully!!",Toast.LONG);
    AsyncStorage.setItem("alreadyLunched",'loggedOut');
    const resetAction = StackActions.reset({
        index: 0,
        actions: [NavigationActions.navigate({ routeName: 'login'})]
  
      })
      this.props.navigation.dispatch(resetAction);
  }


  
  render(){
    const {navigate} = this.props.navigation;
    return (
      <SafeAreaView style={{flex: 1}}>
        <View style={[styles.mainView]}>
          {/* <InternateModal /> */}
          <ScrollView>
            <View >
              <View>
                <View style={{backgroundColor:"white"}}>    
                  <View style={{padding:10 , marginLeft:5 , marginRight:5 , borderBottomWidth:3,borderBottomColor:"#003151"}}>
                    <View style={{flex:1,flexDirection:"row"}}>
                      <View style={{flex:1}}>
                        <Text style={s.headerText}>{this.props.userData.userData.userDetails[0].fullName}</Text>  
                      </View>
                    </View>
                    <Text style={s.normalText}>{this.props.userData.userData.userDetails[0].emailId}</Text>
                  </View>     
                </View>
              </View>
              {/* <this.MyAddress /> */}
               <TouchableOpacity style={{backgroundColor:"white",marginTop:5,marginBottom:5}} onPress={() => this.logoutAction()}>
                  <View style={{flex:1,flexDirection:"row",paddingVertical:10,alignItems:"center",paddingHorizontal:15}}>
                    <Text style={s.headerText}>Log Out</Text>
                    <View style={{flex:1,alignItems:"flex-end",marginRight:5}}>
                      <Image style={{height:15,width:15}} source={require('../../../assets/next.png')}/>
                    </View>
                  </View>
              </TouchableOpacity>
            </View>
            </ScrollView>
            <AdminNavigationBar navigate={navigate}></AdminNavigationBar>  
          </View>
      
      </SafeAreaView>

      
    );
  }

}


let ScreenWidth = Dimensions.get("window").width;
let ScreenHeight = Dimensions.get("window").height;

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,
    height: ScreenHeight,
    paddingBottom:20,
  },
  addtoCartBtnColor: {
    borderWidth: 2,
    borderColor: '#00CCFF',
  //  backgroundColor: '#00009a',
    //color: 'white',
    marginTop: 10
  },
  storeBtn: {
    paddingVertical: 5,
    marginBottom: 5,
    color: 'white',
    borderRadius: 0,
    width: 150,
  },
  containerIcons:{
    width:30,
    height:30,
    backgroundColor:'blue',
  },
  body: {
    backgroundColor: 'grey',
  },
  mainView: {
    // backgroundColor: '#fff',
    flex: 1,
    //height: ScreenHeight - 135
  },
  categoryFooter:{
    width:ScreenWidth,
    paddingVertical:10,
    height:60,
    backgroundColor:'lightgray',
    display:'flex',
    flexDirection:'row',
    justifyContent:"space-evenly",
    alignItems:"center"
  },

  profileViewData:{
    display:"flex",
    alignItems:"center",
    justifyContent:"space-between",
    flexDirection:"row",
    paddingTop:5
  },
  flexRow:{
    display:'flex',
    flexDirection:'row',
  },
updateprofileBtn:{
  padding:5,
  marginLeft:5,
  backgroundColor:'white',
  borderWidth:1,
  borderColor:"lightgray"
},
  profileLayout: {
    height: 150,
    width: ScreenWidth,
    borderBottomLeftRadius: ScreenWidth/2,
    borderBottomRightRadius: ScreenWidth/2,
    borderColor: 'black',
    backgroundColor: 'skyblue',
    position:'relative',
    justifyContent: 'center',
    zIndex:1,
  },
  profilemage:{
    position:'absolute',
    alignSelf: 'center',
    bottom:'-30%',
    height:100,
    width:100,
    borderRadius:50,
    backgroundColor:'darkblue',
  
  },
  profileHeading: {
    color: 'black',
    fontSize: 20,
    fontWeight:"bold"
  },
  profileText:{
    color: 'black',
    fontSize: 20,
    fontWeight:'600',
  },
  profileData:{ 
    borderBottomWidth:1,
    borderBottomColor:'grey',
    paddingVertical:5,
    marginBottom:10
 },
 profileAboutView: {
  marginHorizontal: 20,
  paddingVertical:5,
  // flex: 3,
  backgroundColor: '#fff',
},
  profileMainView: {
    marginHorizontal: 20,
    paddingTop:20,
    // flex: 3,
    backgroundColor: '#fff',
  },
  profilePartition:{
    width:ScreenWidth,
    height:20,
    backgroundColor: 'lightgrey',
    marginTop:10
  },

  // mk start --
  profileHeading: {
    color: 'black',
    fontSize: 20,
    fontWeight:"bold"
  },

});


//--------------------------------------------------- store related fuction  -- Start

const mapStateToProps = (state) => {
	return {
    userData: state.loginData,
	};
}

const mapDispatchToProps = (dispatch) => {
    return {
      loginActions: bindActionCreators(loginSuccess,dispatch),
      navigationAction: bindActionCreators(navigateAction,dispatch),
    };
}

// ---------------------------------------------------- store related function  -- END

export default connect(mapStateToProps, mapDispatchToProps)(AdminProfilePage);  


