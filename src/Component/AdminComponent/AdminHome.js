/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as Config from '../../Constant/Config.js';
import  s  from '../../sharedStyle.js'
import InternateModal from '../ReusableComponent/InternetModal';
import React, { Component, useState } from "react";
import { SafeAreaView, StyleSheet, Text, View, FlatList, Button,Modal,TouchableHighlight, ToastAndroid } from 'react-native';
import axios from 'axios';
import * as navigateAction  from '../../Actions/NavigationBarAction';
import AnimatedLoader from "react-native-animated-loader";
// import SceneLoader from 'react-native-scene-loader';
import { ScrollView } from 'react-native-gesture-handler';
import AdminNavigationBar from '../ReusableComponent/adminNavigationBar';
import Toast from 'react-native-simple-toast';

class AdminHomePage extends React.Component{
  constructor(props) {
    super(props);
    this.state={
      modalVisible: false,
      accountNumber: "",
      acctHolderName: "",
      ifscCode : "",
      showLoader: false,
      userData: [],
      modalData: {}
    }
  }

  setModalVisible(visible, id) {
    if(visible) {
        const index = this.state.userData.findIndex(ind => ind.withDrawId === id);
        this.setState({
          modalData: this.state.userData[index]
        })
    } else {
      this.setState({
        modalData: {}
      })
    }
    
    this.setState({modalVisible: visible});
  }
  amountRefunded = (withDrawId, userId) => {
    console.warn('in amount refunded', withDrawId, userId);
    axios.post(
      Config.URL + Config.updateWithDrawalStatus,
      {
        "withDrawId" : withDrawId,
        "userId": userId
      },
      {
        headers: {
            'Content-Type': 'application/json',
        }
      }
    )
    .then(res => {
      console.warn('response--->', res);
        if (res.data.status == 0) {
          Toast.show("Request mark as completed",Toast.LONG);
          this.setState({
            modalVisible: false,
            modalData: {},
            userData: res.data.result
          });
        } else {
          Toast.show("Not able to update status. Please try again",Toast.LONG);
        }
      }).catch(err => {
        console.warn('error in refund', err);
    })
    

  }
  componentDidMount(){
    console.warn('in admin home ---', this.state.showLoader);
    console.warn('userData---->', this.props.userData.userData);
    this.setState({
      showLoader: true
    })

    axios.post(
      Config.URL + Config.getWithDrawRequest,
      {
        headers: {
            'Content-Type': 'application/json',
        }
      }
    )
    .then(res => {
      this.setState({
        showLoader: false
      })
      console.warn('response--->', res.data.result[0].userDetails.fullName);
      if (res.data.status == 0) {
        this.setState({
          userData : res.data.result
        })
      } else {
          console.warn('error-- get wallet');
      }
  
  
    })

  }


  render() {
    const {navigate} = this.props.navigation;
    return (
      <SafeAreaView style={{flex: 1}}>
        <View style={this.state.modalVisible
            ? styles.containerBlur
            : styles.container}>
            <Modal
              animationType="slide"
              transparent={true}
              visible={this.state.modalVisible}
            >
              <View style={styles.centeredView}>
              <AnimatedLoader
                  visible={this.state.showLoader}
                  overlayColor="rgba(255,255,255,0.75)"
                  source={require("../../../assets/loader.json")}
                  animationStyle= {[styles.lottie,{height: 150,width: 150}]}
                  speed={1}
                />
                {/* <SceneLoader
                  visible={this.state.showLoader}
                  animation={{
                      fade: {timing: {duration: 1000 }}
                  }}
                /> */}
                <View style={styles.modalView}>            
                  <View style={{flexDirection: "row", justifyContent: "space-between"}}>
                    <Text style={styles.textStyle}>Account Details</Text>
                    <TouchableHighlight
                      style={{ ...styles.closeButton }}
                      onPress={() => {
                      this.setModalVisible(false);
                      }}
                    >
                      <Text style={styles.textStyle}>X</Text>
                    </TouchableHighlight>
                  </View>
                  <View style={styles.horizontalSpaceLineForModel} />
                  <View style={{flexDirection: "row", justifyContent: "space-between"}}>
                    <Text style={styles.textStyle}>Acct. Holder Name</Text>
                    <Text style={styles.textStyle}>{this.state.modalData.acctHolderName}</Text>
                  </View>
                  <View style={{flexDirection: "row", justifyContent: "space-between"}}>
                    <Text style={styles.textStyle}>Acct. Number</Text>
                    <Text style={styles.textStyle}>{this.state.modalData.acctNo}</Text>
                  </View>
                  <View style={{flexDirection: "row", justifyContent: "space-between"}}>
                    <Text style={styles.textStyle}>IFSC Code</Text>
                    <Text style={styles.textStyle}>{this.state.modalData.ifscCode}</Text>
                  </View>
                  <View style={styles.horizontalSpaceLineForModel} />
                  <TouchableHighlight
                      style={{ ...styles.refundButton }}
                      onPress={() => {
                      this.amountRefunded(this.state.modalData.withDrawId, this.state.modalData.userId);
                      }}
                    >
                      <Text style={styles.refundButtonText}>Amount Refunded</Text>
                    </TouchableHighlight>
                </View>
              </View>
            </Modal>
            <View style={{alignItems:"center"}}>
              <ScrollView showsVerticalScrollIndicator={false} style={{marginBottom: 15}}>
                <FlatList 
                  data = {this.state.userData}
                  renderItem={({item}) =>(
                      <this.renderCard
                        item = {item}
                      />
                    )            
                  }
                  extraData = {this.state.userData}
                />
              </ScrollView>
              <AdminNavigationBar navigate={navigate}></AdminNavigationBar>  
            </View>
            
          </View>
      </SafeAreaView>
      
    );
  }

  renderCard = (item) => {
    return(
      <View style={styles.card}>
        <View style={{flexDirection: "row", justifyContent: "space-between"}}>
          <Text>withDrawId:</Text>
          <Text>{item.item.withDrawId}</Text>
        </View>
        <View style={styles.horizontalSpaceLine} />
        <View style={{flexDirection: "row", justifyContent: "space-between"}}>
          <Text>User Name</Text>
          <Text>{item.item.userDetails.fullName}</Text>
        </View>
        <View style={{flexDirection: "row", justifyContent: "space-between"}}>
          <Text>Refund Amount</Text>
          <Text>{item.item.walletAmount}</Text>
        </View>
        <View style={styles.horizontalSpaceLine} />
        <View>
          <TouchableHighlight
            style={this.state.modalVisible
              ? styles.openButtonBlur
              : styles.openButton}
            onPress={() => {
              this.setModalVisible(true, item.item.withDrawId);
            }}
          >
            <Text style={styles.textStyle}>Account Details</Text>
          </TouchableHighlight>
        </View>
      </View>
    )
  }



}


const styles = StyleSheet.create({
  container: {
    alignItems: "center",
  },
  containerBlur: {
    flex:1,
    alignItems: "center",
    backgroundColor: 'rgba(100,100,100, 0.5)',
  },
  card: {
    marginTop:10,
    borderRadius: 3,
    borderColor: 'transparent',
    width: 320,
    height: 140,
    padding: 10,
    shadowColor: "#000",
    elevation: 4,
  },
  horizontalSpaceLine:{
    borderBottomColor : "#808080",
    borderBottomWidth : 3,
    marginBottom: 5,
    marginTop: 5
  },
  horizontalSpaceLineForModel:{
    borderBottomColor : "white",
    borderBottomWidth : 3,
    marginBottom: 5,
    marginTop: 5
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    marginTop: 22
  },
  modalView: {
    margin: 20,
    backgroundColor: "#00CCFF",
    borderRadius: 20,
    padding: 10,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5
  },
  openButton: {
    backgroundColor: "#00CCFF",
    borderRadius: 20,
    padding: 10,
    elevation: 2
  },
  openButtonBlur:{
    backgroundColor: 'rgba(100,100,100, 0.5)',
    borderRadius: 20,
    padding: 10,
    elevation: 2
  },
  closeButton: {
    backgroundColor: "#00CCFF",
    borderRadius: 20,
    padding: 1,
    elevation: 2,
  },
  refundButton: {
    backgroundColor: "white",
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center"
  },
  refundButtonText: {
    color: "#00CCFF",
    fontWeight: "bold",
    textAlign: "center"
  },
  modalText: {
    marginBottom: 15
  }
})

//--------------------------------------------------- store related fuction  -- Start

const mapStateToProps = (state) => {
	return {
    userData: state.loginData,
	}
}

const mapDispatchToProps = (dispatch) => {
    return {
        navigationAction: bindActionCreators(navigateAction,dispatch),
    };
}

// ---------------------------------------------------- store related function  -- END


export default connect(mapStateToProps, mapDispatchToProps)(AdminHomePage);


