import React, { Component } from 'react';
import { SafeAreaView, View, Text, Modal, TouchableOpacity,
    ScrollView, StyleSheet, Dimensions,FlatList, TextInput, KeyboardAvoidingView } from 'react-native';
import  s  from '../../sharedStyle.js';
import { getCategoriesByShop } from '../../Constant/Config.js';
import axios from 'axios';
import * as Config from '../../Constant/Config.js';
import * as Constant from '../../Constant/Constant.js';
import { connect } from 'react-redux';
import * as basketAction  from '../../Actions/BasketAction';
import * as loginAction  from '../../Actions/LoginAction';
import { bindActionCreators } from 'redux';
import Toast from 'react-native-simple-toast';

const HEIGHT = Dimensions.get('screen').height;

BasketList = ({item, self}) => {
    return(
        <View>
            <TouchableOpacity style={{ padding:15, justifyContent:"center" }} 
                onPress={() => self.updateBasket(item.basketId)} >  
                    <Text style={s.headerText}>{item.basketName}</Text>
            </TouchableOpacity>
        </View>
    )
        
}


class BasketModal extends Component {
    constructor(props){
        super(props);
        this.state = {
            itemBackground: "white",
            showBasketInput: false,
            newBasketTag: "",
        }
    }

    componentDidMount(){
        console.warn("in basket modal",this.props);
        // this.props.userData.userData.addressDetails.forEach(element => {
        //     if(element.isSelected == true)
        //         this.setState({
        //             itemBackground: "grey"
        //         })
        //   });
    }

    selectItem = (addressId) => {
        // console.warn("in select Item",this.props.userData.userData.addressDetails)
        // // const i = this.props.userData.userData.addressDetails.findIndex(ind => ind.addressId === this.props.userData.userData.addressDetails.addressId);
        // this.props.userData.userData.addressDetails.forEach(element => {
        //     if(element.addressId == addressId){
        //         element.isSelected = true
        //         element.backGroundColor = "#DCF0FA"
        //     }
        //     else{
        //         element.isSelected = false
        //         element.backGroundColor = "white"
        //     }
        // });

        // console.warn("in select Item after change",this.props.userData.userData);
        // this.props.loginActions.loginSuccess(this.props.userData.userData);

    }

     
//   componentWillMount(){             
//     const userId = "U15796901450423894000";
//     console.warn("cat id---",userId);

//     axios.post(
// 			Config.URL + Config.getUserDetails,
// 			{
// 				"userId": userId,
// 			},
// 			{
// 				headers: {
// 						'Content-Type': 'application/json',
// 				}
// 			}
//     )
//     .then(res => {
//         res = res.map(item => {
//           item.iseSelected = false;
//           item.isSelectedClass = styles.list;
//           return item;
//         });
//         console.warn("res---",res.data.result); 
//         console.warn("---------------- length",res.data.result[0].addressDetails.length);  
//         if (res.data.status == 0) {  
//           this.setState({
//             data: res.data.result[0], 
//             address:res.data.result[0].addressDetails
//           });
          
//           if(res.data.result[0].addressDetails.length === 0){
//             this.setState({
//               addAddress: true
//             })
//           }
//         }
//         else if(res.data.status == 1){  
//           Toast.show("Sucesssssssssssss",Toast.LONG);  
//         }
//       }).catch(err => {
//           //console.warn(res.data.status);
//       })
//   }

    showInputBox = () => {
        this.setState({
            showBasketInput: true
        })
    }

    cancelFromBasket = () => {
        this.setState({
            showBasketInput: false
        })
        this.props.onDismiss();
    }

    updateBasket = (id) => {
        const index = this.props.basketItem.basketItem.findIndex(ind => ind.basketId === id);
        console.warn('index in update basket', index);
        if (this.props.basketItem.basketItem[index].itemDetails.some(arr => arr.productId == this.props.basketData.menu.productId)){
            Toast.show("Already added in the basket", Toast.LONG);
        } else {
            const basket = {
                productId: this.props.basketData.menu.productId,
                productName: this.props.basketData.menu.productName,
                productLogo: this.props.basketData.menu.productLogo,
                productDescription: this.props.basketData.menu.productDescription,
                unitCost: this.props.basketData.menu.unitCost,
                unit: this.props.basketData.menu.unit,
                shopId: this.props.basketData.shopDetails.shopId,
                shopName: this.props.basketData.shopDetails.shopName,
                category: this.props.basketData.categoryId,
                counter: 1,
                totalCost: this.props.basketData.menu.unitCost,  
                isProductAvailable: true,
                cgst: parseFloat(this.props.basketData.menu.cgst),
                sgst: parseFloat(this.props.basketData.menu.sgst),
                costExlGst: parseFloat(this.props.basketData.menu.unitCostExclTax),
                shopRate: parseFloat(this.props.basketData.menu.shopRate),
                shopAddress: this.props.basketData.shopDetails.shopAdress,
                shopContactNumber: this.props.basketData.shopDetails.shopContactNumber,
                shopEmail: this.props.basketData.shopDetails.shopEmail,
                shopLocation: this.props.basketData.shopDetails.shopLocation,
                shopOwnerName: this.props.basketData.shopDetails.shopOwnerName,
                isBogo: false
            };    
            this.props.basketItem.basketItem[index].itemDetails.push(basket);
            console.warn("after add",this.props.basketItem.basketItem);
            this.props.basketAction.replaceBasket(this.props.basketItem.basketItem);
            Toast.show(this.props.basketData.menu.productName + " is added in the basket " + this.state.newBasketTag,
                Toast.LONG);
        }

        this.props.onDismiss();
        
    }

    createNewBasket = () => {
        
        const basket = [{
            basketId: Date.now(),
            basketName: this.state.newBasketTag,
            // userId: this.props.userData.userData.userId,
            itemDetails: [{
                productId: this.props.basketData.menu.productId,
                productName: this.props.basketData.menu.productName,
                productLogo: this.props.basketData.menu.productLogo,
                productDescription: this.props.basketData.menu.productDescription,
                unitCost: this.props.basketData.menu.unitCost,
                unit: this.props.basketData.menu.unit,
                shopId: this.props.basketData.shopDetails.shopId,
                shopName: this.props.basketData.shopDetails.shopName,
                category: this.props.basketData.categoryId,
                counter: 1,
                totalCost: this.props.basketData.menu.unitCost,  
                isProductAvailable: true,
                cgst: parseFloat(this.props.basketData.menu.cgst),
                sgst: parseFloat(this.props.basketData.menu.sgst),
                costExlGst: parseFloat(this.props.basketData.menu.unitCostExclTax),
                shopRate: parseFloat(this.props.basketData.menu.shopRate),
                shopAddress: this.props.basketData.shopDetails.shopAdress,
                shopContactNumber: this.props.basketData.shopDetails.shopContactNumber,
                shopEmail: this.props.basketData.shopDetails.shopEmail,
                shopLocation: this.props.basketData.shopDetails.shopLocation,
                shopOwnerName: this.props.basketData.shopDetails.shopOwnerName, 
                isBogo: false
            }]
        }]
        this.setState({
            showBasketInput: false
        })
        console.warn("basket data---",basket);
        this.props.basketAction.addToBasket(basket);
        console.warn("basket data---",this.props.basketItem);
        Toast.show(this.props.basketData.menu.productName + " is added in the basket " + this.state.newBasketTag, Toast.LONG);
        this.props.onDismiss();
        // this.props.visible = false;
    }

    MyAddress = () => {
        if(this.props.basketItem.basketItem.length == 0){
            if(!this.state.showBasketInput) {
                return(
                    <View style={{backgroundColor:"white"}} >
                        <View style={{display:"flex",alignItems:"center"}}>
                            <View style={[{display:"flex",alignItems:"center",padding: 20}]} >
                            <Text style={{ color:'#0080FE',fontSize: 16,fontWeight:"bold",justifyContent:"center"}}>PLEASE CREATE THE BASKET</Text> 
                            </View>
                        </View>
                    </View>
                )
            }
            else {
                return (
                    <View style={{backgroundColor:"white"}}>
                        {/* <NewBasketInput /> */}
                        <View>
                            <KeyboardAvoidingView behavior="padding">
                                <View style={ styles.addressCardView }>     
                                    <Text style={ styles.addressText }>Basket Tag</Text>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          
                                    <TextInput style={ styles.addressTextInput } placeholder="Enter basket name..." placeholderTextColor="#A9A9A9" 
                                        onChangeText={(newBasketTag) => {
                                        this.setState({
                                            newBasketTag: newBasketTag
                                        })}}/>  
                                </View> 
                                
                            </KeyboardAvoidingView>
                           
                            <View style={{width: '100%', flexDirection:"row"}}>
                                <TouchableOpacity style={{width: '50%',backgroundColor:"#F5F5F5",paddingLeft:15,alignItems:"center"}}
                                    onPress={() => this.cancelFromBasket()}>
                                    <Text style={[s.headerText,{paddingVertical:10}]}>Cancel</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={{width: '50%',backgroundColor:"#00CCFF",alignItems:"center"}}
                                    onPress={() => this.createNewBasket()}>
                                    <Text style={[s.headerText,{paddingVertical:10,color:"white"}]}>Create</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                )
            }
        }
        else {
            if(!this.state.showBasketInput){
                return(
                    <View style={{backgroundColor:"white"}}>
                        <View style={{ padding:10 , marginLeft:5}}>
                        <Text style={{fontSize: 16, color: "grey",fontWeight: "bold" }}>ADD TO EXISTING BASKET</Text>            
                        </View>
                        <FlatList data={this.props.basketItem.basketItem}   
                            renderItem={( { item } ) => (
                            <BasketList
                                item = {item}
                                self = {this}
                            // navigation={this.props.navigation}
                            />
                            )}
                            keyExtractor= {item => item}
                            horizontal={false} 
                            extraData= {this.props}
                        />
                        
                    </View>

                )
            }
            else {
                return (
                    <View style={{backgroundColor:"white"}}>
                        {/* <this.NewBasketInput /> */}
                        <View>
                            <View style={ styles.addressCardView }>     
                                <Text style={ styles.addressText }>Basket Tag</Text>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          
                                <TextInput style={ styles.addressTextInput } placeholder="Enter basket name..." placeholderTextColor="#A9A9A9" 
                                    onChangeText={(newBasketTag) => {
                                    this.setState({
                                        newBasketTag: newBasketTag
                                    })}}/>  
                            </View> 
                            <View style={{width: '100%', flexDirection:"row"}}>
                                <TouchableOpacity style={{width: '50%',backgroundColor:"#F5F5F5",paddingLeft:15,alignItems:"center"}}
                                    onPress={() => this.cancelFromBasket()}>
                                    <Text style={[s.headerText,{paddingVertical:10}]}>Cancel</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={{width: '50%',backgroundColor:"#00CCFF",alignItems:"center"}}
                                    onPress={() => this.createNewBasket()}>
                                    <Text style={[s.headerText,{paddingVertical:10,color:"white"}]}>Create</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                )
                
            }  
        }
      }
        


    render() {
        const {navigate} = this.props.navigation;
        return (
            // <SafeAreaView style={{flex: 1}}>
                <Modal
                    transparent={true}
                    animationType={'fade'}
                    visible = {this.props.visible}
                    onRequestClose={this.props.onDismiss}
                    animated={true}>
                    <View style={styles.rootView}>
                    <KeyboardAvoidingView behavior="padding" style={{flex: 1}}>
                        <View style={{flex: 8}}>
                        </View>
                        <View style={{flex: 4}}>
                            <ScrollView style={styles.modalView}>
                                <TouchableOpacity style={{backgroundColor:"#00CCFF",padding: 15}} onPress={() => this.showInputBox()}>
                                    <Text style={{fontSize: 20,fontWeight: "bold",color:"white"}}>+ CREATE NEW BASKET</Text>
                                </TouchableOpacity>
                                <this.MyAddress />
                            </ScrollView>
                        </View>
                        {/* <View style={{ height: 100 }} /> */}
                    </KeyboardAvoidingView>
                        
                    </View>
                </Modal>
            // </SafeAreaView>
           
        )
    }
}

const styles = StyleSheet.create({
    rootView: {
        backgroundColor: '#00000099',
        flex: 1,
    },
    modalView: {
        backgroundColor:"#F5F5F5",
        bottom: 0,
        width: '100%',
        position: "absolute",
        elevation: 4
    },
    sChildHeaderView: {
        paddingHorizontal: 15,
        paddingVertical: 10,
        flexDirection: 'row',
        flexWrap: 'wrap',
    },
    headerTextView: {
        flex: 4, 
        flexDirection: 'row' 
    },
    headerText: {
        color: "black",  
        fontSize:18,
    },
    addressText: {
		fontSize: 14,
		opacity: 0.6,
		marginStart: 4,
	},
    headerIcon: {
        flex: 1, 
        alignItems: 'flex-end'
    },
 
	addressCardView: {
        borderRadius: 5,
        marginTop: 10,
        margin: 15,
        padding: 15,
		elevation: 4,
		alignContent: 'center',
		backgroundColor:"white",
    },
    addressTextInput: {
		borderBottomWidth: 1,
		borderBottomColor: '#D6D6D6',
		height: 35
	},
    bodyMessageView: {
        marginTop: 2,
        paddingHorizontal: 15
    },
    bodyText: {
        fontSize: 16,
        textAlign:'justify',

    },
    buttonView: {
        backgroundColor: '#3692E5',
        width: '80%',
        borderRadius: 50,
        elevation: 6,
        shadowColor: 'rgba(0, 0, 0, 0.1)',
        shadowOpacity: 0.8,
        height: 20,
        justifyContent: 'center',
        alignItems: 'center',
        bottom: 80
      },
    applyButtonStyle: {
        backgroundColor: "blue",
        borderRadius: 25, 
        width: '75%', 
        padding:10,
        // height: 42,
        alignItems: 'center', 
        justifyContent: 'center',
        elevation: 4
    },
    bodyButtonView: {
        marginTop: 10, 
        marginBottom:10, 
        alignItems: 'center', 
        justifyContent: 'center',
        // backgroundColor:'green'
    },
    applyTextStyle: {
        color: "#00000099",
        textTransform: 'uppercase',
    },
})

//--------------------------------------------------- store related fuction  -- Start

const mapStateToProps = (state) => {
	return {
        basketItem: state.basketItem
	};
}

const mapDispatchToProps = (dispatch) => {
    return {
        basketAction: bindActionCreators(basketAction,dispatch),
    };
}

// ---------------------------------------------------- store related function  -- END

export default connect(mapStateToProps, mapDispatchToProps)(BasketModal);
