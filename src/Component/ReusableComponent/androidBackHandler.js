import { BackHandler } from 'react-native';

const disableAndroidBackButton = callback => {
    console.warn("in disable");
    //BackHandler.addEventListener('hardwareBackPress',);
    BackHandler.addEventListener('hardwareBackPress', () => {
        return true;
    })
}

const enableAndroidBackButton = () => {
    BackHandler.removeEventListener('hardwareBackPress');
    console.warn("in enable");
    // BackHandler.removeEventListener('hardwareBackPress',true);
}


export { disableAndroidBackButton, enableAndroidBackButton };