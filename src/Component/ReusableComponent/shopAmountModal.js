import React, { Component } from 'react';
import { SafeAreaView, View, Text, Modal, TouchableOpacity, StyleSheet, Dimensions,FlatList, TextInput, KeyboardAvoidingView } from 'react-native';
import  s  from '../../sharedStyle.js';
import { getCategoriesByShop } from '../../Constant/Config.js';
import axios from 'axios';
import * as Config from '../../Constant/Config.js';
import * as Constant from '../../Constant/Constant.js';
import { connect } from 'react-redux';
import * as couponAction  from '../../Actions/CouponAction.js';
import { bindActionCreators } from 'redux';

class ShopAmountModal extends Component {
    constructor(props){
        super(props);
        this.state = {
            amount: 0.00
        }
    }

    render() {
        const {navigate} = this.props.navigation;
        return (
            // <SafeAreaView style={{flex: 1}}>
                <Modal
                    transparent={true}
                    animationType={'fade'}
                    visible = {this.props.visible}
                    onRequestClose={this.props.onDismiss}
                    animated={true}>
                    <View style={styles.rootView}>
                        <KeyboardAvoidingView style={styles.modalView} behavior="padding">
                            <View style={{backgroundColor:"#00CCFF",padding: 15}}>
                                <Text style={{fontSize: 20,fontWeight: "bold",color:"white"}}>Out standing pay</Text>
                            </View>
                            <View style={ styles.addressCardView }>
                                <Text style={ styles.addressText }>Enter amount you want to pay</Text>
                                <TextInput style={ styles.addressTextInput } placeholder="Enter Amount..." placeholderTextColor="#A9A9A9"
                                    value={this.props.balance}
                                    onChangeText={(balance) => {
                                        this.setState({
                                            amount: balance
                                        })}}
                                />
                            </View>

                            <View style={{width: '100%', flexDirection:"row", marginTop: 15, elevation: 6}}>
                                <TouchableOpacity style={{width: '50%',backgroundColor:"#F5F5F5",paddingLeft:15,alignItems:"center"}}
                                    onPress={this.props.onDismiss}>
                                    <Text style={[s.headerText,{ paddingVertical:10 }]}>Cancel</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={{width: '50%',backgroundColor:"#00CCFF",alignItems:"center"}}
                                    onPress={() => this.props.initiatePay(this.state.amount)}>
                                    <Text style={[s.headerText,{paddingVertical:10,color:"white"}]}>Apply</Text>
                                </TouchableOpacity>
                            </View>
                        </KeyboardAvoidingView>
                    </View>
                </Modal>
            // </SafeAreaView>
            
        )
    }
}

const styles = StyleSheet.create({
    rootView: {
        backgroundColor: '#00000099',
        flex: 1,
    },
    modalView: {
        backgroundColor:"#F5F5F5",
        bottom: 0,
        width: '100%',
        position: "absolute",
        elevation: 4
    },
    sChildHeaderView: {
        paddingHorizontal: 15,
        paddingVertical: 10,
        flexDirection: 'row',
        flexWrap: 'wrap',
    },
    headerTextView: {
        flex: 4,
        flexDirection: 'row'
    },
    headerText: {
        color: "black",
        fontSize:18,
    },
    addressText: {
		fontSize: 14,
		opacity: 0.6,
		marginStart: 4,
	},
    headerIcon: {
        flex: 1,
        alignItems: 'flex-end'
    },

	addressCardView: {
        borderRadius: 5,
        marginTop: 15,
        elevation: 4,
		alignContent: 'center',
        marginHorizontal: 15 ,
        marginVertical: 10,
        padding: 15,
		backgroundColor:"white",
    },
    addressTextInput: {
		borderBottomWidth: 1,
		borderBottomColor: '#D6D6D6',
		height: 35
	},
    bodyMessageView: {
        marginTop: 2,
        paddingHorizontal: 15
    },
    bodyText: {
        fontSize: 16,
        textAlign:'justify',

    },
    buttonView: {
        backgroundColor: '#3692E5',
        width: '80%',
        borderRadius: 50,
        elevation: 6,
        shadowColor: 'rgba(0, 0, 0, 0.1)',
        shadowOpacity: 0.8,
        height: 20,
        justifyContent: 'center',
        alignItems: 'center',
        bottom: 80
      },
    applyButtonStyle: {
        backgroundColor: "blue",
        borderRadius: 25,
        width: '75%',
        padding:10,
        // height: 42,
        alignItems: 'center',
        justifyContent: 'center',
        elevation: 4
    },
    bodyButtonView: {
        marginTop: 10,
        marginBottom:10,
        alignItems: 'center',
        justifyContent: 'center',
        // backgroundColor:'green'
    },
    applyTextStyle: {
        color: "#00000099",
        textTransform: 'uppercase',
    },
})

//--------------------------------------------------- store related fuction  -- Start

const mapStateToProps = (state) => {
	return {
        // basketItem: state.basketItem,
        userData: state.loginData,
        availableCoupons: state.availableCoupons
	};
}

const mapDispatchToProps = (dispatch) => {
    return {
        // basketAction: bindActionCreators(basketAction,dispatch),
        couponAction: bindActionCreators(couponAction,dispatch),
    };
}

// ---------------------------------------------------- store related function  -- END

export default connect(mapStateToProps, mapDispatchToProps)(ShopAmountModal);
