import React, { Component } from 'react';
import { View, Text, Modal, TouchableOpacity, StyleSheet, Dimensions,FlatList,Image} from 'react-native';
import  s  from '../../sharedStyle.js';
import StatusBarAlert from 'react-native-statusbar-alert';
import { getCategoriesByShop } from '../../Constant/Config.js';
import axios from 'axios';
import * as Config from '../../Constant/Config.js';
import * as Constant from '../../Constant/Constant.js';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as internetAction  from '../../Actions/InternetAction';


const HEIGHT = Dimensions.get('screen').height;

class InternateModal extends Component {
    constructor(props){
        super(props);
        this.state = {
            itemBackground: "white"
        }
    }

    componentDidMount() {
        console.warn('in internet modal', this.props);
    }


    render() {
        const navigate = this.props.navigate;
        if(this.props.internetState.isInternet){
            return (
                    <StatusBarAlert
                      visible={true}
                      message= 'Internet Connection Not Available...'
                      backgroundColor= "red"
                      color="white"
                      pulse='text'
                      // statusbarHeight={20}
                      // networkActivityIndicatorVisible={true}
                      // style={{
                        // padding: 5
                      // }}
                    /> 
            )
        }
        else{
            return null;
        }
        
    }
}

const styles = StyleSheet.create({
    rootView: {
        flex: 1,
        justifyContent: "center",
        marginHorizontal: 15
    },
    modalView: {
        backgroundColor:"#F5F5F5",
        bottom: 0,
        elevation: 10,
        position: "absolute",
        height: 60,
        width: '100%',
        backgroundColor: "#0080FE",
        flexDirection: "row",
        justifyContent: "center",

    },
    sChildHeaderView: {
        paddingHorizontal: 15,
        paddingVertical: 10,
        flexDirection: 'row',
        flexWrap: 'wrap',
    },
    headerTextView: {
        flex: 4, 
        flexDirection: 'row' 
    },
    headerText: {
        color: "black",  
        fontSize:18,
    },
    headerIcon: {
        flex: 1, 
        alignItems: 'flex-end'
    },
 
    bodyMessageView: {
        marginTop: 2,
        paddingHorizontal: 15
    },
    bodyText: {
        fontSize: 16,
        textAlign:'justify',

    },
    buttonView: {
        backgroundColor: '#3692E5',
        width: '80%',
        borderRadius: 50,
        elevation: 6,
        shadowColor: 'rgba(0, 0, 0, 0.1)',
        shadowOpacity: 0.8,
        height: 20,
        justifyContent: 'center',
        alignItems: 'center',
        bottom: 80
      },
    applyButtonStyle: {
        backgroundColor: "blue",
        borderRadius: 25, 
        width: '75%', 
        padding:10,
        // height: 42,
        alignItems: 'center', 
        justifyContent: 'center',
        elevation: 4
    },
    bodyButtonView: {
        marginTop: 10, 
        marginBottom:10, 
        alignItems: 'center', 
        justifyContent: 'center',
        // backgroundColor:'green'
    },
    applyTextStyle: {
        color: "#00000099",
        textTransform: 'uppercase',
    },
})

//--------------------------------------------------- store related fuction  -- Start

const mapStateToProps = (state) => {
	return {
        internetState: state.internetState,
	};
}

const mapDispatchToProps = (dispatch) => {
    return {
        internetAction: bindActionCreators(internetAction,dispatch),
    };
}

// ---------------------------------------------------- store related function  -- END

export default connect(mapStateToProps, mapDispatchToProps)(InternateModal);
