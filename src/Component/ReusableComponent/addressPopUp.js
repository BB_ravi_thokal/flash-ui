import React, { Component } from 'react';
import { SafeAreaView, View, Text, Modal, TouchableOpacity, StyleSheet, Dimensions,FlatList, ScrollView} from 'react-native';
import  s  from '../../sharedStyle.js';
import axios from 'axios';
import * as Config from '../../Constant/Config.js';
import * as Constant from '../../Constant/Constant.js';
import { connect } from 'react-redux';
import * as loginSuccess  from '../../Actions/LoginAction';
import { bindActionCreators } from 'redux';
import Toast from 'react-native-simple-toast';

const HEIGHT = Dimensions.get('screen').height;

function AddressListPopUp({ item, selectItem, editAddress, userData, navigation }) {
    if(userData.addressDetails.length == 1){
        return(
            <View>
                <TouchableOpacity style={{ padding:10 , marginLeft:5,marginRight:10,backgroundColor: item.backGroundColor}} onPress={() => selectItem.selectItem(item.addressId)}>    
                    <View style={{flex:1,flexDirection:"row"}}>
                        <View style={{flex:1}}>
                            <Text style={{fontSize: 14,fontWeight:"bold",color:"#131E3A"}}>Address </Text>   
                        </View>
                        <View style={{flex:1,alignItems:"flex-end"}}>
                            <TouchableOpacity onPress={() => selectItem.editAddress(item.addressId)} >
                                {/* <Image source={require('./android/assets/edit.png')}/> onPress={() => this.props.navigation('addressPage',{"addressId": item.addressId, 'callingPage': 'orderDetailsPage'})}    */}
                                <Text style={{color:"#00CCFF",fontSize:14}}>Edit</Text>
                            </TouchableOpacity>
                        </View>          
                    </View>
                    <View>
                        <Text style={s.normalText}>{item.flatNumber}</Text> 
                        <Text style={s.normalText}>{item.addressLine2}</Text> 
                        <Text style={s.normalText}>{item.addressLine1}</Text>
                    </View>  
                </TouchableOpacity>
                <View style={{alignItems:"flex-end",padding:10}}>
                    <TouchableOpacity onPress={() => selectItem.editAddress('')}>
                        <Text style={[s.normalText,{color:"#00CCFF"}]}> + Add New Address</Text>
                    </TouchableOpacity>
                </View>

            </View>
        )

    }
    else{
        return(
            
            <View>
                <TouchableOpacity style={{ padding:10 , marginLeft:5,marginRight:10,backgroundColor: item.backGroundColor}} onPress={() => selectItem.selectItem(item.addressId)}>    
                    <View style={{flex:1,flexDirection:"row"}}>
                        <View style={{flex:1}}>
                            <Text style={{fontSize: 14,fontWeight:"bold",color:"#131E3A"}}>Address </Text>   
                        </View>
                        <View style={{flex:1,alignItems:"flex-end"}}> 
                            <TouchableOpacity onPress={() => selectItem.editAddress(item.addressId)}>
                                {/* <Image source={require('./android/assets/edit.png')}/>    */}
                                <Text style={{color:"#0080FE",fontSize:14}}>Edit</Text>
                            </TouchableOpacity>
                        </View>          
                    </View>
                    <View>
                        <Text style={s.normalText}>{item.flatnumber}</Text> 
                        <Text style={s.normalText}>{item.addressLine2}</Text>
                        <Text style={s.normalText}>{item.addressLine1}</Text>
                    </View>  
                </TouchableOpacity>
                
                
            </View>
        )
        
    }
        
}

class AddressModal extends Component {
    constructor(props){
        super(props);
        this.state = {
            itemBackground: "white",
        }
    }

    componentDidMount(){
        this.props.userData.userData.addressDetails.forEach(element => {
            element.backGroundColor = "white"
        });
        this.props.userData.userData.addressDetails.forEach(element => {
            if(element.isSelected == true)
                this.setState({
                    itemBackground: "grey"
                })
          });
    }

    selectItem = (addressId) => {
        console.warn("in select Item",this.props.userData.userData.addressDetails)
        // const i = this.props.userData.userData.addressDetails.findIndex(ind => ind.addressId === this.props.userData.userData.addressDetails.addressId);
        this.props.userData.userData.addressDetails.forEach(element => {
            if(element.addressId == addressId){
                element.isSelected = true
                element.backGroundColor = "#DCF0FA"
            }
            else{
                element.isSelected = false
                element.backGroundColor = "white"
            }
        });

        console.warn("in select Item after change",this.props.userData.userData);
        this.props.loginActions.loginSuccess(this.props.userData.userData);

    }

    editAddress = (addressId) => {
        console.warn("in edit address", addressId);
        this.props.editAddress(addressId);
        
    }

    

     
//   componentWillMount(){             
//     const userId = "U15796901450423894000";
//     console.warn("cat id---",userId);

//     axios.post(
// 			Config.URL + Config.getUserDetails,
// 			{
// 				"userId": userId,
// 			},
// 			{
// 				headers: {
// 						'Content-Type': 'application/json',
// 				}
// 			}
//     )
//     .then(res => {
//         res = res.map(item => {
//           item.iseSelected = false;
//           item.isSelectedClass = styles.list;
//           return item;
//         });
//         console.warn("res---",res.data.result); 
//         console.warn("---------------- length",res.data.result[0].addressDetails.length);  
//         if (res.data.status == 0) {  
//           this.setState({
//             data: res.data.result[0], 
//             address:res.data.result[0].addressDetails
//           });
          
//           if(res.data.result[0].addressDetails.length === 0){
//             this.setState({
//               addAddress: true
//             })
//           }
//         }
//         else if(res.data.status == 1){  
//           Toast.show("Sucesssssssssssss",Toast.LONG);  
//         }
//       }).catch(err => {
//           //console.warn(res.data.status);
//       })
//   }

    MyAddress = () => {
        const {navigate} = this.props.navigation;
        if(this.props.userData.userData.addressDetails.length == 0){
          return(
            <View style={{backgroundColor:"white"}} >
              <View style={{ padding:10 , marginLeft:5}}>
                <Text style={s.headerText}>My Address</Text>
              </View>
              <View style={{display:"flex",alignItems:"center"}}>
                <TouchableOpacity style={[styles.storeBtn, styles.addtoCartBtnColor,{display:"flex",alignItems:"center"}]} 
                    onPress={() => this.props.navigation('addressPage',{"addressId": ""})}>
                  <Text style={{ color:'#0080FE',fontSize: 16,fontWeight:"bold",justifyContent:"center"}}> ADD ADDRESS</Text> 
                </TouchableOpacity>
              </View>
            </View>
          )
        }
        else{
          return(
            <View style={{backgroundColor:"white"}}>
                <View style={{ padding:10 , marginLeft:5}}>
                <Text style={s.headerText}>My Address</Text>            
                </View>
                <FlatList data={this.props.userData.userData.addressDetails}   
                    renderItem={( { item } ) => (
                    <AddressListPopUp
                        item={item}
                        selectItem={this}
                        editAddress={this.props}
                        userData={this.props.userData.userData}
                        navigation={navigate}
                    />
                    )}
                    keyExtractor= {item => item.addressId}
                    horizontal={false} 
                    extraData= {this.props}
                />
                <View style={{width: '100%', flexDirection:"row"}}>
                    <TouchableOpacity style={{width: '50%',backgroundColor:"#F5F5F5",paddingLeft:15,alignItems:"center"}}
                        onPress={this.props.onDismiss}>
                        <Text style={[s.headerText,{paddingVertical:10}]}>Cancel</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{width: '50%',backgroundColor:"#00CCFF",alignItems:"center"}}
                        onPress={this.props.changeAddress}>
                        <Text style={[s.headerText,{paddingVertical:10,color:"white"}]}>Continue</Text>
                    </TouchableOpacity>
                </View>
            </View>
            
          )
        }
    }

    render() {
        const {navigate} = this.props.navigation;
        return (
            // <SafeAreaView style={{flex: 1}}>
                <Modal
                    transparent={true}
                    animationType={'fade'}
                    visible = {this.props.visible}
                    onRequestClose={this.props.onDismiss}
                    animated={true}>
                    <View style={styles.rootView}>
                        <View style={styles.modalView}>
                            <View style={{backgroundColor:"#00CCFF",padding: 15}}>
                                <Text style={{fontSize: 20,fontWeight: "bold",color:"white"}}>Select the Address</Text>
                            </View>
                            <this.MyAddress />
                        </View>
                    </View>
                </Modal>
            // </SafeAreaView>
            
        )
    }
}

const styles = StyleSheet.create({
    rootView: {
        backgroundColor: '#00000099',
        flex: 1
    },
    modalView: {
        backgroundColor:"#F5F5F5",
        bottom: 0,
        position: "absolute"
    },
    sChildHeaderView: {
        paddingHorizontal: 15,
        paddingVertical: 10,
        flexDirection: 'row',
        flexWrap: 'wrap',
    },
    headerTextView: {
        flex: 4, 
        flexDirection: 'row' 
    },
    headerText: {
        color: "black",  
        fontSize:18,
    },
    headerIcon: {
        flex: 1, 
        alignItems: 'flex-end'
    },
 
    bodyMessageView: {
        marginTop: 2,
        paddingHorizontal: 15
    },
    bodyText: {
        fontSize: 16,
        textAlign:'justify',

    },
    buttonView: {
        backgroundColor: '#3692E5',
        width: '80%',
        borderRadius: 50,
        elevation: 6,
        shadowColor: 'rgba(0, 0, 0, 0.1)',
        shadowOpacity: 0.8,
        height: 20,
        justifyContent: 'center',
        alignItems: 'center',
        bottom: 80
      },
    applyButtonStyle: {
        backgroundColor: "blue",
        borderRadius: 25, 
        width: '75%', 
        padding:10,
        // height: 42,
        alignItems: 'center', 
        justifyContent: 'center',
        elevation: 4
    },
    bodyButtonView: {
        marginTop: 10, 
        marginBottom:10, 
        alignItems: 'center', 
        justifyContent: 'center',
        // backgroundColor:'green'
    },
    applyTextStyle: {
        color: "#00000099",
        textTransform: 'uppercase',
    },
})

//--------------------------------------------------- store related fuction  -- Start

const mapStateToProps = (state) => {
	return {
    userData: state.loginData
	};
}

const mapDispatchToProps = (dispatch) => {
    return {
      loginActions: bindActionCreators(loginSuccess,dispatch)
    };
}

// ---------------------------------------------------- store related function  -- END

export default connect(mapStateToProps, mapDispatchToProps)(AddressModal);
