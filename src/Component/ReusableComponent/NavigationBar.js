/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Fragment } from 'react';
import axios from 'axios';
import * as Config from '../../Constant/Config.js';
import * as Constant from '../../Constant/Constant.js';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as navigateAction  from '../../Actions/NavigationBarAction';
// import 'bootstrap/dist/css/bootstrap.min.css';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  TouchableOpacity,
  ToastAndroid,
  FlatList,
	Image,
	Dimensions,
  ImageBackground
} from 'react-native';


class NavigationBar extends React.Component{

	constructor(props) {
    super(props);
    
  }

  activeTab = (tab) => {
    switch(tab) {
      case 'Home':
          this.props.navigationAction.navigateAction('Home');
          this.props.navigate('homePage');
          break;
      case 'Search':
        this.props.navigationAction.navigateAction('Search');
        this.props.navigate('productSearch');
        break;    
    case 'Basket':
      this.props.navigationAction.navigateAction('Basket');
        this.props.navigate('basketListPage');
        break;    
    case 'Profile':
      this.props.navigationAction.navigateAction('Profile');
        this.props.navigate('profilePage');
        break;    
    }
  }

  HomeIcon = () => {
    const navigate = this.props.navigate;
    if (this.props.selectedIcon.selectedIcon == 'Home') {
      return (
        <View style={styles.iconView}>
          <Image style={styles.iconImageView} source={require('../../../assets/bottom-home-dark.png')}/>
        </View>
      )
    } else {
      return (
        <TouchableOpacity style={styles.iconView} onPress={() => this.activeTab('Home')}>
          <Image style={styles.iconImageView} source={require('../../../assets/bottom-home.png')}/>
        </TouchableOpacity>
      )
    }   
  }

  SearchIcon = () => {
    const navigate = this.props.navigate;
    if (this.props.selectedIcon.selectedIcon == 'Search') {
      return (
        <View style={styles.iconView}>
          <Image style={styles.iconImageView} source={require('../../../assets/bottom-search-dark.png')}/>
        </View>
      )
    } else {
      return (
        <TouchableOpacity style={styles.iconView} onPress={() => this.activeTab('Search')}>
          <Image style={styles.iconImageView} source={require('../../../assets/bottom-search.png')}/>
        </TouchableOpacity>
      )
    }   
  }

  BasketIcon = () => {
    const navigate = this.props.navigate;
    if (this.props.selectedIcon.selectedIcon == 'Basket') {
      return (
        <View style={styles.iconView}>
          <Image style={styles.iconImageView} source={require('../../../assets/bottom-basket-dark.png')}/>
        </View>
      )
    } else {
      return (
        <TouchableOpacity style={styles.iconView} onPress={() => this.activeTab('Basket')}>
          <Image style={styles.iconImageView} source={require('../../../assets/bottom-basket.png')}/>
        </TouchableOpacity>
      )
    }   
  }

  ProfileIcon = () => {
    const navigate = this.props.navigate;
    if (this.props.selectedIcon.selectedIcon == 'Profile') {
      return (
        <View style={styles.iconView}>
          <Image style={styles.iconImageView} source={require('../../../assets/bottom-profile-dark.png')}/>
        </View>
      )
    } else {
      return (
        <TouchableOpacity style={styles.iconView} onPress={() => this.activeTab('Profile')}>
          <Image style={styles.iconImageView} source={require('../../../assets/bottom-profile.png')}/>
        </TouchableOpacity>
      )
    }   
  }
  
	render() {
		const navigate = this.props.navigate;
		return (
			<View style={styles.categoryFooter}>
        <this.HomeIcon />
        <this.SearchIcon />
        <this.BasketIcon />
        <this.ProfileIcon />
				{/* <TouchableOpacity style={styles.containerIcons} onPress={() => navigate('productSearch')}>
						<Image style={styles.iconView} source={require('../../../assets/search-active.png')}/>
				</TouchableOpacity> */}
				{/* <TouchableOpacity style={[styles.containerIcons]} onPress={() => navigate('orderDetailsPage')}>
          <ImageBackground style={[styles.iconView,{alignItems:"flex-end",paddingLeft: 30}]} source={require('../../../assets/carticon.png')}>
            <View style={styles.orderCardButton}>
              <Text style={{fontSize:10,color:"white"}}>1</Text>
            </View>
          </ImageBackground>

        </TouchableOpacity> */}
        {/* <TouchableOpacity style={styles.containerIcons} onPress={() => navigate('basketListPage')}>
						<Image style={styles.iconView} source={require('../../../assets/cart-active.png')}/>
				</TouchableOpacity>  
				<TouchableOpacity style={styles.containerIcons} onPress={() => navigate('profilePage')}>
						<Image style={styles.iconView} source={require('../../../assets/profile-active.png')}/>
				</TouchableOpacity>   */}
			</View>
		);
	}
}


let ScreenWidth = Dimensions.get("window").width;
let ScreenHeight = Dimensions.get("window").height;

const styles = StyleSheet.create({
  
  iconView: {
    width: 35,
    height: 35,
    zIndex: 999
  },
  iconImageView: {
    flex: 1,
    width: undefined, 
    height: undefined, 
    resizeMode:"contain"
  },
  orderCardButton:{
    backgroundColor: "blue",
    borderWidth: 1,
    width:15,
    height:15,
    borderRadius:55,
    alignItems:"center",
    justifyContent:'center',
  },
	categoryFooter:{
    width:ScreenWidth,
    //paddingBottom:20,
    position: "relative",
    bottom: 0,
    height: 45,
    //flexGrow:0.05,
    backgroundColor:'white',
    //display:'flex',
    flexDirection:'row',
    justifyContent:"space-evenly",
    alignItems:"center",
    zIndex: 9999999,
    elevation: 2,
    borderTopWidth: 0.3,
    borderTopColor: 'black',
    shadowColor: "#000000",
  },
});


//--------------------------------------------------- store related fuction  -- Start

const mapStateToProps = (state) => {
	return {
        selectedIcon: state.selectedIcon,
	};
}

const mapDispatchToProps = (dispatch) => {
    return {
        navigationAction: bindActionCreators(navigateAction,dispatch),
    };
}

// ---------------------------------------------------- store related function  -- END

export default connect(mapStateToProps, mapDispatchToProps)(NavigationBar);
