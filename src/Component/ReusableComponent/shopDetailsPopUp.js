import React, { Component } from 'react';
import { SafeAreaView, View, Text, Image, Modal, TouchableOpacity, StyleSheet, Dimensions, Linking} from 'react-native';
import  s  from '../../sharedStyle.js';
import Toast from 'react-native-simple-toast';

const HEIGHT = Dimensions.get('screen').height;

class ShopDetailsModal extends Component {
    constructor(props){
        super(props);
    }

    render() {
        const {navigate} = this.props.navigation;
        return (
            // <SafeAreaView style={{flex: 1}}>
                <Modal
                    transparent={true}
                    animationType={'fade'}
                    visible = {this.props.visible}
                    onRequestClose={this.props.onDismiss}
                    animated={true}>
                    <View style={styles.rootView}>
                        <View style={styles.modalView}>
                            <View style={{backgroundColor:"#00CCFF",padding: 15}}>
                                <Text style={{fontSize: 20,fontWeight: "bold",color:"white"}}>Shop details</Text>
                            </View>
                            <View style={{padding: 15}}>
                                <View style={{marginVertical: 5}}>
                                    <Text style={s.normalText} numberOfLines={3}>Name: {this.props.shopData.shopName}</Text>
                                </View>
                                <TouchableOpacity style={{flex: 1, flexDirection: "row", marginVertical: 5}} onPress={() => this.navigateToCall(this.props.shopData.contactNumber)} >
                                    <Image style={{width: 20, height: 20}} source={require('../../../assets/call.png')}/>
                                    <Text style={[s.normalText, {paddingLeft: 15, color:"#00CCFF"}]}>{this.props.shopData.shopContactNumber}</Text>  
                                </TouchableOpacity>
                                <View style={{marginVertical: 5}}>
                                    <Text style={s.normalText} numberOfLines={10}>Address: {this.props.shopData.shopAddress}</Text>
                                </View>
                            </View>
                            <TouchableOpacity style={[styles.removeButton,{backgroundColor:"#00CCFF",alignItems:"center"}]}
                                onPress={this.props.onDismiss}>
                                <Text style={[s.headerText,{paddingVertical:10,color:"white"}]}>Close</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </Modal>
            // </SafeAreaView>
            
        )
    }

    navigateToCall = (phoneNumber) => {
        this.props.onDismiss();
        Linking.canOpenURL(`tel:${phoneNumber}`)
        .then(supported => {
          if (!supported) {
            Toast.show('Sorry..We not able to redirect you on call log.',Toast.LONG);
          } else {
            return Linking.openURL(`tel:${phoneNumber}`)
          }
        })
    }
}

const styles = StyleSheet.create({
    rootView: {
        backgroundColor: '#00000099',
        flex: 1
    },
    modalView: {
        backgroundColor:"#F5F5F5",
        width: '100%',
        bottom: 0,
        position: "absolute"
    },
    sChildHeaderView: {
        paddingHorizontal: 15,
        paddingVertical: 10,
        flexDirection: 'row',
        flexWrap: 'wrap',
    },
    headerTextView: {
        flex: 4, 
        flexDirection: 'row' 
    },
    headerText: {
        color: "black",  
        fontSize:18,
    },
    headerIcon: {
        flex: 1, 
        alignItems: 'flex-end'
    },
 
    bodyMessageView: {
        marginTop: 2,
        paddingHorizontal: 15
    },
    bodyText: {
        fontSize: 16,
        textAlign:'justify',

    },
    buttonView: {
        backgroundColor: '#3692E5',
        width: '80%',
        borderRadius: 50,
        elevation: 6,
        shadowColor: 'rgba(0, 0, 0, 0.1)',
        shadowOpacity: 0.8,
        height: 20,
        justifyContent: 'center',
        alignItems: 'center',
        bottom: 80
      },
    applyButtonStyle: {
        backgroundColor: "blue",
        borderRadius: 25, 
        width: '75%', 
        padding:10,
        // height: 42,
        alignItems: 'center', 
        justifyContent: 'center',
        elevation: 4
    },
    bodyButtonView: {
        marginTop: 10, 
        marginBottom:10, 
        alignItems: 'center', 
        justifyContent: 'center',
        // backgroundColor:'green'
    },
    applyTextStyle: {
        color: "#00000099",
        textTransform: 'uppercase',
    },
})


export default ShopDetailsModal;
