import React, { Component } from 'react';
import { SafeAreaView, View, Text, Modal, TouchableOpacity, StyleSheet, Dimensions,FlatList, TextInput, KeyboardAvoidingView } from 'react-native';
import  s  from '../../sharedStyle.js';
import { getCategoriesByShop } from '../../Constant/Config.js';
import axios from 'axios';
import * as Config from '../../Constant/Config.js';
import * as Constant from '../../Constant/Constant.js';
import { connect } from 'react-redux';
import * as couponAction  from '../../Actions/CouponAction';
import { bindActionCreators } from 'redux';

CouponList = ({item, self}) => {
    return (
        <View>
            <View style={{backgroundColor:"white",paddingVertical: 15,paddingHorizontal:15,marginHorizontal: 15,elevation: 6}}>
                <View style={{flex: 1, flexDirection: "row"}} >
                    <TouchableOpacity style={{flex: 7,alignItems:"flex-start",justifyContent:"center",alignContent:"center"}}
                        onPress={() => self.applyCoupon(item.coupon)}>
                        <Text style={s.headerText}>{item.coupon} </Text>
                    </TouchableOpacity>
                    <View style={{flex: 5}}>
                        <TouchableOpacity style={{flex: 1, alignItems:"flex-end", paddingRight: 20}} onPress={() => self.showDetails(item.offerTermsCondition, item.offerDescription, item.coupon)}>
                            <Text style={[s.normalText,{color:"#00CCFF"}]}>See details</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        </View>

    )

}

class CouponModal extends Component {
    constructor(props){
        super(props);
        this.state = {
            couponCode: "",
            isDetails: false,
            termsnCondition: "",
            description: ""
            // couponList: [],
            // isCouponAvailable: true
        }
    }

    componentDidMount(){
        
    }

    applyCoupon = (code) => {
        this.setState({
            couponCode: code
        })
    }

    AvailableCoupon = () => {
        if (this.props.availableCoupons.couponList.length > 0 && !this.state.isDetails) {
            return (
                <View style={{marginBottom: 10}}>
                    <FlatList
                        data={this.props.availableCoupons.couponList}
                        renderItem={( { item } ) => (
                            <CouponList
                                item= {item}
                                self= {this} 
                            />

                        )}
                    />
                </View>
            )

        } else if(this.state.isDetails) {
            return null;
        } else {
            return (
                <View style={{backgroundColor:"white",paddingVertical: 15,paddingHorizontal:15,marginHorizontal: 15,elevation: 4}}>
                    <Text style={s.sunHeadertext}>Coupons not available</Text>
                </View>
            )
        }
    }

    showDetails = (tnc, description, code) => {

        this.setState({
            isDetails: true,
            description: description,
            termsnCondition: tnc,
            couponCode: code
        })
    }

    showCouponList = () => {
        this.setState({
            isDetails: false,
            description: "",
            termsnCondition: ""
        })
    }

    CouponDetails= () => {
        if(this.state.isDetails) {
            return (
            <View style={{backgroundColor:"white",paddingVertical: 15,paddingHorizontal:15,marginHorizontal: 15,elevation: 4}}>
                <View style={{flex: 1, flexDirection:"row"}}>
                    <View style={{flex: 10}}>
                        <Text style={s.headerText}>Offer description</Text>
                        <Text style={[s.normalText, {marginTop: 5}]}>{this.state.description}</Text>
                    </View>
                    <TouchableOpacity onPress={() => this.showCouponList()} style={{flex: 2}}>
                        <Text style={[s.normalText, {color: "#00CCFF", marginRight: 15}]}>Back</Text>
                    </TouchableOpacity>
                </View>
                <View style={{marginTop: 10}}>
                    <Text style={s.headerText}>Terms and condition</Text>
                    <Text style={[s.normalText,{marginTop: 5}]}>{this.state.termsnCondition}</Text>
                </View>
            </View>
            )
        } else {
            return null;
        }
        
    }


    render() {
        const {navigate} = this.props.navigation;
        return (
            <SafeAreaView>
                <Modal
                    transparent={true}
                    animationType={'fade'}
                    visible = {this.props.visible}
                    onRequestClose={this.props.onDismiss}
                    animated={true}>
                    <View style={styles.rootView}>
                    
                        <KeyboardAvoidingView style={styles.modalView} behavior="padding">
                            <View style={{backgroundColor:"#00CCFF",padding: 15}} >
                                <Text style={{fontSize: 20,fontWeight: "bold",color:"white"}}>Apply coupon</Text>
                            </View>
                            <View style={ styles.addressCardView } >
                                <Text style={ styles.addressText }>Coupon Code</Text>
                                <TextInput style={ styles.addressTextInput } placeholder="Enter Coupon code..." placeholderTextColor="#A9A9A9"
                                    value={this.state.couponCode}
                                    onChangeText={(code) => {
                                        this.setState({
                                        couponCode: code
                                    })}}
                                />
                            </View>
                            <this.AvailableCoupon />
                            <this.CouponDetails />

                            <View style={{width: '100%', flexDirection:"row", marginTop: 15, elevation: 6}}>
                                <TouchableOpacity style={{width: '50%',backgroundColor:"#F5F5F5",paddingLeft:15,alignItems:"center"}}
                                    onPress={this.props.onDismiss}>
                                    <Text style={[s.headerText,{ paddingVertical:10 }]}>Cancel</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={{width: '50%',backgroundColor:"#00CCFF",alignItems:"center"}}
                                    onPress={() => this.props.applyCoupon(this.state.couponCode)}>
                                    <Text style={[s.headerText,{paddingVertical:10,color:"white"}]}>Apply</Text>
                                </TouchableOpacity>
                            </View>
                        </KeyboardAvoidingView>
                    </View>
                </Modal>
            </SafeAreaView>
            
        )
    }
}

const styles = StyleSheet.create({
    rootView: {
        backgroundColor: '#00000099',
        flex: 1,
    },
    modalView: {
        backgroundColor:"#F5F5F5",
        bottom: 0,
        width: '100%',
        position: "absolute",
        elevation: 4
    },
    sChildHeaderView: {
        paddingHorizontal: 15,
        paddingVertical: 10,
        flexDirection: 'row',
        flexWrap: 'wrap',
    },
    headerTextView: {
        flex: 4,
        flexDirection: 'row'
    },
    headerText: {
        color: "black",
        fontSize:18,
    },
    addressText: {
		fontSize: 14,
		opacity: 0.6,
		marginStart: 4,
	},
    headerIcon: {
        flex: 1,
        alignItems: 'flex-end'
    },

	addressCardView: {
        borderRadius: 5,
        marginTop: 15,
        elevation: 4,
		alignContent: 'center',
        marginHorizontal: 15 ,
        marginVertical: 10,
        padding: 15,
		backgroundColor:"white",
    },
    addressTextInput: {
		borderBottomWidth: 1,
		borderBottomColor: '#D6D6D6',
		height: 35
	},
    bodyMessageView: {
        marginTop: 2,
        paddingHorizontal: 15
    },
    bodyText: {
        fontSize: 16,
        textAlign:'justify',

    },
    buttonView: {
        backgroundColor: '#3692E5',
        width: '80%',
        borderRadius: 50,
        elevation: 6,
        shadowColor: 'rgba(0, 0, 0, 0.1)',
        shadowOpacity: 0.8,
        height: 20,
        justifyContent: 'center',
        alignItems: 'center',
        bottom: 80
      },
    applyButtonStyle: {
        backgroundColor: "blue",
        borderRadius: 25,
        width: '75%',
        padding:10,
        // height: 42,
        alignItems: 'center',
        justifyContent: 'center',
        elevation: 4
    },
    bodyButtonView: {
        marginTop: 10,
        marginBottom:10,
        alignItems: 'center',
        justifyContent: 'center',
        // backgroundColor:'green'
    },
    applyTextStyle: {
        color: "#00000099",
        textTransform: 'uppercase',
    },
})

//--------------------------------------------------- store related fuction  -- Start

const mapStateToProps = (state) => {
	return {
        // basketItem: state.basketItem,
        userData: state.loginData,
        availableCoupons: state.availableCoupons
	};
}

const mapDispatchToProps = (dispatch) => {
    return {
        // basketAction: bindActionCreators(basketAction,dispatch),
        couponAction: bindActionCreators(couponAction,dispatch),
    };
}

// ---------------------------------------------------- store related function  -- END

export default connect(mapStateToProps, mapDispatchToProps)(CouponModal);
