/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Fragment } from 'react';
import axios from 'axios';
import * as Config from '../../Constant/Config.js';
import * as Constant from '../../Constant/Constant.js';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as navigateAction  from '../../Actions/NavigationBarAction';
// import 'bootstrap/dist/css/bootstrap.min.css';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  TouchableOpacity,
  ToastAndroid,
  FlatList,
	Image,
	Dimensions,
  ImageBackground
} from 'react-native';


class DriverNavigationBar extends React.Component{

	constructor(props) {
    super(props);
    
  }

  componentDidMount() {
    console.warn('in driver navigation--', this.props);
  }

  activeTab = (tab) => {
    switch(tab) {
      case 'Order':
        console.warn('Order triggered');
          this.props.navigationAction.driverNavigateAction('Order');
          this.props.navigate('driverOrderListing');
          break;
      case 'Activity':
        this.props.navigationAction.driverNavigateAction('Activity');
        this.props.navigate('driverActivityListing');
        break;    
    case 'Ongoing':
      this.props.navigationAction.driverNavigateAction('Ongoing');
        this.props.navigate('driverOngoingOrder');
        break;    
    case 'Delivered':
      this.props.navigationAction.driverNavigateAction('Delivered');
        this.props.navigate('driverProfilePage');
        break;    
    }
  }

  OrderIcon = () => {
    const navigate = this.props.navigate;
    if (this.props.selectedIcon.driverSelectedIcon == 'Order') {
      return (
        <View style={styles.iconView}>
          <Image style={styles.iconImageView} source={require('../../../assets/bottom-home-dark-shop.png')}/>
        </View>
      )
    } else {
      return (
        <TouchableOpacity style={styles.iconView} onPress={() => this.activeTab('Order')}>
          <Image style={styles.iconImageView} source={require('../../../assets/bottom-home-shop.png')}/>
        </TouchableOpacity>
      )
    }   
  }

  ActivityIcon = () => {
    const navigate = this.props.navigate;
    if (this.props.selectedIcon.driverSelectedIcon == 'Activity') {
      return (
        <View style={styles.iconView}>
          <Image style={styles.iconImageView} source={require('../../../assets/bottom-pending-dark.png')}/>
        </View>
      )
    } else {
      return (
        <TouchableOpacity style={styles.iconView} onPress={() => this.activeTab('Activity')}>
          <Image style={styles.iconImageView} source={require('../../../assets/bottom-pending.png')}/>
        </TouchableOpacity>
      )
    }   
  }

  OngoingIcon = () => {
    const navigate = this.props.navigate;
    if (this.props.selectedIcon.driverSelectedIcon == 'Ongoing') {
      return (
        <View style={styles.iconView}>
          <Image style={styles.iconImageView} source={require('../../../assets/bottom-ongoing-dark.png')}/>
        </View>
      )
    } else {
      return (
        <TouchableOpacity style={styles.iconView} onPress={() => this.activeTab('Ongoing')}>
          <Image style={styles.iconImageView} source={require('../../../assets/bottom-ongoing.png')}/>
        </TouchableOpacity>
      )
    }   
  }

  DeliveredIcon = () => {
    const navigate = this.props.navigate;
    if (this.props.selectedIcon.driverSelectedIcon == 'Delivered') {
      return (
        <View style={styles.iconView}>
          <Image style={styles.iconImageView} source={require('../../../assets/bottom-profile-dark-shop.png')}/>
        </View>
      )
    } else {
      return (
        <TouchableOpacity style={styles.iconView} onPress={() => this.activeTab('Delivered')}>
          <Image style={styles.iconImageView} source={require('../../../assets/bottom-profile-shop.png')}/>
        </TouchableOpacity>
      )
    }   
  }
  
	render() {
		const navigate = this.props.navigate;
		return (
			<View style={styles.categoryFooter}>
        <this.OrderIcon />
        <this.ActivityIcon />
        <this.OngoingIcon />
        <this.DeliveredIcon />
			</View>
		);
	}
}


let ScreenWidth = Dimensions.get("window").width;
let ScreenHeight = Dimensions.get("window").height;

const styles = StyleSheet.create({
  iconView: {
    width: 35,
    height: 35,
    zIndex: 999
  },
  iconImageView: {
    flex: 1,
    width: undefined, 
    height: undefined, 
    resizeMode:"contain"
  },
  containerIcons:{
    width:55,
    height:55,
  },
	iconViewSolid: {
    width: 55,
    height: 55,
    zIndex: 999
  },
  iconViewSolidH: {
    width: 55,
    height: 55,
    zIndex: 999
  },
  orderCardButton:{
    backgroundColor: "blue",
    borderWidth: 1,
    width:15,
    height:15,
    borderRadius:50,
    alignItems:"center",
    justifyContent:'center',
  },
	categoryFooter:{
    width:ScreenWidth,
    //paddingBottom:20,
    position: "relative",
    bottom: 0,
    height: 45,
    //flexGrow:0.05,
    backgroundColor:'#332b5c',
    //display:'flex',
    flexDirection:'row',
    justifyContent:"space-evenly",
    alignItems:"center",
    zIndex: 9999999,
    elevation: 4
  },
});


//--------------------------------------------------- store related fuction  -- Start

const mapStateToProps = (state) => {
	return {
        selectedIcon: state.selectedIcon,
	};
}

const mapDispatchToProps = (dispatch) => {
    return {
        navigationAction: bindActionCreators(navigateAction,dispatch),
    };
}

// ---------------------------------------------------- store related function  -- END

export default connect(mapStateToProps, mapDispatchToProps)(DriverNavigationBar);
