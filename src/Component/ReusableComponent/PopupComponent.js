import React, { Component } from 'react';
import { SafeAreaView, View, Text, Modal, TouchableOpacity, StyleSheet, Dimensions,FlatList} from 'react-native';
import  s  from '../../sharedStyle.js';
import { connect } from 'react-redux';
import * as loginSuccess  from '../../Actions/LoginAction';
import { bindActionCreators } from 'redux';




const HEIGHT = Dimensions.get('screen').height;

class PopupModal extends Component {
    constructor(props){
        super(props);
    }

    AddressListPopUP = (item) => {
        return(
            <View style={{ padding:10}}>
              <View style={{flexDirection:"row"}}>
                <View>
                  <Text style={{fontSize: 14,fontWeight:"bold",color:"#131E3A"}}>Address</Text>
                </View>
              </View>
              <View>
                <Text style={s.normalText}>{item.item.flatNumber}</Text>
                <Text style={s.normalText}>{item.item.addressLine1}</Text>
                <Text style={s.normalText}>{item.item.addressLine2}</Text>
              </View>
            </View>
          )
    }


    render() {
        if(this.props.type == "removeAddress"){
            return (
                <SafeAreaView>
                    <Modal
                        transparent={true}
                        animationType={'fade'}
                        visible = {this.props.visible}
                        onRequestClose={this.props.onDismiss}
                        animated={true}>
                            
                        <View style={styles.rootView}>
                            <View style={styles.modalView}>
                                <View style={{backgroundColor:"#00CCFF",padding: 15}}>
                                    <Text style={{fontSize: 16,fontWeight: "bold"}}>{this.props.message}</Text>
                                </View>
                                <FlatList data={this.props.data}
                                    renderItem={( { item } ) => (
                                    <this.AddressListPopUP
                                        item = {item}
                                    // navigation={this.props.navigation}
                                    />
                                    )}
                                    keyExtractor= {item => item.addressId}
                                    horizontal={false}
                                    extraData = {this.props.userData.userData.addressDetails}
                                />
                                <View style={[styles.buttons,{flexDirection:"row"}]}>
                                    <TouchableOpacity style={[styles.cancelButton,{backgroundColor:"white",paddingLeft:15,alignItems:"center"}]}
                                        onPress={this.props.onDismiss}>
                                        <Text style={[s.headerText,{paddingVertical:10}]}>Cancel</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={[styles.removeButton,{backgroundColor:"#00CCFF",alignItems:"center"}]}
                                        onPress={this.props.removeAddress}>
                                        <Text style={[s.headerText,{paddingVertical:10,color:"white"}]}>Remove</Text>
                                    </TouchableOpacity>
                                </View>

                            </View>
                        </View>

                    </Modal>
                </SafeAreaView>
                
            )

        }
        else if (this.props.type == 'displayMessage') {
            return (
                <SafeAreaView>
                    <Modal  
                        transparent={true}
                        animationType={'fade'}
                        visible = {this.props.visible}
                        onRequestClose={this.props.onDismiss}
                        animated={true}>
                        <View style={styles.rootView}>
                            <View style={styles.modalView}>
                                <View style={{backgroundColor:"#F5F5F5",padding: 15}}>
                                    <Text style={s.subHeaderText}>{this.props.title}</Text>
                                    <Text style={[s.normalText, {marginTop: 10}]}>{this.props.message}</Text>
                                </View>

                    
                                <View style={[styles.buttons,{flexDirection:"row"}]}>
                                    <TouchableOpacity style={[{width: '100%', backgroundColor:"#00CCFF",paddingLeft:15,alignItems:"center"}]}
                                        onPress={this.props.onDismiss}>
                                        <Text style={[s.headerText,{paddingVertical:10}]}>{this.props.btnTag}</Text>
                                    </TouchableOpacity>
                                </View>

                            </View>
                        </View>

                    </Modal>
                </SafeAreaView>
                
            )

        } else {
            return (
                <SafeAreaView>
                    <Modal  
                        transparent={true}
                        animationType={'fade'}
                        visible = {this.props.visible}
                        onRequestClose={this.props.onDismiss}
                        animated={true}>
                        <View style={styles.rootView}>
                            <View style={styles.modalView}>
                                <View style={{backgroundColor:"#F5F5F5",padding: 15}}>
                                    <Text style={{fontSize: 20,fontWeight: "bold"}}>{this.props.message}</Text>
                                </View>

                    
                                <View style={[styles.buttons,{flexDirection:"row"}]}>
                                    <TouchableOpacity style={[styles.cancelButton,{backgroundColor:"white",paddingLeft:15,alignItems:"center"}]}
                                        onPress={this.props.onDismiss}>
                                        <Text style={[s.headerText,{paddingVertical:10}]}>Cancel</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={[styles.removeButton,{backgroundColor:"#00CCFF",alignItems:"center"}]}
                                        onPress={this.props.removeProduct}>
                                            <Text style={[s.headerText,{paddingVertical:10,color:"white"}]}>{this.props.btnTag}</Text>
                                    </TouchableOpacity>
                                </View>

                            </View>
                        </View>

                    </Modal>
                </SafeAreaView>
                
            )

        }
    }
}

const styles = StyleSheet.create({
    modalView: {
        backgroundColor:"#F5F5F5",
        bottom: 0,
        position: "absolute",
        width: '100%'
        //height: 250,
    },
    rootView: {
        backgroundColor: '#00000099',
        flex: 1
    },

    sChildHeaderView: {
        paddingHorizontal: 15,
        paddingVertical: 10,
        flexDirection: 'row',
        flexWrap: 'wrap',
        // borderBottomWidth: 1,
        // borderBottomColor: "grey"
    },
    buttons: {
        width: '100%'
    },
    cancelButton:{
        width: '50%'
    },
    removeButton:{
        width: '50%'
    },
    headerTextView: {
        flex: 4,
        flexDirection: 'row'
    },
    headerText: {
        color: "black",
        fontSize:18,
    },
    headerIcon: {
        flex: 1,
        alignItems: 'flex-end'
    },

    bodyMessageView: {
        marginTop: 2,
        paddingHorizontal: 15
    },
    bodyText: {
        fontSize: 16,
        textAlign:'justify',

    },
    buttonView: {
        backgroundColor: '#3692E5',
        width: '80%',
        borderRadius: 50,
        elevation: 6,
        shadowColor: 'rgba(0, 0, 0, 0.1)',
        shadowOpacity: 0.8,
        height: 20,
        justifyContent: 'center',
        alignItems: 'center',
        bottom: 80
      },
    applyButtonStyle: {
        backgroundColor: "blue",
        borderRadius: 25,
        width: '75%',
        padding:10,
        // height: 42,
        alignItems: 'center',
        justifyContent: 'center',
        elevation: 4
    },
    bodyButtonView: {
        marginTop: 10,
        marginBottom:10,
        alignItems: 'center',
        justifyContent: 'center',
        // backgroundColor:'green'
    },
    applyTextStyle: {
        color: "#00000099",
        textTransform: 'uppercase',
    },
})

//--------------------------------------------------- store related fuction  -- Start

const mapStateToProps = (state) => {
	return {
    userData: state.loginData
	};
}

const mapDispatchToProps = (dispatch) => {
    return {
      loginActions: bindActionCreators(loginSuccess,dispatch)
    };
}

// ---------------------------------------------------- store related function  -- END

export default connect(mapStateToProps, mapDispatchToProps)(PopupModal);
