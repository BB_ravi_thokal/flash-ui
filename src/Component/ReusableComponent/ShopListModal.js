import React, { Component } from 'react';
import { SafeAreaView, View, Text, Modal, TouchableOpacity, StyleSheet, Dimensions,FlatList, ScrollView} from 'react-native';
import  s  from '../../sharedStyle.js';
import axios from 'axios';
import * as Config from '../../Constant/Config.js';
import * as Constant from '../../Constant/Constant.js';
import { connect } from 'react-redux';
import * as loginSuccess  from '../../Actions/LoginAction';
import { bindActionCreators } from 'redux';
import Toast from 'react-native-simple-toast';

const HEIGHT = Dimensions.get('screen').height;

function ShopListPopUp({ item, self, index }) {
   
    return(
        <TouchableOpacity style={{backgroundColor: item.backGroundColor, marginHorizontal: 15, marginVertical: 5, elevation: 4}}
        onPress={() => self.selectShopToAssign(item.shopId, item.shopName)}>
            <View style={{padding: 10}}>
                <Text style={[s.normalText]}> {item.shopName}</Text>
                <Text style={[s.normalText]}> {item.shopAdress}</Text>
            </View>
        </TouchableOpacity>
        
    )
        
}

class ShopListModal extends Component {
    constructor(props){
        super(props);
        this.state = {
            itemBackground: "white",
            selectedShopId: '',
            selectedShopName: ''
        }
    }

    componentDidMount(){
        
    }

    selectShopToAssign = (shopId, shopName) => {
        console.warn("in select Item-----", shopId)
        // const i = this.props.userData.userData.addressDetails.findIndex(ind => ind.addressId === this.props.userData.userData.addressDetails.addressId);
        this.props.shopList.forEach(element => {
            if(element.shopId == shopId){
                console.warn('match shop Id', element);
                element.isSelected = true
                element.backGroundColor = "#DCF0FA"
                this.setState({selectedShopId: shopId, selectedShopName: shopName})
            }
            else{
                element.isSelected = false
                element.backGroundColor = "white"
            }
        });

        // console.warn("in select Item after change",this.props.userData.userData);
        // this.props.loginActions.loginSuccess(this.props.userData.userData);

    }


    AvailableShops = () => {
        return(
            <View >
                <FlatList data={this.props.shopList}   
                    renderItem={( { item, index } ) => (
                    <ShopListPopUp
                        item={item}
                        self={this}
                        index={index}
                    />
                    )}
                    keyExtractor= {item => item.shopId}
                    horizontal={false} 
                    extraData= {this.state}
                />
                <View style={{width: '100%', flexDirection:"row"}}>
                    <TouchableOpacity style={{width: '50%',backgroundColor:"#F5F5F5",paddingLeft:15,alignItems:"center"}}
                        onPress={this.props.onDismiss}>
                        <Text style={[s.headerText,{paddingVertical:10}]}>Cancel</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{width: '50%',backgroundColor:"#00CCFF",alignItems:"center"}}
                        onPress={this.props.assignShopToProduct}>
                        <Text style={[s.headerText,{paddingVertical:10,color:"white"}]}>Continue</Text>
                    </TouchableOpacity>
                </View>
            </View>    
            
        )
    }

    render() {
        const {navigate} = this.props.navigation;
        return (
            // <SafeAreaView style={{flex: 1}}>
                <Modal
                    transparent={true}
                    animationType={'fade'}
                    visible = {this.props.visible}
                    onRequestClose={this.props.onDismiss}
                    animated={true}>
                    <View style={styles.rootView}>
                        <View style={styles.modalView}>
                            <View style={{backgroundColor:"#00CCFF",padding: 15, maxHeight: ScreenHeight/2}}>
                                <Text style={{fontSize: 16,fontWeight: "bold",color:"white"}}>Available in following shops</Text>
                            </View>
                            <this.AvailableShops />
                        </View>
                    </View>
                </Modal>
            // </SafeAreaView>
            
        )
    }
}

let ScreenHeight = Dimensions.get("window").height;
let ScreenWidth = Dimensions.get("window").width;
const styles = StyleSheet.create({
    rootView: {
        backgroundColor: '#00000099',
        flex: 1,
        
    },
    modalView: {
        backgroundColor:"#F5F5F5",
        bottom: 0,
        position: "absolute",
        
    },
    sChildHeaderView: {
        paddingHorizontal: 15,
        paddingVertical: 10,
        flexDirection: 'row',
        flexWrap: 'wrap',
    },
    headerTextView: {
        flex: 4, 
        flexDirection: 'row' 
    },
    headerText: {
        color: "black",  
        fontSize:18,
    },
    headerIcon: {
        flex: 1, 
        alignItems: 'flex-end'
    },
 
    bodyMessageView: {
        marginTop: 2,
        paddingHorizontal: 15
    },
    bodyText: {
        fontSize: 16,
        textAlign:'justify',

    },
    buttonView: {
        backgroundColor: '#3692E5',
        width: '80%',
        borderRadius: 50,
        elevation: 6,
        shadowColor: 'rgba(0, 0, 0, 0.1)',
        shadowOpacity: 0.8,
        height: 20,
        justifyContent: 'center',
        alignItems: 'center',
        bottom: 80
      },
    applyButtonStyle: {
        backgroundColor: "blue",
        borderRadius: 25, 
        width: '75%', 
        padding:10,
        // height: 42,
        alignItems: 'center', 
        justifyContent: 'center',
        elevation: 4
    },
    bodyButtonView: {
        marginTop: 10, 
        marginBottom:10, 
        alignItems: 'center', 
        justifyContent: 'center',
        // backgroundColor:'green'
    },
    applyTextStyle: {
        color: "#00000099",
        textTransform: 'uppercase',
    },
})

//--------------------------------------------------- store related fuction  -- Start

const mapStateToProps = (state) => {
	return {
    userData: state.loginData
	};
}

const mapDispatchToProps = (dispatch) => {
    return {
      loginActions: bindActionCreators(loginSuccess,dispatch)
    };
}

// ---------------------------------------------------- store related function  -- END

export default connect(mapStateToProps, mapDispatchToProps)(ShopListModal);
