import React, { Component } from 'react';
import { SafeAreaView, View, Text, Modal, TouchableOpacity, StyleSheet, Dimensions,FlatList,Image} from 'react-native';
import  s  from '../../sharedStyle.js';
import { getCategoriesByShop } from '../../Constant/Config.js';
import axios from 'axios';
import * as Config from '../../Constant/Config.js';
import * as Constant from '../../Constant/Constant.js';
import { connect } from 'react-redux';
import * as loginSuccess  from '../../Actions/LoginAction';
import { bindActionCreators } from 'redux';
import * as cartModalAction  from '../../Actions/CartModalAction';
import * as addToCart  from '../../Actions/CartItem';


const HEIGHT = Dimensions.get('screen').height;

class OnGoingActivityModal extends Component {
    constructor(props){
        super(props);
        this.state = {
            itemBackground: "white",
            couponList: []
        }
    }

    // CartModal = () => {
    //     const navigate = this.props.navigate;
    //     if(this.props.cartState.cartState) {
    //         return (
    //             <View style={styles.cartModalView}>
    //             <View style={{flex: 2,justifyContent:"center",alignItems:"center", paddingLeft:5}}>
    //                 <Image style={styles.iconView} source={require('../../../assets/carticon.png')}/>
    //             </View>
    //             <View style={{flex: 10,justifyContent: "center",flexDirection:"row",alignItems:"center",alignContent:"center"}}>
    //                 <View style={{flex:6, alignItems:"center"}}>
    //                     <Text style={{fontSize: 14,fontWeight: "bold", color:"white",paddingBottom: 5,paddingLeft: 10}}>{this.props.cartState.cartCount} Item</Text>
    //                     <Text style={{fontSize: 18,fontWeight: "bold", color:"white"}}> ₹{(this.props.cartState.cartSum).toFixed(2)}</Text>
    //                 </View>
    //                 <TouchableOpacity style={{flex:6,justifyContent:"center",alignItems:"flex-end",paddingRight: 15}} onPress={() => navigate('orderDetailsPage')}>
    //                     <Text style={{fontSize: 14,fontWeight: "bold", color:"white"}}> Proceed to cart </Text>
    //                 </TouchableOpacity>
    //             </View>
    //         </View>
    //         )
    //     } else {
    //         return null;
    //     }
    // }

    // this.props.cartState.bidState
    BidModal = () => {
        const navigate = this.props.navigate;
        if(this.props.cartState.bidState) {
            return (
                <View style={styles.bidModalView}> 
                    <TouchableOpacity onPress={() => navigate('userBidDetailsPage')}>
                        <Image style={{width: 60, height: 60}} source={require('../../../assets/placedbid.png')}/>
                    </TouchableOpacity>
                </View>
            )
        } else {
            return null;
        }
    }
    // this.props.cartState.orderState
    OrderModal = () => {
        const navigate = this.props.navigate;
        if(this.props.cartState.orderState) {
            return (
                <View style={{alignItems: "flex-end", flex: 1}}>
                    <View style={styles.orderModalView}>
                        <TouchableOpacity onPress={() => navigate('trackingListPage', {action: 'order', orderId: ''})}>
                            <Image style={{width: 60, height: 60}} source={require('../../../assets/tracking.png')}/>
                        </TouchableOpacity>
                    </View>
                </View>
                
            )
        } else {
            return null;
        }
    }

    SendActivityModal = () => {
        const navigate = this.props.navigate;
        if(this.props.cartState.sendActivityState) {
            return (
                <View style={{alignItems: "flex-end", flex: 1}}>
                    <View style={styles.sendActModalView}>
                        <TouchableOpacity onPress={() => navigate('trackingListPage', {action: 'sendActivity', orderId: ''})}>
                            <Image style={{width: 60, height: 60}} source={require('../../../assets/deliveryPopup.png')}/>
                        </TouchableOpacity>
                    </View>
                </View>
                
            )
        } else {
            return null;
        }
    }

    GetActivityModal = () => {
        const navigate = this.props.navigate;
        if(this.props.cartState.getActivityState) {
            return (
                <View style={{alignItems: "flex-start", flex: 1}}>
                    <View style={[styles.getActModalView]}>
                        <TouchableOpacity onPress={() => navigate('trackingListPage', {action: 'getActivity', orderId: ''})}>
                            <Image style={{width: 60, height: 60}} source={require('../../../assets/deliveryPopup.png')}/>
                        </TouchableOpacity>
                    </View>
                </View>
                
            )
        } else {
            return null;
        }
    }

    render() {
        const navigate = this.props.navigate;

        return (
            // <SafeAreaView style={{flex: 1}}>
                <View style={styles.rootView}>
                    {/* <this.CartModal /> */}
                    <this.BidModal />
                    <this.OrderModal />
                    <this.SendActivityModal />
                    <this.GetActivityModal />
                </View>
            // </SafeAreaView> 
        )
    }    
}

const styles = StyleSheet.create({
    rootView: {
        flex: 1,
        justifyContent: "center",
        marginHorizontal: 15
    },
    iconView: {
        height: 30,
        width: 30
    },
    cartModalView: {
        bottom: 0,
        elevation: 10,
        position: "absolute",
        height: 60,
        width: '100%',
        backgroundColor: "#588BAE",
        flexDirection: "row",
        justifyContent: "center",

    },
    bidModalView: {
        bottom: 90,
        elevation: 10,     
        position: "absolute",
        height: 80,
        width: 80,
        justifyContent: "center",
        alignContent: 'center',
        alignItems: "center",
        backgroundColor: "#f1f1f1", 
        borderRadius: 100

    },
    orderModalView: {
        bottom: 90,
        padding: 10,
        elevation: 10,
        position: "absolute",
        height: 80,
        justifyContent: "flex-end",
        alignItems: "flex-end",
        backgroundColor: "#f1f1f1", 
        borderRadius: 100
    },
    sendActModalView: {
        bottom: 180,
        padding: 10,
        elevation: 10,
        position: "absolute",
        height: 80,
        justifyContent: "flex-start",
        alignItems: "flex-start",
        backgroundColor: "#f1f1f1", 
        borderRadius: 100
    },
    getActModalView: {
        bottom: 180,
        padding: 10,
        elevation: 10,
        position: "absolute",
        height: 80,
        justifyContent: "flex-end",
        alignItems: "flex-end",
        backgroundColor: "#f1f1f1", 
        borderRadius: 100
    },
    sChildHeaderView: {
        paddingHorizontal: 15,
        paddingVertical: 10,
        flexDirection: 'row',
        flexWrap: 'wrap',
    },
    headerTextView: {
        flex: 4, 
        flexDirection: 'row' 
    },
    headerText: {
        color: "black",  
        fontSize:18,
    },
    headerIcon: {
        flex: 1, 
        alignItems: 'flex-end'
    },
 
    bodyMessageView: {
        marginTop: 2,
        paddingHorizontal: 15
    },
    bodyText: {
        fontSize: 16,
        textAlign:'justify',

    },
    buttonView: {
        backgroundColor: '#3692E5',
        width: '80%',
        borderRadius: 50,
        elevation: 6,
        shadowColor: 'rgba(0, 0, 0, 0.1)',
        shadowOpacity: 0.8,
        height: 20,
        justifyContent: 'center',
        alignItems: 'center',
        bottom: 80
      },
    applyButtonStyle: {
        backgroundColor: "blue",
        borderRadius: 25, 
        width: '75%', 
        padding:10,
        // height: 42,
        alignItems: 'center', 
        justifyContent: 'center',
        elevation: 4
    },
    bodyButtonView: {
        marginTop: 10, 
        marginBottom:10, 
        alignItems: 'center', 
        justifyContent: 'center',
        // backgroundColor:'green'
    },
    applyTextStyle: {
        color: "#00000099",
        textTransform: 'uppercase',
    },
})

//--------------------------------------------------- store related fuction  -- Start

const mapStateToProps = (state) => {
	return {
        userData: state.loginData,
        cartState: state.cartState,
        cartItem: state.cartItem,
	};
}

const mapDispatchToProps = (dispatch) => {
    return {
      loginActions: bindActionCreators(loginSuccess,dispatch),
      cartModalAction: bindActionCreators(cartModalAction,dispatch),
      cartActions: bindActionCreators(addToCart, dispatch),
    };
}

// ---------------------------------------------------- store related function  -- END

export default connect(mapStateToProps, mapDispatchToProps)(OnGoingActivityModal);
