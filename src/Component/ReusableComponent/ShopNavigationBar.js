/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Fragment } from 'react';
import axios from 'axios';
import * as Config from '../../Constant/Config.js';
import * as Constant from '../../Constant/Constant.js';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as navigateAction  from '../../Actions/NavigationBarAction';
// import 'bootstrap/dist/css/bootstrap.min.css';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  TouchableOpacity,
  ToastAndroid,
  FlatList,
	Image,
	Dimensions,
  ImageBackground
} from 'react-native';


class ShopNavigationBar extends React.Component{

	constructor(props) {
    super(props);
    
  }

  componentDidMount() {
    console.warn('in driver navigation--', this.props);
  }

  activeTab = (tab) => {
    switch(tab) {
      case 'ShopHome':
          this.props.navigationAction.shopNavigateAction('ShopHome');
          this.props.navigate('shopHomePage');
          break;
      case 'ShopOngoing':
          this.props.navigationAction.shopNavigateAction('ShopOngoing');
          this.props.navigate('activeOrderShop');
          break;
      case 'Bid':
          this.props.navigationAction.shopNavigateAction('Bid');
          this.props.navigate('bidListPage');
          break;
      case 'Profile':
        this.props.navigationAction.shopNavigateAction('Profile');
        this.props.navigate('shopProfilePage');
        break;    
      case 'FlashMartForShop':
        this.props.navigationAction.shopNavigateAction('FlashMartForShop');
        this.props.navigate('wholeSalerDetails');
        break;    
    }
  }

  ShopHomeIcon = () => {
    const navigate = this.props.navigate;
    if (this.props.selectedIcon.shopSelectedIcon == 'ShopHome') {
      return (
        <View style={styles.iconView}>
          <Image style={styles.iconImageView} source={require('../../../assets/bottom-home-dark-shop.png')}/>
        </View>
      )
    } else {
      return (
        <TouchableOpacity style={styles.iconView} onPress={() => this.activeTab('ShopHome')}>
          <Image style={styles.iconImageView} source={require('../../../assets/bottom-home-shop.png')}/>
        </TouchableOpacity>
      )
    }   
  }

  ShopOngoingIcon = () => {
    const navigate = this.props.navigate;
    if (this.props.selectedIcon.shopSelectedIcon == 'ShopOngoing') {
      return (
        <View style={styles.iconView}>
          <Image style={styles.iconImageView} source={require('../../../assets/bottom-ongoing-dark.png')}/>
        </View>
      )
    } else {
      return (
        <TouchableOpacity style={styles.iconView} onPress={() => this.activeTab('ShopOngoing')}>
          <Image style={styles.iconImageView} source={require('../../../assets/bottom-ongoing.png')}/>
        </TouchableOpacity>
      )
    }   
  }

  BidIcon = () => {
    const navigate = this.props.navigate;
    if (this.props.selectedIcon.shopSelectedIcon == 'Bid') {
      return (
        <View style={styles.iconView}>
          <Image style={styles.iconImageView} source={require('../../../assets/bottom-home-dark.png')}/>
        </View>
      )
    } else {
      return (
        <TouchableOpacity style={styles.iconView} onPress={() => this.activeTab('Bid')}>
          <Image style={styles.iconImageView} source={require('../../../assets/bottom-home.png')}/>
        </TouchableOpacity>
      )
    }   
  }

  ProfileIcon = () => {
    const navigate = this.props.navigate;
    if (this.props.selectedIcon.shopSelectedIcon == 'Profile') {
      return (
        <View style={styles.iconView}>
          <Image style={styles.iconImageView} source={require('../../../assets/bottom-profile-dark-shop.png')}/>
        </View>
      )
    } else {
      return (
        <TouchableOpacity style={styles.iconView} onPress={() => this.activeTab('Profile')}>
          <Image style={styles.iconImageView} source={require('../../../assets/bottom-profile-shop.png')}/>
        </TouchableOpacity>
      )
    }   
  }

  WholeSalerIcon = () => {
    const navigate = this.props.navigate;
    if (this.props.selectedIcon.shopSelectedIcon == 'FlashMartForShop') {
      return (
        <View style={styles.iconView}>
        {/* <Text style={{color: '#00CCFF', fontSize: 10, fontFamily: 'Verdana', width: 30, paddingTop: 10}} numberOfLines={2}>Flash Mart</Text> */}
          <Image style={styles.iconImageView} source={require('../../../assets/shopDark.png')}/>
          {/* <Image style={styles.iconImageView} source={{uri: 'https://flash-catagory-image.s3.ap-south-1.amazonaws.com/icon-flash-mart-dark.png'}}/> */}
        </View>
      )
    } else {
      return (
        <TouchableOpacity style={styles.iconView} onPress={() => this.activeTab('FlashMartForShop')}>
          {/* <Text style={{color: 'white', fontSize: 10, fontFamily: 'Verdana', width: 30, paddingTop: 10}} numberOfLines={2}>Flash Mart</Text> */}
          <Image style={styles.iconImageView} source={require('../../../assets/shopWhite.png')}/>
          {/* <Image style={styles.iconImageView} source={{uri: 'https://flash-catagory-image.s3.ap-south-1.amazonaws.com/icon-flash-mart.png'}}/> */}
        </TouchableOpacity>
      )
    }   
  }

  
	render() {
		const navigate = this.props.navigate;
		return (
			<View style={styles.categoryFooter}>
        <this.ShopHomeIcon /> 
        {this.props.userData.userData.shopDetails[0].isShop == true && this.props.userData.userData.shopDetails[0].isWholeSaler != true
          ?
          <this.WholeSalerIcon />
          :
          null
        }
        <this.ShopOngoingIcon />
        {/* <this.BidIcon /> */}
        <this.ProfileIcon />
        
			</View>
		);
	}
}


let ScreenWidth = Dimensions.get("window").width;
let ScreenHeight = Dimensions.get("window").height;

const styles = StyleSheet.create({
  iconView: {
    width: 35,
    height: 35,
    zIndex: 999
  },
  iconImageView: {
    flex: 1,
    width: undefined, 
    height: undefined, 
    resizeMode:"contain"
  },
  iconViewOnGoing: {
    width: 55,
    height: 55,
    zIndex: 999
  },
  orderCardButton:{
    backgroundColor: "blue",
    borderWidth: 1,
    width:15,
    height:15,
    borderRadius:50,
    alignItems:"center",
    justifyContent:'center',
  },
	categoryFooter:{
    width:ScreenWidth,
    //paddingBottom:20,
    position: "relative",
    bottom: 0,
    height: 45,
    //flexGrow:0.05,
    backgroundColor:'#332b5c',
    //display:'flex',
    flexDirection:'row',
    justifyContent:"space-evenly",
    alignItems:"center",
    zIndex: 9999999,
    elevation: 4,
  },
});


//--------------------------------------------------- store related fuction  -- Start

const mapStateToProps = (state) => {
	return {
        selectedIcon: state.selectedIcon,
        userData: state.loginData,
	};
}

const mapDispatchToProps = (dispatch) => {
    return {
        navigationAction: bindActionCreators(navigateAction,dispatch),
    };
}

// ---------------------------------------------------- store related function  -- END

export default connect(mapStateToProps, mapDispatchToProps)(ShopNavigationBar);
