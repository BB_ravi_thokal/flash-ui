import React, { Component } from 'react';
import { SafeAreaView, View, Text, Modal, TouchableOpacity, StyleSheet, Linking} from 'react-native';
import { connect } from 'react-redux';
import  s  from '../../sharedStyle.js';


class VersionUpdateModal extends Component {
    constructor(props){
        super(props);
        this.state = {
            itemBackground: "white",
        }
    }

    update = () => {
        Linking.openURL("https://play.google.com/store/apps/details?id=com.flashLocalDeliveryApp").catch((err) =>
        Toast.show('Not able to open privacy policy. Please try after sometime',Toast.LONG));
    
      }

    render() {
        return (
            <Modal
                transparent={true}
                animationType={'fade'}
                visible = {this.props.visible}
                animated={true}>
                <View style={styles.rootView}>
                    <View style={styles.modalView}>
                        <View style={{backgroundColor:"#00CCFF", width: '100%', padding: 10}}>
                            <Text style={{fontSize: 20,fontWeight: "bold",color:"white"}}>New Update</Text>
                        </View>
                        <View style={{padding: 20}}>
                            <Text style={[s.normalText]}>We have release our new version with more features. To get benifits and new offers update the app</Text>
                        </View>
                        <View style={{flex: 1}}>
                            <TouchableOpacity style={[{backgroundColor:"#00CCFF",alignItems:"center"}]} onPress={() => this.update()}>
                                    <Text style={[s.headerText,{paddingVertical:10,color:"white"}]}>Update</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </Modal>
            
        )
    }
}

const styles = StyleSheet.create({
    rootView: {
        backgroundColor: '#00000099',
        flex: 1
    },
    modalView: {
        backgroundColor:"#F5F5F5",
        bottom: 0,
        position: "absolute"
    },
   
})

export default VersionUpdateModal;
