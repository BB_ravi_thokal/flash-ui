/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import * as addToCart  from '../../Actions/CartItem';
import { bindActionCreators } from 'redux';
import axios from 'axios';
import * as Config from '../../Constant/Config.js';
import * as Constant from '../../Constant/Constant.js';
import  s  from '../../sharedStyle.js'
import * as cartModalAction  from '../../Actions/CartModalAction';
import InternateModal from '../ReusableComponent/InternetModal';
// import Paytm from 'react-native-paytm';
import * as navigateAction  from '../../Actions/NavigationBarAction';
import PopupModal from '../ReusableComponent/PopupComponent.js';
import Toast from 'react-native-simple-toast';
// import 'bootstrap/dist/css/bootstrap.min.css';
import {
    SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  Dimensions,
  TouchableOpacity,
  FlatList,
  Image,
  ToastAndroid
} from 'react-native';


class UserBidDetailsPage extends React.Component{
  interval;
  constructor(props) {
    super(props);
    this.state={
        placedBidDetails: [],
        itemList: [],
        listView: true,
        detailsView: false,
        shopDetails: [],
        totalBid: 0,
        selectedShopId: '',
        modalVisible: false,
        proceedNormal: false,
        checkMashObj: {},
        toPay: 0.00,
        bidCost: 0.00
    }
  }

  componentDidMount(){
    
    // Paytm.addListener(Paytm.Events.PAYTM_RESPONSE, this.handlePaytmResponse);
    if (this.props.navigation.state.params) {
      if (this.props.navigation.state.params.bidId) {
        this.getBidDetailsByIdInterval(this.props.navigation.state.params.bidId);
      } else {
        this.callApi();
      }
    } else {
      this.callApi();
    }
    // -- API to get shop details --------------------------------------------------------------------- Start
    
  }

  callApi = () => {
    axios.post(
      Config.URL + Config.getBidByUserId,
      {
        "userId": this.props.userData.userData.userId,
      },
      {
        headers: {
            'Content-Type': 'application/json',
        }
      }
    )
    .then(res => {
        console.warn('---- res get bids-->', res);
        if (res.data.status == 0) {
            
            if (res.data.result.length == 1) {
                this.getBidDetailsByIdInterval(res.data.result[0].bidId);
            } else {
                this.setState ({
                    placedBidDetails: res.data.result,
                    listView: true,
                    detailsView: false
                })
            }
          
        } else {
            console.warn('error')
        }
    

    })
  }

  componentWillUnmount(){
    // Paytm.removeListener(Paytm.Events.PAYTM_RESPONSE, this.handlePaytmResponse);

  }

//   handlePaytmResponse = (resp) => {
//     console.warn("paytm res---->",resp);
//     if (resp.STATUS == 'TXN_FAILURE') {
//       Toast.show('Failed to process the transaction. Please try again',Toast.LONG);
//     }  else if (resp.STATUS == 'TXN_SUCCESS'){
//       this.placedOrdered(resp);
//     } else {
//       Toast.show('Failed to process the transaction. Please try again',Toast.LONG);
//     }

//   };

  getBidDetailsByIdInterval = (bidId) => {
    this.getBidDetailsById(bidId);
    // this.interval = setInterval(async () => {
    //   this.getBidDetailsById(bidId)
    // }, 20000);
  }


  getBidDetailsById = (bidId) => {
    console.warn('bid id --->', bidId)
    axios.post(
        Config.URL + Config.getBidByBidId,
        {
          "userId": this.props.userData.userData.userId,
          "bidId": bidId
        },
        {
          headers: {
              'Content-Type': 'application/json',
          }
        }
    )
    .then(res => {
        console.warn('---- single bid ----->', res.data.result.bidDetails);
        console.warn('---- single bid item list----->', res.data.result.bidDetails[0].bidInfo.itemList);
        console.warn('---- single bid shop----->', res.data.result.shopBidDetailForUser);
        
        if (res.data.status == 0) {
            console.warn('hjbsdjchbsjdd');
            if (res.data.result.shopBidDetailForUser.length > 0) {
              res.data.result.shopBidDetailForUser = res.data.result.shopBidDetailForUser.map( item => {
                  item.backGroundColor = "white" 
                  return item;
              })
            }
            this.setState ({
                placedBidDetails: res.data.result.bidDetails,
                itemList: res.data.result.bidDetails[0].bidInfo.itemList,
                shopDetails: res.data.result.shopBidDetailForUser,
                listView: false,
                detailsView: true,
                bisCost: res.data.result.bidDetails[0].bidInfo
            })

            // console.warn('shop details----', this.state.shopDetails);
          
            console.warn('shop details----', this.state.shopDetails);
            
           
        } else {
            console.warn('error', res);
            this.setState ({
                // placedBidDetails: res.data.result,
                listView: false,
                detailsView: true
            })
        }
    

    })

  }

  PlacedBidListing = (item) => {
    return (
        <TouchableOpacity style={[styles.bidListingView,{backgroundColor: item.item.backGroundColor}]}
        onPress={() => this.selectShop(item.item.shopId, item.item.bidAmount)}>
            <View style={{flex:10}}>
                <Text style={s.headerText} numberOfLines={1}>{item.item.shopDetails.shopName}</Text>
            </View>
            
            <View style={[styles.Mystores,{marginVertical: 5}]}>
                <View style={{paddingVertical: 5}}>
                    <Text style={[s.subHeaderText]}>Comment:</Text>    
                    <Text style={[s.normalText]} numberOfLines={4}>{item.item.comment}</Text>    
                </View>
                
            </View>
            <View style={{flexDirection:"row"}}>
                <Image style={{height: 18, width: 18}} source={require('../../../assets/money.jpg')}/>  
                <Text style={[s.subHeaderText, {paddingLeft: 5}]}>Bid Amount: ${item.item.bidAmount}</Text>   
            </View>
        </TouchableOpacity>
        
    )
      
  }


  
  ActiveBidListing = (item) => {
    const {navigate} = this.props.navigation;
    return (
        <TouchableOpacity style={[styles.activeListingView]} onPress={() => this.getBidDetailsByIdInterval(item.item.bidId)}>
            <View>
                <Text style={s.headerText} numberOfLines={1}>Bid status: {item.item.bidStatus}</Text>
            </View>
            
            <View style={[styles.Mystores,{marginVertical: 10}]}>
                <View style={{paddingTop: 10, flexDirection:"row"}}>
                    <Image style={{height: 16, width: 16}} source={require('../../../assets/money.jpg')}/>  
                    <Text style={[s.normalText, {paddingLeft: 5}]}>Total cart amount: $ {item.item.bidInfo.totalCost}</Text>   
                </View>
            </View>
        </TouchableOpacity>
        
    )
  }

  CartListing = (item) => {
    // const imgPath = data.productLogo + data.productName.replace(/\s+/g, '') + '.jpg';
        return (  
            <View style={[styles.prodcutListingView]}>
                
                <View style={styles.addCardImage}>
                    <Image style={styles.productImage} source={require('../../../assets/foodIcon.jpg')}></Image>
                </View>
                <View style={styles.stores}>
                    <View style={{flex:10}}>
                        <Text style={s.headerText} numberOfLines={1}>{item.item.productName}</Text>
                    </View>
                    
                    <View style={styles.Mystores}>
                    <View style={{flexDirection: "column"}}>
                        <Text style={[s.normalText]} numberOfLines={1}>Quantity: {item.item.counter}</Text> 
                        <Text style={[s.normalText]} numberOfLines={1}>Unit cost: $ {item.item.unitCost}</Text>
                        <Text style={[s.normalText]} numberOfLines={1}>Total cost: $ {item.item.totalCost}</Text>
                    </View>
                    </View>
                </View>
            </View>
        )
    }  
  
    selectShop = (shopId, cost) => {
        const i = this.state.shopDetails.findIndex(ind => ind.shopId === shopId);
        this.state.shopDetails.forEach(element => {
            element.backGroundColor = "white"
        })

        if (i >= 0) {
          this.state.shopDetails[i].backGroundColor = '#DCF0FA';
        }
        

        this.setState({
            selectedShopId: shopId,
            proceedNormal: true,
            toPay: parseFloat(cost) + 35
        })
    }
  
    BidNotAvailable = () => {
        if(this.state.shopDetails.length == 0) {
            return (
                <View style={{alignItems:"center", marginTop: 20}}>
                    <Text style={s.subHeaderText}>No shop placed the bid against your basket.</Text>
                </View>
            )
        } else {
            return null;
        }
    }


    render() {
        const {navigate} = this.props.navigation;
        if (this.state.listView) {
            return (
                <SafeAreaView style={{flex:1}}>
                    <View style={[s.body,{flex: 1, paddingHorizontal: 15}]}>
                    {/* <InternateModal /> */}
                        <ScrollView showsVerticalScrollIndicator={false}>
                            <View style={{flex: 1,flexDirection: "row",borderBottomWidth:3,borderBottomColor:"#003151", marginVertical: 15, paddingBottom: 10}}>
                                <View style= {{flex: 8}}>
                                    <Text style={s.headerText}>Your bid details</Text>
                                </View>
                                <View style={{flex: 4, flexDirection: "row", alignItems:"flex-end"}}>
                                    <View style={{flex: 3, alignItems:"flex-end"}}>
                                        <Image source={require('../../../assets/bid.png')}/>  
                                    </View>
                                    <View style={{flex: 1, alignItems:"flex-end"}}>
                                        <Text style={s.headerText}>1</Text>
                                    </View>
                                    
                                </View>
                            </View>
                            <FlatList data={this.state.placedBidDetails}
                                renderItem={( { item } ) => (
                                    <this.ActiveBidListing
                                    item={item}
                                    />
                                )}
                                keyExtractor= {item => item.bidId}
                                extraData = {this.state.placedBidDetails}
                            />
                        </ScrollView>
                        
                    </View>
            
                </SafeAreaView>
            
        )
        } else {
            return (
                <SafeAreaView style={{flex: 1}}>
                    <View style={[s.body,{flex: 1}]}>
                    {/* <InternateModal /> */}
                        <View style={[styles.storeDtlContainer,{flex: 11}]}>
                            <ScrollView showsVerticalScrollIndicator={false}>
                                <View style={{flex: 1,flexDirection: "row",borderBottomWidth:3,borderBottomColor:"#003151", paddingBottom: 10}}>
                                    <View style= {{flex: 8}}>
                                        <Text style={s.headerText}>Your bid details</Text>
                                    </View>
                                    
                                    <View style={{flex: 4, flexDirection: "row", alignItems:"flex-end"}}>
                                        <View style={{flex: 3, alignItems:"flex-end"}}>
                                            <Image source={require('../../../assets/bid.png')}/>  
                                        </View>
                                        <View style={{flex: 1, alignItems:"flex-end"}}>
                                            <Text style={s.headerText}>{this.state.shopDetails.length}</Text>
                                        </View>
                                        
                                    </View>
                                </View>
                                <View style= {{flex: 1, paddingVertical: 10}}>
                                    <Text style={s.subHeaderText}>Bid Cost: {this.state.placedBidDetails[0].bidInfo.totalCost}</Text>
                                </View>
                                <View>
                                    <View style={{paddingTop: 10}}>
                                        <Text style={s.subHeaderText}>Product Details</Text>
                                    </View>
                                    <View>
                                    <FlatList data={this.state.itemList}
                                        renderItem={( { item } ) => (
                                            <this.CartListing
                                            item={item}
                                            />
                                        )}
                                        keyExtractor= {item => item.productId}
                                        extraData = {this.state.itemList}
                                    />
                                    </View>
                                </View>
                                <View>
                                    <View style={{paddingTop: 10}}>
                                        <Text style={s.subHeaderText}>Placed Bids</Text>
                                    </View>
                                    <View>
                                    <FlatList data={this.state.shopDetails}
                                        renderItem={( { item } ) => (
                                            <this.PlacedBidListing
                                            item={item}
                                            />
                                        )}
                                        keyExtractor= {item => item.shopId}
                                        extraData = {this.state.shopDetails}
                                    />
                                    <this.BidNotAvailable />    
                                    </View>
                                </View>
                                
                            </ScrollView>
                            
                        </View>
                        
                        <View style={{flex:1,flexDirection:"row"}}>
                        <TouchableOpacity style={{flex:6,backgroundColor:"#F5F5F5",alignItems:"center",justifyContent:"center"}}
                            onPress={() => this.navigateToHome()}>
                            <Text style={{fontSize: 16,fontWeight: "bold",color:"#131E3A"}}>Cancel</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{flex:6,backgroundColor:"#00CCFF",alignItems:"center",justifyContent:"center"}}
                            onPress={() => this.checkout()}>
                            <Text style={[s.headerText,{color:"white"}]}>Checkout</Text>
                        </TouchableOpacity>
                        </View>
                        <PopupModal visible={this.state.modalVisible} onDismiss={this.hideModal} removeProduct={this.proceedCheckout} 
                            message={"You have not selected any shop bid. Do you want to proceed with actual costing?"} btnTag={"Add to cart"}/>
                    </View>
                </SafeAreaView>
               
            )
        }
        
    }

    navigateToHome = () => {
        this.props.navigationAction.navigateAction('Home');
        this.props.navigation.navigate('homePage');
    }


    proceedCheckout = () => {
        this.setState({
            modalVisible: false,
            proceedNormal: true
        })
        if (!this.state.modalVisible) {
            this.checkout();
        }
        
    }

    hideModal = () => {
        this.setState({
            modalVisible: false
        })
    }

    checkout = () => {
            
        if (this.state.proceedNormal) {
            let cost;
          if (this.state.selectedShopId != ''){
            cost = parseFloat(this.state.bidCost) + 35
            
          }  
          axios.post( 
          Config.URL + Config.checkout,
          {
            "userId": this.props.userData.userData.userId,
            "totalAmount": cost
          },
          { 
            headers: {
                'Content-Type': 'application/json',
            }
          }
          )
          .then(res => {
                console.warn("res paytm data--",res.data.data);
                this.setState ({
                  showLoader: false
                })
                const details = {
                  mode: "Staging",
                  MID: res.data.data.MID,
                  ORDER_ID: res.data.data.ORDER_ID,
                  CUST_ID: res.data.data.CUST_ID,
                  INDUSTRY_TYPE_ID: res.data.data.INDUSTRY_TYPE_ID,
                  CHANNEL_ID: res.data.data.CHANNEL_ID,
                  TXN_AMOUNT: res.data.data.TXN_AMOUNT,
                  WEBSITE: res.data.data.WEBSITE,
                  MOBILE_NO: res.data.data.MOBILE_NO,
                  EMAIL : res.data.data.EMAIL,
                  CALLBACK_URL: res.data.data.CALLBACK_URL,
                  CHECKSUMHASH: res.data.data.CHECKSUMHASH
                };
                this.setState({
                  checkMashObj: details
                })
                try {
                //   Paytm.startPayment(details);
                } catch(err) {
                  console.warn("err---->",err);
                  Toast.show('Failed to initiate payment gateway. Please try again!!',Toast.LONG);
                }
          });
        } else {
            console.warn('qwdwe');
            this.setState({
                modalVisible: true
            })
        }  
      }
  
      placedOrdered = (paytmObj) => {
        this.setState ({
          showLoader: true
        })
        const tempObj = {
          orderId: this.state.checkMashObj.ORDER_ID,
          coupon: '',
          totalDiscount: '',
          deliveryCharge: '35.00',
          totalCost: this.state.placedBidDetails[0].bidInfo.totalCost,
          toPay: parseFloat(this.state.placedBidDetails[0].bidInfo.totalCost) + 35,
          itemList: this.state.itemList,
          deliveryAddress: this.state.placedBidDetails[0].bidInfo.deliveryAddress,
          paytmResponse: paytmObj
        }
  
        if (this.state.checkMashObj.ORDER_ID != "") {
          axios.post( 
            Config.URL + Config.placedOrder,
            {
              "orderId": this.state.checkMashObj.ORDER_ID,
              "userId": this.props.userData.userData.userId,
              "order": tempObj,
              "paymentStatus" : 'Completed',
              "bidId": this.state.placedBidDetails[0].bidId,
            },
            { 
              headers: {
                  'Content-Type': 'application/json',
              }
            }
          )
          .then(res => {
            console.warn("res paytm data-))))))))-", res);
            if(res.data.status == 0) {
              Toast.show('Your order placed successfully!!',Toast.LONG);
              this.setState({
                showLoader: false,
                trackOrder: true
              })
              this.props.cartModalAction.changeBidState(false);  
              this.props.cartModalAction.changeOrderState(true); 
              this.props.navigationAction.navigateAction('Home');
              this.props.navigation.navigate('homePage');
            } else if (res.data.status == 0) {
              Toast.show('Failed to place the order please try again!!',Toast.LONG);
              this.setState({
                showLoader: false,
              })
            } else {
              Toast.show('Failed to place the order please try again!!',Toast.LONG);
              this.setState({
                showLoader: false,
              })
            }
            
          });
        }
        
      }
  


}




let ScreenWidth = Dimensions.get("window").width;
let ScreenHeight = Dimensions.get("window").height;

const styles = StyleSheet.create({
    scrollView: {
        flex: 1
      },
      imgBlank: {
        height: ScreenHeight,
        width: ScreenWidth
      },
      containerIcons:{
        width:30,
        height:30,
      },
      inputBox: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
        padding: 10,
        borderRadius: 10,
        width: '100%',
        backgroundColor:"white",
        fontWeight: "bold"
      },
    
      searchShops:{ 
        height: 40, 
        borderColor: '#7285A5', 
        borderWidth: 1,
        paddingHorizontal:10, 
        borderRadius:4 
      },
      sectionContainer: {
        marginTop: 20,
        paddingHorizontal: 24,
      },
      iconView: {
        width: 30,
        height: 30,
        marginRight: 20,
        borderRadius: 100,
      },
      storeCardLayout:{
        marginHorizontal: 15,
        paddingVertical:14,
        borderBottomWidth: 2,
        borderBottomColor: "#131E3A",
        alignItems: "center"
      },
      categoryFooter:{
        width:ScreenWidth,
        paddingBottom:20,
        height:80,
        flexGrow:0.05,
        backgroundColor:'white',
        display:'flex',
        flexDirection:'row',
        justifyContent:"space-evenly",
        alignItems:"center"
      },
      storeDtlContainer: {
        marginHorizontal: 15,
        paddingVertical: 10
      },
    
      storeContainer:{
        display:"flex",
        flexDirection: "row"
      },
      stores: {
        paddingHorizontal: 10,
        flex: 8,
        flexDirection: "column"
      },
    
      storeCount: {
        fontSize: 22,
        fontWeight: 'bold',
        paddingHorizontal: 10,
      },
      storeProductHeading: {
        fontWeight: 'bold',
        fontSize: 30,
      },
      shopdtlCards: {
        width: 80,
        height: 80,
        backgroundColor: 'orange',
        borderRadius: 100,
        borderWidth: 0.5,
        borderColor: 'black',
        marginVertical: 5,
        marginHorizontal: 10,
      },
      storeCardDisplay: {
        // padding: 10,
        paddingVertical: 10,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
      },
      storeCards: {
        height: 100,
        borderRadius: 4,
        borderWidth: 0.5,
        borderColor: 'black',
        backgroundColor: 'white',
        flex: 3,
      },
      advertiseData: {
        height: 200,
        width: ScreenWidth,
        borderBottomLeftRadius: 90,
        borderBottomRightRadius: 90,
        elevation: 4
      },
      storeHeading: {
        fontSize:24,
        
      },
      storeDesc: {
        alignItems: "center"
      },
      trendingShops: {
        borderBottomWidth: 2,
        borderColor: "#131E3A",
        marginHorizontal: 15,
        paddingTop: 10,
      },
      addCardImage: {
        width: 80,
        height: 80,
        flex: 4
      },
      productImage: {
        width: 80,
        height: 80,
      },
      storeCardButton: {
        backgroundColor: 'white',
        color: 'black',
        width: 30,
        height: 30,
        borderRadius: 50,
        alignItems: "center",
        justifyContent: 'center',
      },
      storeborder: {
        borderBottomColor: 'grey',
        borderBottomWidth: 1,
      },
      flexRow: {
        flex: 1,
        textAlignVertical: "bottom",
        flexDirection: 'row',
      },
      prodcutListingView: {
        marginVertical: 5,
        backgroundColor: "white",
        padding: 10,
        elevation: 6,
        flex: 1,
        flexDirection: 'row'
    
      },
      activeListingView: {
        marginVertical: 5,
        backgroundColor: "white",
        padding: 10,
        elevation: 6,
        flex: 1,
    
      },
      bidListingView: {
        marginVertical: 5,
        backgroundColor: "white",
        padding: 10,
        elevation: 6,
    
      },
      flexColumnAlignEnd: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'flex-end'
      },
      storeDtlCenter: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "center"
      },
      Mystores: {
        alignItems: "flex-start",
        justifyContent: "space-between",
        textAlignVertical: "bottom",
        flex: 1
        // paddingBottom:15,
      },
      storeCardSpace: {
        marginBottom: 15,
      },
      headerstyle: {
        paddingBottom: 20,
        paddingTop: 20
      },
    
      storeBtnViewColor: {
        backgroundColor: '#ba55d3',
        color: 'white',
      },
      continueBtnViewColor: {
        backgroundColor: '#2f4f4f',
        color: 'white',
      },
      addtoCartBtnColor: {
        fontSize:12,
        borderWidth: 1,
        borderColor: '#0080FE',
        marginLeft: 20,
      },
      storeBtn: {
        paddingVertical: 5,
        paddingHorizontal: 5,
        marginBottom: 5,
        color: 'white',
        borderRadius: 0,
        width: 50
      },
      basketBtn:{
        paddingVertical: 5,
        paddingHorizontal: 10,
        marginBottom: 10,
        borderRadius: 20,
        height: 30,
        width: 30
      },
      basketImg: {
        height: 30,
        width: 30,
      }
    
});

//--------------------------------------------------- store related fuction  -- Start

const mapStateToProps = (state) => {
	return {
        cartItem: state.cartItem,
        cartState: state.cartState,
        userData: state.loginData,
	}
}

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(addToCart, dispatch),
        cartModalAction: bindActionCreators(cartModalAction,dispatch),
        navigationAction: bindActionCreators(navigateAction,dispatch),
    };
}

// ---------------------------------------------------- store related function  -- END


export default connect(mapStateToProps, mapDispatchToProps)(UserBidDetailsPage);


