/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Fragment } from 'react';

// import 'bootstrap/dist/css/bootstrap.min.css';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  Button,
  TextInput,
  StatusBar,
  TouchableWithoutFeedback,
  TouchableOpacity,
  Image,
  ToastAndroid,
  Dimensions
} from 'react-native';
import Dots from 'react-native-dots-pagination';
import InternateModal from '../ReusableComponent/InternetModal';
// import { Container, Row, Col } from 'reactstrap';




class SendPackageAd extends React.Component{

    constructor(props) {
        super(props);
    }
    render() {
        const {navigate} = this.props.navigation;
        return (
            <SafeAreaView style={{flex: 1}}>
                <View style={{flex: 1}}>
                    {/* <InternateModal /> */}
                    <View style={{flex:1,flexDirection:"row"}}>
                        
                        <View style={{flex:11.5}}>
                            <View style={{flex:1,justifyContent:"center",alignItems:"center",backgroundColor:"#dcf0fa"}}>
                            <View style={{borderRadius:100 , borderColor:"#00ccff" , borderWidth:1,width:200,height:200,justifyContent:"center",alignItems:"center",backgroundColor:"white"}}>
                                <View style={{borderRadius:75 , borderColor:"#00ccff" , borderWidth:2,width:150,height:150,justifyContent:"center",alignItems:"center",backgroundColor:"#dcf0fa"}}>
                                    <Image style={{}} source={require('../../../assets/delivery-man.png')}/>
                                </View>
                            </View>
                            </View>
                            <View style={{flex:1,backgroundColor:"#dcf0fa"}}>
                            <View style={{padding:10,marginHorizontal:10}}>
                                <Text style={{fontWeight: 'bold',fontSize: 25,color:"#131e3a"}}>
                                SEND PACKAGES ANYWHERE IN NAVI MUMBAI
                                </Text>
                            </View>
                            <View style={{padding:10,marginHorizontal:10}}>
                                <Text style={{fontSize: 18,color:"#003151",lineHeight: 30,}}>
                                    Set a pickup address and delevery address.
                                    Our delivery executive would be there to pickup your package and deliver
                                    safely to your delivery address
                                </Text>
                            </View>
                            </View>
                            <View style={{flex:0.3,flexDirection:"row",marginLeft:10,backgroundColor:"#dcf0fa"}}>
                            <View style={{flex:1,flexDirection:"column",justifyContent:"center"}}>
                                <Dots length={4} active={1} activeColor="#00ccff" passiveDotHeight={8} activeDotHeight={12} activeDotWidth={12} passiveDotWidth={8}/>
                            </View>
                            <View style={{flex:1,flexDirection:"row",justifyContent:"flex-end",alignItems:"center"}}>
                                <TouchableOpacity onPress={() => navigate('deliveryAd')}
                                    style={{height:45,width:70,backgroundColor:"#00ccff",borderTopLeftRadius:30,borderBottomLeftRadius:30,justifyContent:"center",alignItems:"center"}}>
                                    <Image style={{}} source={require('../../../assets/right.png')}/>
                                </TouchableOpacity>
                            </View>
                            </View>
                        </View>
                        <View style={{flex:0.5,backgroundColor:"#00ccff"}}></View>
                    </View>
                </View>
            
            </SafeAreaView>
            
        );
    }

}

let ScreenHeight = Dimensions.get("window").height;

const styles = StyleSheet.create({
    logoContainer:{
        flex:2,
        justifyContent: 'center',
        alignItems: 'center',

    },
        logoStyle: {
        height: 300,
        width: 200
    },
});


export default SendPackageAd;
