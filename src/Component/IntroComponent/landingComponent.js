/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import * as loginSuccess  from '../../Actions/LoginAction';
import { bindActionCreators } from 'redux';
import * as internateAction  from '../../Actions/InternetAction';
import * as navigateAction  from '../../Actions/NavigationBarAction';
// import 'bootstrap/dist/css/bootstrap.min.css';
import {
    SafeAreaView,
  StyleSheet,
  View,
  Image,
  AsyncStorage,
  BackHandler
} from 'react-native';
// import { Container, Row, Col } from 'reactstrap';
import { NavigationActions, StackActions } from 'react-navigation';



class LandingPage extends React.Component{

    constructor(props) {
        super(props);
        this.state = {
            firstLaunch: null,
            isConnected: true,
            connection_Status: ''

        }
    }
    
    async componentDidMount() {
        
        console.warn('this.props.userData.userData.token',this.props.userData);
        setTimeout(async () => {
            if (Object.keys(this.props.userData.userData).length > 0) {
                if (this.props.userData.userData.roleDetails.length > 0) {
                    console.warn('in userData token');
                    if (this.props.userData.userData.roleDetails[0].roleName == 'shopOwner') {
                        const resetAction = StackActions.reset({
                            index: 0,
                            actions: [NavigationActions.navigate({ routeName: 'shopHomePage'})]
                            })
                        this.props.navigation.dispatch(resetAction);
                    } else {
                        const resetAction = StackActions.reset({
                            index: 0,
                            actions: [NavigationActions.navigate({ routeName: 'homePage'})]
                            })
                        this.props.navigation.dispatch(resetAction);
                    }
                } else {
                    const resetAction = StackActions.reset({
                        index: 0,
                        actions: [NavigationActions.navigate({ routeName: 'homePage'})]
                        })
                    this.props.navigation.dispatch(resetAction);
                }
            } else if(this.props.userData.driverData.token) {
                const resetAction = StackActions.reset({
                    index: 0,
                    actions: [NavigationActions.navigate({ routeName: 'driverOrderListing'})]
                    })
                await this.props.navigationAction.driverNavigateAction('Order');
                this.props.navigation.dispatch(resetAction);
            } else {
                const resetAction = StackActions.reset({
                    index: 0,
                    actions: [NavigationActions.navigate({ routeName: 'login'})]
                    })
                this.props.navigation.dispatch(resetAction);
            }
            
        }, 1000);

    }

    render() {
        return (
            <SafeAreaView style={{flex: 1}}>
                <View style={{flex: 1,backgroundColor:"#f1f1f1",justifyContent:"center",alignItems:"center"}}>
                    <Image style={styles.logoStyle} source={require('../../../assets/Logo.png')}/>
                </View>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    logoStyle: {
        height: 300,
        width: 200
    },
});

//--------------------------------------------------- store related fuction  -- Start

const mapStateToProps = (state) => {
	return {
        userData: state.loginData,
        internateState: state.internateState,
        currentLocation: state.currentLocation,
        selectedIcon: state.selectedIcon,
	};
}

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(loginSuccess,dispatch),
        internateAction: bindActionCreators(internateAction,dispatch),
        navigationAction: bindActionCreators(navigateAction,dispatch),
    };
}

// ---------------------------------------------------- store related function  -- END

export default connect(mapStateToProps, mapDispatchToProps)(LandingPage);

