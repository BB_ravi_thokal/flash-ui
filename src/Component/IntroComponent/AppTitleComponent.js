/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Fragment } from 'react';

// import 'bootstrap/dist/css/bootstrap.min.css';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  Button,
  TextInput,
  StatusBar,
  TouchableWithoutFeedback,
  TouchableOpacity,
  Image,
  ToastAndroid,
  Dimensions
} from 'react-native';
import Dots from 'react-native-dots-pagination';
import InternateModal from '../ReusableComponent/InternetModal';
import Toast from 'react-native-simple-toast';
//import GestureRecognizer, {swipeDirections} from 'react-native-swipe-gestures';
// import { Container, Row, Col } from 'reactstrap';




class AppTitle extends React.Component{

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        console.warn('screen width--->', ScreenWidth);
    }

    render() {
        const {navigate} = this.props.navigation;
        return (
            <SafeAreaView style={{flex: 1}}>
                <View style={{flex: 1}}>
                    {/* <InternateModal /> */}
                    <View style={{flex:1,flexDirection:"row"}}>
                    
                        <View style={{flex:11.5}}>
                            <View style={{flex:2,justifyContent:"center",alignItems:"center",backgroundColor:"#dcf0fa"}}>
                            <View style={styles.logoContainer}>
                                <Image style={styles.logoStyle} source={require('../../../assets/Logo.png')}/>
                                <Text style={{fontWeight: 'bold',fontSize: 30,color:"#131e3a"}}>FLASH</Text>
                            </View>
                            
                            </View>
                            <View style={{flex:1,backgroundColor:"#dcf0fa"}}>
                            <View style={{padding:10,marginHorizontal:10}}>
                                <Text style={{fontWeight: 'bold',fontSize: 25,color:"#131e3a"}}>
                                PICK UP YOUR DAILY NEEDS
                                </Text>
                            </View>
                            <View style={{padding:10,marginHorizontal:10}}>
                                <Text style={{fontSize: 18,color:"#003151",lineHeight: 25}}>
                                    And we are here to DELIVER
                                </Text>
                            </View>
                            </View>
                            <View style={{flex:0.5,flexDirection:"row",marginLeft:10,backgroundColor:"#dcf0fa"}}>
                            <View style={{flex:1,flexDirection:"column",justifyContent:"center"}}>
                                <Dots length={4} active={0} activeColor="#00ccff" passiveDotHeight={8} activeDotHeight={12} activeDotWidth={12} passiveDotWidth={8}/>
                            </View>
                            <View style={{flex:1,flexDirection:"row",justifyContent:"flex-end",alignItems:"center"}}>
                                <TouchableOpacity onPress={() => navigate('sendPackageAd')}
                                    style={{height:45,width:70,backgroundColor:"#00ccff",borderTopLeftRadius:30,borderBottomLeftRadius:30,justifyContent:"center",alignItems:"center"}}>
                                    <Image style={{}} source={require('../../../assets/right.png')}/>
                                </TouchableOpacity>
                            </View>
                            </View>
                        </View>
                        <View style={{flex:0.5,backgroundColor:"#00ccff"}}>
                        </View>
                    </View>

                </View>

            </SafeAreaView>

        );
    }

}

let ScreenHeight = Dimensions.get("window").height;
let ScreenWidth = Dimensions.get("window").width;

const styles = StyleSheet.create({
    logoContainer:{
        flex:2,
        justifyContent: 'center',
        alignItems: 'center',
        
    },
        logoStyle: {
        height: 300,
        width: 200
    },
});


export default AppTitle;
