/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import axios from 'axios';
import * as Config from '../../Constant/Config.js';
import * as Constant from '../../Constant/Constant.js';
import  s  from '../../sharedStyle.js'
// import Paytm from 'react-native-paytm';
import * as navigateAction  from '../../Actions/NavigationBarAction';
import ShopNavigationBar from '../ReusableComponent/ShopNavigationBar.js'; 
import { NavigationActions, StackActions } from 'react-navigation';
import StepIndicator from 'react-native-step-indicator';
// import 'bootstrap/dist/css/bootstrap.min.css';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  Dimensions,  
  Image,
  TouchableOpacity,
  FlatList,
  Linking,
  Clipboard
} from 'react-native';
import Toast from 'react-native-simple-toast';
import AnimatedLoader from "react-native-animated-loader";
import callAPI from '../../CoreFunctions/callAPI';

const labels = ["Approval Pending","Order Approved","Shipped","Out for delivery", "Delivered"];
const customStyles = {
    stepIndicatorSize: 30,
    currentStepIndicatorSize:30,
    separatorStrokeWidth: 2,
    currentStepStrokeWidth: 3,
    stepStrokeCurrentColor: '#fe7013',
    stepStrokeWidth: 2,
    stepStrokeFinishedColor: '#fe7013',
    stepStrokeUnFinishedColor: '#aaaaaa',
    separatorFinishedColor: '#fe7013',
    separatorUnFinishedColor: '#aaaaaa',
    stepIndicatorFinishedColor: '#fe7013',
    stepIndicatorUnFinishedColor: '#ffffff',
    stepIndicatorCurrentColor: '#ffffff',
    stepIndicatorLabelFontSize: 13,
    currentStepIndicatorLabelFontSize: 13,
    stepIndicatorLabelCurrentColor: '#fe7013',
    stepIndicatorLabelFinishedColor: '#ffffff',
    stepIndicatorLabelUnFinishedColor: '#aaaaaa',
    labelColor: '#999999',
    labelSize: 13,
    currentStepLabelColor: '#fe7013'
  }

class ActiveOrderDetailsShop extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            totalCost: 0.00, 
            currentPosition: 0,
            activeOrderDetails: {},
            showLoader: false,
            isAccepted: false,
            isRejected: false,
            isCancelled: false,
            unAvailableProducts: [],
            unAvailableCombos: [],
            showUnMark: false,
            unavailableShopRate: 0.00,
            isDelivered: false
        }
    }

    async componentDidMount() {
        // console.warn('this.state.activeOrderDetails', this.state.activeOrderDetails.shopOrderStatus.invoiceURL);
        // console.warn('status---========>', this.state.activeOrderDetails.shopOrderStatus);
        // console.warn('===>id', this.props.userData.userData.shopId);
        // this.handleActiveOrderState(this.state.activeOrderDetails);
        const reqBody = {
            shopId : this.props.userData.userData.shopId,
            orderId: this.props.navigation.state.params.orderDetails.orderId
        };
        const orderDetails = await callAPI.axiosCall(Config.getOrderDetailsForShop, reqBody);
        console.warn('orderDetails --->', orderDetails.data.currentOrder[0]);
        this.setState({showLoader: false});
        if (orderDetails) {
            if (orderDetails.data.currentOrder.length > 0) {
                console.warn('order Obj--->', orderDetails.data.currentOrder[0].order.itemList);
                orderDetails.data.currentOrder[0].order.itemList = orderDetails.data.currentOrder[0].order.itemList.map( item => {
                    if (item.isProductAvailable == false) {
                        item.backgroundColor = "red"   
                    } else {
                        item.backgroundColor = "white" 
                    }
                    return item;
                });
                this.setState({ activeOrderDetails: orderDetails.data.currentOrder[0] });
                this.handleActiveOrderState(orderDetails.data.currentOrder[0]);
            } else {
                await this.setState ({ isDelivered: true });
                this.showToaster('Order is delivered/ rejected successfully. Please check your profile for invoice');
                this.props.navigation.navigate('shopHomePage');
            }
            console.warn('order Obj--->', orderDetails.data.currentOrder[0].order.itemList);
            orderDetails.data.currentOrder[0].order.itemList = orderDetails.data.currentOrder[0].order.itemList.map( item => {
                if (item.isProductAvailable == false) {
                    item.backgroundColor = "red"   
                } else {
                    item.backgroundColor = "white" 
                }
                return item;
            });
            this.setState({ activeOrderDetails: orderDetails.data.currentOrder[0] });
            this.handleActiveOrderState(orderDetails.data.currentOrder[0]);
        } else {
            this.setState({showLoader: false});
        }
        
    }

    handleActiveOrderState = async(data) => {
        if (data.shopOrderStatus) {
            console.warn('data.shopOrderStatus======>', data.shopOrderStatus)
            this.setState({
                totalCost: parseFloat(data.shopOrderStatus.totalShopRate) - parseFloat(data.shopOrderStatus.unavailableShopRate),
                unavailableShopRate: parseFloat(data.shopOrderStatus.unavailableShopRate)
            });
            if (data.shopOrderStatus.status == 'Pending') {
                await this.setState({
                    isAccepted: false,
                    showUnMark: false
                });
            } else if (data.shopOrderStatus.status == 'Rejected') {
                console.warn('in rejected');
                // await this.setState({
                //     isRejected: true,
                //     isAccepted: false,
                //     showUnMark: false
                // });
                this.showToaster('You rejected this order. Please contact support team in case of any help');
                this.props.navigation.navigate('shopHomePage');
            } else if (data.shopOrderStatus.status == 'Confirmed') {
                await this.setState({
                    isRejected: false,
                    isAccepted: true,
                    showUnMark: true
                });
            } else if (data.shopOrderStatus.status == 'Partially Confirmed') {
                await this.setState({
                    isRejected: false,
                    isAccepted: true,
                    showUnMark: false
                });
            } else if (data.shopOrderStatus.status == 'Delivered') {
                await this.setState({
                    isDelivered: true
                });
                this.showToaster('Order is delivered successfully. Please check your profile section for invoice');
                this.props.navigation.navigate('shopHomePage');
            } else {
                console.warn('in else condition--->');
                await this.setState({
                    isRejected: false,
                    isAccepted: true,
                    showUnMark: false
                });
            }
        }
        data.order.itemList = data.order.itemList.map( item => {
            if (item.shopId == this.props.userData.userData.shopId) {
                if(item.isProductAvailable == false) {
                    console.warn('in 1st cond===>');
                    abc = true;
                    this.setState({showUnMark: false});
                    item.backgroundColor = "#FF0000" 
                } else {
                    console.warn('in 2nd cond');
                    item.backgroundColor = "white" 
                }
            }
            
              return item;
        });
        await this.setState({activeOrderDetails: data});
        console.warn('showUnMark--->', this.state.showUnMark);
        if (this.state.activeOrderDetails.orderStatus) {
            switch(this.state.activeOrderDetails.orderStatus) {
              case 'Pending': 
                            this.setState({
                              currentPosition: 0
                            })
                            break;
              case 'Confirmed': 
                              this.setState({
                                currentPosition: 1,
                              })
                            break;
              case 'Shipped'  : 
                              this.setState({
                                currentPosition: 2,
                              })
                            break;
              case 'Out for Delivery': 
                              this.setState({
                                currentPosition: 3,
                              })
                            break;
              case 'Delivered':
                              this.markDelivered();
                              
              case 'Cancelled':
                              this.setState({
                                isCancelled: true
                            })
                            break;
            }
        }
    }

    ItemListing = ({data}) => {
        if (this.props.userData.userData.shopId == data.shopId) {
            return (
                <TouchableOpacity style={[styles.prodcutListingView, {backgroundColor: data.backgroundColor}]}
                onPress={() => this.selectProduct(data)}>
                    
                    <View style={styles.addCardImage}>
                        <Image style={styles.productImage} source={{uri: data.productLogo}}/>
                    </View>
                    <View style={styles.stores}>
                        
                        {!data.comboId
                        ? 
                        <View style={{flex:10}}>
                            <Text style={s.subHeaderText} numberOfLines={4}>{data.productName} </Text>
                        </View>
                        :
                        <View style={{flex:10}}>
                            <Text style={s.subHeaderText} numberOfLines={4}>{data.comboName} </Text>
                        </View>
                        }
                        
                        <View style={styles.Mystores}>
                        {!data.comboId
                        ? 
                            <View style={{flexDirection: "column", flex: 7}}>
                                <Text style={[s.normalText]} numberOfLines={4}>Unit cost:{data.unitCost}/{data.unit}</Text>
                                <Text style={[s.normalText]} numberOfLines={1}>Qty: ₹{data.counter}</Text>
                                <Text style={[s.normalText]} numberOfLines={1}>Total: ₹{(parseFloat(data.counter) * parseFloat(data.shopRate))}</Text>
                                <Text style={[s.normalText]} numberOfLines={3}>Shop Name: {data.shopName}</Text>
                                {data.isBogo 
                                ?
                                    <Text style={[s.subHeaderText]} numberOfLines={2}>Buy 1 Get 1 Free</Text>
                                :
                                    null
                                }
                                {!data.isProductAvailable 
                                ?
                                    <Text style={[s.subHeaderText]} numberOfLines={2}>Out of stock</Text>
                                :
                                    null
                                }
                            </View>
                            :
                            <View style={{flexDirection: "column", flex: 7}}>
                                <Text style={[s.normalText]} numberOfLines={1}>Qty: ₹{data.counter}</Text>
                                <Text style={[s.normalText]} numberOfLines={1}>Total: ₹{(parseFloat(data.counter) * parseFloat(data.shopRate))}</Text>
                                <Text style={[s.normalText]} numberOfLines={3}>Shop Name: {data.shopName}</Text>
                                {!data.isProductAvailable 
                                ?
                                <Text style={[s.subHeaderText]} numberOfLines={2}>Out of stock</Text>
                                :
                                null
                                }
                            </View>
                        }
                            
                    </View>
                    {data.comboId
                    ?
                    <View>
                        <Text style={[s.normalText]} numberOfLines={10}>Combo details: {data.comboDescription}</Text>
                    </View> 
                    : null
                    }
                    </View>
                    
                </TouchableOpacity>
            )
        } else {
            return null;
        }
    } 

    render() {
        const {navigate} = this.props.navigation;
        if (!this.state.isDelivered) {
            return (
                <SafeAreaView style={{flex: 1}}>
                    <View style={[s.bodyGray, {flex: 1}]}>
                        <AnimatedLoader
                            visible={this.state.showLoader}
                            overlayColor="rgba(255,255,255,0.75)"
                            source={require("../../../assets/loader.json")}
                            animationStyle= {[styles.lottie,{height: 150,width: 150}]}
                            speed={1}
                        />
                         <View style={{flex: 11}}>
                            <ScrollView showsVerticalScrollIndicator={false}>
                            <View style={{marginBottom: 10, borderBottomWidth: 1, borderBottomColor: 'silver', padding: 15}}>
                                <Text style={[styles.headerText, {textAlign: "center", marginBottom: 10}]}>Order Status</Text>
                                <StepIndicator
                                    customStyles={customStyles}
                                    currentPosition={this.state.currentPosition}
                                    labels={labels}
                                />
                            </View>
                            {Object.keys(this.state.activeOrderDetails).length > 0
                            ?
                                <>
                                <View style={{backgroundColor:"white",elevation: 6}}>
                                    <FlatList
                                        data={this.state.activeOrderDetails.order.itemList}
                                        renderItem={( { item } ) => (
                                        <this.ItemListing
                                            data={item}
                                        />
                                        
                                        )}
                                        extraData={this.state} 
                                    />
                                </View>
                                <View style={{backgroundColor:"white",marginTop:15,paddingVertical: 15,paddingHorizontal:15, elevation: 6}}>
                                    <View style={{flex: 1, flexDirection:"row"}}>
                                        <View style={{flex: 6}}>
                                            <Text style={[s.headerText]}>Bill Details </Text>
                                        </View>
                                        {this.state.activeOrderDetails.shopOrderStatus.invoice
                                        ?
                                        <TouchableOpacity style={{flex: 6, alignItems: "flex-end"}} onPress={() => this.downloadInvoice(this.state.activeOrderDetails.shopOrderStatus.invoice)}>
                                            <Image style={{height: 30, width: 30}} source={require('../../../assets/download.png')}/>  
                                        </TouchableOpacity> 
                                        :
                                        null
                                        }
                                        
                                    </View>
                                    <View style={{paddingTop:10}}>
                                        <View style={{flex: 1, flexDirection: "row",paddingVertical: 3}}>
                                            <View style={{flex: 5}}>
                                                <Text style={[s.normalText]} >Order Id:</Text>
                                            </View>
                                            <View style={{flex:7,alignItems:"flex-end"}}>
                                                <Text style={[s.normalText]} >{this.state.activeOrderDetails.orderId} </Text>
                                            </View>
                                        </View>
                                        {!this.state.showUnMark
                                        ?
                                            <View style={{flex: 1, flexDirection: "row",paddingVertical: 3}}>
                                                <View style={{flex: 5}}>
                                                    <Text style={[s.normalText]} numberOfLines={2}>Unavailable product cost:</Text>
                                                </View>
                                                <View style={{flex:7,alignItems:"flex-end"}}>
                                                    <Text style={[s.normalText]} >₹{(this.state.unavailableShopRate).toFixed(2)}</Text>
                                                </View>
                                            </View>
                                        :
                                            null
                                        }
                                        <View style={{flex: 1, flexDirection: "row",paddingVertical: 3}}>
                                            <View style={{flex: 5}}>
                                                <Text style={[s.normalText]} >Total cost:</Text>
                                            </View>
                                            <View style={{flex:7,alignItems:"flex-end"}}>
                                                <Text style={[s.normalText]} >₹ {(this.state.totalCost).toFixed(2)} </Text>
                                            </View>
                                        </View>
                                    </View>
                                </View>
                                </>
                            :
                                null
    
                            }
                                
                                
                            </ScrollView>
                        </View>
                        <View style={{flex: 1, flexDirection:"row"}}>
                        {this.state.isAccepted
                            ?
                                <>
                                    {this.state.showUnMark
                                    ?
                                        <>
                                            <TouchableOpacity style={{flex: 6,backgroundColor:"#black",alignItems:"center",justifyContent:"center"}}
                                            onPress={() => this.updateOrderStatus('')}>
                                                <Text style={[s.subHeaderText,{color:"white"}]}>Mark as Unavailable</Text>
                                            </TouchableOpacity>
                                            <TouchableOpacity style={{flex: 6,backgroundColor:"#00CCFF",alignItems:"center",justifyContent:"center"}}
                                            onPress={() => this.updateOrderStatus('Partially Confirmed')}>
                                                <Text style={[s.subHeaderText,{color:"white"}]}>Confirm</Text>
                                            </TouchableOpacity>
                                        </>
                                    :
                                        null
                                    }
                                </>
                                
                            :
                                <>
                                {this.state.isRejected
                                ?   
                                    
                                    <View style={{flex: 1,backgroundColor:"#00CCFF",alignItems:"center",justifyContent:"center"}}>
                                        <Text style={[s.headerText,{color:"white"}]}>You Rejected</Text>
                                    </View>
                                :
                                    <>
                                    {this.state.isCancelled
                                    ?
                                        <View style={{flex: 1,backgroundColor:"#00CCFF",alignItems:"center",justifyContent:"center"}}>
                                            <Text style={[s.headerText,{color:"white"}]}>User Cancelled</Text>
                                        </View>
                                    :
                                        <>
                                            <TouchableOpacity style={{flex: 6,backgroundColor:"black",alignItems:"center",justifyContent:"center"}}
                                            onPress={() => this.updateOrderStatus('reject')}>
                                                <Text style={[s.headerText,{color:"white"}]}>Reject</Text>
                                            </TouchableOpacity>
                                            <TouchableOpacity style={{flex: 6,backgroundColor:"#00CCFF",alignItems:"center",justifyContent:"center"}}
                                            onPress={() => this.updateOrderStatus('accept')}>
                                                <Text style={[s.headerText,{color:"white"}]}>Accept</Text>
                                            </TouchableOpacity>
                                        </>
                                    }
                                    </>
                                    
                                }
                                </>
                            }
                        </View>
                    </View>
                </SafeAreaView>
            );
        } else {
            return null
        }
        
    }

    enableDetailsView = (data) => {
        console.warn('in enable details method--->', data);
        this.setState({
            activeOrderDetails: data.order.itemList,
            isListView: false,
        });
    }

    updateOrderStatus = async(status) => {
        console.warn('status', status);
        console.warn('in enable details method--->', this.state.activeOrderDetails.orderId);
        this.setState({showLoader: true});
        let reqBody;
        if (status == 'accept') {
            reqBody = {
                shopId: this.props.userData.userData.shopId,
                orderId: this.state.activeOrderDetails.orderId,
                shopOrderStatus: "Confirmed"
            }
        } else if (status == 'reject') {
            reqBody = {
                shopId: this.props.userData.userData.shopId,
                orderId: this.state.activeOrderDetails.orderId,
                shopOrderStatus: "Rejected"
            };
        } else if (status == 'Partially Confirmed') {
            reqBody = {
                shopId: this.props.userData.userData.shopId,
                orderId: this.state.activeOrderDetails.orderId,
                shopOrderStatus: "Partially Confirmed"
            };
        } else {
            console.warn('in unavailable');
            if (this.state.unAvailableProducts.length > 0 || this.state.unAvailableCombos.length > 0) {
                reqBody = {
                    shopId: this.props.userData.userData.shopId,
                    orderId: this.state.activeOrderDetails.orderId,
                    shopOrderStatus: "Unavailable",
                    productId: this.state.unAvailableProducts,
                    comboId: this.state.unAvailableCombos
                };
            } else {
                this.setState({showLoader: false});
                this.showToaster('Please select the product which is unavailable');
                return;
            }   
        }
        const updateOrderStatus = await callAPI.axiosCall(Config.updateShopOrderStatus, reqBody);
        this.setState({showLoader: false});
        // console.warn('response of order status-->', updateOrderStatus);
        if (updateOrderStatus) {
            this.showToaster(updateOrderStatus.data.message);
            if (updateOrderStatus.data.currentOrder.length > 0) {
                console.warn('shopOrderstatus====--->', updateOrderStatus.data.currentOrder[0].shopOrderStatus);
                await this.setState({
                    activeOrderDetails: updateOrderStatus.data.currentOrder[0]
                });
                this.handleActiveOrderState(updateOrderStatus.data.currentOrder[0]);
            } else {
                this.setState({
                    isRejected: true
                });
            }
            
        } else {
            this.showToaster('Failed to reject order. Please contact support team or try later');
        }   
    }

    selectProduct = (data) => {
        if(!this.state.showUnMark) {
            this.showToaster('You cannot change product status now');
        } else {
            if (data.productId) {
                const i = this.state.activeOrderDetails.order.itemList.findIndex(ind => ind.productId === data.productId);
                if (i >= 0) {
                    if (this.state.activeOrderDetails.order.itemList[i].backgroundColor == 'white') {
                        console.warn('push--->',data.productId);
                        this.state.activeOrderDetails.order.itemList[i].backgroundColor = '#FF0000';
                        this.state.unAvailableProducts.push(data.productId);
                        console.warn('push color change',this.state.activeOrderDetails);
                        this.setState({
                            unAvailableProducts: this.state.unAvailableProducts,
                            activeOrderDetails: this.state.activeOrderDetails
                        });
                    } else {
                        console.warn('in if');
                        this.state.activeOrderDetails.order.itemList[i].backgroundColor = 'white'; 
                        const delIndex = this.state.unAvailableProducts.indexOf(data.productId);
                        if (delIndex >= 0) {
                            this.state.unAvailableProducts.splice(delIndex, 1);
                        } 
                        this.setState({
                            unAvailableProducts: this.state.unAvailableProducts,
                            activeOrderDetails: this.state.activeOrderDetails
                        });
                    }
            
                }
            } else if (data.comboId) {
                const i = this.state.activeOrderDetails.order.itemList.findIndex(ind => ind.comboId === data.comboId);
                if (i >= 0) {
                    if (this.state.activeOrderDetails.order.itemList[i].backgroundColor == 'white') {
                        console.warn('push--->',data.comboId);
                        this.state.activeOrderDetails.order.itemList[i].backgroundColor = '#FF0000';
                        this.state.unAvailableCombos.push(data.comboId);
                        console.warn('push color change',this.state.activeOrderDetails);
                        this.setState({
                            unAvailableCombos: this.state.unAvailableCombos,
                            activeOrderDetails: this.state.activeOrderDetails
                        });
            
                    } else {
                        console.warn('in if');
                        this.state.activeOrderDetails.order.itemList[i].backgroundColor = 'white'; 
                        const delIndex = this.state.unAvailableCombos.indexOf(data.comboId);
                        if (delIndex >= 0) {
                            this.state.unAvailableCombos.splice(delIndex, 1);
                        } 
                        this.setState({
                            unAvailableCombos: this.state.unAvailableCombos,
                            activeOrderDetails: this.state.activeOrderDetails
                        });
                    }
                }
            }   
        }
        
    }

    showToaster = (message) => {
        setTimeout(() => {
            Toast.show(message, Toast.LONG);
        }, 100);
      }

    downloadInvoice = (url) => {
        console.warn('url==>', url);
        Clipboard.setString(url);
        Linking.openURL(url).catch((err) =>
            this.showToaster('Not able to download invoice. Please contact support team')
        );
    }
};

let ScreenHeight = Dimensions.get("window").height;
let ScreenWidth = Dimensions.get("window").width;

const styles = StyleSheet.create({
    card: {
        backgroundColor: "white",
        padding: 15,
    },
    prodcutListingView: {
        flexDirection: 'row',
        marginVertical: 5,
        backgroundColor: "white",
        padding: 10
    },
    addCardImage: {
        width: 80,
        height: 80,
        flex: 4
    },
    productImage: {
        width: 80,
        height: 80
    },
    stores: {
        paddingHorizontal: 10,
        flex: 8,
        flexDirection: "column"
    },
    Mystores: {
        flexDirection: "row",
        alignItems: "flex-start",
        justifyContent: "space-between",
        textAlignVertical: "bottom",
        flex: 2
    },
});



//--------------------------------------------------- store related fuction  -- Start

const mapStateToProps = (state) => {
    return {
        userData: state.loginData,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        navigationAction: bindActionCreators(navigateAction,dispatch),
    };
}

// ---------------------------------------------------- store related function  -- END


export default connect(mapStateToProps, mapDispatchToProps)(ActiveOrderDetailsShop);