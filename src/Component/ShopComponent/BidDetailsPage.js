/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import axios from 'axios';
import * as Config from '../../Constant/Config.js';
import * as Constant from '../../Constant/Constant.js';
import NavigationBar from '../ReusableComponent/NavigationBar.js'
import * as loginSuccess  from '../../Actions/LoginAction';
import  s  from '../../sharedStyle.js'
import InternateModal from '../ReusableComponent/InternetModal';
import * as navigateAction  from '../../Actions/NavigationBarAction';
import Toast from 'react-native-simple-toast';
// import 'bootstrap/dist/css/bootstrap.min.css';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  Dimensions,
  TouchableOpacity,
  TextInput,
  Image,
  ToastAndroid,
  FlatList,
} from 'react-native';


class BidDetailsPage extends React.Component{

  constructor(props) {
    super(props);
    this.state={
        placedBidDetails: [],
        itemList: [],
        shopDetails: [],
        bidAmount: 0.00,
        comment: "",
        isUpdate: false,
        prevBidAmount: 0.00,
        btnText: "Place bid",
        bidAmtWidth: 0,
        bidAmtColor: "black",
        baseCost: 0.00
    }
  }


  PlacedBidListing = (item) => {
    if(this.state.shopDetails.length > 0){
      return (
        <View style={[styles.bidListingView]}>
            <View style={{flex:10}}>
                <Text style={s.headerText} numberOfLines={1}>{item.item.shopDetails.shopName}</Text>
            </View>
            
            <View style={[styles.Mystores,{marginVertical: 5}]}>
                <View style={{paddingVertical: 5}}>
                    <Text style={[s.subHeaderText]}>Comment: </Text>    
                    <Text style={[s.normalText]} numberOfLines={4}>{item.item.comment} </Text>    
                </View>
                
            </View>
            <View style={{flexDirection:"row"}}>
                <Image style={{height: 18, width: 18}} source={require('../../../assets/money.jpg')}/>  
                <Text style={[s.subHeaderText, {paddingLeft: 5}]}>Bid Amount: ₹{item.item.bidAmount}</Text>   
            </View>
        </View>
        
      )
    } else {
        return null;
    }
  
  
  }

    AddBidDetails = () => {
        const {navigate} = this.props.navigation;
        return(
            <View style={{flex: 1, backgroundColor:"white",paddingTop: 10, borderTopWidth:2,borderTopColor:"#7285A5"}}>
                    
                <View style={{flex:1,paddingHorizontal:15, borderBottomWidth: 1, borderColor: "grey"}}>
                    <View style={{ paddingVertical: 5}}>    
                        <Text style={s.subHeaderText}>Your bid:</Text>
                        <TextInput value={this.state.bidAmount}
                              style={[styles.inputBox,{elevation: 4,borderWidth: this.state.bidAmtWidth, borderColor: this.state.bidAmtColor}]}
                              underlineColorAndroid="transparent"
                              onChangeText={(amount) => {
                              this.setState({
                              bidAmount: amount,
                              bidAmtColor: "white",
                              bidAmtWidth: 0
                          })}} />
                    </View>
                    <View style={{ paddingVertical: 5}}>    
                        <Text style={s.subHeaderText}>Add comment: </Text>
                        <TextInput value={this.state.comment}
                              style={[styles.inputBox,{elevation: 4}]}
                              underlineColorAndroid="transparent"
                              numberOfLines = {2}
                              onChangeText={(comment) => {
                              this.setState({
                              comment: comment,
                          })}} />
                    </View>
                </View>
            </View>
        )
    }

  CartListing = (item) => {
    // const imgPath = data.productLogo + data.productName.replace(/\s+/g, '') + '.jpg';
    return (  
      <View style={[styles.prodcutListingView]}>
        
        <View style={styles.addCardImage}>
          <Image style={styles.productImage} source={require('../../../assets/foodIcon.jpg')}></Image>
        </View>
          <View style={styles.stores}>
            <View style={{flex:10}}>
              <Text style={s.headerText} numberOfLines={1}>{item.item.productName}</Text>
            </View>
            
            <View style={styles.Mystores}>
              <View style={{flexDirection: "column"}}>
                <Text style={[s.normalText]} numberOfLines={1}>Quantity: {item.item.counter}</Text> 
                <Text style={[s.normalText]} numberOfLines={1}>Unit cost: ₹ {item.item.unitCost}</Text>
                <Text style={[s.normalText]} numberOfLines={1}>Total cost: ₹ {item.item.totalCost}</Text>
              </View>
            </View>
          </View>
        </View>
    )
  }    

  componentDidMount(){
    
    
    axios.post(
        Config.URL + Config.getBidByBidId,
        {
          "userId": this.props.navigation.state.params.userId,
          "bidId": this.props.navigation.state.params.bidId,
        },
        {
          headers: {
              'Content-Type': 'application/json',
          }
        }
    )
    .then(res => {
        console.warn("res---",res.data.result);
        if (res.data.status == 0) {
          this.setState({
            placedBidDetails: res.data.result.bidDetails,
            itemList: res.data.result.bidDetails[0].bidInfo.itemList,
            shopDetails: res.data.result.shopBidDetailForUser, 
            baseCost: res.data.result.bidDetails[0].bidInfo.totalCost
          })


          if (res.data.result.shopBidDetailForUser.some(arr => arr.shopId == this.props.userData.userData.shopId)) {
            console.warn('isUpdate');
            const i = res.data.result.shopBidDetailForUser.findIndex(ind => ind.shopId === this.props.userData.userData.shopId);
            console.warn('index', i);
            console.warn('bid details', parseFloat(res.data.result.shopBidDetailForUser[i].bidAmount));
            this.setState({
              isUpdate: true,
              btnText: "Update bid",
              bidAmount: res.data.result.shopBidDetailForUser[i].bidAmount,
              comment: res.data.result.shopBidDetailForUser[i].comment,
              prevBidAmount: res.data.result.shopBidDetailForUser[i].bidAmount
            })
          }
          
        }  

      }).catch(err => {
          //console.warn(res.data.status);
      })

  }

  BidNotAvailable = () => {
    if(this.state.shopDetails.length == 0) {
        return (
            <View style={{alignItems:"center", marginTop: 20}}>
                <Text style={s.subHeaderText}>No shop placed the bid against your basket.</Text>
            </View>
        )
    } else {
        return null;
    }
  }

  placedBid = () => {
    console.warn('this.props.userData.userData.userId', this.props.userData.userData);
    if (this.state.isUpdate) {
      if (this.state.prevBidAmount === this.state.bidAmount) {
        Toast.show("Bid amount is same as previously placed bid",Toast.LONG);   
      } else if (this.state.bidAmount > this.state.baseCost) { 
        Toast.show("Your bid amount is more than base cost",Toast.LONG);
      } else {
        axios.post(
          Config.URL + Config.updateBidShop,
          {
            // "userId": this.props.navigation.state.params.userId,
            "bidId": this.props.navigation.state.params.bidId,
            "shopId": this.props.userData.userData.shopId,
            "bidAmount": this.state.bidAmount,
            "comment": this.state.comment
          },
          {
            headers: {
                'Content-Type': 'application/json',
            }
          }
        )
        .then(res => {
            console.warn("res---",res);
            if (res.data.status == 0) {
              console.warn('bid placed success');     
              Toast.show("Bid updated successfully",Toast.LONG);   
              this.props.navigation.navigate('shopHomePage');
            } else {
              Toast.show("Error occured while submiting the bid",Toast.LONG);
            }  
      
          }).catch(err => {
            console.warn('bid placed error', err);
        })
      }
    } else if (this.state.bidAmount != 0) {
      if (this.state.bidAmount > this.state.baseCost) {
        Toast.show("Your bid amount is more than base cost",Toast.LONG);
      } else {
      axios.post(
        Config.URL + Config.placeBidForUser,
        {
          "userId": this.props.navigation.state.params.userId,
          "bidId": this.props.navigation.state.params.bidId,
          "shopId": this.props.userData.userData.shopId,
          "bidAmount": this.state.bidAmount,
          "comment": this.state.comment
        },
        {
          headers: {
              'Content-Type': 'application/json',
          }
        }
      )
      .then(res => {
          console.warn("res---",res);
          if (res.data.status == 0) {
            console.warn('bid placed success');     
            Toast.show("Bid placed successfully",Toast.LONG);   
            this.props.navigation.navigate('shopHomePage');
          } else {
            Toast.show("Error occured while submiting the bid",Toast.LONG);
          }  
    
        }).catch(err => {
          console.warn('bid placed error', err);
      })  
  
    }
    } else {
      Toast.show("Please add bid amount",Toast.LONG);
      this.setState({
        bidAmtColor: "red",
        bidAmtWidth: 1
      })
    }
 
  }


  render() {
    const {navigate} = this.props.navigation;
      return (
        <SafeAreaView style={{flex: 1}}>
          <View style={[s.body,{flex: 1}]}>
            {/* <InternateModal /> */}
              <View style={[styles.storeDtlContainer,{flex: 7}]}>
                  <ScrollView showsVerticalScrollIndicator={false}>
                      <View style={{flex: 1,flexDirection: "row",borderBottomWidth:3,borderBottomColor:"#003151", paddingBottom: 10}}>
                          <View style= {{flex: 8}}>
                              <Text style={s.headerText}>User Name</Text>
                          </View>
                          <View style={{flex: 4, flexDirection: "row", alignItems:"flex-end"}}>
                              <View style={{flex: 3, alignItems:"flex-end"}}>
                                  <Image source={require('../../../assets/bid.png')}/>  
                              </View>
                              <View style={{flex: 1, alignItems:"flex-end"}}>
                                  <Text style={s.headerText}>1</Text>
                              </View>
                              
                          </View>
                      </View>
                      <View>
                      <View style={{flex: 1, flexDirection:"row", paddingTop: 10}}>
                        <View style={{flex: 6}}>
                          <Text style={s.headerText}>Base amount: </Text>
                        </View>
                        <View style={{flex: 6}}>
                          <Text style={[s.headerText,{color: "#00CCFF"}]}>₹ {this.state.baseCost}</Text>
                        </View>
                      
                      </View>  
                        <View style={{paddingTop: 10}}>
                          <Text style={s.subHeaderText}>Product Details</Text>
                        </View>
                          <View>
                          <FlatList data={this.state.itemList}
                              renderItem={( { item } ) => (
                                  <this.CartListing
                                  item={item}
                                  />
                              )}
                              keyExtractor= {item => item.productId}
                              extraData = {this.state.itemList}
                          />
                          </View>
                        </View>
                      <View>
                          <View style={{paddingTop: 10}}>
                              <Text style={s.subHeaderText}>Placed Bids</Text>
                          </View>
                          <View>
                            <FlatList data={this.state.shopDetails}
                                renderItem={( { item } ) => (
                                    <this.PlacedBidListing
                                    item={item}
                                    />
                                )}
                                keyExtractor= {item => item.shopId}
                                extraData = {this.state.shopDetails}
                            />
                            <this.BidNotAvailable />    
                          </View>
                      </View>
                  </ScrollView>
                  
              </View>
              <View style={{flex: 4}}>
                <this.AddBidDetails />
              </View>
              <View style={{flex:1,flexDirection:"row"}}>
                <TouchableOpacity style={{flex:6,backgroundColor:"#F5F5F5",alignItems:"center",justifyContent:"center"}}>
                    <Text style={{fontSize: 16,fontWeight: "bold",color:"#131E3A"}}>Cancel</Text>
                </TouchableOpacity>
                <TouchableOpacity style={{flex:6,backgroundColor:"#00CCFF",alignItems:"center",justifyContent:"center"}}
                  onPress={() => this.placedBid()}>
                  <Text style={[s.headerText,{color:"white"}]}>{this.state.btnText}</Text>
                </TouchableOpacity>
              </View>
          </View>
        
        </SafeAreaView>
      
      );
   
  }

}


let ScreenWidth = Dimensions.get("window").width;
let ScreenHeight = Dimensions.get("window").height;

const styles = StyleSheet.create({
  scrollView: {
    flex: 1
  },
  imgBlank: {
    height: ScreenHeight,
    width: ScreenWidth
  },
  containerIcons:{
    width:30,
    height:30,
  },
  inputBox: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    padding: 10,
    borderRadius: 10,
    width: '100%',
    backgroundColor:"white",
    fontWeight: "bold"
  },

  searchShops:{ 
    height: 40, 
    borderColor: '#7285A5', 
    borderWidth: 1,
    paddingHorizontal:10, 
    borderRadius:4 
  },
  sectionContainer: {
    marginTop: 20,
    paddingHorizontal: 24,
  },
  iconView: {
    width: 30,
    height: 30,
    marginRight: 20,
    borderRadius: 100,
  },
  storeCardLayout:{
    marginHorizontal: 15,
    paddingVertical:14,
    borderBottomWidth: 2,
    borderBottomColor: "#131E3A",
    alignItems: "center"
  },
  categoryFooter:{
    width:ScreenWidth,
    paddingBottom:20,
    height:80,
    flexGrow:0.05,
    backgroundColor:'white',
    display:'flex',
    flexDirection:'row',
    justifyContent:"space-evenly",
    alignItems:"center"
  },
  storeDtlContainer: {
    marginHorizontal: 15,
    paddingVertical: 10
  },

  storeContainer:{
    display:"flex",
    flexDirection: "row"
  },
  stores: {
    paddingHorizontal: 10,
    flex: 8,
    flexDirection: "column"
  },

  storeCount: {
    fontSize: 22,
    fontWeight: 'bold',
    paddingHorizontal: 10,
  },
  storeProductHeading: {
    fontWeight: 'bold',
    fontSize: 30,
  },
  shopdtlCards: {
    width: 80,
    height: 80,
    backgroundColor: 'orange',
    borderRadius: 100,
    borderWidth: 0.5,
    borderColor: 'black',
    marginVertical: 5,
    marginHorizontal: 10,
  },
  storeCardDisplay: {
    // padding: 10,
    paddingVertical: 10,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  storeCards: {
    height: 100,
    borderRadius: 4,
    borderWidth: 0.5,
    borderColor: 'black',
    backgroundColor: 'white',
    flex: 3,
  },
  advertiseData: {
    height: 200,
    width: ScreenWidth,
    borderBottomLeftRadius: 90,
    borderBottomRightRadius: 90,
    elevation: 4
  },
  storeHeading: {
    fontSize:24,
    
  },
  storeDesc: {
    alignItems: "center"
  },
  trendingShops: {
    borderBottomWidth: 2,
    borderColor: "#131E3A",
    marginHorizontal: 15,
    paddingTop: 10,
  },
  addCardImage: {
    width: 80,
    height: 80,
    flex: 4
  },
  productImage: {
    width: 80,
    height: 80,
  },
  storeCardButton: {
    backgroundColor: 'white',
    color: 'black',
    width: 30,
    height: 30,
    borderRadius: 50,
    alignItems: "center",
    justifyContent: 'center',
  },
  storeborder: {
    borderBottomColor: 'grey',
    borderBottomWidth: 1,
  },
  flexRow: {
    flex: 1,
    textAlignVertical: "bottom",
    flexDirection: 'row',
  },
  prodcutListingView: {
    marginVertical: 5,
    backgroundColor: "white",
    padding: 10,
    elevation: 6,
    flex: 1,
    flexDirection: "row"

  },
  bidListingView: {
    marginVertical: 5,
    backgroundColor: "white",
    padding: 10,
    elevation: 6,

  },
  flexColumnAlignEnd: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-end'
  },
  storeDtlCenter: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "center"
  },
  Mystores: {
    alignItems: "flex-start",
    justifyContent: "space-between",
    textAlignVertical: "bottom",
    flex: 1
    // paddingBottom:15,
  },
  storeCardSpace: {
    marginBottom: 15,
  },
  headerstyle: {
    paddingBottom: 20,
    paddingTop: 20
  },

  storeBtnViewColor: {
    backgroundColor: '#ba55d3',
    color: 'white',
  },
  continueBtnViewColor: {
    backgroundColor: '#2f4f4f',
    color: 'white',
  },
  addtoCartBtnColor: {
    fontSize:12,
    borderWidth: 1,
    borderColor: '#0080FE',
    marginLeft: 20,
  },
  storeBtn: {
    paddingVertical: 5,
    paddingHorizontal: 5,
    marginBottom: 5,
    color: 'white',
    borderRadius: 0,
    width: 50
  },
  basketBtn:{
    paddingVertical: 5,
    paddingHorizontal: 10,
    marginBottom: 10,
    borderRadius: 20,
    height: 30,
    width: 30
  },
  basketImg: {
    height: 30,
    width: 30,
  }

});

//--------------------------------------------------- store related fuction  -- Start

const mapStateToProps = (state) => {
	return {
    userData: state.loginData,
	}
}

const mapDispatchToProps = (dispatch) => {
    return {
      loginActions: bindActionCreators(loginSuccess,dispatch),
      navigationAction: bindActionCreators(navigateAction,dispatch),
    };
}

// ---------------------------------------------------- store related function  -- END


export default connect(mapStateToProps, mapDispatchToProps)(BidDetailsPage);


