/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import axios from 'axios';
import * as Config from '../../Constant/Config.js';
import * as Constant from '../../Constant/Constant.js';
import  s  from '../../sharedStyle.js'
import ShopNavigationBar from '../ReusableComponent/ShopNavigationBar.js'; 
import * as navigateAction  from '../../Actions/NavigationBarAction.js';
import callAPI from '../../CoreFunctions/callAPI';
import CartModal from '../ReusableComponent/cartModal';
import * as loginSuccess  from '../../Actions/LoginAction';
import * as walletAction  from '../../Actions/WalletAction';
import * as cartModalAction  from '../../Actions/CartModalAction';
import * as addToCart  from '../../Actions/CartItem';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  Dimensions,
  TouchableOpacity,
  TextInput,
  Image,
  ToastAndroid,
  FlatList,
} from 'react-native';
import Toast from 'react-native-simple-toast';

const MonthArray = ['Jan', 'Feb', 'March', 'Apr', 'May', 'June', 'July', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

class ShopHomePage extends React.Component{

    constructor(props) {
        super(props);
        this.state={
            activeBids: [],
            currentMonthOrders: 0,
            lastMonthOrders: 0,
            currentMonthSale: 0.00,
            lastMonthSale: 0.00,
            currentMonth: '',
            lastMonth: '',
            currentYear: ''
        }
    }

    async componentDidMount(){
        this.props.navigationAction.shopNavigateAction('ShopHome'); 
        console.warn('this.props.userData.userData--shopD', this.props.userData.userData.shopDetails);
        const d = new Date();
        this.setState({
            currentMonth: MonthArray[d.getMonth()],
            lastMonth: MonthArray[d.getMonth() - 1],
            currentYear: d.getFullYear()
        })
        const reqBody = {
            shopId: this.props.userData.userData.shopId,
            month: d.getMonth() + 1,
            year: d.getFullYear()
        }
        console.warn('req body current month --->', reqBody);
        const currentMonthSales = await callAPI.axiosCall(Config.getSalesReportShop, reqBody);
        console.warn('report response current month', currentMonthSales);
        if (currentMonthSales) {
            if (currentMonthSales.data.status == 0) {
                if (currentMonthSales.data.shopConfirmedOrderDeliveredCountDetails.length > 0 && currentMonthSales.data.totalEarningDetails.length > 0) {
                    await this.setState({
                        currentMonthOrders: currentMonthSales.data.shopConfirmedOrderDeliveredCountDetails[0].shopConfirmedOrderDeliveredCount,
                        currentMonthSale: currentMonthSales.data.totalEarningDetails[0].totalEarning
                    });
                }
                
            }
        }
        console.warn('currentMonthSale, currentMonthOrders', this.state.currentMonthSale, this.state.currentMonthOrders)
        const reqBodyLastMonth = {
            shopId: this.props.userData.userData.shopId,
            month: d.getMonth(),
            year: d.getFullYear()
        };
        console.warn('req body last month--->', reqBodyLastMonth);
        const lastMonthSales = await callAPI.axiosCall(Config.getSalesReportShop, reqBodyLastMonth);
        console.warn('report response last month', lastMonthSales);
        if (lastMonthSales) {
            if (lastMonthSales.data.status == 0) {
                if (lastMonthSales.data.shopConfirmedOrderDeliveredCountDetails.length > 0 && lastMonthSales.data.totalEarningDetails.length > 0) {
                    this.setState({
                        lastMonthOrders: lastMonthSales.data.shopConfirmedOrderDeliveredCountDetails[0].shopConfirmedOrderDeliveredCount,
                        lastMonthSale: lastMonthSales.data.totalEarningDetails[0].totalEarning
                    });
                }
            }
        }
        console.warn('herre==============================')
        this.getShopDetails();
        this.checkCart();

        this.props.navigation.addListener('willFocus', () =>{
            console.warn('am in focus Search');
            this.props.navigationAction.shopNavigateAction('ShopHome'); 
        });
        // if (orderDetails) {s

        // }
    }

    

    checkCart = () => {
        console.warn('in check cart', this.props.cartItem.cartItem);
        try {
          if(this.props.cartItem.cartItem){
            if (this.props.cartItem.cartItem.length > 0) {
              let counter = 0;
              let cartSum = 0;
              for (let item of this.props.cartItem.cartItem) {
                cartSum = parseFloat(cartSum) + parseFloat(item.totalCost);
                counter = parseInt(counter) + parseInt(item.counter);
              }     
              this.props.cartModalAction.changeCartState(true,cartSum,counter);
              this.setState({paddingBottomHeight: 80});
            }
          }
        } catch(error) {
          
        }
    }

    getShopDetails = async() => {
        const reqBody = {
            userId: this.props.userData.userData.shopId,
            roleId: this.props.userData.userData.roleDetails[0].roleId,
        };
        const shopDetails = await callAPI.axiosCall(Config.fetchUserData, reqBody);
        console.warn('ShopDetails ===---->', shopDetails.data);
        if (shopDetails) {
            if(shopDetails.data.status == 0) {
                console.warn('ShopDetails from Home API---->', shopDetails.data);
                this.props.actions.loginSuccess(shopDetails.data);
                this.props.walletAction.updateWallet(shopDetails.data.wallet, true);
                if (shopDetails.data.shopCartDetails.length > 0) {
                    if (shopDetails.data.shopCartDetails[0].isUpdated) {
                        if (shopDetails.data.shopCartDetails[0].cart) {
                            this.props.addCartActions.replaceCart(shopDetails.data.shopCartDetails[0].cart);
                        } else {
                            this.props.addCartActions.replaceCart([]);
                        }
                        this.checkCart();
                        
                    }
                }
                 
            }
        }
        this.updateCartBasket();
    }

    updateCartBasket = async() => {
        console.warn('in update basket method');
        const reqBody = {
          shopId: this.props.userData.userData.shopId,
          cart: this.props.cartItem.cartItem
        }
        const test = await callAPI.axiosCall(Config.updateCartShop, reqBody);
        console.warn('rsponse update basket--->', test);
    }

    render() {
    const {navigate} = this.props.navigation;
        return (
            <Fragment>
                <SafeAreaView style={{flex: 0, backgroundColor: '#f1f1f1'}}></SafeAreaView>
                <SafeAreaView style={{flex: 1, backgroundColor: '#332b5c'}}>
                <View style={[s.bodyGray, {flex: 1}]}>
                    <View style={{paddingTop: 10, paddingHorizontal: 15, flexDirection:"row", height: 55}}>
                        <TouchableOpacity style={{flex: 6}} onPress={() => navigate('shopProfilePage')}>
                            <View>
                                <View style={{flexDirection: "row", backgroundColor: "white", padding: 5, borderBottomRightRadius: 100, borderTopLeftRadius: 100, borderBottomLeftRadius: 100}}>
                                <View >
                                    <Image style={{height: 35, width: 25}} source={require('../../../assets/Logo.png')}/>
                                </View>
                                <View style={{paddingLeft: 5}}>
                                    <Text style={[s.normalText,{fontSize: 12}]}>Hello,</Text>
                                    <Text style={[s.normalText,{fontSize: 12}]} numberOfLines={2}>{this.props.userData.userData.shopDetails[0].shopName}</Text>
                                </View>
                                </View>
                            </View>
                        </TouchableOpacity>
                        <View style={{flex: 6, flexDirection:"row"}} >
                            <TouchableOpacity style={{paddingRight: 10, flex: 9, alignItems:"flex-end"}} onPress={() => navigate('walletPage', {id: this.props.userData.userData.shopId, roleName: this.props.userData.userData.roleDetails[0].roleName})}>
                                <View style={{flexDirection: "row"}}>
                                    <View style={{paddingRight: 5, alignItems:"flex-end", paddingTop: 5}}>
                                        <Text numberOfLines={2} style={[s.subHeaderText,{color:"#00CCFF"}]}>₹ {this.props.walletBalance.walletBalance.toFixed(2)}</Text>
                                    </View>
                                    <View style={{height: 35, width: 35}}>
                                        <Image style={styles.imageView} source={require('../../../assets/wallet.png')}/>
                                    </View>  
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={[{marginHorizontal: 15, flexDirection: "row", justifyContent: "center"}]}>
                        <View style={[styles.card, {marginRight: 15}]}>
                            <Text style={styles.saleTextFont}>{this.state.currentMonthSale.toFixed(2)}</Text>
                            <Text style={[s.headerText, {textAlign:"center"}]}>{this.state.currentMonth + ' ' + this.state.currentYear}</Text>
                            <Text style={s.subHeaderText}>Sale</Text>
                        </View>
                        <View style={styles.card}>
                            <Text style={styles.saleTextFont}>{this.state.lastMonthSale.toFixed(2)}</Text>
                            <Text style={[s.headerText, {textAlign:"center"}]}>{this.state.lastMonth + ' ' +  this.state.currentYear}</Text>
                            <Text style={s.subHeaderText}>Sale</Text>
                        </View>
                    </View>
                    <View style={[{marginHorizontal: 15, flexDirection: "row", justifyContent: "center"}]}>
                        <View style={[styles.card, {marginRight: 15}]}>
                            <Text style={styles.saleTextFont}>{this.state.currentMonthOrders}</Text>
                            <Text style={[s.headerText, {textAlign:"center"}]}>{this.state.currentMonth + ' ' + this.state.currentYear}</Text>
                            <Text style={[s.subHeaderText, {textAlign:"center"}]}>Order delivered</Text>
                        </View>
                        <View style={styles.card}>
                            <Text style={styles.saleTextFont}>{this.state.lastMonthOrders}</Text>
                            <Text style={[s.headerText, {textAlign:"center"}]}>{this.state.lastMonth + ' ' + this.state.currentYear}</Text>
                            <Text style={[s.subHeaderText, {textAlign:"center"}]}>Order delivered</Text>
                        </View>
                    </View>
                    
                </View>
                <CartModal navigate={navigate} navigationPage={'shopOrderDetailsPage'}></CartModal>
                <ShopNavigationBar navigate={navigate}></ShopNavigationBar>  
            </SafeAreaView>
            </Fragment>
        )
    }

}


let ScreenWidth = Dimensions.get("window").width;
let ScreenHeight = Dimensions.get("window").height;

const styles = StyleSheet.create({
    card: {
        backgroundColor: "white",
        elevation: 4,
        padding: 20,
        marginVertical: 15,
        alignItems: "center",
        justifyContent: "center",
        width: (ScreenWidth / 2) - 30,
        borderRadius: 20
    },
    saleTextFont: {
        color: '#131E3A',
        fontSize: 24,
        fontWeight: "bold",
        fontFamily: "Verdana",
        color: "#00CCFF",
        marginBottom: 15,
    },
    cardContent: {
        flex: 1,
        flexDirection: "row",
        justifyContent: "space-between",
        flexWrap:"wrap"
    },
    imageView: {
        flex: 1,
        width: undefined,
        height: undefined,
        resizeMode: 'contain',
    },
});

//--------------------------------------------------- store related fuction  -- Start

const mapStateToProps = (state) => {
	return {
        walletBalance: state.walletBalance,
        userData: state.loginData,
        cartItem: state.cartItem,
	}
}

const mapDispatchToProps = (dispatch) => {
    return {
      navigationAction: bindActionCreators(navigateAction,dispatch),
      actions: bindActionCreators(loginSuccess,dispatch),
      walletAction: bindActionCreators(walletAction,dispatch),
      cartModalAction: bindActionCreators(cartModalAction,dispatch),
      addCartActions: bindActionCreators(addToCart, dispatch),
    };
}

// ---------------------------------------------------- store related function  -- END

export default connect(mapStateToProps, mapDispatchToProps)(ShopHomePage);