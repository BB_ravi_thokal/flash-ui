/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import axios from 'axios';
import * as Config from '../../Constant/Config.js';
import * as Constant from '../../Constant/Constant.js';
import  s  from '../../sharedStyle.js'
// import Paytm from 'react-native-paytm';
import * as navigateAction  from '../../Actions/NavigationBarAction';
import ShopNavigationBar from '../ReusableComponent/ShopNavigationBar.js'; 
import { NavigationActions, StackActions, CommonActions } from 'react-navigation';
// import 'bootstrap/dist/css/bootstrap.min.css';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  Dimensions,  
  Image,
  TouchableOpacity,
  FlatList ,
  Platform,
  ToastAndroid,
  modal,
  Modal
} from 'react-native';
import Toast from 'react-native-simple-toast';
import AnimatedLoader from "react-native-animated-loader";
import callAPI from '../../CoreFunctions/callAPI';


OrderListShop = ({data, self}) => {
    return (
        <View style={{marginHorizontal: 15, marginBottom: 10}}>
            <TouchableOpacity style={styles.card} onPress={() => self.enableDetailsView(data)}>
                <Text style={s.subHeaderText}>Order Id: {data.orderId}</Text>
                <Text style={[s.normalText, {marginTop: 10}]}>Customer Name: {data.order.userDetails.fullName}</Text>
            </TouchableOpacity>
        </View>
    )
}

class ActiveOrderShop extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            activeOrderList: [],
            showLoader: false,
        }
    }

    async componentDidMount() {
        this.props.navigationAction.shopNavigateAction('ShopOngoing'); 
        this.initializeOrders();
        this.props.navigation.addListener('willFocus', () =>{
            this.props.navigationAction.shopNavigateAction('ShopOngoing'); 
        });
    }

    async initializeOrders() {
        this.setState({showLoader: true});
        // this.props.navigationAction.shopNavigateAction('ShopOngoing'); 
        const reqBody = {
            shopId : this.props.userData.userData.shopId
        };
        const orderDetails = await callAPI.axiosCall(Config.getOrderDetailsForShop, reqBody);
        this.setState({showLoader: false});
        if (orderDetails) {
            if (orderDetails.data.currentOrder.length > 0) {
                // for (const orderObj of orderDetails.data.currentOrder) {
                //     console.warn('order Obj--->', orderObj.order.itemList);
                //     orderObj.order.itemList = orderObj.order.itemList.map( item => {
                //         if (item.isProductAvailable == false) {
                //           item.backgroundColor = "red"   
                //         } else {
                //           item.backgroundColor = "white" 
                //         }
                //         return item;
                //     })
                // }
                console.warn('after map');
                // if (orderDetails.data.currentOrder.length > 1) {
                // console.warn('in list view--->', orderDetails.data.currentOrder[0].order.userDetails);
                this.setState({
                    activeOrderList: orderDetails.data.currentOrder
                });
                // } else {
                    
                //     // console.warn('length', this.props.navigation);
                //     // this.props.navigation.dispatch({
                //     //     ...CommonActions.navigate('activeOrderDetailsShop'),
                //     //     source: 'shopHomePage',
                //     //     target: 'activeOrderDetailsShop',
                //     //   });
                //     // const resetAction = StackActions.reset({
                //     //     index: 0,
                //     //     actions: [NavigationActions.navigate({ routeName: 'activeOrderDetailsShop'})]
                  
                //     //   })
                //     //   this.props.navigation.dispatch(resetAction);
                //     this.props.navigation.navigate('activeOrderDetailsShop', {'orderDetails': orderDetails.data.currentOrder[0]});
                // }
            }
        } else {
            this.setState({showLoader: false});
        }
    }

    // componentWillReceiveProps() {
    //     if (this.props.selectedIcon.shopSelectedIcon == 'ShopOngoing') {
    //         console.warn('inside what I want')
    //         this.initializeOrders();
    //     }
    // }

    ActiveOrderListView = () => {
        return (
            <View style={{marginTop: 15}}>
                <FlatList
                    data={this.state.activeOrderList}
                    renderItem={( { item } ) => (
                        <OrderListShop
                            data={item}
                            self={this}
                        />
                    )}
                    extraData={this.state} 
                />
            </View>
        )
    }


    render() {
        const {navigate} = this.props.navigation;
        if (this.state.isActiveOrder) {
            return (
                <Fragment>
                    <SafeAreaView style={{flex: 0, backgroundColor: '#f1f1f1'}}></SafeAreaView>
                    <SafeAreaView style={{flex: 1, backgroundColor: '#332b5c'}}>
                        <View style={[s.bodyGray, {alignItems: "center", justifyContent:"center"}]}>
                            <Text style={s.subHeaderText} numberOfLines={5}>No active order for your shop.</Text>
                            <Text style={s.subHeaderText}>Stay tuned!!!</Text>
                        </View>
                        <ShopNavigationBar navigate={navigate}></ShopNavigationBar>  
                    </SafeAreaView>   
                </Fragment> 
            )
        } else {
            return (
                <Fragment>
                    <SafeAreaView style={{flex: 0, backgroundColor: '#f1f1f1'}}></SafeAreaView>
                    <SafeAreaView style={{flex: 1, backgroundColor: '#332b5c'}}>
                        <View style={[s.bodyGray, {flex: 1}]}>
                            <AnimatedLoader
                                visible={this.state.showLoader}
                                overlayColor="rgba(255,255,255,0.75)"
                                source={require("../../../assets/loader.json")}
                                animationStyle= {[styles.lottie,{height: 150,width: 150}]}
                                speed={1}
                            />
                            {this.state.activeOrderList.length > 0
                            ?
                                <this.ActiveOrderListView />    
                            :
                            <View style={{flex: 1, justifyContent:"center"}}>
                                <Text style={[s.normalText, {textAlign: "center", justifyContent:"center"}]} numberOfLines={5}>No active orders available</Text>        
                            </View>
                            
                            }
                        </View>
                        <ShopNavigationBar navigate={navigate}></ShopNavigationBar>  
                    </SafeAreaView>
                </Fragment>
            );
        }
    }

    enableDetailsView = (data) => {
        console.warn('in enable details method--->', data);
        this.props.navigation.navigate('activeOrderDetailsShop', {'orderDetails': data});
    }
};

let ScreenHeight = Dimensions.get("window").height;
let ScreenWidth = Dimensions.get("window").width;

const styles = StyleSheet.create({
    card: {
        backgroundColor: "white",
        padding: 15,
    },
    prodcutListingView: {
        flexDirection: 'row',
        marginVertical: 5,
        backgroundColor: "white",
        padding: 10
    },
    addCardImage: {
        width: 80,
        height: 80,
        flex: 4
    },
    productImage: {
        width: 80,
        height: 80
    },
    stores: {
        paddingHorizontal: 10,
        flex: 8,
        flexDirection: "column"
    },
    Mystores: {
        flexDirection: "row",
        alignItems: "flex-start",
        justifyContent: "space-between",
        textAlignVertical: "bottom",
        flex: 2
    },
});



//--------------------------------------------------- store related fuction  -- Start

const mapStateToProps = (state) => {
	return {
        userData: state.loginData,
        selectedIcon: state.selectedIcon,
	}
}

const mapDispatchToProps = (dispatch) => {
    return {
        navigationAction: bindActionCreators(navigateAction,dispatch),
    };
}

// ---------------------------------------------------- store related function  -- END


export default connect(mapStateToProps, mapDispatchToProps)(ActiveOrderShop);
