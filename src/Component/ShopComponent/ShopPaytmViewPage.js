/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Fragment } from 'react';
import * as walletAction  from '../../Actions/WalletAction';
import * as navigateAction  from '../../Actions/NavigationBarAction';
import * as GetPackageAction  from '../../Actions/GetPackageAction';
import * as cartModalAction  from '../../Actions/CartModalAction';
import * as addToCart  from '../../Actions/CartItem';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  SafeAreaView,
  StyleSheet,
} from 'react-native';
import { WebView } from 'react-native-webview';
import Toast from 'react-native-simple-toast';

class ShopPaytmViewPage extends React.Component{
  
    constructor(props){
        super(props);
        console.warn('this.props.navigation.state.params.paytmUrl', this.props.navigation.state.params.paytmUrl);
    }

    handleNavigation = (data) => {
        console.warn('response--->', data);
        let msg;
        if (data.title) {
            if (data.title.startsWith('success')) {
                this.paytmResponse(data.title, msg);   
            } else {
                switch (data.title) {
                    case '1': 
                        msg = 'Not able to proceed with payment gateway. Please contact admin.';
                        this.paytmResponse(false, msg);
                        break;
                    case '2': 
                        msg = 'Not able to proceed with payment gateway. Please contact admin.';
                        this.paytmResponse(false, msg);
                        break;
                    case 'cancelled': 
                        msg = 'You cancelled the transaction';
                        this.paytmResponse(false, msg);
                        break;
                    case 'success': 
                        msg = 'Your payment is completed';
                        this.paytmResponse(data.title, msg);
                        break;
                    case 'notFound': 
                        msg = 'Not able communicate with paytm gateway. Please try later';
                        this.paytmResponse(false, msg);
                        break;
                    case 'error': 
                        msg = 'Not able communicate with paytm gateway. Please try later';
                        this.paytmResponse(false, msg);
                        break;
                    default:
                        console.warn('response in default-->', data.title);
                        // msg = 'Sorry, Not able to place your order please try again';
                        // this.paytmResponse(false, msg);
                        break;
                }
            }
            
        }

    }

    paytmResponse = (status, message) => {
        console.warn('response from paytm gateway IOS', status);
        Toast.show(message, Toast.LONG);
        if (status) {
        //   let index = status.indexOf("-");
        //   if (index >= 0) {
        //     console('wallet amount--->', index, status.splice(index, string.length - 1));
        //     this.props.walletAction.updateWallet(status.splice(index, string.length - 1), true);
        //   }
            Toast.show(message, Toast.LONG); 
            this.postPayment('success');
        } else {
            Toast.show(message, Toast.LONG);
            this.postPayment('fail');
        }
    }

    postPayment = (status) => {
        // if (status == 'success') {
        //     this.manageStore('home');
        // } else {
        //     this.props.navigationAction.navigateAction('ShopHome');
        //     this.props.navigation.navigate('shopHomePage');
        // }
        this.props.navigationAction.navigateAction('ShopHome');
        this.props.navigation.navigate('shopHomePage');
    }


    render() {
        return (
            <SafeAreaView style={{flex: 1}}>
                <WebView
                    useWebKit={true}
                    // source={{ uri: 'https://www.cricbuzz.com/live-cricket-scores/30330/mi-vs-csk-1st-match-indian-premier-league-2020' }}
                    source={{ uri: this.props.navigation.state.params.paytmUrl }}
                    onNavigationStateChange={data => this.handleNavigation(data)}
                />
            </SafeAreaView>
        )
    }
}


const styles = StyleSheet.create({
 
});

const mapStateToProps = (state) => {
	return {
        walletBalance: state.walletBalance,
        cartItem: state.cartItem,
        cartState: state.cartState,
	}
}

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(addToCart, dispatch),
        GetPackageAction: bindActionCreators(GetPackageAction,dispatch),
        cartModalAction: bindActionCreators(cartModalAction,dispatch),
        navigationAction: bindActionCreators(navigateAction,dispatch),
        walletAction: bindActionCreators(walletAction,dispatch),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(ShopPaytmViewPage);