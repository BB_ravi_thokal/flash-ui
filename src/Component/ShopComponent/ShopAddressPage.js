import React, { Component } from 'react';
import { SafeAreaView, Text, View, ActivityIndicator, Button, StyleSheet,Dimensions, TextInput,
  TouchableOpacity, ToastAndroid, KeyboardAvoidingView } from 'react-native';
import MapView from "react-native-maps";
import Geolocation from '@react-native-community/geolocation';
import { connect } from 'react-redux';
import * as loginSuccess  from '../../Actions/LoginAction';
import * as activityAction  from '../../Actions/ActivityAction';
import * as getPackageAction  from '../../Actions/GetPackageAction';
import { bindActionCreators } from 'redux';
import  s  from '../../sharedStyle.js';
import { ScrollView } from 'react-native-gesture-handler';
import * as navigateAction  from '../../Actions/NavigationBarAction';
import axios from 'axios'; 
import * as Config from '../../Constant/Config.js';
import AnimatedLoader from "react-native-animated-loader";
import DelayInput from "react-native-debounce-input";
import Toast from 'react-native-simple-toast';
import callAPI from '../../CoreFunctions/callAPI';

const {width, height} = Dimensions.get("window")
const ASPECT_RATIO = width / height;
const LATITUDE = 37.78825;
const LONGITUDE = -122.4324;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

class ShopAddressPage extends React.Component{
  constructor(props) {
    super(props);
    this.state = {
        loading: true,
      region: {
        latitude: LATITUDE,
        longitude: LONGITUDE,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA
      },
      isMapReady: false,
      marginTop: 1,
      userLocation: "",
      regionChangeProgress: false,
      addressId: "",
      flatNumber: "",
      flatNoBorder: 0,
      flatNoColor: "white",
      addressLine1: "",
      address1Border: 0,
      address1Color: "white",
      addressLine2: "",
      address2Border: 0,
      address2Color: "white",
      addressState: "Maharashtra",
      addressStateBorder: 0,
      addressStateColor: "white",
      district: "",
      districtBorder: 0,
      districtColor: "white",
      pinCode: "",
      pinCodeBorder: 0,
      pinCodeColor: "white",
      isSelected: false,
      backGroundColor: "white",
      isFormValid: true,
      fetchAddress: [],
      cityData: [ {value:'Navi Mumbai'},{value:'Panvel'},{value:'Kharghar'},{value:'Mumbai'}]
    };
  }

  componentDidMount() {
    console.warn('this.userData--->', this.props.userData.userData);
    if(this.props.navigation.state.params.isUpdate) {
        console.warn("inside edit -----", this.props.userData.userData.shopDetails[0].shopLocation);
        this.setState({
          loading: false,
        });
          // const index = this.props.getPackageData.pickupAddress.findIndex(ind => ind.addressId === this.props.navigation.state.params.addressId);
        this.setState({
            flatNumber: this.props.userData.userData.shopDetails[0].shopNo,
            addressLine2: this.props.userData.userData.shopDetails[0].landmark,
            region: this.props.userData.userData.shopDetails[0].shopLocation,
        });
    } else {
        console.warn('in else');
        this.getCurrentLocation();
    }
    
  }


  getCurrentLocation() {
      console.warn('in get current loc');
    Geolocation.getCurrentPosition(
      (position) => {
        const region = {
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
          latitudeDelta: LATITUDE_DELTA,
          longitudeDelta: LONGITUDE_DELTA
        };
        this.setState({
          region: region,
          loading: false,
          error: null,
        });
      },
      (error) => {
        if (error.PERMISSION_DENIED === 1) {
          this.setState({showLoader: false});
          Toast.show("You will not get the list of your local shops. If you deny the location access", Toast.LONG);
        }
      },
      { enableHighAccuracy: false, timeout: 200000, maximumAge: 5000 },
    );
}

onMapReady = () => {
  this.setState({ isMapReady: true, marginTop: 0 });
}

// Fetch location details as a JOSN from google map API AIzaSyB9Ak6KilFqRJsdGnWV43NK6TTMbEETicY
fetchAddress = () => {
  console.warn('this.state.region.latitude', JSON.stringify(this.state.region));  
  fetch("https://maps.googleapis.com/maps/api/geocode/json?address=" + this.state.region.latitude + "," + this.state.region.longitude + "&key=" + "AIzaSyCUREttpI8l0AgHrxXAJ8_NhP7XntJTNUU")
    .then((response) => response.json())
    .then((responseJson) => {
      
      const userLocation = responseJson.results[0].formatted_address;
      
      console.warn("responseJson------------->", userLocation)
      this.setState({
        userLocation: userLocation,
        fetchAddress: responseJson.results[0].address_components,
        regionChangeProgress: false
      });
    });
}

// Update state on region change
onRegionChange = async(region) => {
  console.warn('onRegionChange', region);
  await this.setState({
    // region: region,
    regionChangeProgress: true
  }, () => this.fetchAddress());
}

fetchLatLong = async(text) => {
  if(text.length > 4) {
    console.warn('fetchLatLong--->', text);
    this.setState({ isMapReady: false, marginTop: 0 });
    fetch("https://maps.googleapis.com/maps/api/geocode/json?address=" + text + "&key=" + "AIzaSyCUREttpI8l0AgHrxXAJ8_NhP7XntJTNUU")
    .then(async(response) => response.json())
    .then(async(responseJson) => {
      
      const lat = responseJson.results[0].geometry.location.lat;
      const lng = responseJson.results[0].geometry.location.lng;
      console.warn('lat--- lag', lat, lng);
      const region = {
        latitude: lat,
        longitude: lng,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA
      };
      await this.setState({region: region, isMapReady: false, marginTop: 0});

      console.warn('refion in fetchLatLong', region);
      // this.setState({
      //   region: region,
      //   regionChangeProgress: true
      // }, () => this.fetchAddress(true));
    this.onRegionChange(region);

      
    });
  }
}

// Action to be taken after select location button click
onLocationSelect = () => alert(this.state.userLocation);

    render() {
        if (this.state.loading) {
        return (
            <SafeAreaView style={{flex: 1}}>
            {/* <SceneLoader
                visible={this.state.showLoader}
                animation={{
                    fade: {timing: {duration: 1000 }}
                }}
            /> */}
                <AnimatedLoader
                visible={this.state.loading}
                overlayColor="rgba(255,255,255,0.75)"
                source={require("../../../assets/loader.json")}
                animationStyle= {[styles.lottie,{height: 150,width: 150}]}
                speed={1}
            />
            </SafeAreaView>
            
        );
        } else {
        return (
            <SafeAreaView style={{flex: 1}}>
            {/* <KeyboardAvoidingView behavior="padding" style={{flex: 1}}> */}
            <View style={s.bodyGray}>
            
                <View style={{ flex: 4.5 }}>
                {this.state.region.latitude && this.state.region.longitude &&
                    <MapView
                    // provider={PROVIDER_GOOGLE}
                    style={styles.map}
                    region={this.state.region}
                    showsUserLocation={false}
                    onMapReady={this.onMapReady}
                    onRegionChangeComplete={this.onRegionChange}
                    >
                    <MapView.Marker
                        coordinate={{ "latitude": this.state.region.latitude, "longitude": this.state.region.longitude }}
                        title={"Your Location"}
                        draggable
                    />
                    </MapView>
                }

                {/* <View style={styles.mapMarkerContainer}>
                    <Text style={{ fontSize: 42, color: "#ad1f1f" }}>&#xf041;</Text>
                </View> */}
                </View>
                
                <View style={{flex: 6.5, paddingHorizontal:15}} behavior="padding">
                
                  <ScrollView showsVerticalScrollIndicator={false}>
                      <Text style={[s.normalText,{marginBottom: 10, color:"silver"}]}>Move map for location</Text>
                      <View style={{paddingVertical: 5}}>
                      <Text style={[s.subHeaderText,{paddingBottom: 5}]}>Track location</Text>
                      <Text numberOfLines={4} style={s.normalText}>
                      {!this.state.regionChangeProgress ? this.state.userLocation : "Identifying Location..."}</Text>
                      {/* <TextInput 
                      style={[styles.inputBox,s.normalText,{elevation: 4}]}
                      underlineColorAndroid="transparent"
                      numberOfLines={2}
                      value={!this.state.regionChangeProgress ? this.state.userLocation : "Identifying Location..."}

                      /> */}
                      </View>
                      <View style={{paddingVertical: 5}}>
                      <Text style={[s.subHeaderText,{paddingBottom: 5}]}>Search :</Text>
                      {/* <TextInput 
                          style={[styles.inputBox,s.normalText,{elevation: 4}]}
                          // value={this.st}
                          onChangeText={(text) => {
                          this.fetchLatLong(text)
                          }}
                          underlineColorAndroid="transparent"
                      />  */}
                      <DelayInput placeholder = {this.state.placeholderText}
                        delayTimeout={500}
                        minLength={4}
                        style={styles.inputBox} inlineImageLeft='searchicon' 
                        value = {this.state.searchText}
                        onChangeText={(searchText) => this.fetchLatLong(searchText)}
                      /> 
                      </View>
                      
                      {/* <Text numberOfLines={2} style={{ fontSize: 14, paddingVertical: 10, borderBottomColor: "silver", borderBottomWidth: 0.5 }}>
                      {!this.state.regionChangeProgress ? this.state.userLocation : "Identifying Location..."}</Text> */}
                      <View style={{paddingVertical: 5}}>
                      <Text style={[s.subHeaderText,{paddingBottom: 5}]}>Shop / Block No:</Text>
                      <TextInput 
                          style={[styles.inputBox,s.normalText,{elevation: 4, borderWidth: this.state.flatNoBorder, borderColor: this.state.flatNoColor}]}
                          value={this.state.flatNumber}
                          onChangeText={(flatNumber) => {
                          this.setState({
                          flatNumber: flatNumber,
                          isFormValid: true,
                          flatNoBorder: 0,
                          flatNoColor: "white"
                          })}}
                          underlineColorAndroid="transparent"
                      /> 
                      </View>  
                      <View style={{paddingVertical: 5}}>
                      <Text style={[s.subHeaderText,{paddingBottom: 5}]}>Landmark:-</Text>
                      <TextInput 
                          style={[styles.inputBox,s.normalText,{elevation: 4,borderWidth: this.state.address2Border, borderColor: this.state.address2Color}]}
                          value={this.state.addressLine2}
                          onChangeText={(addressLine2) => {
                          this.setState({
                          addressLine2: addressLine2,
                          isFormValid: true,
                          address2Border: 0,
                          address2Color: "white"
                          })}}
                          underlineColorAndroid="transparent"
                      /> 
                      </View> 
                      
                      {/* <View style={styles.btnContainer}>
                      <Button
                          title="PICK THIS LOCATION"
                          disabled={this.state.regionChangeProgress}
                          onPress={this.onLocationSelect}
                      >
                      </Button>
                      </View> */}
                  </ScrollView> 
                </View>
                <View style={{flex:1}}>
                <TouchableOpacity style={{flex:6,backgroundColor:"#00CCFF",alignItems:"center",justifyContent:"center"}}
                    onPress={() => this.submitForm()}>
                    <Text style={[s.headerText,{color:"white"}]}>Save</Text>
                </TouchableOpacity>
                </View>
            </View>
            
            {/* </KeyboardAvoidingView> */}
            </SafeAreaView>
        );
        }
    }

    submitForm = () => {
        console.warn('in submit form');
        // let postalCode = '';

        this.state.fetchAddress.forEach(element => {
        if(element.types == 'postal_code') {
            console.warn('inside postal cond', element.short_name);
            postalCode = element.short_name
        }
        })
        if (postalCode) {
        console.warn('postal code -->', postalCode);
        console.warn('indexOf result --->', this.props.userData.userData.pinCode);
        if (this.props.userData.userData.pinCode.indexOf(postalCode) >= 0) {
            this.validateForm();
        } else {
            Toast.show('We are currently operating only in Navi Mumbai areas. If you want to register shop at this location please contact admin',Toast.LONG);
        }
        } else {
            Toast.show('We are currently operating only in Navi Mumbai areas. If you want to register shop at this location please contact admin',Toast.LONG);
        }
    }

    validateForm = () => {
        if(this.state.flatNumber == "") {
        this.setState({
            isFormValid: false,
            flatNoBorder: 1,
            flatNoColor: "red"
        })
        Toast.show('Please fill flat number field',Toast.LONG);
        }  else if(this.state.addressLine2 == "") {
        this.setState({
            isFormValid: false,
            address2Border: 1,
            address2Color: "red"
        })
        Toast.show('Please fill address',Toast.LONG);
        } else if(this.state.isFormValid) {
        this.saveAddress();
        }
    }

    saveAddress = async() => {
        console.warn('in save Address');
        const reqBody = {
            shopId: this.props.userData.userData.shopId,
            shopNo: this.state.flatNumber,
            shopAdress: this.state.userLocation,
            landmark: this.state.addressLine2,
            shopLocation: this.state.region,
            loc: [this.state.region.latitude, this.state.region.longitude]
        }
        console.warn('reqBody ===>', reqBody);
        await this.setState({loading: true});
        const updateShopDetails = await callAPI.axiosCall(Config.updateShopDetails, reqBody);
        await this.setState({loading: false});
        console.warn('updateShopDetails', updateShopDetails);
        if (updateShopDetails) {
            if (updateShopDetails.data.status == 0) {
                this.props.userData.userData.shopDetails[0].shopLocation = this.state.region;
                this.props.userData.userData.shopDetails[0].shopNo = this.state.flatNumber;
                this.props.userData.userData.shopDetails[0].landmark = this.state.addressLine2;
                this.props.userData.userData.shopDetails[0].shopAdress = this.state.userLocation;
                await this.props.actions.loginSuccess(this.props.userData.userData);
                // Toast.show(updateShopDetails.data.message, Toast.LONG);
                Toast.show('Address updated successfully', Toast.LONG);
                if (this.props.navigation.state.params.callingPage == 'shopOrderDetails') {
                    this.props.navigationAction.shopNavigateAction('');
                    this.props.navigation.navigate('shopOrderDetailsPage');
                } else if (this.props.navigation.state.params.callingPage == 'shopProfile'){
                    this.props.navigationAction.shopNavigateAction('Profile');
                    this.props.navigation.navigate('shopProfilePage');
                }
            } else {
                // Toast.show(updateShopDetails.data.message, Toast.LONG);
                Toast.show('NOt able to update the address. Please try again', Toast.LONG);
            }
        }
    }

}

const styles = StyleSheet.create({
  map: {
    ...StyleSheet.absoluteFillObject
  },
  inputBox: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    borderRadius: 10,
    width: '100%',
    height: 40,
    paddingHorizontal: 15
  },
  mapMarkerContainer: {
    left: '47%',
    position: 'absolute',
    top: '42%'
  },
  mapMarker: {
    fontSize: 40,
    color: "red"
  },
  deatilSection: {
    flex: 6,

  },
  spinnerView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  btnContainer: {
    width: Dimensions.get("window").width - 20,
    position: "absolute",
    bottom: 100,
    left: 10
  }
});


//--------------------------------------------------- store related fuction  -- Start

const mapStateToProps = (state) => {
	return {
    userData: state.loginData,
    activityData: state.activityData,
    getPackageData: state.getPackageData,
	};
}

const mapDispatchToProps = (dispatch) => {
    return {
      actions: bindActionCreators(loginSuccess,dispatch),
      activityAction: bindActionCreators(activityAction,dispatch),
      getPackageAction: bindActionCreators(getPackageAction,dispatch),
      navigationAction: bindActionCreators(navigateAction,dispatch),

      //cartActions: bindActionCreators(addToCart,dispatch),
    };
}

// ---------------------------------------------------- store related function  -- END


export default connect(mapStateToProps, mapDispatchToProps)(ShopAddressPage);