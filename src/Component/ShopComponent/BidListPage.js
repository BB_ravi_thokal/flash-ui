/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import axios from 'axios';
import * as Config from '../../Constant/Config.js';
import * as Constant from '../../Constant/Constant.js';
import NavigationBar from '../ReusableComponent/NavigationBar.js'
import  s  from '../../sharedStyle.js'
import InternateModal from '../ReusableComponent/InternetModal.js';
import ShopNavigationBar from '../ReusableComponent/ShopNavigationBar.js'; 
import * as navigateAction  from '../../Actions/NavigationBarAction.js';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  Dimensions,
  TouchableOpacity,
  TextInput,
  Image,
  ToastAndroid,
  FlatList,
} from 'react-native';
import Toast from 'react-native-simple-toast';

class BidListPage extends React.Component{

  constructor(props) {
    super(props);
    this.state={
        activeBids: []
    }
  }


  ActiveBidListing = (item) => {
    const {navigate} = this.props.navigation;
    if(this.state.activeBids.length > 0){
      return (
        <TouchableOpacity style={[styles.prodcutListingView]} onPress={() => navigate('bidDetailsPage', {bidId: item.item.bidId, userId: item.item.userId})}>
            <View style={{flex:10}}>
                <Text style={s.headerText} numberOfLines={1}>Bid Id</Text>
                <Text style={s.subHeaderText} numberOfLines={1}>{item.item.bidId}</Text>
            </View>
            
            {/* <View style={[styles.Mystores,{marginVertical: 15}]}> */}
                {/* <View style={styles.flexRow}>
                    <View style={{flex: 2, flexDirection:"row"}}>
                        <Image style={{height: 16, width: 16}} source={require('../../../assets/clock.png')}/>  
                        <Text style={[s.normalText, {paddingLeft: 5}]}>Placed Date: 19/04/1995 </Text>    
                    </View>
                    <View style={{flex: 1, paddingRight: 15, flexDirection:"row"}}>
                        <Image style={{height: 18, width: 18}} source={require('../../../assets/bid.png')}/>  
                        <Text style={[s.normalText, {paddingLeft: 5}]}> 10 Bids </Text>   
                    </View>
                    
                </View> */}
                <View style={{paddingTop: 10, flexDirection:"row"}}>
                    <Image style={{height: 16, width: 16}} source={require('../../../assets/money.jpg')}/>  
                    <Text style={[s.normalText, {paddingLeft: 5}]}>Total cart amount: ₹ {item.item.bidInfo.totalCost}</Text>   
                </View>
            {/* </View> */}
        </TouchableOpacity>
        
      )
    } else {
        return (
            <View style={{flex: 1, paddingHorizontal: 15}}>
                <View style={{alignContent:"center",justifyContent:"center"}}>
                    <Text style={s.headerText}>No active bid</Text>
                    <Text style={s.headerText}>Stay tuned!!!</Text>
                </View>
            </View>
        )
    }
  
  
  }

  componentDidMount(){
    this.props.navigationAction.shopNavigateAction('Bid'); 
    axios.post(
        Config.URL + Config.getAllBidForShop,
        {
        },
        {
          headers: {
              'Content-Type': 'application/json',
          }
        }
    )
    .then(res => {
        console.warn("res---",res.data.result);
        if (res.data.status == 0) {
          this.setState({
            activeBids: res.data.result
          })
          
        }  

      }).catch(err => {
          console.warn(res.data.status);
      });

      this.props.navigation.addListener('willFocus', () =>{
        this.props.navigationAction.shopNavigateAction('Bid');
      });

  }

  render() {
    const {navigate} = this.props.navigation;
      return (
        <SafeAreaView style={{flex: 1}}>
          <View style={{flex: 1}}>
            <View style={[s.body]}>
              {/* <InternateModal /> */}
              <ScrollView showsVerticalScrollIndicator = {false} style={styles.storeDtlContainer}>
                  <FlatList data={this.state.activeBids}
                    renderItem={( { item } ) => (
                        <this.ActiveBidListing
                        item={item}
                        />
                    )}
                    keyExtractor= {item => item.bidId}
                    extraData = {this.state.activeBids}
                  />
                </ScrollView>
            </View>
            <ShopNavigationBar navigate={navigate}></ShopNavigationBar>
          </View>  
        </SafeAreaView>
      )
   
  }

}


let ScreenWidth = Dimensions.get("window").width;
let ScreenHeight = Dimensions.get("window").height;

const styles = StyleSheet.create({
  scrollView: {
    flex: 1
  },
  imgBlank: {
    height: ScreenHeight,
    width: ScreenWidth
  },
  containerIcons:{
    width:30,
    height:30,
  },

  searchShops:{ 
    height: 40, 
    borderColor: '#7285A5', 
    borderWidth: 1,
    paddingHorizontal:10, 
    borderRadius:4 
  },
  sectionContainer: {
    marginTop: 20,
    paddingHorizontal: 24,
  },
  iconView: {
    width: 30,
    height: 30,
    marginRight: 20,
    borderRadius: 100,
  },
  storeCardLayout:{
    marginHorizontal: 15,
    paddingVertical:14,
    borderBottomWidth: 2,
    borderBottomColor: "#131E3A",
    alignItems: "center"
  },
  categoryFooter:{
    width:ScreenWidth,
    paddingBottom:20,
    height:80,
    flexGrow:0.05,
    backgroundColor:'white',
    display:'flex',
    flexDirection:'row',
    justifyContent:"space-evenly",
    alignItems:"center"
  },
  storeDtlContainer: {
    marginHorizontal: 15,
    paddingVertical: 10
  },

  storeContainer:{
    display:"flex",
    flexDirection: "row"
  },
  stores: {
    paddingHorizontal: 10,
    flex: 1,
    flexDirection: "column"
  },

  storeCount: {
    fontSize: 22,
    fontWeight: 'bold',
    paddingHorizontal: 10,
  },
  storeProductHeading: {
    fontWeight: 'bold',
    fontSize: 30,
  },
  shopdtlCards: {
    width: 80,
    height: 80,
    backgroundColor: 'orange',
    borderRadius: 100,
    borderWidth: 0.5,
    borderColor: 'black',
    marginVertical: 5,
    marginHorizontal: 10,
  },
  storeCardDisplay: {
    // padding: 10,
    paddingVertical: 10,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  storeCards: {
    height: 100,
    borderRadius: 4,
    borderWidth: 0.5,
    borderColor: 'black',
    backgroundColor: 'white',
    flex: 3,
  },
  advertiseData: {
    height: 200,
    width: ScreenWidth,
    borderBottomLeftRadius: 90,
    borderBottomRightRadius: 90,
    elevation: 4
  },
  storeHeading: {
    fontSize:24,
    
  },
  storeDesc: {
    alignItems: "center"
  },
  trendingShops: {
    borderBottomWidth: 2,
    borderColor: "#131E3A",
    marginHorizontal: 15,
    paddingTop: 10,
  },
  addCardImage: {
    width: 80,
    height: 80,
    flex: 4
  },
  productImage: {
    width: 80,
    height: 80,
  },
  storeCardButton: {
    backgroundColor: 'white',
    color: 'black',
    width: 30,
    height: 30,
    borderRadius: 50,
    alignItems: "center",
    justifyContent: 'center',
  },
  storeborder: {
    borderBottomColor: 'grey',
    borderBottomWidth: 1,
  },
  flexRow: {
    flex: 1,
    textAlignVertical: "bottom",
    flexDirection: 'row',
  },
  prodcutListingView: {
    marginVertical: 5,
    backgroundColor: "white",
    padding: 10,
    elevation: 6

  },
  flexColumnAlignEnd: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-end'
  },
  storeDtlCenter: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "center"
  },
  Mystores: {
    alignItems: "flex-start",
    justifyContent: "space-between",
    textAlignVertical: "bottom",
    flex: 1
    // paddingBottom:15,
  },
  storeCardSpace: {
    marginBottom: 15,
  },
  headerstyle: {
    paddingBottom: 20,
    paddingTop: 20
  },

  storeBtnViewColor: {
    backgroundColor: '#ba55d3',
    color: 'white',
  },
  continueBtnViewColor: {
    backgroundColor: '#2f4f4f',
    color: 'white',
  },
  addtoCartBtnColor: {
    fontSize:12,
    borderWidth: 1,
    borderColor: '#0080FE',
    marginLeft: 20,
  },
  storeBtn: {
    paddingVertical: 5,
    paddingHorizontal: 5,
    marginBottom: 5,
    color: 'white',
    borderRadius: 0,
    width: 50
  },
  basketBtn:{
    paddingVertical: 5,
    paddingHorizontal: 10,
    marginBottom: 10,
    borderRadius: 20,
    height: 30,
    width: 30
  },
  basketImg: {
    height: 30,
    width: 30,
  }

});

//--------------------------------------------------- store related fuction  -- Start

const mapStateToProps = (state) => {
	return {
	}
}

const mapDispatchToProps = (dispatch) => {
    return {
      navigationAction: bindActionCreators(navigateAction,dispatch),
    };
}

// ---------------------------------------------------- store related function  -- END


export default connect(mapStateToProps, mapDispatchToProps)(BidListPage);

