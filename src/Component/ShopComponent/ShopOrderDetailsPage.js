/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import * as addToCart  from '../../Actions/CartItem';
import { bindActionCreators } from 'redux';
import axios from 'axios';
import * as Config from '../../Constant/Config';
import * as Constant from '../../Constant/Constant';
import NavigationBar from '../ReusableComponent/NavigationBar';
import * as loginSuccess  from '../../Actions/LoginAction';
import * as walletAction  from '../../Actions/WalletAction';
import  s  from '../../sharedStyle'
import PopupModal from '../ReusableComponent/PopupComponent';
import AddressModal from '../ReusableComponent/addressPopUp';
import * as cartModalAction  from '../../Actions/CartModalAction';
import CouponModal from '../ReusableComponent/couponModal';
// import Paytm from 'react-native-paytm';
import * as navigateAction  from '../../Actions/NavigationBarAction';
import { NavigationActions, StackActions } from 'react-navigation';
import { PaytmIntegration } from '../../CoreFunctions/paytmIntegration';
import callAPI from '../../CoreFunctions/callAPI';
import ImageViewer from 'react-native-image-zoom-viewer';
// import 'bootstrap/dist/css/bootstrap.min.css';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  Dimensions,  
  Image,
  TouchableOpacity,
  FlatList ,
  Platform,
  ToastAndroid,
  modal,
  Modal
} from 'react-native';
import Toast from 'react-native-simple-toast';
import InternateModal from '../ReusableComponent/InternetModal';
import AnimatedLoader from "react-native-animated-loader";
import SceneLoader from 'react-native-scene-loader';
import * as couponAction  from '../../Actions/CouponAction';

class ShopOrderDetailsPage extends React.Component{
  constructor(props) {
    super(props);
    this.state={
      // cartProduct: this.props.cartItem.cartItem,
      cartTotal: 0.00,
      cartTotalExlGst: 0.00,
      totalPayable: 0.00,
      modalVisible: false,
      removeProduct: false,
      selectedProduct: {},
      data : "", 
      address:[],
      addAddress: false,   
      addressModal: false,
      defaultAddress: [],
      appliedCoupon: "--//--",
      totalDiscount: 0.00,
      deliveryCharges: 0.00,
      couponList: [],
      isCouponAvailable: false,
      checkMashObj: {},
      showLoader: false,
      walletAmountUsed: 0.00,
      paidAmount: 0.00,
      isPastOrder: false,
      couponModal: false,
      gst: 0.00,

    }

  }

  async componentDidMount(){
    console.warn('this.userData', this.props.userData.userData.shopId);
    
    if (this.props.navigation.state.params) {
      if (this.props.navigation.state.params.orderType == 'Past') {
        this.setState({isPastOrder: true})
      }
    }
    // Paytm.addListener(Paytm.Events.PAYTM_RESPONSE, this.handlePaytmResponse);

    this.replaceCart();
    // Api to fetch available coupons
    const reqBody = {
      shopId: this.props.userData.userData.shopId,
    };
    const getCoupon = await callAPI.axiosCall(Config.getCouponForShop, reqBody);
    if (getCoupon) {
      this.props.couponAction.addCoupons(getCoupon.data.result, '');
    } else {
      this.setState ({isCouponAvailable: false});
    } 
  }
    //-----------------------------
    CartListing = ({data}) => {
      return (  

        <View style={[styles.prodcutListingView, {backgroundColor: data.backGroundColor}]}>
          <View style={styles.addCardImage}>
            <Image style={styles.productImage} source={{uri: data.productLogo}}/>
          </View>
          <View style={styles.stores}>
            <View style={{flex:10}}>
              <Text style={s.subHeaderText} numberOfLines={4}>{data.productName} </Text>
            </View>
            <View style={styles.Mystores}>
              <View style={{flexDirection: "column", flex: 8}}>
                <Text style={[s.normalText]} numberOfLines={4}>Total: ₹{data.totalCost}/{data.qtyPurchase}</Text>
                <Text style={[s.normalText]} numberOfLines={1}>Unit cost: ₹{data.unitCost}/ {data.unit}</Text>
                <Text style={[s.normalText]} numberOfLines={3}>Shop Name: {data.shopName}</Text>
              </View>
              <View style={styles.flexRow}>
                <View style={styles.flexRowAdd}>
                  <TouchableOpacity onPress={() => this.decrementCount(data)}>
                    <View style={styles.orderCardButton}>
                      <Text style={{fontSize:18,color:"#0080FE"}}>-</Text>
                    </View>
                  </TouchableOpacity>
                    <Text style={styles.orderCount}>{data.qtyPurchase}</Text>
                  <TouchableOpacity onPress={() => this.incrementCount(data)}>
                    <View style={styles.orderCardButton}>
                      <Text style={{fontSize:18,color:"#0080FE"}}>+</Text>
                    </View>
                  </TouchableOpacity>
                </View>
              </View>  
            </View>
            {/* <View style={{flex: 1, flexDirection:'row'}}>
              <View style={{flex: 7}}>
              </View>
              <View style={styles.flexRow}>
                <View style={styles.flexRowAdd}>
                  <TouchableOpacity onPress={() => this.decrementCount(data)}>
                    <View style={styles.orderCardButton}>
                      <Text style={{fontSize:18,color:"#0080FE"}}>-</Text>
                    </View>
                  </TouchableOpacity>
                    <Text style={styles.orderCount}>{data.qtyPurchase}</Text>
                  <TouchableOpacity onPress={() => this.incrementCount(data)}>
                    <View style={styles.orderCardButton}>
                      <Text style={{fontSize:18,color:"#0080FE"}}>+</Text>
                    </View>
                  </TouchableOpacity>
                </View>
              </View>  
            </View> */}
            
          </View>
        </View>
      )
    }    

    removeProduct = () => {

      this.setState({
        removeProduct: true,
        modalVisible: false
      });
      const i = this.props.cartItem.cartItem.findIndex(ind => ind.productId === this.state.selectedProduct.productId);
      
      this.props.cartState.cartCount -= 1;
      this.props.cartState.cartSum = parseFloat(this.props.cartState.cartSum) - parseFloat(this.props.cartItem.cartItem[i].unitCost); 
      
      this.props.cartItem.cartItem[i].counter = this.props.cartItem.cartItem[i].counter - 1;
      this.props.cartItem.cartItem[i].totalCost = this.props.cartItem.cartItem[i].unitCost * this.props.cartItem.cartItem[i].counter;

      this.props.cartItem.cartItem.splice(i,1);
      if(this.props.cartState.cartCount === 0){
        this.props.cartModalAction.changeCartState(false,this.props.cartState.cartSum,this.props.cartState.cartCount);
      }
      else{
        this.props.cartModalAction.changeCartState(true,this.props.cartState.cartSum,this.props.cartState.cartCount);
      }
      
      this.replaceCart();
    
    }

    hideModal = () => {
      this.setState({ 
        removeProduct: false,
        modalVisible: false,
      });
    }

    hideCouponModal = () => {
      this.setState({
        couponModal: false
      })
    }

    //function to increment the count of particular item
    decrementCount = (productData) => {
      this.setState({
        selectedProduct: productData
      });
      let id = this.props.cartItem.cartItem.findIndex(ind => ind.productId === productData.productId);
      
      if(productData.counter - 1 <= 0){
        this.setState({
          modalVisible: true
        });
      }
      else {
        this.props.cartItem.cartItem[id].counter = this.props.cartItem.cartItem[id].counter - 1;
        this.props.cartItem.cartItem[id].qtyPurchase = this.props.cartItem.cartItem[id].qtyPriceInfo[this.props.cartItem.cartItem[id].counter - 1].qty;
        this.props.cartState.cartSum = parseFloat(this.props.cartState.cartSum) - parseFloat(this.props.cartItem.cartItem[id].totalCost);
        this.props.cartItem.cartItem[id].totalCost = this.props.cartItem.cartItem[id].qtyPriceInfo[this.props.cartItem.cartItem[id].counter - 1].price;
        this.props.cartState.cartCount -= 1;
        this.props.cartModalAction.changeCartState(true,this.props.cartState.cartSum,this.props.cartState.cartCount);
        this.replaceCart();
      }
      
    }

    replaceCart = () => {
      this.props.actions.replaceCart(this.props.cartItem.cartItem);
      let cartSum = 0;
      let cartSumExlGst = 0.00;
      let temp = [];
      let counter = 0;
      console.warn('this.props.cartItem.cartItem', this.props.cartItem.cartItem);
      this.props.cartItem.cartItem.forEach(element => {
        if (!temp.some(arr => arr.shopId == element.shopId)) {
          temp.push(element);
          counter = counter + element.counter; 
        }      
        console.warn('element.counter', element.counter);
        console.warn('element.qtyPriceInfo[element.counter - 1].priceExcltax', element.qtyPriceInfo[element.counter - 1].priceExclTax);
        cartSum = parseFloat(cartSum) + parseFloat(element.totalCost);
        cartSumExlGst = parseFloat(cartSumExlGst) + parseFloat(element.qtyPriceInfo[element.counter - 1].priceExclTax);
      });
      console.warn('Testttttttttt----->cartSumExlGst', cartSumExlGst);
      console.warn('Testttttttttt----->cartSum', cartSum);
      this.setState({ gst: cartSum - cartSumExlGst});
      let payable;
      if (this.props.walletBalance.canUse) {
        payable = (parseFloat(cartSum)) - parseFloat(this.props.walletBalance.walletBalance);
      } else {
        payable = parseFloat(cartSum);
      }

      if (payable <= 0) {
        this.setState({
          totalPayable: 0.00,
          paidAmount: parseFloat(cartSum),
          walletAmountUsed: parseFloat(cartSum)
        })
  
      } else {
        this.setState({
          totalPayable: payable,
          paidAmount: parseFloat(cartSum),
          walletAmountUsed: parseFloat(this.props.walletBalance.walletBalance)
        })
      }
  
      this.setState({
        cartTotal: parseFloat(cartSum),
        gst: cartSum - cartSumExlGst,
        cartTotalExlGst: cartSumExlGst
      });

      this.props.cartModalAction.changeCartState(true, cartSum, counter);

      if(this.props.cartItem.cartItem.length == 0) {
        this.props.cartModalAction.changeCartState(false, 0.0, 0);
        this.props.navigation.navigate('shopHomePage');
      } else if (this.state.appliedCoupon != '--//--') {
        this.applyCoupon(this.state.couponModal);
      }
    }

    //function to increment the count of particular item
    incrementCount = (productData) => {
      this.setState({
        selectedProduct: productData
      });
      let index = this.props.cartItem.cartItem.findIndex(ind => ind.productId === productData.productId);
      if (this.props.cartItem.cartItem[index].counter + 1 > this.props.cartItem.cartItem[index].qtyPriceInfo.length) {
        Toast.show('Please contact flash mart (admin) in case you want to order more', Toast.LONG);
      } else {
        this.props.cartState.cartCount += 1;
        // if (this.props.cartItem.cartItem[index].counter + 1 == this.props.cartItem.cartItem[index].qtyPriceInfo.length);
        this.props.cartItem.cartItem[index].counter = this.props.cartItem.cartItem[index].counter + 1;
        this.props.cartItem.cartItem[index].qtyPurchase = this.props.cartItem.cartItem[index].qtyPriceInfo[this.props.cartItem.cartItem[index].counter - 1].qty;
        this.props.cartState.cartSum = parseFloat(this.props.cartState.cartSum) - parseFloat(this.props.cartItem.cartItem[index].totalCost);
        this.props.cartItem.cartItem[index].totalCost = this.props.cartItem.cartItem[index].qtyPriceInfo[this.props.cartItem.cartItem[index].counter - 1].price;
        this.props.cartState.cartSum = parseFloat(this.props.cartState.cartSum) + parseFloat(this.props.cartItem.cartItem[index].totalCost); 
        this.props.cartModalAction.changeCartState(true,this.props.cartState.cartSum,this.props.cartState.cartCount);
        this.replaceCart();
      }
      

    }

    //function to increment the count of particular item
    changeAddress = () => {
      // need to navigate address page
      this.props.navigation.navigate('shopAddressPage',{isUpdate: true, callingPage: 'shopOrderDetails'});
    }

    DeliveryAddress = () => {
      const {navigate} = this.props.navigation;
      if (this.props.userData.userData.shopDetails[0].shopAdress) {
        return(
        <View style={{flex:4,backgroundColor:"white",paddingTop: 10, borderTopWidth:2,borderTopColor:"#7285A5"}}>
                
          <View style={{flex:8,paddingHorizontal:15, borderBottomWidth: 1, borderColor: "grey"}}>
            <View style={{flexDirection:"row"}}>
              <View style={{flex: 6}}>
                <Text style={s.headerText}>Deliver to</Text>
              </View>
              <TouchableOpacity style={{flex: 6,alignItems:"flex-end"}} onPress={() => this.changeAddress()}>
                <Text style={{color: "orange"}}>Change Address</Text>
              </TouchableOpacity>
            </View>
            <View style={{ paddingVertical: 5}}> 
              <View>
                <Text style={s.normalText}>{this.props.userData.userData.shopDetails[0].shopNo}</Text>
                <Text style={s.normalText}>{this.props.userData.userData.shopDetails[0].landmark}</Text>
                {/* <Text style={s.normalText}>{this.props.userData.userData.addressDetails[0].flatNumber}</Text> */}
                {/* <Text style={s.normalText}>{this.props.userData.userData.addressDetails[0].addressLine2}</Text> */}
              </View>   
            </View>
          </View>
          <View style={{paddingVertical: 10, paddingHorizontal:15}}>
            <Text style={{fontSize: 16,fontWeight: "bold",color:"#131E3A"}}>Total Payable: ₹{(this.state.totalPayable).toFixed(2)}</Text>
          </View>
          <View style={{flex:4,flexDirection:"row"}}>
          <TouchableOpacity style={{flex: 1,backgroundColor:"#00CCFF",alignItems:"center",justifyContent:"center"}}
              onPress={() => this.checkout()}>
            <Text style={[s.headerText,{color:"white"}]}>Place Order</Text>
          </TouchableOpacity>
        </View>
      </View>
        )
      } else {
        return (
          <View style={{flex:2,backgroundColor:"white",paddingTop: 10,borderTopWidth:2,borderTopColor:"#7285A5"}}>
             
            <View style={{flex:0.5,paddingHorizontal:15,alignItems:"center"}}>
              <TouchableOpacity onPress={() => navigate('addressPage',{"addressId": "", 'callingPage': 'orderDetailsPage'})}>
                <Text style={[s.headerText,{textAlign:"center", color:"#00CCFF"}]}>Add address</Text>
              </TouchableOpacity>
              
            </View>
            <View style={{paddingVertical: 10}}>
              <Text style={{fontSize: 16,fontWeight: "bold",color:"#131E3A"}}>Total Payable: ₹{(this.state.totalPayable).toFixed(2)}</Text>
            </View>
            <View style={{flex:1,flexDirection:"row"}}>
            {/* <TouchableOpacity style={{flex:6,backgroundColor:"#F5F5F5",justifyContent:"center",alignItems:"center"}}
              onPress={() => this.placeBid()}>
              <Text style={{fontSize: 16,fontWeight: "bold",color:"#131E3A"}}>Place bid</Text>
            </TouchableOpacity> */}
            <TouchableOpacity style={{flex:1,backgroundColor:"#00CCFF",alignItems:"center",justifyContent:"center"}}
                onPress={() => this.checkout()}>
              <Text style={[s.headerText,{color:"white"}]}>Place Order</Text>
            </TouchableOpacity>
          </View>
          </View>
        )
      }
      
    }

    // setDefaultAddress = () => {
    //   if(this.props.userData.userData.addressDetails.length > 0){
    //     this.props.userData.userData.addressDetails.forEach(element => {
    //       if(element.isSelected == true){
    //         this.state.defaultAddress = [];
    //         this.state.defaultAddress.push(element); 
    //         this.setState({
    //           defaultAddress: this.state.defaultAddress,
    //           addressModal: false
    //         })
    //       }
  
    //     });  
    //     const i = this.props.userData.userData.addressDetails.findIndex(ind => ind.isSelected === true);
    //     if (i == 1) {
    //       const temp = this.props.userData.userData.addressDetails[0];
    //       this.props.userData.userData.addressDetails[0] = this.props.userData.userData.addressDetails[1];
    //       this.props.userData.userData.addressDetails[1] = temp;
    //     }
    //     this.setState({
    //       addressModal: false
    //     })

    //   }
    // }

    showCouponModal = () => {
      console.warn('this.sate.coupon list', this.state.couponList, this.props.availableCoupons);
      this.setState({
        couponModal: true
      })
    }

    applyCoupon = (couponCode) => {
      console.warn('couponCode-->', couponCode);
      if(couponCode == '' || couponCode == undefined) {
        Toast.show('Please select valid coupon from list',Toast.LONG);
      } else {
        const i = this.props.availableCoupons.couponList.findIndex(ind => ind.coupon === couponCode);
        if (i < 0) {
          Toast.show('Coupon code is invalid',Toast.LONG);
        } else {
            if (parseFloat(this.state.cartTotal) > parseFloat(this.props.availableCoupons.couponList[i].minLimit)) {
              console.warn('couponCode-->', this.props.availableCoupons.couponList);
              console.warn('index---', i);
              const percentage = parseFloat(this.props.availableCoupons.couponList[i].percentage);
              console.warn('percentage', percentage);
              console.warn('totalCost, totalPay, deliveryCharges', this.state.cartTotal, this.state.totalPayable, this.state.deliveryCharges);
              let discount = (parseFloat(this.state.cartTotal) * (percentage/100));
              if(discount > parseFloat(this.props.availableCoupons.couponList[i].maxDiscount)) {
                discount = parseFloat(this.props.availableCoupons.couponList[i].maxDiscount)
              }
    
              let payable;
              if (this.props.walletBalance.canUse) {
                payable = (parseFloat(this.state.cartTotal) - discount) - parseFloat(this.props.walletBalance.walletBalance);
              } else {
                payable = (parseFloat(this.state.cartTotal) - discount);
              }
    
              if(payable <= 0) {
                this.setState({
                  totalPayable: 0.00,
                  paidAmount: (parseFloat(this.state.cartTotal) - discount),
                  walletAmountUsed: (parseFloat(this.state.cartTotal) - discount)
                })
              } else {
                this.setState({
                  totalPayable: payable,
                  paidAmount: (parseFloat(this.state.cartTotal) - discount),
                  walletAmountUsed: parseFloat(this.props.walletBalance.walletBalance)
                })
              }
              
              this.setState({
                appliedCoupon: couponCode,
                couponModal: false,
                totalDiscount: discount,
              });
              Toast.show('Coupon applied successfully. You saved total Rs ' + discount + 'on your cart amount',Toast.LONG);
        
            
            } else {
              Toast.show('Coupon is valid only if cart value is more than Rs 2000',Toast.LONG); 
            }
          }
         
      }
     
    }

    postPayment = (res) => {
      this.setState({showLoader: false});
      this.props.cartItem.cartItem = [];
      // this.props.actions.placedCart(tempObj);
      this.props.actions.replaceCart(this.props.cartItem.cartItem);
      this.props.cartState.cartCount = 0;
      this.props.cartState.cartSum = 0; 
      // this.props.walletAction.updateWallet(0, true);
      this.props.cartModalAction.changeOrderState(true); 
      this.props.cartModalAction.changeCartState(false,this.props.cartState.cartSum,this.props.cartState.cartCount);  
      if (res) {
        this.props.walletAction.updateWallet(res.data.wallet, true);
      } 
      this.props.navigationAction.navigateAction('Home');
      this.props.navigation.navigate('homePage');
    }

    WalletAmount = () => {
      if(this.props.walletBalance.canUse) {
        return(
          <View style={{flex: 1, flexDirection: "row",paddingVertical: 3}}>
            <View style={{flex: 8}}>
              <Text style={[s.normalText]} >Flash Wallet </Text>
            </View>
            <View style={{flex:4,alignItems:"flex-end"}}>
              <Text style={[s.normalText]} >₹{this.props.walletBalance.walletBalance} </Text>
            </View>
          </View>
        )
      } else {
        return null;
      }
    }


    render() {
        const {navigate} = this.props.navigation;
        if(this.props.cartItem.cartItem.length == 0) {
          return (
            <SafeAreaView style={{flex: 1}}>
              <View style={[s.bodyGray]}>
              <AnimatedLoader
                visible={this.state.showLoader}
                overlayColor="rgba(255,255,255,0.75)"
                source={require("../../../assets/loader.json")}
                animationStyle= {[styles.lottie,{height: 150,width: 150}]}
                speed={1}
              />
                {/* <InternateModal /> */}
                <View style={[{flex:1, paddingHorizontal: 10, alignItems:"center"}]}> 
                  {/* <View style={{flex:1,paddingVertical: 20}}>
                    <Text style={s.headerText}> Shooping Bag is Empty</Text>
                  </View>
                  <View style={{flex:1,paddingVertical: 20}}>
                    <Text style={s.normalText}> There is nothing in your bag go & grab the offers</Text>
                  </View> */}
                    <Image style={{width:ScreenWidth-30,height:ScreenHeight - 80}} source={require('../../../assets/emptyCartPage.jpeg')}/>
                  
                  
                </View>
                {/* <NavigationBar navigate={navigate}></NavigationBar> */}
              </View>
              
            </SafeAreaView>
            
          )
        } else {
          return (
            <SafeAreaView style={{flex: 1}}>
              <View style={[s.bodyGray]}>
              <AnimatedLoader
                visible={this.state.showLoader}
                overlayColor="rgba(255,255,255,0.75)"
                source={require("../../../assets/loader.json")}
                animationStyle= {[styles.lottie,{height: 150,width: 150}]}
                speed={1}
              />
                {/* <InternateModal /> */}
              <PopupModal visible={this.state.modalVisible} onDismiss={this.hideModal} removeProduct={this.removeProduct} 
                message={"Are you sure want to remove the product from cart?"} btnTag={"Remove"}/>
              <CouponModal visible={this.state.couponModal} avalableCoupons= {this.state.couponList} onDismiss={this.hideCouponModal} navigation = {navigate} applyCoupon={this.applyCoupon} />
              
    
                {/* <View style={{flex:1,backgroundColor:"#F5FCFF"}}>
                  <View style={{marginHorizontal:15}}>
                    <Text style={[s.headerText,styles.header]}>My Orders </Text>
                  </View>
    
                </View> */}
                <View style={{flex:8}}>
                  <ScrollView showsVerticalScrollIndicator={false}>
                    <View style={{backgroundColor:"white",elevation: 6}}>
                      <FlatList
                        data={this.props.cartItem.cartItem}
                        renderItem={( { item } ) => (
                          <this.CartListing
                            data={item}
                          />
                          
                        )}
                        extraData={this.props} 
                      />
                    </View>
                    <View style={{backgroundColor:"white",marginTop:15,paddingVertical: 15,paddingHorizontal:15, elevation: 6}}>
                      <TouchableOpacity style={{flex: 1, flexDirection: "row"}} onPress={() => this.showCouponModal()}> 
                        <View style={{flex: 2,alignItems:"flex-start",justifyContent:"center",alignContent:"center"}}>
                          <Image style={{height: 30,width: 30}} source={require('../../../assets/offericon.png')}/>
                        </View>
                        <View style={{flex: 8,alignItems:"flex-start",justifyContent:"center",alignContent:"center"}}>
                          <Text style={s.headerText}>Apply Coupon </Text>
                        </View>
                        <View style={{flex: 2,alignItems:"center",justifyContent:"center",alignContent:"center"}}>
                          <Text style={s.headerText}> > </Text>
                        </View>
                      </TouchableOpacity>
                    </View>
                    <View style={{backgroundColor:"white",marginTop:15,paddingVertical: 15,paddingHorizontal:15, elevation: 6}}>
                      <View>
                        <Text style={s.headerText}>Bill Details </Text>
                      </View>
                      <View style={{paddingTop:10}}>
                        <View style={{flex: 1, flexDirection: "row",paddingVertical: 3}}>
                          <View style={{flex: 8}}>
                            <Text style={[s.normalText]} >Item Total</Text>
                          </View>
                          <View style={{flex:4,alignItems:"flex-end"}}>
                            <Text style={[s.normalText]} >₹{(this.state.cartTotal).toFixed(2)} </Text>
                          </View>
                        </View>
                        <View style={{paddingTop:10,borderBottomWidth: 1, borderBottomColor:"#7285A5"}}/>
                          <View style={{flex: 1, flexDirection: "row",paddingVertical: 3}}>
                            <View style={{flex: 8}}>
                              <Text style={[s.normalText]} >Total cost</Text>
                            </View>
                            <View style={{flex:4,alignItems:"flex-end"}}>
                              <Text style={[s.normalText]} >₹{(this.state.cartTotalExlGst).toFixed(2)} </Text>
                            </View>
                          </View>
                          <View style={{flex: 1, flexDirection: "row",paddingVertical: 3}}>
                            <View style={{flex: 8}}>
                              <Text style={[s.normalText]} >Total GST applicable </Text>
                            </View>
                            <View style={{flex:4,alignItems:"flex-end"}}>
                              <Text style={[s.normalText]} >₹{(this.state.gst).toFixed(2)} </Text>
                            </View>
                          </View>
                          <View style={{flex: 1, flexDirection: "row",paddingVertical: 3}}>
                            <View style={{flex: 8}}>
                              <Text style={[s.normalText]} >Coupon </Text>
                            </View>
                            <View style={{flex:4,alignItems:"flex-end"}}>
                              <Text style={[s.normalText]} >{this.state.appliedCoupon} </Text>
                            </View>
                          </View>
                          <View style={{flex: 1, flexDirection: "row",paddingVertical: 3}}>
                            <View style={{flex: 8}}>
                              <Text style={[s.normalText]} >Total discount </Text>
                            </View>
                            <View style={{flex:4,alignItems:"flex-end"}}>
                              <Text style={[s.normalText]} >₹{(this.state.totalDiscount).toFixed(2)} </Text>
                            </View>
                          </View>
                          <this.WalletAmount />
                      </View>
                      <View style={{paddingTop:10,flexDirection: "row",flex:1}}>
                        <View style={{flex: 8}}>
                          <Text style={[s.normalText]} >To Pay</Text>
                        </View>
                        <View style={{flex:4,alignItems:"flex-end"}}>
                          <Text style={[s.normalText]} >₹{(this.state.totalPayable).toFixed(2)} </Text>
                        </View>
                      </View>
                    </View>
                  </ScrollView>
    
                </View>
                <this.DeliveryAddress />
                
                {/* <View style={{flex:4,backgroundColor:"white",paddingTop: 10,borderTopWidth:2,borderTopColor:"#7285A5"}}>
                  
                  <View style={{flex:4,flexDirection:"row"}}>
                    <View style={{flex:6,backgroundColor:"#F5F5F5",paddingLeft:15,justifyContent:"center"}}>
                      <Text style={{fontSize: 16,fontWeight: "bold",color:"#131E3A"}}>Total Payable</Text>
                      <Text style={s.normalText}>₹{this.state.cartTotal}</Text>
                    </View>
                    <TouchableOpacity style={{flex:6,backgroundColor:"#00CCFF",alignItems:"center",justifyContent:"center"}}
                      onPress={() => this.checkout()}>
                      <Text style={[s.headerText,{color:"white"}]}>Checkout</Text>
                    </TouchableOpacity>
                  </View>
                </View> */}
                {/* <NavigationBar navigate={navigate}></NavigationBar>   */}
              </View>
              
            </SafeAreaView>
            
            
          );
        
    }
  }

  getOrderObject = async() => {
    let orderData;
    let couponCode;
    if (this.state.appliedCoupon == '--//--') {
      couponCode = '';
    } else {
      couponCode = this.state.appliedCoupon;
    }
      orderData = {
        coupon: couponCode,
        totalDiscount: this.state.totalDiscount,
        totalCost: this.state.cartTotal,
        totalCostExclTax: parseFloat((this.state.cartTotalExlGst).toFixed(2)),
        appliedGst: parseFloat((this.state.gst).toFixed(2)), 
        toPay: this.state.paidAmount,
        itemList: this.props.cartItem.cartItem,
        shopDetails: {
          fullName: this.props.userData.userData.shopDetails[0].shopOwnerName,
          contactNumber: this.props.userData.userData.shopDetails[0].shopContactNumber,
          shopAddress: this.props.userData.userData.shopDetails[0].shopAdress,
          emailId: this.props.userData.userData.shopDetails[0].shopEmail 
        }
      } 
      console.warn('orderData --->', orderData)
    return orderData;
  }

  checkout = async() => {
    const orderObj = await this.getOrderObject();
    const reqBody = {
      order: orderObj,
      shopId: this.props.userData.userData.shopId,
      amountUserFromWallet: this.state.amountUserFromWallet
    }
    this.setState({ showLoader: true });
    const resPlaceOrder = await callAPI.axiosCall(Config.placeShopOrder, reqBody);
    this.setState({ showLoader: false });
    if (resPlaceOrder) {
      
      if (resPlaceOrder.data.status == 0) {
        Toast.show(resPlaceOrder.data.message, Toast.LONG);
        this.props.cartItem.cartItem = [];
        this.props.actions.replaceCart(this.props.cartItem.cartItem);
        this.props.cartModalAction.changeCartState(false, 0.00, 0);
        this.props.navigationAction.navigateAction('ShopHome');
        const resetAction = StackActions.reset({
          index: 0,
          actions: [NavigationActions.navigate({ routeName: 'shopHomePage'})]
        })
        this.props.navigation.dispatch(resetAction);
      } else {
        Toast.show(resPlaceOrder.data.message, Toast.LONG);
      }
    } else {
      
    } 
  }

     
};

let ScreenHeight = Dimensions.get("window").height;
let ScreenWidth = Dimensions.get("window").width;

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: "#F5FCFF",
    height: ScreenHeight
  },
  body: {
    backgroundColor: '#F5FCFF',
  },
  prodcutListingView: {
    flexDirection: 'row',
    marginVertical: 5,
    backgroundColor: "white",
    padding: 10

  },
  addCardImage: {
    width: 80,
    height: 80,
    flex: 3
  },
  productImage: {
    width: 80,
    height: 80
  },
  stores: {
    paddingHorizontal: 15,
    flex: 9,
    flexDirection: "column"
  },
  addressButtonView: {
		backgroundColor:"#00CCFF",  
		width: '80%',
		//borderRadius: 50,
		elevation: 6,
		shadowColor: 'rgba(0, 0, 0, 0.1)',
		shadowOpacity: 0.8,
		height: 40,
		justifyContent: 'center',
		alignItems: 'center',
	},

	addressButtonText: {
		color:"white",  
		fontSize: 16 ,
		fontWeight:"bold"
	},
  Mystores: {
    flexDirection: "row",
    alignItems: "flex-start",
    justifyContent: "space-between",
    textAlignVertical: "bottom",
    flex: 2
    // paddingBottom:15,
  },
  flexRow: {
    flex: 4,
    textAlignVertical: "bottom",
    flexDirection: 'row',
    alignItems: "flex-end"
  },
  flexRowAdd: {
    paddingTop: 10,
    textAlignVertical: "bottom",
    flexDirection: 'row',
  },
  basketBtn:{
    paddingVertical: 5,
    paddingHorizontal: 10,
    marginBottom: 10,
    borderRadius: 20,
    height: 30,
    width: 30
  },
  basketImg: {
    height: 30,
    width: 30,
  },
  storeBtn: {
    paddingVertical: 5,
    paddingHorizontal: 5,
    marginBottom: 5,
    color: 'white',
    borderRadius: 0,
    width: 50
  },
  addtoCartBtnColor: {
    fontSize:12,
    borderWidth: 1,
    borderColor: '#0080FE',
    marginLeft: 20,
  },
  header: {
    fontSize: 24,
    paddingVertical: 5
  },
  orderDtlContainer:{
    borderRadius:0,
  },
  orderHeading:{
    paddingLeft: 10,
    paddingVertical:5,
    marginTop:15
  },
  orders:{
    paddingLeft:10,
  },
  orderTitles:{
    paddingLeft: 10,
    paddingVertical:15,
  },
  orderCount:{
    fontSize:18,
    fontWeight:'bold',
    paddingHorizontal:10,
    color: '#131E3A'
},
orderProductName:{
  fontWeight: 'bold',
  fontSize:16
},
  orderPriceText:{
    color:'#3cb371',
    fontWeight:'bold'
  },
  orderProductHeading:{
    fontWeight: 'bold',
    fontSize:30,
  },
  
  orderCardImage:{
    backgroundColor:'hotpink',
    width:50,
    height:50,
    borderRadius:50
},
orderCardButton:{
  borderColor:'#0080FE',
  borderWidth: 1,
  width:25,
  height:25,
  borderRadius:50,
  alignItems:"center",
  justifyContent:'center',
},
  orderList:{
    flex: 1,
    flexDirection: "column",
    padding:20,
    paddingLeft: 10,
  },
  orderBorder:{    
    borderTopColor: 'grey',
    borderTopWidth: 1,
  },
flexColumnAlignEnd:{
  display:'flex',
  flexDirection:'column',
  alignItems:'flex-end'
},
priceAlign:{
  paddingRight:5,
  paddingTop: 5,
},
orderDtlCenter:{
  display:"flex",
  flexDirection:"row",
  justifyContent:"center"
},
  MyOrders:{
    flexDirection:"row",
    alignItems:"center",
    justifyContent:"space-between",
    // paddingBottom:15,
  },
  orderCardSpace:{
    marginBottom: 15,
  },
  orderCardPadding:{
    paddingBottom: 15,
  },
  orderMainView: {
    marginHorizontal: 20,
    // flex: 3,
    backgroundColor: '#EBECF0',
  },
  headerstyle: {
    paddingBottom: 20,
    paddingTop: 20
  },

  orderBtnViewColor: {
    backgroundColor: '#ba55d3',
    color:'white',
  },
  continueBtnViewColor: {
    backgroundColor: '#2f4f4f',
    color:'white',
  },
  checkoutBtnViewColor: {
    backgroundColor: '#20b2aa',
    color:'white',
    marginLeft:20,
  },
  orderBtn: {
    paddingVertical:10,
    paddingHorizontal:20,
    marginBottom:10,
    color:'white',
    borderRadius: 20,
  },

});



//--------------------------------------------------- store related fuction  -- Start

const mapStateToProps = (state) => {
	return {
    cartItem: state.cartItem,
    userData: state.loginData,
    cartState: state.cartState,
    availableCoupons: state.availableCoupons,
    walletBalance: state.walletBalance
	}
}

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(addToCart, dispatch),
        loginActions: bindActionCreators(loginSuccess,dispatch),
        cartModalAction: bindActionCreators(cartModalAction,dispatch),
        couponAction: bindActionCreators(couponAction,dispatch),
        navigationAction: bindActionCreators(navigateAction,dispatch),
        walletAction: bindActionCreators(walletAction,dispatch),
    };
}

// ---------------------------------------------------- store related function  -- END


export default connect(mapStateToProps, mapDispatchToProps)(ShopOrderDetailsPage);
