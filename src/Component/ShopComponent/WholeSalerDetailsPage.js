/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Fragment } from 'react';

import * as Config from '../../Constant/Config.js';
import * as Constant from '../../Constant/Constant.js';
import { connect } from 'react-redux';
import * as loginSuccess  from '../../Actions/LoginAction.js';
import { bindActionCreators } from 'redux';
import  s  from '../../sharedStyle.js';
import AnimatedLoader from "react-native-animated-loader";
import DelayInput from "react-native-debounce-input";
import * as cartModalAction  from '../../Actions/CartModalAction';
import * as addToCart  from '../../Actions/CartItem';
import CartModal from '../ReusableComponent/cartModal';
import ShopNavigationBar from '../ReusableComponent/ShopNavigationBar.js'; 
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  Dimensions,
  TouchableOpacity,
  Image,
  FlatList,
  Linking
} from 'react-native';
import {
  Colors,
} from 'react-native/Libraries/NewAppScreen';
import * as navigateAction  from '../../Actions/NavigationBarAction.js';
import Toast from 'react-native-simple-toast';
import callAPI from '../../CoreFunctions/callAPI.js';


class WholeSalerDetails extends React.Component{

    constructor(props){
        super(props);    
        this.state = {
            showLoader: false,
            isNotAvailable: false,
            shopDetails: {},
            menu: [],
            shopImage: '',
            displayMsg: false,
            paddingBottomHeight: 0
        }
        
    }

    componentDidMount(){       
        this.props.navigationAction.shopNavigateAction('FlashMartForShop');
        this.getMenu();
        this.props.navigation.addListener('willFocus', () =>{
            this.props.navigationAction.shopNavigateAction('FlashMartForShop');
        });
        if (this.props.cartState.cartState) {
            this.setState({paddingBottomHeight: 100});
        } else {
            this.setState({paddingBottomHeight: 0});
        }

        this.props.navigation.addListener('willFocus', () =>{
            if (this.props.cartState.cartState) {
                this.setState({paddingBottomHeight: 100});
            } else {
                this.setState({paddingBottomHeight: 0});
            }
        });
    }

    getMenu = async() => {
        console.warn('get menu called');
        await this.setState({ showLoader: true });
        const menuDetails = await callAPI.axiosCall(Config.getWholeSalerMenu, {});
        await this.setState({ showLoader: false });
        if (menuDetails) {
            
            console.warn('menuDetails data===>', menuDetails);
            console.warn('menuDetails data Menu===>', menuDetails.data.result);
            console.warn('menuDetails data===>', menuDetails.data.result[0].shopDetails);
            
            this.setState({
                shopDetails: menuDetails.data.result[0].shopDetails,
                menu: menuDetails.data.result,
                shopImage: menuDetails.data.result[0].shopDetails.shopLogo
            });
            if (menuDetails.data.result.length <= 0) {
                this.setState({
                    displayMsg: true
                }); 
            } else {
                this.setState({
                    displayMsg: false
                });
            }
        } else {
            console.warn('menuDetails data else===>', menuDetails);
        }

    }
 
    ProductListView = ({item}) => {
        return (
            <View style={[styles.prodcutListingView]}>
                <View style={styles.addCardImage} >
                    <Image style={styles.productImage} source={{uri: item.wholesalerMenu.productLogo}}/>
                </View>
                <View style={styles.stores} >
                    <View style={{flex:10}}>
                    <Text style={s.subHeaderText} numberOfLines={3}>{item.wholesalerMenu.productName} </Text>
                    </View>
                   
                    <View style={{flex:1, flexDirection:"row", marginTop: 10}}>
                        <Text style={[s.normalText]}>Flash amount:- </Text>
                        <Text style={[s.normalText]} numberOfLines={1}>₹{item.wholesalerMenu.unitPrice}/ unit</Text>
                    </View>
                    <View style={{flex:1, flexDirection:"row", marginTop: 10}}>
                        <Text style={[s.normalText]}>Min purchase qty:- </Text>
                        <Text style={[s.normalText]} numberOfLines={1}>{item.wholesalerMenu.qtyPriceInfo[0].qty}</Text>
                    </View>                   
                    {item.shopDetails.shopId ?
                        <>
                        <Text style={[s.normalText]} numberOfLines={3}>Shop Name:- {item.shopDetails.shopName}</Text>
                        <Text style={[s.normalText]} numberOfLines={1}>{item.shopDetails.shopAdress}</Text>
                        </>
                        :
                        null  
                    }
                    <View style={styles.Mystores}>
                        <View style={{paddingTop: 10, flex: 7}}>
                            <Text style={[s.normalText]} numberOfLines={3}>Unit:- {item.wholesalerMenu.unit}</Text>
                        </View>
                        <View style={styles.flexRow}>
                            <TouchableOpacity style={[styles.storeBtn, styles.addtoCartBtnColor]} onPress={() => this.addCart(item)}>
                                <Text style={{ color: '#0080FE',paddingHorizontal: 4}}>ADD</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
                
            </View>
                
        )
    }

    render(){
        const {navigate} = this.props.navigation;
        return (
            <Fragment>
                <SafeAreaView style={{flex: 0, backgroundColor: '#332b5c'}}></SafeAreaView>
                <SafeAreaView style={{flex: 1, backgroundColor: '#332b5c'}}>
                    <View style={[s.bodyGray]}>
                        <AnimatedLoader
                            visible={this.state.showLoader}
                            overlayColor="rgba(255,255,255,0.75)"
                            source={require("../../../assets/loader.json")}
                            animationStyle= {[styles.lottie,{height: 150,width: 150}]}
                            speed={1}
                        />
                        <ScrollView showsVerticalScrollIndicator={false}>
                            <View style={styles.advertiseData}>
                                <Image style={styles.advertiseShop} source={{uri: this.state.shopImage}}/>
                            </View>
                            <View style={styles.sectionContainer}>
                                {/* <TextInput placeholder = {this.state.placeholderText}
                                    style={styles.searchShops} inlineImageLeft='searchicon' 
                                    value = {this.state.searchText}
                                    onChangeText={(searchText) => this.searchData(searchText,this.state.searchType)}/> */}
                                <DelayInput placeholder = {this.state.placeholderText}
                                    delayTimeout={500}
                                    minLength={2}
                                    style={styles.searchShops} inlineImageLeft='searchicon' 
                                    value = {this.state.searchText}
                                    onChangeText={(searchText) => this.searchData(searchText)}
                                />
                            </View>
                            <View style={{paddingBottom: this.state.paddingBottomHeight}}>
                                {!this.state.displayMsg 
                                ?
                                    <FlatList data={this.state.menu}
                                        renderItem={( { item } ) => (
                                            <this.ProductListView
                                            item = {item}
                                            />
                                        )}
                                        keyExtractor= {item => item}
                                        horizontal={false}
                                        extraData={this.state.menu}      
                                    />
                                :
                                    <View style={{flex: 1, justifyContent:"center", alignItems:"center", paddingHorizontal: 15}}>
                                        <Text style={[s.normalText]} numberOfLines={5}>Currently we are out of stock with our goods. Please contact flash team in case your requirement is urgent</Text>
                                    </View>
                                }
                                
                            </View>
                        </ScrollView>
                        <CartModal navigate={navigate} navigationPage={'shopOrderDetailsPage'}></CartModal>
                    </View>
                    <ShopNavigationBar navigate={navigate}></ShopNavigationBar>  
                </SafeAreaView>
            </Fragment>
        
        );
    }

    searchData = async(searchText) => {
        console.warn('searchText--->', searchText);
        const menuDetails = await callAPI.axiosCall(Config.getWholeSalerMenu, { searchData: searchText});
        console.warn('Menu details===>', menuDetails);
        if (menuDetails) {
            this.setState({
                menu: menuDetails.data.result,
            });
            if (menuDetails.data.result.length <= 0) {
                this.setState({
                    displayMsg: true
                });
            } else {
                this.setState({
                    displayMsg: false
                });
            }

        }
        
    }

    addCart = (productData) => {
    console.warn('add cart data', productData);
    const cart = {
      productId: productData.wholesalerMenu.productId,
      productName: productData.wholesalerMenu.productName,
      productLogo: productData.wholesalerMenu.productLogo,
      unitCost: parseFloat(productData.wholesalerMenu.unitPrice),
      qtyPriceInfo: productData.wholesalerMenu.qtyPriceInfo,
      minimumPurchaseQty: productData.wholesalerMenu.qtyPriceInfo[0].qty,
      qtyPurchase: productData.wholesalerMenu.qtyPriceInfo[0].qty,
      unit: productData.wholesalerMenu.unit,
      shopId: this.state.shopDetails.shopId,
      shopName: this.state.shopDetails.shopName,
      counter: 1,
      totalCost: parseFloat(productData.wholesalerMenu.qtyPriceInfo[0].price),
      cgst: parseFloat(productData.wholesalerMenu.cgst),
      sgst: parseFloat(productData.wholesalerMenu.sgst),
      shopAddress: this.state.shopDetails.shopAdress,
      shopContactNumber: this.state.shopDetails.shopContactNumber,
      shopEmail: this.state.shopDetails.shopEmail,
      shopLocation: this.state.shopDetails.shopLocation,
      shopOwnerName: this.state.shopDetails.shopOwnerName,
    }

    if(this.props.cartItem.cartItem.some(arr => arr.productId == productData.wholesalerMenu.productId)){
      Toast.show("Already added in the cart", Toast.LONG);
    } else {
        this.props.cartState.cartCount += 1;
        this.props.cartState.cartSum = (parseFloat(this.props.cartState.cartSum) + parseFloat(cart.totalCost)); 
        this.props.cartModalAction.changeCartState(true,this.props.cartState.cartSum,this.props.cartState.cartCount);
        this.props.actions.addToCart(cart);
        this.setState({paddingBottomHeight: 100});
        Toast.show(cart.productName + " is added in the cart",Toast.LONG);
    }
    
    }

}


let ScreenWidth = Dimensions.get("window").width;
let ScreenHeight = Dimensions.get("window").height;

const styles = StyleSheet.create({
    sectionContainer: {
        // marginTop: 24,
        paddingHorizontal: 24,
        paddingBottom:20
    },
    searchShops:{ 
        marginTop: 20,
        height: 40, 
        borderColor: 'gray', 
        borderWidth: 1,
        paddingHorizontal:10, 
        backgroundColor:"white",
        borderRadius:4 
    },
    addCardImage: {
        width: 80,
        height: 80,
        flex: 4
    },
    productImage: {
        width: 80,
        height: 80,
    },
      storeborder: {
        borderBottomColor: 'grey',
        borderBottomWidth: 1,
    },
    flexRow: {
        flex: 5,
        textAlignVertical: "bottom",
        flexDirection: 'row',
        alignItems:"flex-end",
        paddingRight: 10
    },
    prodcutListingView: {
        flexDirection: 'row',
        marginVertical: 5,
        backgroundColor: "white",
        padding: 10,
        marginHorizontal: 10,
        elevation: 4
    
    },
    stores: {
        paddingHorizontal: 10,
        flex: 8,
        flexDirection: "column"
    },
    Mystores: {
        flexDirection: "row",
        alignItems: "flex-start",
        justifyContent: "space-between",
        textAlignVertical: "bottom",
        flex: 2,
        marginTop: 5
        // paddingBottom:15,
    },
    addtoCartBtnColor: {
        fontSize:12,
        borderWidth: 1,
        borderColor: '#0080FE',
        marginLeft: 20,
    },
    storeBtn: {
        paddingVertical: 5,
        paddingHorizontal: 5,
        marginBottom: 5,
        color: 'white',
        borderRadius: 0,
        width: 50
    },
    basketBtn:{
        paddingVertical: 5,
        paddingHorizontal: 10,
        marginBottom: 10,
        borderRadius: 20,
        height: 30,
        width: 30,
    },
    basketImg: {
        height: 30,
        width: 30,
    },
    advertiseData: {
        height: 200,
        width: ScreenWidth,
        elevation: 4,
        borderBottomLeftRadius: 60,
        borderBottomRightRadius: 60,
      },
    advertiseShop: {
        flex: 1,
        width: undefined,
        height: undefined,
        resizeMode: 'cover',
        borderBottomLeftRadius: 50,
        borderBottomRightRadius: 50,
    }
});


//--------------------------------------------------- store related fuction  -- Start

const mapStateToProps = (state) => {
	return {
        cartItem: state.cartItem,
        basketItem: state.basketItem,
        cartState: state.cartState,
        userData: state.loginData,
	};
}

const mapDispatchToProps = (dispatch) => {
    return {
      loginActions: bindActionCreators(loginSuccess,dispatch),
      navigationAction: bindActionCreators(navigateAction,dispatch),
      actions: bindActionCreators(addToCart, dispatch),
      cartModalAction: bindActionCreators(cartModalAction,dispatch),
    };
}

// ---------------------------------------------------- store related function  -- END

export default connect(mapStateToProps, mapDispatchToProps)(WholeSalerDetails);  


