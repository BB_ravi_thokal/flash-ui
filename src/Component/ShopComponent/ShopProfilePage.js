/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Fragment } from 'react';

import * as Config from '../../Constant/Config.js';
import * as Constant from '../../Constant/Constant.js';
import { connect } from 'react-redux';
import * as loginSuccess  from '../../Actions/LoginAction';
import { bindActionCreators } from 'redux';
import * as cartModalAction  from '../../Actions/CartModalAction';
import * as addToCart  from '../../Actions/CartItem';
import * as walletAction  from '../../Actions/WalletAction';
import { NavigationActions, StackActions } from 'react-navigation';
import  s  from '../../sharedStyle.js';
import axios from 'axios'; 
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  Dimensions,
  TouchableOpacity,
  Image,
  FlatList,
  Linking
} from 'react-native';
import {
  Colors,
} from 'react-native/Libraries/NewAppScreen';
import ShopNavigationBar from '../ReusableComponent/ShopNavigationBar';
import * as navigateAction  from '../../Actions/NavigationBarAction';
import Toast from 'react-native-simple-toast';
import callAPI from '../../CoreFunctions/callAPI';

PastOrdersListShop = ({item, self}) => {
  const {navigate} = self.props.navigation;
  return (
    <View>
      <View style={{margin:10}}>
        <View>
          <Text style={{fontSize: 16,fontWeight:"bold"}}>Order Id: {item.orderId}</Text>
        </View>
        {item.invoiceURL != '' && item.invoiceURL != undefined 
        ?
        <View style={{flex: 1, flexDirection: "row", justifyContent: "flex-end", marginTop: 10}}>
          <Text style={[s.normalText, {justifyContent:"center", paddingTop: 5}]}>Download invoice </Text>
          <TouchableOpacity onPress={() => self.downloadInvoice(item.invoiceURL)}>
            <Image style={{height: 30, width: 30}} source={require('../../../assets/download.png')}/>  
          </TouchableOpacity> 
        </View>
        :
        null

        }

        {/* <FlatList data={item.order.shopOrderDetails}   
          renderItem={( { item } ) => (
            <this.ShopOrderDetails
              item = {item}
            // navigation={this.props.navigation}
            />
          )}
          keyExtractor= {item => item.shopId}
          extraData = {item.shopOrderDetails} 
        /> */}
        
      </View>
      {/* <TouchableOpacity style={{display:"flex",padding:5, alignItems:"center",backgroundColor:"#00ccff",marginTop:10}}>     
          <Text style={{ color:'#fff',fontSize: 16,fontWeight:"bold",justifyContent:"center"}}>REORDER</Text>                     
      </TouchableOpacity> */}
      <View style={{borderBottomColor:"#131E3A",borderBottomWidth:1,marginTop:10}}></View>
      
    </View> 
  )
}

PastServedOrdersListShop = ({item, self}) => {
  const {navigate} = self.props.navigation;
  return (
    <View>
      <View style={{margin:10}}>
        <View>
          <Text style={{fontSize: 16,fontWeight:"bold"}}>Order Id: {item.orderId}</Text>
        </View>
        <View>
          <Text style={s.normalText}>Your action: {item.shopOrderStatus.status}</Text>
        </View>
        <View>
          <Text style={s.normalText}>Order status: {item.orderStatus}</Text>
        </View>
        {item.shopOrderStatus.status == 'Rejected' ||  item.orderStatus == 'Cancelled'
        ?
        null
        :
        <View style={{flex: 1, flexDirection: "row", justifyContent: "flex-end", marginTop: 10}}>
          <Text style={[s.normalText, {justifyContent:"center", paddingTop: 5}]}>Download invoice </Text>
          <TouchableOpacity onPress={() => self.downloadInvoice(item.shopOrderStatus.invoice)}>
            <Image style={{height: 30, width: 30}} source={require('../../../assets/download.png')}/>  
          </TouchableOpacity> 
        </View>
        }
        

        {/* <FlatList data={item.order.shopOrderDetails}   
          renderItem={( { item } ) => (
            <this.ShopOrderDetails
              item = {item}
            // navigation={this.props.navigation}
            />
          )}
          keyExtractor= {item => item.shopId}
          extraData = {item.shopOrderDetails} 
        /> */}
        
      </View>
      {/* <TouchableOpacity style={{display:"flex",padding:5, alignItems:"center",backgroundColor:"#00ccff",marginTop:10}}>     
          <Text style={{ color:'#fff',fontSize: 16,fontWeight:"bold",justifyContent:"center"}}>REORDER</Text>                     
      </TouchableOpacity> */}
      <View style={{borderBottomColor:"#131E3A",borderBottomWidth:1,marginTop:10}}></View>
      
    </View> 
  )
}

class ShopProfilePage extends React.Component{

  constructor(props){
    super(props);    
    this.state = {
      data : "", 
      address:[],
      addAddress: false,
      isModalVisible: false,
      removeAddressId: [],
      removeAddressIndex: 0,       
      getActivityData: [],
      sendActivityData: [],
      pastOrderData: [],
      servedOrder: []   
    }
    
  }

  
  componentDidMount(){       
    this.props.navigationAction.shopNavigateAction('Profile');
    this.getPastOrder(); 
    this.servedOrders(); 
    this.props.navigation.addListener('willFocus', () =>{
      console.warn('am in focus Search');
      this.props.navigationAction.shopNavigateAction('Profile');
    });
  }

  getPastOrder = async() => {
    const reqBody = {
      shopId : this.props.userData.userData.shopId
    };
    const orderDetails = await callAPI.axiosCall(Config.getPastWholeSalerOrders, reqBody);
    console.warn('orderDetails past order--->', orderDetails.data);
    if (orderDetails) {
      if (orderDetails.data.result) {
        this.setState({ pastOrderData: orderDetails.data.result })
      }
    }
  }

  servedOrders = async() => {
    const reqBody = {
      shopId : this.props.userData.userData.shopId
    };
    const orderDetails = await callAPI.axiosCall(Config.getOrderDetailsForShop, reqBody);
    console.warn('orderDetails servedOrders order--->', orderDetails.data);
    if (orderDetails) {
      if (orderDetails.data.pastOrder) {
        this.setState({ servedOrder: orderDetails.data.pastOrder })
      }
    }
  }

  MyAddress = () => {
    const {navigate} = this.props.navigation;
    if(!this.props.userData.userData.shopDetails[0].shopAdress){
      return(
        <View style={{backgroundColor:"white"}}>
          <View style={{ padding:10 , marginLeft:5}}>
            <Text style={s.headerText}>My Address</Text>
          </View>
          <View style={{display:"flex",alignItems:"center"}}>
            <TouchableOpacity style={[styles.storeBtn, styles.addtoCartBtnColor,{display:"flex",alignItems:"center"}]} 
              onPress={() => navigate('addressPage',{"addressId": ""})}>
              <Text style={{ color:'#00CCFF',fontSize: 16,fontWeight:"bold",justifyContent:"center"}}> ADD ADDRESS</Text> 
            </TouchableOpacity>
          </View>
        </View>
      )
    }
    else{
      return(
        <View>
          <View style={{ padding:10, backgroundColor:"white"}}>    
            <View style={{flex:1,flexDirection:"row"}}>
              <View style={{flex:1}}>
                <Text style={{fontSize: 14,fontWeight:"bold",color:"#131E3A"}}>Address </Text>   
              </View>
              <View style={{flex:1,flexDirection:"row",justifyContent:"flex-end"}}>
                <TouchableOpacity style={{paddingHorizontal: 5}} onPress={() => navigate('shopAddressPage',{ callingPage: "shopProfile", isUpdate: true })}>
                  {/* <Image source={require('./android/assets/edit.png')}/>     */}
                  <Image style={{height: 20, width: 20}} source={require('../../../assets/edit.png')}/>
                </TouchableOpacity>
              </View>          
            </View>
            <View>
              <Text style={s.normalText}>{this.props.userData.userData.shopDetails[0].shopNo}</Text>
              <Text style={s.normalText}>{this.props.userData.userData.shopDetails[0].shopAdress}</Text> 
              <Text style={s.normalText}>{this.props.userData.userData.shopDetails[0].landmark}</Text> 
            </View>   
          </View>
        </View>
      )
    }
  }
	
  
  logoutAction = () => {
    console.warn('while log -- cart--', this.props.cartItem.cartItem);
    axios.post(
      Config.URL + Config.logoutShop,
      {
        shopId: this.props.userData.userData.shopId,
        cart: this.props.cartItem.cartItem
      },
      {
          headers: {
              'Content-Type': 'application/json',
          }
      }
      )
      .then(res => {
          console.warn("res",res);
          if (res.data.status == 0) {
            Toast.show("Logout successfully!!",Toast.LONG);
            this.handleLogout();
            
          }
          else if(res.data.status == 1){  //
            Toast.show("Failed to logout. Please try again",Toast.LONG);
          }
      }).catch(err => {
        console.warn('error in logout', err)
        Toast.show("Failed to logout. Please try again",Toast.LONG);
      })

  }

  signOut = () => {
    this.props.userData.userData.token = '';
    this.props.loginActions.loginSuccess(this.props.userData.userData);
    this.props.walletAction.updateWallet(0, true);
    this.props.addCartActions.replaceCart([]);
    this.props.cartModalAction.changeCartState(false,0,0);
    this.props.cartModalAction.changeOrderState(false); 
    
    const resetAction = StackActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName: 'login'})]
    })
    this.props.navigation.dispatch(resetAction);
  }

  handleLogout = async () => {
    try {
      await GoogleSignin.revokeAccess();
      await GoogleSignin.signOut();
      setUserInfo(null); // Remember to remove the user from your app's state as well
      this.signOut();
    } catch (error) {
      console.warn(error);
      this.signOut();
    }
  };  

  downloadInvoice = (url) => {
    console.warn('rl --', url);
    if (url) {
      Linking.openURL(url).catch((err) =>
      Toast.show('Not able to download invoice. Please contact support team',Toast.LONG));
    } else {
      Toast.show('Not able to download invoice. Please contact support team',Toast.LONG);
    }
    
  }

  PastOrders = () => {
    if (this.state.pastOrderData.length > 0) {
      return (
        <View style={{backgroundColor:"white",marginTop:5}}>
          <View style={{flex:1,paddingVertical:10,paddingHorizontal:15}}>
            <View style={{marginBottom:10}}>
              <Text style={s.headerText}>Past Orders</Text>
            </View>
              <View>
                <FlatList data={this.state.pastOrderData}   
                  renderItem={( { item } ) => (
                    <PastOrdersListShop
                      item = {item}
                      self = {this}
                    // navigation={this.props.navigation}
                    />
                  )}
                  keyExtractor= {item => item.orderId}
                  extraData = {this.state.pastOrderData} 
                />
              </View>
          </View>
        </View> 
      )
    } else {
      return null;
    }
  }

  PastServedOrders = () => {
    if (this.state.servedOrder.length > 0) {
      return (
        <View style={{backgroundColor:"white",marginTop:5}}>
          <View style={{flex:1,paddingVertical:10,paddingHorizontal:15}}>
            <View style={{marginBottom:10}}>
              <Text style={s.headerText}>Customer Orders</Text>
            </View>
              <View>
                <FlatList data={this.state.servedOrder}   
                  renderItem={( { item } ) => (
                    <PastServedOrdersListShop
                      item = {item}
                      self = {this}
                    // navigation={this.props.navigation}
                    />
                  )}
                  keyExtractor= {item => item.orderId}
                  extraData = {this.state.servedOrder} 
                />
              </View>
          </View>
        </View> 
      )
    } else {
      return null;
    }
  }

  
  render(){
    const {navigate} = this.props.navigation;
    return (
      <Fragment>
        <SafeAreaView style={{flex: 0, backgroundColor: 'white'}}></SafeAreaView>
          <SafeAreaView style={{flex: 1, backgroundColor: '#332b5c'}}>
            <View style={[s.bodyGray]}>
              {/* <InternateModal /> */}
              <ScrollView>
                <View >
                  <View>
                    <View style={{backgroundColor:"white"}}>    
                      <View style={{padding:10 , marginLeft:5 , marginRight:5 , borderBottomWidth:3,borderBottomColor:"#003151"}}>
                        <View style={{flex:1,flexDirection:"row"}}>
                          <View style={{flex:1}}>
                            <Text style={s.headerText}>{this.props.userData.userData.shopDetails[0].shopName}</Text>  
                          </View>
                          {/* <TouchableOpacity style={{flex:1,alignItems:"flex-end"}} onPress={() => navigate('personalInfoPage')}>
                            <Image style={{height: 20, width: 20}} source={require('../../../assets/edit.png')}/>
                          </TouchableOpacity> */}
                        </View>
                        <Text style={s.normalText}>{this.props.userData.userData.shopDetails[0].shopDescription}</Text>
                        <Text style={s.normalText}>{this.props.userData.userData.shopDetails[0].shopContactNumber}</Text>
                      </View>     
                    </View>
                  </View>
                  <this.MyAddress />
                  <this.PastOrders />
                  <this.PastServedOrders />
                  <TouchableOpacity style={{backgroundColor:"white",marginTop:10}} onPress={() => this.navigateToWhatsApp()}>
                      <View style={{flex:1,flexDirection:"row",paddingVertical:10,alignItems:"center",paddingHorizontal:15}}>
                        <Text style={s.headerText}>Contact Us</Text>
                        <View style={{flex:1,alignItems:"flex-end",marginRight:5}}>
                          <Image style={{height:25,width:25}} source={require('../../../assets/whatsappG.png')}/>
                        </View>
                      </View>
                  </TouchableOpacity>
                  <TouchableOpacity style={{backgroundColor:"white",marginTop:10}} onPress={() => navigate('aboutUsPage')}>
                      <View style={{flex:1,flexDirection:"row",paddingVertical:10,alignItems:"center",paddingHorizontal:15}}>
                        <Text style={s.headerText}>About Us</Text>     
                        <View style={{flex:1,alignItems:"flex-end",marginRight:5}}>
                          <Image style={{height:15,width:15}} source={require('../../../assets/next.png')}/>
                        </View>
                      </View>
                  </TouchableOpacity>
                  <TouchableOpacity style={{backgroundColor:"white",marginTop:10}} onPress={() => this.openTermsAndCondition('TnC')}>
                      <View style={{flex:1,flexDirection:"row",paddingVertical:10,alignItems:"center",paddingHorizontal:15}}>
                        <Text style={s.headerText}>Terms & Conditions</Text>
                        <View style={{flex:1,alignItems:"flex-end",marginRight:5}}>
                          <Image style={{height:15,width:15}} source={require('../../../assets/next.png')}/>
                        </View>
                      </View>
                  </TouchableOpacity>
                  <TouchableOpacity style={{backgroundColor:"white",marginTop:10}} onPress={() => this.openTermsAndCondition('policy')}>
                      <View style={{flex:1,flexDirection:"row",paddingVertical:10,alignItems:"center",paddingHorizontal:15}}>
                        <Text style={s.headerText}>Privacy policy</Text>
                        <View style={{flex:1,alignItems:"flex-end",marginRight:5}}>
                          <Image style={{height:15,width:15}} source={require('../../../assets/next.png')}/>
                        </View>
                      </View>
                  </TouchableOpacity>
                  <TouchableOpacity style={{backgroundColor:"white",marginTop:10,marginBottom:5}} onPress={() => this.logoutAction()}>
                      <View style={{flex:1,flexDirection:"row",paddingVertical:10,alignItems:"center",paddingHorizontal:15}}>
                        <Text style={s.headerText}>Log Out</Text>
                        <View style={{flex:1,alignItems:"flex-end",marginRight:5}}>
                          <Image style={{height:15,width:15}} source={require('../../../assets/next.png')}/>
                        </View>
                      </View>
                  </TouchableOpacity>
                </View>
                </ScrollView>
                <ShopNavigationBar navigate={navigate}></ShopNavigationBar>
              </View>
          
          </SafeAreaView>
        </Fragment>
      
    );
  }

  navigateToWhatsApp = () => {
    try {
      const url = "whatsapp://send?text=&phone=" + this.props.userData.userData.whatsappNumber;
      Linking.canOpenURL(url)
      .then(supported => {
        console.warn('response-->', supported);
        if (!supported) {
          Toast.show('Sorry..We not able to redirect you on call log.',Toast.LONG);
        } else {
          console.warn('response 22-->', supported);
          return Linking.openURL(url)
        }
      })
    } catch(err) {
      console.warn('error response-->', supported);
      Toast.show('Not able to open whats app due to' + err ,Toast.LONG); 
    }
    
  }

  openTermsAndCondition = () => {
    if ('policy') {
      Linking.openURL("https://flash-local-deliver.flycricket.io/privacy.html").catch((err) =>
      Toast.show('Not able to open privacy policy. Please try after sometime',Toast.LONG));
 
    } else {
      Linking.openURL("http://flash-local-deliver.flycricket.io/terms.html").catch((err) =>
      Toast.show('Not able to open terms and conditions. Please try after sometime',Toast.LONG));
 
    }

  }

}


let ScreenWidth = Dimensions.get("window").width;
let ScreenHeight = Dimensions.get("window").height;

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,
    height: ScreenHeight,
    paddingBottom:20,
  },
  addtoCartBtnColor: {
    borderWidth: 2,
    borderColor: '#00CCFF',
  //  backgroundColor: '#00009a',
    //color: 'white',
    marginTop: 10
  },
  storeBtn: {
    paddingVertical: 5,
    marginBottom: 5,
    color: 'white',
    borderRadius: 0,
    width: 150,
  },
  containerIcons:{
    width:30,
    height:30,
    backgroundColor:'blue',
  },
  body: {
    backgroundColor: 'grey',
  },
  mainView: {
    // backgroundColor: '#fff',
    flex: 1,
    //height: ScreenHeight - 135
  },
  categoryFooter:{
    width:ScreenWidth,
    paddingVertical:10,
    height:60,
    backgroundColor:'lightgray',
    display:'flex',
    flexDirection:'row',
    justifyContent:"space-evenly",
    alignItems:"center"
  },

  profileViewData:{
    display:"flex",
    alignItems:"center",
    justifyContent:"space-between",
    flexDirection:"row",
    paddingTop:5
  },
  flexRow:{
    display:'flex',
    flexDirection:'row',
  },
updateprofileBtn:{
  padding:5,
  marginLeft:5,
  backgroundColor:'white',
  borderWidth:1,
  borderColor:"lightgray"
},
  profileLayout: {
    height: 150,
    width: ScreenWidth,
    borderBottomLeftRadius: ScreenWidth/2,
    borderBottomRightRadius: ScreenWidth/2,
    borderColor: 'black',
    backgroundColor: 'skyblue',
    position:'relative',
    justifyContent: 'center',
    zIndex:1,
  },
  profilemage:{
    position:'absolute',
    alignSelf: 'center',
    bottom:'-30%',
    height:100,
    width:100,
    borderRadius:50,
    backgroundColor:'darkblue',
  
  },
  profileHeading: {
    color: 'black',
    fontSize: 20,
    fontWeight:"bold"
  },
  profileText:{
    color: 'black',
    fontSize: 20,
    fontWeight:'600',
  },
  profileData:{ 
    borderBottomWidth:1,
    borderBottomColor:'grey',
    paddingVertical:5,
    marginBottom:10
 },
 profileAboutView: {
  marginHorizontal: 20,
  paddingVertical:5,
  // flex: 3,
  backgroundColor: '#fff',
},
  profileMainView: {
    marginHorizontal: 20,
    paddingTop:20,
    // flex: 3,
    backgroundColor: '#fff',
  },
  profilePartition:{
    width:ScreenWidth,
    height:20,
    backgroundColor: 'lightgrey',
    marginTop:10
  },

  // mk start --
  profileHeading: {
    color: 'black',
    fontSize: 20,
    fontWeight:"bold"
  },

});


//--------------------------------------------------- store related fuction  -- Start

const mapStateToProps = (state) => {
	return {
    userData: state.loginData,
    cartItem: state.cartItem,
    cartState: state.cartState,
	};
}

const mapDispatchToProps = (dispatch) => {
    return {
      loginActions: bindActionCreators(loginSuccess,dispatch),
      navigationAction: bindActionCreators(navigateAction,dispatch),
      addCartActions: bindActionCreators(addToCart, dispatch),
      cartModalAction: bindActionCreators(cartModalAction,dispatch),
      walletAction: bindActionCreators(walletAction,dispatch),
    };
}

// ---------------------------------------------------- store related function  -- END

export default connect(mapStateToProps, mapDispatchToProps)(ShopProfilePage);