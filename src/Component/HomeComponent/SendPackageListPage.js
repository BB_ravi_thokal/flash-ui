/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Fragment } from 'react';
import DatePicker from 'react-native-datepicker';

import * as Config from '../../Constant/Config.js';
import * as Constant from '../../Constant/Constant.js';
import { NavigationBar } from '../ReusableComponent/NavigationBar.js';
import { connect } from 'react-redux';
import * as activityAction  from '../../Actions/ActivityAction';
import * as walletAction  from '../../Actions/WalletAction';
import { bindActionCreators } from 'redux';
import  s  from '../../sharedStyle.js';
import InternateModal from '../ReusableComponent/InternetModal';
import * as cartModalAction  from '../../Actions/CartModalAction';
import * as navigateAction  from '../../Actions/NavigationBarAction';
// import Paytm from 'react-native-paytm';
import Toast from 'react-native-simple-toast';
import AnimatedLoader from "react-native-animated-loader";
import PopupModal from '../ReusableComponent/PopupComponent.js';
import callAPI from '../../CoreFunctions/callAPI';
import * as GetPackageAction  from '../../Actions/GetPackageAction';
// import DateTimePicker from '@react-native-community/datetimepicker';
// import 'bootstrap/dist/css/bootstrap.min.css';
import {
    SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  Dimensions,
  TouchableOpacity,
  TextInput,
  ToastAndroid,
  Image,
  FlatList,
  KeyboardAvoidingView
} from 'react-native';

import axios from 'axios'; 


class SendPackageList extends React.Component{

  constructor(props){
    super(props);    
    this.state = {
        currentActivity: {},
        activityTag: "",
        pickupDate: new Date(),
        todaysDate: new Date(),
        // deliveryDate: new Date(),
        pickupAddress: '',
        deliveryAddress: "",
        packageDescription: "",
        pickupAdd: {},
        deliveryAdd: {},
        calledPickAdd: false,
        calledDeliveryAdd: false,
        contactPersonName: "",
        contactPersonMobNo: null,
        disableCheckout: true,
        isNewActivity: true,
        isFormValid: true,
        pickDateBorder: 0,
        pickDateColor: "white",
        delDateBorder: 0,
        delDateColor: "white",
        delAddBorder: 0,
        delAddColor: "white",
        checkMashObj: {},
        checkoutBtnTag: 'Check charges',
        showLoader: false,
        toPay: 0.00,
        totalDistance: '',
        info: '',
        isEdit: false,
        modalVisible: false,
        paytmUrl: Config.paytmViewURL,
        payableAmount: 0.00,
        amountUsedFromWallet: 0.00,
        showDate: false,
        showTime: false,
        // showDeliveryDate: false,
        // showDeliveryTime: false,
        dateOne: new Date(),
        // dateSecond: new Date()
    }
    
  }

  componentDidMount() {
    console.warn("in sendpackage page",this.props.userData.userData);
    console.warn("this.props.activityData.ongoingActivity", this.props.activityData.ongoingActivity);
    // this.props.activityData.deliveryAddress = {};
    // this.props.activityAction.ongoingActivity(this.props.activityData.ongoingActivity);

    if (this.props.activityData.placedActivity.length > 0) {
        this.setState ({
            isNewActivity: false
        })
    }
    // Paytm.addListener(Paytm.Events.PAYTM_RESPONSE, this.handlePaytmResponse);

  }

    

  calculateDeliveryCharge = async() => {
    console.warn('in calculateDeliveryCharge', this.state.pickupDate);
        this.setState({showLoader: true});
        const tempObj = {
            activityId : new Date().getTime(),
            pickupDate: this.state.pickupDate,
            // deliveryDate: this.state.deliveryDate,   
            pickupAddress: this.props.activityData.pickupAddress,
            isPlaced: true,
            deliveryCharge: parseFloat(this.props.userData.userData.activity_delivery_charge),
            toPay: parseFloat(this.props.userData.userData.activity_delivery_charge),
            activityDetails: this.props.activityData.ongoingActivity,
            userDetails: this.props.userData.userData.userDetails[0],
            // paytmResponse: paytmObj
        }
        console.warn('final req body==>', tempObj);
        axios.post(
            Config.URL + Config.calculatePriceForActivity,
            {
                "order": tempObj,
                "activityType": 'Get',
            },
            {
              headers: {
                  'Content-Type': 'application/json',
              }
            }
        )
        .then(res => {
            console.warn('response cal price--->', res.data.distance);
            this.setState({showLoader: false});
            if (res.data.status == 0) {  
            this.setState({
                isEdit: true,
                checkoutBtnTag: 'Checkout',
                info: res.data.infoMessage,
                toPay: res.data.price,
                totalDistance: res.data.distance
            });
            if (parseFloat(this.props.walletBalance.walletBalance) > 0) {
                if (parseFloat(res.data.price) - parseFloat(this.props.walletBalance.walletBalance) > 0) {
                    this.setState({
                        payableAmount: parseFloat(res.data.price) - parseFloat(this.props.walletBalance.walletBalance),
                        amountUsedFromWallet: parseFloat(res.data.price) - (parseFloat(res.data.price) - parseFloat(this.props.walletBalance.walletBalance))
                    })
                } else {
                    this.setState({
                        payableAmount: 0.00,
                        amountUsedFromWallet: parseFloat(res.data.price)
                    })
                }
            } else {
                this.setState({
                    payableAmount: parseFloat(res.data.price)
                });
            }
            this.props.activityAction.ongoingActivity(this.props.activityData.ongoingActivity);
            
            this.showToaster(res.data.message);
            }
            else if(res.data.status == 1){  
                this.showToaster(res.data.message);
            }
        }).catch(err => {
            console.warn('cal price -->', err);
            this.showToaster(res.data.message);
        }) 
    }


  componentWillUnmount() {
    // Paytm.removeListener(Paytm.Events.PAYTM_RESPONSE, this.handlePaytmResponse);
    this.props.activityData.ongoingActivity = [];
    this.props.activityData.pickupAddress = {};
    this.props.activityData.deliveryAddress = {};
    this.props.activityAction.pickupAddress(this.props.activityData.pickupAddress);
    this.props.activityAction.deliveryAddress(this.props.activityData.deliveryAddress);
    this.props.activityAction.ongoingActivity(this.props.activityData.ongoingActivity);
    this.props.GetPackageAction.pickupAddress({});
    this.props.GetPackageAction.deliveryAddress({});
  }

//   handlePaytmResponse = (resp) => {
//     let paytmRes;
//     if (Platform.OS == 'android') {
//       paytmRes = resp;
//     } else {
//       paytmRes = JSON.parse(resp.response);
//     }

//     if (paytmRes.STATUS == 'TXN_FAILURE') {
//         if (paytmRes.RESPCODE == "141") {
//           Toast.show('You cancelled the transaction',Toast.LONG);
//         } else {
//           this.placedActivity(paytmRes, 'Failed');  
//         }
//       }  else if (paytmRes.STATUS == 'TXN_SUCCESS'){
//         this.placedActivity(paytmRes, 'Completed');
//       } else {
//         if (paytmRes.RESPCODE == "141") {
//           Toast.show('You cancelled the transaction',Toast.LONG);
//         } else {
//           this.placedActivity(paytmRes, 'Failed');  
//         }
//     }
//   };

  
  ActivityListSend = (data) => {
    const {navigate} = this.props.navigation;
      if(this.props.activityData.ongoingActivity.length >= 1) {
        return(
            <View>
                {!this.state.isEdit
                ?
                <View style={{flex: 1, flexDirection: "row"}}>
                <TouchableOpacity style={{flex: 11,alignItems:"flex-end", paddingBottom: 5}}
                    onPress={() => navigate('sendPackage',{"tagId": data.tagId})}>
                    <Image style={{height: 20, width: 20}} source={require('../../../assets/edit.png')}></Image>
                    
                </TouchableOpacity>
                <TouchableOpacity style={{flex: 1,alignItems:"flex-end",paddingRight: 20, paddingBottom: 5}}
                    onPress={() => this.removeActivity(data.tagId)}>
                    <Image style={{height: 20, width: 20}} source={require('../../../assets/delete.png')}></Image>
                </TouchableOpacity>
                </View>
                : null

                }
                
                <TextInput 
                    style={[styles.inputBox,s.subHeaderText,{elevation: 4, paddingHorizontal: 15}]}
                    value={data.activityTag }
                    underlineColorAndroid="transparent"
                    editable={false}
                /> 
                <View style={{alignItems:"center"}}>
                    <Image style={{height: 30,width: 10,alignItems:"center"}} source={require('../../../assets/verticalDots.png')}/>
                </View>
            </View>
        )
      } else {
          return null;
      }

  }


  PickUpAddress = () => {
    const {navigate} = this.props.navigation;
    const addressId = this.props.activityData.pickupAddress.addressId;
    if(Object.keys(this.props.activityData.pickupAddress).length > 0) {
        return (
            <View>
                <View style={{flex: 1,flexDirection:"row"}}>
                    <Text style={[s.subHeaderText,{flex: 8,paddingBottom: 5, marginHorizontal: 15}]}>Pickup Address</Text>
                    {!this.state.isEdit
                    ?
                    <TouchableOpacity style={{flex: 4,alignItems:"flex-end",paddingRight: 20}} 
                        onPress={() => navigate('addressPage',{"addressId": addressId,"callingPage": "pickAddress"})}>
                        <Image style={{height: 20, width: 20}} source={require('../../../assets/edit.png')}/>    
                    </TouchableOpacity>
                    : null

                    }
                    
                </View>
                <View style={{paddingVertical: 10, paddingHorizontal: 10, backgroundColor:"#f1f1f1",borderRadius: 10, elevation: 4, marginHorizontal: 15}}>
                    <Text style={[s.normaltext]}> {this.props.activityData.pickupAddress.flatNumber} , {this.props.activityData.pickupAddress.addressLine1}</Text>
                    <Text style={[s.normaltext]}> {this.props.activityData.pickupAddress.addressLine2}</Text>
                    {/* <Text style={[s.normaltext,{fontWeight: "bold"}]} numberOfLines={2}> {this.props.activityData.pickupAddress.state},{this.props.activityData.pickupAddress.state}, 
                        {this.props.activityData.deliveryAddress.pincode}</Text> */}
                </View>
            </View>
            
        )
    } else {
        return (
            <View>
                <View style={{flex: 1,flexDirection:"row"}}>
                    <Text style={[s.subHeaderText,{flex: 8,paddingBottom: 5, marginHorizontal: 15}]}>Pickup Address</Text>
                </View>
                <TouchableOpacity style={{flex: 1,alignItems:"center",backgroundColor:"#f1f1f1",borderRadius: 10, elevation: 4, marginHorizontal: 15,
                    borderWidth: this.state.delAddBorder, borderColor: this.state.delAddColor }}
                    onPress={() => navigate('addressPage',{"addressId": "","callingPage": "pickAddress"})}>
                    <View style={[{display:"flex",alignItems:"center",paddingVertical: 15}]} >
                        <Text style={{ color:'#00CCFF',fontSize: 16,fontWeight:"bold",justifyContent:"center"}}> Add Address</Text> 
                    </View>
                </TouchableOpacity>
            </View>
        )
    }  
  }

  displayNewActivity = () => {
      this.setState({
          isNewActivity: true
      })
  }

  allowEdit = () => {
      this.setState({
        toPay: '',
        totalDistance: '',
        info: '',
        isEdit: false,
      });
      this.props.activityAction.ongoingActivity(this.props.activityData.ongoingActivity);
  }

  removeActivity = (tagId) => {
    const index = this.props.activityData.ongoingActivity.findIndex(ind => ind.tagId === tagId);
    this.props.activityData.ongoingActivity.splice(index, 1);
    this.props.activityAction.ongoingActivity(this.props.activityData.ongoingActivity);
  }

  hideModal = () => {
    this.setState({ 
      modalVisible: false,
    });
  }

  render() {
    const {navigate} = this.props.navigation;
        return ( 
            <SafeAreaView style={{flex: 1}}>
                <View style={[s.bodyGray,{flex: 1}]}>
                <AnimatedLoader
                    visible={this.state.showLoader}
                    overlayColor="rgba(255,255,255,0.75)"
                    source={require("../../../assets/loader.json")}
                    animationStyle= {[styles.lottie,{height: 150,width: 150}]}
                    speed={1}
                />
                    {/* <InternateModal /> */}
                    <ScrollView showsVerticalScrollIndicator = {false}>
                        <View style={styles.advertiseData}>
                            <Image style={styles.imageView} source={{uri: 'https://flash-catagory-image.s3.ap-south-1.amazonaws.com/ActivityHeader-Get.png'}}/>
                        </View>
                        <View style={{marginHorizontal: 10, marginVertical: 20,paddingBottom: 100, backgroundColor:"white", borderRadius: 30, paddingVertical: 20, elevation: 4}}>
                            <KeyboardAvoidingView behavior="padding" >
                                {this.state.isEdit 
                                ?
                                <View style={{flex: 1, alignItems:"flex-end"}}>
                                    <View style={{flex:5,alignItems:"center",justifyContent:"center", marginRight: 10}}>
                                        <TouchableOpacity style={{alignItems: "center", paddingVertical: 10, paddingHorizontal: 20, borderRadius: 20, backgroundColor:"#00CCFF"}} 
                                            onPress={() => this.allowEdit()}>
                                            <Text style={[s.subHeaderText,{color:"white"}]}>Edit</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                                : null
                                }
                                
                                <Text style={[s.subHeaderText,{paddingBottom: 5, marginHorizontal: 15}]}>Your Activity List</Text>
                                <FlatList
                                    data={this.props.activityData.ongoingActivity}
                                    renderItem={( { item } ) => (
                                        <this.ActivityListSend
                                            activityTag={item.activityTag}
                                            tagId={item.tagId}
                                        />
                                    )}
                                    keyExtractor= {item => item.tagId}
                                    extraData={this.props}
                                    
                                />
                                <TouchableOpacity  style={[styles.inputBox,{elevation: 6}]} onPress={() => navigate('sendPackage',{"tagId": ""})}
                                    disabled={this.state.isEdit}>
                                    <Text style={[s.subHeaderText, {textAlign: "center", color:"#003151"}]}>Add delivery stop</Text>
                                    {/* <TextInput placeholder="Add delivery stop"
                                        placeholderTextColor='#003151'
                                        style={[s.subHeaderText, {height: 60, textAlign:"center", color:"#003151", width:'100%'}]}
                                        underlineColorAndroid="transparent"
                                        editable={false}
                                    />  */}
                                </TouchableOpacity>
                                
                                <View style={{marginTop : 15 }}>
                                    <Text style={[s.subHeaderText,{paddingBottom: 5,elevation: 6, marginHorizontal: 15}]}>Pickup Date </Text>
                                    <DatePicker
                                        style={[styles.inputBoxDate,{elevation: 4, borderWidth: this.state.pickDateBorder, borderColor: this.state.pickDateColor }]}
                                        date={this.state.pickupDate}
                                        disabled={this.state.isEdit}
                                        mode="datetime"
                                        placeholder="select pickup date"
                                        format="DD-MM-YYYY MM-HH"
                                        minDate={this.state.todaysDate}
                                        confirmBtnText="Confirm"
                                        cancelBtnText="Cancel"
                                        customStyles={{
                                            dateIcon: {
                                                // position: 'absolute',
                                                // left: 0,
                                                // top: 4,
                                                // marginLeft: 0
                                            },
                                            dateInput: {
                //                               marginLeft: 36
                                            }
                                        }}
                                        onDateChange={(date) => this.setState({
                                            pickupDate: date,
                                            pickDateBorder: 0,
                                            pickDateColor: "white"
                                        })}
                                    />
                                     {/* <TouchableOpacity  style={[styles.inputBox,{elevation: 6}]} onPress={() => this.setState({ showDate: true })}
                                        disabled={this.state.isEdit}>
                                        <TextInput placeholder="Select pickup date"
                                            placeholderTextColor='#003151'
                                            style={[s.subHeaderText, {height: 60, textAlign:"center", color:"#003151", width:'100%'}]}
                                            underlineColorAndroid="transparent"
                                            value={this.state.pickupDate}
                                            editable={false}
                                        /> 
                                    </TouchableOpacity>  
                                    {this.state.showDate
                                    ?
                                        <DateTimePicker
                                            testID='dateTimePicker'
                                            value={this.state.dateOne}
                                            mode='date'
                                            is24Hour={false}
                                            onChange={(time, abc) => this.pickupDateChange(time, abc)}
                                        />
                                    :
                                        null
                                    }
                                    {this.state.showTime
                                    ?
                                        <DateTimePicker
                                            testID='dateTimePicker'
                                            value={this.state.dateOne}
                                            mode='time'
                                            is24Hour={false}
                                            onChange={(time, abc) => this.pickupTimeChange(time, abc)}
                                        />
                                    :
                                        null
                                    }
                                     */}
                                </View>
                                {/* <View style={{marginTop : 15 }}>
                                    <Text style={[s.subHeaderText,{paddingBottom: 5,elevation: 6, marginHorizontal: 15}]}>Delivery Date</Text> */}
                                    {/* <DatePicker
                                        style={[styles.inputBoxDate,{elevation: 4, borderWidth: this.state.delDateBorder, borderColor: this.state.delDateColor}]}
                                        date={this.state.deliveryDate}
                                        disabled={this.state.isEdit}
                                        mode="date"
                                        placeholder="select deliery date"
                                        format="DD-MM-YYYY"
                                        minDate={this.state.todaysDate}
                                        confirmBtnText="Confirm"
                                        cancelBtnText="Cancel"
                                        customStyles={{
                                            dateIcon: {
                                                // position: 'absolute',
                                                // left: 0,
                                                // top: 4,
                                                // marginLeft: 0
                                            },
                                            dateInput: {
                                                // marginLeft: 36
                                            }
                                        }}
                                        onDateChange={(date) => this.setState({
                                            deliveryDate: date, 
                                            delDateBorder: 0,
                                            delDateColor: "white"
                                        })}
                                    /> */}
                                     {/* <TouchableOpacity  style={[styles.inputBox,{elevation: 6}]} onPress={() => this.setState({ showDeliveryDate: true })}
                                        disabled={this.state.isEdit}>
                                        <TextInput placeholder="Select delivery date"
                                            placeholderTextColor='#003151'
                                            style={[s.subHeaderText, {height: 60, textAlign:"center", color:"#003151", width:'100%'}]}
                                            underlineColorAndroid="transparent"
                                            value={this.state.deliveryDate}
                                            editable={false}
                                        /> 
                                    </TouchableOpacity>  
                                    {this.state.showDeliveryDate
                                    ?
                                        <DateTimePicker
                                        testID='dateTimePicker2'
                                        value={this.state.dateSecond}
                                        mode='date'
                                        is24Hour={false}
                                        onChange={(time, abc) => this.deliveryDateChange(time, abc)}
                                    />
                                    :
                                        null
                                    }
                                    {this.state.showDeliveryTime
                                    ?
                                        <DateTimePicker
                                        testID='dateTimePicker2'
                                        value={this.state.dateSecond}
                                        mode='time'
                                        is24Hour={false}
                                        onChange={(time, abc) => this.deliveryTimeChange(time, abc)}
                                    />
                                    :
                                        null
                                    }
                                </View>  */}
                                <View style={{marginTop : 15 }}>
                                    <this.PickUpAddress />
                                </View>
                                {this.state.isEdit
                                    ?
                                    <View style={{paddingHorizontal: 15, flex: 1, marginTop: 15}}>
                                        <Text style={[s.normalText]} numberOfLines={5}>Note: {this.state.info}</Text>
                                    </View>
                                    : null
                                }
                                
                            </KeyboardAvoidingView>
                        </View>    
                </ScrollView>  
                {!this.state.isEdit
                    ?
                    <View style={{flexDirection:"row",position:"absolute",bottom:0, marginTop: 15}}>
                        <TouchableOpacity style={{flex:1,alignItems:"center",justifyContent:"center", backgroundColor:"#00CCFF", paddingVertical: 10, paddingHorizontal: 5}} 
                            onPress={() => this.submitForm('checkPrice')}>
                            <Text style={[s.subHeaderText,{color:"white"}]}>Check prices</Text>
                        </TouchableOpacity>
                    </View>
                    : 
                    
                    <View style={{flexDirection:"row",position:"absolute",bottom:0, marginTop: 15}}>
                        <View style={{flex:7,backgroundColor:"#f1f1f1",paddingLeft:15, paddingVertical: 10, paddingHorizontal: 15, justifyContent:"center"}}>
                            <View style={{flexDirection:"row"}}>
                                <Text style={{fontSize: 16,fontWeight: "bold",color:"#131E3A"}}>Total cost: </Text>
                                <Text style={s.normalText}>₹{(this.state.toPay).toFixed(2)}</Text>
                            </View>
                            <View style={{flexDirection:"row"}}>
                                <Text style={{fontSize: 16,fontWeight: "bold",color:"#131E3A"}}>Total distance: </Text>
                                <Text style={s.normalText}>{this.state.totalDistance}</Text>
                            </View>
                            <View style={{flexDirection:"row"}}>
                                <Text style={{fontSize: 16,fontWeight: "bold",color:"#131E3A"}}>Flash wallet: </Text>
                                <Text style={s.normalText}>₹{parseFloat(this.props.walletBalance.walletBalance)}</Text>
                            </View>
                            <View style={{flexDirection:"row"}}>
                                <Text style={{fontSize: 16,fontWeight: "bold",color:"#131E3A"}}>To pay: </Text>
                                <Text style={s.normalText}>₹{parseFloat(this.state.payableAmount).toFixed(2)}</Text>
                            </View>
                        </View>
                        <View style={{flex:5,alignItems:"center",justifyContent:"center", backgroundColor:'#f1f1f1'}}>
                            <TouchableOpacity style={{alignItems: "center", paddingVertical: 10, paddingHorizontal: 20, borderRadius: 20, backgroundColor:"#00CCFF"}} 
                                onPress={() => this.submitForm('checkout')}>
                                <Text style={[s.subHeaderText,{color:"white"}]}>Checkout</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                    }
                    
                <PopupModal visible={this.state.modalVisible} onDismiss={this.hideModal} removeProduct={this.checkout} 
                message={"Please verify and then confirm the activity. You won't be able to change it once you submit."} btnTag={"Checkout"}/>
                </View>
            </SafeAreaView>
            
          );
    
  }

    // pickDateChange = (date) => {
    //     if(date > this.state.deliveryDate) {
    //         this.setState({
    //             isFormValid: false,
    //             pickDateBorder: 1,
    //             pickDateColor: "red"
    //         })
    //         Toast.show('Pickup date must be less than delivery date',Toast.LONG);
    //     } else {
    //         this.setState({
    //             isFormValid: true,
    //             pickDateBorder: 0,
    //             pickDateColor: "white"
    //         })
    //     }
    // }

    // deliveryDateChange = (date) => {
    //     if(date < this.state.pickupDate) {
    //         this.setState({
    //             isFormValid: false,
    //             delDateBorder: 1,
    //             delDateColor: "red"
    //         })
    //         Toast.show('Delivery date must be greater than pickup date',Toast.LONG);
    //     } else {
    //         this.setState({
    //             isFormValid: true,
    //             delDateBorder: 0,
    //             delDateColor: "white"
    //         })
    //     }
    // }

    // pickupDateChange = async(event, slectedDate) => {
    //     console.warn('event', event);
    //     console.warn('slectedTime', slectedDate);
    //     if (slectedDate == undefined) {
    //         await this.setState({
    //             showDate: false,
    //             showTime: false
    //         });    
    //     } else{
    //         await this.setState({
    //             dateOne: slectedDate,
    //             pickupDate: slectedDate,
    //             showDate: false,
    //             showTime: true
    //         });
    //     }
        

    // }

    // pickupTimeChange = async(event, slectedTime) => {
    //     console.warn('event', event);
    //     console.warn('1', new Date(slectedTime).toLocaleString());
    //     console.warn('get Date', new Date(slectedTime).getDate());
    //     if (slectedTime == undefined) {
    //         await this.setState({
    //             showDate: false,
    //             showTime: false
    //         });
    //     } else {
    //         const date = new Date(slectedTime).getDate();
    //         const month = new Date(slectedTime).getMonth();
    //         const year = new Date(slectedTime).getFullYear();
    //         await this.setState({
    //             dateOne: new Date(year, month, date),
    //             pickupDate: new Date(slectedTime).toLocaleString(),
    //             showDate: false,
    //             showTime: false
    //         });
    //     }
        
    // }

    // deliveryDateChange = async(event, slectedDate) => {
    //     console.warn('event', event);
    //     console.warn('slectedTime', slectedDate);
    //     if (slectedDate == undefined) {
    //         await this.setState({
    //             showDeliveryDate: false,
    //             showDeliveryTime: false
    //         });    
    //     } else {
    //         await this.setState({
    //             dateSecond: slectedDate,
    //             deliveryDate: slectedDate,
    //             showDeliveryDate: false,
    //             showDeliveryTime: true
    //         });
    //     }
        

    // }

    // deliveryTimeChange = async(event, slectedTime) => {
    //     console.warn('event', event);
    //     if (slectedTime == undefined) {
    //         await this.setState({
    //             showDeliveryDate: false,
    //             showDeliveryTime: false
    //         });    
    //     }
    //     const date = new Date(slectedTime).getDate();
    //     const month = new Date(slectedTime).getMonth();
    //     const year = new Date(slectedTime).getFullYear();
    //     await this.setState({
    //         dateSecond: new Date(year, month, date),
    //         deliveryDate: new Date(slectedTime).toLocaleString(),
    //         showDeliveryDate: false,
    //         showDeliveryTime: false
    //     });
    // }

    // validateDate = async() => {
    //     console.warn('pickupdate', this.state.pickupDate);
    //     console.warn('deliveryDate', this.state.deliveryDate);
    //     const pickArray = this.state.pickupDate.split(' ');
    //     const pickTimeArray = pickArray[3];
    //     const pTime = pickTimeArray.split(':');
    //     const delArray = this.state.deliveryDate.split(' ');
    //     const DelTimeArray = delArray[3];
    //     const dTime = DelTimeArray.split(':');
    //     console.warn('dateOne', this.state.dateOne);
    //     console.warn('dateSecond', this.state.dateSecond);
    //     if (this.state.dateSecond > this.state.dateOne) {
    //         console.warn('ok')
            
    //         await this.setState({
    //             isFormValid: true,
    //             pickDateBorder: 0,
    //             pickDateColor: "white",
    //             delDateBorder: 0,
    //             delDateColor: "white"
    //         });
    //         return true;
    //     } else if (this.state.dateSecond < this.state.dateOne) {
    //         console.warn('date not ok');
    //         Toast.show('Please check the pickup date. It should not be more than delivery date',Toast.LONG);
    //         await this.setState({
    //             pickDateBorder: 1,
    //             pickDateColor: "red",
    //             delDateBorder: 1,
    //             delDateColor: "red"
    //         });
    //         return false;
    //     } else {
    //         console.warn('deliveryTimeArray[0]', dTime[0]);
    //         console.warn('pickTimeArray[0]', pTime[0]);
    //         if (dTime[0] > pTime[0]) {
    //             console.warn('time bhi ok');
    //             await this.setState({
    //                 pickDateBorder: 0,
    //                 pickDateColor: "white",
    //                 delDateBorder: 0,
    //                 delDateColor: "white"
    //             });
    //             return true;
    //         } else {
    //             Toast.show('Please check the delivery date & time. It should be at least 1 hour more than pickup time',Toast.LONG);
    //             await this.setState({
    //                 pickDateBorder: 1,
    //                 pickDateColor: "red",
    //                 delDateBorder: 1,
    //                 delDateColor: "red"
    //             });
    //             return false;
    //         }
    //     }
    // }




    

    submitForm = async(type) => {
        console.warn('pickupDate', this.state.pickupDate);
        // console.warn('delivery date', this.state.deliveryDate);
        await this.setState({ isFormValid: true });
        // const checkDate = await this.validateDate();
        // if (checkDate) {
            if(Object.keys(this.props.activityData.deliveryAddress).length > 0) {
                await this.setState({
                    isFormValid: true,
                    delAddBorder: 0,
                    delAddColor: "white"
                })
            }
            if(this.props.activityData.ongoingActivity.length < 1) {
                Toast.show('You have not added activity yet',Toast.LONG);
                await this.setState({
                    isFormValid: false,
                })
            } else if(this.state.pickupDate == '' || this.state.pickupDate == undefined) {
                await this.setState({
                    isFormValid: false,
                    pickDateBorder: 1,
                    pickDateColor: "red"
                })
                Toast.show('please add the pickup date & time',Toast.LONG);
            } else if(Object.keys(this.props.activityData.pickupAddress).length <= 0) {
                await this.setState({
                    isFormValid: false,
                    delAddBorder: 1,
                    delAddColor: "red"
                })
                Toast.show('please add the delivery address',Toast.LONG);
            } else if(this.state.isFormValid) {
                // this.checkout();
                if (type == 'checkout') {
                    this.setState({modalVisible: true});
                    // this.checkout();
                } else {
                    console.warn('hete how--->', this.state.isFormValid);
                    this.calculateDeliveryCharge();
                }
            }
    } 


    checkout = async() => {
        console.warn('parseFloat(this.state.amountUsedFromWallet)', parseFloat(this.state.amountUsedFromWallet));
        await this.setState({modalVisible: false});
        console.warn('this.props.userData.userData.userDetails[0].contactNumber', this.props.userData.userData.userDetails[0].contactNumber);
        if (!this.props.userData.userData.userDetails[0].contactNumber) {
            this.showToaster('Please add mobile number');
            this.props.navigation.navigate('personalInfoPage', {"callingPage": "getActivity"});
        } else if (this.state.payableAmount <= 0 ){
            this.placeActivity();
        } else {
            this.saveOrderToDB();
            // this.initiatePaymentIOS();
        }
    }

    showToaster = (message) => {
        setTimeout(() => {
            Toast.show(message, Toast.LONG);
        }, 100);
    }

    placeActivity = async() => {
        const activityObject = await this.getActivityObject();
        var reqBody = {
            order: activityObject,
            userId: this.props.userData.userData.userId,
            activityType: 'Get',
            amountUsedFromWallet: parseFloat(this.state.amountUsedFromWallet),
            paymentStatus: 'Completed',
            isNewVersion: true
        }
        console.warn('before place API req body--->', reqBody);
        const placeOrder = await callAPI.axiosCall(Config.placedOrder, reqBody);
        this.setState({showLoader: false});
        console.warn('place order response====>', placeOrder);
        if (placeOrder) {
            if(placeOrder.data.status == 0) {
                this.showToaster(placeOrder.data.message);  
                this.mangeStore(placeOrder);
            } else {
                this.setState({showLoader: false});
                this.showToaster(placeOrder.data.message);
            }
        } else {
            await this.setState({showLoader: false});
            this.showToaster(placeOrder.data.message);
        }
    }

    mangeStore = async(res) => {
        // this.props.GetPackageAction.placedActivity(tempObj);
        this.props.activityData.ongoingActivity = [];
        this.props.activityData.pickupAddress = {};
        this.props.activityData.deliveryAddress = {};
        this.props.getPackageData.pickupAddress = {};
        this.props.getPackageData.deliveryAddress = {};
        this.props.activityAction.ongoingActivity(this.props.activityData.ongoingActivity);
        this.props.GetPackageAction.pickupAddress(this.props.getPackageData.pickupAddress);
        this.props.GetPackageAction.deliveryAddress(this.props.getPackageData.deliveryAddress);
        this.showToaster('Your Activity placed successfully');
        if (res) {
            this.props.walletAction.updateWallet(res.data.wallet, true);
        }
        this.props.cartModalAction.changeGetActivityState(true);
        this.props.navigationAction.navigateAction('Home');
        this.props.navigation.navigate('homePage');
    }

    saveOrderToDB = async () => {
        try {
            const activityObject = await this.getActivityObject();
            const order = {
            "userId": this.props.userData.userData.userId,
            "order": activityObject,
            }
            const reqBody = {
                order: order,
                activityType: "Get"
            }
            const placeOrder = await callAPI.axiosCall(Config.saveOrderActivityDetails, reqBody);
            if (placeOrder) {
                console.warn('place order--->', placeOrder.data);
                this.initiatePaymentIOS(placeOrder.data.orderId);
            } else {
                setTimeout(() => {
                    Toast.show('Failed to open payment gateway. Please try again later', Toast.LONG);
                }, 100);
            }
        } catch (err) {
            this.setState({showLoader: false});
        }
    }

    initiatePaymentIOS = async(orderId) => {
        // const activityObject = await this.getActivityObject();
        // const order = {
        //   "userId": this.props.userData.userData.userId,
        //   "order": activityObject,
        // }
        console.warn('orderId ===>', orderId)
        // await this.setState({
        //   paytmUrl: this.state.paytmUrl + 'orderId=' + orderId + '&userId=' + this.props.userData.userData.userId
        //   + '&totalAmount=' + 100 + '&activityType=Get' + '&amountUsedFromWallet=' + this.state.amountUsedFromWallet
        //   + '&bidId='
        // });
        const paytmUrl = this.state.paytmUrl + 'orderId=' + orderId + '&userId=' + this.props.userData.userData.userId
            + '&totalAmount=' + this.state.payableAmount + '&activityType=Get' + '&amountUsedFromWallet=' + this.state.amountUsedFromWallet
            + '&bidId=';
        console.warn('called---->');
        this.props.navigationAction.navigateAction('Home');
        this.props.navigation.navigate('paytmViewPage', {"paytmUrl": paytmUrl, "callingPage": 'sendActivity'});
    }

    // initiatePaymentAndroid = () => {
    //     this.setState({showLoader: true});
    //     axios.post( 
    //         Config.URL + Config.checkout,
    //         {
    //             "userId": this.props.userData.userData.userId,
    //             "totalAmount": this.state.toPay,
    //             "activityType": 'Get'
    //         },
    //         { 
    //             headers: {
    //                 'Content-Type': 'application/json',
    //             }
    //         }
    //     )
    //     .then(res => {
    //         this.setState({showLoader: false})
    //         const details = {
    //             // mode: "Production",
    //             // mode: "Staging",
    //             mode: res.data.data.mode,
    //             MID: res.data.data.MID,
    //             ORDER_ID: res.data.data.ORDER_ID,
    //             CUST_ID: res.data.data.CUST_ID,
    //             INDUSTRY_TYPE_ID: res.data.data.INDUSTRY_TYPE_ID,
    //             CHANNEL_ID: res.data.data.CHANNEL_ID,
    //             TXN_AMOUNT: res.data.data.TXN_AMOUNT,
    //             WEBSITE: res.data.data.WEBSITE,
    //             MOBILE_NO: res.data.data.MOBILE_NO,
    //             EMAIL : res.data.data.EMAIL,
    //             CALLBACK_URL: res.data.data.CALLBACK_URL,
    //             CHECKSUMHASH: res.data.data.CHECKSUMHASH
    //         };
    //         this.setState({checkMashObj: details});
    //         try {
    //             // Paytm.startPayment(details); //un-comment for Android
    //         } catch(err) {
    //             Toast.show('Error while initiating the payment gettway. Please try again',Toast.LONG);
    //         }
    //     });
    // }

    getActivityObject = async(paytmObj) => {
        console.warn('this.props.activityData.pickUpAddress', this.props.activityData.pickupAddress);
        let activityObject = {
                activityId : new Date().getTime(),
                pickupDate: this.state.pickupDate,
                // deliveryDate: this.state.deliveryDate,   
                pickupAddress: this.props.activityData.pickupAddress,
                isPlaced: true,
                deliveryCharge: parseFloat(this.state.toPay),
                toPay: parseFloat(this.state.toPay),
                activityDetails: this.props.activityData.ongoingActivity,
                userDetails: {
                    fullName: this.props.userData.userData.userDetails[0].fullName,
                    contactNumber: this.props.userData.userData.userDetails[0].contactNumber 
                }
            }
        console.warn('activityObject --->', activityObject);    
        return activityObject;
        
    }

    // placedActivity = async(paytmObj, paymentStatus) => {
    //     this.setState({showLoader: true});
    //     const activityObject = await this.getActivityObject();
    //     axios.post(
    //         Config.URL + Config.placedOrder,
    //         {
    //             "orderId": this.state.checkMashObj.ORDER_ID,
    //             "userId": this.props.userData.userData.userId,
    //             "order": activityObject,
    //             "activityType": 'Get',
    //             "paymentStatus" : paymentStatus,
    //             "MID": this.state.checkMashObj.MID,
    //             "CHECKSUMHASH": this.state.checkMashObj.CHECKSUMHASH
    //         },
    //         {
    //           headers: {
    //               'Content-Type': 'application/json',
    //           }
    //         }
    //     )
    //     .then(res => {
    //         this.setState({showLoader: false});
    //         if (res.data.status == 0) {  
                
    //         this.props.activityAction.placedActivity(tempObj);
    //         this.props.activityData.ongoingActivity = [];
    //         this.props.activityData.pickupAddress = {};
    //         this.props.activityData.deliveryAddress = {};
    //         this.props.activityAction.pickupAddress(this.props.activityData.pickupAddress);
    //         this.props.activityAction.deliveryAddress(this.props.activityData.deliveryAddress);
    //         this.props.activityAction.ongoingActivity(this.props.activityData.ongoingActivity);
    //         this.props.cartModalAction.changeGetActivityState(true);
    //         this.props.navigationAction.navigateAction('Home');
    //         this.props.navigation.navigate('homePage');
    //         Toast.show('Your activity is placed successfully',Toast.LONG);
    //         }
    //         else if(res.data.status == 1){  
    //         Toast.show('Failed to place the activity please try again',Toast.LONG);
    //         }
    //     }).catch(err => {
    //         Toast.show('Failed to place the activity please try again',Toast.LONG);
    //     }) 

    // }
}

let ScreenWidth = Dimensions.get("window").width;
let ScreenHeight = Dimensions.get("window").height;

const styles = StyleSheet.create({

    advertiseData: {
        height: 220,
        width: ScreenWidth,
    },
    imageView: {
        flex: 1,
        width: undefined,
        height: undefined,
        resizeMode: 'contain',
    },
    inputBox: {
        justifyContent: 'center',
        alignItems: 'center',
        padding: 10,
        borderRadius: 10,
        backgroundColor:"#f1f1f1",
        paddingHorizontal: 15,
        marginHorizontal: 15,
        height: 40
      },
    inputBoxDate: {
        justifyContent: 'center',
        alignItems: 'center',
        padding: 10,
        borderRadius: 10,
        backgroundColor:"#f1f1f1",
        paddingHorizontal: 15,
        marginHorizontal: 15,
        height: 40,
        width: '90%'
      },
    inputBoxDetails: {
        justifyContent: 'center',
        alignItems: 'center',
        padding: 10,
        borderRadius: 10,
        backgroundColor:"#f1f1f1",
        paddingHorizontal: 15,
        marginHorizontal: 15,
        height: 80
      },
	
	addressCardView: {
		borderRadius: 5,
		marginHorizontal: '2%',
		elevation: 4,
		alignContent: 'center',
		padding: '3%',      
		backgroundColor:"white",
	},

	addressButtonView: {
		backgroundColor:"#00CCFF",  
		width: '80%',
		//borderRadius: 50,
		elevation: 6,
		shadowColor: 'rgba(0, 0, 0, 0.1)',
		shadowOpacity: 0.8,
		height: 40,
		justifyContent: 'center',
		alignItems: 'center',
	},

	addressButtonText: {
		color:"white",  
		fontSize: 16 ,
		fontWeight:"bold"
	},
    

});


//--------------------------------------------------- store related fuction  -- Start

const mapStateToProps = (state) => {
	return {
        activityData: state.activityData,
        userData: state.loginData,
        walletBalance: state.walletBalance,
        getPackageData: state.getPackageData,
	};
}

const mapDispatchToProps = (dispatch) => {
    return {
        activityAction: bindActionCreators(activityAction,dispatch),
        cartModalAction: bindActionCreators(cartModalAction,dispatch),
        navigationAction: bindActionCreators(navigateAction,dispatch),
        walletAction: bindActionCreators(walletAction,dispatch),
        GetPackageAction: bindActionCreators(GetPackageAction,dispatch),
    };
}

// ---------------------------------------------------- store related function  -- END


export default connect(mapStateToProps, mapDispatchToProps)(SendPackageList);