/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Fragment, createRef } from 'react';
import { connect } from 'react-redux';
import * as addToCart  from '../../Actions/CartItem';
import { bindActionCreators } from 'redux';
import axios from 'axios';
import * as Config from '../../Constant/Config.js';
import * as Constant from '../../Constant/Constant.js';
import  NavigationBar  from '../ReusableComponent/NavigationBar.js';
import  s  from '../../sharedStyle.js';
import CartModal from '../ReusableComponent/cartModal';
import OnGoingActivityModal from '../ReusableComponent/OnGoingActivityModal';
import * as basketAction  from '../../Actions/BasketAction';
import * as cartModalAction  from '../../Actions/CartModalAction';
import BasketModal from '../ReusableComponent/basketModal';
import InternateModal from '../ReusableComponent/InternetModal';
import PopupModal from '../ReusableComponent/PopupComponent.js';
import { Rating } from 'react-native-ratings';
import * as navigateAction  from '../../Actions/NavigationBarAction';
import Toast from 'react-native-simple-toast';
import ShopListModal from '../ReusableComponent/ShopListModal';
import DelayInput from "react-native-debounce-input";
// import 'bootstrap/dist/css/bootstrap.min.css';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    Dimensions,
    TouchableOpacity,
    TextInput,
    ToastAndroid,
    Image,
    FlatList,
    BackHandler
} from 'react-native';

import {
  Colors,
} from 'react-native/Libraries/NewAppScreen';

const inputRef = createRef();


class ProductSearch extends React.Component{
    constructor(props) {
        super(props);
        this.state={
          data: [],
          searchText: "",
          searchType: "Product",
          productBBColor: "#0080FE",
          shopBBColor: "#7285A5",
          showBasketModal: false,
          addtoBasketItem: {},
          productData: {},
          basketData: {},
          modalVisible: false,
          cart: {},
          shopModal: false,
          availableShopList: [],
          placeholderText: 'Search product ...',
          shopId: '',
          shopName: '',
          addType: '',
          cartRemoveModal: false,
          selectedProduct: {},
          cartModalMessage: '',
          paddingBottomHeight: 0
        }
        //this.backAndroid = this.backAndroid.bind(this);
    }

    componentDidMount(){
      this.props.navigationAction.navigateAction('Search');
     // enableAndroidBackButton();
      //BackHandler.removeEventListener('hardwareBackPress', this.backAndroid);
      this.props.navigation.addListener('willFocus', () =>{
        console.warn('am in focus Search');
        this.props.navigationAction.navigateAction('Search');
        if (this.props.cartState.cartState) {
          this.setState({paddingBottomHeight: 110});
        } else {
          this.setState({paddingBottomHeight: 0});
        }
      });
      if (this.props.cartState.cartState) {
        this.setState({paddingBottomHeight: 110});
      } else {
        this.setState({paddingBottomHeight: 0});
      }
    }

    ShopListing = (item) => {
      const {navigate} = this.props.navigation;
        if(item.item){
            if(this.state.searchType === "Product") {
              if (item.item.menu.isClickable) {
                return (
                  <View style={[styles.prodcutListingView]}>
                    <TouchableOpacity style={styles.addCardImage} onPress={() => navigate('productDetailsPage', {productData: item.item, shopId: this.state.shopId, shopName: this.state.shopName, category: item.item.categoryId})}>
                        <Image style={styles.productImage} source={{uri: item.item.menu.productLogo}}/>
                      </TouchableOpacity>
                        <TouchableOpacity style={styles.stores} onPress={() => navigate('productDetailsPage', {productData: item.item, shopId: this.state.shopId, shopName: this.state.shopName, category: item.item.categoryId})}>
                          <View style={{flex:10}}>
                            <Text style={s.subHeaderText} numberOfLines={3}>{item.item.menu.productName} </Text>
                          </View>
                          {item.item.menu.mrp != item.item.menu.unitCost 
                            ? 
                            <>
                            <View style={{flex:1, flexDirection:"row", marginTop: 10}}>
                              <Text style={[s.normalText]}>MRP:- </Text>
                              <Text style={[s.normalText,{textDecorationLine: 'line-through', textDecorationStyle: 'solid'}]} numberOfLines={1}>
                                ₹{item.item.menu.mrp}</Text>
                            </View>
                            <View style={{flex:1, flexDirection:"row"}}>
                              <Text style={[s.normalText]}>Flash amount:- </Text>
                              <Text style={[s.normalText]} numberOfLines={1}>₹{item.item.menu.unitCost}</Text>
                            </View>
                            </>
                            : 
                            <View style={{flex:1, flexDirection:"row", marginTop: 10}}>
                              <Text style={[s.normalText]}>Flash amount:- </Text>
                              <Text style={[s.normalText]} numberOfLines={1}>₹{item.item.menu.unitCost}</Text>
                            </View>
                          }
                          {item.item.shopDetails.shopId ?
                          <>
                            <Text style={[s.normalText]} numberOfLines={3}>Shop Name:- {item.item.shopDetails.shopName}</Text>
                            <Text style={[s.normalText]} numberOfLines={1}>{item.item.shopDetails.shopAdress}</Text>
                          </>
                          :
                          null  
                          }
                          <View style={styles.Mystores}>
                            <View style={{paddingTop: 10, flex: 5}}>
                              {/* <Text style={[s.normalText,{textDecorationLine: 'line-through', textDecorationStyle: 'solid'}]} numberOfLines={1}>
                                MRP:- ₹{item.item.menu.unitCost}</Text>
                              <Text style={[s.normalText]} numberOfLines={1}>Flash amount:- ₹{item.item.menu.unitCost}</Text> */}
                              <Text style={[s.normalText]} numberOfLines={3}>Unit:- {item.item.menu.unit}</Text>
                            </View>
                            <View style={styles.flexRow}>
                              <TouchableOpacity style={styles.basketBtn} onPress={() => this.showBasketModal(item.item)}>
                                <Image style={styles.basketImg} source={require('../../../assets/basketIcon.png')}/>
                              </TouchableOpacity>
                              {/* <TouchableOpacity onPress={() => this.addCart(item.item)}>
                                <View style={[styles.storeBtn, styles.addtoCartBtnColor]}>
                                  <Text style={{ color: '#0080FE',paddingHorizontal: 4}}>ADD</Text>
                                </View>
                              </TouchableOpacity> */}
                              {this.props.cartItem.cartItem.findIndex(ind => ind.productId === item.item.menu.productId) >= 0
                              ?
                                <View style={styles.flexRowAdd}>
                                  <TouchableOpacity onPress={() => this.decrementCount(item.item, 'ProductList')}>
                                    <View style={styles.orderCardButton}>
                                      <Text style={{fontSize:18,color:"#0080FE"}}>-</Text>
                                    </View>
                                  </TouchableOpacity>
                                    {/* <Text style={styles.orderCount}>{item.item.counter}</Text> */}
                                    <Text style={styles.orderCount}>{this.props.cartItem.cartItem[this.props.cartItem.cartItem.findIndex(ind => ind.productId === item.item.menu.productId)].counter}</Text>
                                  <TouchableOpacity onPress={() => this.incrementCount(item.item, 'ProductList')}>
                                    <View style={styles.orderCardButton}>
                                      <Text style={{fontSize:18,color:"#0080FE"}}>+</Text>
                                    </View>
                                  </TouchableOpacity>
                                </View>
                              :
                                <TouchableOpacity style={[styles.storeBtn, styles.addtoCartBtnColor]} onPress={() => this.addCart(item.item, 'ProductList')}>
                                  <Text style={{ color: '#0080FE',paddingHorizontal: 4}}>ADD</Text>
                                </TouchableOpacity>

                              }
              
                            </View>
                          </View>
                        </TouchableOpacity>
                       
                    </View>
                      
                  )
                }
                else {
                  return (
                    <View style={[styles.prodcutListingView]}>
                      <View style={styles.addCardImage}>
                        <Image style={styles.productImage} source={{uri: item.item.menu.productLogo}}/>
                      </View>
                        <View style={styles.stores}>
                          <View style={{flex:10}}>
                            <Text style={s.subHeaderText} numberOfLines={3}>{item.item.menu.productName} </Text>
                          </View>
                          {item.item.menu.mrp != item.item.menu.unitCost 
                            ? 
                            <>
                            <View style={{flex:1, flexDirection:"row", marginTop: 10}}>
                              <Text style={[s.normalText]}>MRP:- </Text>
                              <Text style={[s.normalText,{textDecorationLine: 'line-through', textDecorationStyle: 'solid'}]} numberOfLines={1}>
                                ₹{item.item.menu.mrp}</Text>
                            </View>
                            <View style={{flex:1, flexDirection:"row"}}>
                              <Text style={[s.normalText]}>Flash amount:- </Text>
                              <Text style={[s.normalText]} numberOfLines={1}>₹{item.item.menu.unitCost}</Text>
                            </View>
                            </>
                            : 
                            <View style={{flex:1, flexDirection:"row", marginTop: 10}}>
                              <Text style={[s.normalText]}>Flash amount:- </Text>
                              <Text style={[s.normalText]} numberOfLines={1}>₹{item.item.menu.unitCost}</Text>
                            </View>
                          }
                          {item.item.shopDetails.shopId ?
                          <>
                            <Text style={[s.normalText]} numberOfLines={3}>Shop Name:- {item.item.shopDetails.shopName}</Text>
                            <Text style={[s.normalText]} numberOfLines={1}>{item.item.shopDetails.shopAdress}</Text>
                          </>
                          :
                          null  
                          }
                          <View style={styles.Mystores}>
                            <View style={{paddingTop: 10, flex: 5}}>
                              {/* <Text style={[s.normalText,{textDecorationLine: 'line-through', textDecorationStyle: 'solid'}]} numberOfLines={1}>
                                MRP:- ₹{item.item.menu.unitCost}</Text>
                              <Text style={[s.normalText]} numberOfLines={1}>Flash amount:- ₹{item.item.menu.unitCost}</Text> */}
                              <Text style={[s.normalText]} numberOfLines={3}>Unit:- {item.item.menu.unit}</Text>
                            </View>
                            <View style={styles.flexRow}>
                              <TouchableOpacity style={styles.basketBtn} onPress={() => this.showBasketModal(item.item)}>
                                <Image style={styles.basketImg} source={require('../../../assets/basketIcon.png')}/>
                              </TouchableOpacity>
                              {/* <TouchableOpacity onPress={() => this.addCart(item.item)}>
                                <View style={[styles.storeBtn, styles.addtoCartBtnColor]}>
                                  <Text style={{ color: '#0080FE',paddingHorizontal: 4}}>ADD</Text>
                                </View>
                              </TouchableOpacity> */}
                              {this.props.cartItem.cartItem.findIndex(ind => ind.productId === item.item.menu.productId) >= 0
                              ?
                                <View style={styles.flexRowAdd}>
                                  <TouchableOpacity onPress={() => this.decrementCount(item.item)}>
                                    <View style={styles.orderCardButton}>
                                      <Text style={{fontSize:18,color:"#0080FE"}}>-</Text>
                                    </View>
                                  </TouchableOpacity>
                                    {/* <Text style={styles.orderCount}>{item.item.counter}</Text> */}
                                    <Text style={styles.orderCount}>{this.props.cartItem.cartItem[this.props.cartItem.cartItem.findIndex(ind => ind.productId === item.item.menu.productId)].counter}</Text>
                                  <TouchableOpacity onPress={() => this.incrementCount(item.item)}>
                                    <View style={styles.orderCardButton}>
                                      <Text style={{fontSize:18,color:"#0080FE"}}>+</Text>
                                    </View>
                                  </TouchableOpacity>
                                </View>
                              :
                                <TouchableOpacity style={[styles.storeBtn, styles.addtoCartBtnColor]} onPress={() => this.addCart(item.item)}>
                                  <Text style={{ color: '#0080FE',paddingHorizontal: 4}}>ADD</Text>
                                </TouchableOpacity>

                              }
              
                            </View>
                          </View>
                        </View>
                        
                    </View>
                        
                  )
                }
                
            }
            else{
                return (
                  <View style={styles.sectionContainer}>
                  <View style={styles.flexRowSpaceBetween}>
                      <View style={styles.imageCardView}>
                        <Image style={styles.imageCards} source={{uri: item.item.shopMiniLogo}}></Image>
                      </View>
                      <View style={{ flex: 0.2 }} />
                      <TouchableOpacity style={styles.storeCards} onPress={() => navigate('flashHubPage',{"shopId": item.item.shopId})}>
                          <View style={styles.bookCardLayout}>
                              <Text style={s.subHeaderText} numberOfLines = { 3 }>{item.item.shopName}</Text>
                              <Text style={s.normalText} numberOfLines = { 1 }>{item.item.shopDescription}</Text>
                              <Text style={s.normalText} numberOfLines = { 1 }>{item.item.shopAdress}</Text>
                              
                          </View>
                      </TouchableOpacity>
                  </View>
              </View>
                )
    
            }
        }  
    }

    ProductNotAvailable= () => {
      const {navigate} = this.props.navigation;
      if (this.state.searchType === "Product") {
        if (this.state.data.length <= 0 && this.state.searchText == "") {
          return (
            <View style={{flex: 1,alignItems:"center", justifyContent:"center", alignContent:"center", marginTop: 40}}>
              <Text style={[s.normalText, {textAlign:"center"}]} numberOfLines={3}>Search your required product. We will deliver it for you</Text>
              {/* <Image style={styles.imgBlank} source={require('../../../assets/ShopMissing.jpg')}/> */}
            </View>
          )
        } else if (this.state.data.length <= 0) {
          return (
            <View style={{flex: 1,alignItems:"center", justifyContent:"center", alignContent:"center", marginTop: 40}}>
              <Text style={[s.normalText, {textAlign:"center"}]} numberOfLines={3}>Sorry!! Searched product is not available in the list.</Text>
              {/* <TouchableOpacity onPress={() => this.searchDataFunction('', 'Product')}>
                <Text style={[s.normalText, {color: "#00CCFF", paddingTop: 15}]}>clear search</Text>
              </TouchableOpacity> */}
              {/* <Image style={styles.imgBlank} source={require('../../../assets/ShopMissing.jpg')}/> */}
            </View>
          )
        } else {
          return null
        }
      } else {
        if (this.state.data.length <= 0 && this.state.searchText == "") {
          return (
            <View style={{flex: 1,alignItems:"center", justifyContent:"center", alignContent:"center", marginTop: 40}}>
              <Text style={[s.normalText, {textAlign:"center"}]} numberOfLines={3}>Search local shop near you</Text>
              {/* <Image style={styles.imgBlank} source={require('../../../assets/ShopMissing.jpg')}/> */}
            </View>
          )
        } else if (this.state.data.length <= 0) {
          return (
            <View style={{flex: 1,alignItems:"center", justifyContent:"center", alignContent:"center", marginTop: 40, marginHorizontal: 20}}>
              <Text style={[s.normalText, {textAlign:"center"}]} numberOfLines={3}>Sorry for the inconvenience!</Text>
              <Text style={[s.normalText, {textAlign:"center"}]} numberOfLines={3}>The shop that you have searched for is not tied with Flash at the moment</Text>
              <Text style={[s.normalText, {textAlign:"center"}]} numberOfLines={3}>Please search your needs from Flash Mart</Text>
              <View style={{flex: 1}}>
                {/* <TouchableOpacity style={{flex: 6, alignItems:"flex-start"}} onPress={() => this.searchDataFunction('')}>
                  <Text style={[s.normalText, {color: "#00CCFF", paddingTop: 15}]}>Clear search</Text>
                </TouchableOpacity> */}
                <TouchableOpacity style={{flex: 1, alignItems:"flex-end"}} onPress={() => navigate('flashHubPage')}>
                  <Text style={[s.normalText, {color: "#00CCFF", paddingTop: 15}]}>Flash Mart ></Text>
                </TouchableOpacity>
              </View>
              {/* <Image style={styles.imgBlank} source={require('../../../assets/ShopMissing.jpg')}/> */}
            </View>
          )
        } else {
          return null
        }
      }
      
    }


    render() {
        const {navigate} = this.props.navigation;
        return (
          <Fragment>
            <SafeAreaView style={{flex: 0, backgroundColor: '#f1f1f1'}} />
            <SafeAreaView style={{flex: 1, backgroundColor: 'white'}}>
              <View style={[s.bodyGray, {flex: 1}]}>
                {/* <InternateModal /> */}
                <BasketModal visible={this.state.showBasketModal} onDismiss={this.hideModal} navigation= {navigate} basketData= {this.state.addtoBasketItem}
                  shopId={this.state.shopId} shopName={this.state.shopName} categoryId={""} />  
                <ScrollView contentInsetAdjustmentBehavior="automatic" style={styles.scrollView} showsVerticalScrollIndicator={false}>
                  {/* <View style={styles.sectionContainer}>
                    <View style={styles.advertiseData}></View>
                  </View> */}
                  <View style={styles.sectionContainer}>
                    {/* <TextInput placeholder = {this.state.placeholderText}
                      style={styles.searchShops} inlineImageLeft='searchicon' 
                      value = {this.state.searchText}
                      onChangeText={(searchText) => this.searchDataFunction(searchText,this.state.searchType)}/> */}    
                    <DelayInput placeholder = {this.state.placeholderText}
                      delayTimeout={500}
                      minLength={3}
                      // inputRef={inputRef}
                      style={styles.searchShops} inlineImageLeft='searchicon' 
                      // value = {this.state.searchText}
                      onChangeText={(text) => this.searchDataFunction(text,this.state.searchType)}
                    />
                  </View>
                  <View style={styles.searchHeaderView}>
                      <TouchableOpacity style={[styles.searchHeader,{ borderBottomColor: this.state.productBBColor}]}
                        onPress={() => this.searchType('Product')}>
                        <Text style={styles.storeHeading}>Product</Text>
                      </TouchableOpacity>
                      <TouchableOpacity style={[styles.searchHeader,{borderBottomColor: this.state.shopBBColor}]}
                        onPress={() => this.searchType('Shop')}>
                        <Text style={styles.storeHeading}>Shop</Text>
                      </TouchableOpacity>
                  </View>
                  <View style={{paddingBottom: this.state.paddingBottomHeight}}>
                      <FlatList data={this.state.data}
                        renderItem={( { item } ) => (
                            <this.ShopListing
                              item = {item}
                            />
                        )}
                        keyExtractor= {item => item.productId}
                        horizontal={false}
                        extraData={this.state || this.props}
                        
                      />
                    </View>         
                  

                    <this.ProductNotAvailable />
                  
                </ScrollView>
                <CartModal navigate={navigate} navigationPage={'orderDetailsPage'}></CartModal>
                <ShopListModal visible={this.state.shopModal} onDismiss={this.hideModal} assignShopToProduct={this.assignShopToProduct}
                navigation = {navigate} shopList={this.state.availableShopList} ></ShopListModal>
                {/* <OnGoingActivityModal navigate={navigate}></OnGoingActivityModal> */}
                <PopupModal visible={this.state.cartRemoveModal} onDismiss={this.hideModal} removeProduct={this.removeProduct} 
                          message={"Are you sure want to remove the product from cart?"} btnTag={"Remove"}/>
                <PopupModal visible={this.state.modalVisible} onDismiss={this.hideModal} removeProduct={this.userAckCart} 
                  message={this.state.cartModalMessage} 
                  btnTag={"Add to cart"}/>
                <NavigationBar navigate={navigate}></NavigationBar>
              </View>
          </SafeAreaView>
        </Fragment>  
              
    );
  }

  //function to add the new item in the basket   --- Start
  showBasketModal = (basketData) => {
    console.warn("basket data",basketData);
    this.setState({
      addtoBasketItem: basketData,
      showBasketModal: true,
    });
  }
  //function to add the new item in the basket   --- End

  hideModal = () => {
    this.setState({
      showBasketModal: false,
      modalVisible: false,
      shopModal: false,
      cartRemoveModal: false
    })
  }

  assignShopToProduct = async() => {
    console.warn('selected shop--->', this.state.availableShopList);
    const index = this.state.availableShopList.findIndex(ind => ind.isSelected === true);
    console.warn('Testttttt--->', index);
    console.warn('Testttttt---> shopId', this.state.availableShopList[index].shopId);
    await this.setState({
      shopId: this.state.availableShopList[index].shopId, 
      shopName: this.state.availableShopList[index].shopName, 
    });
    this.setState({shopModal: false});
    if (this.state.addType == 'cart') {
      this.addCart(this.state.productData);  
    } else {
      this.showBasketModal(this.state.basketData);
    }
  }

  getAvailableShopList = (productData, type) => {
    console.warn('in get shop -->', productData);
    if (type == 'basket') {
      this.setState({basketData: productData, addType: type});
    } else {
      this.setState({productData: productData, addType: type});
    }
    
    console.warn('category id--->', productData.categoryId);
    axios.post(
      Config.URL + Config.getShopByProductSearch,
      {
        "categoryId": productData.categoryId,
        "position": this.props.currentLocation.currentRegion
      },
      {
          headers: {
              'Content-Type': 'application/json',
          }
      }
    )
    .then(res => {
        console.warn("res-----kjbeakdskjbas",res.data.result);
        if (res.data.status == 0) {
          if (res.data.result.length <= 0) {
            Toast.show("Sorry, currently this product is not available in any of the shop",Toast.LONG);
          } else {
            res.data.result = res.data.result.map( item => {
              item.backGroundColor = "white"; 
              return item;
            });
            this.setState({
              availableShopList: res.data.result,
              shopModal: true
            })
          }

        }
      }).catch(err => {
          console.warn('errr--->', err);
      })
  }

  incrementCount = async(productData) => {
    this.setState({
      selectedProduct: productData,
    });
    let index = this.props.cartItem.cartItem.findIndex(ind => ind.productId === productData.menu.productId);
    this.props.cartState.cartCount += 1;
    this.props.cartState.cartSum = parseFloat(this.props.cartState.cartSum) + parseFloat(this.props.cartItem.cartItem[index].unitCost); 
    this.props.cartModalAction.changeCartState(true,this.props.cartState.cartSum,this.props.cartState.cartCount);
    this.props.cartItem.cartItem[index].counter = this.props.cartItem.cartItem[index].counter + 1;
    this.props.cartItem.cartItem[index].totalCost = this.props.cartItem.cartItem[index].unitCost * this.props.cartItem.cartItem[index].counter;
    this.props.actions.replaceCart(this.props.cartItem.cartItem);
    await this.setState({ data: this.state.data});
  }

  decrementCount = async(productData) => {
    this.setState({
      selectedProduct: productData
    });
    let id = this.props.cartItem.cartItem.findIndex(ind => ind.productId === productData.menu.productId);
   if (id >= 0) {
    if(this.props.cartItem.cartItem[id].counter - 1 <= 0){
      this.setState({
        cartRemoveModal: true
      });
    } else {
      this.props.cartItem.cartItem[id].counter = this.props.cartItem.cartItem[id].counter - 1;
      this.props.cartItem.cartItem[id].totalCost = this.props.cartItem.cartItem[id].unitCost * this.props.cartItem.cartItem[id].counter;
      this.props.cartState.cartCount -= 1;
      this.props.cartState.cartSum = parseFloat(this.props.cartState.cartSum) - parseFloat(this.props.cartItem.cartItem[id].unitCost); 
      this.props.cartModalAction.changeCartState(true,this.props.cartState.cartSum,this.props.cartState.cartCount);
      this.props.actions.replaceCart(this.props.cartItem.cartItem);
      await this.setState({ data: this.state.data});
    }
   }
    
    
    
  }

  removeProduct = async() => {

    this.setState({
      cartRemoveModal: false,
      removeProduct: true,
      modalVisible: false,
      paddingBottomHeight: 0
    });
    const i = this.props.cartItem.cartItem.findIndex(ind => ind.productId === this.state.selectedProduct.menu.productId);
    
    this.props.cartState.cartCount -= 1;
    this.props.cartState.cartSum = parseFloat(this.props.cartState.cartSum) - parseFloat(this.props.cartItem.cartItem[i].unitCost); 
    
    this.props.cartItem.cartItem.splice(i,1);
    if(this.props.cartState.cartCount === 0){
      this.props.cartModalAction.changeCartState(false,this.props.cartState.cartSum,this.props.cartState.cartCount);
    } else {
      this.props.cartModalAction.changeCartState(true,this.props.cartState.cartSum,this.props.cartState.cartCount);
    }
    this.props.actions.replaceCart(this.props.cartItem.cartItem);
    await this.setState({ data: this.state.data});
  }

    //function to add the new item in the cart   --- Start
    addCart = async(productData) => {
      console.warn("---------productId ", productData.shopDetails.deliveryTime);
        const cart = {
          productId: productData.menu.productId,
          productName: productData.menu.productName,
          productLogo: productData.menu.productLogo,
          productDescription: productData.menu.productDescription,
          unitCost: productData.menu.unitCost,
          unit: productData.menu.unit,
          shopId: productData.shopDetails.shopId,
          shopName: productData.shopDetails.shopName,
          category: productData.categoryId,
          counter: 1,
          totalCost: productData.menu.unitCost,  
          isProductAvailable: true,
          cgst: parseFloat(productData.menu.cgst),
          sgst: parseFloat(productData.menu.sgst),
          costExlGst: parseFloat(productData.menu.unitCostExclTax),
          shopRate: parseFloat(productData.menu.shopRate),
          shopAddress: productData.shopDetails.shopAdress,
          shopContactNumber: productData.shopDetails.shopContactNumber,
          shopEmail: productData.shopDetails.shopEmail,
          shopLocation: productData.shopDetails.shopLocation,
          shopOwnerName: productData.shopDetails.shopOwnerName,
          deliveryTime: productData.shopDetails.deliveryTime,
          isBogo: false
        }

        this.setState({
          cart: cart 
        })
        console.warn("---------cart ",cart);

        // this.confirmAddCart(cart);
    
        if(this.props.cartItem.cartItem.some(arr => arr.productId == productData.menu.productId)){
          Toast.show("Already added in the cart",Toast.LONG);
        }
        else if (!this.props.cartItem.cartItem.some(arr => arr.shopId == cart.shopId)) {
          if (this.props.cartItem.cartItem.length > 0) {
            if (!this.props.cartItem.cartItem.some(arr => arr.deliveryTime == productData.shopDetails.deliveryTime)) {
              this.setState({
                modalVisible: true,
                cartModalMessage: 'Delivery time and delivery charges of your order added in cart depends upon the delivery time of the individual shop and number of shop you shopped, if ordered time crosses the delivery time of any shop(s) then the whole order will be delivered tomorrow or next day.'
              })
            } else {
              this.setState({
                modalVisible: true,
                cartModalMessage: 'Delivery charges may vary as per the changes in different shop locations. Would you like to continue with your purchase?'
              })
            }
          } else {
            this.confirmAddCart(cart);
          }
        }
        else{
          this.confirmAddCart(cart);
        }
      }
      //function to add the new item in the cart   --- End


  confirmAddCart = async(cart) => {
    this.props.cartState.cartCount += 1;
    console.warn('cartSUm, cartCount', this.props.cartState.cartSum,this.props.cartState.cartCount);
    this.props.cartState.cartSum = parseFloat(this.props.cartState.cartSum) + parseFloat(cart.unitCost); 
    this.props.cartModalAction.changeCartState(true,this.props.cartState.cartSum,this.props.cartState.cartCount);
    this.props.actions.addToCart(cart);
    this.setState({paddingBottomHeight: 110});
    Toast.show(cart.productName + " is added in the cart",Toast.LONG);
    this.setState({productData: {}, data: this.state.data});

  }

  userAckCart = () => {
    this.setState({
      modalVisible: false
    })
    this.confirmAddCart(this.state.cart);

  }

   //function to search product or shop   --- Start
  searchDataFunction = async(text, searchType) => {
    console.warn('called=== here how', text, searchType);
    await this.setState({searchText: text});
    if (text.length >= 3) {
      var url;
      console.warn("in search Data search Type",searchType,text);
      let reqBody;
      if(searchType === "Product"){
          url = Config.URL + Config.productSearch
          reqBody = {
            "searchData": text,
            "position": this.props.currentLocation.currentRegion
          }
      }
      else{
          console.warn("in shopsearch");
          url = Config.URL + Config.shopSearch
          reqBody = {
            "searchData": text,
            "position": this.props.currentLocation.currentRegion
          }
      }
      axios.post(
          url,
          reqBody,
          {
              headers: {
                  'Content-Type': 'application/json',
              }
          }
      )
      .then(res => {
      console.warn('response', res);
        if (res.data.status == 0) {
          this.setState({data: res.data.result});

          if (this.state.data.length <= 0) {
            Toast.show(res.data.message, Toast.LONG);
          }
        }
        else if(res.data.status == 1){  
          Toast.show(res.data.message,Toast.LONG);
        }
  
      }).catch(err => {
        Toast.show('Searched shop is not yet tied up with flash', Toast.LONG);
      })
    } else {
        this.setState({data: []});
    }
    console.warn('searchText:-', this.state.searchText);
    
    
  }
    //function to search product or shop   --- End

    //function to changr search based on   --- Start
    searchType = (type) => {
        console.warn("in searchType method",type);
        if(type === "Product"){
            this.setState({
                searchText: "",
                searchType: "Product",
                productBBColor: "#0080FE",
                shopBBColor: "#7285A5",
                placeholderText: 'Search product ...',
                data: []
            })
            
        }
        else{
            this.setState({
                searchText: "",
                searchType: "Shop",
                productBBColor: "#7285A5",
                shopBBColor: "#0080FE",
                placeholderText: 'Search shop ...',
                data: []
            })
            
        }
        console.warn("test11",this.state.searchType);
        // this.searchData(this.state.searchText,type);
        
    }
    //function to add the new item in the cart   --- End

    
    //function to changr search based on   --- Start
    shopNavigation = (data) => {
        console.warn("in shopnavigation method",data.shopId);
        this.props.navigation.navigate('shopDetailPage',{shopId: data.shopId,categoryId: data.categoryId});
    }
    //function to add the new item in the cart   --- End
    

}



let ScreenHeight = Dimensions.get("window").height;
let ScreenWidth = Dimensions.get("window").width;
const styles = StyleSheet.create({
  scrollView: {
    paddingTop:20,
  },
  imgBlank: {
    // height: ScreenHeight,
    width: ScreenWidth -30,
    height: ScreenHeight - 230,
    margin: 15
  },
  addtoCartBtnColor: {
    fontSize:12,
    borderWidth: 1,
    borderColor: '#0080FE',
  //  backgroundColor: '#00009a',
    //color: 'white',
    marginLeft: 20,
  },
  storeBtn: {
    paddingVertical: 5,
    paddingHorizontal: 5,
    marginBottom: 5,
    color: 'white',
    borderRadius: 0,
    width: 50
  },

  sectionContainer: {
    // marginTop: 24,
    paddingHorizontal: 24,
    paddingBottom:20
  },
  searchHeader:{
    flex:1, 
    borderBottomWidth:2,
    alignItems:"center"
  },
  searchHeaderView:{
    flex:1,
    flexDirection:"row",
    // borderBottomWidth: 2,
    // borderBottomColor: "grey",
    marginHorizontal:20,
    paddingBottom: 20
  },
  basketBtn:{
    paddingBottom: 30,
    height: 30,
    width: 30,
  },
  searchShops:{ 
    height: 40, 
    borderColor: 'gray', 
    borderWidth: 1,
    paddingHorizontal:10, 
    backgroundColor:"white",
    borderRadius:4 
},
iconView: {
    width: 30,
    height: 30,
    marginRight: 20,
    borderRadius: 100,
},
  advertiseData: {
    height:150,
    borderRadius: 4,
    borderWidth: 0.5,
    borderColor: 'black',
    backgroundColor: 'grey',
  },
  containerIcons:{
    width:30,
    height:30,
  },
  categoryFooter:{
    width:ScreenWidth,
    paddingBottom:20,
    height:80,
    flexGrow:0.05,
    backgroundColor:'lightgray',
    display:'flex',
    flexDirection:'row',
    justifyContent:"space-evenly",
    alignItems:"center"
  },
  storeHeadinglbl:{ 
    color: 'white',
    fontSize:16 
  },
  storeHeadingView:{
    display:'flex',
    flexDirection:'row',
    justifyContent:'space-between',
    padding:10
  },
  StoreCardsLayout:{ 
    flex: 1, 
    flexDirection: 'row', 
  justifyContent: 'space-between' 
},
  materialTypeHeading:{ 
    color: 'black',
    fontSize:18,
    fontWeight: "bold", 
    paddingBottom:10 
},
orderCardButton:{
  borderColor:'#0080FE',
  borderWidth: 1,
  width:25,
  height:25,
  borderRadius:50,
  alignItems:"center",
  justifyContent:'center',
},
orderCount:{
  fontSize:18,
  fontWeight:'bold',
  paddingHorizontal:10,
  color: '#131E3A'
},
flexRowAdd: {
  // marginLeft: 15,
  marginHorizontal: 10,
  marginBottom: 5, 
  // paddingTop: 10,
  textAlignVertical: "bottom",
  flexDirection: 'row',
},
  categoryHeadingTxt:{ 
    color: 'white',
    fontSize:16,
    textAlign:'center' 
},
  flexRowSpaceBetween:{ 
    // display: 'flex', 
    flexDirection: 'row', 
    justifyContent: 'space-between',
   // marginBottom: 20
  },
  bookCardLayout:{
    padding:10,
    display:'flex',
    flexDirection:'column'
},
  categoryHeadingView:{
    borderBottomColor: 'lightgray',
    borderBottomWidth: 1,
    padding:10,
    textAlign:'center',
  },
  storeHeading:{
    color: '#131E3A',
    fontSize:16,
    paddingHorizontal: 15,
    fontWeight: "bold"
  },
  storeDesc:{ 
    color: '#003151',
    fontSize: 14
  },
  trendingShops: {
    borderWidth: 1,
    borderColor: 'lightgray',
    borderRadius: 4,
    marginTop: 32,
    padding: 15,
    margin: 10,
  },
  imageCardView: {
    height: 80,
    borderRadius: 4,
    flex: 1,
    elevation: 4
  },
  imageCards: {
    height: 80,
    borderWidth: 1,
    borderRadius: 4,
    flex: 1
  },
  offerCards:{
    width: 130,
    height: 130,
    backgroundColor:'orange',
    borderRadius: 100,
    borderWidth: 0.5,
    borderColor: 'black',
    marginVertical:10,
    marginHorizontal:10,
  },
  offerCardDisplay:{
    padding:10,
    display:'flex',
    flexDirection:'column',
    alignItems:'center',
  },
  storeCards: {
    borderRadius: 4,
    borderWidth: 0.5,
    borderColor: '#7285A5',
    backgroundColor: 'white',
    flex: 3,
    elevation: 4
  },
  productHeader: {
    height: 100,
    borderColor: 'black',
  },
  storeContainer: {
    borderRadius: 4,
    borderWidth: 1,
    borderColor: 'black',
    backgroundColor: 'navy',
  },
  sectionTitle: {
    borderRadius: 4,
    borderWidth: 0.5,
    borderColor: '#d6d7da',
    // fontSize: 24,
    // fontWeight: '600',
    color: Colors.black,
    // borderStyle:1px solid red
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
  prodcutListingView: {
    flexDirection: 'row',
    marginVertical: 5,
    backgroundColor: "white",
    padding: 10,
    marginHorizontal: 10,
    elevation: 4

  },
  addCardImage: {
    width: 80,
    height: 80,
    flex: 3
  },
  productImage: {
    width: 80,
    height: 80,
  },
  storeCardButton: {
    backgroundColor: 'white',
    color: 'black',
    width: 30,
    height: 30,
    borderRadius: 50,
    alignItems: "center",
    justifyContent: 'center',
  },
  storeborder: {
    borderBottomColor: 'grey',
    borderBottomWidth: 1,
  },
  flexRow: {
    flex: 7,
    textAlignVertical: "bottom",
    flexDirection: 'row',
    justifyContent:"flex-end",
    alignItems:"flex-end",
    paddingRight: 10
  },
  prodcutListingView: {
    flexDirection: 'row',
    marginVertical: 5,
    backgroundColor: "white",
    padding: 10,
    marginHorizontal: 10,
    elevation: 4

  },
  stores: {
    paddingLeft: 15,
    flex: 9,
    flexDirection: "column"
  },

  storeCount: {
    fontSize: 22,
    fontWeight: 'bold',
    paddingHorizontal: 10,
  },
  storeProductHeading: {
    fontWeight: 'bold',
    fontSize: 30,
  },
  shopdtlCards: {
    width: 80,
    height: 80,
    backgroundColor: 'orange',
    borderRadius: 100,
    borderWidth: 0.5,
    borderColor: 'black',
    marginVertical: 5,
    marginHorizontal: 10,
  },
  Mystores: {
    flexDirection: "row",
    alignItems: "flex-start",
    justifyContent: "space-between",
    textAlignVertical: "bottom",
    flex: 2,
    marginTop: 5
    // paddingBottom:15,
  },
  storeCardSpace: {
    marginBottom: 15,
  },
  addtoCartBtnColor: {
    fontSize:12,
    borderWidth: 1,
    borderColor: '#0080FE',
    marginLeft: 20,
  },
  storeBtn: {
    paddingVertical: 5,
    paddingHorizontal: 5,
    marginBottom: 5,
    color: 'white',
    borderRadius: 0,
    width: 50
  },
  basketBtn:{
    paddingVertical: 5,
    marginBottom: 10,
    height: 30,
    width: 30,
  },
  basketImg: {
    height: 30,
    width: 30,
  }
});

//--------------------------------------------------- store related fuction  -- Start

const mapStateToProps = (state) => {
  return {
    cartItem: state.cartItem,
    basketItem: state.basketItem,
    cartState: state.cartState,
    currentLocation: state.currentLocation,
  }
}

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(addToCart, dispatch),
        basketAction: bindActionCreators(basketAction,dispatch),
        cartModalAction: bindActionCreators(cartModalAction,dispatch),
        navigationAction: bindActionCreators(navigateAction,dispatch),
    };
}

// ---------------------------------------------------- store related function  -- END

export default connect(mapStateToProps, mapDispatchToProps)(ProductSearch);

