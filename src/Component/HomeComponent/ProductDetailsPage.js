/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import * as addToCart  from '../../Actions/CartItem';
import { bindActionCreators } from 'redux';
import axios from 'axios';
import * as Config from '../../Constant/Config.js';
import * as Constant from '../../Constant/Constant.js';
import NavigationBar from '../ReusableComponent/NavigationBar.js';
//import ProductListing from './productListing.js';
import * as navigateAction  from '../../Actions/NavigationBarAction';
import Toast from 'react-native-simple-toast';
import  s  from '../../sharedStyle.js';
import ImageView from 'react-native-image-view';
import { SliderBox } from 'react-native-image-slider-box';
import * as loginSuccess  from '../../Actions/LoginAction';
import * as basketAction  from '../../Actions/BasketAction';
import * as cartModalAction  from '../../Actions/CartModalAction';
import PopupModal from '../ReusableComponent/PopupComponent.js';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  Dimensions,
  TouchableOpacity,
  Button,
  TextInput,
  StatusBar,
  TouchableWithoutFeedback,
  Image,
  ToastAndroid,
  FlatList,
  SectionList,
  Modal
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
class ProductDetailsPage extends React.Component{
  shopId;
  shopName;
  categoryId;
  deliveryTime;
  constructor(props) {
    super(props);
    this.state={
      images: [],
      isImageVisible: false,
      imageIndex: 0,
      productData: this.props.navigation.state.params.productData,
      modalVisible: false,
      deliveryTime: ''
    }
    
  }
  

  async componentDidMount(){
    
    if (this.props.navigation.state.params) {
      await this.setState({productData: this.props.navigation.state.params.productData}); 
      this.shopId = this.props.navigation.state.params.shopId;
      this.shopName = this.props.navigation.state.params.shopName;
      this.categoryId = this.props.navigation.state.params.categoryId;
      this.deliveryTime = this.props.navigation.state.params.categoryId;this.deliveryTime
    }
    console.warn('product data in details page--->', this.props.navigation.state.params.productData);
    // const images = [{
    //   // Simplest usage.
    //   // url: 'https://avatars2.githubusercontent.com/u/7970947?v=3&s=460',
    //   source: require('../../../assets/Logo.png'),
   
    //   width: 400,
    //   height: 200
    //   // Optional, if you know the image size, you can set the optimization performance
   
    //   // You can pass props to <Image />.
    //   // props: {
    //   //     // headers: ...
    //   // }
    // },
    // {
    //   // url: 'https://avatars2.githubusercontent.com/u/7970947?v=3&s=460'
    //   source: require('../../../assets/Logo.png'),
    //   width: 400,
    //   height: 200
    // },
    // {
    //   // url: 'https://avatars2.githubusercontent.com/u/7970947?v=3&s=460'
    //   source: require('../../../assets/Logo.png'),
    //   width: 400,
    //   height: 200
    // }];
    // console.warn('this.state.productData.productDetailImages', this.state.productData.menu.productDetailImages)
    console.warn('this.state.images', this.state.productData.menu.productDetailImages);
    await this.setState({images: this.state.productData.menu.productDetailImages});
  }

  ImageSlider = ({item}) => {
    return (
      <TouchableOpacity style={{width: ScreenWidth, height: '100%'}} onPress={() => this.imageClick()}>
        <Image style={styles.imageView} source={{uri: item.source.uri}}/>
      </TouchableOpacity> 
    )  
  } 

  imageClick = () => {
    console.warn('index --->');
    this.setState({
      isImageVisible: true
    });
  }

  render() {
    const {navigate} = this.props.navigation;
    
    return (
      <SafeAreaView style={{flex: 1}}>
        <View style={s.bodyGray}>
       
          <View style={{flex: 5}}>
            <SliderBox   
              images={this.state.images}   
              // sliderBoxHeight={200}
              renderItem={( { item } ) => (
                <this.ImageSlider
                  item={item}
                />
              )}
              dotColor="#00CCFF"   
              inactiveDotColor="white"   
              paginationBoxVerticalPadding={20}   
              autoplay   
              dotStyle={{width: 8,height: 8,borderRadius: 4,marginHorizontal: 0,padding: 0,margin: 0,}}
              circleLoop 
            />
            
          </View>
          <View style={{flex: 6, backgroundColor: 'white', marginHorizontal: 15, paddingHorizontal: 15, paddingVertical: 20, borderRadius: 40, marginTop: 15}}>
            <ScrollView showsVerticalScrollIndicator={false}>
              <View>
                <Text style={[s.subHeaderText, {textAlign:"center"}]}>{this.state.productData.menu.productName}</Text>
                <Text style={[s.normalText, {textAlign:"center"}]}>{this.state.productData.menu.productSubCategory}</Text>
              </View>
              {this.state.productData.menu.mrp != this.state.productData.menu.unitCost 
              ? 
                <>
                <View style={{flex:1, flexDirection:"row", marginTop: 10, alignItems:"center", justifyContent:"center"}}>
                    <Text style={[s.normalText, {textAlign:"center"}]}>MRP:- </Text>
                    <Text style={[s.normalText,{textDecorationLine: 'line-through', textDecorationStyle: 'solid', textAlign:"center"}]} numberOfLines={1}>
                    ₹{this.state.productData.menu.mrp}</Text>
                </View>
                <View style={{flex:1, flexDirection:"row", alignItems:"center", justifyContent:"center"}}>
                    <Text style={[s.subHeaderText, {textAlign:"center"}]}>Flash Amount:- </Text>
                    <Text style={[s.subHeaderText, {textAlign:"center"}]} numberOfLines={1}>₹{this.state.productData.menu.unitCost}</Text>
                </View>
                </>
              : 
                <View style={{flex:1, flexDirection:"row", marginTop: 10, justifyContent:"center"}}>
                    <Text style={[s.normalText, {textAlign:"center"}]}>Flash Amount:- </Text>
                    <Text style={[s.normalText, {textAlign:"center"}]} numberOfLines={1}>₹{this.state.productData.menu.unitCost}</Text>
                </View>
                }
              {this.state.productData.menu.productDescription != ''
              ?
                <View style={{marginTop: 15}}>
                  <Text style={[s.subHeaderText]}>Description</Text>
                  <Text style={[s.normalText]} numberOfLines={150}>{this.state.productData.menu.productDescription}</Text>
                </View>
              :
                null
              }  
              {this.state.productData.menu.specification != ''
              ?
                <View style={{marginTop: 15}}>
                  <Text style={[s.subHeaderText]}>Specification</Text>
                  <Text style={[s.normalText]} numberOfLines={150}>{this.state.productData.menu.specification}</Text>
                </View>
              :
                null
              }
              
            </ScrollView>
          </View>
          <TouchableOpacity style={{flex: 1, backgroundColor:'#00CCFF', marginTop: 15, alignItems:"center", alignContent:"center", alignContent:"space-around"}}
            onPress={() => this.addCart()}>
            <Text style={[s.headerText, {color:"white", textAlign:"center", marginTop: 8}]}>Add to cart</Text>
          </TouchableOpacity>
          <ImageView
            images={this.state.images}
            imageIndex={this.state.imageIndex}
            isVisible={this.state.isImageVisible}
              renderFooter={(currentImage) => (<View style={{marginBottom: 30}}><Text style={[s.subHeaderText, {textAlign:"center"}]}>{this.state.productData.productName}</Text></View>)}
            onClose={() => this.setState({isImageVisible: false})}
            // onImageChange={index => {
            //   this.setState({imageIndex: index})
            // }}

          />
          <PopupModal visible={this.state.modalVisible} onDismiss={this.hideModal} removeProduct={this.userAckCart} 
            message={this.state.cartModalMessage} 
            btnTag={"Add to cart"}/>  
          
        </View>
      </SafeAreaView>
  );
  }

  addCart = () => {
    console.warn('this.state.productData', this.state.productData);
    const cart = {
      productId: this.state.productData.productId,
      productId: this.state.productData.menu.productId,
      productName: this.state.productData.menu.productName,
      productLogo: this.state.productData.menu.productLogo,
      productDescription: this.state.productData.menu.productDescription,
      unitCost: parseFloat(this.state.productData.menu.unitCost),
      unit: this.state.productData.menu.unit,
      shopId: this.shopId,
      shopName: this.shopName,
      category: this.categoryId,
      counter: 1,
      totalCost: parseFloat(this.state.productData.menu.unitCost),  
      isProductAvailable: true,
      cgst: parseFloat(this.state.productData.menu.cgst),
      sgst: parseFloat(this.state.productData.menu.sgst),
      costExlGst: parseFloat(this.state.productData.menu.unitCostExclTax),
      shopRate: parseFloat(this.state.productData.menu.shopRate),
      shopAddress: this.state.productData.shopDetails.shopAdress,
      shopContactNumber: this.state.productData.shopDetails.shopContactNumber,
      shopEmail: this.state.productData.shopDetails.shopEmail,
      shopLocation: this.state.productData.shopDetails.shopLocation,
      shopOwnerName: this.state.productData.shopDetails.shopOwnerName,
      deliveryTime: this.deliveryTime,
      isBogo: false
    }

    this.setState({
        cart: cart 
    });
    
    console.warn('testtttt-->', this.props.cartItem.cartItem.some(arr => arr.shopId == this.shopId));

    if(this.props.cartItem.cartItem.some(arr => arr.productId == this.state.productData.productId)){
    Toast.show("Already added in the cart",Toast.LONG);
    } else if (!this.props.cartItem.cartItem.some(arr => arr.shopId == this.shopId)) {
    if (this.props.cartItem.cartItem.length > 0) {
      if (!this.props.cartItem.cartItem.some(arr => arr.deliveryTime == this.deliveryTime)) {
        this.setState({
          modalVisible: true,
          cartModalMessage: 'Delivery time and delivery charges of your order added in cart depends upon the delivery time of the individual shop and number of shop you shopped, if ordered time crosses the delivery time of any shop(s) then the whole order will be delivered tomorrow or next day.'
        })
      } else {
        this.setState({
          modalVisible: true,
          cartModalMessage: 'Delivery charges may vary as per the changes in different shop locations. Would you like to continue with your purchase?'
        })
      }
    } else {
        this.confirmAddCart(cart);
    }
    }
    else{
    this.confirmAddCart(cart);
    }
}

  confirmAddCart = (cart) => {
    this.props.cartState.cartCount += 1;
    this.props.cartState.cartSum = (parseFloat(this.props.cartState.cartSum) + parseFloat(cart.unitCost)); 
    this.props.cartModalAction.changeCartState(true,this.props.cartState.cartSum,this.props.cartState.cartCount);
    this.props.actions.addToCart(cart);
    this.setState({paddingBottomHeight: 100});
    Toast.show(cart.productName + " is added in the cart",Toast.LONG);

  }

  userAckCart = () => {
    this.setState({
      modalVisible: false
    });
    this.confirmAddCart(this.state.cart);

  }

  hideModal = () => { 
    this.setState({
      modalVisible: false
    })
}

}


let ScreenWidth = Dimensions.get("window").width;
let ScreenHeight = Dimensions.get("window").height;

const styles = StyleSheet.create({
  imageView: {
    flex: 1,
    width: undefined,
    height: undefined,
    resizeMode: 'contain',
  },
});

//--------------------------------------------------- store related fuction  -- Start

const mapStateToProps = (state) => {
	return {
		cartItem: state.cartItem,
    basketItem: state.basketItem,
    cartState: state.cartState,
    userData: state.loginData,
	}
}

const mapDispatchToProps = (dispatch) => {
    return {
      actions: bindActionCreators(addToCart, dispatch),
      basketAction: bindActionCreators(basketAction,dispatch),
      cartModalAction: bindActionCreators(cartModalAction,dispatch),
      loginActions: bindActionCreators(loginSuccess,dispatch),
      navigationAction: bindActionCreators(navigateAction,dispatch),
    };
}

// ---------------------------------------------------- store related function  -- END


export default connect(mapStateToProps, mapDispatchToProps)(ProductDetailsPage);
