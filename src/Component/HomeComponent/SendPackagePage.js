/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Fragment } from 'react';
import Toast from 'react-native-simple-toast';
import { connect } from 'react-redux';
import * as activityAction  from '../../Actions/ActivityAction';
import { bindActionCreators } from 'redux';
import  s  from '../../sharedStyle.js';
import InternateModal from '../ReusableComponent/InternetModal';
import * as navigateAction  from '../../Actions/NavigationBarAction';
// import 'bootstrap/dist/css/bootstrap.min.css';
import {
    SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  Dimensions,
  TouchableOpacity,
  TextInput,
  ToastAndroid,
  Image,
  KeyboardAvoidingView
} from 'react-native';

import axios from 'axios'; 

class SendPackage extends React.Component{

  constructor(props){
    super(props);    
    this.state = {
        activityTag: "",
        pickupDate: new Date(),
        todaysDate: new Date(),
        deliveryAddress: {},
        packageDescription: "",
        pickupAdd: {},
        deliveryAdd: {},
        calledPickAdd: false,
        calledDeliveryAdd: false,
        contactPersonName: "",
        contactPersonMobNo: null,
        editIndex: null,
        isFormValid: false,
        activityTagBorder: 0,
        activityTagColor: "white",
        pkgBorder: 0,
        pkgColor: "white",
        cntNameBorder: 0,
        cntNameColor: "white",
        cntNoBorder: 0,
        cntNoColor: "white",
        pickAddressBorder: 1,
        pickAddressColor: "white",
        totalDistance: '',

    }
    
  }

  componentDidMount() {
    console.warn("tag id --->",this.props.navigation.state.params);
    if (this.props.navigation.state.params.tagId != "") {
        const i = this.props.activityData.ongoingActivity.findIndex(ind => ind.tagId === this.props.navigation.state.params.tagId);
        this.setState ({
            editIndex: i
        })
        this.state.activityTag = this.props.activityData.ongoingActivity[i].activityTag;
        this.state.packageDescription = this.props.activityData.ongoingActivity[i].packageDescription;
        this.state.deliveryAddress = this.props.activityData.ongoingActivity[i].deliveryAddress;
        this.state.contactPersonName = this.props.activityData.ongoingActivity[i].contactPersonName;
        this.state.contactPersonMobNo = this.props.activityData.ongoingActivity[i].contactPersonMobNo;
    }
    
  }

  changePickAdd = () => {
      this.setState ({
        calledPickAdd: true,
        calledDeliveryAdd: false
      })
    this.props.navigation.navigate('addressPage',{"addressId": "","callingPage": "sendPackage"});
  }

  changeDeliveryAdd = () => {
    this.setState ({
        calledPickAdd: false,
        calledDeliveryAdd: true
      })
    this.props.navigation.navigate('addressPage',{"addressId": "","callingPage": "sendPackage"});
  }

  ConfirmButton = () => {
    const {navigate} = this.props.navigation;
      if (!this.props.navigation.state.params.tagId) {
          return (
            // <TouchableOpacity style={{flex:6,backgroundColor:"#00CCFF",alignItems:"center",justifyContent:"center",paddingVertical:10}}
            //     onPress={() => this.confirm()}>
            //     <Text style={[s.subHeaderText,{color:"white"}]}>Continue</Text>
            // </TouchableOpacity>
            <View style={{flex:1,flexDirection:"row", bottom:0, position:"absolute",elevation:6}}>
                <TouchableOpacity style={{flex:6,backgroundColor:"#F5F5F5",alignItems:"center",paddingVertical:10}} onPress={() => navigate('sendPackageList')}>
                    <Text style={{fontSize: 16,fontWeight: "bold",color:"#131E3A"}}>Cancel</Text>
                </TouchableOpacity>
                <TouchableOpacity style={{flex:6,backgroundColor:"#00CCFF",alignItems:"center",justifyContent:"center",paddingVertical:10}}
                    onPress={() => this.submitForm('New')}>
                    <Text style={[s.subHeaderText,{color:"white"}]}>Continue</Text>
                </TouchableOpacity>
            </View>
          )
      } else {
        return (
            <View style={{flex:1,flexDirection:"row", bottom:0, position:"absolute",elevation:6}}>
                <TouchableOpacity style={{flex:6,backgroundColor:"#F5F5F5",alignItems:"center",paddingVertical:10}} onPress={() => navigate('sendPackageList')}>
                    <Text style={{fontSize: 16,fontWeight: "bold",color:"#131E3A"}}>Cancel</Text>
                </TouchableOpacity>
                <TouchableOpacity style={{flex:6,backgroundColor:"#00CCFF",alignItems:"center",justifyContent:"center",paddingVertical:10}}
                onPress={() => this.submitForm('edit')}>
                <Text style={[s.subHeaderText,{color:"white"}]}>Save</Text>
            </TouchableOpacity>
            </View>
          )
      }

  }

  DeliveryAddress = () => {  
    const {navigate} = this.props.navigation;
    const addressId = this.props.activityData.deliveryAddress.addressId;
    if (Object.keys(this.state.deliveryAddress).length > 0) {
        return (
            <View>
                <View style={{flex: 1,flexDirection:"row"}}>
                    <Text style={[s.subHeaderText,{flex: 8,paddingBottom: 5, marginHorizontal: 15}]}>Delivery Address</Text>
                    <TouchableOpacity style={{flex: 4,alignItems:"flex-end",paddingRight: 20}}
                    onPress={() => navigate('addressPage',{"addressId": addressId,"callingPage": "sendPackageList"})}>
                        <Image style={{height: 20, width: 20}} source={require('../../../assets/edit.png')}/>    
                    </TouchableOpacity>
                </View>
                <View style={{paddingVertical: 10, paddingHorizontal: 10, backgroundColor:"#f1f1f1",borderRadius: 10, elevation: 4, marginHorizontal: 15}}>
                    <Text style={[s.normaltext]}> {this.state.deliveryAddress.flatNumber}, {this.state.deliveryAddress.addressLine1}</Text>
                    <Text style={[s.normaltext]}> {this.state.deliveryAddress.addressLine2}</Text>
                    {/* <Text style={[s.normaltext,{fontWeight: "bold"}]} numberOfLines={2}> {this.state.deliveryAddress.state}, {this.state.deliveryAddress.state}, {this.state.deliveryAddress.pincode}</Text> */}
                </View>
            </View>
        )
    } else if(Object.keys(this.props.activityData.deliveryAddress).length > 0) {
        return (
            <View>
                <View style={{flex: 1,flexDirection:"row"}}>
                    <Text style={[s.subHeaderText,{flex: 8,paddingBottom: 5, marginHorizontal: 15}]}>Delivery Address</Text>
                    <TouchableOpacity style={{flex: 4,alignItems:"flex-end",paddingRight: 20}}
                    onPress={() => navigate('addressPage',{"addressId": addressId,"callingPage": "sendPackageList"})}>
                        <Image style={{height: 20, width: 20}} source={require('../../../assets/edit.png')}/>    
                    </TouchableOpacity>
                </View>
                <View style={{paddingVertical: 10, paddingHorizontal: 10, backgroundColor:"#f1f1f1",borderRadius: 10, elevation: 4, marginHorizontal: 15}}>
                    <Text style={[s.normaltext]}> {this.props.activityData.deliveryAddress.flatNumber}, {this.props.activityData.deliveryAddress.addressLine1}</Text>
                    <Text style={[s.normaltext]}> {this.props.activityData.deliveryAddress.addressLine2}</Text>
                    {/* <Text style={[s.normaltext,{fontWeight: "bold"}]} numberOfLines={2}> {this.props.activityData.deliveryAddress.state}, {this.props.activityData.deliveryAddress.state}, {this.props.activityData.deliveryAddress.pincode}</Text> */}
                </View>
            </View>
            
        )
    } else {
        return (
            <View>
                <View style={{flex: 1,flexDirection:"row"}}>
                    <Text style={[s.subHeaderText,{flex: 8,paddingBottom: 5, marginHorizontal: 15}]}>Delivery Address</Text>
                </View>
                <TouchableOpacity style={{display:"flex",alignItems:"center",backgroundColor:"#f1f1f1",borderRadius: 10, elevation: 4, marginHorizontal: 15, borderWidth: this.state.pickAddressBorder, borderColor: this.state.pickAddressColor}}
                onPress={() => navigate('addressPage',{"addressId": "","callingPage": "sendPackageList"})}>
                    <View style={[styles.storeBtn, styles.addtoCartBtnColor,{display:"flex",alignItems:"center",paddingVertical: 15}]} >
                        <Text style={{ color:'#00CCFF',fontSize: 16,fontWeight:"bold",justifyContent:"center"}}> Add Address</Text> 
                    </View>
                </TouchableOpacity>
            </View>
            
        )
    }
    
  }

  render() {
    const {navigate} = this.props.navigation;
    return ( 
        <SafeAreaView style={{flex: 1}}>
            <View style={{flex: 1}}>
                {/* <KeyboardAvoidingView style={[s.bodyGray]} behavior="padding"> */}
                    <KeyboardAvoidingView behavior="padding">
                        <ScrollView showsVerticalScrollIndicator = {false}>
                            <View style={styles.advertiseData}>
                                <Image style={styles.imageView} source={{uri: 'https://flash-catagory-image.s3.ap-south-1.amazonaws.com/ActivityHeader-Get.png'}}/>
                            </View>
                            <View style={{marginHorizontal: 10, marginVertical: 20,paddingBottom: 60, backgroundColor:"white", borderRadius: 30, paddingVertical: 20, elevation: 4}}>
                                <View>
                                    <Text style={[s.subHeaderText,{paddingBottom: 5, marginHorizontal: 15}]}>Activity Tag</Text>
                                    <TextInput value={this.state.activityTag}
                                        style={[styles.inputBox,{elevation: 4,borderWidth: this.state.activityTagBorder,borderColor: this.state.activityTagColor}]}
                                        underlineColorAndroid="transparent"
                                        onChangeText={(tag) => {
                                        this.setState({
                                            isFormValid: true,
                                            activityTag: tag,
                                            activityTagBorder: 0,
                                            activityTagColor: "white"
                                        })}} />
                                </View>
                                <View style={{marginTop : 15}}> 
                                    <Text style={[s.subHeaderText,{paddingBottom: 5,elevation: 6, marginHorizontal: 15}]}>Package Description </Text>
                                    <TextInput value={this.state.packageDescription}
                                        style={[styles.inputBoxDetails,{elevation: 4,borderWidth: this.state.pkgBorder,borderColor: this.state.pkgColor}]}
                                        underlineColorAndroid="transparent"
                                        numberOfLines={3}
                                        multiline={true}
                                        onChangeText={(desc) => {
                                        this.setState({
                                            isFormValid: true,
                                            packageDescription: desc,
                                            pkgBorder: 0,
                                            pkgColor: "white"
                                        })}} />
                                </View>
                                <View style={{marginTop : 15}}>
                                    <this.DeliveryAddress />
                                </View>
                                <View style={{marginTop : 15}}>
                                    <Text style={[s.subHeaderText,{paddingBottom: 5, marginHorizontal: 15}]}>Contact Person Name</Text>
                                    <TextInput value={this.state.contactPersonName}
                                        style={[styles.inputBox,{elevation: 4,borderWidth: this.state.cntNameBorder,borderColor: this.state.cntNameColor}]}
                                        underlineColorAndroid="transparent"
                                        onChangeText={(name) => {
                                        this.setState({
                                            isFormValid: true,
                                            contactPersonName: name,
                                            cntNameBorder: 0,
                                            cntNameColor: "white"
                                        })}} />
                                </View>
                                <View style={{marginTop : 15}}>
                                    <Text style={[s.subHeaderText,{paddingBottom: 5, marginHorizontal: 15}]}>Contact Person Mobile Number</Text>
                                    <TextInput value={this.state.contactPersonMobNo}
                                        style={[styles.inputBox,{elevation: 4,borderWidth: this.state.cntNoBorder,borderColor: this.state.cntNoColor}]}
                                        keyboardType = {'numeric'} maxLength={10}
                                        underlineColorAndroid="transparent"
                                        onChangeText={(mobile) => {
                                        this.setState({
                                            isFormValid: true,
                                            contactPersonMobNo: mobile,
                                            cntNoBorder: 0,
                                            cntNoColor: "white"
                                        })}} />
                                </View>
                            </View>
                        </ScrollView>  
                        <View style={{ height: 100 }} />
                    </KeyboardAvoidingView>
                        {/* <View style={{flex:1,flexDirection:"row", bottom:0, position:"absolute",elevation:6}}>
                            <TouchableOpacity style={{flex:6,backgroundColor:"#F5F5F5",alignItems:"center",paddingVertical:10}} onPress={() => navigate('sendPackageList')}>
                                <Text style={{fontSize: 16,fontWeight: "bold",color:"#131E3A"}}>Cancel</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={{flex:6,backgroundColor:"#00CCFF",alignItems:"center",justifyContent:"center",paddingVertical:10}}
                                onPress={() => this.submitForm()}>
                                <Text style={[s.subHeaderText,{color:"white"}]}>Continue</Text>
                            </TouchableOpacity>
                        </View> */}
                        <this.ConfirmButton />
                    
                {/* </KeyboardAvoidingView> */}
            </View>
        </SafeAreaView>
     
    );
  }


    submitForm = async(type) => {
        console.warn('in submit form--', this.state.contactPersonName);
        await this.setState({ isFormValid: true });
        

        if(this.state.activityTag == "") {
            await this.setState({
                isFormValid: false,
                activityTagBorder: 1,
                activityTagColor: "red"
            });
            Toast.show('Please add activity tag',Toast.LONG);
        } else if(this.state.packageDescription == "") {
            await this.setState({
                isFormValid: false,
                pkgBorder: 1,
                pkgColor: "red"
            });
            Toast.show('Please add package description',Toast.LONG);
        } else if(this.state.contactPersonName == "") {
            await this.setState({
                isFormValid: false,
                cntNameBorder: 1,
                cntNameColor: "red"
            });
            console.warn('inside name not enter');
            Toast.show('Please add contact person name',Toast.LONG);
        } else if(this.state.contactPersonMobNo == "" || this.state.contactPersonMobNo == null) {
            await this.setState({
                isFormValid: false,
                cntNoBorder: 1,
                cntNoColor: "red"
            });
            Toast.show('Please add contact person mobile number',Toast.LONG);
        } else if(Object.keys(this.props.activityData.deliveryAddress).length <= 0) {
            console.warn('11');
            if (Object.keys(this.state.deliveryAddress).length <= 0) {
                console.warn('22');
                await this.setState({
                    isFormValid: false,
                    pickAddressBorder: 1,
                    pickAddressColor: "red"
                });
                Toast.show('Please add delivery address',Toast.LONG);
            } else {
                if (type == 'New') {
                    this.confirm();
                } else {
                    this.editAction();
                }    
            }
        } else if (this.state.isFormValid) {
            console.warn('inside success validation');
            if (type == 'New') {
                this.confirm();
            } else {
                this.editAction();
            }
            
        }
        console.warn('end---', this.state.isFormValid);
    }

    confirm = () => {
        const tempObj ={
            tagId: new Date().getTime(),
            activityTag: this.state.activityTag,
            packageDescription: this.state.packageDescription,
            deliveryAddress: this.props.activityData.deliveryAddress,
            contactPersonName: this.state.contactPersonName,
            contactPersonMobNo: this.state.contactPersonMobNo
        }
        this.props.activityData.ongoingActivity.push(tempObj);
        this.props.activityAction.ongoingActivity(this.props.activityData.ongoingActivity);
        this.state.deliveryAddress = {};
        this.props.activityAction.deliveryAddress({});
        this.props.navigation.navigate('sendPackageList');
    }

    editAction = () => {
        console.warn("edit indes",this.state.editIndex);
        this.props.activityData.ongoingActivity[this.state.editIndex].activityTag = this.state.activityTag;
        this.props.activityData.ongoingActivity[this.state.editIndex].packageDescription = this.state.packageDescription;
        this.props.activityData.ongoingActivity[this.state.editIndex].deliveryAddress = this.state.deliveryAddress;
        this.props.activityData.ongoingActivity[this.state.editIndex].contactPersonName = this.state.contactPersonName;
        this.props.activityData.ongoingActivity[this.state.editIndex].contactPersonMobNo = this.state.contactPersonMobNo;
        
        this.props.activityAction.ongoingActivity(this.props.activityData.ongoingActivity);

        this.props.navigation.navigate('sendPackageList');
    }

}

let ScreenWidth = Dimensions.get("window").width;
let ScreenHeight = Dimensions.get("window").height;

const styles = StyleSheet.create({

    advertiseData: {
        height: 220,
        width: ScreenWidth,
    },
    imageView: {
        flex: 1,
        width: undefined,
        height: undefined,
        resizeMode: 'contain',
    },
    inputBox: {
        justifyContent: 'center',
        alignItems: 'center',
        padding: 10,
        borderRadius: 10,
        backgroundColor:"#f1f1f1",
        paddingHorizontal: 15,
        marginHorizontal: 15,
        height: 40
      },
    inputBoxDate: {
        justifyContent: 'center',
        alignItems: 'center',
        padding: 10,
        borderRadius: 10,
        backgroundColor:"#f1f1f1",
        paddingHorizontal: 15,
        marginHorizontal: 15,
        height: 40,
        width: '90%'
      },
    inputBoxDetails: {
        justifyContent: 'center',
        alignItems: 'center',
        padding: 10,
        borderRadius: 10,
        backgroundColor:"#f1f1f1",
        paddingHorizontal: 15,
        marginHorizontal: 15,
        height: 80
      },
	
	addressCardView: {
		borderRadius: 5,
		marginHorizontal: '2%',
		elevation: 4,
		alignContent: 'center',
		padding: '3%',      
        backgroundColor:"white",
        fontWeight: "bold"
	},

	addressButtonView: {
		backgroundColor:"#00CCFF",  
		width: '80%',
		//borderRadius: 50,
		elevation: 6,
		shadowColor: 'rgba(0, 0, 0, 0.1)',
		shadowOpacity: 0.8,
		height: 40,
		justifyContent: 'center',
		alignItems: 'center',
	},

	addressButtonText: {
		color:"white",  
		fontSize: 16 ,
		fontWeight:"bold"
	},
    

});


//--------------------------------------------------- store related fuction  -- Start

const mapStateToProps = (state) => {
	return {
        activityData: state.activityData,
	};
}

const mapDispatchToProps = (dispatch) => {
    return {
        activityAction: bindActionCreators(activityAction,dispatch),
        navigationAction: bindActionCreators(navigateAction,dispatch),
    };
}

// ---------------------------------------------------- store related function  -- END


export default connect(mapStateToProps, mapDispatchToProps)(SendPackage);

