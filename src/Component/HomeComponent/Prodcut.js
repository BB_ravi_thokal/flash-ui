
import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import * as addToCart  from '../../Actions/CartItem';
import { bindActionCreators } from 'redux';
import axios from 'axios';
import * as Config from '../../Constant/Config.js';
import * as Constant from '../../Constant/Constant.js';
import NavigationBar from '../ReusableComponent/NavigationBar.js'
import  s  from '../../sharedStyle.js'
import { Rating } from 'react-native-ratings';
import BasketModal from '../ReusableComponent/basketModal';
import CartModal from '../ReusableComponent/cartModal';
import OnGoingActivityModal from '../ReusableComponent/OnGoingActivityModal';
import * as basketAction  from '../../Actions/BasketAction';
import * as cartModalAction  from '../../Actions/CartModalAction';
import InternateModal from '../ReusableComponent/InternetModal';
import PopupModal from '../ReusableComponent/PopupComponent.js';
import * as loginSuccess  from '../../Actions/LoginAction';
import * as navigateAction  from '../../Actions/NavigationBarAction';
import AnimatedLoader from "react-native-animated-loader";
import Toast from 'react-native-simple-toast';
import DelayInput from "react-native-debounce-input";
// import SceneLoader from 'react-native-scene-loader';

import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  Dimensions,
  TouchableOpacity,
  TextInput,
  Image,
  FlatList,
  ActivityIndicator,
} from 'react-native';




class ProductListPage extends React.PureComponent {

    constructor(props) {
        super(props);
        this.state={
            categoryId: '',
            searchText: this.props.navigation.state.params.searchText,
            showLoader: false,
            shopMenu: [],
            paddingBottomHeight: 0,
            modalVisible: false,
            cart: {},
            showBasketModal: false,
            addtoBasketItem: {},
            productNotAvailable: false,
            shopId: '',
            shopName: '',
            productSubCategory: '',
            productCategory: '',
            cartRemoveModal: false,
            selectedProduct: {},
            cartModalMessage: '',
            deliveryTime: ''
        }
        
    }

    componentDidMount = () => {
        this.setState({showLoader: true});
        if (this.props.cartState.cartState) {
          this.setState({paddingBottomHeight: 80});
        }
        console.warn('searchText--->', this.props.navigation.state.params.searchText);
        // if(this.props.navigation.state.params.categoryId && this.props.navigation.state.params.searchText) {
            this.setState({
                // categoryId: this.props.navigation.state.params.categoryId,
                searchText: this.props.navigation.state.params.searchText,
                shopId: this.props.navigation.state.params.shopId,
                shopName: this.props.navigation.state.params.shopName,
                productCategory: this.props.navigation.state.params.productCategory,
                productSubCategory: this.props.navigation.state.params.productSubCategory,
                deliveryTime: this.props.navigation.state.params.deliveryTime
            });
            var searchObj = {
                "searchData": this.props.navigation.state.params.searchText,
                "shopId": this.props.navigation.state.params.shopId,
                "productCategory": this.props.navigation.state.params.productCategory,
                "productSubCategory": this.props.navigation.state.params.productSubCategory,

            }
            this.getShopMenu(searchObj);
        // }
    }

    ProductListing = (item) => {
      const {navigate} = this.props.navigation;
      if (item.item.menu.isClickable) {
        return (
          <View style={[styles.prodcutListingView]}>
              <TouchableOpacity style={styles.addCardImage} onPress={() => navigate('productDetailsPage', {productData: item.item, shopId: this.state.shopId, shopName: this.state.shopName, category: this.state.categoryId})}>
                <Image style={styles.productImage} source={{uri: item.item.menu.productLogo}}/>
              </TouchableOpacity>
              <TouchableOpacity style={styles.stores} onPress={() => navigate('productDetailsPage', {productData: item.item, shopId: this.state.shopId, shopName: this.state.shopName, category: this.state.categoryId})}>
                  <View style={{flex:10}}>
                  <Text style={s.subHeaderText} numberOfLines={3}>{item.item.menu.productName} </Text>
                  </View>
                  {item.item.menu.mrp != item.item.menu.unitCost 
                  ? 
                  <>
                  <View style={{flex:1, flexDirection:"row", marginTop: 10}}>
                      <Text style={[s.normalText]}>MRP:- </Text>
                      <Text style={[s.normalText,{textDecorationLine: 'line-through', textDecorationStyle: 'solid'}]} numberOfLines={1}>
                      ₹{item.item.menu.mrp}</Text>
                  </View>
                  <View style={{flex:1, flexDirection:"row"}}>
                      <Text style={[s.normalText]}>Flash amount:- </Text>
                      <Text style={[s.normalText]} numberOfLines={1}>₹{item.item.menu.unitCost}</Text>
                  </View>
                  </>
                  : 
                  <View style={{flex:1, flexDirection:"row", marginTop: 10}}>
                      <Text style={[s.normalText]}>Flash amount:- </Text>
                      <Text style={[s.normalText]} numberOfLines={1}>₹{item.item.menu.unitCost}</Text>
                  </View>
                  }
                  
                  
              
                  <View style={styles.Mystores}>
                    <View style={{paddingTop: 10, flex: 5}}>
                        {/* <Text style={[s.normalText,{textDecorationLine: 'line-through', textDecorationStyle: 'solid'}]} numberOfLines={1}>
                        MRP:- ₹{item.item.menu.unitCost}</Text>
                        <Text style={[s.normalText]} numberOfLines={1}>Flash amount:- ₹{item.item.menu.unitCost}</Text> */}
                        <Text style={[s.normalText]} numberOfLines={3}>Unit:- {item.item.menu.unit}</Text>
                    </View>
                    <View style={styles.flexRow}>
                        <TouchableOpacity style={styles.basketBtn} onPress={() => this.showBasketModal(item.item)}>
                        <Image style={styles.basketImg} source={require('../../../assets/basketIcon.png')}/>
                        </TouchableOpacity>
                        {/* <TouchableOpacity onPress={() => this.addCart(item.item)}>
                          <View style={[styles.storeBtn, styles.addtoCartBtnColor]}>
                              <Text style={{ color: '#0080FE',paddingHorizontal: 4}}>ADD</Text>
                          </View>
                        </TouchableOpacity> */}
                        {this.props.cartItem.cartItem.findIndex(ind => ind.productId === item.item.menu.productId) >= 0
                        ?
                          <View style={styles.flexRowAdd}>
                            <TouchableOpacity onPress={() => this.decrementCount(item.item)}>
                              <View style={styles.orderCardButton}>
                                <Text style={{fontSize:18,color:"#0080FE"}}>-</Text>
                              </View>
                            </TouchableOpacity>
                              {/* <Text style={styles.orderCount}>{item.item.counter}</Text> */}
                              <Text style={styles.orderCount}>{this.props.cartItem.cartItem[this.props.cartItem.cartItem.findIndex(ind => ind.productId === item.item.menu.productId)].counter}</Text>
                            <TouchableOpacity onPress={() => this.incrementCount(item.item)}>
                              <View style={styles.orderCardButton}>
                                <Text style={{fontSize:18,color:"#0080FE"}}>+</Text>
                              </View>
                            </TouchableOpacity>
                          </View>
                        :
                          <TouchableOpacity style={[styles.storeBtn, styles.addtoCartBtnColor]} onPress={() => this.addCart(item.item)}>
                            <Text style={{ color: '#0080FE',paddingHorizontal: 4}}>ADD</Text>
                          </TouchableOpacity>

                        }
        
                    </View>
                  </View>
              </TouchableOpacity>
              
          </View>
          )
      } else {
        return (
          <View style={[styles.prodcutListingView]}>
              <View style={styles.addCardImage}>
                <Image style={styles.productImage} source={{uri: item.item.menu.productLogo}}/>
              </View>
              <View style={styles.stores}>
                  <View style={{flex:10}}>
                  <Text style={s.subHeaderText} numberOfLines={3}>{item.item.menu.productName} </Text>
                  </View>
                  {item.item.menu.mrp != item.item.menu.unitCost 
                  ? 
                  <>
                  <View style={{flex:1, flexDirection:"row", marginTop: 10}}>
                      <Text style={[s.normalText]}>MRP:- </Text>
                      <Text style={[s.normalText,{textDecorationLine: 'line-through', textDecorationStyle: 'solid'}]} numberOfLines={1}>
                      ₹{item.item.menu.mrp}</Text>
                  </View>
                  <View style={{flex:1, flexDirection:"row"}}>
                      <Text style={[s.normalText]}>Flash amount:- </Text>
                      <Text style={[s.normalText]} numberOfLines={1}>₹{item.item.menu.unitCost}</Text>
                  </View>
                  </>
                  : 
                  <View style={{flex:1, flexDirection:"row", marginTop: 10}}>
                      <Text style={[s.normalText]}>Flash amount:- </Text>
                      <Text style={[s.normalText]} numberOfLines={1}>₹{item.item.menu.unitCost}</Text>
                  </View>
                  }
                  
                  
              
                  <View style={styles.Mystores}>
                  <View style={{paddingTop: 10, flex: 7}}>
                      {/* <Text style={[s.normalText,{textDecorationLine: 'line-through', textDecorationStyle: 'solid'}]} numberOfLines={1}>
                      MRP:- ₹{item.item.menu.unitCost}</Text>
                      <Text style={[s.normalText]} numberOfLines={1}>Flash amount:- ₹{item.item.menu.unitCost}</Text> */}
                      <Text style={[s.normalText]} numberOfLines={3}>Unit:- {item.item.menu.unit}</Text>
                  </View>
                  <View style={styles.flexRow}>
                      <TouchableOpacity style={styles.basketBtn} onPress={() => this.showBasketModal(item.item)}>
                      <Image style={styles.basketImg} source={require('../../../assets/basketIcon.png')}/>
                      </TouchableOpacity>
                      {/* <TouchableOpacity onPress={() => this.addCart(item.item)}>
                        <View style={[styles.storeBtn, styles.addtoCartBtnColor]}>
                            <Text style={{ color: '#0080FE',paddingHorizontal: 4}}>ADD</Text>
                        </View>
                      </TouchableOpacity> */}
                      {this.props.cartItem.cartItem.findIndex(ind => ind.productId === item.item.menu.productId) >= 0
                      ?
                        <View style={styles.flexRowAdd}>
                          <TouchableOpacity onPress={() => this.decrementCount(item.item, 'ProductList')}>
                            <View style={styles.orderCardButton}>
                              <Text style={{fontSize:18,color:"#0080FE"}}>-</Text>
                            </View>
                          </TouchableOpacity>
                            {/* <Text style={styles.orderCount}>{item.item.counter}</Text> */}
                            <Text style={styles.orderCount}>{this.props.cartItem.cartItem[this.props.cartItem.cartItem.findIndex(ind => ind.productId === item.item.menu.productId)].counter}</Text>
                          <TouchableOpacity onPress={() => this.incrementCount(item.item, 'ProductList')}>
                            <View style={styles.orderCardButton}>
                              <Text style={{fontSize:18,color:"#0080FE"}}>+</Text>
                            </View>
                          </TouchableOpacity>
                        </View>
                      :
                        <TouchableOpacity style={[styles.storeBtn, styles.addtoCartBtnColor]} onPress={() => this.addCart(item.item, 'ProductList')}>
                          <Text style={{ color: '#0080FE',paddingHorizontal: 4}}>ADD</Text>
                        </TouchableOpacity>

                      }
      
                  </View>
                  </View>
              </View>
              
          </View>
          )
      }
    }

    incrementCount = async(productData) => {
      this.setState({
        selectedProduct: productData,
      });
      let index = this.props.cartItem.cartItem.findIndex(ind => ind.productId === productData.menu.productId);
      this.props.cartState.cartCount += 1;
      this.props.cartState.cartSum = parseFloat(this.props.cartState.cartSum) + parseFloat(this.props.cartItem.cartItem[index].unitCost); 
      this.props.cartModalAction.changeCartState(true,this.props.cartState.cartSum,this.props.cartState.cartCount);
      this.props.cartItem.cartItem[index].counter = this.props.cartItem.cartItem[index].counter + 1;
      this.props.cartItem.cartItem[index].totalCost = this.props.cartItem.cartItem[index].unitCost * this.props.cartItem.cartItem[index].counter;
      this.props.actions.replaceCart(this.props.cartItem.cartItem);
      await this.setState({ shopMenu: this.state.shopMenu});
    }
  
    decrementCount = async(productData) => {
      this.setState({
        selectedProduct: productData
      });
      let id = this.props.cartItem.cartItem.findIndex(ind => ind.productId === productData.menu.productId);
     if (id >= 0) {
      if(this.props.cartItem.cartItem[id].counter - 1 <= 0){
        this.setState({
          cartRemoveModal: true
        });
      } else {
        this.props.cartItem.cartItem[id].counter = this.props.cartItem.cartItem[id].counter - 1;
        this.props.cartItem.cartItem[id].totalCost = this.props.cartItem.cartItem[id].unitCost * this.props.cartItem.cartItem[id].counter;
        this.props.cartState.cartCount -= 1;
        this.props.cartState.cartSum = parseFloat(this.props.cartState.cartSum) - parseFloat(this.props.cartItem.cartItem[id].unitCost); 
        this.props.cartModalAction.changeCartState(true,this.props.cartState.cartSum,this.props.cartState.cartCount);
        this.props.actions.replaceCart(this.props.cartItem.cartItem);
        await this.setState({ shopMenu: this.state.shopMenu});
      }
     }
      
      
      
    }
  
    removeProduct = async() => {
  
      this.setState({
        cartRemoveModal: false,
        removeProduct: true,
        modalVisible: false,
        paddingBottomHeight: 0
      });
      const i = this.props.cartItem.cartItem.findIndex(ind => ind.productId === this.state.selectedProduct.menu.productId);
      
      this.props.cartState.cartCount -= 1;
      this.props.cartState.cartSum = parseFloat(this.props.cartState.cartSum) - parseFloat(this.props.cartItem.cartItem[i].unitCost); 
      
      this.props.cartItem.cartItem.splice(i,1);
      if(this.props.cartState.cartCount === 0){
        this.props.cartModalAction.changeCartState(false,this.props.cartState.cartSum,this.props.cartState.cartCount);
      } else {
        this.props.cartModalAction.changeCartState(true,this.props.cartState.cartSum,this.props.cartState.cartCount);
      }
      this.props.actions.replaceCart(this.props.cartItem.cartItem);
      await this.setState({ shopMenu: this.state.shopMenu});
    }

    //function to add the new item in the cart   --- Start
    addCart = (productData) => {
      console.warn('in add cat data -->', productData.shopDetails.deliveryTime);
        const cart = {
          productId: productData.menu.productId,
          productName: productData.menu.productName,
          productLogo: productData.menu.productLogo,
          productDescription: productData.menu.productDescription,
          unitCost: parseFloat(productData.menu.unitCost),
          unit: productData.menu.unit,
          shopId: this.state.shopId,
          shopName: this.state.shopName,
          category: this.state.categoryId,
          counter: 1,
          totalCost: parseFloat(productData.menu.unitCost),  
          isProductAvailable: true,
          cgst: parseFloat(productData.menu.cgst),
          sgst: parseFloat(productData.menu.sgst),
          costExlGst: parseFloat(productData.menu.unitCostExclTax),
          shopRate: parseFloat(productData.menu.shopRate),
          shopAddress: productData.shopDetails.shopAdress,
          shopContactNumber: productData.shopDetails.shopContactNumber,
          shopEmail: productData.shopDetails.shopEmail,
          shopLocation: productData.shopDetails.shopLocation,
          shopOwnerName: productData.shopDetails.shopOwnerName,
          deliveryTime: productData.shopDetails.deliveryTime,
          isBogo: false
        }

        this.setState({
            cart: cart 
        })
        
        console.warn('testtttt-->', this.props.cartItem.cartItem.some(arr => arr.shopId == this.state.shopId));

        if(this.props.cartItem.cartItem.some(arr => arr.productId == productData.menu.productId)){
        Toast.show("Already added in the cart",Toast.LONG);
        } else if (!this.props.cartItem.cartItem.some(arr => arr.shopId == this.state.shopId)) {
          if (this.props.cartItem.cartItem.length > 0) {
            if (!this.props.cartItem.cartItem.some(arr => arr.deliveryTime == this.state.deliveryTime)) {
              this.setState({
                modalVisible: true,
                cartModalMessage: 'Delivery time and delivery charges of your order added in cart depends upon the delivery time of the individual shop and number of shop you shopped, if ordered time crosses the delivery time of any shop(s) then the whole order will be delivered tomorrow or next day.'
              })
            } else {
              this.setState({
                modalVisible: true,
                cartModalMessage: 'Delivery charges may vary as per the changes in different shop locations. Would you like to continue with your purchase?'
              })
            }
          } else {
              this.confirmAddCart(cart);
          }
        } else{
        this.confirmAddCart(cart);
        }
    }

    confirmAddCart = (cart) => {
        this.props.cartState.cartCount += 1;
        this.props.cartState.cartSum = (parseFloat(this.props.cartState.cartSum) + parseFloat(cart.unitCost)); 
        this.props.cartModalAction.changeCartState(true,this.props.cartState.cartSum,this.props.cartState.cartCount);
        this.props.actions.addToCart(cart);
        this.setState({paddingBottomHeight: 100, shopMenu: this.state.shopMenu});
        Toast.show(cart.productName + " is added in the cart",Toast.LONG);

    }

    userAckCart = () => {
        this.setState({
        modalVisible: false
        })
        this.confirmAddCart(this.state.cart);

    }
    showBasketModal = (basketData) => {
        this.setState({
          showBasketModal: true,
          addtoBasketItem: basketData
        })
    }

    getShopMenu = (reqBody) => {
        console.warn('request body--->', reqBody);
        axios.post(
          Config.URL + Config.getShopMenu,
          reqBody,
          {
              headers: {
                  'Content-Type': 'application/json',
              }
          }
        )
        .then(res => {
          this.setState({showLoader: false});
            
            if (res.data.status == 0) {
              console.warn('data--- this.state.shopMenu', res.data.result);
              this.setState({
                shopMenu: res.data.result,
              })
              
              if (this.state.shopMenu.length <= 0) {
                this.setState({productNotAvailable: true})
                Toast.show('Sorry!!! Searched product is not available in the list',Toast.LONG);
              } else {
                this.setState ({
                  productNotAvailable: false
                })
              }
            } else if(res.data.status == 1){  // Fail to signup
              console.warn('response get menu-->', res.data);
              Toast.show(res.data.message,Toast.LONG);
            }
    
        }).catch(err => {
            console.warn('error-->', err);
            this.setState({showLoader: false});
        })
    }

    searchData = (searchText) => {
        console.warn('search test in search data',);
       this.setState({searchText: searchText});
   
       if(searchText == ''){
         var searchObj = {
           "searchData": '',
           "shopId": this.state.shopId,
         };
         this.setState({productNotAvailable: false, shopMenu: []})
        //  this.getShopMenu(searchObj);
       } else if (searchText.length >= 1) {
         var searchObj = {
           "searchData": searchText,
           "shopId": this.state.shopId,
         }
         console.warn('test--', searchObj);
         this.getShopMenu(searchObj);
       }
     
     }

    ProductNotAvailable = () => {
        if (this.state.productNotAvailable) {
            return (
                <View style={{flex: 1, alignItems:"center", justifyContent:"center"}}onPress={() => this.setState({showList: false})}>
                    <Text style={[s.normalText,{paddingHorizontal: 15, paddingTop: 40, textAlign:"center"} ]} numberOfLines={3}>Sorry!!! Searched product is not available in the shop</Text>
                </View>
            )
        } else {
          return null;
        }
        
    } 

    hideModal = () => { 
        this.setState({
          showBasketModal: false,
          modalVisible: false,
          cartRemoveModal: false
        })
    }

    render() {
        const {navigate} = this.props.navigation;
        return(
            <SafeAreaView style={{flex: 1}}>
                <View style={{flex: 1}}>
                    <View style={[s.bodyGray, {flex: 1}]}>
                    <AnimatedLoader
                        visible={this.state.showLoader}
                        overlayColor="rgba(255,255,255,0.75)"
                        source={require("../../../assets/loader.json")}
                        animationStyle= {[styles.lottie,{height: 150,width: 150}]}
                        speed={1}
                    />
                        <View style={styles.sectionContainer}>
                            {/* <TextInput placeholder = "Search product...."
                                value = {this.state.searchText}
                                style={styles.searchShops} inlineImageLeft='searchicon' 
                                onChangeText={(searchText) => this.searchData(searchText)}/> */}
                              <DelayInput placeholder = "Search product...."
                                delayTimeout={500}
                                minLength={3}
                                value = {this.state.searchText}
                                style={styles.searchShops} inlineImageLeft='searchicon' 
                                onChangeText={(searchText) => this.searchData(searchText)} />  
                        </View>
                        <ScrollView showsVerticalScrollIndicator={false} style={[{marginBottom: this.state.paddingBottomHeight}]} >
                            <FlatList
                                data={this.state.shopMenu}
                                renderItem={( { item, index } ) => (
                                <this.ProductListing
                                item= {item}
                                />
                                )}
                                keyExtractor= {item => item.productId}
                                extraData={this.state}
                            /> 
                        </ScrollView>
                        <this.ProductNotAvailable />
                        <BasketModal visible={this.state.showBasketModal} onDismiss={this.hideModal} navigation= {navigate} basketData= {this.state.addtoBasketItem}
                        shopId={this.state.shopId} shopName={this.state.shopName} categoryId={this.state.categoryId} onRequestClose={() => { this.hideModal()}} />  
                        <CartModal navigate={navigate} navigationPage={'orderDetailsPage'}></CartModal>
                        <PopupModal visible={this.state.cartRemoveModal} onDismiss={this.hideModal} removeProduct={this.removeProduct} 
                          message={"Are you sure want to remove the product from cart?"} btnTag={"Remove"}/>
                        <PopupModal visible={this.state.modalVisible} onDismiss={this.hideModal} removeProduct={this.userAckCart} 
                        message={this.state.cartModalMessage} 
                        btnTag={"Add to cart"}/>  
                    </View>
                </View>
               

            </SafeAreaView>
            
        );
    }
}
    

let ScreenWidth = Dimensions.get("window").width;
let ScreenHeight = Dimensions.get("window").height;

const styles = StyleSheet.create({
  scrollView: {
    flex: 1
  },
  imgBlank: {
    height: ScreenHeight,
    width: ScreenWidth
  },
  containerIcons:{
    width:30,
    height:30,
  },

  searchShops:{ 
    height: 40, 
    borderColor: '#7285A5', 
    borderWidth: 1,
    paddingHorizontal:10, 
    borderRadius:4,
    backgroundColor: "white" 
  },
  sectionContainer: {
    marginTop: 20,
    paddingHorizontal: 24,
  },
  iconView: {
    width: 30,
    height: 30,
    marginRight: 20,
    borderRadius: 100,
  },
  storeCardLayout:{
    marginHorizontal: 15,
    paddingVertical:14,
    borderBottomWidth: 2,
    borderBottomColor: "#131E3A",
    alignItems: "center"
  },
  categoryFooter:{
    width:ScreenWidth,
    paddingBottom:20,
    height:80,
    flexGrow:0.05,
    backgroundColor:'white',
    display:'flex',
    flexDirection:'row',
    justifyContent:"space-evenly",
    alignItems:"center"
  },
  orderCardButton:{
    borderColor:'#0080FE',
    borderWidth: 1,
    width:25,
    height:25,
    borderRadius:50,
    alignItems:"center",
    justifyContent:'center',
  },
  orderCount:{
    fontSize:18,
    fontWeight:'bold',
    paddingHorizontal:10,
    color: '#131E3A'
  },
  storeContainer:{
    display:"flex",
    flexDirection: "row"
  },
  stores: {
    paddingLeft: 15,
    flex: 9,
    flexDirection: "column"
  },
  textPadding: {
    paddingVertical: 2
  },
  storeCount: {
    fontSize: 22,
    fontWeight: 'bold',
    paddingHorizontal: 10,
  },
  storeProductHeading: {
    fontWeight: 'bold',
    fontSize: 30,
  },
  shopdtlCards: {
    width: 80,
    height: 80,
    backgroundColor: 'orange',
    borderRadius: 100,
    borderWidth: 0.5,
    borderColor: 'black',
    marginVertical: 5,
    marginHorizontal: 10,
  },
  storeCardDisplay: {
    // padding: 10,
    paddingVertical: 10,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  storeCards: {
    height: 100,
    borderRadius: 4,
    borderWidth: 0.5,
    borderColor: 'black',
    backgroundColor: 'white',
    flex: 3,
  },
  advertiseData: {
    height: 200,
    width: ScreenWidth,
    elevation: 4
  },
  advertiseShop: {
    height: 200,
    width: ScreenWidth,
    elevation: 4,
    borderBottomLeftRadius: 60,
    borderBottomRightRadius: 60,
  },
  flexRowAdd: {
    marginLeft: 10,
    marginBottom: 5, 
    // paddingTop: 10,
    textAlignVertical: "bottom",
    flexDirection: 'row',
  },
  imageView: {
    flex: 1,
    width: undefined,
    height: undefined,
    resizeMode: 'contain',
    borderBottomLeftRadius: 60,
    borderBottomRightRadius: 60,
  },
  cardImageView: {
    flex: 1,
    width: undefined,
    height: undefined,
    resizeMode: 'contain',
  },
  storeHeading: {
    fontSize:24,
    
  },
  storeDesc: {
    alignItems: "center"
  },
  trendingShops: {
    borderBottomWidth: 2,
    borderColor: "#131E3A",
    marginHorizontal: 15,
    paddingTop: 10,
  },
  addCardImage: {
    width: 80,
    height: 80,
    flex: 3
  },
  cardImage: {
    width: (ScreenWidth / 2) - 20,
    height: 80,
  },
  productImage: {
    width: 80,
    height: 80,
  },
  storeCardButton: {
    backgroundColor: 'white',
    color: 'black',
    width: 30,
    height: 30,
    borderRadius: 50,
    alignItems: "center",
    justifyContent: 'center',
  },
  storeborder: {
    borderBottomColor: 'grey',
    borderBottomWidth: 1,
  },
  flexRow: {
    flex: 7,
    textAlignVertical: "bottom",
    flexDirection: 'row',
    alignItems:"flex-end",
    paddingRight: 10
  },
  prodcutListingView: {
    flexDirection: 'row',
    marginVertical: 5,
    backgroundColor: "white",
    padding: 10,
    elevation: 6,
    marginHorizontal: 10,
  },
  flexColumnAlignEnd: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-end'
  },
  storeDtlCenter: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "center"
  },
  Mystores: {
    flexDirection: "row",
    alignItems: "flex-start",
    justifyContent: "space-between",
    textAlignVertical: "bottom",
    flex: 2,
    marginTop: 5
    // paddingBottom:15,
  },
  storeCardSpace: {
    marginBottom: 15,
  },
  headerstyle: {
    paddingBottom: 20,
    paddingTop: 20
  },

  storeBtnViewColor: {
    backgroundColor: '#ba55d3',
    color: 'white',
  },
  continueBtnViewColor: {
    backgroundColor: '#2f4f4f',
    color: 'white',
  },
  addtoCartBtnColor: {
    fontSize:12,
    borderWidth: 1,
    borderColor: '#0080FE',
    marginLeft: 20,
  },
  storeBtn: {
    paddingVertical: 5,
    paddingHorizontal: 5,
    marginBottom: 5,
    color: 'white',
    borderRadius: 0,
    width: 50
  },
  basketBtn:{
    paddingVertical: 5,
    paddingRight: 15,
    marginBottom: 10,
    borderRadius: 20,
    height: 30,
    width: 30,
  },
  basketImg: {
    height: 30,
    width: 30,
  }

});

//--------------------------------------------------- store related fuction  -- Start

const mapStateToProps = (state) => {
	return {
    cartItem: state.cartItem,
    basketItem: state.basketItem,
    cartState: state.cartState,
    userData: state.loginData,
	}
}

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(addToCart, dispatch),
        basketAction: bindActionCreators(basketAction,dispatch),
        cartModalAction: bindActionCreators(cartModalAction,dispatch),
        loginActions: bindActionCreators(loginSuccess,dispatch),
        navigationAction: bindActionCreators(navigateAction,dispatch),
    };
}

// ---------------------------------------------------- store related function  -- END


export default connect(mapStateToProps, mapDispatchToProps)(ProductListPage);


