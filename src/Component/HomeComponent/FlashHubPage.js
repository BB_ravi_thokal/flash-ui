/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import * as addToCart  from '../../Actions/CartItem';
import { bindActionCreators } from 'redux';
import axios from 'axios';
import * as Config from '../../Constant/Config.js';
import * as Constant from '../../Constant/Constant.js';
import NavigationBar from '../ReusableComponent/NavigationBar.js'
import  s  from '../../sharedStyle.js'
import { Rating } from 'react-native-ratings';
import BasketModal from '../ReusableComponent/basketModal';
import CartModal from '../ReusableComponent/cartModal';
import OnGoingActivityModal from '../ReusableComponent/OnGoingActivityModal';
import * as basketAction  from '../../Actions/BasketAction';
import * as cartModalAction  from '../../Actions/CartModalAction';
import InternateModal from '../ReusableComponent/InternetModal';
import PopupModal from '../ReusableComponent/PopupComponent.js';
import * as loginSuccess  from '../../Actions/LoginAction';
import * as navigateAction  from '../../Actions/NavigationBarAction';
import AnimatedLoader from "react-native-animated-loader";
import Toast from 'react-native-simple-toast';
import DelayInput from "react-native-debounce-input";
import callAPI from '../../CoreFunctions/callAPI';
import { NavigationActions, StackActions } from 'react-navigation';
// import SceneLoader from 'react-native-scene-loader';

import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  Dimensions,
  TouchableOpacity,
  TextInput,
  Image,
  FlatList,
  ActivityIndicator,
} from 'react-native';

function SubCategoryListView ({item, self}) {
  return (
    <TouchableOpacity style={[{width: (ScreenWidth)/3 - 2}, {height: (ScreenWidth)/3 - 2},
        {marginHorizontal: 2}, {marginBottom: 2, borderColor:"#DCF0FA", borderWidth:1}]} onPress={() => self.fetchSubCategoryMenu(item._id)}>
        <Image style={{flex: 1, width: undefined, height: undefined}} source={{uri: item.productSubCategoryLogo}}></Image>
    </TouchableOpacity>
  )
}



class FlashHubPage extends React.Component{
  constructor(props) {
    super(props);
    this.state={
      shopData: "",
      shopMenu: "",
      shopName: "",
      shopDescription: "",
      shopAddress: "",
      shopRating: null,
      shopId: "",
      categoryId: "",
      subCategoryId: "",
      cartItem: [],
      searchText: "",
      showBasketModal: false,
      addtoBasketItem: {},
      isShopAvailable: true,
      modalVisible: false,
      cart: {},
      distinctCategories: [],
      selectedCategory: '',
      selectedSubCategory: '',
      showLoader: false,
      cartRemoveModal: false,
      selectedProduct: {},
      selectedProductType: '',
      // page: 1,
      // prev: true,
      // next: false,
      // maxCount: 0,
      // showList: false,
      // prevColor: "#00CCFF",
      // nextColor: "#00CCFF",
      subCategoryList: [],
      paddingBottomHeight: 0,
      productNotAvailable: false,
      bogoMenu: [],
      bestSellerProducts: [],
      refreshList: false,
      deliveryTime: '',
      cartModalMessage: '',
      headTitleFirst: '',
      headTitleSecond: ''
    }
  }


  fetchSubCategoryMenu = (id) => {
    this.setState({showList: false, selectedSubCategory: id});
    // console.warn('in fetch sub category ');
    // reqBody = {
    //   categoryId: this.state.categoryId,
    //   productCategory: this.state.selectedCategory,
    //   productSubCategory: id,
    //   searchData: "",
    // }
    // this.getShopMenu(reqBody);
    this.props.navigation.navigate('productListPage',{"categoryId": this.state.categoryId, "searchText": '', "shopId": this.state.shopId,
    "productSubCategory": id, "productCategory": this.state.selectedCategory, "shopName": this.state.shopName, "deliveryTime": this.state.deliveryTime});
   
  }

  getSubCategories = async(body) => {
    // console.warn('in et sub cat req body', body);
    this.setState({showLoader: true});
    const getSubCategory = await callAPI.axiosCall(Config.getMenuSubCategories, body);
    this.setState({showLoader: false});
    if (getSubCategory) {
      if (getSubCategory.data.status == 0) {
        this.setState({
          subCategoryList: getSubCategory.data.result
        });
      }
      else if(getSubCategory.data.status == 1){  // Fail to signup
        Toast.show(getSubCategory.data.message,Toast.LONG);
      }
    }
  }

  BestSellingProducts = (item) => {
    const {navigate} = this.props.navigation;
    if (item.item.menu.isClickable) {
      return (
          <View style={{backgroundColor: '#f1f1f1', padding: 10, marginHorizontal: 10, elevation: 4, borderColor:"#DCF0FA", borderWidth: 1, borderRadius: 5, width: (ScreenWidth / 2) - 20}}>
            <TouchableOpacity style={styles.cardImage} onPress={() => navigate('productDetailsPage', {productData: item.item, shopId: this.state.shopId, shopName: this.state.shopName, category: this.state.categoryId, deliveryTime: this.state.deliveryTime})}>
              {/* <Image style={styles.cardImageView} source={require('../../../assets/foodcategory.jpeg')}/> */}
              <Image style={styles.cardImageView} source={{uri: item.item.menu.productLogo}}/>
            </TouchableOpacity>
            <View style={{paddingVertical: 10}}>
              <Text style={[s.subHeaderText, styles.textPadding ]} numberOfLines={3}>{item.item.menu.productName} </Text>
              <Text style={[s.normalText, styles.textPadding]} numberOfLines={3}>{item.item.menu.unit}</Text>
              <View style={{flexDirection: "row", marginTop: 10}}>
                <View style={{flex: 6}}>
                  {item.item.menu.mrp != item.item.menu.unitCost
                    ?
                    <>
                      <Text style={[s.subHeaderText, styles.textPadding]} numberOfLines={1}>₹{item.item.menu.unitCost}</Text>
                      <Text style={[s.normalText,{textDecorationLine: 'line-through', textDecorationStyle: 'solid'}, styles.textPadding]} numberOfLines={1}>
                        ₹{item.item.menu.mrp}</Text>
                    </>
                    :
                      <Text style={[s.subHeaderText, styles.textPadding]} numberOfLines={1}>₹{item.item.menu.unitCost}</Text>
                  }
                </View>
                <View style={{flex: 6, alignItems: 'flex-end'}}>
                  {this.props.cartItem.cartItem.findIndex(ind => ind.productId === item.item.menu.productId) >= 0
                  ?
                    <View style={styles.flexRowAdd}>
                      <TouchableOpacity onPress={() => this.decrementCount(item.item, 'BestSelling')}>
                        <View style={styles.orderCardButton}>
                          <Text style={{fontSize:18,color:"#0080FE"}}>-</Text>
                        </View>
                      </TouchableOpacity>
                        {/* <Text style={styles.orderCount}>{item.item.counter}</Text> */}
                        <Text style={styles.orderCount}>{this.props.cartItem.cartItem[this.props.cartItem.cartItem.findIndex(ind => ind.productId === item.item.menu.productId)].counter}</Text>
                      <TouchableOpacity onPress={() => this.incrementCount(item.item, 'BestSelling')}>
                        <View style={styles.orderCardButton}>
                          <Text style={{fontSize:18,color:"#0080FE"}}>+</Text>
                        </View>
                      </TouchableOpacity>
                    </View>
                  :
                  <TouchableOpacity style={[styles.storeBtn, styles.addtoCartBtnColor]} onPress={() => this.addCart(item.item, 'BestSelling')}>
                    <Text style={{ color: '#0080FE',paddingHorizontal: 4}}>ADD</Text>
                  </TouchableOpacity>

                  }
                </View>
              </View>
            </View>
          </View>
        )
    } else {
      return (
          <View style={{backgroundColor: '#f1f1f1', padding: 10, marginHorizontal: 10, elevation: 4, borderColor:"#DCF0FA", borderWidth: 1, borderRadius: 5, width: (ScreenWidth / 2) - 20}}>
            <View style={styles.cardImage}>
              {/* <Image style={styles.cardImageView} source={require('../../../assets/foodcategory.jpeg')}/> */}
              <Image style={styles.cardImageView} source={{uri: item.item.menu.productLogo}}/>
            </View>
            <View style={{paddingVertical: 10}}>
              <Text style={[s.subHeaderText, styles.textPadding ]} numberOfLines={3}>{item.item.menu.productName} </Text>
              <Text style={[s.normalText, styles.textPadding]} numberOfLines={3}>{item.item.menu.unit}</Text>
              <View style={{flexDirection: "row", marginTop: 10}}>
                <View style={{flex: 6}}>
                  {item.item.menu.mrp != item.item.menu.unitCost
                    ?
                    <>
                      <Text style={[s.subHeaderText, styles.textPadding]} numberOfLines={1}>₹{item.item.menu.unitCost}</Text>
                      <Text style={[s.normalText,{textDecorationLine: 'line-through', textDecorationStyle: 'solid'}, styles.textPadding]} numberOfLines={1}>
                        ₹{item.item.menu.mrp}</Text>
                    </>
                    :
                      <Text style={[s.subHeaderText, styles.textPadding]} numberOfLines={1}>₹{item.item.menu.unitCost}</Text>
                  }
                </View>
                <View style={{flex: 6, alignItems: 'flex-end'}}>
                {this.props.cartItem.cartItem.findIndex(ind => ind.productId === item.item.menu.productId) >= 0
                  ?
                    <View style={styles.flexRowAdd}>
                      <TouchableOpacity onPress={() => this.decrementCount(item.item, 'BestSelling')}>
                        <View style={styles.orderCardButton}>
                          <Text style={{fontSize:18,color:"#0080FE"}}>-</Text>
                        </View>
                      </TouchableOpacity>
                        {/* <Text style={styles.orderCount}>{item.item.counter}</Text> */}
                        <Text style={styles.orderCount}>{this.props.cartItem.cartItem[this.props.cartItem.cartItem.findIndex(ind => ind.productId === item.item.menu.productId)].counter}</Text>
                      <TouchableOpacity onPress={() => this.incrementCount(item.item, 'BestSelling')}>
                        <View style={styles.orderCardButton}>
                          <Text style={{fontSize:18,color:"#0080FE"}}>+</Text>
                        </View>
                      </TouchableOpacity>
                    </View>
                  :
                    <TouchableOpacity style={[styles.storeBtn, styles.addtoCartBtnColor]} onPress={() => this.addCart(item.item, 'BestSelling')}>
                      <Text style={{ color: '#0080FE',paddingHorizontal: 4}}>ADD</Text>
                    </TouchableOpacity>

                }
                </View>
              </View>
            </View>
          </View>
        )
    }
  }

  BogoProductList = (item) => {
    const {navigate} = this.props.navigation;
    if (item.item.menu.isClickable) {
      return (
          <View style={{backgroundColor: '#f1f1f1', padding: 10, marginHorizontal: 10, elevation: 4, borderColor:"#DCF0FA", borderWidth: 1, borderRadius: 5, width: (ScreenWidth / 2) - 20}}>
            <TouchableOpacity style={styles.cardImage} onPress={() => navigate('productDetailsPage', {productData: item.item, shopId: this.state.shopId, shopName: this.state.shopName, category: this.state.categoryId, deliveryTime: this.state.deliveryTime})}>
              {/* <Image style={styles.cardImageView} source={require('../../../assets/foodcategory.jpeg')}/> */}
              <Image style={styles.cardImageView} source={{uri: item.item.menu.productLogo}}/>
            </TouchableOpacity>
            <View style={{paddingVertical: 10}}>
              <Text style={[s.subHeaderText, styles.textPadding ]} numberOfLines={3}>{item.item.menu.productName} </Text>
              <Text style={[s.normalText, styles.textPadding]} numberOfLines={3}>{item.item.menu.unit}</Text>
              <View style={{flexDirection: "row", marginTop: 10}}>
                <View style={{flex: 6}}>
                  {item.item.menu.mrp != item.item.menu.unitCost
                    ?
                    <>
                      <Text style={[s.subHeaderText, styles.textPadding]} numberOfLines={1}>₹{item.item.menu.unitCost}</Text>
                      <Text style={[s.normalText,{textDecorationLine: 'line-through', textDecorationStyle: 'solid'}, styles.textPadding]} numberOfLines={1}>
                        ₹{item.item.menu.mrp}</Text>
                    </>
                    :
                      <Text style={[s.subHeaderText, styles.textPadding]} numberOfLines={1}>₹{item.item.menu.unitCost}</Text>
                  }
                </View>
                <View style={{flex: 6, alignItems: 'flex-end'}}>
                  {/* <TouchableOpacity style={[styles.storeBtn, styles.addtoCartBtnColor]} onPress={() => this.addCart(item.item, 'BogoList')}>
                    <Text style={{ color: '#0080FE',paddingHorizontal: 4}}>ADD</Text>
                  </TouchableOpacity> */}
                  {this.props.cartItem.cartItem.findIndex(ind => ind.productId === item.item.menu.productId) >= 0
                  ?
                    <View style={styles.flexRowAdd}>
                      <TouchableOpacity onPress={() => this.decrementCount(item.item, 'BogoList')}>
                        <View style={styles.orderCardButton}>
                          <Text style={{fontSize:18,color:"#0080FE"}}>-</Text>
                        </View>
                      </TouchableOpacity>
                        {/* <Text style={styles.orderCount}>{item.item.counter}</Text> */}
                        <Text style={styles.orderCount}>{this.props.cartItem.cartItem[this.props.cartItem.cartItem.findIndex(ind => ind.productId === item.item.menu.productId)].counter}</Text>
                      <TouchableOpacity onPress={() => this.incrementCount(item.item, 'BogoList')}>
                        <View style={styles.orderCardButton}>
                          <Text style={{fontSize:18,color:"#0080FE"}}>+</Text>
                        </View>
                      </TouchableOpacity>
                    </View>
                  :
                    <TouchableOpacity style={[styles.storeBtn, styles.addtoCartBtnColor]} onPress={() => this.addCart(item.item, 'BogoList')}>
                      <Text style={{ color: '#0080FE',paddingHorizontal: 4}}>ADD</Text>
                    </TouchableOpacity>

                  }
                </View>
              </View>
            </View>
          </View>
        )
    } else {
      return (
          <View style={{backgroundColor: '#f1f1f1', padding: 10, marginHorizontal: 10, elevation: 4, borderColor:"#DCF0FA", borderWidth: 1, borderRadius: 5, width: (ScreenWidth / 2) - 20}}>
            <View style={styles.cardImage}>
              {/* <Image style={styles.cardImageView} source={require('../../../assets/foodcategory.jpeg')}/> */}
              <Image style={styles.cardImageView} source={{uri: item.item.menu.productLogo}}/>
            </View>
            <View style={{paddingVertical: 10}}>
              <Text style={[s.subHeaderText, styles.textPadding ]} numberOfLines={3}>{item.item.menu.productName} </Text>
              <Text style={[s.normalText, styles.textPadding]} numberOfLines={3}>{item.item.menu.unit}</Text>
              <View style={{flexDirection: "row", marginTop: 10}}>
                <View style={{flex: 6}}>
                  {item.item.menu.mrp != item.item.menu.unitCost
                    ?
                    <>
                      <Text style={[s.subHeaderText, styles.textPadding]} numberOfLines={1}>₹{item.item.menu.unitCost}</Text>
                      <Text style={[s.normalText,{textDecorationLine: 'line-through', textDecorationStyle: 'solid'}, styles.textPadding]} numberOfLines={1}>
                        ₹{item.item.menu.mrp}</Text>
                    </>
                    :
                      <Text style={[s.subHeaderText, styles.textPadding]} numberOfLines={1}>₹{item.item.menu.unitCost}</Text>
                  }
                </View>
                <View style={{flex: 6, alignItems: 'flex-end'}}>
                  {/* <TouchableOpacity style={[styles.storeBtn, styles.addtoCartBtnColor]} onPress={() => this.addCart(item.item, 'BogoList')}>
                    <Text style={{ color: '#0080FE',paddingHorizontal: 4}}>ADD</Text>
                  </TouchableOpacity> */}
                  {this.props.cartItem.cartItem.findIndex(ind => ind.productId === item.item.menu.productId) >= 0
                  ?
                    <View style={styles.flexRowAdd}>
                      <TouchableOpacity onPress={() => this.decrementCount(item.item, 'BogoList')}>
                        <View style={styles.orderCardButton}>
                          <Text style={{fontSize:18,color:"#0080FE"}}>-</Text>
                        </View>
                      </TouchableOpacity>
                        {/* <Text style={styles.orderCount}>{item.item.counter}</Text> */}
                        <Text style={styles.orderCount}>{this.props.cartItem.cartItem[this.props.cartItem.cartItem.findIndex(ind => ind.productId === item.item.menu.productId)].counter}</Text>
                      <TouchableOpacity onPress={() => this.incrementCount(item.item, 'BogoList')}>
                        <View style={styles.orderCardButton}>
                          <Text style={{fontSize:18,color:"#0080FE"}}>+</Text>
                        </View>
                      </TouchableOpacity>
                    </View>
                  :
                    <TouchableOpacity style={[styles.storeBtn, styles.addtoCartBtnColor]} onPress={() => this.addCart(item.item, 'BogoList')}>
                      <Text style={{ color: '#0080FE',paddingHorizontal: 4}}>ADD</Text>
                    </TouchableOpacity>

                  }
                </View>
              </View>
            </View>
          </View>
        )
    }
  }


  ProductListing = (item) => {
    if (this.state.showList) {
      if (item.item.menu.isClickable) {
        return (
          <View style={[styles.prodcutListingView]}>
            <TouchableOpacity style={styles.addCardImage} onPress={() => navigate('productDetailsPage', {productData: item.item, shopId: this.state.shopId, shopName: this.state.shopName, category: this.state.categoryId, deliveryTime: this.state.deliveryTime})}>
              <Image style={styles.productImage} source={{uri: item.item.menu.productLogo}}/>
            </TouchableOpacity>
              <TouchableOpacity style={styles.stores} onPress={() => navigate('productDetailsPage', {productData: item.item, shopId: this.state.shopId, shopName: this.state.shopName, category: this.state.categoryId, deliveryTime: this.state.deliveryTime})}>
                <View style={{flex:10}}>
                  <Text style={s.subHeaderText} numberOfLines={3}>{item.item.menu.productName} </Text>
                </View>
                {item.item.menu.mrp != item.item.menu.unitCost
                  ?
                  <>
                  <View style={{flex:1, flexDirection:"row", marginTop: 10}}>
                    <Text style={[s.normalText]}>MRP:- </Text>
                    <Text style={[s.normalText,{textDecorationLine: 'line-through', textDecorationStyle: 'solid'}]} numberOfLines={1}>
                      ₹{item.item.menu.mrp}</Text>
                  </View>
                  <View style={{flex:1, flexDirection:"row"}}>
                    <Text style={[s.normalText]}>Flash amount:- </Text>
                    <Text style={[s.normalText]} numberOfLines={1}>₹{item.item.menu.unitCost}</Text>
                  </View>
                  </>
                  :
                  <View style={{flex:1, flexDirection:"row", marginTop: 10}}>
                    <Text style={[s.normalText]}>Flash amount:- </Text>
                    <Text style={[s.normalText]} numberOfLines={1}>₹{item.item.menu.unitCost}</Text>
                  </View>
                }
               
               
             
                <View style={styles.Mystores}>
                  <View style={{paddingTop: 10, flex: 7}}>
                    {/* <Text style={[s.normalText,{textDecorationLine: 'line-through', textDecorationStyle: 'solid'}]} numberOfLines={1}>
                      MRP:- ₹{item.item.menu.unitCost}</Text>
                    <Text style={[s.normalText]} numberOfLines={1}>Flash amount:- ₹{item.item.menu.unitCost}</Text> */}
                    <Text style={[s.normalText]} numberOfLines={3}>Unit:- {item.item.menu.unit}</Text>
                  </View>
                  <View style={styles.flexRow}>
                    <TouchableOpacity style={styles.basketBtn} onPress={() => this.showBasketModal(item.item)}>
                      <Image style={styles.basketImg} source={require('../../../assets/basketIcon.png')}/>
                    </TouchableOpacity>
                    {/* <TouchableOpacity onPress={() => this.addCart(item.item, 'ProductList')}>
                      <View style={[styles.storeBtn, styles.addtoCartBtnColor]}>
                        <Text style={{ color: '#0080FE',paddingHorizontal: 4}}>ADD</Text>
                      </View>
                    </TouchableOpacity> */}
                    {this.props.cartItem.cartItem.findIndex(ind => ind.productId === item.item.menu.productId) >= 0
                    ?
                      <View style={styles.flexRowAdd}>
                        <TouchableOpacity onPress={() => this.decrementCount(item.item, 'ProductList')}>
                          <View style={styles.orderCardButton}>
                            <Text style={{fontSize:18,color:"#0080FE"}}>-</Text>
                          </View>
                        </TouchableOpacity>
                          {/* <Text style={styles.orderCount}>{item.item.counter}</Text> */}
                          <Text style={styles.orderCount}>{this.props.cartItem.cartItem[this.props.cartItem.cartItem.findIndex(ind => ind.productId === item.item.menu.productId)].counter}</Text>
                        <TouchableOpacity onPress={() => this.incrementCount(item.item, 'ProductList')}>
                          <View style={styles.orderCardButton}>
                            <Text style={{fontSize:18,color:"#0080FE"}}>+</Text>
                          </View>
                        </TouchableOpacity>
                      </View>
                    :
                      <TouchableOpacity style={[styles.storeBtn, styles.addtoCartBtnColor]} onPress={() => this.addCart(item.item, 'ProductList')}>
                        <Text style={{ color: '#0080FE',paddingHorizontal: 4}}>ADD</Text>
                      </TouchableOpacity>

                    }
   
                  </View>
                </View>
              </TouchableOpacity>
             
          </View>
        )
      } else {
        return (
          <View style={[styles.prodcutListingView]}>
            <View style={styles.addCardImage}>
              <Image style={styles.productImage} source={{uri: item.item.menu.productLogo}}/>
            </View>
              <View style={styles.stores}>
                <View style={{flex:10}}>
                  <Text style={s.subHeaderText} numberOfLines={3}>{item.item.menu.productName} </Text>
                </View>
                {item.item.menu.mrp != item.item.menu.unitCost
                  ?
                  <>
                  <View style={{flex:1, flexDirection:"row", marginTop: 10}}>
                    <Text style={[s.normalText]}>MRP:- </Text>
                    <Text style={[s.normalText,{textDecorationLine: 'line-through', textDecorationStyle: 'solid'}]} numberOfLines={1}>
                      ₹{item.item.menu.mrp}</Text>
                  </View>
                  <View style={{flex:1, flexDirection:"row"}}>
                    <Text style={[s.normalText]}>Flash amount:- </Text>
                    <Text style={[s.normalText]} numberOfLines={1}>₹{item.item.menu.unitCost}</Text>
                  </View>
                  </>
                  :
                  <View style={{flex:1, flexDirection:"row", marginTop: 10}}>
                    <Text style={[s.normalText]}>Flash amount:- </Text>
                    <Text style={[s.normalText]} numberOfLines={1}>₹{item.item.menu.unitCost}</Text>
                  </View>
                }
               
               
             
                <View style={styles.Mystores}>
                  <View style={{paddingTop: 10, flex: 7}}>
                    {/* <Text style={[s.normalText,{textDecorationLine: 'line-through', textDecorationStyle: 'solid'}]} numberOfLines={1}>
                      MRP:- ₹{item.item.menu.unitCost}</Text>
                    <Text style={[s.normalText]} numberOfLines={1}>Flash amount:- ₹{item.item.menu.unitCost}</Text> */}
                    <Text style={[s.normalText]} numberOfLines={3}>Unit:- {item.item.menu.unit}</Text>
                  </View>
                  <View style={styles.flexRow}>
                    <TouchableOpacity style={styles.basketBtn} onPress={() => this.showBasketModal(item.item)}>
                      <Image style={styles.basketImg} source={require('../../../assets/basketIcon.png')}/>
                    </TouchableOpacity>
                    {/* <TouchableOpacity onPress={() => this.addCart(item.item, 'ProductList')}>
                      <View style={[styles.storeBtn, styles.addtoCartBtnColor]}>
                        <Text style={{ color: '#0080FE',paddingHorizontal: 4}}>ADD</Text>
                      </View>
                    </TouchableOpacity> */}
                    {this.props.cartItem.cartItem.findIndex(ind => ind.productId === item.item.menu.productId) >= 0
                    ?
                      <View style={styles.flexRowAdd}>
                        <TouchableOpacity onPress={() => this.decrementCount(item.item, 'ProductList')}>
                          <View style={styles.orderCardButton}>
                            <Text style={{fontSize:18,color:"#0080FE"}}>-</Text>
                          </View>
                        </TouchableOpacity>
                          {/* <Text style={styles.orderCount}>{item.item.counter}</Text> */}
                          <Text style={styles.orderCount}>{this.props.cartItem.cartItem[this.props.cartItem.cartItem.findIndex(ind => ind.productId === item.item.menu.productId)].counter}</Text>
                        <TouchableOpacity onPress={() => this.incrementCount(item.item, 'ProductList')}>
                          <View style={styles.orderCardButton}>
                            <Text style={{fontSize:18,color:"#0080FE"}}>+</Text>
                          </View>
                        </TouchableOpacity>
                      </View>
                    :
                      <TouchableOpacity style={[styles.storeBtn, styles.addtoCartBtnColor]} onPress={() => this.addCart(item.item, 'ProductList')}>
                        <Text style={{ color: '#0080FE',paddingHorizontal: 4}}>ADD</Text>
                      </TouchableOpacity>

                    }
   
                  </View>
                </View>
              </View>
             
          </View>
        )
      }
     
    } else {
      return null;
    }
    // if(this.state.isFilter){
    //   // const imgPath = item.item.menu.productLogo + item.item.menu.productName.replace(/\s+/g, '') + '.jpg';
     
     
    // }
    // else{
      // const imgPath = item.item.productLogo + item.item.productName.replace(/\s+/g, '') + '.jpg';
      // return (
     
      //   <View style={[styles.prodcutListingView]}>
      //     <View style={styles.addCardImage}>
      //       <Image style={styles.productImage} source={{uri: item.item.productLogo}}/>
      //     </View>
      //       <View style={styles.stores}>
      //         <View style={{flex:10}}>
      //           <Text style={s.subHeaderText} numberOfLines={3}>{item.item.productName} </Text>
      //         </View>
      //         <View style={{flex:1, flexDirection:"row", marginTop: 10}}>
      //           <Text style={[s.normalText]}>MRP:- </Text>
      //           <Text style={[s.normalText,{textDecorationLine: 'line-through', textDecorationStyle: 'solid'}]} numberOfLines={1}>
      //             ₹{item.item.mrp}</Text>
      //         </View>
      //         <View style={{flex:1, flexDirection:"row"}}>
      //           <Text style={[s.normalText]}>Flash amount:- </Text>
      //           <Text style={[s.normalText]} numberOfLines={1}>₹{item.item.unitCost}</Text>
      //         </View>
             
      //         <View style={styles.Mystores}>
      //           <View style={{paddingTop: 10, flex: 7}}>
      //             {/* <Text style={[s.normalText,{textDecorationLine: 'line-through', textDecorationStyle: 'solid'}]} numberOfLines={1}>
      //             MRP:- ₹{item.item.unitCost}</Text>
      //             <Text style={[s.normalText]} numberOfLines={1}>Flash amount:- ₹{item.item.unitCost}</Text> */}
      //             <Text style={[s.normalText]} numberOfLines={3}>Unit:- {item.item.unit}</Text>
      //           </View>
      //           <View style={styles.flexRow}>
      //             <TouchableOpacity style={styles.basketBtn} onPress={() => this.showBasketModal(item.item)}>
      //               <Image style={styles.basketImg} source={require('../../../assets/basketIcon.png')}/>
      //             </TouchableOpacity>
      //             <TouchableOpacity onPress={() => this.addCart(item.item)}>
      //               <View style={[styles.storeBtn, styles.addtoCartBtnColor]}>
      //                 <Text style={{ color: '#0080FE',paddingHorizontal: 4}}>ADD</Text>
      //               </View>
      //             </TouchableOpacity>
 
      //           </View>
      //         </View>
      //       </View>
      //   </View>
      // )
         
    // }
 
 
  }

  async componentDidMount(){
   
    if (this.props.cartState.cartState) {
      this.setState({paddingBottomHeight: 100});
    } else {
      this.setState({paddingBottomHeight: 0});
    }

    this.props.navigation.addListener('willFocus', () =>{
      if (this.props.cartState.cartState) {
        this.setState({paddingBottomHeight: 100});
      } else {
        this.setState({paddingBottomHeight: 0});
      }
    });

    await this.setState({showLoader: true});
    let reqBody;
    if (this.props.navigation.state.params) {
      if (this.props.navigation.state.params.shopId) {
        reqBody = {
          "shopId": this.props.navigation.state.params.shopId,
        };
        await this.setState({shopId: this.props.navigation.state.params.shopId});
      } else {
        reqBody = {
          "isFlash": true
        }
      }
    } else {
      reqBody = {
        "isFlash": true
      }
    }
    this.getShopDetails(reqBody);
  }

  getData = async(reqBody) => {
    const getMenuCategories = await callAPI.axiosCall(Config.getShopMenuCategories, reqBody);
    // console.warn('in getData shopBogoMenu ====>', getMenuCategories.data.shopBogoMenu);
    // console.warn('in getData shopBogoMenu ====>', getMenuCategories.data.shopBogoMenu);
    
    if (getMenuCategories) {
      if (getMenuCategories.data.status == 0) {
        if (getMenuCategories.data.shopBogoMenu && getMenuCategories.data.shopBogoMenu.length > 0) {
          this.setState({ bogoMenu: getMenuCategories.data.shopBogoMenu })
        }
        getMenuCategories.data.result = getMenuCategories.data.result.map( item => {
          if (item._id == 'All') {
            item.backGroundColor = "#00CCFF";
          } else {
            item.backGroundColor = "white";
          }
          return item;
        });

        this.setState({distinctCategories: getMenuCategories.data.result});

        if(getMenuCategories.data.result.length <= 0) {
          // const reqBody = {categoryId: this.state.categoryId}
          const reqBody = {shopId: this.state.shopId}
          this.getShopMenu(reqBody);
        } else {
          let subCategoryBody = {
            shopId: this.state.shopId,
            subCategoryId: this.state.subCategoryId
          }
          this.getSubCategories(subCategoryBody);
        }
      } else if(getMenuCategories.data.status == 1){  // Fail to signup
        this.setState({showLoader: false});
        this.showToaster(getMenuCategories.data.message);
      }
    } else {
      this.setState({showLoader: false});
      this.showToaster('Currently this shop is unavailable for any kind of orders. Please try later');
    }

  }

  showToaster = (message) => {
    setTimeout(() => {
        Toast.show(message, Toast.LONG);
    }, 100);
  }

  getShopDetails = async(body) => {
    const getShopData = await callAPI.axiosCall(Config.getCategoriesByShop, body);
    console.warn('shop data===========--->', getShopData);
    if (getShopData) {
      await this.setState({
        shopData: getShopData.data.result,
        shopId: getShopData.data.result[0].shopId,
        shopName: getShopData.data.result[0].shopName,
        shopImage: getShopData.data.result[0].shopLogo,
        shopDescription: getShopData.data.result[0].shopDescription,
        shopAddress: getShopData.data.result[0].shopAdress,
        shopRating: getShopData.data.result[0].shopRating,  
        categoryId: getShopData.data.result[0].categoryId,
        deliveryTime: getShopData.data.result[0].deliveryTime,
        headTitleFirst: getShopData.data.result[0].shopMenuTitle1,
        headTitleSecond: getShopData.data.result[0].shopMenuTitle2,
      });
      const reqBody = {
        "shopId": getShopData.data.result[0].shopId
      };
      this.getData(reqBody);
      const getBestSelling = await callAPI.axiosCall(Config.getBestSellingProduct, { shopId: getShopData.data.result[0].shopId });
      // console.warn('Best selling response111===>', getBestSelling);
      if (getBestSelling) {
        if (getBestSelling.data.status == 0) {
          // console.warn('Best selling response===>', getBestSelling.data.result );
          this.setState({ bestSellerProducts: getBestSelling.data.result });
        }
      }
    } else {
      this.setState({showLoader: false});
      Toast.show('Sorry, currently this shop is not available',Toast.LONG);
      this.props.navigationAction.navigateAction('Home');
      const resetAction = StackActions.reset({
        index: 0,
        actions: [NavigationActions.navigate({ routeName: 'homePage'})]
      });
      this.props.navigation.dispatch(resetAction);
    }
   
  }

  // handleLoadMore = () => {
  //     console.warn('in handle more---------------- inside cond', this.onEndReachedCalledDuringMomentum);
  //     // if (!this.onEndReachedCalledDuringMomentum) {
  //       this.onEndReachedCalledDuringMomentum = true;
  //       this.setState(
  //         (prevState, nextProps) => ({
  //           page: prevState.page + 1,
  //           loading: true,
  //           loadData: false,
           
  //         }),
  //         () => {
  //           let reqBody = {
  //             categoryId: this.state.categoryId,
  //             productCategory: this.state.productCategory,
  //             searchData: this.state.searchText,
  //             pageNumber: this.state.page
  //           }
  //           this.getShopMenu(reqBody);
  //         }
  //       );
  //     // }
     
  // };


  getShopMenu = async(reqBody) => {
    // console.warn('request body--->', reqBody);
    const getShopMenu = await callAPI.axiosCall(Config.getShopMenu, reqBody);
    this.setState({showLoader: false});
    if (getShopMenu) {
      if (res.data.status == 0) {
        console.warn('data---', res);
        this.setState({
          shopMenu: res.data.result,
          showList: true,
        })
       
        if (this.state.shopMenu.length <= 0) {
          this.setState({productNotAvailable: true})
          Toast.show('Sorry!!! Searched product is not available in the list',Toast.LONG);
        } else {
          this.setState ({
            // page: this.state.page,
            // maxCount: res.data.totalRecord,
            // pageCount: res.data.limit,
            // totalPage: res.data.totalPage,
            productNotAvailable: false
          })
        }
        // console.warn('this.state.page', this.state.page, res.data.totalRecord, res.data.totalPage);
       
        // if (res.data.totalPage == 1) {
        //   this.setState({prev: true, next: true, prevColor: "silver", nextColor: "silver"})
        // } else if (this.state.page == 1) {
        //   this.setState({prev: true, next: false, prevColor: "silver", nextColor: "#00CCFF"})
        // } else if(this.state.page == res.data.totalPage) {
        //   this.setState({prev: false, next: true, prevColor: "#00CCFF", nextColor: "#00CCFF"})
        // } else {
        //   this.setState({prev: false, next: false, prevColor: "#00CCFF", nextColor: "#00CCFF"})
        // }
      } else if(res.data.status == 1){  // Fail to signup
        // console.warn('response get menu-->', res.data);
        Toast.show(res.data.message,Toast.LONG);
      }
    }
  }

  backAndroid = () => {
    return false;
  }

  hideModal = () => {
    this.setState({
      showBasketModal: false,
      modalVisible: false,
      cartRemoveModal: false,
    })
  }

  selectProductCategory = (id) => {
    let index = this.state.distinctCategories.findIndex(ind => ind._id === id);
    // this.setState({page: 1, searchText: ""});
    this.setState({searchText: ""});
   
    let reqBody;
    if (this.state.distinctCategories[index]._id == 'All') {
      reqBody = {
        shopId: this.state.shopId,
        productCategory: "",
      }
      this.state.distinctCategories.forEach(element => {
        element.backGroundColor = 'white';
      });  
      this.state.distinctCategories[index].backGroundColor = '#00CCFF';
      this.setState({distinctCategories: this.state.distinctCategories});  
    } else {
      if(this.state.distinctCategories[index].backGroundColor == '#00CCFF') {
        this.state.distinctCategories.forEach(element => {
          element.backGroundColor = 'white';
        });  
        this.setState({
          selectedCategory: ""
        })
        reqBody = {
          shopId: this.state.shopId,
          productCategory: "",
        }    
      } else {
        this.state.distinctCategories.forEach(element => {
          element.backGroundColor = 'white';
        });
        this.setState({
          selectedCategory: this.state.distinctCategories[index]._id
        })
        this.state.distinctCategories[index].backGroundColor = '#00CCFF';
        reqBody = {
          shopId: this.state.shopId,
          productCategory: this.state.distinctCategories[index]._id,
        }    
      }

      this.setState({
        distinctCategories: this.state.distinctCategories
      })
    }
   
   
    this.setState({showLoader: true});
    this.getSubCategories(reqBody);

   
  }

  CategoryFilter = (item) => {
    return (
      <TouchableOpacity style={{borderWidth: 1, borderColor:'#131E3A', padding: 10, marginRight: 5, backgroundColor:item.item.backGroundColor}}
        onPress={() => this.selectProductCategory(item.item._id)}>
        <Text style={[s.normalText]}>{item.item._id}</Text>
      </TouchableOpacity>
    )

  }

 

  render() {
    const {navigate} = this.props.navigation;
      return (
        <SafeAreaView style={{flex: 1}}>
        <View style={{flex: 1}}>
          <View style={[s.body,{flex: 1}]}>
            <AnimatedLoader
              visible={this.state.showLoader}
              overlayColor="rgba(255,255,255,0.75)"
              source={require("../../../assets/loader.json")}
              animationStyle= {[styles.lottie,{height: 150,width: 150}]}
              speed={1}
            />
            {/* <InternateModal /> */}
            <BasketModal visible={this.state.showBasketModal} onDismiss={this.hideModal} navigation= {navigate} basketData= {this.state.addtoBasketItem}
              shopId={this.state.shopId} shopName={this.state.shopName} categoryId={this.state.categoryId} onRequestClose={() => { this.hideModal()}} />  
             
            {/* <ScrollView showsVerticalScrollIndicator={false} > */}
            <ScrollView showsVerticalScrollIndicator={false}>
              <View style={{flex: 6}}>
                {this.state.shopImage != ''
                ?
                  <View style={styles.advertiseData}>
                    <Image style={styles.imageView} source={{uri: this.state.shopImage}}/>
                  </View>
                :
                <View>
                  <View style={styles.advertiseData}>
                    <Image style={styles.imageView} source={require('../../../assets/flashhubshop.jpg')}/>
                  </View>
                </View>
                }
               
                <View style={styles.storeCardLayout}>
                  <Text style={[s.headerText,styles.storeHeading]} >{this.state.shopName}</Text>
                  <Text style={styles.storeDesc} numberOfLines = { 2 }>{this.state.shopDescription}</Text>
                  <Text style={styles.storeDesc} numberOfLines = { 1 }>{this.state.shopAddress}</Text>
                  {/* <View style={{alignItems:"center"}}> */}
                  {/* </View> */}
                </View>
                <View style={[styles.borderLine, {flex:1, flexDirection:"row"}]}>
                  <View style={{flex: 8, alignItems:"flex-end", marginRight: 15}}>
                    <Rating imageSize={20} readonly startingValue={this.state.shopRating} />
                  </View>
                  <View style={{flex:4, marginBottom: 10}}>
                    <Text style={[s.normalText, {textAlign:"right", color:"#00CCFF", paddingRight: 10}]} numberOfLines = { 2 }>{this.state.deliveryTime == '24' ? '24 Hours away' : '55 Mins away'}</Text>  
                  </View>

                 
                </View>

                {this.state.showList
                ?
                <View style={styles.sectionContainer}>
                  {/* <TextInput placeholder = "Search product...."
                    value = {this.state.searchText}
                    style={styles.searchShops} inlineImageLeft='searchicon'
                    onChangeText={(searchText) => this.searchData(searchText)}/> */}
                    <DelayInput placeholder = "Search product...."
                      delayTimeout={500}
                      minLength={3}
                      value = {this.state.searchText}
                      style={styles.searchShops} inlineImageLeft='searchicon'
                      onChangeText={(searchText) => this.searchData(searchText)} />
                </View>
                :
                <View style={styles.sectionContainer}>
                  {/* <TextInput placeholder = "Search product...."
                    value = {this.state.searchText}
                    style={styles.searchShops} inlineImageLeft='searchicon'
                    onChangeText={(searchText) => this.searchAllProduct(searchText)}/> */}
                    <DelayInput placeholder = "Search product...."
                      delayTimeout={500}
                      minLength={3}
                      value = {this.state.searchText}
                      style={styles.searchShops} inlineImageLeft='searchicon'
                      onChangeText={(searchText) => this.searchAllProduct(searchText)} />
                </View>

                }
               
                {this.state.showList && this.state.subCategoryList.length > 0 && !this.state.productNotAvailable
                ?
                <TouchableOpacity onPress={() => this.setState({showList: false})}>
                  <Text style={[s.normalText,{color: '#00CCFF', paddingTop: 10, paddingHorizontal: 15} ]}>Back to subcategories</Text>
                </TouchableOpacity>
                : null
                }
                {this.state.productNotAvailable
                ?
                <View style={{flex: 1, alignItems:"center", justifyContent:"center"}}onPress={() => this.setState({showList: false})}>
                  <Text style={[s.normalText,{paddingHorizontal: 15, paddingTop: 40, textAlign:"center"} ]} numberOfLines={3}>Sorry!!! Searched product is not available in the shop</Text>
                  <TouchableOpacity style={{paddingVertical: 15}} onPress={() => this.searchData('')}>
                      <Text style={[s.normalText, {color: "#00CCFF"}]}>Back</Text>
                  </TouchableOpacity>
                </View>
                : null
                }
                {this.state.bogoMenu.length > 0
                ?
                  <View style={{paddingHorizontal: 15, marginTop: 15}}>
                    <Text style={s.subHeaderText}>{this.state.headTitleFirst}</Text>
                    <View style={{marginVertical: 15}}>
                    <FlatList
                      data={this.state.bogoMenu}
                      renderItem={( { item, index } ) => (
                      <this.BogoProductList
                        item= {item}
                      />
                      )}
                      keyExtractor= {item => item.menu.productId}
                      extraData={this.state}
                      horizontal={true}
                      showsHorizontalScrollIndicator = {false}
                    />
                    </View>
                  </View>
                :  
                null

                }
                {!this.state.showList && this.state.distinctCategories.length > 1 && !this.state.productNotAvailable ?
                  <View style={{elevation: 4,paddingHorizontal: 15, marginVertical: 10}}>
                    <Text style={s.subHeaderText}>Shop By Categories</Text>
                    <ScrollView style={{marginVertical: 15, marginLeft: 10}}>
                      <FlatList data={this.state.distinctCategories}
                        renderItem={( { item } ) => (
                          <this.CategoryFilter
                            item={item}
                          />
                        )}
                        keyExtractor= {item => item._id}
                        extraData={this.state}
                        horizontal={true}
                        showsHorizontalScrollIndicator = {false}
                      />
                    </ScrollView>
                  </View>
               
                : null
                }
              </View>
             

              {!this.state.showList && !this.state.productNotAvailable ?
               <View style={[styles.storeDtlContainer,{flex: 6}]} >
                  <FlatList
                    data={this.state.subCategoryList}
                    renderItem={( { item, index } ) => (
                      <SubCategoryListView
                        item= {item}
                        self={this}
                      />
                    )}
                    keyExtractor= {item => item._id}
                    extraData={this.state}
                    numColumns={3}
                    // onEndReached={this.handleLoadMore}
                    // onEndReachedThreshold={0.01}
                    // initialNumToRender={30}
                    // onMomentumScrollBegin={this.moment}
                    // onMomentumScrollEnd ={this.handleLoadMore}
                  />
                 
                </View>
              : null
              }
             
              {this.state.bestSellerProducts.length > 0
              ?
                <View style={{paddingBottom: this.state.paddingBottomHeight, marginTop: 15, paddingHorizontal: 15}}>
                  <Text style={s.subHeaderText}>{this.state.headTitleSecond}</Text>
                  <View style={{marginVertical: 15}}>
                  <FlatList
                    data={this.state.bestSellerProducts}
                    renderItem={( { item, index } ) => (
                    <this.BestSellingProducts
                      item= {item}
                    />
                    )}
                    // keyExtractor= {item => item.productId}
                    extraData={this.state}
                    horizontal={true}
                    showsHorizontalScrollIndicator = {false}
                  />
                  </View>
                </View>
              :  
              <View style={{height: this.state.paddingBottomHeight}}></View>

              }
           
             
                {/* bottom part */}
                {this.state.showList ?
                <View style={[styles.storeDtlContainer,{flex: 6, paddingBottom: this.state.paddingBottomHeight}]} >
                  <FlatList
                    data={this.state.shopMenu}
                    renderItem={( { item, index } ) => (
                      <this.ProductListing
                      item= {item}
                      />
                    )}
                    keyExtractor= {item => item.menu.productId}
                    extraData={this.state}
                    // onEndReached={this.handleLoadMore}
                    // onEndReachedThreshold={0.01}
                    // initialNumToRender={30}
                    // onMomentumScrollBegin={this.moment}
                    // onMomentumScrollEnd ={this.handleLoadMore}
                  />
                 
                </View>
                : null
                }
            </ScrollView>
            <PopupModal visible={this.state.cartRemoveModal} onDismiss={this.hideModal} removeProduct={this.removeProduct}
                message={"Are you sure want to remove the product from cart?"} btnTag={"Remove"}/>
            <CartModal navigate={navigate} navigationPage={'orderDetailsPage'}></CartModal>
            {/* <OnGoingActivityModal navigate={navigate}></OnGoingActivityModal> */}
            <PopupModal visible={this.state.modalVisible} onDismiss={this.hideModal} removeProduct={this.userAckCart}
              message={this.state.cartModalMessage}
              btnTag={"Add to cart"}/>  
            {/* <NavigationBar navigate={navigate}></NavigationBar>        */}
          </View>
         
        </View>
         
        </SafeAreaView>
     
    )
   
  }

  getItemLayout = (data, index) => (
    { length: 29, offset: 29 * index, index }
  )

  handlePagination = (type) => {
    this.setState({showLoader: true, showList: false});
    if(type == 'next') {
      const reqBody = {
        shopId: this.state.shopId,
        productCategory: this.state.selectedCategory,
        searchData: this.state.searchText,
      }

      this.setState({page: this.state.page + 1});
      this.getShopMenu(reqBody);
    } else {
      const reqBody = {
        shopId: this.state.shopId,
        productCategory: this.state.selectedCategory,
        searchData: this.state.searchText,
      }
      this.setState({page: this.state.page - 1});
      this.getShopMenu(reqBody);
    }
   
   
  }

  incrementCount = async(productData, type) => {
    this.setState({
      selectedProduct: productData,
      selectedProductType: type
    });
    let index = this.props.cartItem.cartItem.findIndex(ind => ind.productId === productData.menu.productId);
    this.props.cartState.cartCount += 1;
    this.props.cartState.cartSum = parseFloat(this.props.cartState.cartSum) + parseFloat(this.props.cartItem.cartItem[index].unitCost);
    this.props.cartModalAction.changeCartState(true,this.props.cartState.cartSum,this.props.cartState.cartCount);
    this.props.cartItem.cartItem[index].counter = this.props.cartItem.cartItem[index].counter + 1;
    this.props.cartItem.cartItem[index].totalCost = this.props.cartItem.cartItem[index].unitCost * this.props.cartItem.cartItem[index].counter;
    this.props.actions.replaceCart(this.props.cartItem.cartItem);
    // await this.setState({ bestSellerProducts: this.state.bestSellerProducts});
    this.manageDisplayData();
  }

  decrementCount = async(productData, type) => {
    this.setState({
      selectedProduct: productData,
      selectedProductType: type
    });
    let id = this.props.cartItem.cartItem.findIndex(ind => ind.productId === productData.menu.productId);
   if (id >= 0) {
    if(this.props.cartItem.cartItem[id].counter - 1 <= 0){
      this.setState({
        cartRemoveModal: true
      });
    } else {
      this.props.cartItem.cartItem[id].counter = this.props.cartItem.cartItem[id].counter - 1;
      this.props.cartItem.cartItem[id].totalCost = this.props.cartItem.cartItem[id].unitCost * this.props.cartItem.cartItem[id].counter;
      this.props.cartState.cartCount -= 1;
      this.props.cartState.cartSum = parseFloat(this.props.cartState.cartSum) - parseFloat(this.props.cartItem.cartItem[id].unitCost);
      this.props.cartModalAction.changeCartState(true,this.props.cartState.cartSum,this.props.cartState.cartCount);
      this.props.actions.replaceCart(this.props.cartItem.cartItem);
      // await this.setState({ bestSellerProducts: this.state.bestSellerProducts});
      this.manageDisplayData();
    }
   }
  }

  removeProduct = async() => {

    this.setState({
      cartRemoveModal: false,
      removeProduct: true,
      modalVisible: false,
      paddingBottomHeight: 0
    });
    const i = this.props.cartItem.cartItem.findIndex(ind => ind.productId === this.state.selectedProduct.menu.productId);
   
    this.props.cartState.cartCount -= 1;
    this.props.cartState.cartSum = parseFloat(this.props.cartState.cartSum) - parseFloat(this.props.cartItem.cartItem[i].unitCost);
   
    this.props.cartItem.cartItem.splice(i,1);
    if(this.props.cartState.cartCount === 0){
      this.props.cartModalAction.changeCartState(false,this.props.cartState.cartSum,this.props.cartState.cartCount);
    } else {
      this.props.cartModalAction.changeCartState(true,this.props.cartState.cartSum,this.props.cartState.cartCount);
    }
    this.props.actions.replaceCart(this.props.cartItem.cartItem);
    // await this.setState({ bestSellerProducts: this.state.bestSellerProducts});
    this.manageDisplayData();
  }

  //function to add the new item in the cart   --- Start
  addCart = async(productData, type) => {
    console.warn('add cart data ====>', type);
    await this.setState({ selectedProductType: type, selectedProduct: productData });
    const cart = {
      productId: productData.menu.productId,
      productName: productData.menu.productName,
      productLogo: productData.menu.productLogo,
      productDescription: productData.menu.productDescription,
      unitCost: parseFloat(productData.menu.unitCost),
      unit: productData.menu.unit,
      shopId: this.state.shopId,
      shopName: this.state.shopName,
      category: this.state.categoryId,
      counter: 1,
      totalCost: parseFloat(productData.menu.unitCost),  
      isProductAvailable: true,
      cgst: parseFloat(productData.menu.cgst),
      sgst: parseFloat(productData.menu.sgst),
      costExlGst: parseFloat(productData.menu.unitCostExclTax),
      shopRate: parseFloat(productData.menu.shopRate),
      shopAddress: productData.shopDetails.shopAdress,
      shopContactNumber: productData.shopDetails.shopContactNumber,
      shopEmail: productData.shopDetails.shopEmail,
      shopLocation: productData.shopDetails.shopLocation,
      shopOwnerName: productData.shopDetails.shopOwnerName,
      deliveryTime: this.state.deliveryTime,
      isBogo: type == 'BogoList' ? true : false
    }

    this.setState({
      cart: cart
    })
   
    // console.warn('testtttt-->', this.props.cartItem.cartItem.some(arr => arr.shopId == this.state.shopId));

    if(this.props.cartItem.cartItem.some(arr => arr.productId == productData.menu.productId)){
      Toast.show("Already added in the cart",Toast.LONG);
    } else if (!this.props.cartItem.cartItem.some(arr => arr.shopId == this.state.shopId)) {
      if (this.props.cartItem.cartItem.length > 0) {
        // console.warn('check status==>', this.props.cartItem.cartItem.some(arr => arr.deliveryTime == this.state.deliveryTime));
        if (!this.props.cartItem.cartItem.some(arr => arr.deliveryTime == this.state.deliveryTime)) {
          this.setState({
            modalVisible: true,
            cartModalMessage: 'Delivery time and delivery charges of your order added in cart depends upon the delivery time of the individual shop and number of shop you shopped, if ordered time crosses the delivery time of any shop(s) then the whole order will be delivered tomorrow or next day.'
          })
        } else {
          this.setState({
            modalVisible: true,
            cartModalMessage: 'Delivery charges may vary as per the changes in different shop locations. Would you like to continue with your purchase?'
          })
        }
         
      } else {
        this.confirmAddCart(cart);
      }
    } else{
      this.confirmAddCart(cart);
    }
  }

  confirmAddCart = async(cart) => {
    this.props.cartState.cartCount += 1;
    this.props.cartState.cartSum = (parseFloat(this.props.cartState.cartSum) + parseFloat(cart.unitCost));
    this.props.cartModalAction.changeCartState(true,this.props.cartState.cartSum,this.props.cartState.cartCount);
    this.props.actions.addToCart(cart);
    this.setState({paddingBottomHeight: 100});
    Toast.show(cart.productName + " is added in the cart",Toast.LONG);
    // await this.setState({ bestSellerProducts: this.state.bestSellerProducts});
    this.manageDisplayData();
  }

  manageDisplayData = async() => {
    // console.warn('in manage disaplay data===>', this.state.selectedProductType);
    // console.warn('this.state.selectedProduct.menu.productId', this.state.selectedProduct.menu.productId);
    switch (this.state.selectedProductType) {
      case 'BestSelling' :
          await this.setState({ bestSellerProducts: this.state.bestSellerProducts});
          break;
      case 'ProductList' :
          await this.setState({ shopMenu: this.state.shopMenu});
          break;
      case 'BogoList' :
          await this.setState({ bogoMenu: this.state.bogoMenu});
          break;
    }
  }

  userAckCart = () => {
    this.setState({
      modalVisible: false
    })
    this.confirmAddCart(this.state.cart);

}


  //function to add the new item in the cart   --- End

 
  //function to add the new item in the basket   --- Start
  showBasketModal = (basketData) => {
    this.setState({
      showBasketModal: true,
      addtoBasketItem: basketData
    })
  }
  //function to add the new item in the basket   --- End

  searchAllProduct = (text) => {
    // console.warn('text--->', text);
    if (text != '') {
      this.props.navigation.navigate('productListPage',{"categoryId": this.state.categoryId, "searchText": text, "shopId": this.state.shopId,
      "productSubCategory": '', "productCategory": '', "shopName": this.state.shopName, "deliveryTime": this.state.deliveryTime});
    }
   
   
  }
   //function to search product or shop   --- Start
   searchData = (searchText) => {
    //  console.warn('search test in search data',);
    this.setState({searchText: searchText});

    if(searchText == ''){
      var searchObj = {
        "searchData": '',
        "shopId": this.state.shopId,
        "productCategory": this.state.selectedCategory,
        "productSubCategory": this.state.selectedSubCategory,
      }
      this.getShopMenu(searchObj);
    } else if (searchText.length > 2) {
      var searchObj = {
        "searchData": searchText,
        "shopId": this.state.shopId,
        "productCategory": this.state.selectedCategory,
        "productSubCategory": this.state.selectedSubCategory,
      }
      console.warn('test--', searchObj);
      this.getShopMenu(searchObj);
    }
   

    // axios.post(
    //     Config.URL + Config.getShopMenu,
    //     searchObj,
    //     {
    //       headers: {
    //         'Content-Type': 'application/json',
    //       }
    //     }
    // )
    // .then(res => {
       
    //     if (res.data.status == 0) {
    //       console.warn("res-----123",res.data.result);
    //       this.setState({
    //         shopMenu: res.data.result,
    //         searchText: searchText
    //       });
    //     }
    //     else if(res.data.status == 1){  // Fail to signup
    //       Toast.show(res.data.message,Toast.LONG);
    //     }
 
    //   }).catch(err => {
    //       console.warn('err--->', err);
    //   })
 
  }
  //function to search product or shop   --- End


}


let ScreenWidth = Dimensions.get("window").width;
let ScreenHeight = Dimensions.get("window").height;

const styles = StyleSheet.create({
  scrollView: {
    flex: 1
  },
  imgBlank: {
    height: ScreenHeight,
    width: ScreenWidth
  },
  containerIcons:{
    width:30,
    height:30,
  },
  flexRowAdd: {
    paddingTop: 10,
    paddingLeft: 15,
    textAlignVertical: "bottom",
    flexDirection: 'row',
  },
  orderCount:{
    fontSize:18,
    fontWeight:'bold',
    paddingHorizontal:10,
    color: '#131E3A'
  },
  searchShops:{
    height: 40,
    borderColor: '#7285A5',
    borderWidth: 1,
    paddingHorizontal:10,
    borderRadius:4,
    backgroundColor: "#f1f1f1"
  },
  sectionContainer: {
    marginTop: 20,
    paddingHorizontal: 24,
  },
  iconView: {
    width: 30,
    height: 30,
    marginRight: 20,
    borderRadius: 100,
  },
  storeCardLayout:{
    marginHorizontal: 15,
    paddingVertical:14,
    alignItems: "center"
  },
  borderLine: {
    borderBottomWidth: 2,
    borderBottomColor: "#131E3A",
    marginHorizontal: 15,
  },
  categoryFooter:{
    width:ScreenWidth,
    paddingBottom:20,
    height:80,
    flexGrow:0.05,
    backgroundColor:'white',
    display:'flex',
    flexDirection:'row',
    justifyContent:"space-evenly",
    alignItems:"center"
  },
  storeDtlContainer: {
    // marginHorizontal: 15,
    paddingVertical: 10,
    // flex: 4,
  },

  storeContainer:{
    display:"flex",
    flexDirection: "row"
  },
  stores: {
    paddingHorizontal: 10,
    flex: 9,
    flexDirection: "column"
  },

  storeCount: {
    fontSize: 22,
    fontWeight: 'bold',
    paddingHorizontal: 10,
  },
  storeProductHeading: {
    fontWeight: 'bold',
    fontSize: 30,
  },
  shopdtlCards: {
    width: 80,
    height: 80,
    backgroundColor: 'orange',
    borderRadius: 100,
    borderWidth: 0.5,
    borderColor: 'black',
    marginVertical: 5,
    marginHorizontal: 10,
  },
  storeCardDisplay: {
    // padding: 10,
    paddingVertical: 10,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  storeCards: {
    height: 100,
    borderRadius: 4,
    borderWidth: 0.5,
    borderColor: 'black',
    backgroundColor: 'white',
    flex: 3,
  },
  advertiseData: {
    height: 180,
    width: ScreenWidth,
    elevation: 4
  },
  advertiseShop: {
    height: 200,
    width: ScreenWidth,
    elevation: 4,
    borderBottomLeftRadius: 60,
    borderBottomRightRadius: 60,
  },
  imageView: {
    flex: 1,
    width: undefined,
    height: undefined,
    resizeMode: 'cover',
    borderBottomLeftRadius: 60,
    borderBottomRightRadius: 60,
  },
  storeHeading: {
    fontSize:24,
   
  },
  storeDesc: {
    alignItems: "center"
  },
  trendingShops: {
    borderBottomWidth: 2,
    borderColor: "#131E3A",
    marginHorizontal: 15,
    paddingTop: 10,
  },
  addCardImage: {
    width: 80,
    height: 80,
    flex: 3
  },
  cardImage: {
    // marginRight: 10,
    width: (ScreenWidth / 2) - 40,
    height: 80,
  },
  cardImageView: {
    flex: 1,
    width: undefined,
    height: undefined,
    resizeMode: 'contain',
  },
  productImage: {
    width: 80,
    height: 80,
  },
  storeCardButton: {
    backgroundColor: 'white',
    color: 'black',
    width: 30,
    height: 30,
    borderRadius: 50,
    alignItems: "center",
    justifyContent: 'center',
  },
  storeborder: {
    borderBottomColor: 'grey',
    borderBottomWidth: 1,
  },
  flexRow: {
    flex: 5,
    textAlignVertical: "bottom",
    flexDirection: 'row',
    alignItems:"flex-end",
    paddingRight: 10
  },
  prodcutListingView: {
    flexDirection: 'row',
    marginVertical: 5,
    backgroundColor: "white",
    padding: 10,
    marginHorizontal: 10,
    elevation: 4

  },
  flexColumnAlignEnd: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-end'
  },
  storeDtlCenter: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "center"
  },
  Mystores: {
    flexDirection: "row",
    alignItems: "flex-start",
    justifyContent: "space-between",
    textAlignVertical: "bottom",
    flex: 2,
    marginTop: 5
    // paddingBottom:15,
  },
  storeCardSpace: {
    marginBottom: 15,
  },
  headerstyle: {
    paddingBottom: 20,
    paddingTop: 20
  },
  orderCardButton:{
    borderColor:'#0080FE',
    borderWidth: 1,
    width:25,
    height:25,
    borderRadius:50,
    alignItems:"center",
    justifyContent:'center',
  },

  storeBtnViewColor: {
    backgroundColor: '#ba55d3',
    color: 'white',
  },
  continueBtnViewColor: {
    backgroundColor: '#2f4f4f',
    color: 'white',
  },
  addtoCartBtnColor: {
    fontSize:12,
    borderWidth: 1,
    borderColor: '#0080FE',
    marginLeft: 20,
  },
  storeBtn: {
    paddingVertical: 5,
    paddingHorizontal: 5,
    marginBottom: 5,
    color: 'white',
    borderRadius: 0,
    width: 50
  },
  basketBtn:{
    paddingVertical: 5,
    paddingRight: 14,
    marginBottom: 10,
    borderRadius: 20,
    height: 30,
    width: 30,
  },
  basketImg: {
    height: 30,
    width: 30,
  }

});

//--------------------------------------------------- store related fuction  -- Start

const mapStateToProps = (state) => {
return {
    cartItem: state.cartItem,
    basketItem: state.basketItem,
    cartState: state.cartState,
    userData: state.loginData,
}
}

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(addToCart, dispatch),
        basketAction: bindActionCreators(basketAction,dispatch),
        cartModalAction: bindActionCreators(cartModalAction,dispatch),
        loginActions: bindActionCreators(loginSuccess,dispatch),
        navigationAction: bindActionCreators(navigateAction,dispatch),
    };
}

// ---------------------------------------------------- store related function  -- END


export default connect(mapStateToProps, mapDispatchToProps)(FlashHubPage);