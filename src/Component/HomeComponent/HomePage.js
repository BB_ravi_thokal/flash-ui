/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Fragment, useEffect, PureComponent } from 'react';
import axios from 'axios';
import * as Config from '../../Constant/Config.js';
import * as Constant from '../../Constant/Constant.js';
import NavigationBar from '../ReusableComponent/NavigationBar.js';
import { connect } from 'react-redux';
import * as loginSuccess  from '../../Actions/LoginAction';
import * as walletAction  from '../../Actions/WalletAction';
import * as currentLocation  from '../../Actions/CurrentLocationAction';
//import * as addToCart  from '../../Actions/CartItem.js';
import { bindActionCreators } from 'redux';
import  s  from '../../sharedStyle.js';
import CartModal from '../ReusableComponent/cartModal';
import OnGoingActivityModal from '../ReusableComponent/OnGoingActivityModal';
import InternateModal from '../ReusableComponent/InternetModal';
import Geolocation from '@react-native-community/geolocation';
import { NavigationActions, StackActions } from 'react-navigation';
import Toast from 'react-native-simple-toast';
import { SliderBox } from 'react-native-image-slider-box';
import * as basketAction  from '../../Actions/BasketAction';
import googleIntegration from '../../CoreFunctions/googleIntegration';
import callAPI from '../../CoreFunctions/callAPI';
// import 'bootstrap/dist/css/bootstrap.min.css';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  Dimensions,
  View,
  Text,
  TouchableOpacity,
  FlatList,
  Image,
  BackHandler,
  Linking,
  Platform
} from 'react-native';

import {
  Colors,
} from 'react-native/Libraries/NewAppScreen';
import AnimatedLoader from "react-native-animated-loader";
import * as addToCart from '../../Actions/CartItem.js';
import * as cartModalAction  from '../../Actions/CartModalAction';
import VersionUpdateModal from '../ReusableComponent/VersionUpdtePopup';
import * as navigateAction  from '../../Actions/NavigationBarAction';
import PopupModal from '../ReusableComponent/PopupComponent';
import { thisExpression } from '@babel/types';
import RNExitApp from 'react-native-exit-app';
// import SceneLoader from 'react-native-scene-loader';


function CategoryList({ categoryId,categoryName, categoryLogo,navigation}) {
  
  return (
    <View style={styles.categorySection}>
      <TouchableOpacity style={[styles.imageCards]} onPress={() => navigation.navigate('shopList',{"categoryId": categoryId})}>
        <Image style={styles.imageViewCategory} source={{uri: categoryLogo}}></Image>
        {/* <Text style={{fontSize: 12}}>{categoryLogo}</Text> */}
      </TouchableOpacity>
      
    </View>
  )
}


function TrandingShopList ({shopId,shopName,logo,navigation}) {
  return (
    <View style={styles.offerCardDisplay}>
      <TouchableOpacity style={styles.offerCards} onPress={() => navigation.navigate('flashHubPage',{shopId: shopId})}>
        <Image style={styles.imageView} source={{uri: logo}}></Image>
      </TouchableOpacity>
      <Text style={{color: "#003151",fontSize: 12,fontFamily: "verdana",paddingHorizontal: 10, width: 130}} numberOfLines={4}>{shopName}</Text>
    </View>
  )

}

class HomePage extends PureComponent {
  
 // backHandler;
  constructor(props) {
    super(props);
    this.state={
      data: "",
      shopData: [],
      userData1: {},
      stateData: {},
      showVersionModal: false,
      isModalVisible: false,
      isAllowClick: false,
      locationBtnTag: 'Allow',
      locationMsg: '',
      showLoader: false,
      loading: false,
      region: {
        latitude: 10,
        longitude: 10,
        latitudeDelta: 0.001,
        longitudeDelta: 0.001
      },
      userLocation: "",
      currentRegion: "",
      regionChangeProgress: false,
      paddingBottomHeight: 0
    }

    
    // obj.backAndroid = obj.backAndroid.bind(this);
    
    // BackHandler.addEventListener('hardwareBackPress', this.backAndroid);
    //this.backAndroid = this.backAndroid.bind(this);
  }

  OfferListHome = ({item}) => {
    return (
      <TouchableOpacity style={styles.sectionContainer} onPress={() => this.advertiseBannerClick(item)}>
        <View style={styles.advertiseData}>
          <Image style={styles.imageView} source={{uri: item.imageURL}}/>
        </View>
      </TouchableOpacity> 
    )  
  } 

  getLocation = () => {
    if (Object.keys(this.props.currentLocation.currentRegion).length <= 0) {
      Geolocation.getCurrentPosition(
        (position) => {
          let region = {};
          region = {
              latitude: position.coords.latitude,
              longitude: position.coords.longitude,
              latitudeDelta: 0.001,
              longitudeDelta: 0.001
          };
          this.manageAfterGetLocation(region);
        },
        (error) => {
          if (error.PERMISSION_DENIED === 1) {
            this.setState({showLoader: false});
            console.warn('1nd if ---error--->', error);
            // if (error.code == 2) {
            //   this.setState({showLoader: false, locationBtnTag: 'Allow', locationMsg: 'Please check your location properties in setting. We not able to access the permissions.'});  
            // } else {
            //   this.setState({showLoader: false, locationBtnTag: 'Allow', locationMsg: 'Location permission is mandatory in order to use this app. Please check your device settings to activate location'});
            // }
            // this.setState({ isModalVisible: true });
            this.props.navigation.navigate('selectLocationPage');
          } else {
            // console.warn('2nd else ---error--->', error);
            // this.setState({showLoader: false, locationBtnTag: 'Allow', locationMsg: 'Location not found. Please allow location permission to detect the location of your device or check your network'});
            // this.setState({ isModalVisible: true });
            this.setState({showLoader: false});
            this.props.navigation.navigate('selectLocationPage');
          }
        },
        { enableHighAccuracy: false, timeout: 5000, maximumAge: 5000 },
      );
    } else {
      console.log('has loc already');
      this.setState({ region: this.props.currentLocation.currentRegion });
      this.getData();
      if (Object.keys(this.props.userData.userData).length > 0) { 
        this.fetchUserDetails();
      }
    }
  }

  manageAfterGetLocation = async (region) => {
    await this.setState({region: region});
    const currentAddress = await googleIntegration.fetchAddress(region.latitude, region.longitude); 
    // console.warn('currentAddress', currentAddress);
    await this.props.locationAction.changeLocation(region, currentAddress.rawAddress[1].short_name);
    const isValidRegion = await googleIntegration.verifyPostalCode(currentAddress.rawAddress, this.props.userData.userData.pinCode);
    if (isValidRegion) {
      this.getData();
      this.checkCart();
      // this.fetchUserDetails();
    } else {
      // console.warn('navigate');
      this.showNotServingMessage();
    }
  }
    

  // getLocation = async() => {
  //   console.warn('Test--> in get location');
  //   // let test = true;
  //   // setTimeout(() => {
  //   //   if (Object.keys(this.props.currentLocation.currentRegion).length <= 0) {
  //   //     this.setState({showLoader: false}); 
  //   //     this.setState({ locationBtnTag: 'Allow', locationMsg: 'Please check your location properties in setting. We not able to access the permissions.'});  
  //   //     this.setState({ isModalVisible: true });
  //   //    }
  //   // }, 5000);
  //   try {
  //     if (Object.keys(this.props.currentLocation.currentRegion).length <= 0) {
  //       console.warn('in 2nd if----> get location');
        
  //       // const region = await googleIntegration.getCurrentPosition();
  //       Geolocation.getCurrentPosition(
  //         (position) => {
  //           // console.warn('location-->', position);    
  //           let region = {};
  //           region = {
  //               latitude: position.coords.latitude,
  //               longitude: position.coords.longitude,
  //               latitudeDelta: 0.001,
  //               longitudeDelta: 0.001
  //           };
  //           // test = false;
  //           this.setState({region: region});
  //           const currentAddress = googleIntegration.fetchAddress(region.latitude, region.longitude); 
  //           // console.warn('currentAddress', currentAddress);
  //           this.props.locationAction.changeLocation(region, currentAddress.rawAddress[1].short_name);
  //           const isValidRegion = googleIntegration.verifyPostalCode(currentAddress.rawAddress, this.props.userData.userData.pinCode);
  //           if (isValidRegion) {
  //             this.getData();
  //             this.checkCart();
  //             // this.fetchUserDetails();
  //           } else {
  //             // console.warn('navigate');
  //             this.showNotServingMessage();
  //           }
  //         },
  //         (error) => {
  //           // test = true;
  //           this.setState({showLoader: false}); 
  //           if (error.PERMISSION_DENIED === 1) {
  //             console.warn('1nd if ---error--->', error);
  //             if (error.code == 2) {
  //               this.setState({showLoader: false, locationBtnTag: 'Allow', locationMsg: 'Please check your location properties in setting. We not able to access the permissions.'});  
  //             } else {
  //               this.setState({showLoader: false, locationBtnTag: 'Allow', locationMsg: 'Location permission is mandatory in order to use this app. Please check your device settings to activate location'});
  //             }
  //             this.setState({ isModalVisible: true });
  //           } else {
  //             console.warn('2nd else ---error--->', error);
  //             this.setState({showLoader: false, locationBtnTag: 'Allow', locationMsg: 'Location not found. Please allow location permission to detect the location of your device or check your network'});
  //             this.setState({ isModalVisible: true });
  //           }
  //           // this.getData();
  //           // this.fetchUserDetails();
  //           return error;
              
  //         },
  //         { enableHighAccuracy: false, timeout: 5000, maximumAge: 5000 },
  //     );
  //     } else {
  //       console.warn('in first else can continue');
  //       this.setState({ region: this.props.currentLocation.currentRegion });
  //       this.getData();
  //       this.fetchUserDetails();
  //     }
  //   } catch (error) {
  //     console.warn('error in region --', error);
  //     this.setState({showLoader: false});
  //   }
    
  // }

  checkCart = () => {
    try {
      if(this.props.cartItem.cartItem){
        if (this.props.cartItem.cartItem.length > 0) {
          let counter = 0;
          let cartSum = 0;
          for (let item of this.props.cartItem.cartItem) {
            cartSum = parseFloat(cartSum) + parseFloat(item.totalCost);
            counter = parseInt(counter) + parseInt(item.counter);
          }     
          this.props.cartModalAction.changeCartState(true,cartSum,counter);
          this.setState({paddingBottomHeight: 80});
        }
      }
    } catch(error) {
      
    }
  }

  componentWillReceiveProps() {
    console.warn('in receive props home page');
    if (this.props.cartState.cartState) {
      this.setState({paddingBottomHeight: 80});
    } else {
      this.setState({paddingBottomHeight: 0});
    }
  }
  
  componentDidMount(){
    console.warn('here on home');
    this.props.navigationAction.navigateAction('Home');
    if (this.props.cartState.cartState) {
      this.setState({paddingBottomHeight: 80});
    }
    // checkVersion()
    // .then(version => {
    //     console.warn('Got version info:', version);
    //     if (version.needsUpdate) {
    //       // this.setState({showLoader: false});
    //       this.setState({showLoader: false, showVersionModal: true});
    //     } else {
    //       this.setState({showLoader: true});
    //       this.getLocation(); 
    //     }
    // })
    // .catch(e => {
    //     // Error - could not get version info for some reason
    //     console.log('Failed getting version info:', e);
    //     this.setState({showLoader: true});
    //     this.getLocation(); 
    // })
    //disableAndroidBackButton();
    this.setState({showLoader: true});
    if (Object.keys(this.props.userData.userData).length > 0) {
      if (this.props.userData.userData.userDetails.length > 0) {
        console.warn('111111111111');
        this.getLocation();
      } else {
        console.warn('2222222222');
        this.getDataWithoutLogin();
      }
    } else {
      console.warn('33333333333333');
      this.getDataWithoutLogin();
    };
    this.props.navigation.addListener('willFocus', () =>{
      console.warn('am in focus Home');
      this.props.navigationAction.navigateAction('Home');
    });
  }

  handleURL = (event) => {
    console.warn('Linking URL---===>', event.url);
  }

  getDataWithoutLogin = async () => {
    console.warn('this.props.cartItem.cartItem before api', this.props.cartItem.cartItem);
    const reqBody = {
      position: this.state.region,
      basket: this.props.basketItem.basketItem,
      cart: this.props.cartItem.cartItem 
    }
    const getData = await callAPI.axiosCall(Config.getDataWithoutLogin, reqBody);
    await this.props.loginActions.loginSuccess(getData.data);
    if (getData.data.cartDetails.length > 0) {
      console.warn('11111');
      if (getData.data.cartDetails[0].cart) {
        console.warn('in cart11');
        if (getData.data.cartDetails[0].cart.length > 0) {
          console.warn('in cart12');
          await this.props.addCartActions.replaceCart(getData.data.cartDetails[0].cart);
        }
      }
    }
    if (getData.data.basketDetails.length > 0) {
      if (getData.data.basketDetails[0].basket) {
        console.warn('basket1');
        if (getData.data.basketDetails[0].basket.length > 0) {
          console.warn('basket2')
          await this.props.basketAction.replaceBasket(getData.data.basketDetails[0].basket);
        }
      }
    }
    if (getData.data.topSliderDetails) {
      this.props.loginActions.addTopSliders(getData.data.topSliderDetails);
    }
    this.setState({showLoader: false});
    if (Object.keys(this.props.currentLocation.currentRegion).length <= 0) {
      this.getLocation();
    } else {
      this.getData();
    }
    
  }

  fetchUserDetails = async() => {
    this.setState({showLoader: true});
    console.warn('in fetch user details');
    const reqBody = {
      userId: this.props.userData.userData.userId,
      basket: this.props.basketItem.basketItem,
      cart: this.props.cartItem.cartItem
    }
    await callAPI.axiosCall(Config.updateCArtBasket, reqBody);
    try {
      const reqBody = {
        userId: this.props.userData.userData.userDetails[0].userId,
        roleId: this.props.userData.userData.roleDetails[0].roleId,
        position: this.state.region,
        isNewVersion: true
      };
      const userDetailsRes = await callAPI.axiosCall(Config.fetchUserData, reqBody);
      console.warn('user details after fecth data---->', userDetailsRes);
      if (userDetailsRes) {
        userDetailsRes.data.addressDetails = userDetailsRes.data.addressDetails.map( item => {
          if(item.isSelected == true){
            item.backGroundColor = "#DCF0FA" 
          }
          else{
            item.backGroundColor = "white"
          }
          return item;
          
        })
        this.setState({showLoader: false});
        Toast.show(userDetailsRes.data.message,Toast.LONG);
        this.props.loginActions.loginSuccess(userDetailsRes.data);
        this.props.walletAction.updateWallet(userDetailsRes.data.userDetails[0].wallet, true);
        
        if (userDetailsRes.data.topSliderDetails) {
          this.props.loginActions.addTopSliders(userDetailsRes.data.topSliderDetails);
        }
        console.warn('userDetailsRes.data.cartDetails length', userDetailsRes.data.cartDetails[0].cart.length);
        if (userDetailsRes.data.cartDetails) {
          console.warn('11111');
          if (userDetailsRes.data.cartDetails[0].cart) {
            console.warn('in cart11');
            if (userDetailsRes.data.cartDetails[0].cart.length > 0) {
              console.warn('in cart12');
              await this.props.addCartActions.replaceCart(userDetailsRes.data.cartDetails[0].cart);
            }
          }
        }
        if (userDetailsRes.data.basketDetails) {
          if (userDetailsRes.data.basketDetails[0].basket) {
            console.warn('basket1');
            if (userDetailsRes.data.basketDetails[0].basket.length > 0) {
              console.warn('basket2')
              await this.props.basketAction.replaceBasket(userDetailsRes.data.basketDetails[0].basket);
            }
          }
        }
        console.warn('call update basket');
        this.checkCart();
      }  
    } catch (err) {
      console.warn('error in fetch user data-->', err);
      this.setState({showLoader: false});
    }
  }

  updateCartBasket = async() => {
    console.warn('in update basket method');
    const reqBody = {
      "userId": this.props.userData.userData.userId,
      "basket": this.props.basketItem.basketItem,
      "cart": this.props.cartItem.cartItem
    }
    const test = await callAPI.axiosCall(Config.updateCArtBasket, reqBody);
    console.warn('response update basket--->', test);
  }

  advertiseBannerClick = (item) => {
    // console.warn('in click-->', item);
    if (item.shopId) {
      this.props.navigation.navigate('flashHubPage', {"shopId": item.shopId});
    } else if(item.url) {
      this.props.navigation.navigate(item.url);
    }
  }


  showNotServingMessage = async() => {
    await this.setState({showLoader: false});
    Toast.show('We are currently operating only in Navi Mumbai areas',Toast.LONG);
    // if (!this.state.showLoader) {
    //   console.warn('in if ---', this.state.showLoader);
    //   this.props.navigation.navigate('selectLocationPage');
    // }
    this.props.navigation.navigate('selectLocationPage');
  }

  async getData(){
    this.setState({showLoader: true});
    console.warn('in get data called--->', this.state.region);
    try {
      const getCategoryRes = await callAPI.axiosCall(Config.getCategories, { userId: this.props.userData.userData.userId });
      // console.log('getCategoryRes--->', getCategoryRes);
      if (getCategoryRes) {
        this.setState({data: getCategoryRes.data.result});
      }
      const reqBody = {
        position: this.state.region
      }
      console.warn('Req body for region==>', reqBody);
      const getCategoriesByShopRes = await callAPI.axiosCall(Config.getCategoriesByShop, reqBody);
      this.setState({showLoader: false});
      console.warn('getCategoriesByShopRes==========--->', getCategoriesByShopRes);
      if (getCategoriesByShopRes) {
        if (getCategoriesByShopRes.data.status == 0) {
          this.setState({shopData: getCategoriesByShopRes.data.result});
        }
      }
    } catch (err) {
      this.setState({showLoader: false});
      console.warn('getData error-->', err);
    }
  }

  
  TrandingShop = () => {
    if(this.state.shopData.length > 0) {
      return (
        <View style={{flex: 1,marginHorizontal: 10}}>
          <Text style = {[s.subHeaderText,{marginTop: 5,elevation: 6}]}>Shops Near You</Text>
          <ScrollView style={{paddingBottom: 15,elevation: 4}}>
            <FlatList data={this.state.shopData}
              renderItem={( { item } ) => (
                <TrandingShopList
                  shopId={item.shopId}
                  shopName={item.shopName}
                  logo={item.shopMiniLogo}
                  navigation={this.props.navigation}
                />
              )}
              keyExtractor= {item => item.shopId}
              horizontal={true}
              showsHorizontalScrollIndicator = {false}
            />
          </ScrollView>
        </View>
      )
    } else {
      return null;
    }
   
  }

  ComboOffer = () => {
    const {navigate} = this.props.navigation;
    return (
      <View style={{flex: 1, marginHorizontal: 10, marginBottom: 10}}>
        <Text style = {[s.subHeaderText,{marginTop: 10,elevation: 6}]}>Get Combos From Flash</Text>
        <TouchableOpacity style={styles.advertiseDataFlash} onPress={() => navigate('comboOfferPage')}>
          <Image style={styles.imageView} source={{uri: 'https://flash-catagory-image.s3.ap-south-1.amazonaws.com/combobanner.jpg'}}/>
        </TouchableOpacity>
      </View>
    )
  }

  rejectLocation = () => {
    this.setState({
      isModalVisible: false,
      locationMsg: '',
    });
    if (Platform.OS == 'android') {
      BackHandler.exitApp();
    } else {
      console.warn('close app');
      RNExitApp.exitApp();
      // this.openDeviceSettings();
    }
  }

  changeLocation = () => {
    if (Object.keys(this.props.userData.userData).length > 0) {
      if (this.props.userData.userData.userDetails.length > 0) {
        this.props.navigation.navigate('currentLocationPage');
      } else {
        this.props.navigation.navigate('selectLocationPage');
      }
    } else {
      this.props.navigation.navigate('selectLocationPage');
    }
  }

  showWallet = () => {
    if (Object.keys(this.props.userData.userData).length > 0) { 
      if (this.props.userData.userData.userDetails.length > 0) {
        this.props.navigation.navigate('walletPage', {id: this.props.userData.userData.userId, roleName: this.props.userData.userData.roleDetails[0].roleName});
      } else {
        Toast.show('Please login to view your wallet', Toast.LONG);
      }
    } else {
      Toast.show('Please login to view your wallet', Toast.LONG);
    }
  }

  checkLogin = async () => {
    if (Object.keys(this.props.userData.userData).length > 0) { 
      if (this.props.userData.userData.userDetails.length > 0) {
        return true;
      }
      return false;
    }
    return false;
  }

  activityClick = async(activityType) => {
    const isLogin = await this.checkLogin();
    if (isLogin) {
      this.props.navigation.navigate(activityType);
    } else {
      Toast.show('Please login into application to use this feature', Toast.LONG);
    }
  }
 
  openDeviceSettings = async() => {
    console.warn('here 111', this.state.isAllowClick);
    console.warn('Platform.OS', Platform.OS);
    if (!this.state.isAllowClick) {
      await this.setState({
        isAllowClick: true,
        locationMsg: 'Location permission is mandatory in order to use this app. If already given please ignore and enjoy your shopping!!.',
        locationBtnTag: 'Ok'
      });
      if (Platform.OS == 'android') {
        Linking.openSettings();
      } else {
        Linking.openURL('app-settings:');
      }
      
    } else {
      this.setState({
        isModalVisible: false,
        isAllowClick: false,
        locationMsg: '',
        locationBtnTag: 'Allow',
      })
      this.getLocation();
    }
  }

    render() {
        const {navigate} = this.props.navigation;
        return (
          <Fragment>
            <SafeAreaView style={{flex: 0, backgroundColor: '#f1f1f1'}} />
            <SafeAreaView style={{flex: 1, backgroundColor: 'white'}}>
              <View style={[s.bodyGray]}>
                <AnimatedLoader
                  visible={this.state.showLoader}
                  overlayColor="rgba(255,255,255,0.75)"
                  source={require("../../../assets/loader.json")}
                  animationStyle= {[styles.lottie,{height: 150,width: 150}]}
                  speed={1}
                />
                {/* <SceneLoader
                  visible={this.state.showLoader}
                  animation={{
                      fade: {timing: {duration: 1000}}
                  }}
                /> */}
                {/* <InternateModal /> */}
                <View style={{paddingTop: 10, paddingHorizontal: 15, flexDirection:"row", height: 55}}>
                  <TouchableOpacity style={{flex: 6}} onPress={() => this.changeLocation()}>
                    <View>
                      <View style={{flexDirection: "row", backgroundColor: "white", padding: 5, borderBottomRightRadius: 100, borderTopLeftRadius: 100, borderBottomLeftRadius: 100}}>
                        <View >
                          <Image style={{height: 35, width: 25}} source={require('../../../assets/Logo.png')}/>
                        </View>
                        <View style={{paddingLeft: 5, width: 120}}>
                          <Text style={[s.normalText,{fontSize: 12}]}>Current location</Text>
                          <Text numberOfLines={1} style={[s.normalText,{color:"#00CCFF"}]}>{this.props.currentLocation.currentPlace}</Text>
                        </View>
                      </View>
                    </View>
                    
                  </TouchableOpacity>
                  <View style={{flex: 6, flexDirection:"row"}} >
                    <TouchableOpacity style={{paddingRight: 10, flex: 9, alignItems:"flex-end"}} onPress={() => this.showWallet()}>
                      <View style={{flexDirection: "row"}}>
                        <View style={{paddingRight: 5, alignItems:"flex-end", paddingTop: 5}}>
                          <Text numberOfLines={2} style={[s.subHeaderText,{color:"#00CCFF"}]}>₹{this.props.walletBalance.walletBalance}</Text>
                        </View>
                        <View style={{height: 35, width: 35}}>
                          <Image style={styles.imageViewIcon} source={require('../../../assets/wallet.png')}/>
                        </View>  
                      </View>
                    </TouchableOpacity>
                    <TouchableOpacity style={{flex: 3, alignItems:"flex-end"}} onPress={() => navigate('comboOfferPage')}>
                      <View style={{height: 35, width: 35}}>
                        <Image style={styles.imageViewIcon} source={require('../../../assets/discount.png')}/>
                      </View>  
                    </TouchableOpacity>
                  </View>
                    
                </View>
                {/* <View style={{ borderBottomColor: "silver", borderBottomWidth: 0.5, marginTop: 10}}></View> */}
                
                <ScrollView showsVerticalScrollIndicator = {false}>
                <SliderBox   
                  images={this.props.userData.topSlider}   
                  // sliderBoxHeight={200}
                  renderItem={( { item } ) => (
                    <this.OfferListHome
                      item={item}
                    />
                  )}
                  dotColor="#00CCFF"   
                  inactiveDotColor="white"   
                  paginationBoxVerticalPadding={20}   
                  autoplay   
                  dotStyle={{width: 8,height: 8,borderRadius: 4,marginHorizontal: 0,padding: 0,margin: 0,}}
                  circleLoop />
                        {/* <FlatList data={this.props.userData.userData.topSliderDetails}
                          renderItem={( { item } ) => (
                            <OfferListHome
                              item={item}
                            />
                          )}
                          keyExtractor= {item => item.offerId}
                          horizontal={true}
                          
                        /> */}
                        {/* <View style={styles.sectionContainer}>
                            <View style={styles.advertiseData}>
                              <Image style={styles.advertiseImage} source={require('../../../assets/Banner_01.jpg')}/>
                            </View>
                        </View>
                        <View style={styles.sectionContainer}>
                            <View style={styles.advertiseData}>
                              <Image style={styles.advertiseImage} source={require('../../../assets/Banner_02.jpg')}/>
                            </View>
                        </View>
                        <View style={styles.sectionContainer}>
                            <View style={styles.advertiseData}>
                              <Image style={styles.advertiseImage} source={require('../../../assets/Banner_03.jpg')}/>
                            </View>
                        </View> */}
                      <View style={{backgroundColor:"white", borderTopLeftRadius: 40, borderTopRightRadius: 40, paddingTop: 15, elevation: 4, paddingBottom: this.state.paddingBottomHeight}}>
                      <View style={{flex: 1,marginHorizontal: 10}}>
                        <Text style = {[s.subHeaderText,{marginTop: 10,elevation: 6}]}>Shop From Flash Mart</Text>
                          {/* <View style={{backgroundColor:"#DCF0FA",paddingBottom: 15,alignItems:"center",elevation: 4}}> */}
                            {/* <TouchableOpacity style={styles.advertiseDataFlash} onPress={() => navigate('flashHubPage')}>
                              <Image style={styles.advertiseImageFlash} source={require('../../../assets/flashHubBanner1.jpeg')}/>
                            </TouchableOpacity> */}
                            <TouchableOpacity style={styles.advertiseDataFlash} onPress={() => navigate('flashHubPage')}>
                              <Image style={styles.imageView} source={{uri: 'https://flash-catagory-image.s3.ap-south-1.amazonaws.com/flashbanner.jpg'}}/>
                            </TouchableOpacity>
                          {/* </View> */}
                          
                      </View>
                      <View style={{flex: 1,marginHorizontal: 10}}>
                        <Text style = {[s.subHeaderText,{marginTop: 10,elevation: 6}]}>Add Activities</Text>
                          <View style={{flex: 1, flexDirection: "row",paddingVertical: 10,alignItems:"center"}}>
                            <TouchableOpacity style={{flex: 1, width: 100, height: 100, borderRadius: 4}}
                            onPress={() => this.activityClick('sendPackageList')} >
                              <Image style={styles.imageView} source={{uri: 'https://flash-catagory-image.s3.ap-south-1.amazonaws.com/send-package.jpg'}}/>
                              {/* <Text style={[s.normalText,{fontWeight:"bold",paddingTop: 10}]} numberOfLines={2}>Get Package From Anywhere</Text> */}
                            </TouchableOpacity>
                            <TouchableOpacity style={{flex: 1, width: 100, height: 100, borderRadius: 4}}
                            onPress={() => this.activityClick('packageDetailsPage')}>
                              <Image style={styles.imageView} source={{uri: 'https://flash-catagory-image.s3.ap-south-1.amazonaws.com/get-package.jpg'}}/>
                              {/* <Text style={[s.normalText,{fontWeight:"bold",paddingTop: 10}]} numberOfLines={2}>Send Package Anywhere</Text> */}
                            </TouchableOpacity>
                          </View>
                      </View>
                        
                      <View style={{flex: 1,marginHorizontal: 10}}>
                        <Text style = {[s.subHeaderText,{marginTop: 10,elevation: 6}]}>Shop By Categories</Text>
                        <View style={{flex: 1, paddingBottom: 15, alignItems:"center", justifyContent:"space-between"}}>
                          <FlatList data={this.state.data}
                            renderItem={( { item } ) => (
                              <CategoryList
                                categoryId={item.categoryId}
                                categoryName={item.categoryName}
                                categoryLogo={item.categoryLogo}
                                navigation={this.props.navigation}
                              />
                            )}
                            keyExtractor= {item => item.categoryId}
                            horizontal={false}
                            numColumns={2}
                            
                          />
                        </View>
                      
                      </View>     
                      <this.TrandingShop />
                      <this.ComboOffer />
                  </View>
                  
                </ScrollView>
                <PopupModal visible={this.state.isModalVisible} onDismiss={this.rejectLocation} removeProduct = {this.openDeviceSettings} 
                  message={this.state.locationMsg} btnTag={this.state.locationBtnTag} 
                  />  
                <CartModal navigate={navigate} navigationPage={'orderDetailsPage'}></CartModal>
                <OnGoingActivityModal navigate={navigate}></OnGoingActivityModal>
                <VersionUpdateModal visible={this.state.showVersionModal} navigate={navigate}></VersionUpdateModal>
                <NavigationBar navigate={navigate}></NavigationBar>  
              </View>        
                
            </SafeAreaView>
          </Fragment>
          
          
          
        );
    }
};

let ScreenHeight = Dimensions.get("window").height;
let ScreenWidth = Dimensions.get("window").width;

const styles = StyleSheet.create({
  
  
  engine: {
    position: 'absolute',
    right: 0,
  },
  sectionContainer: {
    marginTop: 15,
    marginBottom: 40,
    paddingHorizontal: 20,
    alignItems: "center",
    justifyContent: "center",
    alignContent: "center",
    flex: 1,
    elevation: 6   
  },
  advertiseData: {
    height:140,
    width: ScreenWidth - 40,
    // borderRadius: 10
  },
  imageViewIcon: {
    flex: 1,
    width: undefined,
    height: undefined,
    resizeMode: 'contain',
  },
  imageView: {
    flex: 1,
    width: undefined,
    height: undefined,
    resizeMode: 'contain',
  },
  imageViewCategory: {
    flex: 1,
    width: (ScreenWidth - (ScreenWidth/2) - 45),
    height: undefined,
    borderRadius: 10,
  },

  categorySection:{ 
    marginTop: 15,
    paddingHorizontal: 10,
  },
  storeHeadinglbl:{
    color: 'white',
    fontSize:16
  },
  storeHeadingView:{
    display:'flex',
    flexDirection:'row',
    justifyContent:'space-between',
    padding:10
  },
  StoreCardsLayout:{
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between'
},
  materialTypeHeading:{
    color: 'black',
    fontSize:18,
    fontWeight: "bold",
    paddingBottom:10
},
  categoryHeadingTxt:{
    color: 'white',
    fontSize:16,
    textAlign:'center'
},
  flexRowSpaceBetween:{
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  bookCardLayout:{
    padding:10,
    display:'flex',
  flexDirection:'column'
},
  categoryHeadingView:{
    borderBottomColor: 'lightgray',
    borderBottomWidth: 1,
    padding:10,
    textAlign:'center',
  },
  storeHeading:{
    color: 'black',
    fontSize:18,
    fontWeight: "bold"
  },
  storeDesc:{
    color: 'gray',
    fontSize:16
  },
  trendingShops: {
    borderWidth: 1,
    borderColor: 'lightgray',
    borderRadius: 4,
    marginTop: 32,
    padding: 15,
    margin: 10,
  },
  advertiseDataFlash: {
    height:140,
    marginHorizontal: 15,
    marginTop: 10,
    flex: 1,
    borderRadius: 10,
    width: ScreenWidth - 45
  },
  imageCards: {
    flex: 1,
    height: 120,
    // width: 140,
    width: (ScreenWidth - (ScreenWidth/2) - 45),
    // paddingLeft: 15,
    paddingBottom: 10,
    borderRadius: 50
  },
  offerCards:{
    width: 100,
    height: 100,
    // borderColor: 'black',
    marginVertical:10,
    marginHorizontal:10,
    // elevation: 8
  },
  offerCardDisplay:{
    paddingTop: 5,
    alignItems:'center',
    elevation: 10,
  },
  storeCards: {
    height:100,
    borderRadius: 4,
    borderWidth: 0.5,
    borderColor: 'black',
    backgroundColor: 'white',
    flex: 3,
  },
  productHeader: {
    height: 100,
    borderColor: 'black',
  },
  storeContainer: {
    borderRadius: 4,
    borderWidth: 1,
    borderColor: 'black',
    backgroundColor: 'navy',
  },
  sectionTitle: {
    borderRadius: 4,
    borderWidth: 0.5,
    borderColor: '#d6d7da',
    // fontSize: 24,
    // fontWeight: '600',
    color: Colors.black,
    // borderStyle:1px solid red
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
});


//--------------------------------------------------- store related fuction  -- Start

const mapStateToProps = (state) => {
	return {
    userData: state.loginData,
    cartState: state.cartState,
    cartItem: state.cartItem,
    basketItem: state.basketItem,
    currentLocation: state.currentLocation,
    selectedIcon: state.selectedIcon,
    walletBalance: state.walletBalance
	};
}

const mapDispatchToProps = (dispatch) => {
    return {
      loginActions: bindActionCreators(loginSuccess,dispatch),
      addCartActions: bindActionCreators(addToCart, dispatch),
      basketAction: bindActionCreators(basketAction,dispatch),
      cartModalAction: bindActionCreators(cartModalAction,dispatch),
      locationAction: bindActionCreators(currentLocation,dispatch),
      walletAction: bindActionCreators(walletAction,dispatch),
      navigationAction: bindActionCreators(navigateAction,dispatch),
    };
}

// ---------------------------------------------------- store related function  -- END

export default connect(mapStateToProps, mapDispatchToProps)(HomePage);
