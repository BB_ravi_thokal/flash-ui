/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import * as addToCart  from '../../Actions/CartItem';
import { bindActionCreators } from 'redux';
import axios from 'axios';
import * as Config from '../../Constant/Config.js';
import * as Constant from '../../Constant/Constant.js';
import NavigationBar from '../ReusableComponent/NavigationBar.js'
import  s  from '../../sharedStyle.js'
import { Rating } from 'react-native-ratings';
import BasketModal from '../ReusableComponent/basketModal';
import CartModal from '../ReusableComponent/cartModal';
import OnGoingActivityModal from '../ReusableComponent/OnGoingActivityModal';
import * as basketAction  from '../../Actions/BasketAction';
import * as cartModalAction  from '../../Actions/CartModalAction';
import InternateModal from '../ReusableComponent/InternetModal';
import * as navigateAction  from '../../Actions/NavigationBarAction';
//import ProductListing from './productListing.js';
import Toast from 'react-native-simple-toast';
// import SceneLoader from 'react-native-scene-loader';
import AnimatedLoader from "react-native-animated-loader";
// import 'bootstrap/dist/css/bootstrap.min.css';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  Dimensions,
  TouchableOpacity,
  Button,
  TextInput,
  StatusBar,
  TouchableWithoutFeedback,
  Image,
  ToastAndroid,
  FlatList,
  SectionList,
  BackHandler
} from 'react-native';
import PopupModal from '../ReusableComponent/PopupComponent.js';

function SubCategoryListView ({item, self}) {
  return (
    <TouchableOpacity style={[{width: (ScreenWidth)/3 - 2}, {height: (ScreenWidth)/3 - 2}, 
        {marginHorizontal: 2}, {marginBottom: 2, borderColor:"#DCF0FA", borderWidth:1}]} onPress={() => self.fetchSubCategoryMenu(item._id)}>
        <Image style={{flex: 1, width: undefined, height: undefined}} source={require('../../../assets/foodIcon.jpg')}></Image>
    </TouchableOpacity>
  )
}

class ShopDetailPage extends React.Component{

  constructor(props) {
    super(props);
    this.state={
      shopData: "",
      shopMenu: "",
      shopName: "",
      shopImage: "",
      shopDescription: "",
      shopAddress: "",
      shopRating: null,
      shopId: "",
      categoryId: "",
      cartItem: [],
      searchText: "",
      showBasketModal: false,
      addtoBasketItem: {},
      modalVisible: false,
      cart: {},
      distinctCategories: [],
      selectedCategory: '',
      isFilter: false,
      showLoader: true,
      showCategoryFilter: false,
      page: 1,
      prev: true,
      next: false,
      maxCount: 0,
      showList: false,
      prevColor: "#00CCFF",
      nextColor: "#00CCFF",
      subCategoryList: [],
      paddingBottomHeight: 0,
      productNotAvailable: false
    }
  }

  fetchSubCategoryMenu = (id) => {
    this.setState({showLoader: true, showList: false, selectedCategory: id});
    console.warn('in fetch sub category ');
    reqBody = {
      categoryId: this.state.categoryId,
      productCategory: this.state.selectedCategory,
      productSubCategory: id,
      searchData: "",
      pageNumber: 1 
    }
    this.getShopMenu(reqBody);
    
  }

  getSubCategories = (body) => {
    console.warn('in et sub cat req body', body);
    axios.post(
      Config.URL + Config.getMenuSubCategories,
      body,
      {
        headers: {
          'Content-Type': 'application/json',
        }
      }
    )
    .then(res => {
      console.warn('in get sub category-->', res);
      this.setState({showLoader: false});
        if (res.data.status == 0) {
          this.setState({
            subCategoryList: res.data.result
          });

        }
        else if(res.data.status == 1){  // Fail to signup
          Toast.show(res.data.message,Toast.LONG);
        }

    }).catch(err => {
      this.setState({showLoader: false});
    })
  }



  // ProductListing = (item) => {
  //   if(this.state.searchText == ""){
  //     const imgPath = item.item.productLogo + item.item.productName.replace(/\s+/g, '') + '.jpg';
  //     return (
  //       <View style={[styles.prodcutListingView]}>
  //         <View style={styles.addCardImage}>
  //           <Image style={styles.productImage} source={{uri: imgPath}}/>
  //         </View>
  //           <View style={styles.stores}>
  //             <View style={{flex:10}}>
  //               <Text style={s.headerText} numberOfLines={1}>{item.item.productName} </Text>
  //             </View>
              
  //             <View style={styles.Mystores}>
  //               <View style={{paddingTop: 10}}>
  //                 <Text style={[s.normalText]} numberOfLines={1}>₹{item.item.unitCost}/{item.item.unit}</Text>
  //               </View>
  //               <View style={styles.flexRow}>
  //                 <TouchableOpacity style={styles.basketBtn} onPress={() => this.showBasketModal(item.item)}>
  //                   <Image style={styles.basketImg} source={require('../../../assets/basketIcon.png')}/>
  //                 </TouchableOpacity>
  //                 <TouchableOpacity onPress={() => this.addCart(item.item)}>
  //                   <View style={[styles.storeBtn, styles.addtoCartBtnColor]}>
  //                     <Text style={{ color: '#0080FE',paddingHorizontal: 4}}>ADD</Text>
  //                   </View>
  //                 </TouchableOpacity>
  
  //               </View>
  //             </View>
  //           </View>
  //       </View>
  //     )
  //   }
  //   else{
  //     const imgPath = item.item.productLogo + item.item.productName.replace(/\s+/g, '') + '.jpg';
  //     return (
  //       <View style={[styles.prodcutListingView]}>
  //         <View style={styles.addCardImage}>
  //           <Image style={styles.productImage} source={{uri: imgPath}}/>
  //         </View>
  //           <View style={styles.stores}>
  //             <View style={{flex:10}}>
  //               <Text style={s.headerText} numberOfLines={1}>{item.item.menu.productName} </Text>
  //             </View>
              
  //             <View style={styles.Mystores}>
  //               <View style={{paddingTop: 10}}>
  //                 <Text style={[s.normalText]} numberOfLines={1}>₹{item.item.menu.unitCost}/{item.item.menu.unit}</Text>
  //               </View>
  //               <View style={styles.flexRow}>
  //                 <View style={styles.basketBtn}>
  //                   <Image style={styles.basketImg} source={require('../../../assets/basketIcon.png')}/>
  //                 </View>
  //                 <TouchableOpacity onPress={() => this.addCart(item.item.menu)}>
  //                   <View style={[styles.storeBtn, styles.addtoCartBtnColor]}>
  //                     <Text style={{ color: '#0080FE',paddingHorizontal: 4}}>ADD</Text>
  //                   </View>
  //                 </TouchableOpacity>
  
  //               </View>
  //             </View>
  //           </View>
  //       </View>
  //     ) 
          
  //   }
  
  
  // }

  ProductListing = (item) => {
    // if(this.state.isFilter){
    //   // const imgPath = item.item.menu.productLogo + item.item.menu.productName.replace(/\s+/g, '') + '.jpg';
    if (this.state.showList) {
      return (
        <View style={[styles.prodcutListingView]}>
          <View style={styles.addCardImage}>
            <Image style={styles.productImage} source={{uri: item.item.menu.productLogo}}/>
          </View>
            <View style={styles.stores}>
              <View style={{flex:10}}>
                <Text style={s.subHeaderText} numberOfLines={3}>{item.item.menu.productName} </Text>
              </View>
              <View style={{flex:1, flexDirection:"row", marginTop: 10}}>
                <Text style={[s.normalText]}>MRP:- </Text>
                <Text style={[s.normalText,{textDecorationLine: 'line-through', textDecorationStyle: 'solid'}]} numberOfLines={1}>
                  ₹{item.item.menu.mrp}</Text>
              </View>
              <View style={{flex:1, flexDirection:"row"}}>
                <Text style={[s.normalText]}>Flash amount:- </Text>
                <Text style={[s.normalText]} numberOfLines={1}>₹{item.item.menu.unitCost}</Text>
              </View>
              
            
              <View style={styles.Mystores}>
                <View style={{paddingTop: 10, flex: 7}}>
                  {/* <Text style={[s.normalText,{textDecorationLine: 'line-through', textDecorationStyle: 'solid'}]} numberOfLines={1}>
                    MRP:- ₹{item.item.menu.unitCost}</Text>
                  <Text style={[s.normalText]} numberOfLines={1}>Flash amount:- ₹{item.item.menu.unitCost}</Text> */}
                  <Text style={[s.normalText]} numberOfLines={3}>Unit:- {item.item.menu.unit}</Text>
                </View>
                <View style={styles.flexRow}>
                  <TouchableOpacity style={styles.basketBtn} onPress={() => this.showBasketModal(item.item.menu)}>
                    <Image style={styles.basketImg} source={require('../../../assets/basketIcon.png')}/>
                  </TouchableOpacity>
                  <TouchableOpacity onPress={() => this.addCart(item.item.menu)}>
                    <View style={[styles.storeBtn, styles.addtoCartBtnColor]}>
                      <Text style={{ color: '#0080FE',paddingHorizontal: 4}}>ADD</Text>
                    </View>
                  </TouchableOpacity>
  
                </View>
              </View>
            </View>
        </View>
      )
    } else {
      return null;
    }
      
    // }
    // else{
      // const imgPath = item.item.productLogo + item.item.productName.replace(/\s+/g, '') + '.jpg';
      // return (
      
      //   <View style={[styles.prodcutListingView]}>
      //     <View style={styles.addCardImage}>
      //       <Image style={styles.productImage} source={{uri: item.item.productLogo}}/>
      //     </View>
      //       <View style={styles.stores}>
      //         <View style={{flex:10}}>
      //           <Text style={s.subHeaderText} numberOfLines={3}>{item.item.productName} </Text>
      //         </View>
      //         <View style={{flex:1, flexDirection:"row", marginTop: 10}}>
      //           <Text style={[s.normalText]}>MRP:- </Text>
      //           <Text style={[s.normalText,{textDecorationLine: 'line-through', textDecorationStyle: 'solid'}]} numberOfLines={1}>
      //             ₹{item.item.mrp}</Text>
      //         </View>
      //         <View style={{flex:1, flexDirection:"row"}}>
      //           <Text style={[s.normalText]}>Flash amount:- </Text>
      //           <Text style={[s.normalText]} numberOfLines={1}>₹{item.item.unitCost}</Text>
      //         </View>
              
      //         <View style={styles.Mystores}>
      //           <View style={{paddingTop: 10, flex: 7}}>
      //             {/* <Text style={[s.normalText,{textDecorationLine: 'line-through', textDecorationStyle: 'solid'}]} numberOfLines={1}>
      //             MRP:- ₹{item.item.unitCost}</Text>
      //             <Text style={[s.normalText]} numberOfLines={1}>Flash amount:- ₹{item.item.unitCost}</Text> */}
      //             <Text style={[s.normalText]} numberOfLines={3}>Unit:- {item.item.unit}</Text>
      //           </View>
      //           <View style={styles.flexRow}>
      //             <TouchableOpacity style={styles.basketBtn} onPress={() => this.showBasketModal(item.item)}>
      //               <Image style={styles.basketImg} source={require('../../../assets/basketIcon.png')}/>
      //             </TouchableOpacity>
      //             <TouchableOpacity onPress={() => this.addCart(item.item)}>
      //               <View style={[styles.storeBtn, styles.addtoCartBtnColor]}>
      //                 <Text style={{ color: '#0080FE',paddingHorizontal: 4}}>ADD</Text>
      //               </View>
      //             </TouchableOpacity>
  
      //           </View>
      //         </View>
      //       </View>
      //   </View>
      // )
          
    // }
  
  
  }

  componentDidMount(){
    
    if (this.props.cartState.cartState) {
      this.setState({paddingBottomHeight: 100});
    }
    console.warn('shopId -->', this.props.navigation.state.params.shopId);
    console.warn('shopId -->', this.props.navigation.state.params.categoryId);
    // BackHandler.addEventListener('basket data', this.props.basketItem.basketItem);
    this.setState({
      shopId: this.props.navigation.state.params.shopId,
      categoryId: this.props.navigation.state.params.categoryId
    })
    // -- API to get shop details --------------------------------------------------------------------- Start
    axios.post(
        Config.URL + Config.getCategoriesByShop,
        {
          "shopId": this.props.navigation.state.params.shopId,
        },
        {
          headers: {
              'Content-Type': 'application/json',
          }
        }
    )
    .then(res => {
        if (res.data.status == 0) {
          this.setState({
            shopData: res.data.result,
            shopName: res.data.result[0].shopName,
            shopImage: res.data.result[0].shopLogo,
            shopDescription: res.data.result[0].shopDescription,
            shopAddress: res.data.result[0].shopAdress,
            shopRating: res.data.result[0].shopRating,  
            categoryId: res.data.result[0].categoryId
        });

        // --- API to get product categories ---- Start------//
        axios.post(
          Config.URL + Config.getShopMenuCategories,
          {
            "categoryId": this.state.categoryId,
          },
          {
              headers: {
                  'Content-Type': 'application/json',
              }
          }
        )
        .then(res => {
          console.warn('category-->', res.data.result);
            if (res.data.status == 0) {
              res.data.result = res.data.result.map( item => {
                item.backGroundColor = ""; 
                return item;
              });

              this.setState({
                distinctCategories: res.data.result
              });
              if (res.data.result.length > 0) {
                this.setState({showCategoryFilter: true})
              }
              
            }
            else if(res.data.status == 1){  // Fail to signup
              // Toast.show(res.data.message,Toast.LONG);
            }
    
        }).catch(err => {
        })

        // --- API to get product categories ---- ENd------//

        // // --- API to get the shop menu ----------------------------------------- Start
        let reqBody = {
          categoryId: this.state.categoryId,
          pageNumber: 1
        }
        // this.getShopMenu(reqBody);
        this.getSubCategories();
        // axios.post(
        //   Config.URL + Config.getShopMenu,
        //   {
        //     "categoryId": this.state.categoryId,
        //   },
        //   {
        //       headers: {
        //           'Content-Type': 'application/json',
        //       }
        //   }
        // )
        // .then(res => {
        //     console.warn("res--- shop menu########",res.data.result);
        //     if (res.data.status == 0) {
        //       this.setState({
        //         shopMenu: res.data.result[0].menu,
              
        //       });

    
        //     }
        //     else if(res.data.status == 1){  // Fail to signup
        //       Toast.show(res.data.message,Toast.LONG);
        //     }
    
        // }).catch(err => {
        //     //console.warn(res.data.status);
        // })
        // // --- API to get the shop menu ------------------------------------- End

        }
        else if(res.data.status == 1){  // Fail to signup
          // Toast.show(res.data.message,Toast.LONG);
        }

      }).catch(err => {
          //console.warn(res.data.status);
      })
      // -- API to get shop details -------------------------------------------------------------- End

  }

  backAndroid = () => {
    return false;
  }

  hideModal = () => {
    this.setState({
      showBasketModal: false,
      modalVisible: false
    })
  }
  
  selectProductCategory = (id) => {
    let index = this.state.distinctCategories.findIndex(ind => ind._id === id);
    this.setState({page: 1, searchText: ""});
    
    let reqBody;
    if(this.state.distinctCategories[index].backGroundColor == '#00CCFF') {
      this.state.distinctCategories.forEach(element => {
        element.backGroundColor = 'white';
      });  
      this.setState({
        selectedCategory: ""
      })
      reqBody = {
        categoryId: this.state.categoryId,
        productCategory: "",
      }    
    } else {
      this.state.distinctCategories.forEach(element => {
        element.backGroundColor = 'white';
      });
      this.setState({
        selectedCategory: this.state.distinctCategories[index]._id
      })
      this.state.distinctCategories[index].backGroundColor = '#00CCFF';
      reqBody = {
        categoryId: this.state.categoryId,
        productCategory: this.state.distinctCategories[index]._id,
      }    
    }

    this.setState({
      distinctCategories: this.state.distinctCategories
    })
    
    this.setState({showLoader: true});
    this.getSubCategories(reqBody);

    
  }

  CategoryFilter = (item) => {
    if (this.state.showCategoryFilter) {
      return (
        <TouchableOpacity style={{borderWidth: 1, borderColor:'#131E3A', padding: 10, marginRight: 5, backgroundColor:item.item.backGroundColor}}
          onPress={() => this.selectProductCategory(item.item._id)}>
          <Text style={[s.normalText]}>{item.item._id}</Text>
        </TouchableOpacity>
      )
    } else {
      return null;
    }
    

  }


  render() {
    const {navigate} = this.props.navigation;
      return (
        <SafeAreaView style={{flex: 1}}>
          <View style={[styles.scrollView,s.body]}>
            {/* <InternateModal /> */}
            <AnimatedLoader
                visible={this.state.showLoader}
                overlayColor="rgba(255,255,255,0.75)"
                source={require("../../../assets/loader.json")}
                animationStyle= {[styles.lottie,{height: 150,width: 150}]}
                speed={1}
              />
            {/* <SceneLoader
              visible={this.state.showLoader}
              animation={{
                  fade: {timing: {duration: 1000 }}
              }}
            /> */}
            <BasketModal visible={this.state.showBasketModal} onDismiss={this.hideModal} navigation= {navigate} basketData= {this.state.addtoBasketItem}
              shopId={this.state.shopId} shopName={this.state.shopName} categoryId={this.state.categoryId} onRequestClose={() => { this.hideModal()}} />  
              
            <ScrollView showsVerticalScrollIndicator = {false} style={{marginBottom: 20}}>
              <View style={styles.storeMainView}>
    
                <View>
                  <View style={styles.advertiseData}>
                    <Image style={styles.advertiseData} source={{uri: this.state.shopImage}}/>
                  </View>
                </View>
                <View style={styles.storeCardLayout}>
                  <Text style={[s.headerText,styles.storeHeading]} >{this.state.shopName}</Text>
                  <Text style={styles.storeDesc} numberOfLines = { 2 }>{this.state.shopDescription}</Text>
                  <Text style={styles.storeDesc} numberOfLines = { 1 }>{this.state.shopAddress}</Text>
                  {/* <View style={{alignItems:"center"}}> */}
                    <Rating imageSize={20} readonly startingValue={this.state.shopRating} />
                  {/* </View> */}
                </View>
                <View style={styles.sectionContainer}>
                  <TextInput placeholder = "Search product...."
                    style={styles.searchShops} inlineImageLeft='searchicon' 
                    onChangeText={(searchText) => this.searchData(searchText)}/>
                </View>
                {this.state.showCategoryFilter && !this.state.showList ? 
                <View style={{backgroundColor:"#DCF0FA",elevation: 4,paddingHorizontal: 15, marginVertical: 10}}>
                  <Text style={[s.subHeaderText,{textAlign:"center"}]}>Sort by</Text> 
                  <ScrollView style={{ paddingVertical: 5}}>
                  <FlatList data={this.state.distinctCategories}
                    renderItem={( { item } ) => (
                      <this.CategoryFilter
                        item={item}
                      />
                    )}
                    keyExtractor= {item => item._id}
                    extraData={this.state}
                    horizontal={true}
                    showsHorizontalScrollIndicator = {false}
                  />
                </ScrollView>
                </View>
                : null
                }
                {this.state.showList ? 
                  <View style={styles.storeDtlContainer}>
                    <FlatList
                      data={this.state.shopMenu}
                      renderItem={( { item } ) => (
                        <this.ProductListing
                        item= {item}
                        />
                      )}
                      keyExtractor= {item => item.productId}
                      extraData={this.state.shopMenu}
                    />
                    
                  </View>
                  : null
                }

                {!this.state.showList ?
               <View style={[styles.storeDtlContainer]} >
                  <FlatList
                    data={this.state.subCategoryList}
                    renderItem={( { item, index } ) => (
                      <SubCategoryListView
                        item= {item}
                        self={this}
                      />
                    )}
                    keyExtractor= {item => item.categoryId}
                    extraData={this.state.subCategoryList}
                    numColumns={3}
                    // onEndReached={this.handleLoadMore}
                    // onEndReachedThreshold={0.01}
                    // initialNumToRender={30}
                    // onMomentumScrollBegin={this.moment}
                    // onMomentumScrollEnd ={this.handleLoadMore}
                  /> 
                  
                </View>
              : null
              }
                {this.state.showList ?
                  <View style={{flexDirection: "row", flex: 1, paddingVertical: 10, paddingBottom: this.state.paddingBottomHeight}}>
                    
                    <TouchableOpacity style={{alignItems:"flex-start", paddingLeft: 15, flex: 3}} disabled={this.state.prev}
                      onPress={() => this.handlePagination('prev')}>
                      <Text style={{color: this.state.prevColor, fontSize: 14, fontFamily: "verdana"}}>Previous</Text>
                    </TouchableOpacity>
                    
                    <View style={{flex: 6, alignItems:"center"}}>
                      <Text style={{color: "#00CCFF", fontSize: 14, fontFamily: "verdana"}}>{this.state.page}</Text>
                    </View>
                    <TouchableOpacity style={{alignItems:"flex-end", paddingRight: 15, flex: 3, }} disabled={this.state.next}
                      onPress={() => this.handlePagination('next')}>
                      <Text style={{color: this.state.nextColor, fontSize: 14, fontFamily: "verdana"}}>Next</Text>
                    </TouchableOpacity>

                  </View>
                : null  
                }
              </View>
            </ScrollView>
            <CartModal navigate={navigate} navigationPage={'orderDetailsPage'}></CartModal>
            <OnGoingActivityModal navigate={navigate}></OnGoingActivityModal>
            <PopupModal visible={this.state.modalVisible} onDismiss={this.hideModal} removeProduct={this.userAckCart} 
              message={"Delivery charges may vary on shopping from different shops. Do you want to continue?"} 
              btnTag={"Add to cart"}/>
            {/* <NavigationBar navigate={navigate}></NavigationBar>        */}
          </View>
        </SafeAreaView>
        
      )
   
  }

  handlePagination = (type) => {
    this.setState({showLoader: true, showList: false});
    if(type == 'next') {
      const reqBody = {
        categoryId: this.state.categoryId,
        productCategory: this.state.selectedCategory,
        searchData: this.state.searchText,
        pageNumber: this.state.page + 1
      }

      this.setState({page: this.state.page + 1});
      this.getShopMenu(reqBody);
    } else {
      const reqBody = {
        categoryId: this.state.categoryId,
        productCategory: this.state.selectedCategory,
        searchData: this.state.searchText,
        pageNumber: this.state.page - 1
      }
      this.setState({page: this.state.page - 1});
      this.getShopMenu(reqBody);
    }
    
  }

  //function to add the new item in the cart   --- Start
  addCart = (productData) => {
    const cart = {
      "productId": productData.productId,
      "productName": productData.productName,
      "productDescription": productData.productDescription,
      "productLogo": productData.productLogo,
      "unitCost": parseFloat(productData.unitCost),
      "unit": productData.unit,
      "shopId": this.state.shopId,
      "shopName": this.state.shopName,
      "category": this.state.categoryId,
      "counter": 1,
      "totalCost": parseFloat(productData.unitCost),  
      "isProductAvailable": true
    }

    this.setState({
      cart: cart 
    })


    if(this.props.cartItem.cartItem.some(arr => arr.productId == productData.productId)){
      Toast.show("Already added in the cart",Toast.LONG);
    }
    else if (!this.props.cartItem.cartItem.some(arr => arr.shopId == this.state.shopId)) {
      if (this.props.cartItem.cartItem.length > 0) {
        this.setState({
          modalVisible: true
        })
      } else {
        this.confirmAddCart(cart);
      }
    }
    else{
      this.confirmAddCart(cart);
    }
  }


  //function to add the new item in the cart   --- End


  confirmAddCart = (cart) => {
    this.props.cartState.cartCount += 1;
    this.props.cartState.cartSum = (parseFloat(this.props.cartState.cartSum) + parseFloat(cart.unitCost)); 
    this.props.cartModalAction.changeCartState(true,this.props.cartState.cartSum,this.props.cartState.cartCount);
    this.props.actions.addToCart(cart);
    this.setState({paddingBottomHeight: 100});
    Toast.show(cart.productName + " is added in the cart",Toast.LONG);

  }

  userAckCart = () => {
    this.setState({
      modalVisible: false
    })
    this.confirmAddCart(this.state.cart);

  }

  
  //function to add the new item in the basket   --- Start
  showBasketModal = (basketData) => {
    this.setState({
      showBasketModal: true,
      addtoBasketItem: basketData
    })
  }
  //function to add the new item in the basket   --- End

   //function to search product or shop   --- Start
  //  searchData = (searchText) => {
    
  //   this.setState({
  //       searchText: searchText
  //   });

  //   if(searchText == ""){
  //     var searchObj = {
  //       "categoryId": this.state.categoryId
  //     }
  //   }
  //   else{
  //     var searchObj = {
  //       "searchData": searchText,
  //       "categoryId": this.state.categoryId
  //     }
  //   }
  //   console.warn("searchObj",searchObj,searchText);

  //   axios.post(
  //       Config.URL + Config.getShopMenu,
  //       searchObj,
  //       {
  //         headers: {
  //           'Content-Type': 'application/json',
  //         }
  //       }
  //   )
  //   .then(res => {
        
  //       if (res.data.status == 0) {
  //         console.warn("res-----123",res.data.result,this.state.searchText);
  //         this.setState({
  //           shopMenu: res.data.result
  //         });
  //       }
  //       else if(res.data.status == 1){  // Fail to signup
  //         Toast.show(res.data.message,Toast.LONG);
  //       }
  
  //     }).catch(err => {
  //         //console.warn(res.data.status);
  //     })
  
  // }
  //function to search product or shop   --- End

  searchData = (searchText) => {
    this.setState({searchText: searchText, page: 1});

    if(searchText == ""){
      var searchObj = {
        "categoryId": this.state.categoryId,
        "productCategory": this.state.selectedCategory,
        "searchData": "",
        "pageNumber": this.state.page
      }
    }
    else{
      var searchObj = {
        "searchData": searchText,
        "categoryId": this.state.categoryId,
        "productCategory": this.state.selectedCategory,
        "pageNumber": this.state.page
      }
    }
    this.getShopMenu(searchObj);
    
  }

  
  getShopMenu = (reqBody) => {
    console.warn('request body--->', reqBody);
    axios.post(
      Config.URL + Config.getShopMenu,
      reqBody,
      {
          headers: {
              'Content-Type': 'application/json',
          }
      }
    )
    .then(res => {
      this.setState({showLoader: false});
        
        if (res.data.status == 0) {
          console.warn('data---', res);
          
            // this.setState({loadData: true});
            if(reqBody.searchData) {
              this.setState({
                shopMenu: res.data.result,
                showList: true
              })
            } else if (reqBody.productCategory) {
              this.setState({
                shopMenu: res.data.result,
                showList: true
              })
            } else {
              console.warn('in main');
              this.setState({
                shopMenu: res.data.result,
                showList: true
              })
            }


          if (this.state.shopMenu.length <= 0) {
            this.setState ({
              isShopAvailable: false
            })
          } else {
            this.setState ({
              page: this.state.page,
              maxCount: res.data.totalRecord,
              pageCount: res.data.limit,
              totalPage: res.data.totalPage,
              
            })
          }
          console.warn('this.state.page', this.state.page, res.data.totalRecord, res.data.totalPage);
          
          if (res.data.totalPage == 1) {
            this.setState({prev: true, next: true, prevColor: "silver", nextColor: "silver"})
          } else if (this.state.page == 1) {
            this.setState({prev: true, next: false, prevColor: "silver", nextColor: "#00CCFF"})
          } else if(this.state.page == res.data.totalPage) {
            this.setState({prev: false, next: true, prevColor: "#00CCFF", nextColor: "#00CCFF"})
          } else {
            this.setState({prev: false, next: false, prevColor: "#00CCFF", nextColor: "#00CCFF"})
          }
        } else if(res.data.status == 1){  // Fail to signup
          console.warn('response get menu-->', res.data);
          Toast.show(res.data.message,Toast.LONG);
        }

    }).catch(err => {
    })
  }


}


let ScreenWidth = Dimensions.get("window").width;
let ScreenHeight = Dimensions.get("window").height;

const styles = StyleSheet.create({
  scrollView: {
    flex: 1
  },
  imgBlank: {
    height: ScreenHeight,
    width: ScreenWidth
  },
  containerIcons:{
    width:30,
    height:30,
  },

  searchShops:{ 
    height: 40, 
    borderColor: '#7285A5', 
    borderWidth: 1,
    paddingHorizontal:10, 
    borderRadius:4 
  },
  sectionContainer: {
    marginTop: 20,
    paddingHorizontal: 24,
  },
  iconView: {
    width: 30,
    height: 30,
    marginRight: 20,
    borderRadius: 100,
  },
  storeCardLayout:{
    marginHorizontal: 15,
    paddingVertical:14,
    borderBottomWidth: 2,
    borderBottomColor: "#131E3A",
    alignItems: "center"
  },
  categoryFooter:{
    width:ScreenWidth,
    paddingBottom:20,
    height:80,
    flexGrow:0.05,
    backgroundColor:'white',
    display:'flex',
    flexDirection:'row',
    justifyContent:"space-evenly",
    alignItems:"center"
  },
  storeDtlContainer: {
    paddingVertical: 10
  },

  storeContainer:{
    display:"flex",
    flexDirection: "row"
  },
  stores: {
    paddingHorizontal: 10,
    flex: 8,
    flexDirection: "column"
  },

  storeCount: {
    fontSize: 22,
    fontWeight: 'bold',
    paddingHorizontal: 10,
  },
  storeProductHeading: {
    fontWeight: 'bold',
    fontSize: 30,
  },
  shopdtlCards: {
    width: 80,
    height: 80,
    backgroundColor: 'orange',
    borderRadius: 100,
    borderWidth: 0.5,
    borderColor: 'black',
    marginVertical: 5,
    marginHorizontal: 10,
  },
  storeCardDisplay: {
    // padding: 10,
    paddingVertical: 10,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  storeCards: {
    height: 100,
    borderRadius: 4,
    borderWidth: 0.5,
    borderColor: 'black',
    backgroundColor: 'white',
    flex: 3,
  },
  advertiseData: {
    height: 200,
    width: ScreenWidth,
    borderBottomLeftRadius: 90,
    borderBottomRightRadius: 90,
    elevation: 4
  },
  storeHeading: {
    fontSize:24,
    
  },
  storeDesc: {
    alignItems: "center"
  },
  trendingShops: {
    borderBottomWidth: 2,
    borderColor: "#131E3A",
    marginHorizontal: 15,
    paddingTop: 10,
  },
  addCardImage: {
    width: 80,
    height: 80,
    flex: 4
  },
  productImage: {
    width: 80,
    height: 80,
  },
  storeCardButton: {
    backgroundColor: 'white',
    color: 'black',
    width: 30,
    height: 30,
    borderRadius: 50,
    alignItems: "center",
    justifyContent: 'center',
  },
  storeborder: {
    borderBottomColor: 'grey',
    borderBottomWidth: 1,
  },
  flexRow: {
    flex: 5,
    textAlignVertical: "bottom",
    flexDirection: 'row',
    alignItems:"flex-end",
     paddingRight: 10
  },
  prodcutListingView: {
    flexDirection: 'row',
    marginVertical: 5,
    backgroundColor: "white",
    padding: 10,
    elevation: 4

  },
  flexColumnAlignEnd: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-end'
  },
  storeDtlCenter: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "center"
  },
  Mystores: {
    flexDirection: "row",
    alignItems: "flex-start",
    justifyContent: "space-between",
    textAlignVertical: "bottom",
    flex: 2,
    marginTop: 5
    // paddingBottom:15,
  },
  storeCardSpace: {
    marginBottom: 15,
  },
  headerstyle: {
    paddingBottom: 20,
    paddingTop: 20
  },

  storeBtnViewColor: {
    backgroundColor: '#ba55d3',
    color: 'white',
  },
  continueBtnViewColor: {
    backgroundColor: '#2f4f4f',
    color: 'white',
  },
  addtoCartBtnColor: {
    fontSize:12,
    borderWidth: 1,
    borderColor: '#0080FE',
    marginLeft: 20,
  },
  storeBtn: {
    paddingVertical: 5,
    paddingHorizontal: 5,
    marginBottom: 5,
    color: 'white',
    borderRadius: 0,
    width: 50
  },
  basketBtn:{
    paddingVertical: 5,
    paddingHorizontal: 10,
    marginBottom: 10,
    borderRadius: 20,
    height: 30,
    width: 30
  },
  basketImg: {
    height: 30,
    width: 30,
  }

});

//--------------------------------------------------- store related fuction  -- Start

const mapStateToProps = (state) => {
	return {
    cartItem: state.cartItem,
    basketItem: state.basketItem,
    cartState: state.cartState,
	}
}

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(addToCart, dispatch),
        basketAction: bindActionCreators(basketAction,dispatch),
        cartModalAction: bindActionCreators(cartModalAction,dispatch),
        navigationAction: bindActionCreators(navigateAction,dispatch),
    };
}

// ---------------------------------------------------- store related function  -- END


export default connect(mapStateToProps, mapDispatchToProps)(ShopDetailPage);


