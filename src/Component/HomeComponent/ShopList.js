/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Fragment } from 'react';
import axios from 'axios';
import * as Config from '../../Constant/Config.js';
import * as Constant from '../../Constant/Constant.js';
import  NavigationBar  from '../ReusableComponent/NavigationBar.js';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Rating } from 'react-native-ratings';
import  s  from '../../sharedStyle.js';
import CartModal from '../ReusableComponent/cartModal';
import InternateModal from '../ReusableComponent/InternetModal';
import * as currentLocation  from '../../Actions/CurrentLocationAction';
import Toast from 'react-native-simple-toast';
import AnimatedLoader from "react-native-animated-loader";
import { SliderBox } from 'react-native-image-slider-box';
import DelayInput from "react-native-debounce-input";
// import 'bootstrap/dist/css/bootstrap.min.css';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    Dimensions,
    TouchableOpacity,
    Button,
    TextInput,
    StatusBar,
    TouchableWithoutFeedback,
    Image,
    FlatList,
    BackHandler
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import * as navigateAction  from '../../Actions/NavigationBarAction';

function ShopListing({shopId,shopName,shopDescription,logo,address, navigation}) {
    return (
      
        <View style={styles.shopContainer}>
            <View style={styles.flexRowSpaceBetween}>
                <View style={styles.imageCardView}>
                  <Image style={styles.imageCards} source={{uri: logo}}></Image>
                </View>
                <View style={{ flex: 0.2 }} />
                <TouchableOpacity style={styles.storeCards} onPress={() => navigation.navigate('flashHubPage',{"shopId": shopId})}>
                    <View style={styles.bookCardLayout}>
                        <Text style={s.subHeaderText} numberOfLines = { 3 }>{shopName}</Text>
                        <Text style={s.normalText} numberOfLines = { 1 }>{shopDescription}</Text>
                        <Text style={s.normalText} numberOfLines = { 1 }>{address}</Text>
                        
                    </View>
                </TouchableOpacity>
            </View>
        </View>
    )
  
}


class ShopList extends React.Component{
    constructor(props) {
        super(props);
        this.state={
          data: [],
          searchText: "",
          categoryId: "",
          isShopAvailable: true,
          showLoader: false,
          isLinked: false,
          isBlanked: false
        }
    }

    componentDidMount(){
      console.warn('this.props.navigation.state.params.categoryId',this.props.navigation.state.params.categoryId);
      
        this.setState({
          categoryId: this.props.navigation.state.params.categoryId,
          showLoader: true
        });
        console.warn("cat id---",this.props.currentLocation.currentRegion);


        axios.post(
            Config.URL + Config.getCategoriesByShop,
            {
              "categoryId": this.props.navigation.state.params.categoryId,
              "position": this.props.currentLocation.currentRegion
            },
            {
                headers: {
                    'Content-Type': 'application/json',
                }
            }
        )
        .then(res => {
            console.warn("res---",res);
            if (res.data.status == 0) {
              this.setState({
                data: res.data.result,
                showLoader: false
              });

              if (res.data.isLinked) {
                this.setState({isLinked: true})
              }

              if (this.state.data.length <= 0) {
                this.setState({
                  isBlanked: true
                })
              }
            }
            else if(res.data.status == 1){
              this.setState({
                isBlanked: true,
                showLoader: false
              })
            } else {
              this.setState({
                isBlanked: true,
                showLoader: false
              })
            }
  
        }).catch(err => {
            //console.warn(res.data.status);
            this.setState({showLoader: false});
        })
  
    }


    OfferList = (item) => {
      return (
        <View style={styles.sectionContainer}>
            <View style={styles.advertiseData}>
              <Image style={styles.imageView} source={{uri: item.item.imageURL}}/>
          </View>
        </View> 
      )
    
    }

    ShopNotAvailable = () => {
      const {navigate} = this.props.navigation;
      if (!this.state.isShopAvailable) {
        if (this.state.isLinked) {
          return (
            <View style={{flex: 1,alignItems:"center", justifyContent:"center", alignContent:"center", marginTop: 40, marginHorizontal: 20}}>
                <Text style={[s.normalText, {textAlign:"center"}]} numberOfLines={3}>Sorry for the inconvenience!</Text>
                <Text style={[s.normalText, {textAlign:"center"}]} numberOfLines={3}>The shop that you have searched for is not tied with Flash at the moment</Text>
                <Text style={[s.normalText, {textAlign:"center"}]} numberOfLines={3}>Please search your needs from Flash Mart</Text>
                <View style={{flex: 1}}>
                  {/* <TouchableOpacity style={{flex: 6, alignItems:"flex-start"}} onPress={() => this.searchShop('')}>
                    <Text style={[s.normalText, {color: "#00CCFF", paddingTop: 15}]}>Clear search</Text>
                  </TouchableOpacity> */}
                  <TouchableOpacity style={{flex: 1, alignItems:"flex-end"}} onPress={() => navigate('flashHubPage')}>
                    <Text style={[s.normalText, {color: "#00CCFF", paddingTop: 15}]}>Flash Mart ></Text>
                  </TouchableOpacity>
                </View>
                
                {/* <Image style={styles.imgBlank} source={require('../../../assets/ShopMissing.jpg')}/> */}
              </View>
          )
        } else {
          return (
            <View style={{flex: 1,alignItems:"center", justifyContent:"center", alignContent:"center", marginTop: 40, marginHorizontal: 20}}>
                <Text style={[s.normalText, {textAlign:"center"}]} numberOfLines={3}>Sorry for the inconvenience!</Text>
                <Text style={[s.normalText, {textAlign:"center"}]} numberOfLines={3}>The shop that you have searched for is not tied with Flash at the moment</Text>
                <Text style={[s.normalText, {textAlign:"center"}]} numberOfLines={3}>Please search your needs from Flash Mart</Text>
                {/* <View style={{flex: 1, alignItems: "center"}}>
                  <TouchableOpacity onPress={() => this.searchShop('')}>
                    <Text style={[s.normalText, {color: "#00CCFF", paddingTop: 15}]}>Clear search</Text>
                  </TouchableOpacity> */}
                  {/* <TouchableOpacity style={{flex: 6, alignItems:"flex-end"}} onPress={() => navigate('flashHubPage')}>
                    <Text style={[s.normalText, {color: "#00CCFF", paddingTop: 15}]}>Flash Mart ></Text>
                  </TouchableOpacity> */}
                {/* </View> */}
                
                {/* <Image style={styles.imgBlank} source={require('../../../assets/ShopMissing.jpg')}/> */}
              </View>
          )
        }
        
      } else if (this.state.isBlanked) {
        if (this.state.isLinked) {
          return (
            <View style={{flex: 1,alignItems:"center", justifyContent:"center", alignContent:"center", marginTop: 40, marginHorizontal: 20}}>
                <Text style={[s.normalText, {textAlign:"center"}]} numberOfLines={3}>Sorry for the inconvenience!</Text>
                <Text style={[s.normalText, {textAlign:"center"}]} numberOfLines={3}>The shops for this category are not tied with Flash at the moment</Text>
                <Text style={[s.normalText, {textAlign:"center"}]} numberOfLines={3}>Please search your needs from Flash Mart</Text>
                <View style={{flex: 1, flexDirection: "row"}}>
                  <TouchableOpacity style={{flex: 6, alignItems:"flex-start"}} onPress={() => this.searchShop('')}>
                    <Text style={[s.normalText, {color: "#00CCFF", paddingTop: 15}]}>Clear search</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={{flex: 6, alignItems:"flex-end"}} onPress={() => navigate('flashHubPage')}>
                    <Text style={[s.normalText, {color: "#00CCFF", paddingTop: 15}]}>Flash Mart ></Text>
                  </TouchableOpacity>
                </View>
                
                {/* <Image style={styles.imgBlank} source={require('../../../assets/ShopMissing.jpg')}/> */}
              </View>
          )
        } else {
          return (
            <View style={{flex: 1,alignItems:"center", justifyContent:"center", alignContent:"center", marginTop: 40, marginHorizontal: 20}}>
                <Text style={[s.normalText, {textAlign:"center"}]} numberOfLines={3}>Sorry for the inconvenience!</Text>
                <Text style={[s.normalText, {textAlign:"center"}]} numberOfLines={3}>The shops for this category are not tied with Flash at the moment</Text>
                <Text style={[s.normalText, {textAlign:"center"}]} numberOfLines={3}>Please search your needs from Flash Mart</Text>
                <View style={{flex: 1, alignItems: "center"}}>
                  <TouchableOpacity onPress={() => this.searchShop('')}>
                    <Text style={[s.normalText, {color: "#00CCFF", paddingTop: 15}]}>Clear search</Text>
                  </TouchableOpacity>
                  {/* <TouchableOpacity style={{flex: 6, alignItems:"flex-end"}} onPress={() => navigate('flashHubPage')}>
                    <Text style={[s.normalText, {color: "#00CCFF", paddingTop: 15}]}>Flash Mart ></Text>
                  </TouchableOpacity> */}
                </View>
                
                {/* <Image style={styles.imgBlank} source={require('../../../assets/ShopMissing.jpg')}/> */}
              </View>
          )
        }
      }
       else {
        return null;
      }
    }

    render() {
        const {navigate} = this.props.navigation;
        return (
          <SafeAreaView style={{flex: 1}}>
            <View style={[s.bodyGray]}>
              {/* <InternateModal /> */}
              <AnimatedLoader
                visible={this.state.showLoader}
                overlayColor="rgba(255,255,255,0.75)"
                source={require("../../../assets/loader.json")}
                animationStyle= {[styles.lottie,{height: 150,width: 150}]}
                speed={1}
              />
              <ScrollView contentInsetAdjustmentBehavior="automatic" showsVerticalScrollIndicator = {false}>
                
                <SliderBox   
                  images={this.props.userData.topSlider}   
                  // sliderBoxHeight={200}
                  renderItem={( { item } ) => (
                    <this.OfferList
                      item={item}
                    />
                  )}
                  dotColor="#00CCFF"   
                  inactiveDotColor="white"   
                  paginationBoxVerticalPadding={20}   
                  autoplay   
                  dotStyle={{width: 8,height: 8,borderRadius: 4,marginHorizontal: 0,padding: 0,margin: 0,}}
                  circleLoop />
                <View style={{marginHorizontal: 15}}>
                  {/* <TextInput placeholder = "Search store names...."
                    value={this.state.searchText}
                    style={styles.searchShops} inlineImageLeft='searchicon' 
                    onChangeText={(searchText) => this.searchShop(searchText)}/> */}
                  <DelayInput placeholder = "Search store names...."
                    delayTimeout={500}
                    minLength={2}
                    value={this.state.searchText}
                    style={styles.searchShops} inlineImageLeft='searchicon' 
                    onChangeText={(searchText) => this.searchShop(searchText)}/>
                </View>
                <FlatList data={this.state.data}
                    renderItem={( { item } ) => (
                        <ShopListing
                        shopId={item.shopId}
                        shopName={item.shopName}
                        shopDescription={item.shopDescription}
                        logo={item.shopMiniLogo}
                        address={item.shopAdress}
                        navigation={this.props.navigation}
                        />
                    )}
                    keyExtractor= {item => item.shopId}
                    horizontal={false}
                    extraData={this.state.data}
                    
                  />
                  <this.ShopNotAvailable />
                
              </ScrollView>
              {/* <CartModal navigate={navigate}></CartModal> */}
              {/* <NavigationBar navigate={navigate}></NavigationBar>     */}
            </View>
        
          </SafeAreaView>
            
      );
        
  }

   //function to add the new item in the cart   --- Start
  searchShop = async(searchText) => {
    console.warn('search called');
    await this.setState({searchText: searchText});
    let reqBody;
    if (searchText == '') {
      reqBody = {
        "categoryId": this.state.categoryId,
        "position": this.props.currentLocation.currentRegion,
      };
    } else {
      reqBody = {
        "categoryId": this.state.categoryId,
        "position": this.props.currentLocation.currentRegion,
        "searchData": searchText
      };
    }
    console.warn('reqBody--->', reqBody);
    try {
      axios.post(
        Config.URL + Config.getCategoriesByShop,
        reqBody,
        {
          headers: {
              'Content-Type': 'application/json',
          }
        }
      )
      .then(res => {
        // this.setState({showLoader: false});
        console.warn('--------response-->', res.data.result.length);
          if (res.data.status == 0) {
            this.setState({
              data: res.data.result,
              isShopAvailable: true
            });
            if (res.data.result.length <= 0) {
              this.setState({isShopAvailable: false});
            }
          }
          else if(res.data.status == 1){ 
            this.setState({isShopAvailable: false, showLoader: false});
          }
  
        }).catch(err => {
            //console.warn(res.data.status);
            this.setState({showLoader: false});
        })

    } catch(err) {
      console.warn('err', err);
    }
    

  }
    //function to add the new item in the cart   --- End

}



let ScreenHeight = Dimensions.get("window").height;
let ScreenWidth = Dimensions.get("window").width;
const styles = StyleSheet.create({

  imgBlank: {
    height: ScreenHeight - 80,
    width: ScreenWidth
  },

  shopContainer: {
    marginTop: 10,
    paddingHorizontal: 20,
    alignItems: "center",
    justifyContent: "center",
    alignContent: "center",
    flex: 1,
    elevation: 6
  },
  sectionContainer: {
    marginTop: 15,
    marginBottom: 40,
    paddingHorizontal: 20,
    alignItems: "center",
    justifyContent: "center",
    alignContent: "center",
    flex: 1,
    elevation: 6   
  },
  advertiseData: {
    height:140,
    width: ScreenWidth - 30,
    borderRadius: 10
  },
  imageView: {
    flex: 1,
    width: undefined,
    height: undefined,
    resizeMode: 'contain',
    borderRadius: 6
  },
  advertiseImage: {
    height:150,
    width: ScreenWidth - 40,
    borderRadius: 10,
  },
  searchShops:{ 
    height: 40, 
    borderColor: '#7285A5', 
    borderWidth: 1,
    paddingHorizontal:10, 
    borderRadius:4,
    elevation: 10,
    backgroundColor:"white"
},
iconView: {
    width: 30,
    height: 30,
    marginRight: 20,
    borderRadius: 100,
},
  containerIcons:{
    width:30,
    height:30,
  },
  categoryFooter:{
    width:ScreenWidth,
    paddingBottom:20,
    height:80,
    flexGrow:0.05,
    backgroundColor:'lightgray',
    display:'flex',
    flexDirection:'row',
    justifyContent:"space-evenly",
    alignItems:"center"
  },
  storeHeadinglbl:{ 
    color: 'white',
    fontSize:16 
  },
  storeHeadingView:{
    display:'flex',
    flexDirection:'row',
    justifyContent:'space-between',
    padding:10
  },
  StoreCardsLayout:{ 
    flex: 1, 
    flexDirection: 'row', 
  justifyContent: 'space-between' 
},
  materialTypeHeading:{ 
    color: 'black',
    fontSize:18,
    fontWeight: "bold", 
    paddingBottom:10 
},
  categoryHeadingTxt:{ 
    color: 'white',
    fontSize:16,
    textAlign:'center' 
},
  flexRowSpaceBetween:{ 
    display: 'flex', 
    flexDirection: 'row', 
    justifyContent: 'space-between' 
  },
  bookCardLayout:{
    padding:10,
    display:'flex',
    flexDirection:'column',
    elevation: 4
  },
  categoryHeadingView:{
    borderBottomColor: 'lightgray',
    borderBottomWidth: 1,
    padding:10,
    textAlign:'center',
  },



  imageCardView: {
    height: 80,
    borderRadius: 4,
    flex: 1,
  },
  imageCards: {
    height: 80,
    borderWidth: 1,
    borderRadius: 4,
    flex: 1
  },
  offerCards:{
    width: 130,
    height: 130,
    backgroundColor:'orange',
    borderRadius: 100,
    borderWidth: 0.5,
    borderColor: 'black',
    marginVertical:10,
    marginHorizontal:10,
  },
  offerCardDisplay:{
    padding:10,
    display:'flex',
    flexDirection:'column',
    alignItems:'center',
  },
  storeCards: {
    borderRadius: 4,
    borderWidth: 1,
    borderColor: '#7285A5',
    backgroundColor: 'white',
    flex: 3,
    elevation: 4
  },
  productHeader: {
    height: 100,
    borderColor: 'black',
  },
  storeContainer: {
    borderRadius: 4,
    borderWidth: 1,
    borderColor: 'black',
    backgroundColor: 'navy',
  },
  sectionTitle: {
    borderRadius: 4,
    borderWidth: 0.5,
    borderColor: '#d6d7da',
    // fontSize: 24,
    // fontWeight: '600',
    color: Colors.black,
    // borderStyle:1px solid red
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
});

const mapStateToProps = (state) => {
  return {
    currentLocation: state.currentLocation,
    userData: state.loginData,
  }
}

const mapDispatchToProps = (dispatch) => {
    return {
        navigationAction: bindActionCreators(navigateAction,dispatch),
    };
}

// ---------------------------------------------------- store related function  -- END


export default connect(mapStateToProps, mapDispatchToProps)(ShopList);