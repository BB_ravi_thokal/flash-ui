
import React, { Component } from 'react';

import InternateModal from '../ReusableComponent/InternetModal';
import Toast from 'react-native-simple-toast';
import {
    Header,
    LearnMoreLinks,
    Colors,
    DebugInstructions,
    ReloadInstructions,
  } from 'react-native/Libraries/NewAppScreen';
// import 'bootstrap/dist/css/bootstrap.min.css';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    Dimensions,
    TouchableOpacity,
    Image,
} from 'react-native';

class ProductListing extends Component {
    constructor(props) {
        super(props);
        this.state={
            cartItem: []
        }
        console.warn("----------",this.props.dependent);

    }
    render() {
        const data = this.props.dependent;
        const shopId = this.props.shopId;
        const categoryId = this.props.categoryId;
        //const cartItem = this.props.cartItem;

        return (
            // <View>
            //     <Text>jhbsbdjhbsdd</Text>
            // </View>
            <SafeAreaView style={{flex: 1}}> 
              <View style={[styles.storeTitles, styles.storeCategoryBox]}>
                {/* <InternateModal /> */}
                <View style={[styles.storeList]}>
                  <View style={[styles.storeCardPadding]}>
                    <View style={[styles.flexRow,styles.categoryWidth]}>
                      <View style={styles.addCardImage}>
                        <Image  source={require('../../../assets/bookIcon.jpg')}/>
                      </View>
                        <View style={styles.stores}>
                          <Text style={styles.storeProductName} numberOfLines={1}>{data.productName} </Text>
                          <View style={styles.Mystores}>
                            <View>
                              <Text style={[styles.storePriceText, styles.priceAlign]} numberOfLines={1}>{data.unitCost}/{data.unit}</Text>
                            </View>
                            <View style={styles.flexRow}>
                              <View style={styles.basketBtn}>
                                <Image style={styles.basketImg} source={require('../../../assets/basketIcon.png')}/>
                              </View>
                              <TouchableOpacity onPress={() => this.addCart(data.productId,shopId,categoryId)}>
                                <View style={[styles.storeBtn, styles.addtoCartBtnColor]}>
                                  <Text style={{ color: 'white' }}>ADD</Text>
                                </View>
                              </TouchableOpacity>
          
                            </View>
                          </View>
                        </View>
                    </View>
                  </View>
                </View>
              </View>
          
            </SafeAreaView>
        
        )
    }

    addCart = (productId,shopId,categoryId) => {
        console.warn("------------------",productId,shopId,categoryId)
        //console.log("productId,shopId,categoryId",productId,shopId,categoryId);
        const cart = {
          "productId": productId,
          "shopId": shopId,
          "category": categoryId  
        }
        console.warn("cart",cart);
        const temp = this.state.cartItem;
        console.warn("first",temp);
        temp.push(cart);
    
        this.setState({
          cartItem: temp
        })
    
        console.warn("cartItem -----",this.state.cartItem);
    }
}

let ScreenWidth = Dimensions.get("window").width;
let ScreenHeight = Dimensions.get("window").height;

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,
    height: ScreenHeight,
    marginBottom: 40,
  },
  containerIcons:{
    width:30,
    height:30,
  },
  body: {
    backgroundColor: 'grey',
  },
  mainView: {
    backgroundColor: 'white',
    // height: ScreenHeight
  },
  iconView: {
    width: 30,
    height: 30,
    marginRight: 20,
    borderRadius: 100,
  },
  storeCardLayout:{
    marginHorizontal: 15,
    paddingVertical:14,
    borderBottomWidth: 2,
    borderColor: 'lightgray',
  },
  categoryFooter:{
    width:ScreenWidth,
    paddingBottom:20,
    height:80,
    flexGrow:0.05,
    backgroundColor:'white',
    display:'flex',
    flexDirection:'row',
    justifyContent:"space-evenly",
    alignItems:"center"
  },
  storeDtlContainer: {
    // height: ScreenHeight - 200,
    marginHorizontal: 15,
    paddingVertical: 10
  },

  storeContainer:{
    display:"flex",
    flexDirection: "row"
  },
  stores: {
    paddingHorizontal: 10,
    flex: 10
    // width:"85%"
  },
  storeTitles: {
    // marginHorizontal: 15,
    // paddingLeft: 20,
    marginBottom: 20,
    paddingVertical: 10,
  },
  storeCount: {
    fontSize: 22,
    fontWeight: 'bold',
    paddingHorizontal: 10,
  },
  storeProductName: {
    fontWeight: 'bold',
    fontSize: 15
  },
  storePriceText: {
    color: '#3cb371',
    fontWeight: 'bold'
  },
  storeProductHeading: {
    fontWeight: 'bold',
    fontSize: 30,
  },
  shopdtlCards: {
    width: 80,
    height: 80,
    backgroundColor: 'orange',
    borderRadius: 100,
    borderWidth: 0.5,
    borderColor: 'black',
    marginVertical: 5,
    marginHorizontal: 10,
  },
  storeCardDisplay: {
    // padding: 10,
    paddingVertical: 10,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  storeCards: {
    height: 100,
    borderRadius: 4,
    borderWidth: 0.5,
    borderColor: 'black',
    backgroundColor: 'white',
    flex: 3,
  },
  advertiseData: {
    height: 200,

  },
  storeHeading: {
    color: 'black',
    fontSize: 18,
    fontWeight: "bold"
  },
  storeDesc: {
    color: 'gray',
    fontSize: 16
  },
  trendingShops: {
    borderBottomWidth: 2,
    borderColor: 'lightgray',
    // borderRadius: 4,
    marginHorizontal: 15,
    paddingTop: 10,
    // paddingLeft:10,
    backgroundColor: 'white'
    // padding: 15,
    // margin: 10,
  },
  addCardImage: {
    width: 50,
    height: 50,
    borderRadius: 5,
    flex: 2
  },
  productImage:{
    width: 50,
    height: 50,
    borderRadius: 5,
    flex: 2
  },
  storeCardButton: {
    backgroundColor: 'white',
    color: 'black',
    width: 30,
    height: 30,
    borderRadius: 50,
    alignItems: "center",
    justifyContent: 'center',
  },
  storeList: {
    display: "flex",
    flexDirection: "column",
    justifyContent:"center",
    // paddingVertical: 10,
    // marginHorizontal:15
  },
  storeborder: {
    borderBottomColor: 'grey',
    borderBottomWidth: 1,
  },
  flexRow: {
    //flex: 1,
    flexDirection: 'row',
  },
  categoryWidth:{
    paddingVertical: 10
  },
  flexColumnAlignEnd: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-end'
  },
  priceAlign: {
    paddingRight: 5,
    // paddingTop: 5,
  },
  storeDtlCenter: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "center"
  },
  Mystores: {
    flexDirection: "row",
    alignItems: "flex-start",
    justifyContent: "space-between",

    // paddingBottom:15,
  },
  storeCardSpace: {
    marginBottom: 15,
  },
  storeCardPadding: {
    // paddingBottom: 5,
  },
  storeMainView: {
    // marginHorizontal: 20,
    // marginVertical:20,
    // flex: 3,
    backgroundColor: 'white',
  },
  headerstyle: {
    paddingBottom: 20,
    paddingTop: 20
  },

  storeBtnViewColor: {
    backgroundColor: '#ba55d3',
    color: 'white',
  },
  continueBtnViewColor: {
    backgroundColor: '#2f4f4f',
    color: 'white',
  },
  addtoCartBtnColor: {
    fontSize:12,
    backgroundColor: '#00009a',
    color: 'white',
    marginLeft: 20,
  },
  storeBtn: {
    paddingVertical: 5,
    paddingHorizontal: 5,
    marginBottom: 5,
    color: 'white',
    borderRadius: 0,
  },
  basketBtn:{
    paddingVertical: 5,
    paddingHorizontal: 10,
    marginBottom: 10,
    borderRadius: 20,
    height: 30,
    width: 30
  },
  basketImg: {
    height: 30,
    width: 30,
  }

});



  

export default ProductListing;