/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Fragment } from 'react';
import axios from 'axios';
import * as Config from '../../Constant/Config.js';
import * as Constant from '../../Constant/Constant.js';
import InternateModal from '../ReusableComponent/InternetModal';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as navigateAction  from '../../Actions/NavigationBarAction';
import Toast from 'react-native-simple-toast';
// import 'bootstrap/dist/css/bootstrap.min.css';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  TouchableOpacity,
  ToastAndroid,
  FlatList,
  Image
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

function ShopList({ shopId,shopName,shopDescription,shopRating,logo, navigation}) {
  return (

    <View style={styles.sectionContainer}>
      <View style={styles.flexRowSpaceBetween}>
        <TouchableOpacity style={styles.imageCards}>
          <Image source={require('../../../assets/medicalcategory.jpeg')}></Image>
        </TouchableOpacity>
        <View style={{ flex: 0.2 }} />
        <View style={styles.storeCards}>
          <TouchableOpacity style={styles.bookCardLayout} >
          {/* onPress={() => navigation.navigate('shopListPage',{"categoryId": categoryId})} */}
            <Text style={styles.storeHeading} numberOfLines = { 1 }>{shopName}</Text>
            <Text style={styles.storeDesc} numberOfLines = { 1 }>{shopDescription}</Text>
            <Text style={styles.storeDesc} numberOfLines = { 1 }>{shopRating}</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  )


}

class ShopListPage extends React.Component{
  constructor(props) {
    super(props);
    this.state={
      data: ""
    }
  }
  componentWillMount(){
  const categoryId = this.props.navigation.state.params.categoryId;
  console.warn("cat id-----",this.props.navigation.state.params);

    axios.post(
      Config.URL + Config.getCategoriesByShop,
      {
        "categoryId": categoryId,
      },
      {
        headers: {
            'Content-Type': 'application/json',
        }
      }
    )
    .then(res => {
        console.warn("res---",res.data.result);
        if (res.data.status == 0) {
          this.setState({
            data: res.data.result
          });
        }
        else if(res.data.status == 1){  // Fail to signup
          Toast.show(res.data.message,Toast.LONG);
        }

      }).catch(err => {
          //console.warn(res.data.status);
      })

  }

  render() {
    const {navigate} = this.props.navigation;
    return (
      <SafeAreaView>
        <ScrollView>
            <View style={styles.body}>
              {/* <InternateModal /> */}
                <View style={styles.sectionContainer}>
                    <View style={styles.advertiseData}></View>
                </View>
            <FlatList data={this.state.data}
              renderItem={( { item } ) => (
                <ShopList
                  shopId={item.shopId}
                  shopName={item.shopName}
                  shopDescription={item.shopDescription}
                  rating = {item.shopRating}
                  logo={item.shoplogo}
                  navigation={this.props.navigation}
                />
              )}
              keyExtractor= {item => item.categoryId}
              horizontal={false}
            />
          </View>
        </ScrollView>
        {/* <NavigationBar></NavigationBar> */}
      </SafeAreaView>
 
    );
  }
}


const styles = StyleSheet.create({

  storeHeadingView:{
    display:'flex',
    flexDirection:'row',
    justifyContent:'space-between',
    padding:10
  },
  StoreCardsLayout:{
    flex: 1,
    flexDirection: 'row',
  justifyContent: 'space-between'
},
  materialTypeHeading:{
    color: 'black',
    fontSize:18,
    fontWeight: "bold",
    paddingBottom:10
},
  categoryHeadingTxt:{
    color: 'white',
    fontSize:16,
    textAlign:'center'
},
  flexRowSpaceBetween:{
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  bookCardLayout:{
    padding:10,
    display:'flex',
  flexDirection:'column'
},
  categoryHeadingView:{
    borderBottomColor: 'lightgray',
    borderBottomWidth: 1,
    padding:10,
    textAlign:'center',
  },
  storeHeading:{
    color: 'black',
    fontSize:18,
    fontWeight: "bold"
  },
  storeDesc:{
    color: 'gray',
    fontSize:16
  },
  trendingShops: {
    borderWidth: 1,
    borderColor: 'lightgray',
    borderRadius: 4,
    marginTop: 32,
    padding: 15,
    margin: 10,
  },
  advertiseData: {
    height:150,
    borderRadius: 4,
    borderWidth: 0.5,
    borderColor: 'black',
    backgroundColor: 'grey',
    flex: 1,
  },
  imageCards: {
    height: 100,
    borderRadius: 4,
    borderWidth: 0.5,
    borderColor: 'black',
    backgroundColor: 'lightgray',
    flex: 1,
  },
  offerCards:{
    width: 130,
    height: 130,
    backgroundColor:'orange',
    borderRadius: 100,
    borderWidth: 0.5,
    borderColor: 'black',
    marginVertical:10,
    marginHorizontal:10,
  },
  offerCardDisplay:{
    padding:10,
    display:'flex',
    flexDirection:'column',
    alignItems:'center',
  },
  storeCards: {
    height:100,
    borderRadius: 4,
    borderWidth: 0.5,
    borderColor: 'black',
    backgroundColor: 'white',
    flex: 3,
  },
  productHeader: {
    height: 100,
    borderColor: 'black',
  },
  storeContainer: {
    borderRadius: 4,
    borderWidth: 1,
    borderColor: 'black',
    backgroundColor: 'navy',
  },
  sectionTitle: {
    borderRadius: 4,
    borderWidth: 0.5,
    borderColor: '#d6d7da',
    // fontSize: 24,
    // fontWeight: '600',
    color: Colors.black,
    // borderStyle:1px solid red
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
  
  scrollView: {
    backgroundColor: Colors.lighter,
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  body: {
    backgroundColor: "#F5FCFF",
  },
  sectionContainer: {
    marginTop: 24,
    paddingHorizontal: 24,
    
  },
  storeHeadinglbl:{ 
    color: 'white',
    fontSize:16 
  },

  categoryHeadingTxt:{ 
    color: 'white',
    fontSize:16,
    textAlign:'center' 
},
  flexRowSpaceBetween:{ 
    display: 'flex', 
    flexDirection: 'row', 
    justifyContent: 'space-between' 
  },
  bookCardLayout:{
    padding:10,
    display:'flex',
  flexDirection:'column'
},
  categoryHeadingView:{
    borderBottomColor: 'lightgray',
    borderBottomWidth: 1,
    padding:10,
    textAlign:'center',
  },
  storeHeading:{
    color: 'black',
    fontSize:18,
    fontWeight: "bold"
  },
  storeDesc:{ 
    color: 'gray',
    fontSize:16
  },
  trendingShops: {
    borderWidth: 1,
    borderColor: 'lightgray',
    borderRadius: 4,
    marginTop: 32,
    padding: 15,
    margin: 10,
  },
  advertiseData: {
    borderRadius: 4,
    borderWidth: 0.5,
    borderColor: 'black',
    backgroundColor: 'white',
    flex: 1,
  },
  imageCards: {
    height: 100,
    borderRadius: 4,
    borderWidth: 0.5,
    borderColor: 'black',
    backgroundColor: 'lightgray',
    flex: 1,
  },
  offerCards:{
    width: 130,
    height: 130,
    backgroundColor:'orange',
    borderRadius: 100,
    borderWidth: 0.5,
    borderColor: 'black',
    marginVertical:10,
    marginHorizontal:10,
  },
  offerCardDisplay:{
    padding:10,
    display:'flex',
    flexDirection:'column',
    alignItems:'center',
  },
  storeCards: {
    height:100,
    borderRadius: 4,
    borderWidth: 0.5,
    borderColor: 'black',
    backgroundColor: 'white',
    flex: 3,
  },
  productHeader: {
    height: 100,
    borderColor: 'black',
  },
  storeContainer: {
    borderRadius: 4,
    borderWidth: 1,
    borderColor: 'black',
    backgroundColor: 'navy',
  },
  sectionTitle: {
    borderRadius: 4,
    borderWidth: 0.5,
    borderColor: '#d6d7da',
    // fontSize: 24,
    // fontWeight: '600',
    color: Colors.black,
    // borderStyle:1px solid red
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
});


const mapStateToProps = (state) => {
	return {
	}
}

const mapDispatchToProps = (dispatch) => {
    return {
        navigationAction: bindActionCreators(navigateAction,dispatch),
    };
}

// ---------------------------------------------------- store related function  -- END


export default connect(mapStateToProps, mapDispatchToProps)(ShopListPage);
