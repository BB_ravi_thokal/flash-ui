//Action type for cartItemAction
export const CART_CHANGE = 'CART_CHANGE';
export const CART_REPLACE = 'CART_REPLACE';
export const PLACED_CART = 'PLACED_CART';

//Action type for loginAction
export const USER_LOGIN = 'USER_LOGIN';
export const OFFER_DETAILS = 'OFFER_DETAILS';
export const AVAILABLE_PINCODES = 'AVAILABLE_PINCODES';

//Action type for loginAction
export const ADD_BASKET = 'ADD_BASKET';
export const REPLACE_BASKET = 'REMOVE_BASKET';
export const PLACED_BID = 'PLACED_BID';

//Action type for CartModalAction
export const CHANGE_CART_STATE = 'CHANGE_CART_STATE';
export const CHANGE_BID_STATE = 'CHANGE_BID_STATE';
export const CHANGE_ORDER_STATE = 'CHANGE_ORDER_STATE';
export const CHANGE_GET_ACTIVITY_STATE = 'CHANGE_GET_ACTIVITY_STATE';
export const CHANGE_SEND_ACTIVITY_STATE = 'CHANGE_SEND_ACTIVITY_STATE';

//Action type for Activity action
export const PICKUP_ADDRESS = 'PICKUP_ADDRESS';
export const DELIVERY_ADDRESS = 'DELIVERY_ADDRESS';
export const PLACE_ACTIVITY = 'PLACE_ACTIVITY';
export const ONGOING_ACTIVITY = 'ONGOING_ACTIVITY';

//Action type for Get package action
export const GET_PKG_PICK_ADDRESS = 'GET_PKG_PICK_ADDRESS';
export const GET_PKG_DELIVER_ADDRESS = 'GET_PKG_DELIVER_ADDRESS';
export const PLACED_GET_PKG = 'PLACED_GET_PKG';
export const ONGOING_GET_PKG = 'ONGOING_GET_PKG';

export const INTERNET_CHANGE = 'INTERNET_CHANGE';

export const SELECTED_ICON = 'SELECTED_ICON';  
export const SELECTED_DRIVER_ICON = "SELECTED_DRIVER_ICON";
export const DRIVER_LOGIN = "DRIVER_LOGIN";
export const SELECTED_SHOP_ICON = "SELECTED_SHOP_ICON";

export const COUPON_CODE = "COUPON_CODE";
export const CHANGE_LOCATION = "CHANGE_LOCATION";
export const CHANGE_WALLET_AMOUNT = "CHANGE_WALLET_AMOUNT";

export const USER_LOGGED_OUT = "USER_LOGGED_OUT";

export const SELECTED_ADMIN_ICON = 'SELECTED_ADMIN_ICON';

export const DRIVER_STATUS_CHANGE = 'DRIVER_STATUS_CHANGE';

export const TOP_SLIDER = 'TOP_SLIDER';

export const ADD_LOCATION = 'ADD_LOCATION';
export const REPLACE_LOCATION = 'REPLACE_LOCATION';




