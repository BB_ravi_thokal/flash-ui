// const BASE_URL = 'http://192.168.0.107:';
//const BASE_URL = 'http://198.168.43.212:';
// const BASE_URL = 'http://6c0a92780dc2.ngrok.io/';

// const BASE_URL = 'http://ec2-13-234-122-244.ap-south-1.compute.amazonaws.com:'; // Test Machine
const BASE_URL = 'http://ec2-3-92-221-138.compute-1.amazonaws.com/'; // c8  Machine
// const BASE_URL = 'http://2284109ae201.ngrok.io/';
// const port = '2000/';
export const encryptionKey = '0what$je569ywhatever1';

export const URL = BASE_URL;
export const loginUser = 'api/user/login';
export const addNewUser = 'api/user/add-user-login';
export const getUserDetails = 'api/user/get-user-details';
export const gmailLoginUser = 'api/user/google-login';
export const fetchUserData = 'api/user/get-user-detials-by-user-and-role-id';

export const addUserAddress = 'api/user/add-update-user-address';
export const deleteUserAddress = 'api/user/delete-user-address';
export const addUpdateUserDetails = 'api/user/add-update-user-details';

export const addUpdateBasket = 'api/user/add-update-basket-details';
export const applyCoupon = 'api/user/user-coupon-offer';
export const addUpdateCart = 'api/user/add-update-cart-details';

export const generateOTP = 'api/user/forgot-password';
export const verifyOTP = 'api/user/verify-otp';
export const resetPassword = 'api/user/update-password';
export const genrateOTPSMS = 'api/user/generate-otp-and-send-sms';

export const getCategories = 'api/categories/get-categories';
export const getMenuSubCategories = 'api/categories/get-shop-menu-sub-categories';
export const getCategoriesByShop = 'api/categories/get-shop-by-categories';
export const getShopMenu = 'api/categories/get-shop-menu';
export const productSearch = 'api/categories/get-product-by-search';
export const shopSearch = 'api/categories/get-shop-by-search';
export const getShopMenuCategories = 'api/categories/get-shop-menu-categories';
export const getShopByProductSearch = 'api/categories/get-shop-on-product-search';

export const checkout = 'api/communication/initiate-payment';
export const placedOrder = 'api/user/add-order-details';
export const verifyOrder = 'api/communication/verify-delivery-time';
export const sendPackage = 'api/activity/add-send-package-activity';
export const getPackage = 'api/activity/add-get-package-activity';
export const placedBid = 'api/user/place-bid-for-shop';
export const getBidByUserId = 'api/user/get-bid-for-user';
export const getBidByBidId = 'api/user/get-bid-details-for-user';
export const getAllBidForShop = 'api/user/get-bid-for-shop';
export const placeBidShop = 'api/user/place-bid-for-shop';
export const placeBidForUser = 'api/user/place-bid-for-user';
export const updateBidShop = 'api/user/update-existing-bid-of-shop';
export const calculatePriceForActivity = 'api/user/calculate-price-for-activity';

// Driver Api
export const getPlacedOrderDriver = 'api/user/get-order-details';
export const ongoingOrderListDriver = 'api/user/get-order-details-for-delivery-boy';
export const pastDeliveredOrderDriver = 'api/user/get-past-delivered-order';
export const orderDeliveredDriver = 'api/user/order-delivered';
export const assignedDeliveryBoy = 'api/user/assign-order-for-delivery';
export const updateOrderStatus = 'api/user/update-order-status';
export const getOrderDetailsUser = 'api/user/get-order-details-for-user'


export const getState = 'api/get-state-list';
export const getCity = 'api/get-city-list-by-state';

export const logout = 'api/user/logout';

export const markProductUnavailable = 'api/user/update-unavailable-product-of-order';
export const getWallet = 'api/user/get-wallet';
export const cancelOrder = 'api/user/cancel-order';
export const withDrawWallet = 'api/user/withdraw-wallet';
export const updateWithdrawalStatus = 'api/user/update-withdrawal-status';
export const getWithdrawalWalletList = 'api/user/get-withdrawal-wallet-list';
export const updateDeliveryBoyStatus = 'api/user/update-delivery-boy-status';
export const getWithDrawRequest = 'api/user/get-withdrawal-wallet-list';
export const updateWithDrawalStatus = 'api/user/update-withdrawal-status';

export const getComboOffers = 'api/categories/get-combo-details';
export const getTopSlider = 'api/user/get-banner-details';

export const paytmViewURL = URL + 'api/communication/initiate-payment-ios?';
export const reOrder = 'api/categories/get-reorder-details';
export const updateCArtBasket = 'api/user/add-update-cart-and-basket';

export const updateDeliveryBoyLocation = 'api/user/update-delivery-boy-status';

// Shop Module
export const getOrderDetailsForShop = 'api/user/get-order-details-for-shop';
export const updateShopOrderStatus = 'api/user/update-shop-order-status';
export const getSalesReportShop = 'api/user/get-sales-report-of-shop';
export const getWholeSalerList = 'api/user/api/user/get-wholesaler-list';
export const getWholeSalerMenu = 'api/user/get-wholesaler-menu';
export const updateShopDetails = 'api/user/update-shop-details';
export const getCouponForShop = 'api/user/get-shop-offer';
export const logoutShop = 'api/user/shop-logout';
export const placeShopOrder = 'api/user/place-shop-order';
export const shopPaytmViewURL = URL + 'api/communication/initiate-payment-shop?';
export const getPastWholeSalerOrder = 'api/user/get-shop-order';

export const getBestSellingProduct = 'api/categories/get-best-seller-menu';
export const saveOrderActivityDetails = 'api/communication/store-order-before-payment';

export const getDataWithoutLogin = 'api/user/skip-login';























