import { COUPON_CODE } from '../Constant/index';

const initialState = {
    couponList: [],
    appliedCoupon: ''
};

const couponReducer = (state = initialState, action) => {
    switch(action.type) {
        case COUPON_CODE:
            console.warn('in coupon reducer', action.couponList);
            return {
                ...state,
                couponList: action.couponList,
                appliedCoupon: action.appliedCoupon
            };  
        default:
        return state;
    }
}

export default couponReducer;