import { GET_PKG_PICK_ADDRESS, GET_PKG_DELIVER_ADDRESS, PLACED_GET_PKG, ONGOING_GET_PKG } from '../Constant/index';

const initialState = {
    pickupAddress: {},
    deliveryAddress: {},
    ongoingActivity: [],
    placedActivity: []
};

const getPackageReducer = (state = initialState, action) => {
    switch(action.type) {
        case GET_PKG_PICK_ADDRESS:
            console.warn("in PICKUP_ADDRESS--",action.pickupAddress);
            return {
                ...state,
                pickupAddress: action.pickupAddress
            };  
        case GET_PKG_DELIVER_ADDRESS: 
            console.warn("in DELIVERY_ADDRESS--",action.pickupAddress);
            return {
                ...state,
                deliveryAddress: action.deliveryAddress
            };  
        case PLACED_GET_PKG: 
            console.warn("in PLACE_ACTIVITY--     here",action.placedActivity);
            return {
                ...state,
                placedActivity: state.placedActivity.concat(action.placedActivity)
            }; 
        case ONGOING_GET_PKG: 
            console.warn("in ONGOING_ACTIVITY--",action.ongoingActivity);
            return {
                ...state,
                ongoingActivity: action.ongoingActivity
            };              
        default:
        return state;
    }
}


export default getPackageReducer;