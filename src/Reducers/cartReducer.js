import { CART_CHANGE, CART_REPLACE, PLACED_CART, PLACED_BID } from '../Constant/index';

const initialState = {
    cartItem: [],
    placedCart: [],
    counter: 0,
    placedBid: []
};

const cartReducer = (state = initialState, action) => {
    switch(action.type) {
        case CART_CHANGE:
            console.warn("--- in the reducer",action.cartItem)
            //var temp = cartItem;
            //temp.push(actions.payload);
            return {
                ...state,
                cartItem: state.cartItem.concat(action.cartItem),
                counter: 0
            };
        case CART_REPLACE:
            console.warn("--- in the reducer CART Replace",action.cartItem)
            //var temp = cartItem;
            //temp.push(actions.payload);
            return {
                ...state,
                cartItem: action.cartItem
            };
        case PLACED_CART:
            console.warn("--- in the reducer placed cart",action.cartItem)
            return {
                ...state,
                placedCart: state.placedCart.concat(action.placedCart)
            };
        case PLACED_BID:
            console.warn("--- in the reducer placed cart",action.placedBid)
            return {
                ...state,
                placedBid: state.placedCart.concat(action.placedBid)
            };    
        default:
        return state;
    }
}

export default cartReducer;