import { CHANGE_LOCATION } from '../Constant/index';

const initialState = {
    currentRegion: {},
    currentPlace: ""
};

const currentLocationReducer = (state = initialState, action) => {
    switch(action.type) {
        case CHANGE_LOCATION:
            console.warn('in location reducer', action.currentRegion, action.currentPlace);
            return {
                ...state,
                currentRegion: action.currentRegion,
                currentPlace: action.currentPlace
            };  
        default:
        return state;
    }
}

export default currentLocationReducer;