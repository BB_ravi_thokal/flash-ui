import { ADD_LOCATION, REPLACE_LOCATION } from '../Constant';

const initialState = {
    driverLocation: [],
};

const locationReducer = (state = initialState, action) => {
    switch(action.type) {
        case ADD_LOCATION:
            console.warn("--- in the reducer",action.driverLocation)
            //var temp = cartItem;
            //temp.push(actions.payload);
            return {
                ...state,
                driverLocation: state.driverLocation.concat(action.driverLocation),
                counter: 0
            };
        case REPLACE_LOCATION:
            console.warn("--- in the reducer location Replace",action.driverLocation)
            //var temp = driverLocation;
            //temp.push(actions.payload);
            return {
                ...state,
                driverLocation: action.driverLocation
            };   
        default:
        return state;
    }
}

export default locationReducer;