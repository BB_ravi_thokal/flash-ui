import { SELECTED_ICON, SELECTED_DRIVER_ICON, SELECTED_SHOP_ICON, SELECTED_ADMIN_ICON } from '../Constant/index';

const initialState = {
    selectedIcon: '',
    driverSelectedIcon: '',
    shopSelectedIcon: '',
    adminSelectedIcon: '',
};

const navigationReducer = (state = initialState, action) => {
    switch(action.type) {
        case SELECTED_ICON:
            console.warn('in selected icon', action.selectedIcon);
            return {
                ...state,
                selectedIcon: action.selectedIcon
            };     
        case SELECTED_DRIVER_ICON:
            return {
                ...state,
                driverSelectedIcon: action.driverSelectedIcon
            }; 
        case SELECTED_SHOP_ICON:
            return {
                ...state,
                shopSelectedIcon: action.shopSelectedIcon
            };
        case SELECTED_ADMIN_ICON:
            return {
                ...state,
                adminSelectedIcon: action.adminSelectedIcon
            };   
        default:
        return state;
    }
}

export default navigationReducer;