import { PICKUP_ADDRESS, DELIVERY_ADDRESS, PLACE_ACTIVITY, ONGOING_ACTIVITY } from '../Constant/index';

const initialState = {
    pickupAddress: {},
    deliveryAddress: {},
    ongoingActivity: [],
    placedActivity: []
};

const activityReducer = (state = initialState, action) => {
    switch(action.type) {
        case PICKUP_ADDRESS:
            console.warn("in PICKUP_ADDRESS--",action.pickupAddress);
            return {
                ...state,
                pickupAddress: action.pickupAddress
            };  
        case DELIVERY_ADDRESS: 
            console.warn("in DELIVERY_ADDRESS--",action.deliveryAddress);
            return {
                ...state,
                deliveryAddress: action.deliveryAddress
            };  
        case PLACE_ACTIVITY: 
            console.warn("in PLACE_ACTIVITY--     here",action.placedActivity);
            return {
                ...state,
                placedActivity: state.placedActivity.concat(action.placedActivity)
            }; 
        case ONGOING_ACTIVITY: 
            console.warn("in ONGOING_ACTIVITY--",action.ongoingActivity);
            return {
                ...state,
                ongoingActivity: action.ongoingActivity
            };              
        default:
        return state;
    }
}


export default activityReducer;