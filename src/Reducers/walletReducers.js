import { CHANGE_WALLET_AMOUNT } from '../Constant/index';

const initialState = {
    walletBalance: 0.00,
    canUse: true
};

const walletReducer = (state = initialState, action) => {
    switch(action.type) {
        case CHANGE_WALLET_AMOUNT:
            console.warn("in CHANGE_WALLET_AMOUNT----------->",action.walletBalance);
            return {
                ...state,
                walletBalance: action.walletBalance,
                canUse: action.canUse
            };          
        default:
        return state;
    }
}

export default walletReducer;