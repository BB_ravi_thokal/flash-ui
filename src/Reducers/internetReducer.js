import { INTERNET_CHANGE } from '../Constant/index';

const initialState = {
    isInternet: true
};

const internetReducer = (state = initialState, action) => {
    switch(action.type) {
        case INTERNET_CHANGE:
            console.warn("internet diable reducer--",action.isInternet);
            return {
                ...state,
                isInternet: action.isInternet
            };              
        default:
        return state;
    }
}


export default internetReducer;