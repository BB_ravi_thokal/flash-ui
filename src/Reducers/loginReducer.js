import { USER_LOGIN, OFFER_DETAILS, DRIVER_LOGIN, AVAILABLE_PINCODES, TOP_SLIDER } from '../Constant/index';

const initialState = {
    userData: {},
    skipLogin: false,
    offerData: [],
    driverData: {},
    availablePincode: [],
    topSlider: []
};

const loginReducer = (state = initialState, action) => {
    switch(action.type) {
        case USER_LOGIN:
            console.warn('login sucess---->', action.userData)
            return {
                ...state,
                userData: action.userData,
            };
        case OFFER_DETAILS:
            console.warn("in USER_LOGIN offer --",action.offerData);
            return {
                ...state,
                offerData: action.offerData
            };
        case DRIVER_LOGIN:
            return {
                ...state,
                driverData: action.driverData
            };
        case AVAILABLE_PINCODES:
            return {
                ...state,
                availablePincode: action.availablePincode
            };
        case TOP_SLIDER:
            return {
                ...state,
                topSlider: action.topSlider
            };    
        default:
        return state;
    }
}

export default loginReducer;