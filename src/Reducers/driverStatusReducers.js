import { DRIVER_STATUS_CHANGE } from '../Constant/index';

const initialState = {
    isActive: false,
};

const driverStatusReducer = (state = initialState, action) => {
    switch(action.type) {
        case DRIVER_STATUS_CHANGE:
            console.warn("in DRIVER_STATUS_CHANGE--",action.isActive);
            return {
                ...state,
                isActive: action.isActive
            };        
        default:
        return state;
    }
}

export default driverStatusReducer;