import { CHANGE_CART_STATE, CHANGE_BID_STATE, CHANGE_ORDER_STATE, CHANGE_SEND_ACTIVITY_STATE, CHANGE_GET_ACTIVITY_STATE } from '../Constant/index';

const initialState = {
    cartState: false,
    cartSum: 0,
    cartCount: 0,
    bidState: false,
    totalPlacedBids: 0,
    orderState: false,
    totalPlacedOrders: 0,
    sendActivityState: false,
    getActivityState: false
};

const cartModalReducer = (state = initialState, action) => {
    switch(action.type) {
        case CHANGE_CART_STATE:
            console.warn('inside change cart state');
            return {
                ...state,
                cartState: action.cartState,
                cartSum: action.cartSum,
                cartCount: action.cartCount
            };
        case CHANGE_BID_STATE:
            return {
                ...state,
                bidState: action.bidState,
            };  
        case CHANGE_ORDER_STATE:
            console.warn('in order state change', action.orderState)
            return {
                ...state,
                orderState: action.orderState,
            };  
        case CHANGE_GET_ACTIVITY_STATE:
            console.warn('in order state change', action.orderState)
            return {
                ...state,
                getActivityState: action.getActivityState,
            }; 
        case CHANGE_SEND_ACTIVITY_STATE:
            console.warn('in order state change', action.orderState)
            return {
                ...state,
                sendActivityState: action.sendActivityState,
            };
            default:    
        return state;
    }
}

export default cartModalReducer;