import { ADD_BASKET, REPLACE_BASKET } from '../Constant/index';

const initialState = {
    basketItem: []
};

const basketReducer = (state = initialState, action) => {
    switch(action.type) {
        case ADD_BASKET:
            console.warn("--- in the reducer basket",action.basketItem)
            return {
                ...state,
                basketItem: state.basketItem.concat(action.basketItem),
            };
          
        case REPLACE_BASKET:
            console.warn("--- in the reducer CART Replace",action.basketItem)
            return {
                ...state,
                basketItem: action.basketItem
            };
        default:
        return state;
    }
}

export default basketReducer;