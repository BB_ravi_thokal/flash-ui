
import { StyleSheet } from 'react-native';

module.exports = StyleSheet.create({
    body: {
        backgroundColor: "white",
        // backgroundColor: "grey",
        flex: 1

    },
    bodyGray: {
        backgroundColor: "#f1f1f1",
        // backgroundColor: "grey",
        flex: 1
    },
    normalText: {
        color: "#003151",
        fontSize: 14,
        fontFamily: "Verdana"
    },
    headerText: {
        color: '#131E3A',
        fontSize:18,
        fontWeight: "bold",
        fontFamily: "Verdana"
    },  
    subHeaderText: {
        color: '#131E3A',
        fontSize:16,
        fontWeight: "bold",
        fontFamily: "Verdana"
    }    

})