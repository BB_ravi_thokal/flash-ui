/**
 * @format
 */

import {AppRegistry} from 'react-native';
import React from 'react';
import App from './App';
import {name as appName} from './app.json';
import { configStore, persistor } from './store/configureStore';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
// import configureStore from './store/configureStore';

// const store = configureStore()

const RNRedux = () => (
    <Provider store={configStore}>
        <PersistGate persistor={persistor}>
            <App />
            </PersistGate>
        </Provider>
  )
// AppRegistry.registerComponent(appName, () => App);
AppRegistry.registerComponent(appName, () => RNRedux);
